//
//  MGOMESubFileImporter.m
//  Filament
//
//  Created by Dennis Lorson on 28/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGOMESubFileImporter.h"
#import "MGOMEFileImporter_Private.h"
#import "MGMetadataSet.h"


@interface MGOMESubFileImporter ()


- (NSRange)_representationRangeFromXMLData;


@end


@implementation MGOMESubFileImporter

- (MGOMESubFileImporter *)initWithCommonMetadata:(MGMetadataSet *)metadata xmlDocument:(NSXMLDocument *)doc 
										filePath:(NSString *)filePath representationFilePath:(NSString *)repFilePath superImporter:(MGOMEFileImporter *)superImporter;
{
	if ((self = [super init])) {
		filePath_ = [filePath retain];
		representationFilePath_ = [repFilePath retain];
		
		doc_ = [doc retain];
		superImporter_ = [superImporter retain];

		commonMetadata_ = [metadata clone];
		
		didFetchRepresentationRange_ = NO;
		representationRange_ = NSMakeRange(NSNotFound, 0);

        

	}
	
	return self;
}


- (void) dealloc
{
	[doc_ release];
	[representationFilePath_ release];
	[commonMetadata_ release];
	[superImporter_ release];
	
	[super dealloc];
}

- (MGImageFileImporter *)superImporter;
{
	return superImporter_;
}

- (NSString *)representationFilePath;
{
	return representationFilePath_;
}



- (NSRange)_representationRangeFromXMLData
{
	if (didFetchRepresentationRange_)
		return representationRange_;
	
	representationRange_ = NSMakeRange(NSNotFound, 0);
	
	NSXMLNode *offsetAttr = [self _elementForXPath:@".//Image[1]/Pixels[1]/TiffData[1]/@IFD" inElement:[doc_ rootElement]];
	NSXMLNode *numPlanesAttr = [self _elementForXPath:@".//Image[1]/Pixels[1]/TiffData[1]/@NumPlanes" inElement:[doc_ rootElement]];
	
	int numPlanes = -1, offset = -1;
	
	if ([numPlanesAttr stringValue]) {
		
		// numPlanes must exist in order to follow the data from the XML
		
		numPlanes = [[numPlanesAttr stringValue] intValue];
		representationRange_.length = numPlanes;
		
		if ([offsetAttr stringValue]) {
			offset = [[offsetAttr stringValue] intValue];
			representationRange_.location = offset;
		}
		else {
			// valid number of planes but no offset?  -->  offset 0
			representationRange_.location = 0;
		}
	}
	
	
	didFetchRepresentationRange_ = YES;
	
	if (representationRange_.location != NSNotFound)
		NSLog(@"%@: %i reps with offset %i", [self description], (int)(representationRange_.length), (int)(representationRange_.location));
	else
		NSLog(@"%@: No range data in metadata, using default", [self description]);
	
	return representationRange_;
	
}

- (MGMetadataSet *)_generateMetadata
{
	// don't call super!
	
	[self _addMetadataFromRoot:[doc_ rootElement] toSet:commonMetadata_];
    
//    // Deferred crash...
//    if (!MG_IS_LICENSE_VALID1((NSInteger)self))
//        [[[[NSObject alloc] init] autorelease] release];

	return commonMetadata_;
}


- (int)numberOfImageRepresentations
{
	NSRange range = [self _representationRangeFromXMLData];
	
	if (range.location == NSNotFound)
		return [super numberOfImageRepresentations];
	else
		return range.length;
}


- (NSBitmapImageRep *)imageRepresentationAtIndex:(int)index
{
	NSRange range = [self _representationRangeFromXMLData];
	
	if (range.location == NSNotFound)
		return [super imageRepresentationAtIndex:index];
	else
		return [super imageRepresentationAtIndex:index + range.location];
	
}


@end
