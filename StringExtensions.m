//
//  StringExtensions.m
//  Macnification
//
//  Created by Peter Schols on 17/04/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "StringExtensions.h"


@implementation NSString (StringExtensions)


// This method takes an absolute path and returns self if self (= the absolute path) does not already exist.
// If path does exist, it returns path-1 or path-2 or ... 
// This method works for both files and folders

- (NSString *)uniquePath;
{	
	NSString *pathExtension = [self pathExtension];
	NSFileManager *fm = [NSFileManager defaultManager];
    
    // Add a number suffix to the path if a file/folder with this name already exists
    NSString *finalName = [NSString stringWithString:self];
    
    int i = 1;
    BOOL isDir;
    while ([fm fileExistsAtPath:[finalName stringByExpandingTildeInPath] isDirectory:&isDir]) {
        // Remove the path extension (in case of a file), add the suffix and add the path extension back to the path
        finalName = [[self stringByDeletingPathExtension] stringByAppendingString:[NSString stringWithFormat:@"-%d", i]];
        
        // Add the extension back to the path if it's not a folder
        if (!isDir)
            finalName = [finalName stringByAppendingPathExtension:pathExtension];
        
        i++;
    }
    
    return finalName;
}


- (NSString *)uniquePathByUsingBasePath:(NSString *)base;
{	
	NSString *pathExtension = [self pathExtension];
	NSFileManager *fm = [NSFileManager defaultManager];
    
    // Add a number suffix to the path if a file/folder with this name already exists
    NSString *name = [NSString stringWithString:self];
    
    NSString *finalName = name;
    
    NSString *fullPath = [base stringByAppendingPathComponent:self];
    NSString *finalFullPath = fullPath;

    int i = 1;
    BOOL isDir;
    while ([fm fileExistsAtPath:[finalFullPath stringByExpandingTildeInPath] isDirectory:&isDir]) {
        // Remove the path extension (in case of a file), add the suffix and add the path extension back to the path
        finalFullPath = [[fullPath stringByDeletingPathExtension] stringByAppendingString:[NSString stringWithFormat:@"-%d", i]];
        finalName = [[name stringByDeletingPathExtension] stringByAppendingString:[NSString stringWithFormat:@"-%d", i]];
        
        // Add the extension back to the path if it's not a folder
        if (!isDir) {
            finalFullPath = [finalFullPath stringByAppendingPathExtension:pathExtension];
            finalName = [finalName stringByAppendingPathExtension:pathExtension];
        }
        
        i++;
    }
    
    return finalName;
}


- (NSString *)stringByAppendingFilenameSuffix:(NSString *)suffix
{    
    return [[[self stringByDeletingPathExtension] stringByAppendingString:suffix] stringByAppendingPathExtension:[self pathExtension]];
}

 
- (NSString *)compositePathExtension;
{
    // returns the extended path extension, e.g. ".stk.tiff"
    
    NSString *remainder = self;
    NSString *compositeExtension = nil;
    
    while (![[remainder stringByDeletingPathExtension] isEqualToString:remainder]) {
        // there's still a path extension
        NSString *extension = [remainder pathExtension];
        remainder = [remainder stringByDeletingPathExtension];
        
        if (compositeExtension)
            compositeExtension = [NSString stringWithFormat:@"%@.%@", extension, compositeExtension];
        else
            compositeExtension = extension;
        
    }
    
    return compositeExtension;
}


- (BOOL)beginsWith:(NSString *)startString;
{
    return ([self rangeOfString:startString].location == 0);
}


@end
