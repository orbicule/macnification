//
//  CaptureDC1394Device.m
//  Filament
//
//  Created by Dennis Lorson on 18/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureDC1394Device.h"

#import "CaptureOpenGLPreview.h"


typedef void (*dc1394capture_callback_t)(dc1394camera_t *, void *);

int
dc1394_capture_schedule_with_runloop (dc1394camera_t * camera,
									  CFRunLoopRef run_loop, CFStringRef run_loop_mode);
void
dc1394_capture_set_callback (dc1394camera_t * camera,
							 dc1394capture_callback_t callback, void * user_data);





@interface CaptureDC1394Device ()



- (void)processFrameForCapture:(dc1394video_frame_t *)frame withSize:(NSSize)size;

- (void)startLiveMode;
- (void)stopLiveMode;
- (void)sendFrameToPreview:(dc1394video_frame_t *)frame;

- (void)updateLiveMode;


- (dc1394error_t)startFormat7Capture;
- (dc1394error_t)startNonFormat7Capture;

@end


@implementation CaptureDC1394Device

@synthesize guid = guid_;
@synthesize controller = controller_;

- (id)initWithDeviceID:(dc1394_device_id)guid controller:(dc1394_t *)ctl;
{
	self = [super init];
	if (self != nil) {
		self.guid = guid;
		self.controller = ctl;
		
		[self startSession];
		
        NSString *vendor = camera_ ? [NSString stringWithCString:camera_->vendor encoding:NSASCIIStringEncoding] : @"Unknown device";
        
		name_ = [vendor retain];
						
	}
	return self;
}

- (void)dealloc 
{
    if (camera_)
        dc1394_camera_free(camera_);
	camera_ = NULL;
    
    [name_ release];
    [super dealloc];
}

- (BOOL)isValid
{
    if (!camera_)
        return NO;
    
    return YES;
}

- (NSString *)name;
{
	return name_;
}

- (NSString *)uidString;
{
    return [[super uidString] stringByAppendingFormat:@":%lld", guid_];
}


- (BOOL)canTakePicture;
{
	return YES;
}

- (NSImage *)iconImage;
{
    if (!camera_)
        return nil;
    
	NSString *vendor = [NSString stringWithCString:camera_->vendor encoding:NSASCIIStringEncoding];
	NSString *model = [NSString stringWithCString:camera_->model encoding:NSASCIIStringEncoding];
	
	NSImage *iconImage;
	
	if ([vendor rangeOfString:@"pixelink" options:NSCaseInsensitiveSearch].location != NSNotFound ||
		[model rangeOfString:@"pixelink" options:NSCaseInsensitiveSearch].location != NSNotFound)
		iconImage = [NSImage imageNamed:@"capture_device_pixelink"];
	else
		iconImage = [NSImage imageNamed:@"capture_device_qt"];

	
	[iconImage setSize:NSMakeSize(16, 16)];
	return iconImage;
}




- (void)didChangeValueForAdjustment:(NSString *)adjustment;
{
    if (!camera_)
        return;
    
	if (adjustment == CaptureAdjustmentExposure) {
		dc1394_feature_set_mode(camera_, DC1394_FEATURE_SHUTTER, DC1394_FEATURE_MODE_MANUAL);
		//dc1394_feature_set_value(camera_, DC1394_FEATURE_SHUTTER, [[adjustments_ objectForKey:CaptureAdjustmentExposure] integerValue]);
	}
}





#pragma mark -
#pragma mark Sessions



void CaptureDCCloseCamera(dc1394camera_t *camera)
{
    if (!camera)
        return;
    
    dc1394_video_set_transmission(camera, DC1394_OFF);
    dc1394_capture_stop(camera);
}


- (void)startSession;
{	
	sessionIsStarted_ = YES;

	if (!camera_)
		camera_ = dc1394_camera_new(self.controller, self.guid);
    
    if (!camera_)
        return;
    
    dc1394error_t err = dc1394_video_set_iso_speed(camera_, DC1394_ISO_SPEED_400);
	DC1394_WRN(err, "Could not set iso speed");
}

- (void)stopSession;
{
	if(!sessionIsStarted_)
		return;
	
	[self disconnectPreviewFromView:preview_];
	
	//NSLog(@"Closing DC session (GUID %llx)", self.guid);
	
	CaptureDCCloseCamera(camera_);
	
	sessionIsStarted_ = NO;
}



#pragma mark -
#pragma mark Acquisition


- (void)takePicture
{
	[self stopLiveMode];
    
    if (!camera_)
        return;
	
	
	dc1394error_t err = DC1394_SUCCESS;
	dc1394video_frame_t *frame = NULL;
	
	dc1394video_modes_t modes;
	
	err = dc1394_video_get_supported_modes(camera_, &modes);
	DC1394_ERR_CLN(err,, "Could not get video modes");

	dc1394video_mode_t mode;

	
	BOOL modeFound = NO;
	
	// find out if the camera supports format 7
	for (int i = 0; i < modes.num; i++) {
		if (modeFound) break;
		
		if (modes.modes[i] <= DC1394_VIDEO_MODE_FORMAT7_MAX && modes.modes[i] >= DC1394_VIDEO_MODE_FORMAT7_MIN) {
			mode = modes.modes[i];
			modeFound = YES;
		}
	}
	
	if (modeFound) {
		
		err=dc1394_video_set_mode(camera_, mode);	
		DC1394_ERR_CLN(err,, "Could not set video mode");
		
		err = [self startFormat7Capture];
	}
	
	if (!modeFound || err != DC1394_SUCCESS) {
		
		// choose the max "other" format and adjust its settings
		mode = modes.modes[modes.num - 1];
		
		err=dc1394_video_set_mode(camera_, mode);	
		DC1394_ERR_CLN(err,, "Could not set video mode");
		
		err = [self startNonFormat7Capture];
		
	}


	
	err = dc1394_capture_setup(camera_, 4, DC1394_CAPTURE_FLAGS_DEFAULT); 
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera_), "Could not setup capture\n");

    err = dc1394_video_set_transmission(camera_, DC1394_ON);
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera_), "Could not start framerate\n");

	err = dc1394_capture_dequeue(camera_, DC1394_CAPTURE_POLICY_WAIT, &frame);
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera_), "Could not dequeue frame\n");
	
	//dc1394_get_image_size_from_video_mode(camera_, mode, &w, &h);
	
	[self processFrameForCapture:frame withSize:NSMakeSize(0, 0)];
		
	err = dc1394_capture_enqueue(camera_, frame);                          
 
	CaptureDCCloseCamera(camera_);
	
	
	[self startLiveMode];
	
}

- (void)processFrameForCapture:(dc1394video_frame_t *)frame withSize:(NSSize)size
{	
	//NSLog(@"Received image from DC1394 device");
	
	// debayer
	dc1394video_frame_t *debayeredFrame;
	if (isInFormat7Mode_) {
		debayeredFrame = calloc(1, sizeof(dc1394video_frame_t));
		dc1394_debayer_frames(frame, debayeredFrame, DC1394_BAYER_METHOD_BILINEAR);
	} else {
		debayeredFrame = frame;
	}
	
	
	// create a new, color converted frame
	dc1394video_frame_t *convertedFrame = calloc(1, sizeof(dc1394video_frame_t));
	convertedFrame->color_coding = DC1394_COLOR_CODING_RGB8;
	dc1394_convert_frames(debayeredFrame, convertedFrame);
	
	if (isInFormat7Mode_) {
		free(debayeredFrame->image);
		free(debayeredFrame);
	}
	
	
	unsigned char *data = convertedFrame->image;
	
	int w = convertedFrame->size[0];
	int h = convertedFrame->size[1];
	
	int bps = convertedFrame->data_depth;
	int bpr = convertedFrame->stride;
		
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:&data
																	 pixelsWide:w
																	 pixelsHigh:h
																  bitsPerSample:bps
																samplesPerPixel:3
																	   hasAlpha:NO
																	   isPlanar:NO
																 colorSpaceName:NSDeviceRGBColorSpace
																   bitmapFormat:0
																	bytesPerRow:bpr
																   bitsPerPixel:0] autorelease];
	
	
	CIImage *ciimage = [[[CIImage alloc] initWithBitmapImageRep:rep] autorelease];
	
	if ([self.delegate respondsToSelector:@selector(captureDevice:adjustedImage:)])
		ciimage = [self.delegate captureDevice:self adjustedImage:ciimage];
	
	
	NSImage *image = [[[NSImage alloc] initWithSize:NSMakeSize(w, h)] autorelease];
	[image addRepresentation:[NSCIImageRep imageRepWithCIImage:ciimage]];
	
	NSString *path = @"/var/tmp/wDC1394File.tiff";
	
	[[image TIFFRepresentation] writeToFile:path atomically:YES];
	
	if ([self.delegate respondsToSelector:@selector(captureDevice:didReceivePicturesForImport:)])
		[self.delegate captureDevice:self didReceivePicturesForImport:[NSArray arrayWithObject:path]];

    // delegate is responsible for removing files

}





#pragma mark -
#pragma mark Live mode


- (CapturePreviewType)previewType; 
{ 
	return CapturePreviewOpenGL;
}

- (void)connectPreviewToView:(NSView *)view;
{
	[preview_ release];
	preview_ = (CaptureOpenGLPreview *)[view retain];
	
	[self startLiveMode];
}

- (void)disconnectPreviewFromView:(NSView *)view;
{
	[self stopLiveMode];
	
	[preview_ release];
	preview_ = nil;
}


static void DCLiveModeFrameCallBack(dc1394camera_t *camera, void *userData)
{
	CaptureDC1394Device *dev = (CaptureDC1394Device *)userData;

	// ignore frames if we're not live anymore
	if (!dev->isInLiveMode_)
		return;
	
	
	dc1394video_frame_t *frame;
	dc1394error_t err = dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera), "Could not dequeue frame\n");
		
	if (!frame)
		return;
	
	[dev sendFrameToPreview:frame];
	
	err = dc1394_capture_enqueue(camera, frame);
	
}

- (void)sendFrameToPreview:(dc1394video_frame_t *)frame
{
	// debayer
	dc1394video_frame_t *debayeredFrame;
	if (isInFormat7Mode_) {
		debayeredFrame = calloc(1, sizeof(dc1394video_frame_t));
		dc1394_debayer_frames(frame, debayeredFrame, DC1394_BAYER_METHOD_BILINEAR);
	} else {
		debayeredFrame = frame;
	}

	
	// create a new, color converted frame
	dc1394video_frame_t *convertedFrame = calloc(1, sizeof(dc1394video_frame_t));
	convertedFrame->color_coding = DC1394_COLOR_CODING_RGB8;
	dc1394_convert_frames(debayeredFrame, convertedFrame);
	
	if (isInFormat7Mode_) {
		free(debayeredFrame->image);
		free(debayeredFrame);
	}

	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:&(convertedFrame->image)
																	 pixelsWide:convertedFrame->size[0]
																	 pixelsHigh:convertedFrame->size[1]
																  bitsPerSample:8
																samplesPerPixel:3
																	   hasAlpha:NO
																	   isPlanar:NO
																 colorSpaceName:NSDeviceRGBColorSpace
																   bitmapFormat:0
																	bytesPerRow:0//convertedFrame->stride
																   bitsPerPixel:0] autorelease];
	
	
	CIImage *image = [[[CIImage alloc] initWithBitmapImageRep:rep] autorelease];
	
	if ([self.delegate respondsToSelector:@selector(captureDevice:adjustedImage:)])
		image = [self.delegate captureDevice:self adjustedImage:image];
	
	[preview_ setImage:image];
	 
	[pool drain];
	
	free(convertedFrame->image);
	free(convertedFrame);
	
	 
}

- (void)startLiveMode
{
	if (isInLiveMode_)
		return;
    
    if (!camera_)
        return;
		
	dc1394error_t err = DC1394_SUCCESS;
	
	dc1394video_modes_t modes;

	
	err = dc1394_video_get_supported_modes(camera_, &modes);
	DC1394_ERR_CLN(err,, "Could not get video modes");

	dc1394video_mode_t mode;
	BOOL modeFound = NO;
	
	// find out if the camera supports format 7
	for (int i = 0; i < modes.num; i++) {
		if (modeFound) break;
		
		if (modes.modes[i] <= DC1394_VIDEO_MODE_FORMAT7_MAX && modes.modes[i] >= DC1394_VIDEO_MODE_FORMAT7_MIN) {
			mode = modes.modes[i];
			modeFound = YES;
		}
	}
	
	if (modeFound) {
		
		err=dc1394_video_set_mode(camera_, mode);	
		DC1394_ERR_CLN(err,, "Could not set video mode");
		
		err = [self startFormat7Capture];
	}
	
	if (!modeFound || err != DC1394_SUCCESS) {
		
		// choose the max "other" format and adjust its settings
		mode = modes.modes[modes.num - 1];
		
		err=dc1394_video_set_mode(camera_, mode);	
		DC1394_ERR_CLN(err,, "Could not set video mode");
		
		err = [self startNonFormat7Capture];
		
	}

	
	err = dc1394_capture_schedule_with_runloop(camera_, CFRunLoopGetMain(), kCFRunLoopCommonModes);
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera_), "Could not schedule live mode\n");
	dc1394_capture_set_callback(camera_, DCLiveModeFrameCallBack, self);
	
	err = dc1394_capture_setup(camera_, 4, DC1394_CAPTURE_FLAGS_DEFAULT); 
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera_), "Could not setup live mode\n");
	
    err = dc1394_video_set_transmission(camera_, DC1394_ON);
	DC1394_ERR_CLN(err, CaptureDCCloseCamera(camera_), "Could not start live transmission\n");
	
	
	isInLiveMode_ = YES;
}


- (void)stopLiveMode
{
	if (!isInLiveMode_)
		return;
    
    if (!camera_)
        return;
	
	CaptureDCCloseCamera(camera_);
	
	isInLiveMode_ = NO;
}

- (void)updateLiveMode
{
	[self stopLiveMode];
	[self startLiveMode];
}


#pragma mark -
#pragma mark Camera config

- (dc1394error_t)startFormat7Capture
{
    if (!camera_)
        return DC1394_FAILURE;
    
	dc1394error_t err = DC1394_SUCCESS;
	dc1394format7mode_t f7mode;
	dc1394video_mode_t mode;
	
	err = dc1394_video_get_mode(camera_, &mode);
	DC1394_ERR_RTN(err, "Could not get video mode");
	
	err = dc1394_format7_get_mode_info(camera_, mode, &f7mode);
	DC1394_ERR_RTN(err, "Could not get mode info");
	
	//uint32_t w = f7mode.max_size_x;
	//uint32_t h = f7mode.max_size_y;
	
	/////////////////////////////////
	// get packet size
	
	uint32_t unitBytes, maxBytes;
	err = dc1394_format7_get_packet_parameters(camera_, mode, &unitBytes, &maxBytes);
	DC1394_ERR_RTN(err, "Could not get packet params");

	/////////////////////////////////
	
	//err = dc1394_format7_set_roi(camera_, mode, f7mode.color_coding, unitBytes, 0, 0, w, h);
	DC1394_ERR_RTN(err, "Could not set capture ROI");
	
	
	isInFormat7Mode_ = YES;
	
	return err;
}

- (dc1394error_t)startNonFormat7Capture
{
    if (!camera_)
        return DC1394_FAILURE;
    
	dc1394error_t err = DC1394_SUCCESS;
	
	dc1394video_mode_t mode;
	dc1394framerates_t framerates;
	
	err = dc1394_video_get_mode(camera_, &mode);
	DC1394_ERR_RTN(err, "Could not get video mode");
	err = dc1394_video_get_supported_framerates(camera_, mode, &framerates);
	DC1394_ERR_RTN(err, "Could not get supported framerates");
	err = dc1394_video_set_framerate(camera_, framerates.framerates[framerates.num - 1]);
	DC1394_ERR_RTN(err, "Could not set framerate");
	
	isInFormat7Mode_ = NO;
	
	return err;
}

#pragma mark -
#pragma mark Adjustments

- (void)resetExposure;
{
    // do nothing
	//[self setExposure:0.5];
}

- (void)setExposureAbsolute:(CGFloat)exposure;
{	    
    if (!([self exposureModes] & CaptureExposureModeManual))
		return;
    
    if (!camera_)
        return;
    
   // NSLog(@"set exposure abs: %f", exposure);

	
	exposure_ = exposure;
	
	dc1394error_t err = DC1394_SUCCESS;	
	
    err = dc1394_feature_set_mode(camera_, DC1394_FEATURE_SHUTTER, DC1394_FEATURE_MODE_MANUAL);
    DC1394_WRN(err, "Could not set exposure");
    
    float min, max;
    dc1394_feature_get_absolute_boundaries(camera_, DC1394_FEATURE_SHUTTER, &min, &max);
    float adjusted = exposure / (max - min) - min;
    
    //NSLog(@"set exposure adj: %f", adjusted);

    err = dc1394_feature_set_value(camera_, DC1394_FEATURE_SHUTTER, (uint32_t)(adjusted * (float)0xFFF));
    
	DC1394_WRN(err, "Could not set exposure");
	
}

- (CGFloat)absoluteExposure;
{
    uint32_t exposure;
    
    if (!camera_)
        return 0.;
    
    dc1394_feature_get_value(camera_, DC1394_FEATURE_SHUTTER, &exposure);
    
    float min, max;
    dc1394_feature_get_absolute_boundaries(camera_, DC1394_FEATURE_SHUTTER, &min, &max);

    float rel = (float)exposure/(float)0xFFF;
    
    rel = min + (max - min) * rel;
    
    
	//NSLog(@"ret absolute exposure: %f", rel);
    
    return rel;
}


- (void)setAutoExposure:(BOOL)autoExposure
{        
    if (!([self exposureModes] & CaptureExposureModeManual))
		return;
    
    if (!camera_)
        return;
		
	dc1394error_t err = DC1394_SUCCESS;	
	
	if (autoExposure)
		err = dc1394_feature_set_mode(camera_, DC1394_FEATURE_SHUTTER, DC1394_FEATURE_MODE_AUTO);
    else
		err = dc1394_feature_set_mode(camera_, DC1394_FEATURE_SHUTTER, DC1394_FEATURE_MODE_MANUAL);
    
    DC1394_WRN(err, "Could not set exposure");

}

- (BOOL)autoExposure 
{
    if (!camera_)
        return NO;
    
    dc1394feature_mode_t currentMode;
    dc1394_feature_get_mode(camera_, DC1394_FEATURE_SHUTTER, &currentMode);
    
    return (currentMode == DC1394_FEATURE_MODE_AUTO);
}




- (CaptureExposureMode)exposureModes
{    
    if (!camera_)
        return 0;
    
    dc1394bool_t isPresent;
	dc1394_feature_is_present(camera_, DC1394_FEATURE_SHUTTER, &isPresent);
	
	if (isPresent != DC1394_TRUE)
        return CaptureExposureModeNone;


    dc1394feature_modes_t supportedModes;
    dc1394_feature_get_modes(camera_, DC1394_FEATURE_SHUTTER, &supportedModes);
    
    
    CaptureExposureMode modes = CaptureExposureModeNone;

    for (int i = 0 ; i < supportedModes.num; i++) {
        if (supportedModes.modes[i] == DC1394_FEATURE_MODE_AUTO)
            modes = modes | CaptureExposureModeAuto;
        
        if (supportedModes.modes[i] == DC1394_FEATURE_MODE_MANUAL)
            modes = modes | CaptureExposureModeManual;
    }
    
    return modes;
}


- (CaptureAdjustmentRange)exposureRange
{
    CaptureAdjustmentRange range;
 
    if (!camera_)
        return range;
    
    dc1394_feature_get_absolute_boundaries(camera_, DC1394_FEATURE_SHUTTER, &range.min, &range.max);
    
    range.linear = NO;
    
    return range;
}

- (NSString *)exposureDescription
{
    return [NSString stringWithFormat:@"%1.0f ms", [self absoluteExposure] * 1000.];
}


@end
