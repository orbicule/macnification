//
//  NSController_MGAdditions.h
//  Filament
//
//  Created by Dennis Lorson on 01/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSController (MGAdditions)


- (void)batchFaultObjects:(NSArray *)objects ofEntity:(NSString *)entity;


@end
