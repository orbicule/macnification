//
//  KeywordTreeController.m
//  Macnification
//
//  Created by Peter Schols on 08/12/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import "KeywordTreeController.h"

#import "Keyword.h"


@implementation KeywordTreeController


- (void)awakeFromNib
{
	[super awakeFromNib];
	
	[keywordOutlineView setAutosaveExpandedItems:NO];

	[keywordOutlineView registerForDraggedTypes:[NSArray arrayWithObjects:@"keywordsPboardType", nil]];
	
	NSSortDescriptor *desc = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];
	
	[self setSortDescriptors:[NSArray arrayWithObjects:desc, nil]];
	
	// don't do this in the nib. it fails...
	[keywordOutlineView setAllowsMultipleSelection:YES];
	
}

- (BOOL)outlineView:(NSOutlineView *)outlineView acceptDrop:(id < NSDraggingInfo >)info item:(id)item childIndex:(NSInteger)index
{
	
	Keyword *destinationKeyword = [item representedObject];
	NSSet *sourceKeywords = [self draggedKeywords];
	
	for (Keyword *keyword in sourceKeywords) {
		
		[keyword setValue:destinationKeyword forKey:@"parent"];
		
		
	}
	
	[outlineView expandItem:item];
	
	return YES;
}


- (NSDragOperation)outlineView:(NSOutlineView *)outlineView validateDrop:(id < NSDraggingInfo >)info proposedItem:(id)item proposedChildIndex:(NSInteger)index
{
	//if (item == nil) return NSDragOperationNone;
	
	id destinationNode = nil;
	
	if (index != NSOutlineViewDropOnItemIndex) {
		
		NSArray *nodeChildren = [item childNodes];
		NSInteger destinationIndex = MIN(index, [nodeChildren count] - 1);
		
		destinationNode = [nodeChildren objectAtIndex:destinationIndex];
		
	} else {
		
		destinationNode = item;
		
	}
	
	//NSLog([[destinationNode representedObject] description]);
	
	
	[outlineView setDropItem:destinationNode dropChildIndex:NSOutlineViewDropOnItemIndex];
	
	
	// if we try to drag the dragItems onto one of themselves, disallow it..
	for (Keyword *keyword in [(Keyword *)[destinationNode representedObject] entireAncestryObjectArray]) {
		//NSLog(@"array: %@", [[(Keyword *)[destinationNode representedObject] entireAncestryObjectArray] description]);
		//NSLog(@"dragged:  %@", [draggedKeywords description]);
		for (Keyword *draggedKeyword in draggedKeywords) {
			NSString *draggedKeywordURI = [[[draggedKeyword objectID] URIRepresentation] absoluteString];
			NSString *destNodeURI = [[[keyword objectID] URIRepresentation] absoluteString];
			
			if ([draggedKeywordURI isEqual:destNodeURI]) {
				
				//NSLog(@"don't drag on self...");
				return NSDragOperationNone;
			}
			
			
		}
				
	}
	
	return NSDragOperationMove;
	
}


- (BOOL)outlineView:(NSOutlineView *)outlineView writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard;
{
    // Pasteboard
	[pboard declareTypes:[NSArray arrayWithObject:@"keywordsPboardType"] owner:self];
	
	// Create the new set of draggedKeywords and fill it
	NSMutableSet *keywordsToDrag = [NSMutableSet setWithCapacity:5];
	for (id item in items) {
	    id keyword = [item representedObject];
		[keywordsToDrag addObject:keyword];
		//NSLog(@"Keyword itself: %@", [keyword valueForKey:@"name"]);
		//NSLog(@"Keyword hierarchy: %@", [keyword entireAncestry]);

		[pboard setString:[keyword valueForKey:@"name"] forType:@"keywordsPboardType"];
	}
	
	// Set our draggedKeywords instance variable so that it can be accessed by the drag destinations
	[self setDraggedKeywords:keywordsToDrag];
	//NSLog(@"Dragged keywords: %@", [self draggedKeywords]);

	return YES;
}


- (IBAction)add:(id)sender;
{
	[super add:sender];	
	[self performSelector:@selector(editSelectedKeyword) withObject:nil afterDelay:0.3];
}


- (IBAction)addChild:(id)sender;
{
	[super addChild:sender];	
	[self performSelector:@selector(editSelectedKeyword) withObject:nil afterDelay:0.3];
}


- (void)editSelectedKeyword;
{
	[keywordOutlineView editColumn:0 row:[keywordOutlineView selectedRow] withEvent:nil select:YES];
}



// Accessors
- (NSMutableSet *)draggedKeywords {
    return [[draggedKeywords retain] autorelease];
}

- (void)setDraggedKeywords:(NSMutableSet *)value {
    if (draggedKeywords != value) {
        [draggedKeywords release];
        draggedKeywords = [value copy];
    }
}



@end
