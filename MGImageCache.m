//
//  MGImageCache.m
//  Filament
//
//  Created by Dennis Lorson on 24/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGImageCache.h"
#import "MGAppDelegate.h"
#import "Image.h"
#import "MGAppDelegate.h"
#import "Stack.h"
#import "MGLibraryController.h"

#define MAX_CACHE_SIZE 50000

@interface MGImageCache ()


- (NSManagedObjectContext *)managedObjectContext;


- (void)purgeCacheToAccomodateImage:(CIImage *)image;
- (NSInteger)dataSizeOfImage:(CIImage *)image;



- (void)updateThumbnailForImageIfNoneExists:(Image *)image;
- (void)removeThumbnailForPath:(NSString *)path;
- (void)generateThumbnailForPath:(NSString *)path uid:(NSString *)uid;
- (BOOL)imageUsesThumbnail:(Image *)image;

+ (void)createDirectoriesForPath:(NSString *)path;
- (NSString *)thumbnailBasePath;
- (BOOL)thumbnailExistsForPath:(NSString *)path;
- (NSString *)thumbnailPathForImagePath:(NSString *)path;

- (void)startOperation:(MGImageCacheThumbnailOp *)op;


@end



@implementation MGImageCache

static MGImageCache *sharedCache = nil;

+ (MGImageCache *)sharedCache;
{
	if (!sharedCache)
		sharedCache = [[MGImageCache alloc] init];
	
	return sharedCache;
}


- (id) init
{
	self = [super init];
	if (self != nil) {
		
		thumbnailGenerationQueue_ = [[NSOperationQueue alloc] init];
		[thumbnailGenerationQueue_ setMaxConcurrentOperationCount:2];
		
		activeThumbnailOperations_ = [[NSMutableDictionary dictionary] retain];

		[[self class] createDirectoriesForPath:[self thumbnailBasePath]];

		ciImageCache_ = [[NSMutableArray array] retain];
		
	}
	return self;
}


- (NSManagedObjectContext *)managedObjectContext
{
	return [MGLibraryControllerInstance managedObjectContext];
}



#pragma mark -
#pragma mark Full-size cache

- (void)printCacheInfo
{
	NSInteger currentCacheSize = 0;
	for (NSArray *entry in ciImageCache_)
		currentCacheSize += [self dataSizeOfImage:(CIImage *)[entry objectAtIndex:0]];
	
	
	//NSLog(@"Cache entries: %i -- size: %i kb", (int)[ciImageCache_ count], (int)currentCacheSize);
}

- (void)cacheCIImage:(CIImage *)filtered forImage:(Image *)image;
{	
	@synchronized (self) {
		
		if (!filtered)
			return;
		
		NSArray *entry = [NSArray arrayWithObjects:filtered, image, nil];
		
		[self printCacheInfo];
		
		//NSLog(@"Purging cache...");
		
		[self purgeCacheToAccomodateImage:filtered];
		
		[self printCacheInfo];
		
		//NSLog(@"Adding image...");
		
		[ciImageCache_ insertObject:entry atIndex:0];
		
		[self printCacheInfo];
		
		
	}
	
	
}

- (CIImage *)retrieveCIImageForImage:(Image *)image;
{
	@synchronized (self) {
		
		NSArray *foundEntry = nil;
		
		for (NSArray *entry in ciImageCache_)
			if ([entry count] > 1 && [entry objectAtIndex:1] == image)
				foundEntry = entry;
		
		if (!foundEntry)
			return nil;
		
		[foundEntry retain];
		[ciImageCache_ removeObject:foundEntry];
		[ciImageCache_ insertObject:foundEntry atIndex:0];
		[foundEntry release];
		
		
		return [foundEntry objectAtIndex:0];
		
	}
	
	return nil;
}

- (void)removeCachedCIImagesForImage:(Image *)image
{
	@synchronized (self) {
		
		for (NSArray *entry in [[ciImageCache_ copy] autorelease])
			if ([entry objectAtIndex:1] == image)
				[ciImageCache_ removeObject:entry];	
		
	}
	
}

- (void)purgeCacheToAccomodateImage:(CIImage *)image
{
	// to avoid retaining images any longer
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSInteger maxAllowedCacheSize = MAX_CACHE_SIZE - [self dataSizeOfImage:image];
	
	while([ciImageCache_ count] > 0) {
		
		// update the current cache size
		NSInteger currentCacheSize = 0;
		for (NSArray *entry in ciImageCache_)
			currentCacheSize += [self dataSizeOfImage:(CIImage *)[entry objectAtIndex:0]];
		
		if (currentCacheSize < maxAllowedCacheSize)
			break;
		
		// remove the last entry
		[ciImageCache_ removeLastObject];
	}
	
	[pool drain];
	
}

- (NSInteger)dataSizeOfImage:(CIImage *)image
{
	// return an approximation of the size of the image in kbytes
	// this does not count any private cached data...
	
	return (NSInteger)(((CGFloat)([image extent].size.width * [image extent].size.height * 4)) * 0.001);
}


#pragma mark -
#pragma mark Thumbnail retrieval


- (NSImage *)thumbnailForImage:(Image *)image;
{
	NSString *activeImagePath = [image activeImagePathExpanded:NO];
	if (!activeImagePath)
		return nil;
	
	NSString *thumbnailPath = [self thumbnailPathForImagePath:activeImagePath];
	
	NSImage *img = [[[NSImage alloc] initWithContentsOfFile:thumbnailPath] autorelease];
	
	if (!img)
		return nil;
	
	return img;
}

- (CIImage *)filteredThumbnailForImage:(Image *)image;
{
	NSString *activeImagePath = [image activeImagePathExpanded:NO];
	if (!activeImagePath)
		return nil;
	
	NSString *thumbnailPath = [self thumbnailPathForImagePath:activeImagePath];
	
	CIImage *img = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:thumbnailPath]];
	
	if (!img)
		return nil;
	
	return [image filterImage:img];
}


#pragma mark -
#pragma mark Thumbnail generation



- (void)performAppStartThumbnailGeneration;
{
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	[req setEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:[self managedObjectContext]]];
	
	NSError *error = nil;
	NSArray *images = [[self managedObjectContext] executeFetchRequest:req error:&error];
	
	if (error) {
		NSLog(@"Error loading images: %@", [error description]);
		return;
	}
	
	for (Image *image in images)
		[self updateThumbnailForImageIfNoneExists:image];
	
}


- (void)stopThumbnailGeneration;
{
	[thumbnailGenerationQueue_ cancelAllOperations];
}



- (void)updateThumbnailForImageIfNoneExists:(Image *)image
{
	// this method will update the active image path thumbnail of the image, but only if no thumbnail exists yet
	if (![self imageUsesThumbnail:image])
		return;
	
	NSString *path = [image activeImagePathExpanded:NO];
	
	if (path && [path length] && ![self thumbnailExistsForPath:path] && [self imageExistsAtPath:path])
		[self updateThumbnailForImage:image];
}

- (void)updateThumbnailForImage:(Image *)image
{
	// called when an Image has changed (i.e. its original image file has been changed; filters do not count).
	// the thumbnail should be recreated., or created at the possibly new active path.
	// the thumbnails at inactive paths should be removed.
	
	// also called for images that have no thumbnail yet.
	
	//NSLog(@"updating image %@", [image valueForKey:@"name"]);
	
	if (![self imageUsesThumbnail:image])
		return;
	
	NSString *activeImageUID = [image activeImagePathExpanded:NO];  
	NSString *editedImageUID = [image editedImagePathExpanded:NO];
	NSString *normalImageUID = [image originalImagePathExpanded:NO];
    
    NSString *activeImagePath = [image activeImagePathExpanded:YES];  
	NSString *editedImagePath = [image editedImagePathExpanded:YES];
	NSString *normalImagePath = [image originalImagePathExpanded:YES];
	
	if ([activeImageUID isEqual:editedImageUID]) {
		[self generateThumbnailForPath:editedImagePath uid:editedImageUID];
		[self removeThumbnailForPath:normalImageUID];
	} 
	
	else if ([activeImageUID isEqual:normalImageUID]) {
		[self generateThumbnailForPath:normalImagePath uid:normalImageUID];
		[self removeThumbnailForPath:editedImageUID];
	}
	
	else if (activeImageUID) {
		[self generateThumbnailForPath:activeImagePath uid:activeImageUID];
	}
	
}


- (void)removeThumbnailForImage:(Image *)image
{
	// removes thumbnails for all paths of the image
	NSString *editedImagePath = [image editedImagePathExpanded:NO];
	NSString *normalImagePath = [image originalImagePathExpanded:NO];
	
	if (normalImagePath)
		[self removeThumbnailForPath:normalImagePath];
	
	if (editedImagePath)
		[self removeThumbnailForPath:editedImagePath];
	
}


- (void)removeThumbnailForPath:(NSString *)path
{
	if (!path) return;
	
	// removes a thumbnail for the given path.
	// will remove any pending operations for the given path!
	MGImageCacheThumbnailOp *op = [[[MGImageCacheThumbnailOp alloc] init] autorelease];
	op.destinationPath = [self thumbnailPathForImagePath:path];
	op.opType = MGImageCacheOperationTypeRemoval;
	
	[self startOperation:op];
}

- (void)generateThumbnailForPath:(NSString *)path uid:(NSString *)uid
{
	// generates a thumbnail for the given path.
	// will remove any pending operations for the given path!
	MGImageCacheThumbnailOp *op = [[[MGImageCacheThumbnailOp alloc] init] autorelease];
	op.sourcePath = path;
	op.destinationPath = [self thumbnailPathForImagePath:uid];
	op.opType = MGImageCacheOperationTypeGeneration;
	
	[self startOperation:op];
}

- (BOOL)imageUsesThumbnail:(Image *)image
{
	if ([image isKindOfClass:[Stack class]])
		return NO;
	
	return YES;
}


- (void)startOperation:(MGImageCacheThumbnailOp *)op
{
	NSString *path = op.destinationPath;
	
	NSMutableSet *set = [activeThumbnailOperations_ objectForKey:path];
	
	if (!set) {
		set = [NSMutableSet set];
		[activeThumbnailOperations_ setObject:set forKey:path];
	}
	
	// clean all unnecessary previous ops
	for (NSOperation *otherOp in [[set copy] autorelease]) {
		if ([otherOp isFinished])
			[set removeObject:otherOp];
	}
	
	// cancel all other operations,		
	// and make sure this op executes later!
	for (NSOperation *otherOp in set) {
		[otherOp addDependency:op];
		[otherOp cancel];
	}
	
	[set addObject:op];
	[thumbnailGenerationQueue_ addOperation:op];
	
}



#pragma mark -
#pragma mark Thumbnail file utils
							 
- (BOOL)thumbnailExistsForPath:(NSString *)path
{
	NSString *thumbnailPath = [self thumbnailPathForImagePath:path];
	return [[NSFileManager defaultManager] fileExistsAtPath:thumbnailPath isDirectory:NO];
}

- (BOOL)imageExistsAtPath:(NSString *)path
{
	return [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO];
}


- (NSString *)thumbnailBasePath;
{
    return [[MGLibraryControllerInstance library] thumbnailDataPath];

}

- (NSString *)thumbnailPathForImagePath:(NSString *)path;
{	
	if (!path || [path length] == 0)
		return nil;
	
	NSString *abbrev = [path stringByAbbreviatingWithTildeInPath];
	
	
	const char *cstr = NULL;
	int len = 0;
	
	cstr = [abbrev cStringUsingEncoding:NSASCIIStringEncoding];
	len = (int)[abbrev lengthOfBytesUsingEncoding:NSASCIIStringEncoding];

	if (len == 0 || cstr == NULL) {
		cstr = [abbrev cStringUsingEncoding:NSUTF8StringEncoding];
		len = (int)[abbrev lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
	}
	
	if (len == 0 || cstr == NULL)
		return nil;
	
	unsigned char d[20];
		
	SHA1((const unsigned char *)cstr, len, d);
	
	NSMutableString *thumbnailPath = [[[self thumbnailBasePath] mutableCopy] autorelease];
	[thumbnailPath appendFormat:@"/%x/", d[0]];
	
	for (int i = 1; i < 20; i++)
		[thumbnailPath appendFormat:@"%x", d[i]];
		
	return thumbnailPath;
}

+ (void)createDirectoriesForPath:(NSString *)path
{	
	if ([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:nil])
		return;
	
	NSError *error = nil;
	[[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
	
}



@end


#pragma mark -
#pragma mark -


@interface MGImageCacheThumbnailOp ()

- (NSBitmapImageRep *)scaledRepresentationOfImage:(NSImage *)img;

- (void)threaded_removeThumbnailAtPath:(NSString *)path;
- (void)threaded_generateThumbnailForPath:(NSString *)path atPath:(NSString *)destPath;

@end


@implementation MGImageCacheThumbnailOp

@synthesize sourcePath = sourcePath_;
@synthesize destinationPath = destinationPath_;

@synthesize opType = opType_;
 
#define RETURN_IF_CANCELLED		if ([self isCancelled]) {return;}

- (void)main
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	if (self.opType == MGImageCacheOperationTypeRemoval)
		[self threaded_removeThumbnailAtPath:self.destinationPath];
	else
		[self threaded_generateThumbnailForPath:self.sourcePath atPath:self.destinationPath];
	
	[pool drain];
}

- (void)threaded_removeThumbnailAtPath:(NSString *)path
{
	RETURN_IF_CANCELLED;

	//NSLog(@"removing thumbnail for path: %@", path);

	NSError *error = nil;
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:&error];
	if (error)
		NSLog(@"%@", [error description]);
}

- (void)threaded_generateThumbnailForPath:(NSString *)path atPath:(NSString *)destPath
{		
	NSImage *img = [[[NSImage alloc] initWithContentsOfFile:path] autorelease];
	
	RETURN_IF_CANCELLED;
	
	if (!img) return;
	if ([[img representations] count] == 0) return;
	
	//NSLog(@"generating thumbnail for path: %@", path);

	NSBitmapImageRep *resized = [self scaledRepresentationOfImage:img];
	
	RETURN_IF_CANCELLED;

	[MGImageCache createDirectoriesForPath:[destPath stringByDeletingLastPathComponent]];
	
	RETURN_IF_CANCELLED;
	
	NSData *data = [resized representationUsingType:NSJPEGFileType properties:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:0.5] forKey:NSImageCompressionFactor]];
	
	RETURN_IF_CANCELLED;
	
	[data writeToFile:destPath atomically:YES];
}


#pragma mark -
#pragma mark Resizing

- (NSBitmapImageRep *)scaledRepresentationOfImage:(NSImage *)img
{
	NSBitmapImageRep *original = [[img representations] objectAtIndex:0];
	
	CGFloat w = [original pixelsWide];
	CGFloat h = [original pixelsHigh];
	
	CGFloat maxSize = 150.;
	
	NSInteger newW, newH;
	
	if (w > h) {
		newW = maxSize;
		newH = maxSize * h/w;
	} else {
		newH = maxSize;
		newW = maxSize * w/h;
	}
	
	[img setScalesWhenResized:YES];
	[img setSize:NSMakeSize(newW, newH)];
	
	NSBitmapImageRep *destRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		 pixelsWide:newW
																		 pixelsHigh:newH
																	  bitsPerSample:8
																	samplesPerPixel:4
																		   hasAlpha:YES
																		   isPlanar:NO
																	 colorSpaceName:NSDeviceRGBColorSpace
																	   bitmapFormat:0
																		bytesPerRow:0
																	   bitsPerPixel:0] autorelease];
	
	
	NSGraphicsContext *ctx = [NSGraphicsContext graphicsContextWithBitmapImageRep:destRep];
	
	// for this thread only, so we don't need to save/restore the old one
	[NSGraphicsContext setCurrentContext:ctx];
	
	[img drawInRect:NSMakeRect(0, 0, newW, newH) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	
	return destRep;
}


- (void) dealloc
{
	self.sourcePath = nil;
	self.destinationPath = nil;
	
	[super dealloc];
}



@end





