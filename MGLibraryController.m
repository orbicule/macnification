//
//  MGLibraryController.m
//  Filament
//
//  Created by Dennis Lorson on 05/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGLibraryController.h"

#import "CaptureController.h"
#import "SourceListTreeController.h"
#import "ImageArrayController.h"
#import "PreferenceController.h"
#import "ScaleBarController.h"
#import "MiniBrowserController.h"
#import "KeywordController.h"
#import "ExperimentsController.h"
#import "MGImageCache.h"
#import "MGLibrary.h"
#import "FullScreenController.h"
#import "AnalysisPlugin.h"
#import "MGAppDelegate.h"
#import "RBSplitView.h"



@interface MGLibraryController ()

- (void)_addAllStacksToLibrary;
- (void)_setupSplitView;

@end



@implementation MGLibraryController


# pragma mark -
# pragma mark Lifecycle

- (id)initWithLibrary:(MGLibrary *)library;
{
    self = [super init];
    if (self) {
        library_ = [library retain];
    }
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)loadUI
{
    [NSBundle loadNibNamed:@"Library" owner:self];

    [self _addAllStacksToLibrary];
	
	[[self managedObjectContext] setUndoManager:nil];
	
	// Show the main window
	
    
	[[MGImageCache sharedCache] performAppStartThumbnailGeneration];
	
	// register for capture device changes
	[[CaptureDeviceBrowser sharedBrowser] setDelegate:sourceListTreeController];
	[[CaptureDeviceBrowser sharedBrowser] startSearchingForDevices];
}



- (void)prepareForTermination;
{
    [[CaptureDeviceBrowser sharedBrowser] stopAllSessions];

    [[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:[NSNumber numberWithFloat:[sourceListEmbeddedView frame].size.width] forKey:@"mainSplitViewPosition"];

}

# pragma mark -
# pragma mark General accessors

- (MGLibrary *)library;
{
    return library_;
}


- (NSManagedObjectContext *)managedObjectContext;
{
    return [library_ managedObjectContext];
}

- (id)imageArrayController;
{
	return imageArrayController;	
}

- (SourceListTreeController *)sourcelistTreeController
{
	return sourceListTreeController;
}

- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;
{
	[sourceListTreeController searchCurrentFolderWithPredicate:filterPredicate naturalLanguageString:description];
}



#pragma mark -
#pragma mark Setup


- (void)awakeFromNib;
{    
	NSView *browserTabView = [[tabView tabViewItemAtIndex:0] view];
	[browserEmbeddedView setFrame:[browserTabView bounds]];
	[browserTabView addSubview:browserEmbeddedView];
	
	NSView *projectTabView = [[tabView tabViewItemAtIndex:1] view];
	[projectEmbeddedView setFrame:[projectTabView bounds]];
	[projectTabView addSubview:projectEmbeddedView];
	
    
    [self _setupSplitView];
	
	// Set up our plugins
	[self discoverPlugins];
	[self setupPluginMenu];
    
    [window setDelegate:self];
    
}	


- (void)updateImagesModifiedByExternalEditor;
{
    [imageArrayController updateImagesModifiedByExternalEditor];
}


- (void)openFiles:(NSArray *)files
{
	if ([[files lastObject] hasSuffix:@".mgimagemetadata"]) {
        
		// We are opening spotlight files
		NSMutableArray *imagesToOpen = [NSMutableArray arrayWithCapacity:5];
		
		for (NSString *filename in files) {
			NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filename];
			NSURL *uri = [NSURL URLWithString:[dict objectForKey:@"uri"]];
			NSManagedObjectID *id = [[library_ persistentStoreCoordinator] managedObjectIDForURIRepresentation:uri];
			NSManagedObject *theImage = [[self managedObjectContext] objectWithID:id];
			[imagesToOpen addObject:theImage];
		}
		[imageArrayController performSelector:@selector(selectSpotlightedImages:) withObject:imagesToOpen afterDelay:0.80];
        
	}
    
	else {
		// We are opening image files, so try to import them
		[sourceListTreeController performSelector:@selector(importFiles:) withObject:files afterDelay:0.80];
	}
}


#pragma mark -
#pragma mark Other refactoring

- (void)_addAllStacksToLibrary
{
	NSArray *allStacks = [library_ executeRequestWithEntityName:@"Stack" prefetchPaths:nil returnsObjectsAsFaults:NO error:nil limit:0 predicate:nil];
	
	NSMutableSet *libraryImages = [[[MGLibraryControllerInstance library] libraryGroup] mutableSetValueForKey:@"images"];
    
    
    
	
	for (id stack in allStacks)
		[libraryImages addObject:stack];
	
	
	NSMutableSet *allLibraryImages = [[[MGLibraryControllerInstance library] libraryGroup] mutableSetValueForKeyPath:@"images"];
	
	// for each stack, remove its stack images from the library collection
	for (id stack in allStacks) {
		for (id stackImage in [stack valueForKeyPath:@"stackImages"]) {
			[allLibraryImages removeObject:[stackImage valueForKeyPath:@"image"]];
		}
	}
    
}



# pragma mark -
# pragma mark Actions

- (IBAction)showExperimentsWindow:(id)sender;
{
	if (!experimentsController) {
		experimentsController = [[ExperimentsController alloc]init];
		[experimentsController showWindow:self];
	}
	else {
		if ([[experimentsController window] isVisible]) {
			[[experimentsController window] orderOut:self];
		}
		else {
			[[experimentsController window] makeKeyAndOrderFront:self];		
		}
	}	
}


- (IBAction)toggleKeywordPanel:(id)sender;
{
	if (!keywordController) {
		keywordController = [[KeywordController alloc]initWithImageArrayController:imageArrayController];
		[[keywordController window] setAlphaValue:0.0];
		[[keywordController window] makeKeyAndOrderFront:self];		
		[[[keywordController window] animator] setAlphaValue:1.0];
	}
	else {
		if ([[keywordController window] isVisible]) {
			[[keywordController window] orderOut:self];
		}
		else {
			[[keywordController window] setAlphaValue:0.0];
			[[keywordController window] makeKeyAndOrderFront:self];		
			[[[keywordController window] animator] setAlphaValue:1.0];
		}
	}
}



- (IBAction)toggleROIPanel:(id)sender;
{
	if ([roiPanel isVisible]) {
		[roiPanel orderOut:self];
	}
	else {
		[roiPanel setAlphaValue:0.0];
		[roiPanel makeKeyAndOrderFront:self];
		[[roiPanel animator] setAlphaValue:1.0];
	}
}




- (IBAction)showScaleBarPanel:(id)sender;
{
	if (!scaleBarController)
		scaleBarController = [[ScaleBarController alloc] initWithImageArrayController:imageArrayController];
	
	[scaleBarController window];
	[scaleBarController setup];
}







- (IBAction)showMiniBrowser:(id)sender;
{
	if (!miniBrowserController) {
		miniBrowserController = [[MiniBrowserController alloc] init];
	}
	[miniBrowserController showWindow:self];
	[[window animator] setAlphaValue:0.0];
	[window performSelector:@selector(close) withObject:nil afterDelay:1];
}


- (void)showMainWindow;
{
	[window makeKeyAndOrderFront:self];
	[[window animator] setAlphaValue:1.0];
}


- (IBAction)showRegistrationPanel:(id)sender;
{
    [MGAppDelegateInstance showRegistrationPanel:sender];
}



# pragma mark -
# pragma mark SplitViews

- (void)_setupSplitView
{
    splitView = [[RBSplitView alloc] initWithFrame:NSInsetRect([[window contentView] bounds], 6, 6)];
    [splitView setDivider:[NSImage imageNamed:@"splitview_divider"]];
    [splitView setDividerThickness:6];
    [splitView setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
    [[window contentView] addSubview:splitView];
    
    [splitView setDelegate:self];
    
    // Add
    [splitView addSubview:sourceListEmbeddedView];
    [splitView addSubview:tabView];
    [splitView addSubview:mdContainerView];
    
    // Collapse
    [[splitView subviewAtPosition:0] setCanCollapse:NO];
    [[splitView subviewAtPosition:1] setCanCollapse:NO];
    [[splitView subviewAtPosition:2] setCanCollapse:YES];
    
    // Dimensions
    [[splitView subviewAtPosition:0] setMinDimension:130 andMaxDimension:280];
    [[splitView subviewAtPosition:2] setMinDimension:250 andMaxDimension:350];

    
    [splitView setAutosaveName:@"mainWindowSplitView" recursively:YES];
    
    if ([splitView restoreState:YES])
        [splitView adjustSubviews];
}


- (void)splitView:(RBSplitView *)sender wasResizedFrom:(float)oldDimension to:(float)newDimension 
{
    [sender adjustSubviewsExcepting:[splitView subviewAtPosition:2]];
}


// Shows/Hides the MDView 
- (IBAction)toggleMdSplitView:(id)sender;
{
    CGFloat dim = [[splitView subviewAtPosition:0] dimension];
    
    if ([[splitView subviewAtPosition:2] isCollapsed])
        [[splitView subviewAtPosition:2] expand];
    else
        [[splitView subviewAtPosition:2] collapse];
    
    [[splitView subviewAtPosition:0] setDimension:dim];

}

- (NSSize)windowWillResize:(NSWindow *)sender toSize:(NSSize)frameSize
{
    CGFloat dim = [[splitView subviewAtPosition:0] dimension];
    
    [[splitView subviewAtPosition:0] setMinDimension:dim andMaxDimension:dim];
    
    return frameSize;
}


// Make sure the mdView retains its size when resizing the window
- (void)windowDidResize:(NSNotification *)aNotification;
{
	[[[self sourcelistTreeController] captureController] updateAdjustmentsHUDPosition];

    [[splitView subviewAtPosition:0] setMinDimension:130 andMaxDimension:280];

}


- (void)windowDidChangeScreen:(NSNotification *)notification
{
	[fullScreenController updateScreens];
	
}



# pragma mark -
# pragma mark Progress Window

// Start showing the progress sheet
- (void)showProgressSheet;
{
	[progressBar setUsesThreadedAnimation:YES];
	[progressBar setIndeterminate:NO];
	[progressBar setDoubleValue:0.00];
	[progressBar display];
	[progressTextField setStringValue:@""];
	[progressTextField display];
	[NSApp beginSheet:progressSheet modalForWindow:window modalDelegate:self didEndSelector:NULL contextInfo:nil];
	
	
}


// Update the progress and progress message
- (void)setProgress:(double)progress;
{
	if (progress < 0) {
		[progressBar setIndeterminate:YES];
		[progressBar startAnimation:self];
		[progressBar display];
        
	} else {
		if ([progressBar isIndeterminate]) {
			[progressBar setIndeterminate:NO];
			[progressBar stopAnimation:self];
			[progressBar setDoubleValue:progress];
			[progressBar startAnimation:self];
		} else {
			[progressBar setDoubleValue:progress];
			[progressBar display];
		}
        
		
	}
    
}


// Starts barber pole to indicate indeterminate progress
- (void)startProgressAnimation;
{
	[progressBar setIndeterminate:YES];
	[progressBar startAnimation:self];
}


- (void)setProgressMessage:(NSString *)message;
{
	[progressTextField setStringValue:message];
	[progressTextField display];	
}

- (void)setProgressTitle:(NSString *)title
{
	[progressTitleTextField setStringValue:title];
	[progressTitleTextField display];	
    
}

- (void)setCancelImportButtonHidden:(BOOL)flag
{
	[cancelImportButton setHidden:flag];
	NSRect frame = [progressSheet frame];
	frame.size.height = flag ? 120 : 170;
	[progressSheet setFrame:frame display:YES];
	
}


// Hide the progress sheet
- (void)hideProgressSheet;
{
	[progressBar setDoubleValue:100.0];
	[progressSheet orderOut:nil];
	[NSApp endSheet:progressSheet];   
}




- (NSWindow *)window;
{
	return window;
}




# pragma mark -
# pragma mark Spotlight


// This method is called in applicationShouldTerminate (see below)
- (void)exportSpotlightMetadata;
{
	// Make sure the metadata folder is present
	NSString *spotlightPath = [@"~/Library/Caches/Metadata/Macnification" stringByExpandingTildeInPath];
	NSFileManager *fm = [NSFileManager defaultManager];
	if (![fm fileExistsAtPath:spotlightPath]) {
		[fm createDirectoryAtPath:spotlightPath withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	// Get all images
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Image" inManagedObjectContext:[self managedObjectContext]];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	[fetch setPredicate:predicate];
	[fetch setEntity:entity];
	NSArray *allImages = [[self managedObjectContext] executeFetchRequest:fetch error:nil];
	[fetch release];
	
	// Iterate all images and write their spotlightMetadata to file
	for (Image *image in allImages) {
		if ([image hasBeenChanged]) {
			//NSLog(@"Spotlight index for image: %@", image);
			NSString *fileName = [NSString stringWithFormat:@"%@.mgimagemetadata", [[[[image objectID] URIRepresentation] relativePath] lastPathComponent]];
			[[image spotlightMetadata] writeToURL:[NSURL fileURLWithPath:[spotlightPath stringByAppendingPathComponent:fileName]] atomically:YES];
		}
	}
}



# pragma mark -
# pragma mark Plugins 

// Checks for available plugins in all default plugin locations
- (void)discoverPlugins;
{
    Class analysisPluginClass;
    NSString *appSupport = @"Library/Application Support/Macnification/PlugIns/";
    NSString *appPath = [[NSBundle mainBundle] builtInPlugInsPath];
    NSString *userPath = [NSHomeDirectory() stringByAppendingPathComponent:appSupport];
    NSString *sysPath = [@"/" stringByAppendingPathComponent:appSupport];
    NSArray *paths = [NSArray arrayWithObjects:appPath, userPath, sysPath, nil];
    
    analysisPlugins = [[NSMutableDictionary alloc] init];
    
    for (NSString *path in paths) {
        for (NSString *name in [NSBundle pathsForResourcesOfType:@"plugin" inDirectory:path]) {
            if ([[name pathExtension] isEqualToString:@"plugin"]) {
                NSBundle *plugin = [NSBundle bundleWithPath:name];
                if ((analysisPluginClass = [plugin principalClass])) {
                    NSString *menuTitle = [[plugin infoDictionary] objectForKey:@"PluginName"];
                    [analysisPlugins setObject:[analysisPluginClass analysisPlugin] forKey:menuTitle];
                }
            }
        }
    }
}



// Creates the plugin menu
- (void)setupPluginMenu;
{ 
	if ([[analysisPlugins allKeys] count] == 0)
        return;
    
    // Create an NSMenu that will hold all plugins menuitems
    NSMenu *pluginsMenu = [[[NSMenu alloc] init] autorelease];
    [pluginsMenu setTitle:@"Plugins"];
    
    // Create the Plugins NSMenuItem for the Main menu
    NSMenuItem *pluginsMenuItem = [[NSMenuItem alloc] initWithTitle:@"pluginsMenuItem" action:nil keyEquivalent:@""];
    [[NSApp mainMenu] insertItem:pluginsMenuItem atIndex:4];
    [pluginsMenuItem setSubmenu:pluginsMenu];
    [pluginsMenuItem release];
    
    for (NSString *name in analysisPlugins) { 
        // Add a menu item for every plugin
        NSMenuItem *myMenuItem = [[NSMenuItem alloc] initWithTitle:name action:@selector(launchPlugin:) keyEquivalent:@""];
        [[[[NSApp mainMenu] itemWithTitle:@"pluginsMenuItem"] submenu] insertItem:myMenuItem atIndex:0];
        [myMenuItem setEnabled:YES];
        [myMenuItem release];
    } 
} 



- (IBAction)launchPlugin:(id)sender;
{
	id plugin = [analysisPlugins objectForKey:[sender title]];
	NSWindow *pluginSheet = [plugin analysisWindowForImages:[imageArrayController selectedObjects]];
	[NSApp beginSheet:pluginSheet modalForWindow:window modalDelegate:self didEndSelector:NULL contextInfo:nil];
}


- (void)endPluginSheet:(NSWindow *)sheet
{
	[sheet orderOut:nil];
	[NSApp endSheet:sheet];	
}





# pragma mark -
# pragma mark Menu validation

- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
    SEL theAction = [anItem action];
	
    if (theAction == @selector(showScaleBarPanel:))    {
        if ([[imageArrayController selectedObjects] count] > 0) {
            return YES;
		} 
		else {
			return NO;	
		}
	}
	return YES;
}


# pragma mark -
# pragma mark Save/undo

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window 
{
	return [[self managedObjectContext] undoManager];
}


// Called by PSUndoManager
- (void)refreshAndReloadImageBrowser;
{
	[imageArrayController refreshAndReloadImageBrowser];	
}




- (IBAction)saveAction:(id)sender 
{
	NSError *error = nil;
	if (![[self managedObjectContext] save:&error]) {
		[[NSApplication sharedApplication] presentError:error];
	}
}



- (void)save;
{	
	[[self managedObjectContext] processPendingChanges];
	
	NSError *error = nil;
	if (![[self managedObjectContext] save:&error])
		[[NSApplication sharedApplication] presentError:error];
    
}



@end
