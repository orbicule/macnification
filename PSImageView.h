//
//  PSImageView.h
//  Macnification
//
//  Created by Peter Schols on 16/11/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageKit/ImageKit.h>
#import "Image.h"
#import "LineROI.h"
#import "MAAttachedWindow.h"
#import "ROIArrayController.h"
#import "MGImageView.h"
#import "NSWindow_Exploding.h"


@class MGNavigatorView;
@class WorkflowInfoPanel;
@class FullScreenImageArrayController;

@interface PSImageView : MGImageView {
	
	BOOL isUsingDCCalibration;
	BOOL isDrawingPolygon;
	BOOL handleClicked;
	BOOL roiHasBeenClicked;
	BOOL scaleBarClicked;
	BOOL isInColorPickerMode;
	BOOL shouldObserveSelectedImage;
	BOOL isHidingROIs;
	
	int cursorMode;
	int handle;
	CGFloat scale;
	NSPoint downPoint;
	NSPoint currentPoint;
	NSPoint upPoint;
	NSPoint movedPoint;
	NSPoint oppositePoint;
	NSPoint lastDraggedPoint; // TEST
	NSRect dirtyOldRect;
	
	NSPoint calibrationPoint1;
	NSPoint calibrationPoint2;
	float calibrationLength;
	
	NSMutableArray *polygonPoints;
	
	Image *image;
		
	IBOutlet id theWindow;
	IBOutlet NSPanel * calibrationPanel;
	IBOutlet id calibrationScaleBarLengthField;
	IBOutlet id calibrationUnitPopup;
	IBOutlet id calibrationNameField;
	
	IBOutlet FullScreenImageArrayController *fullScreenImageArrayController;
	IBOutlet ROIArrayController *roiArrayController;
	
	MAAttachedWindow *attachedWindow;
	IBOutlet id attachedWindowView;
	
	IBOutlet NSPanel *navigatorWindow;
	IBOutlet MGNavigatorView *navigatorView;
	
	IBOutlet WorkflowInfoPanel *infoPanel;
	
	// Tool buttons
	IBOutlet NSButton *arrowButton;
	IBOutlet NSButton *lineButton;
	IBOutlet NSButton *curvedLineButton;
	IBOutlet NSButton *rectangleButton;
	IBOutlet NSButton *circleButton;
	IBOutlet NSButton *polygonButton;

	
	NSArray *temporaryROIs;
}


@property (nonatomic, retain) NSArray *temporaryROIs;

// IBActions
- (IBAction)setCursorMode:(id)sender;
- (void)deselectAllToolButtonsExceptNumber:(NSUInteger)selectedButtonTag;

//- (IBAction)startChat:(id)sender;
- (IBAction)calibrate:(id)sender;
- (IBAction)showCalibrationPanel:(id)sender;
- (IBAction)toggleROIs:(id)sender;


// ROIs
- (void)updateOverlayForChangesInROIs:(NSArray *)rois;

- (void)drawROIsInBounds:(NSRect)bounds withTransform:(NSAffineTransform *)trafo inView:(NSView *)view;
- (BOOL)ROIIsSelected:(ROI *)roi;
- (ROI *)addROIWithClickCount:(int)clickCount;
- (void)showROICommentHUDAtPoint:(NSPoint)point;
- (IBAction)closeROICommentHUD:(id)sender;
- (NSBezierPath *)roiWithPoints:(NSArray *)roiPoints;
- (NSPoint)constrainedPointForNormalizedPoint:(NSPoint)nPoint oppositePoint:(NSPoint)oppositePoint;


// zooming
- (IBAction)zoomIn:(id)sender;
- (IBAction)zoomOut:(id)sender;
- (IBAction)zoomImageToFit:(id)sender;

// White balance correction (color picker mode)
- (void)readColorForColorPickerAtPoint:(NSPoint)point;


// Calibration
- (void)oneClickCalibrationWithPoint:(NSPoint)point;
- (void)drawCalibrationLine;
- (void)showCalibrationPanelAtImagePoint:(NSPoint)point;


// Scaling
- (NSPoint)scaledPoint:(NSPoint)point;
- (NSPoint)unScaledPoint:(NSPoint)point;

// Filters
- (void)filtersDidChange;


// Info Panel
- (void)showInfoPanelWithMessage:(NSString *)message;
- (void)closeInfoPanel;


// Navigator
- (CGRect)viewBounds;
- (BOOL)rect:(CGRect)rect1 containsRectInSize:(CGRect)rect2;
- (IBAction)closeNavigatorWindow:(id)sender;
- (void)showNavigatorIfNeeded;
- (void)scrollToNormalizedRect:(NSRect)normalizedRect;
- (void)scrollByX:(CGFloat)x y:(CGFloat)y;

- (void)setNavigatorImage:(CIImage *)newImage;
- (void)updateNavigatorVisibleRect;
- (NSWindow *)navigatorWindow;


// Accessors
- (CGFloat)scale;
- (void)setScale:(CGFloat)value;

@property(readwrite) BOOL shouldObserveSelectedImage;


enum
{
	pointerCursorMode		= 0,
	lineCursorMode			= 1,
	rectangleCursorMode		= 2,
	circleCursorMode		= 3,
	polygonCursorMode		= 4,
	handCursorMode			= 5,
	curvedLineCursorMode	= 6
};




@end
