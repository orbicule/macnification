//
//  MGOpaqueHUDBox.m
//  Filament
//
//  Created by Dennis Lorson on 18/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGOpaqueHUDBox.h"


@implementation MGOpaqueHUDBox

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect
{
	NSRect bounds = [self bounds];
	
	NSRect firstRect = bounds;
	firstRect.size.height = 1;
	
	NSRect secondRect = firstRect;
	secondRect.origin.y += 1;
	
	
	[[NSColor colorWithCalibratedWhite:0.2 alpha:1.0] set];	
	[[NSBezierPath bezierPathWithRect:firstRect] fill];
	[[NSColor colorWithCalibratedWhite:0.0 alpha:1.0] set];
	[[NSBezierPath bezierPathWithRect:secondRect] fill];
	
}

@end
