//
//  MGContextualToolStrip.m
//  toolbarTest
//
//  Created by Dennis Lorson on 20/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGContextualToolStrip.h"

#import "MGContextualToolGenericView.h"
#import "NSButton_MGAdditions.h"
#import "NSPopUpButton_MGAdditions.h"


static CGFloat MGTopItemMargin = 5.0;
static CGFloat MGBottomItemMargin = 5.0;
static CGFloat MGInterItemMargin = 10.0;

static CGFloat MGMinimumEllipseButtonSize = 20.0;

@implementation MGContextualToolStrip


#pragma mark init - dealloc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// init - dealloc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
	
    if (self) {
		
		leftViews = [[NSMutableArray array] retain];
		centeredViews = [[NSMutableArray array] retain];
		rightViews = [[NSMutableArray array] retain];
		ellipsedViews = [[NSMutableArray array] retain];
		
		// the ellipsis button (thanks Alexander for the icon ;)
		ellipseButton = [[NSButton toolStripButtonWithImage:[NSImage imageNamed:@"overflow"] title:nil] retain];
		[ellipseButton setAlternateImage:[NSImage imageNamed:@"overflow_pressed"]];
		[ellipseButton setTarget:self];
		[ellipseButton setAction:@selector(showEllipseMenu:)];
	
    }
	
	
    return self;
}


- (void)dealloc
{
	[leftViews release];
	[centeredViews release];
	[rightViews release];
	[ellipsedViews release];
	
	[ellipseButton release];
	
	[super dealloc];
	
}



#pragma mark -
#pragma mark Add / remove views
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Add / remove views
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)addToolView:(NSView *)theView withAlignment:(MGViewAlignment)alignment
{
	// the view supplied here will only retain its width.
	
	[[self viewsWithAlignment:alignment] addObject:theView];
	
	// update the frame of this new view.
	[self updateViewTiling];
	
	[theView setAutoresizingMask:NSViewMaxYMargin];
	
	[self addSubview:theView];
	
}

- (void)addSeparatorWithAlignment:(MGViewAlignment)alignment
{
	MGContextualToolGenericView *sep = [[[MGContextualToolGenericView alloc] initWithFrame:NSMakeRect(0,0,3,5)] autorelease];
	sep.toolType = MGSeparatorToolType;
	[self addToolView:sep withAlignment:alignment];
}

- (void)addWhitespaceWithAlignment:(MGViewAlignment)alignment
{
	MGContextualToolGenericView *sep = [[[MGContextualToolGenericView alloc] initWithFrame:NSMakeRect(0,0,20,5)] autorelease];
	sep.toolType = MGWhitespaceToolType;
	[self addToolView:sep withAlignment:alignment];
}

- (void)removeItems
{
	// TODO: implement?
	for (NSView *view in [[leftViews arrayByAddingObjectsFromArray:centeredViews] arrayByAddingObjectsFromArray:rightViews])
		[view removeFromSuperview];
	
	[leftViews removeAllObjects];
	[centeredViews removeAllObjects];
	[rightViews removeAllObjects];
	
	[self updateViewTiling];
}



#pragma mark -
#pragma mark Layout (tiling)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Layout (tiling)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (CGFloat)minEdgeForViewWithAlignment:(MGViewAlignment)alignment atIndex:(NSInteger)viewIndex
{
	// returns the edge appropriate for a view at the given index in the view array (meaning toolbar item with position viewIndex)
	// for a right aligned view, this means the maxX edge.  For other alignments, minX edge.
	
	// recursive
	
	NSMutableArray *views = [self viewsWithAlignment:alignment];
	
	if (viewIndex == 0) {
		
		// return the minimum edge position (MinX/MaxX) for the first view in the array of view with the given alignment
		
		switch (alignment) {
				
			case MGRightViewAlignment:
				// maxX of rightmost view
				// do not allow the right aligned views to overlap with the centered ones.
				// NOTE: cannot use [self centerBoxMinEdge] here, would cause an infinite loop!
				return fmax(NSMaxX([self bounds]) - MGInterItemMargin, [self rightViewBoxAbsoluteMinEdge]) ;
				
			case MGLeftViewAlignment:
				// minX of leftmost view
				return NSMinX([self bounds]) + MGInterItemMargin;
				
			case MGCenterViewAlignment:
				// minX of the first centered view
				return [self centerViewBoxMinEdge];
				
		}
	}
	
	CGFloat previousCoord = [self minEdgeForViewWithAlignment:alignment atIndex:viewIndex - 1];
	
	if (alignment == MGRightViewAlignment) {
		
		return previousCoord - [(NSView *)[views objectAtIndex:viewIndex-1] frame].size.width - MGInterItemMargin;
		
	} else {
		
		return previousCoord + [(NSView *)[views objectAtIndex:viewIndex-1] frame].size.width + MGInterItemMargin;
		
	}
	
}

- (CGFloat)rightViewBoxAbsoluteMinEdge
{
	// absolute minimum for the MaxX edge of the right aligned box (used when the view has a width such that it can just contain all subviews)
	
	
	// the rightmost edge of the left aligned views
	CGFloat leftBoxMaxX;
	
	if ([leftViews count] > 0) {
		
		leftBoxMaxX = [self minEdgeForViewWithAlignment:MGLeftViewAlignment atIndex:[leftViews count] - 1] + [(NSView *)[leftViews lastObject] frame].size.width + MGInterItemMargin;
		
	} else {
		
		leftBoxMaxX = NSMinX([self bounds]) + MGInterItemMargin;
		
	}
	
	
	// the size of the center box
	CGFloat centerBoxSize = 0;
	
	for (NSView *centerView in centeredViews) 
		centerBoxSize += [centerView frame].size.width;
	
	centerBoxSize += ([centeredViews count] - 1) * MGInterItemMargin;
	
	
	// the size of the right box
	CGFloat rightBoxSize = 0;
	
	for (NSView *rightView in rightViews) 
		rightBoxSize += [rightView frame].size.width;
	
	rightBoxSize += ([rightViews count] - 1) * MGInterItemMargin;
	
	
	
	return centerBoxSize + rightBoxSize + leftBoxMaxX + MGInterItemMargin;
		
}

- (CGFloat)centerViewBoxMinEdge
{
	// return the min x edge of the box that bounds the centered views.
	// this is dependent on the positions of the left and right centered views.
	
	// get the maxX of the rightmost image of the left aligned images
	CGFloat leftBoxMaxX;
	
	if ([leftViews count] > 0) {
		
		leftBoxMaxX = [self minEdgeForViewWithAlignment:MGLeftViewAlignment atIndex:[leftViews count] - 1] + [(NSView *)[leftViews lastObject] frame].size.width + MGInterItemMargin;
		
	} else {
		
		leftBoxMaxX = NSMinX([self bounds]) + MGInterItemMargin;
		
	}
	
	
	// get the minX of the leftmost image of the right aligned images
	CGFloat rightBoxMinX;
	
	if ([rightViews count] > 0) {
		
		rightBoxMinX = [self minEdgeForViewWithAlignment:MGRightViewAlignment atIndex:[rightViews count] - 1] - [(NSView *)[rightViews lastObject] frame].size.width - MGInterItemMargin;
		
	} else {
		
		rightBoxMinX = NSMaxX([self bounds]) - MGInterItemMargin;
		
	}
		
	// now compute the size of the center box
	CGFloat centerBoxSize = 0;
	
	for (NSView *centerView in centeredViews) 
		centerBoxSize += [centerView frame].size.width;
	
	centerBoxSize += ([centeredViews count] - 1) * MGInterItemMargin;
	
	// centered position of the box gives us:
	CGFloat minEdge = leftBoxMaxX + (rightBoxMinX - leftBoxMaxX)/2 - centerBoxSize/2;
	
	// do not allow the centered box to overlap with the left aligned views
	
	minEdge = fmax(minEdge, leftBoxMaxX);
	
	
	return (float)floor(minEdge);
	
}

- (void)layoutView:(NSView *)theView withAlignment:(MGViewAlignment)alignment
{
	
	// set theViews height to our own height minus the margin.
	NSRect frame = [theView frame];
	frame.size.height = [self bounds].size.height - MGTopItemMargin - MGBottomItemMargin;
	frame.origin.y = MGBottomItemMargin;
	frame.origin.x = [self minEdgeForViewWithAlignment:alignment atIndex:[[self viewsWithAlignment:alignment] indexOfObject:theView]];
	
	frame.origin.x -= (alignment == MGRightViewAlignment) ? frame.size.width : 0;		// subtract the width if right aligned, because we need the MinX and not MaxX edge...
	
	[theView setFrame:frame];

}

- (void)updateViewTiling
{
	for (NSView *subview in leftViews) {
		
		[self layoutView:subview withAlignment:MGLeftViewAlignment];
		
	}
	
	for (NSView *subview in centeredViews) {
		
		[self layoutView:subview withAlignment:MGCenterViewAlignment];
		
	}
	
	for (NSView *subview in rightViews) {
		
		[self layoutView:subview withAlignment:MGRightViewAlignment];
		
	}
	
	// all views are now tiled, determine if there should be an ellipse in the right aligned views, then the centered, and so on. 
	[self ellipseViewsIfNeeded];

}

- (void)ellipseViewsIfNeeded
{
	// - check if the subviews exceed the allowed space
	// - if so, determine the last visible one
	// - hide all subsequent views
	// - show the ellipsis button on the edge of the strip bounds
	
	CGFloat maxAllowableSpace = NSMaxX([self bounds]);
		
	// add all the views of this strip into one array (in reading order, meaning L to R for all alignments!) to make this operation easier.
	NSMutableArray *allViews = [NSMutableArray array];
	for (NSView *view in leftViews)
		[allViews addObject:view];
	for (NSView *view in centeredViews)
		[allViews addObject:view];
	for (NSView *view in [rightViews reverseObjectEnumerator])
		[allViews addObject:view];
	
	
	BOOL ellipseFound = NO;
	NSInteger i = 0;
	
	// compute the index (meaning, the position in the respective view array) and alignment of the first ellipsed view
	while (i != [allViews count] && !ellipseFound) {
		
		// we've already set the frames of all views correctly by now, so we can just rely on these values to be correct.
		CGFloat spaceReached = NSMaxX([(NSView *)[allViews objectAtIndex:i] frame]);
		
		if (spaceReached > maxAllowableSpace) {
			
			// this will be the first ellipsed view.  Be sure to inverse the index if right aligned because the order was reversed earlier on.
			firstEllipseAlignment = [self alignmentOfView:[allViews objectAtIndex:i]];
			firstEllipseIndex = i;
			
			// we will need to display the ellipse button, so there has to be room for this button.
			// make the max space available even smaller, and if the current view is too large, ellipse it too.
			spaceReached = NSMaxX([(NSView *)[allViews objectAtIndex:i-1] frame]);
			
			if (spaceReached > maxAllowableSpace - MGMinimumEllipseButtonSize - 2 * MGInterItemMargin) {
				
				firstEllipseAlignment = [self alignmentOfView:[allViews objectAtIndex:i-1]];
				firstEllipseIndex = (firstEllipseAlignment == MGRightViewAlignment) ? i-1 : i-1;
				
			}
			
			ellipseFound = YES;
			
		}
		
		// unhide this view, as it is certainly not taking too much space.
		[[allViews objectAtIndex:i] setHidden:NO];
		
		i++;
		
	}
	
	// remove the ellipse if it is still visible from last time
	[ellipseButton removeFromSuperview];
	
	if (ellipseFound) {
		
		// hide all views exceeding the space
		
		NSInteger currentIndex = firstEllipseIndex;
		while (currentIndex < [allViews count]) {
			
			[[allViews objectAtIndex:currentIndex] setHidden:YES];
			
			currentIndex++;
			
		}
		
		// set the ellipse button frame to the frame of the first ellipsed image.
		NSRect ellipseFrame = [(NSView *)[allViews objectAtIndex:firstEllipseIndex] frame];
		ellipseFrame.size.width = MGMinimumEllipseButtonSize;
		ellipseFrame.origin.x = NSMaxX([self bounds]) - MGMinimumEllipseButtonSize - MGInterItemMargin;
		
		[ellipseButton setFrame:ellipseFrame];
		[self addSubview:ellipseButton];
		
		
	}
	
	
}


- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize
{
	[self updateViewTiling];
}



#pragma mark -
#pragma mark Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)drawRect:(NSRect)rect 
{
	// background color
	
	//NSGradient *grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.6 alpha:1.0] 
	//												  endingColor:[NSColor colorWithCalibratedWhite:0.75 alpha:1.0]] autorelease];
	
	NSGradient *grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:/*0.76*/178.0/255 alpha:1.0] 
													  endingColor:[NSColor colorWithCalibratedWhite:/*0.64*/138.0/255 alpha:1.0]] autorelease];

	NSGradient *gloss = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:/*0.76*/255.0/255 alpha:0.2] 
													  endingColor:[NSColor colorWithCalibratedWhite:/*0.64*/255.0/255 alpha:0.0]] autorelease];
	
	NSRect glossRect = [self bounds];
	glossRect.origin.y = NSMaxY([self bounds]) - NSHeight([self bounds])/2;
	glossRect.size.height = NSHeight([self bounds])/2;
	
	//NSBezierPath *path = [NSBezierPath bezierPathWithRect:NSInsetRect([self bounds], -1, 1)];
	[grad drawInRect:[self bounds] angle:90.0];
	[gloss drawInRect:glossRect angle:90.0];

	
	NSBezierPath *div = [NSBezierPath bezierPath];
	[div moveToPoint:NSMakePoint(NSMinX([self bounds]), NSMaxY([self bounds]) - 1)];
	[div lineToPoint:NSMakePoint(NSMaxX([self bounds]), NSMaxY([self bounds]) - 1)];

	NSBezierPath *div2 = [NSBezierPath bezierPath];
	[div2 moveToPoint:NSMakePoint(NSMinX([self bounds]), NSMaxY([self bounds]) -2)];
	[div2 lineToPoint:NSMakePoint(NSMaxX([self bounds]), NSMaxY([self bounds]) -2)];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	[[NSColor colorWithCalibratedWhite:104.0/255.0 alpha:1.0] set];
	[div stroke];
	[[NSColor colorWithCalibratedWhite:160.0/255.0 alpha:1.0] set];
	[div2 stroke];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
	
	//NSImage *bg = [NSImage imageNamed:@"toolstrip_background"];
	//[bg drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];

}


#pragma mark -
#pragma mark Other
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Other
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSMutableArray *)viewsWithAlignment:(MGViewAlignment)alignment
{
	
	switch (alignment) {
			
		case MGLeftViewAlignment:
			return leftViews;
			
		case MGCenterViewAlignment:
			return centeredViews;
			
		case MGRightViewAlignment:
			return rightViews;
			
	}
	
	return nil;
}

- (MGViewAlignment)alignmentOfView:(NSView *)view
{
	if ([leftViews containsObject:view])
		return MGLeftViewAlignment;
	
	if ([centeredViews containsObject:view])
		return MGCenterViewAlignment;
	
	if ([rightViews containsObject:view])
		return MGRightViewAlignment;
	
	//NSLog(@"This view does not belong to the toolstrip.");
	return MGLeftViewAlignment;
}

- (void)showEllipseMenu:(id)sender
{
	NSEvent *clickEvent = [NSApp currentEvent];
	
	
	NSPoint location = NSMakePoint(NSMaxX([self convertRect:[(NSView *)sender frame] toView:nil]), [clickEvent locationInWindow].y);
	
	NSEvent *adjEvent = [NSEvent mouseEventWithType:[clickEvent type]
										   location:location
									  modifierFlags:[clickEvent modifierFlags]
										  timestamp:[clickEvent timestamp]
									   windowNumber:[clickEvent windowNumber]
											context:[clickEvent context]
										eventNumber:[clickEvent eventNumber]
										 clickCount:[clickEvent clickCount]
										   pressure:[clickEvent pressure]];
						 
	 [NSMenu popUpContextMenu:[self menuWithEllipsedOptions] withEvent:adjEvent forView:self]; 
	
}

- (NSMenu *)menuWithEllipsedOptions
{
	// returns a menu with the items that have been ellipsed, when there is not enough space.
	NSMenu *menu = [[[NSMenu alloc] initWithTitle:@""] autorelease];
	[menu setAutoenablesItems:NO];

	// add all the views of this strip into one array (in reading order, meaning L to R for all alignments!) to make this operation easier.
	NSMutableArray *allViews = [NSMutableArray array];
	for (NSView *view in leftViews)
		[allViews addObject:view];
	for (NSView *view in centeredViews)
		[allViews addObject:view];
	for (NSView *view in [rightViews reverseObjectEnumerator])
		[allViews addObject:view];
	
	NSInteger nbItemsAlreadyInMenu = 0;
	
	MGViewAlignment currentAlignment = MGLeftViewAlignment;
	
	// now add all these options to the menu.
	for (NSView *view in allViews) {
		
		// keep track of the arrays we're traversing.  if we cross to another aligned array, insert a separator
		
		if (currentAlignment != [self alignmentOfView:view] && nbItemsAlreadyInMenu > 0) {
			
			[menu addItem:[NSMenuItem separatorItem]];
			
		}
		
		currentAlignment = [self alignmentOfView:view];
		
		if ([view isKindOfClass:[NSButton class]] && [view isHidden]) {
			NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:[(NSButton *)view title]  action:[(NSButton *)view action] keyEquivalent:@""] autorelease];
			
			[item setTarget:[(NSButton *)view target]];
			[item setEnabled:YES];
			
			[menu addItem:item];
			
			nbItemsAlreadyInMenu ++;
			
			
		}
		
		// the segmented controls
		if ([view isMemberOfClass:[NSView class]] && [view isHidden]) {
			
			// Add items for the sort order selectors?
			NSControl *embeddedControl = nil;
			
			for (NSView *subview in [view subviews])
				if ([subview isMemberOfClass:[NSSegmentedControl class]])
					embeddedControl = (NSControl *)subview;
			
			if ([embeddedControl action] == @selector(switchStackSortOrder:)) {
				
				NSMenu *submenu = [[[NSMenu alloc] initWithTitle:@""] autorelease];
				NSMenuItem *subMenuItem1 = [[[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Ascending", nil) 
																	   action:@selector(switchStackSortOrder:) keyEquivalent:@""] autorelease];
				[subMenuItem1 setTag:0];
				[subMenuItem1 setTarget:[embeddedControl target]];
				NSMenuItem *subMenuItem2 = [[[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Descending", nil) 
																	   action:@selector(switchStackSortOrder:) keyEquivalent:@""] autorelease];
				[subMenuItem1 setTag:1];
				[subMenuItem2 setTarget:[embeddedControl target]];
				
				[submenu addItem:subMenuItem1];
				[submenu addItem:subMenuItem2];
				
				NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Sort Order", nil)  
															   action:nil keyEquivalent:@""] autorelease];
				[item setSubmenu:submenu];
				
				
				
				[item setEnabled:YES];
				
				[menu addItem:item];
			
				nbItemsAlreadyInMenu ++;
				
			} else if ([embeddedControl action] == @selector(switchStackSortKey:)) {
				
				NSMenu *submenu = [[[NSMenu alloc] initWithTitle:@""] autorelease];
				NSMenuItem *subMenuItem1 = [[[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"By Date", nil) 
																	   action:@selector(switchStackSortKey:) keyEquivalent:@""] autorelease];
				[subMenuItem1 setTag:0];
				[subMenuItem1 setTarget:[embeddedControl target]];
				NSMenuItem *subMenuItem2 = [[[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"By Name", nil) 
																	   action:@selector(switchStackSortKey:) keyEquivalent:@""] autorelease];
				[subMenuItem1 setTag:1];
				[subMenuItem2 setTarget:[embeddedControl target]];
				
				[submenu addItem:subMenuItem1];
				[submenu addItem:subMenuItem2];
				
				NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Sort Key", nil)  
															   action:nil keyEquivalent:@""] autorelease];
				[item setSubmenu:submenu];
				
				
				
				[item setEnabled:YES];
				
				[menu addItem:item];
				
				nbItemsAlreadyInMenu ++;
				
			}
			
		}
		
		// if there is a separator view between the buttons, add a separator item here.
		if ([view isMemberOfClass:[MGContextualToolGenericView class]] && [view isHidden] && nbItemsAlreadyInMenu > 0)
			[menu addItem:[NSMenuItem separatorItem]];
		
	}
	
	return menu;
}


@end
