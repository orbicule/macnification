//
//  CustomMDController.m
//  Filament
//
//  Created by Dennis Lorson on 09/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGCustomMetadataManagementController.h"

#import "MGCustomMetadataController.h"

@implementation MGCustomMetadataManagementController




- (void)awakeFromNib
{
	[[MGCustomMetadataController sharedController] addObserver:self];
	[self refreshContent];
}

- (void)refreshContent
{
	[metadataFieldsController setContent:[[MGCustomMetadataController sharedController] customAttributes]];
}

#pragma mark User interaction

- (IBAction)addAttribute:(id)sender
{
	[NSApp beginSheet:addMDSheet modalForWindow:prefsWindow modalDelegate:self didEndSelector:nil contextInfo:nil];
	
}


- (void)closeAddAttributeSheet:(id)sender
{
	if ([sender tag] == 0) {
		// ok
		if ([[addMDNameField stringValue] rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location == NSNotFound ||
			([[addMDNameField stringValue] rangeOfString:@"custom" options:NSCaseInsensitiveSearch].location != NSNotFound &&
			  [[addMDNameField stringValue] rangeOfString:@"metadata" options:NSCaseInsensitiveSearch].location != NSNotFound)) {
			NSBeep();
			return;
		}
		
		[[MGCustomMetadataController sharedController] addAttributeWithName:[addMDNameField stringValue]];
		
		[self refreshContent];
		
	}
	
	[NSApp endSheet:addMDSheet];
	[addMDSheet orderOut:self];
	
	
}

- (IBAction)removeAttribute:(id)sender
{
	NSString *attrName = [[[metadataFieldsController selectedObjects] objectAtIndex:0] objectForKey:@"name"];
	
	// display warning
	NSAlert *alert = [NSAlert alertWithMessageText:[NSString stringWithFormat:@"Are you sure you want to delete the attribute \"%@\"?", attrName]
									 defaultButton:@"Delete"
								   alternateButton:@"Cancel"
									   otherButton:nil
						 informativeTextWithFormat:@"Deleting an attribute will remove its data in all images, and can not be undone."];
	
	[alert beginSheetModalForWindow:prefsWindow modalDelegate:self didEndSelector:@selector(removeAttributeAlertDidEnd:returnCode:contextInfo:) contextInfo:nil];
					  
	
	
}
	 

- (void)removeAttributeAlertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo;
{	
	if (returnCode == NSAlertAlternateReturn || returnCode == NSAlertOtherReturn) return;
	
	// get the selection
	NSDictionary *attrToRemove = [[metadataFieldsController selectedObjects] objectAtIndex:0];

	[[MGCustomMetadataController sharedController] removeAttributeWithPath:[attrToRemove objectForKey:@"path"]];
	
	[self refreshContent];
	
}


- (void)changeAttributeName:(id)sender
{
	[NSApp beginSheet:renameMDSheet modalForWindow:prefsWindow modalDelegate:self didEndSelector:nil contextInfo:nil];
}


- (void)endChangeAttributeNameSheet:(id)sender
{
	if ([sender tag] == 1) {
		// cancel
		[NSApp endSheet:renameMDSheet];
		[renameMDSheet orderOut:self];
		
	}
	
	if ([[changeMDNameField stringValue] rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location == NSNotFound ||
		([[changeMDNameField stringValue] rangeOfString:@"custom" options:NSCaseInsensitiveSearch].location != NSNotFound &&
		 [[changeMDNameField stringValue] rangeOfString:@"metadata" options:NSCaseInsensitiveSearch].location != NSNotFound)) {
		NSBeep();
		return;
	}

	[[MGCustomMetadataController sharedController] changeAttributeWithName:[[[metadataFieldsController selectedObjects] objectAtIndex:0] objectForKey:@"name"] to:[changeMDNameField stringValue]];

	
	[self refreshContent];
	
	[NSApp endSheet:renameMDSheet];
	[renameMDSheet orderOut:self];
	
	
	
}

- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)controller;
{
	[self refreshContent];
}


@end
