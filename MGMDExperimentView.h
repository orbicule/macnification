//
//  MGMDExperimentView.h
//  Filament
//
//  Created by Dennis Lorson on 02/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGMDExperimentView : NSView {
	
	IBOutlet NSTextField *experimentSummaryField;
	IBOutlet NSPopUpButton *experimentPopup;
	
	IBOutlet NSArrayController *experimentsController;

	id observableController;
	NSString *observableKeyPath;

}


- (CGFloat)contentsHeight;


@end
