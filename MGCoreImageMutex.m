//
//  MGCoreImageMutex.m
//  Filament
//
//  Created by Dennis Lorson on 21/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGCoreImageMutex.h"


@implementation MGCoreImageMutex

+ (MGCoreImageMutex *)sharedMutex;
{
	static MGCoreImageMutex *sharedMutex_ = NULL;
	
	if (!sharedMutex_)
		sharedMutex_ = [[MGCoreImageMutex alloc] init];
	
	return sharedMutex_;
}

@end
