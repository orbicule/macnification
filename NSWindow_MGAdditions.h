//
//  NSWindow_MGAdditions.h
//  Filament
//
//  Created by Dennis Lorson on 01/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSWindow (MGAdditions)

- (void)setContentView:(NSView *)contentView andResizeAnimated:(BOOL)animated;

@end
