//
//  PSImageBrowserView.h
//  Filament
//
//  Created by Peter Schols on 07/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>
#import "KeywordAnimationController.h"
@class PSGradientView;


@interface PSImageBrowserView : IKImageBrowserView {

	BOOL isShowingSearchBar;
	IBOutlet PSGradientView *searchView;
	
	
	IBOutlet NSTextField *searchMessage;
	IBOutlet NSButton *doneButton;
	
	// Animation
	KeywordAnimationController *keywordAnimationController;
}


- (void)showSearchBarWithMessage:(NSString *)message;
- (void)hideSearchBar;

- (void)startAnimation:(NSString *)animation withSymbol:(NSString *)symbol forImagesAtIndexes:(NSIndexSet *)indices;


@end
