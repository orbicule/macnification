//
//  IFVerticallyExpandingTextfield.h
//  MetaDataView
//
//  Created by Dennis on 10/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//


#import <Cocoa/Cocoa.h>
#import "MGMDView.h"


enum { IFVerticalPadding = 1 };


@interface MGMDTextField : NSTextField <MGMDManualObserver>
{
	BOOL raisedEditingActive;
	
	id modelBindableValue;
	id observableController;
	NSString *observableKeyPath;
	
}

@property(nonatomic) BOOL raisedEditingActive;
@property(nonatomic, copy) id modelBindableValue;


- (id)initContentsFieldWithFrame:(NSRect)frame;

- (void)manualObserveValueForKeyPath:(NSString *)keypath ofObject:(NSString *)obj;

@end
