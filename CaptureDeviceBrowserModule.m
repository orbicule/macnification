//
//  CaptureDeviceBrowserModule.m
//  Filament
//
//  Created by Dennis Lorson on 16/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureDeviceBrowserModule.h"

#import "CaptureDevice.h"
#import "CaptureICDevice.h"
#import "CaptureQTDevice.h"
#import "CapturePRDevice.h"
#import "CaptureDC1394Device.h"

@implementation CaptureDeviceBrowserModule

@synthesize delegate = delegate_;

- (id) init
{
	self = [super init];
	if (self != nil) {
		devices_ = [[NSMutableArray array] retain];
	}
	return self;
}

- (void) dealloc
{
	[devices_ release];
	[super dealloc];
}


+ (CaptureDeviceBrowserModule *)sharedBrowserModule;  { return NULL; }

- (void)startSearchingForDevices {}
- (void)stopSearchingForDevices {}


- (void)didAddDevice:(CaptureDevice *)dev
{
	if ([self.delegate respondsToSelector:@selector(captureDeviceBrowserModule:didAddDevice:)])
		[self.delegate captureDeviceBrowserModule:self didAddDevice:dev];
	
}

- (void)didRemoveDevice:(CaptureDevice *)dev
{
	if ([self.delegate respondsToSelector:@selector(captureDeviceBrowserModule:didRemoveDevice:)])
		[self.delegate captureDeviceBrowserModule:self didRemoveDevice:dev];
}



@end


#pragma mark -
@interface CaptureDeviceBrowserModuleQT ()
#pragma mark -

- (void)qtDevicesDidChange:(NSNotification *)note;

@end

#pragma mark -
@implementation CaptureDeviceBrowserModuleQT
#pragma mark -



static CaptureDeviceBrowserModuleQT *sharedQTModule = nil;

+ (CaptureDeviceBrowserModule *)sharedBrowserModule;
{
	if (!sharedQTModule)
		sharedQTModule = [[CaptureDeviceBrowserModuleQT alloc] init];
	
	return sharedQTModule;
}

- (void)startSearchingForDevices;
{ 	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qtDevicesDidChange:) name:QTCaptureDeviceWasConnectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qtDevicesDidChange:) name:QTCaptureDeviceWasDisconnectedNotification object:nil];
	
	[self qtDevicesDidChange:nil];	
}

- (void)stopSearchingForDevices;
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:QTCaptureDeviceWasConnectedNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:QTCaptureDeviceWasDisconnectedNotification object:nil];
	
}


- (void)qtDevicesDidChange:(NSNotification *)note
{
	NSArray *listOfCameras = [QTCaptureDevice inputDevicesWithMediaType:QTMediaTypeVideo];
	
	// add all new devices
	for (QTCaptureDevice *srcDevice in listOfCameras) {
		
		BOOL alreadyPresent = NO;
		
		for (CaptureDevice *knownDevice in devices_) {
			if ([knownDevice originalDevice] == srcDevice)
				alreadyPresent = YES;
		}
		
		if (alreadyPresent)
			continue;
		
		CaptureDevice *device = [CaptureDevice deviceWithQTDevice:srcDevice];
		[devices_ addObject:device];
		
		[self didAddDevice:device];
		
	}
	
	// remove all old devices
	for (CaptureDevice *knownDevice in [[devices_ copy] autorelease]) {
		
		// only if it's a QT device of course...
		if ([knownDevice isMemberOfClass:[CaptureICDevice class]])
			continue;
		
		BOOL stillAvailable = NO;
		for (QTCaptureDevice *srcDevice in listOfCameras)
			if (srcDevice == [knownDevice originalDevice])
				stillAvailable = YES;
		
		if (!stillAvailable) {
			[knownDevice stopSession];
			[devices_ removeObject:knownDevice];
			
			[self didRemoveDevice:knownDevice];
			
		}
		
	}
	
}



@end


#pragma mark -
@implementation CaptureDeviceBrowserModuleIC
#pragma mark -



static CaptureDeviceBrowserModuleIC *sharedICModule = nil;

+ (CaptureDeviceBrowserModule *)sharedBrowserModule;
{
	if (!sharedICModule)
		sharedICModule = [[CaptureDeviceBrowserModuleIC alloc] init];
	
	return sharedICModule;
}

- (void)startSearchingForDevices;
{ 
	
	icDeviceBrowser_ = [[ICDeviceBrowser alloc] init];
	icDeviceBrowser_.delegate = self;
	
	icDeviceBrowser_.browsedDeviceTypeMask = ICDeviceTypeMaskCamera
	| ICDeviceLocationTypeMaskLocal
	| ICDeviceLocationTypeMaskShared
	| ICDeviceLocationTypeMaskBonjour
	| ICDeviceLocationTypeMaskBluetooth
	| ICDeviceLocationTypeMaskRemote;
	
	[icDeviceBrowser_ start];
}

- (void)stopSearchingForDevices;
{
	[icDeviceBrowser_ stop];
}

#pragma mark -


- (void)deviceBrowser:(ICDeviceBrowser*)browser didAddDevice:(ICDevice*)addedDevice moreComing:(BOOL)moreComing
{
	CaptureDevice *device = [CaptureDevice deviceWithICDevice:(ICCameraDevice *)addedDevice];
	[devices_ addObject:device];
	
	[self didAddDevice:device];
	
	
}

- (void)deviceBrowser:(ICDeviceBrowser*)browser didRemoveDevice:(ICDevice*)device moreGoing:(BOOL)moreGoing;
{
 	for (CaptureDevice *knownDevice in [[devices_ copy] autorelease]) {
		if (device == [knownDevice originalDevice]) {
			[knownDevice stopSession];
			[devices_ removeObject:knownDevice];
			
			[self didRemoveDevice:knownDevice];
			
		}
	}
}


@end




#pragma mark -
@interface CaptureDeviceBrowserModulePR ()
#pragma mark -

//- (void)pollTimerDidFire:(NSTimer *)timer;

@end


#pragma mark -
@implementation CaptureDeviceBrowserModulePR
#pragma mark -

/*
void mexBusCallback(unsigned long status, unsigned long UserValue, pr_device_id CamGUID)
{
	// async!!
	
	// callback when something on the FW bus changes
	CaptureDeviceBrowserModulePR *module = (CaptureDeviceBrowserModulePR *)UserValue;
	[module performSelectorOnMainThread:@selector(pollTimerDidFire:) withObject:nil waitUntilDone:YES];
}


static CaptureDeviceBrowserModulePR *sharedPRModule = nil;

+ (CaptureDeviceBrowserModule *)sharedBrowserModule;
{
	if (!sharedPRModule)
		sharedPRModule = [[CaptureDeviceBrowserModulePR alloc] init];
	
	return sharedPRModule;
}

- (void)startSearchingForDevices
{
	long err = mexInit(mexBusCallback, (unsigned long)self);
	if (err == NOERR)
		NSLog(@"Initialized ProgRes framework successfully");
	else
		NSLog(@"ProgRes framework failed to initialize (error %ld)", err);
	
	[self performSelector:@selector(pollTimerDidFire:) withObject:nil afterDelay:0.2];
	//[self pollTimerDidFire:nil];
	//pollTimer_ = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(pollTimerDidFire:) userInfo:nil repeats:YES] retain];
}

- (void)pollTimerDidFire:(NSTimer *)timer
{
	unsigned int numberOfCamerasFound = 100;
	pr_device_id cameraIDs[100];

	long err = mexFindCameras(&numberOfCamerasFound, cameraIDs);
	
	if (err == NOERR) {
		//NSLog(@"Found %i cameras", numberOfCamerasFound);
	} else {
		NSLog(@"Error finding cameras");
		return;
	}
	
	if (numberOfCamerasFound > 0) {
		for (int i = 0; i < numberOfCamerasFound; i++) {
			
			BOOL deviceIsAlreadyPresent = NO;
			for (CapturePRDevice *otherDevice in devices_)
				if (otherDevice.guid == cameraIDs[i])
					deviceIsAlreadyPresent = YES;
			
			if (deviceIsAlreadyPresent)
				continue;
			
			NSLog(@"Adding device: %llx", cameraIDs[i]);
			CaptureDevice *dev = [CapturePRDevice deviceWithPRDeviceID:cameraIDs[i]];
			
			[devices_ addObject:dev];
			
			[self didAddDevice:dev];
			
		}
	}
	
	
	// remove the camera's that are not in the list anymore
	for (CapturePRDevice *existingDevice in [[devices_ copy] autorelease]) {
		
		BOOL isNotInGUIDListAnymore = YES;
		
		for (int i = 0; i < numberOfCamerasFound; i++) {
			if (existingDevice.guid == cameraIDs[i])
				isNotInGUIDListAnymore = NO;
		}
		
		if (isNotInGUIDListAnymore) {
			NSLog(@"Removing device: %llx", existingDevice.guid);

			[self didRemoveDevice:existingDevice];
			[devices_ removeObject:existingDevice];
		}
		
	}
	
}


- (void)stopSearchingForDevices
{
	mexExit();
}*/

@end


#pragma mark -
@implementation CaptureDeviceBrowserModuleDC1394
#pragma mark -


static CaptureDeviceBrowserModuleDC1394 *sharedDC1394Module = nil;

+ (CaptureDeviceBrowserModule *)sharedBrowserModule;
{
	if (!sharedDC1394Module)
		sharedDC1394Module = [[CaptureDeviceBrowserModuleDC1394 alloc] init];
	
	return sharedDC1394Module;
}



- (void)startSearchingForDevices
{
	dc1394_ = dc1394_new();                                                     /* Initialize libdc1394 */
	if (dc1394_) {
		//NSLog(@"Initialized DC1394 framework successfully");
	}
	else {
		NSLog(@"DC1394 framework failed to initialize");
		return;
	}
	
	pollTimer_ = [[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(pollTimerDidFire:) userInfo:nil repeats:YES] retain];
}


- (void)stopSearchingForDevices
{
	[pollTimer_ invalidate];
	[pollTimer_ release];
	pollTimer_ = nil;
	
	dc1394_free(dc1394_);
	dc1394_ = NULL;
}


- (void)pollTimerDidFire:(NSTimer *)timer
{
	dc1394error_t err;
	
	dc1394camera_list_t * list;
	err = dc1394_camera_enumerate(dc1394_, &list);
	DC1394_ERR(err, "Failed to enumerate cameras");
	
	//NSLog(@"polling....%i cameras", list->num);

	
	if (list->num > 0) {
		
		for (int i = 0; i < list->num; i++) {
						
			BOOL deviceIsAlreadyPresent = NO;
			for (CaptureDC1394Device *otherDevice in devices_)
				if (otherDevice.guid == list->ids[i].guid)
					deviceIsAlreadyPresent = YES;
			
			if (deviceIsAlreadyPresent)
				continue;
			
			CaptureDevice *dev = [CapturePRDevice deviceWithDC1394DeviceID:list->ids[i].guid controller:dc1394_];
            
            
            if ([dev isValid]) {
                [devices_ addObject:dev];
                
                [self didAddDevice:dev];
            }

			
		}
		
	}
	
	
	// remove the camera's that are not in the list anymore
	for (CaptureDC1394Device *existingDevice in [[devices_ copy] autorelease]) {
		
		BOOL isNotInGUIDListAnymore = YES;
		
		for (int i = 0; i < list->num; i++) {
			if (existingDevice.guid == list->ids[i].guid)
				isNotInGUIDListAnymore = NO;
		}
		
		if (isNotInGUIDListAnymore) {
			[self didRemoveDevice:existingDevice];
			[devices_ removeObject:existingDevice];
		}
		
	}
	
	dc1394_camera_free_list (list);

	
}


@end
