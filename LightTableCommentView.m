//
//  LightTableCommentView.m
//  LightTable
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableCommentView.h"
#import "QuartzCore/QuartzCore.h"


@implementation LightTableCommentView

/*- (id)animationForKey:(NSString *)key
{
	if ([key isEqualToString:@"frame"]) {
		
		CAAnimation *anim = [CAAnimation animation];
		anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		
		return anim;
	}
	
	return [CAAnimation animation];
}*/
		
		
- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super initWithCoder:decoder])) {
		
		
	}
	return self;
}

- (void)drawRect:(NSRect)rect 
{
	NSRect innerBounds = NSMakeRect(1,1, [self bounds].size.width-2,[self bounds].size.height-2);
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:innerBounds xRadius:6 yRadius:6];
	[[NSColor colorWithCalibratedRed:1.0 green:1.0 blue:1.0 alpha:0.7] set];
	[path setLineWidth:2.0];
	[path fill];
	[[NSColor darkGrayColor] set];
	[path stroke];

}


@end
