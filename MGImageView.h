//
//  MGImageView.h
//  Filament
//
//  Created by Dennis Lorson on 06/09/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@class MGImageLayer;
@class MGImageLayer;
@class MGImageOverlayView;

typedef enum _MGToolMode 
{
	
	MGToolModeNone = 0,
	MGToolModeSelect = 1,
	MGToolModeMove = 2
	
} MGToolMode;



@interface MGImageView : NSView 
{
    CGSize originalImageSize_;
	
	CGFloat zoomFactor_;
	CIImage *image_;
	MGToolMode toolMode_;
	
	MGImageLayer *imageLayer_;
	MGImageOverlayView *overlayView_;
	
	NSArray *filters_;
	
	CGPoint imagePosition_;		// position of the image layer, in the view coordinate system
	CGRect imageBounds_;
    
}


@property (nonatomic, retain) CIImage *image;
@property (nonatomic) CGFloat zoomFactor;
@property (nonatomic) MGToolMode toolMode;


- (void)initCommon;

- (void)setImageRep:(NSBitmapImageRep *)rep;


// filters
- (void)setFilters:(NSArray *)filters;

// overlay
- (void)hideOverlay;
- (void)showOverlay;
- (BOOL)isOverlayHidden;
- (void)setOverlayNeedsDisplay;
- (void)setOverlayNeedsDisplayInRect:(NSRect)rect;
- (void)drawOverlayRectInBounds:(NSRect)bounds withTransform:(NSAffineTransform *)trafo inView:(NSView *)view;

// zooming
- (void)zoomInWithAnimation:(BOOL)animated;
- (void)zoomOutWithAnimation:(BOOL)animated;
- (void)zoomImageToFitWithAnimation:(BOOL)animated;

- (BOOL)imageIsCompletelyVisible;

// scrolling
- (void)scrollToPoint:(NSPoint)aPoint;
- (void)scrollToRect:(NSRect)aRect;
- (void)scrollByX:(CGFloat)x y:(CGFloat)y;
- (NSRect)visibleImageRect;

// coordinate conversion
- (NSPoint)convertViewPointToImagePoint:(NSPoint)viewPoint;
- (NSPoint)convertImagePointToViewPoint:(NSPoint)viewPoint;
- (NSRect)convertImageRectToViewRect:(NSRect)rect;


- (void)updateLayoutWithAnimation:(BOOL)animated;


@end
