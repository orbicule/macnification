//
//  StackTableView.m
//  StackTable
//
//  Created by Dennis Lorson on 19/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "StackView.h"

#import "StackLayer.h"
#import "StackView_Layout.h"
#import "StackImageMainLayer.h"
#import "Stack.h"
#import "NSImage_MGAdditions.h"
#import "NSButton_MGAdditions.h"
#import "MGContextualToolStrip.h"
#import "MGPDFHUDView.h"
#import "MGAppDelegate.h"
#import "NSController_MGAdditions.h"
#import "MGEFImager.h"
#import "SourceListTreeController.h"
#import "MGLibraryController.h"

@interface NSEvent (NSEventPrivateStuffThatReallyShouldNotBePrivate)

- (CGFloat)magnification;

@end


@interface StackView (Private)

- (NSSize)windowSizeWithOldSize:(NSSize)oldSize;

- (void)imageSelectionDidChange;
- (void)imageContentDidChange;
- (void)stackSelectionDidChange;
- (void)imageSortingDidChange;

- (CGImageRef)imageWithGradient:(NSGradient *)gradient;


@end



@implementation StackView

@synthesize showReflections, layoutType;



- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
	
    if (self) {
		
		[self bind:@"showReflections" toObject:[NSUserDefaultsController sharedUserDefaultsController] withKeyPath:@"values.showStackReflections" options:nil];
		
		[self registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
		imageLayers = [[NSMutableArray array] retain];
		
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSliceLayer) name:@"UpdateSliceNotification" object:nil];
		
		arrowTimeoutTimer = nil;
		
		animatesLayout = YES;
		
		imageArrayController = [MGLibraryControllerInstance imageArrayController];
		
    }
	
    return self;
}

- (void)dealloc
{
	[imageLayers release];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}

- (void)awakeFromNib
{
	
	[self setupLayout];
	
	// the bindings that are both forwarded to the content view and used by ourself.
	[self bind:@"imageContent" toObject:stackImageArrayController withKeyPath:@"arrangedObjects" options:nil];
	[self bind:@"imageSelection" toObject:stackImageArrayController withKeyPath:@"selectedObjects" options:nil];
	[self bind:@"stackTableSelection" toObject:stackArrayController withKeyPath:@"selectedObjects" options:nil];
	
	[stackImageArrayController addObserver:self forKeyPath:@"sortDescriptors" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];

	
	sliceStartPoint = NSMakePoint(50, 60);
	sliceEndPoint = NSMakePoint(200, 100);
	
			
	[stackArrayController addObserver:self forKeyPath:@"selection.browserZoom" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:nil];
	
	
	// register for events from the view that encompasses the PDF HUD controls
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageQueueDidEmpty:) name:@"MGOperationQueueEmptyNotification" object:nil];

	[self observeValueForKeyPath:@"selectedObjects" ofObject:stackArrayController change:0 context:nil];
	[self observeValueForKeyPath:@"arrangedObjects" ofObject:stackImageArrayController change:0 context:nil];
	[self observeValueForKeyPath:@"selectedObjects" ofObject:stackImageArrayController change:0 context:nil];
	

}

- (void)initializeToolStripForLayoutType:(MGStackLayoutType)type
{
	[toolStrip removeItems];
	
	NSImage *slice = [NSImage imageNamed:@"slice"];
	NSImage *saveSlice = [NSImage imageNamed:@"saveslice"];
	NSImage *movieImg = [NSImage imageNamed:@"movie"];
	NSImage *efi = [NSImage imageNamed:@"efi"];
	NSImage *remove = [NSImage imageNamed:@"Remove"];
	
	[slice setSize:NSMakeSize(32, 32)];
	[saveSlice setSize:NSMakeSize(32, 32)];
	[movieImg setSize:NSMakeSize(32, 32)];
	[efi setSize:NSMakeSize(32, 32)];
	[remove setSize:NSMakeSize(32, 32)];
	
	[sliceButton release]; sliceButton = nil;
	[saveSliceButton release]; saveSliceButton = nil;
	[movieButton release]; movieButton = nil;
	[prevButton release]; prevButton = nil;
	[nextButton release]; nextButton = nil;
	[sliceButton release]; sliceButton = nil;
	[efiButton release]; efiButton = nil;
	[removeButton release]; removeButton = nil;
	[resetButton release]; resetButton = nil;
	
	sliceButton = [[NSButton toolStripButtonWithImage:slice title:@"Toggle Slice"] retain];
	saveSliceButton = [[NSButton toolStripButtonWithImage:saveSlice title:@"Save Slice"] retain];
	movieButton = [[NSButton toolStripButtonWithImage:movieImg title:@"Movie"] retain];
	efiButton = [[NSButton toolStripButtonWithImage:efi title:@"Focused Image"] retain];
	prevButton = [[NSButton toolStripButtonWithImage:[NSImage imageNamed:@"downArrow"] title:@"Previous"] retain];
	[prevButton setContinuous:YES];
	[prevButton setPeriodicDelay:0.5 interval:0.15];
	nextButton = [[NSButton toolStripButtonWithImage:[NSImage imageNamed:@"upArrow"] title:@"Next"] retain];
	[nextButton setPeriodicDelay:0.5 interval:0.15];
	[nextButton setContinuous:YES];
	
	removeButton = [[NSButton toolStripButtonWithImage:remove title:@"Remove"] retain];
	resetButton = [[NSButton toolStripButtonWithImage:[NSImage imageNamed:@"reload"] title:@"Reset Offsets"] retain];
	
	[saveSliceButton setEnabled:sliceModeOn];
	
	[sliceButton setTarget:self];
	[saveSliceButton setTarget:self];
	[movieButton setTarget:self];
	[efiButton setTarget:self];
	[prevButton setTarget:self];
	[nextButton setTarget:self];
	[resetButton setTarget:self];
	[removeButton setTarget:self];
	
	
	[sliceButton setAction:@selector(toggleSliceTools:)];
	[saveSliceButton setAction:@selector(saveSliceAs:)];
	[movieButton setAction:@selector(createMovie:)];
	[efiButton setAction:@selector(createEFI:)];
	[prevButton setAction:@selector(previousLayer:)];
	[nextButton setAction:@selector(nextLayer:)];
	[resetButton setAction:@selector(resetLayerOffsets:)];
	[removeButton setAction:@selector(removeSelectedImage:)];
	
	[toolStrip addToolView:layoutTypeControlContainer withAlignment:MGLeftViewAlignment];
	
	if (type != MGBrowserStackLayout) {
		[toolStrip addToolView:sliceButton withAlignment:MGCenterViewAlignment];
		[toolStrip addToolView:saveSliceButton withAlignment:MGCenterViewAlignment];
	}

	[toolStrip addToolView:efiButton withAlignment:MGCenterViewAlignment];
	[toolStrip addToolView:movieButton withAlignment:MGCenterViewAlignment];
	
	[toolStrip addToolView:removeButton withAlignment:MGRightViewAlignment];
	
	if (type != MGBrowserStackLayout) {
		
		[toolStrip addSeparatorWithAlignment:MGRightViewAlignment];
		
		[toolStrip addToolView:prevButton withAlignment:MGRightViewAlignment];
		[toolStrip addToolView:nextButton withAlignment:MGRightViewAlignment];
	}
	
}




#pragma mark -
#pragma mark Bindings/observing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bindings/observing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	
	NSKeyValueObservingOptions opt = (NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial);
	
	if ([binding isEqualToString:@"imageSelection"]) {
		// this binding is used for handling the selected image.
		// the object bound to is the STimage array ctl
		stackImageArrayController = observableController;
		[observableController addObserver:self forKeyPath:keyPath options:opt context:nil];

		// observe the filtered image content for changes in filters etc
		[observableController addObserver:self forKeyPath:@"arrangedObjects.image.filteredImage" options:NSKeyValueObservingOptionNew context:nil];
		//[observableController addManualObserver:self forKeyPath:@"arrangedObjects.image.filteredImage"];
	}
	
	else if ([binding isEqualToString:@"imageContent"]) {
		// this binding is used for handling the array of images that this view displays in its layer tree.
		stackImageArrayController = observableController;
		[observableController addObserver:self forKeyPath:keyPath options:opt context:nil];

		
		
	}
	
	else if ([binding isEqualToString:@"stackTableSelection"]) {
		// this binding is used to update all other stack table properties like its name, when displayed in this view.
		stackArrayController = observableController;
		[observableController addObserver:self forKeyPath:keyPath options:opt context:nil];
		
	}
	else {
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
	}
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (([keyPath isEqualToString:@"selectedObjects"] || [keyPath isEqualToString:@"selection"]) && object == stackImageArrayController) {
		[self imageSelectionDidChange];
	}
	
	else if (([keyPath isEqualToString:@"selectedObjects"] || [keyPath isEqualToString:@"selection"]) && object == stackArrayController) {
		[self stackSelectionDidChange];
	}
	
	else if ([keyPath isEqualToString:@"arrangedObjects"] && object == stackImageArrayController) {
		[self imageContentDidChange];
	}
	
	else if ([keyPath isEqualToString:@"selection.browserZoom"]) {
		[self updateStackLayout];
	}
	
	else if (object == stackImageArrayController && [keyPath isEqualToString:@"sortDescriptors"]) {
		[self imageSortingDidChange];
	}
	
	
	else if ([keyPath isEqualToString:@"arrangedObjects.image.filteredImage"]) {
		
		if ([[stackImageArrayController selectedObjects] count] == 1) {
			
			//StackImage *selectedImg = [[stackImageArrayController selectedObjects] objectAtIndex:0];
			
			for (StackImageMainLayer *layer in [stackLayer sublayers]) {
				
				/*if (layer.stackImage == selectedImg)*/ [layer setHierarchyNeedsDisplay];
				
			}
			
		}
		
	}
	
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:nil];
	}
	
	
}


- (NSArray *)exposedBindings
{
	return [NSArray arrayWithObjects:@"imageSelection", @"imageContent", @"stackTableSelection", nil];
}


#pragma mark -


- (void)imageSelectionDidChange
{
	
	// need to select the layer belonging to the correct stackTableImage; this can only be one image...
	// this means, if the new selection count != 1, ignore it.
	
	if ([[stackImageArrayController selectedObjects] count] != 1) {
		[prevButton setEnabled:NO];
		[nextButton setEnabled:NO];
		[removeButton setEnabled:NO];
		return;
	}
	
	// deselect all previously selected layers first (should be just one)
	NSMutableArray *layersToDeselect = [[imageLayers mutableCopy] autorelease];
	
	for (StackImageMainLayer *layer in layersToDeselect) {
		layer.selected = NO;
	}
	
	// now select according to the array ctl.
	for (StackImageMainLayer *layer in imageLayers) {
		if ([[stackImageArrayController selectedObjects] containsObject:layer.stackImage]) {
			layer.selected = YES;
			stackLayer.selectedLayer = layer;
			
		}
		
	}
	
	
	[stackLayer setNeedsLayout];
	
	// propagate the selection to the main image array controller
	if ([[stackImageArrayController selectedObjects] count] == 1 && [[stackArrayController selectedObjects] count] == 1) {
		id image = [[[stackImageArrayController selectedObjects] objectAtIndex:0] valueForKey:@"image"];
		if (image) {
			[imageArrayController setSelectedObjects:[NSArray arrayWithObject:image]];				
		}
		else
			NSLog(@"StackImage has an empty Image relationship");
	} else {
		[imageArrayController setSelectedObjects:[NSArray array]];
	}
	
	// update UI controls
	NSArray *sortedLayers = [stackLayer sortedSublayers];
	
	NSInteger selectedIndex = [sortedLayers indexOfObject:stackLayer.selectedLayer];
	
	BOOL canMoveForward = selectedIndex < [sortedLayers count] - 1;
	BOOL canMoveBack = selectedIndex > 0;
	
	[prevButton setEnabled:canMoveBack];
	[nextButton setEnabled:canMoveForward];
	[removeButton setEnabled:YES];
	
}

- (void)imageContentDidChange
{

	[self rearrangeStackImageLayers];
	
	
	// array of images in current stack table changed
	
	// update UI controls
	[movieButton setEnabled:[[stackImageArrayController arrangedObjects] count] > 0];
	[efiButton setEnabled:[[stackImageArrayController arrangedObjects] count] > 0];
	
	
	// to make sure images are fetched in order
	//[self updateStackOrder];
	
	// now remove the old ones
	NSMutableArray *layersToRemove = [[imageLayers mutableCopy] autorelease];
	
	// construct an array of imgviews whose model objects are still in the arrayctl
	NSMutableArray *layersToRetain = [NSMutableArray array];
	for (StackImageMainLayer *layer in imageLayers) {
		if ([[stackImageArrayController arrangedObjects] containsObject:layer.stackImage]) {
			[layersToRetain addObject:layer];
		}
	}
	
	[layersToRemove removeObjectsInArray:layersToRetain];
	
	
	
	// we need to select the closest layer to the currently selected layer, if the latter is removed.
	// create a temp array to accomodate these objects.
	NSArray *sortedLayers = nil;
	
	BOOL needsReselect = [layersToRemove containsObject:stackLayer.selectedLayer];
	StackImageMainLayer *closestLayer = nil;
	
	if (needsReselect) {
		sortedLayers = [stackLayer sortedSublayers];
		
		NSInteger startIndex = [sortedLayers indexOfObject:stackLayer.selectedLayer];
		// try to select the next one, then the prev one, then +2, then -2,... until a valid one is found
		
		NSInteger offset = 0;
		
		while (!closestLayer) {
			
			// search the next one.
			if (startIndex + offset < [sortedLayers count])
				if (![layersToRemove containsObject:[sortedLayers objectAtIndex:startIndex + offset]])
					closestLayer = [sortedLayers objectAtIndex:startIndex + offset];
			
			// if still not found, search the previous one
			if (!closestLayer && startIndex - offset > -1)
				if (![layersToRemove containsObject:[sortedLayers objectAtIndex:startIndex - offset]])
					closestLayer = [sortedLayers objectAtIndex:startIndex - offset];
			
			// we have nothing (empty array)
			if (startIndex - offset < 0 && startIndex + offset >= [sortedLayers count])
				break;
			
			offset++;
		}
		
	}
	
	
	
	for (StackImageMainLayer *layer in layersToRemove) {
		
		[self removeLayer:layer];
		
	}
	
	// select the new layer, if we have one
	if (needsReselect) {
		stackLayer.selectedLayer = closestLayer;
		if (closestLayer.stackImage)
			[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:closestLayer.stackImage]];
		
	}
	
	
	// get the new images (that weren't there before)
	// The KVO change dictionaries don't work (even mmalc says so) so we have to use our own variable to keep track of the old image array.
	NSMutableArray *imagesToAdd = [[[stackImageArrayController valueForKey:@"arrangedObjects"] mutableCopy] autorelease];
	for (StackImageMainLayer *layer in imageLayers) {
		[imagesToAdd removeObject:layer.stackImage];
	}
	
	// insert them
	
	NSMutableArray *newLayers = [NSMutableArray array];
	
	for (StackImage *stImage in imagesToAdd) {
		StackImageMainLayer *newLayer = [self imageLayerWithStackTableImage:stImage];
		[self addImageLayer:newLayer];
		
		[newLayers addObject:newLayer];
		
	}
	
	// if we haven't got a selection, selct the first image in the sorted array.
	if (!stackLayer.selectedLayer)
		stackLayer.selectedLayer = [[stackLayer sortedSublayers] objectAtIndex:0];
	
	[stackLayer setNeedsLayout];
	
	[self updateSlicePointsIfNeeded];
	
	
	// update the Back/Forward buttons
	if (!sortedLayers)
		sortedLayers = [stackLayer sortedSublayers];
	
	if (stackLayer.selectedLayer) {
		
		NSInteger selectedIndex = [sortedLayers indexOfObject:stackLayer.selectedLayer];
		
		BOOL canMoveForward = selectedIndex < [sortedLayers count] - 1;
		BOOL canMoveBack = selectedIndex > 0;
		
		[prevButton setEnabled:canMoveBack];
		[nextButton setEnabled:canMoveForward];
		[removeButton setEnabled:YES];
		
	} else {
		
		[prevButton setEnabled:NO];
		[nextButton setEnabled:NO];
	}

	[self refreshSliceLayer];
}

- (void)stackSelectionDidChange
{
	
	stackLayer.selectedLayer = nil;
	
	if ([[stackArrayController selectedObjects] count] == 0) return;
	
	NSArray *sortedSublayers = [stackLayer sortedSublayers];
	
	id collection = [[stackArrayController selectedObjects] objectAtIndex:0];
	
	if ([collection isKindOfClass:[NSManagedObject class]] && [[[collection entity] name] isEqualTo:@"Stack"]) {
		// stack table selection changed
		
		stackLayer.selectedLayer = nil;
		
		
		if ([[stackArrayController selectedObjects] count] == 0) return;
		
		Stack *table = [[stackArrayController selectedObjects] objectAtIndex:0];
		
		
		// update slice endpoints if needed
		
		NSPoint sliceStart = NSMakePoint([[table valueForKey:@"sliceStartX"] floatValue], [[table valueForKey:@"sliceStartY"] floatValue]);
		NSPoint sliceEnd = NSMakePoint([[table valueForKey:@"sliceEndX"] floatValue], [[table valueForKey:@"sliceEndY"] floatValue]);
		
		if (sliceStart.x != 0 || sliceStart.y != 0 || sliceEnd.x != 0 || sliceEnd.y != 0) {
			sliceStartPoint = sliceStart;
			sliceEndPoint = sliceEnd;
		} else {
			NSPoint p;
			p.x = 100;
			p.y = 100;
			sliceStartPoint = p;
			p.x = 200;
			p.y = 200;
			sliceEndPoint = p;
		}
		
		[self updateControls];
		// update slice image
		[self performSelector:@selector(refreshSliceLayer) withObject:nil afterDelay:0.1];
		
		// select the first layer
		if ([[stackLayer sublayers] count] > 0) {
			
			StackImageMainLayer *firstImgLayer = ((StackImageMainLayer *)[sortedSublayers objectAtIndex:0]);
			
			if (firstImgLayer.stackImage)
				[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:firstImgLayer.stackImage]];
			
			stackLayer.selectedLayer = firstImgLayer;
			
		}
		
		[stackLayer setNeedsLayout];
		
	}
	
}

- (void)imageSortingDidChange
{
	[self rearrangeStackImageLayers];

	[stackLayer setNeedsLayout];

	[self refreshSliceLayer];

	
}


- (void)updateStackLayout
{
	
	if ([[stackArrayController selectedObjects] count] == 0) return;
	
	id collection = [[stackArrayController selectedObjects] objectAtIndex:0];

	if ([collection isKindOfClass:[Stack class]]) {
		
		Stack *table = [[stackArrayController selectedObjects] objectAtIndex:0];
		
		
		// update layout type
		MGStackLayoutType type = MGBrowserStackLayout;
		NSString *modelLayoutType = [table valueForKey:@"layoutType"];
		
		if (!modelLayoutType) modelLayoutType = @"TimeMachine";
			
		if ([modelLayoutType isEqualToString:@"TimeMachine"]) type = MGTimeMachineStackLayout;
		if ([modelLayoutType isEqualToString:@"Reorder"]) type = MGReorderStackLayout;
		if ([modelLayoutType isEqualToString:@"Perpendicular"]) type = MGPerpendicularStackLayout;
		
		[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
		[self changeStackLayoutType:type];
		
	}
	
}




#pragma mark -
#pragma mark Layer modification
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Layer modification
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)removeLayer:(StackImageMainLayer *)layer
{
	[CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];
	[CATransaction setValue:[NSNumber numberWithFloat:0.5] forKey:kCATransactionAnimationDuration];
	[imageLayers removeObject:layer];
	[layer removeFromSuperlayer];
	
	
}

- (void)addImageLayer:(StackImageMainLayer *)layer
{
	[imageLayers addObject:layer];
	[stackLayer addSublayer:layer];
	[stackLayer setNeedsLayout];
	
}

- (StackImageMainLayer *)imageLayerWithStackTableImage:(StackImage *)stImage
{
	StackImageMainLayer *newLayer = [[[StackImageMainLayer alloc] init] autorelease];
	
	// batch fault the filters, we'll need them threaded...
	[imageArrayController batchFaultObjects:[[stImage valueForKeyPath:@"image.filters"] allObjects] ofEntity:nil];
	
	// set the model reference.
	newLayer.stackImage = stImage;
	
	newLayer.showReflections = self.showReflections;
	
	// add a sublayer which will contain the AppKit drawing
	CALayer *overlay = [[[CALayer alloc] init] autorelease];
	
	overlay.frame = CGRectMake(0, 0, newLayer.frame.size.width, newLayer.frame.size.height);
	overlay.delegate = self;
	overlay.needsDisplayOnBoundsChange = YES;
	overlay.autoresizingMask = (kCALayerHeightSizable | kCALayerWidthSizable );
	overlay.name = @"toolLayer";
	
	[newLayer addSublayer:overlay];
	newLayer.toolLayer = overlay;
	
	return newLayer;
	
	
}

- (NSArray *)sortedSublayers
{
	return [stackLayer sortedSublayers];
	
}

#pragma mark -
#pragma mark Drag and drop
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drag and drop
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSDragOperation)draggingEntered:(id < NSDraggingInfo >)sender
{
	return [[stackArrayController selectedObjects] count] == 1 ? NSDragOperationCopy : NSDragOperationNone;	
}


- (BOOL)prepareForDragOperation:(id < NSDraggingInfo >)sender
{
	return [[stackArrayController selectedObjects] count] == 1;
}


- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender
{
	/*NSArray *files = [[sender draggingPasteboard] propertyListForType:NSFilenamesPboardType];
	
	if ([[stackTableArrayController selectedObjects] count] != 1) 
	{
		NSBeep();
		return NO;
	}
	[(Stack *)[[stackTableArrayController selectedObjects] objectAtIndex:0] insertImages:files];
	
	// this call makes sure the needed layout is performed, otherwise no images appear when performing a drag (probably invalid temp. state)
	[self scrollBy:1];*/
	
	return YES;	
	
}



#pragma mark -
#pragma mark Events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (BOOL)acceptsFirstResponder
{
	return YES;
}

- (void)mouseDown:(NSEvent *)theEvent
{
	// we only need to check if the layer hit is the currently selected layer.  if so, indicate that dragging will invoke drawing updates for the bezier layers
	NSPoint viewPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	CGPoint location = CGPointMake(viewPoint.x, viewPoint.y);
	
	[lastHitLayer release];
	lastHitLayer = [(StackImageMainLayer *)[stackLayer hitTest:location] retain];
	
	if (![lastHitLayer isMemberOfClass:[StackImageMainLayer class]]) return;
	
	// convert location to the bezier layer
	CALayer *bezierLayer = lastHitLayer.toolLayer;
	CGPoint locationInBezierLayer = [bezierLayer convertPoint:location fromLayer:rootLayer];
	
	NSPoint layerOffset = lastHitLayer.layerOffset;
	layerOffset.x *= lastHitLayer.frame.size.width;
	layerOffset.y *= lastHitLayer.frame.size.height;
	
	NSPoint sliceStartPointWithOffset = sliceStartPoint;
	NSPoint sliceEndPointWithOffset = sliceEndPoint;
	
	sliceStartPointWithOffset.x -= layerOffset.x;
	sliceStartPointWithOffset.y -= layerOffset.y;
	sliceEndPointWithOffset.x -= layerOffset.x;
	sliceEndPointWithOffset.y -= layerOffset.y;
	
	NSPoint start = sliceStartPointWithOffset;
	NSPoint end = sliceEndPointWithOffset;
	CGPoint loc = locationInBezierLayer;
	
	CGFloat maxDist = 80.0;
	
	// determine the point closest to the click location
	if (((fabs(loc.x - start.x) < maxDist && fabs(loc.y - start.y) < maxDist) || (fabs(loc.x - end.x) < maxDist && fabs(loc.y - end.y) < maxDist)) && lastHitLayer == stackLayer.selectedLayer) {
		
		if (powf(loc.x - start.x, 2) + powf(loc.y - start.y, 2) > powf(loc.x - end.x, 2) + powf(loc.y - end.y, 2))
			lastClickedControlPoint = MGEndSliceControlPoint;
		else 
			lastClickedControlPoint = MGStartSliceControlPoint;
		
	} else
		lastClickedControlPoint = MGNoSliceControlPoint;
	
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	NSPoint viewPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	CGPoint location = CGPointMake(viewPoint.x, viewPoint.y);
	
	//StackImageLayer *hitLayer = (StackImageLayer *)[stackLayer hitTest:location];
	
	if (![lastHitLayer isMemberOfClass:[StackImageMainLayer class]]) return;
	
	CALayer *bezierLayer = lastHitLayer.toolLayer;
	CGPoint locationInBezierLayer = [bezierLayer convertPoint:location fromLayer:rootLayer];
	
	NSPoint layerOffset = lastHitLayer.layerOffset;
	layerOffset.x *= lastHitLayer.frame.size.width;
	layerOffset.y *= lastHitLayer.frame.size.height;
	
	CGPoint locationInBezierLayerWithOffset = locationInBezierLayer;
	
	locationInBezierLayerWithOffset.x += layerOffset.x;
	locationInBezierLayerWithOffset.y += layerOffset.y;
	
	// change the control points if applicable
	if (lastClickedControlPoint == MGStartSliceControlPoint && lastHitLayer == stackLayer.selectedLayer) {
		sliceStartPoint = NSMakePoint(locationInBezierLayerWithOffset.x, locationInBezierLayerWithOffset.y);
		[[[stackArrayController selectedObjects] objectAtIndex:0] setValue:[NSNumber numberWithFloat:sliceStartPoint.x] forKey:@"sliceStartX"];
		[[[stackArrayController selectedObjects] objectAtIndex:0] setValue:[NSNumber numberWithFloat:sliceStartPoint.y] forKey:@"sliceStartY"];
	}
	
	else if (lastClickedControlPoint == MGEndSliceControlPoint && lastHitLayer == stackLayer.selectedLayer) {
		sliceEndPoint = NSMakePoint(locationInBezierLayerWithOffset.x, locationInBezierLayerWithOffset.y);
		[[[stackArrayController selectedObjects] objectAtIndex:0] setValue:[NSNumber numberWithFloat:sliceEndPoint.x] forKey:@"sliceEndX"];
		[[[stackArrayController selectedObjects] objectAtIndex:0] setValue:[NSNumber numberWithFloat:sliceEndPoint.y] forKey:@"sliceEndY"];
	}
	
	// update the bezier layers if needed (only the visible ones)
	if (lastClickedControlPoint == MGEndSliceControlPoint || lastClickedControlPoint == MGStartSliceControlPoint) {
		
		[self updateBezierPathLayers];
				
	}
	
	
	if (lastClickedControlPoint == MGEndSliceControlPoint || lastClickedControlPoint == MGStartSliceControlPoint) {
		// only update if the amount of images < a set level
		//if ([[stackLayer sublayers] count] < 100)
			[self refreshSliceLayer];
	}
}

- (void)mouseUp:(NSEvent *)theEvent
{
	//NSPoint viewPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	//CGPoint location = CGPointMake(viewPoint.x, viewPoint.y);
	
	//StackImageLayer *hitLayer = (StackImageLayer *)[stackLayer hitTest:location];
	
	if (![lastHitLayer isMemberOfClass:[StackImageMainLayer class]]) return;
	
	
	[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:lastHitLayer.stackImage]];
	
	if ([theEvent clickCount] == 2) {
		//[self switchStackLayoutType:TimeMachineStackLayout];
	}
	
	// check if the points given are still inside the valid rect
	[self updateSlicePointsIfNeeded];
	
	if (lastClickedControlPoint == MGEndSliceControlPoint || lastClickedControlPoint == MGStartSliceControlPoint) {
		
		[self refreshSliceLayer];
		
	}
	
	if ([theEvent clickCount] == 2)
		[(ImageArrayController *)[MGLibraryControllerInstance imageArrayController] enterFullScreen];
	
}

- (void)scrollWheel:(NSEvent *)theEvent
{
	if ([[stackImageArrayController arrangedObjects] count] == 0) return;
	
	NSInteger delta;
	
	if (fabs([theEvent deltaY]) > fabs([theEvent deltaX])) {
		delta = ([theEvent deltaY] > 0) ? fmax(1, [theEvent deltaY]) : fmin(-1, [theEvent deltaY]);
	} else {
		delta = ([theEvent deltaX] > 0) ? fmax(1, [theEvent deltaX]) : fmin(-1, [theEvent deltaX]);
	}
	
	[self scrollBy:delta];	
	
}

- (void)scrollBy:(NSInteger)scrollAmount
{
	NSArray *sortedStackLayerArray = [self arrangedStackImagelayers];
	int currentSelectedIndex = [sortedStackLayerArray indexOfObject:stackLayer.selectedLayer];
	
	// out of bounds?
	if (scrollAmount + currentSelectedIndex < 0)
		scrollAmount = - currentSelectedIndex;
	if (scrollAmount + currentSelectedIndex > [sortedStackLayerArray count] - 1) 
		scrollAmount = [sortedStackLayerArray count] - 1 - currentSelectedIndex;
	
	StackImage *newlySelectedImage = ((StackImageMainLayer *)[sortedStackLayerArray objectAtIndex:currentSelectedIndex + scrollAmount]).stackImage;
	
	if (!newlySelectedImage) return;
	
	[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:newlySelectedImage]];
	
}



- (void)keyDown:(NSEvent *)theEvent
{
	
	
    NSString  *characters = [theEvent characters];
    int key = [characters characterAtIndex: 0];
	
	
	if (self.layoutType != MGPerpendicularStackLayout) {
		
		
		// move forward or backward in the stack
		
		switch (key) {
				
			case NSUpArrowFunctionKey:
			case NSRightArrowFunctionKey:
				[self nextLayer:self];
				break;
				
			case NSDownArrowFunctionKey:				
			case NSLeftArrowFunctionKey:
				[self previousLayer:self];
				break;
					
		}
		
		
		
		
	} else {
		
		
		
		CGFloat imageOffsetX = [[((StackImageMainLayer *)stackLayer.selectedLayer).stackImage valueForKey:@"offsetX"] floatValue];
		CGFloat imageOffsetY = [[((StackImageMainLayer *)stackLayer.selectedLayer).stackImage valueForKey:@"offsetY"] floatValue];
		
		CGFloat delta = [theEvent modifierFlags] & NSShiftKeyMask ? 12 : 1;
		
		// no image should be offset more than 300 pixels.
		NSInteger maxOffset = 300;
		
		switch (key) {
				
			case NSUpArrowFunctionKey:
				imageOffsetY = fmin(imageOffsetY + delta, maxOffset);
				[self showUpArrowLayer];
				break;
				
			case NSDownArrowFunctionKey:
				imageOffsetY = fmax(imageOffsetY - delta, -maxOffset);
				[self showDownArrowLayer];
				break;
				
			case NSLeftArrowFunctionKey:
				imageOffsetX = fmax(imageOffsetX - delta, -maxOffset);
				[self showLeftArrowLayer];
				break;
				
			case NSRightArrowFunctionKey:
				imageOffsetX = fmin(imageOffsetX + delta, maxOffset);
				[self showRightArrowLayer];
				break;
				
		}
		
		StackImage *image = ((StackImageMainLayer *)stackLayer.selectedLayer).stackImage;
		
		if (![image isDeleted]) {
			
			[image setValue:[NSNumber numberWithFloat:imageOffsetX] forKey:@"offsetX"];
			[image setValue:[NSNumber numberWithFloat:imageOffsetY] forKey:@"offsetY"];
		}
		
		
		
		[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
		[stackLayer setNeedsLayout];
		
		
		if (!sliceModeOn) return;
		
		[self updateSlicePointsIfNeeded];
		
		// update ALL layers
		[self updateBezierPathLayers];
		
		
		[self refreshSliceLayer];

	}

}



- (void)magnifyWithEvent:(NSEvent *)theEvent;
{
	CGFloat magnification = (float)[(id)theEvent magnification];
	
	CGFloat oldZoomLevel = [[[MGLibraryControllerInstance sourcelistTreeController] valueForKeyPath:@"selection.browserZoom"] floatValue];
	CGFloat newVal = oldZoomLevel + magnification*1.;
	
	if (newVal > 0.99 || newVal < 0.01) return;
	//[(LightTableView *)[self superview] setZoomLevel:newVal];
	
	[[MGLibraryControllerInstance sourcelistTreeController] setValue:[NSNumber numberWithFloat:newVal] forKeyPath:@"selection.browserZoom"];
	
}



#pragma mark -
#pragma mark IBActions
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// IBActions
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
	//NSLog(@"validate %@", [anItem description]);
	SEL action = [anItem action];
	
	if (action == @selector(switchStackLayout:)) {
		
		return YES;
		
		
	}
	
	else if (action == @selector(switchStackSortKey:)) {
		
		return YES;
		
		
	}
	
	else if (action == @selector(toggleSliceTools:)) {
		
		return YES;
		
		
	}
	
	else if (action == @selector(saveSliceAs:)) {
		
		return sliceModeOn && [[stackImageArrayController arrangedObjects] count] > 0;
		
		
	}
	
	else if (action == @selector(createMovie:)) {
		
		return [[stackImageArrayController arrangedObjects] count] > 0;
		
		
	}
	
	else if (action == @selector(createEFI:)) {
		
		return [[stackImageArrayController arrangedObjects] count] > 0;
		
		
	}
	
	else if (action == @selector(nextLayer:)) {
		
		NSArray *sortedStackLayerArray = [self arrangedStackImagelayers];
		int currentSelectedIndex = [sortedStackLayerArray indexOfObject:stackLayer.selectedLayer];
		
		// out of bounds: return
		BOOL notLastLayer =  !(-1 + currentSelectedIndex < 0 || -1 + currentSelectedIndex > [sortedStackLayerArray count] - 1);
		
		return [[stackImageArrayController arrangedObjects] count] > 0 && notLastLayer;
		
		
	}
	
	else if (action == @selector(switchStackSortKey:)) {
		
		return YES;
		
		
	}
	
	return YES;
	
	
}


- (IBAction)switchStackLayout:(id)sender
{
	MGStackLayoutType newType;
	NSString *modelValue;
	
	switch ([sender tag]) {
			
		case 1:
		default:
			newType = MGTimeMachineStackLayout;
			modelValue = @"TimeMachine";
			break;
		case 2:
			newType = MGReorderStackLayout;
			modelValue = @"Reorder";
			break;
		case 0:
			newType = MGPerpendicularStackLayout;
			modelValue = @"Perpendicular";
			break;
		case 3:
			newType = MGBrowserStackLayout;
			modelValue = @"Browser";
			break;
			
	}
	
	[self changeStackLayoutType:newType];
	
	[self initializeToolStripForLayoutType:newType];
	
	// update model
	Stack *selectedTable = [[stackArrayController selectedObjects] objectAtIndex:0];
	if (![selectedTable isDeleted])
		[selectedTable setValue:modelValue forKey:@"layoutType"];
}

- (IBAction)toggleSliceTools:(id)sender
{
	sliceModeOn = !sliceModeOn;
	
	[saveSliceButton setEnabled:sliceModeOn];
	
	CGRect newStacklayerFrame = stackLayer.frame;
	if (sliceModeOn) {
		
		newStacklayerFrame.origin.y += 100;
		newStacklayerFrame.size.height -= 100;
		
	} else {
		
		newStacklayerFrame.origin.y -= 100;
		newStacklayerFrame.size.height += 100;
	}
	
	stackLayer.frame = newStacklayerFrame;
	
	slicePreviewLayer.opacity = sliceModeOn ? 0.7 : 0.0;
		
	//[stackLayer setNeedsLayout];
	[self updateSlicePointsIfNeeded];
	
	// ALL layers
	[self updateBezierPathLayers];
	// delay, otherwise stacklayer resize is not smooth
	if (sliceModeOn) 
		[self performSelector:@selector(refreshSliceLayer) withObject:nil afterDelay:0.25];
	else
		[self clearAllSliceResources];
	
}


- (void)updateControls
{
	// will update the controls belonging to the view based on the current table properties, when a table change occurs.
	switch (self.layoutType) {
			
		case MGPerpendicularStackLayout:
			
			[layoutSingleButton setState:NSOnState];
			[layoutCoverFlowButton setState:NSOffState];
			[layoutTimeMachineButton setState:NSOffState];
			
			break;
		case MGTimeMachineStackLayout:
			
			[layoutSingleButton setState:NSOffState];
			[layoutCoverFlowButton setState:NSOffState];
			[layoutTimeMachineButton setState:NSOnState];
			
			break;
		case MGReorderStackLayout:
			
			[layoutSingleButton setState:NSOffState];
			[layoutCoverFlowButton setState:NSOnState];
			[layoutTimeMachineButton setState:NSOffState];
			
			break;
			
		case MGBrowserStackLayout:
			
			[layoutSingleButton setState:NSOffState];
			[layoutCoverFlowButton setState:NSOffState];
			[layoutTimeMachineButton setState:NSOffState];
			
			break;
	}
	
}


#pragma mark -


- (IBAction)saveSliceAs:(id)sender
{	
	// TODO - add IKSaveOptions ???
	NSSavePanel *panel = [NSSavePanel savePanel];
	[panel setRequiredFileType:@"tiff"];
	[panel setExtensionHidden:NO];
	
	[panel beginSheetForDirectory:nil
							 file:[NSLocalizedString(@"Slice of ",nil) stringByAppendingString:[[[stackArrayController selectedObjects] objectAtIndex:0] valueForKey:@"name"]]
				   modalForWindow:[self window]
					modalDelegate:self 
				   didEndSelector:@selector(sliceSavePanelDidEnd:returnCode:contextInfo:) contextInfo:nil];
	
}

- (void)sliceSavePanelDidEnd:(NSSavePanel *)panel returnCode:(int)returnCode  contextInfo:(void  *)contextInfo;
{
	[panel orderOut:self];
	
	if (returnCode == NSOKButton) {
		
		if (lastChosenSlicePath)
			[lastChosenSlicePath release];
		lastChosenSlicePath = [[panel filename] copy];
		
		[saveSliceIndicator startAnimation:self];
		
		[NSApp beginSheet:saveSliceWindow modalForWindow:[self window] modalDelegate:self didEndSelector:nil contextInfo:nil];
		
		[self performSelectorInBackground:@selector(threaded_composeFullResSlice) withObject:nil];
		

		
	}
	
}

- (void)sliceFullResCompositionDidEnd
{
	// save the file
	NSURL *saveURL = [NSURL fileURLWithPath:[lastChosenSlicePath stringByExpandingTildeInPath]];
	[lastChosenSlicePath release];
	lastChosenSlicePath = nil;
	
	CGImageDestinationRef dest = CGImageDestinationCreateWithURL((CFURLRef)saveURL, kUTTypeTIFF, 1, NULL);
	// TODO add: some save properties here...
	CGImageDestinationAddImage(dest, fullResSlice, NULL);
	
	CGImageDestinationFinalize(dest);
	CFRelease(dest);
	
	CGImageRelease(fullResSlice);
	fullResSlice = nil;
	
	
	[NSApp endSheet:saveSliceWindow];
	
	[saveSliceWindow orderOut:self];
	
}

#pragma mark -

- (IBAction)createEFI:(id)sender
{
	[self showEFISheet];
}

#pragma mark -


- (IBAction)createMovie:(id)sender
{
	
	// set the controls to disabled while the data is loaded asynchronously
	[movieSaveButton setEnabled:NO];
	[movieCancelButton setEnabled:NO];
	[movieIndicator setHidden:NO];
	
	[movieView setHidden:YES];
	[movieDurationView setHidden:YES];
	[movieLoadingMessage setHidden:NO];
	[movieIndicator setHidden:NO];
	[movieIndicator startAnimation:self];

	
	// determine the aspect ratio
	NSArray *layers = [self arrangedStackImagelayers];
	if ([layers count] > 0) {
		StackImageMainLayer *layer = [layers objectAtIndex:0];
		
		NSImage *image = [layer.stackImage valueForKeyPath:@"image.filteredImage"];
		
		movieAspectRatio = [image size].height / [image size].width;
	} else {
		movieAspectRatio = -1;
	}
	
	if (movieAspectRatio != -1) {
		
		// resize the window
		
		NSSize newWindowSize = [self windowSizeWithOldSize:[movieWindow frame].size];

		NSRect newFrame = [movieWindow frame];
		newFrame.size = newWindowSize;
		[movieWindow setFrame:newFrame display:YES];
	}
	

	[NSApp beginSheet:movieWindow modalForWindow:[self window] modalDelegate:self didEndSelector:nil contextInfo:nil];	

	[self composeMovie];
	
}

- (IBAction)saveMovie:(id)sender
{
	[NSApp endSheet:movieWindow];
	[movieWindow orderOut:self];
	
	NSSavePanel *savePanel = [NSSavePanel savePanel];
	[savePanel setRequiredFileType:@"mov"];
	[savePanel setExtensionHidden:NO];
	
	[savePanel beginSheetForDirectory:nil 
								 file:[NSLocalizedString(@"Movie of ",nil) stringByAppendingString:[[[stackArrayController selectedObjects] objectAtIndex:0] valueForKey:@"name"]] 
					   modalForWindow:[self window] 
						modalDelegate:self 
					   didEndSelector:@selector(movieSavePanelDidEnd:returnCode:contextInfo:) contextInfo:nil];
	
}

- (void)movieSavePanelDidEnd:(NSSavePanel *)panel returnCode:(int)returnCode  contextInfo:(void  *)contextInfo;
{
	if (returnCode == NSOKButton) {
		
		[movie writeToFile:[panel filename] withAttributes:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:QTMovieFlatten]];
		
	}
	
	[self cleanupMovie];
	
}

- (IBAction)cancelMovie:(id)sender
{
	[NSApp endSheet:movieWindow];
	[movieWindow orderOut:self];
	
	[self cleanupMovie];
	
}

- (IBAction)changeMovieDuration:(id)sender
{
	[self updateMovieDuration:[[[sender selectedItem] title] floatValue]];
	
}


#pragma mark -

- (IBAction)nextLayer:(id)sender
{
	NSArray *sortedStackLayerArray = [self arrangedStackImagelayers];
	int currentSelectedIndex = [sortedStackLayerArray indexOfObject:stackLayer.selectedLayer];
	
	// out of bounds: return
	if (1 + currentSelectedIndex < 0 || 1 + currentSelectedIndex > [sortedStackLayerArray count] - 1) return;
	
	StackImage *newlySelectedImage = ((StackImageMainLayer *)[sortedStackLayerArray objectAtIndex:currentSelectedIndex + 1]).stackImage;
	[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:newlySelectedImage]];
	
	
}
- (IBAction)previousLayer:(id)sender
{
	NSArray *sortedStackLayerArray = [self arrangedStackImagelayers];
	int currentSelectedIndex = [sortedStackLayerArray indexOfObject:stackLayer.selectedLayer];
	
	// out of bounds: return
	if (-1 + currentSelectedIndex < 0 || -1 + currentSelectedIndex > [sortedStackLayerArray count] - 1) return;
	
	StackImage *newlySelectedImage = ((StackImageMainLayer *)[sortedStackLayerArray objectAtIndex:currentSelectedIndex - 1]).stackImage;
	[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:newlySelectedImage]];
	
}


- (IBAction)resetLayerOffsets:(id)sender
{
	
	[CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];
	
	for (StackImageMainLayer *layer in [stackLayer sublayers]) {
		
		StackImage *image = layer.stackImage;
		
		if (![image isDeleted]) {
			
			[image setValue:[NSNumber numberWithFloat:0] forKey:@"offsetX"];
			[image setValue:[NSNumber numberWithFloat:0] forKey:@"offsetY"];
		}
		
	}
	
	[stackLayer setNeedsLayout];
	
	if (!sliceModeOn) return;
	
	[self updateSlicePointsIfNeeded];
	
	// update ALL layers
	[self updateBezierPathLayers];
	
	[self refreshSliceLayer];
	
	
}

- (IBAction)removeSelectedImage:(id)sender
{
	// get the next and previous image, and select it
	// prefer the next image, unless this was the last image of the stack
	
	NSInteger index = [[stackImageArrayController arrangedObjects] indexOfObject:[[stackImageArrayController selectedObjects] objectAtIndex:0]];
	
	StackImage *stackImageToSelect = nil;
	
	// if the removed image is not the last image
	if (index < [[stackImageArrayController arrangedObjects] count] - 1)
		stackImageToSelect = [[stackImageArrayController arrangedObjects] objectAtIndex:index+1];
	// or not the first image
	else if (index > 0)
		stackImageToSelect = [[stackImageArrayController arrangedObjects] objectAtIndex:index-1];
	
	// remove the image
	
	StackImage *stackImageToRemove = [[stackImageArrayController selectedObjects] objectAtIndex:0];
	Stack *selectedStack = [[stackArrayController selectedObjects] objectAtIndex:0];
	NSMutableSet *stackImages = [selectedStack mutableSetValueForKey:@"stackImages"];

	Image *image = [stackImageToRemove valueForKey:@"image"];

	NSMutableSet *albums = [image mutableSetValueForKey:@"albums"];
	
	[albums addObject:[[MGLibraryControllerInstance library] libraryGroup]];
	
	[stackImages removeObject:stackImageToRemove];
	
	if (stackImageToSelect) {
		[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:stackImageToSelect]];
	}
	
	NSError *error = nil;
	[[image managedObjectContext] save:&error];
	
#ifdef _DEBUG
	if (error)
		[[NSApplication sharedApplication] presentError:error];
#endif
	
}


#pragma mark -
#pragma mark Guide Arrows
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Guide Arrows
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)initializeArrows
{
	
	// add the arrow layers
	NSImage *arrow;
	NSBitmapImageRep *arrowRep;
	CGRect bgFrame = [self layer].frame;
	CGFloat arrowSize = 64;
	CGFloat outerMargin = 30;
	
	leftArrowLayer = [CALayer layer];
	arrow = [NSImage imageNamed:@"arrowLeft"];
	arrowRep = (NSBitmapImageRep *)[arrow representation];
	leftArrowLayer.contents = (id)[arrowRep CGImage];
	leftArrowLayer.frame = CGRectMake(outerMargin, CGRectGetMidY(bgFrame) - arrowSize/2 , arrowSize, arrowSize);
	leftArrowLayer.autoresizingMask = kCALayerMaxXMargin | kCALayerMaxYMargin | kCALayerMinYMargin;
	[[self layer] addSublayer:leftArrowLayer];
	
	rightArrowLayer = [CALayer layer];
	arrow = [NSImage imageNamed:@"arrowRight"];
	arrowRep = (NSBitmapImageRep *)[arrow representation];
	rightArrowLayer.autoresizingMask = kCALayerMinXMargin | kCALayerMaxYMargin | kCALayerMinYMargin;
	rightArrowLayer.contents = (id)[arrowRep CGImage];
	rightArrowLayer.frame = CGRectMake(CGRectGetMaxX(bgFrame) - outerMargin - arrowSize, CGRectGetMidY(bgFrame) - arrowSize/2 , arrowSize, arrowSize);
	[[self layer] addSublayer:rightArrowLayer];
	
	upArrowLayer = [CALayer layer];
	arrow = [NSImage imageNamed:@"arrowUp"];
	arrowRep = (NSBitmapImageRep *)[arrow representation];
	upArrowLayer.autoresizingMask = kCALayerMinYMargin | kCALayerMaxXMargin | kCALayerMinXMargin;
	upArrowLayer.contents = (id)[arrowRep CGImage];
	upArrowLayer.frame = CGRectMake(CGRectGetMidX(bgFrame) - arrowSize/2 , CGRectGetMaxY(bgFrame) - outerMargin - arrowSize, arrowSize, arrowSize);
	[[self layer] addSublayer:upArrowLayer];
	
	downArrowLayer = [CALayer layer];
	arrow = [NSImage imageNamed:@"arrowDown"];
	arrowRep = (NSBitmapImageRep *)[arrow representation];
	downArrowLayer.autoresizingMask = kCALayerMaxYMargin | kCALayerMaxXMargin | kCALayerMinXMargin;
	downArrowLayer.contents = (id)[arrowRep CGImage];
	downArrowLayer.frame = CGRectMake(CGRectGetMidX(bgFrame) - arrowSize/2, outerMargin , arrowSize, arrowSize);
	[[self layer] addSublayer:downArrowLayer];
	
	
	upArrowLayer.opacity = downArrowLayer.opacity = leftArrowLayer.opacity = rightArrowLayer.opacity = 0.0;
	
	
}

- (void)setArrowTimeout:(NSTimeInterval)time
{
	// set the arrow to timeout within a given time.
	// resets the internal NSTimer as needed
	if (arrowTimeoutTimer) {
		// if it is not nil, it is guaranteed to be not released (by our code), so invalidate
		[arrowTimeoutTimer invalidate];
	}
	arrowTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(arrowTimerDidEnd:) userInfo:nil repeats:NO];
	
}

- (void)disableArrowTimeout
{ 
	if (arrowTimeoutTimer) {
		[arrowTimeoutTimer invalidate];
		arrowTimeoutTimer = nil;
	}
}

- (void)arrowTimerDidEnd:(NSTimer *)timer
{
	// first and foremost, set our timer var to nil because the timer has been released
	arrowTimeoutTimer = nil;
	
	[self hideAllArrowLayers];
	
}

- (void)hideAllArrowLayers
{
	[CATransaction setValue:[NSNumber numberWithFloat:0.5] forKey:kCATransactionAnimationDuration];
	upArrowLayer.opacity = downArrowLayer.opacity = leftArrowLayer.opacity = rightArrowLayer.opacity = 0.0;
}


- (void)showUpArrowLayer
{
	[self hideAllArrowLayers];
	
	[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
	upArrowLayer.opacity = 0.6;
	[self setArrowTimeout:0.8];
	
}


- (void)showDownArrowLayer
{
	[self hideAllArrowLayers];
	
	[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
	downArrowLayer.opacity = 0.6;
	[self setArrowTimeout:0.8];
	
}


- (void)showLeftArrowLayer
{
	[self hideAllArrowLayers];
	
	[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
	leftArrowLayer.opacity = 0.6;
	[self setArrowTimeout:0.8];
	
}


- (void)showRightArrowLayer
{
	[self hideAllArrowLayers];
	
	[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
	rightArrowLayer.opacity = 0.6;
	[self setArrowTimeout:0.8];
	
}



#pragma mark -
#pragma mark Layer layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Layer layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)setupLayout
{
	// set up the basic layout of the window (create layers and constraints)
	[self setWantsLayer:YES];
	
	backgroundLayer = [CALayer layer];
	
	backgroundLayer.backgroundColor = CGColorCreateGenericGray(0.18, 1);
	backgroundLayer.frame = [self layer].bounds;
	
	[self setLayer:backgroundLayer];
	
	
	// create the container layer for the stack
	stackLayer = [StackLayer layer];
	stackLayer.frame = CGRectMake(backgroundLayer.bounds.origin.x + 40, 
								  backgroundLayer.bounds.origin.y + 40, 
								  backgroundLayer.bounds.size.width - 80,
								  backgroundLayer.bounds.size.height - 80);
	stackLayer.layoutManager = self;
	stackLayer.autoresizingMask = (kCALayerHeightSizable | kCALayerWidthSizable);
	
	// set the correct layout type
	[self changeStackLayoutType:MGTimeMachineStackLayout];
	
	[backgroundLayer addSublayer:stackLayer];
	
	slicePreviewLayer = [CALayer layer];
	//slicePreviewLayer = [CATiledLayer layer];
	slicePreviewLayer.delegate = self;
	
	CGRect sliceRect = CGRectInset([backgroundLayer bounds], 20, 20);
	sliceRect.size.width = 300;
	sliceRect.size.height = 250;
	sliceRect.origin.y = 20;
	sliceRect.origin.x = (backgroundLayer.frame.size.width - 300)/2;

	sliceRect.size.height = 200;
	
	slicePreviewLayer.frame = sliceRect;
	slicePreviewLayer.autoresizingMask = (kCALayerMinXMargin | kCALayerMaxYMargin | kCALayerMaxXMargin);
	slicePreviewLayer.backgroundColor = CGColorCreateGenericGray(0.5, 1.0);
	slicePreviewLayer.magnificationFilter = kCAFilterLinear;
	slicePreviewLayer.minificationFilter = kCAFilterLinear;
	slicePreviewLayer.borderColor = CGColorCreateGenericGray(1.0, 0.8);
	slicePreviewLayer.borderWidth = 1.0;
	slicePreviewLayer.opacity = 0.0;
	
	[backgroundLayer addSublayer:slicePreviewLayer];
	
	// add the navigation layers.
	[self initializeArrows];
	
}


- (void)changeStackLayoutType:(MGStackLayoutType)newType
{
	// browserzoom from 0.0 to 1.0, default 0.2
	CGFloat browserZoom = [[stackArrayController valueForKeyPath:@"selection.browserZoom"] floatValue];
		
	CGFloat magnification = 0.85 + 0.75 * browserZoom;
	
	
	CATransform3D sublayerTransform;
	
	switch (newType) {
			
		case MGTimeMachineStackLayout:
		default:
			
			[layoutSingleButton setState:NSOffState];
			[layoutCoverFlowButton setState:NSOffState];
			[layoutTimeMachineButton setState:NSOnState];
			
			sublayerTransform = CATransform3DMakeRotation(0.4, 1, 0, 0);											// rotate so we get the feeling we're looking over the edge of the array
			sublayerTransform = CATransform3DConcat(sublayerTransform, CATransform3DMakeScale(magnification, magnification, 1));
			sublayerTransform = CATransform3DConcat(CATransform3DMakeTranslation(0, 130, 0), sublayerTransform);
			sublayerTransform = NO ? CATransform3DConcat(CATransform3DMakeTranslation(0, 0, -300), sublayerTransform) : sublayerTransform;  // to be removed (replaced by layer frame adj)
			sublayerTransform.m34 = (1. / -990.0);																		// perspective transform
			break;
			
		case MGReorderStackLayout:
			
			[layoutSingleButton setState:NSOffState];
			[layoutCoverFlowButton setState:NSOnState];
			[layoutTimeMachineButton setState:NSOffState];
			
			
			sublayerTransform = CATransform3DMakeRotation(0.25, 1, 0, 0);
			sublayerTransform = CATransform3DConcat(sublayerTransform, CATransform3DMakeScale(magnification, magnification, 1));
			sublayerTransform.m34 = 1. / -950;
			sublayerTransform = CATransform3DConcat(CATransform3DMakeTranslation(600, -100, -1800), sublayerTransform);	// position the layer correctly in the coordinate space
			sublayerTransform = CATransform3DConcat(CATransform3DMakeRotation(0.85, 0, 1, 0), sublayerTransform);		// apply a rotation
			
			break;
			
		case MGPerpendicularStackLayout:
			
			
			[layoutSingleButton setState:NSOnState];
			[layoutCoverFlowButton setState:NSOffState];
			[layoutTimeMachineButton setState:NSOffState];
			
			sublayerTransform = CATransform3DIdentity;
			sublayerTransform = CATransform3DConcat(sublayerTransform, CATransform3DMakeScale(magnification, magnification, 1));
			sublayerTransform = CATransform3DConcat(CATransform3DMakeTranslation(0, 130, 0), sublayerTransform);
			break;
			
		case MGBrowserStackLayout:
			
			break;
			
			
	}
	
	[CATransaction setValue:[NSNumber numberWithFloat:0.4] forKey:kCATransactionAnimationDuration];
	stackLayer.sublayerTransform = sublayerTransform;
	self.layoutType = newType;
	[stackLayer setNeedsLayout];
	
}

- (void)setShowReflections:(BOOL)flag
{
	showReflections = flag;
	
	for (StackImageMainLayer *layer in [stackLayer sublayers])
		layer.showReflections = flag;
	
	[stackLayer setNeedsLayout];
	
}


#pragma mark -
#pragma mark View resizing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// View resizing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)viewWillStartLiveResize
{
	[super viewWillStartLiveResize];
	
	
}


- (void)viewDidEndLiveResize
{
	[super viewDidEndLiveResize];
	
	// adjust the slices
	[self updateBezierPathLayers];
	[self updateSlicePointsIfNeeded];
	[self refreshSliceLayer];
	
	
}


#pragma mark -
#pragma mark Drawing and custom layer drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing and custom layer drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (CGImageRef)imageWithGradient:(NSGradient *)gradient
{
	// returns a gradient image.  Needs to be released by caller.
	// thank you CovertFlow!  Unneccessary tedious context creating calls....
	CGRect rect;
	rect.origin = CGPointZero;
	rect.size = CGSizeMake(10,1000);
	size_t bytesPerRow = 4 * rect.size.width;
	void *bitmapData = malloc(bytesPerRow * rect.size.height);
	CGContextRef context = CGBitmapContextCreate(bitmapData, rect.size.width,
												 rect.size.height, 8,  bytesPerRow, 
												 CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedFirst);
	NSGraphicsContext *nsContext = [NSGraphicsContext graphicsContextWithGraphicsPort:context flipped:YES];
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:nsContext];
	[gradient drawInRect:NSMakeRect(0, 0, rect.size.width, rect.size.height) angle:90];
	[NSGraphicsContext restoreGraphicsState];
	CGImageRef img = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
	free(bitmapData);
	
	return img;
}



- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
	if (layer == slicePreviewLayer) {
		
		[self composeSliceInContext:ctx];
		
	} 
	
	else if ([layer.name isEqualToString:@"toolLayer"]) {
		
		if (!sliceModeOn) return;
		
		if (!((StackImageMainLayer *)layer.superlayer).contentsVisible) return;
		
		// need to offset the points by the layer offset...
		NSPoint offset = ((StackImageMainLayer *)layer.superlayer).layerOffset;
		offset.x *= layer.superlayer.frame.size.width;
		offset.y *= layer.superlayer.frame.size.height;
		
		NSPoint sliceStartPointWithOffset = sliceStartPoint;
		NSPoint sliceEndPointWithOffset = sliceEndPoint;
		
		sliceStartPointWithOffset.x -= offset.x;
		sliceStartPointWithOffset.y -= offset.y;
		sliceEndPointWithOffset.x -= offset.x;
		sliceEndPointWithOffset.y -= offset.y;
		
		// draw in the context
		NSGraphicsContext *nsGraphicsContext = [NSGraphicsContext graphicsContextWithGraphicsPort:ctx flipped:NO];
		[NSGraphicsContext saveGraphicsState];
		[NSGraphicsContext setCurrentContext:nsGraphicsContext];
		
		[self drawDotEndedLineFromPoint:sliceStartPointWithOffset toPoint:sliceEndPointWithOffset];
		
		[NSGraphicsContext restoreGraphicsState];
		
		
		
	}
}




- (BOOL)isOpaque
{
	return YES;
}


#pragma mark -
#pragma mark Slice + slice tools
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Slice + slice tools
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)drawDotEndedLineFromPoint:(NSPoint)first toPoint:(NSPoint)second
{
	NSBezierPath *linePath= [NSBezierPath bezierPath];
	[linePath moveToPoint:first];
	[linePath lineToPoint:second];
	[linePath setLineWidth:2.0];
	
	NSBezierPath *dots = [NSBezierPath bezierPath];
	[dots appendBezierPathWithOvalInRect:NSMakeRect(first.x-4,first.y-4,8,8)];
	[dots appendBezierPathWithOvalInRect:NSMakeRect(second.x-4,second.y-4,8,8)];
	
	
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	NSColor *strokeColor = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"sliceToolColor"]];
	
	[strokeColor set];
	[linePath stroke];
	
	[[NSColor colorWithCalibratedRed:1.0 green:1.0 blue:1.0 alpha:1.0] set];
	[dots setLineWidth:3.0];
	[dots stroke];
	[[NSColor colorWithCalibratedRed:0.3 green:0.3 blue:0.3 alpha:1.0] set];
	[dots fill];
	
	
}

- (void)updateSlicePointsIfNeeded
{
	// will check if the slice points are inside the frame of every image layer in the stack, if not, it will choose the best available points to replace them.
	// the slice point globals are already in the right coordinate space.
	
	//NSLog(@"slice points: %f %f    %f %f", sliceStartPoint.x, sliceStartPoint.y, sliceEndPoint.x, sliceEndPoint.y);
	
	// construct the union rectangle of all stack images.
	NSArray *imgLayers = [stackLayer sublayers];
	if (!imgLayers) return;

	// find a layer that is visible (--> it has bounds != 0)
	StackImageMainLayer *anyVisibleLayer = nil;
	int i = 0;
	while (i != [imgLayers count] && !anyVisibleLayer) {
		if (((StackImageMainLayer *)[imgLayers objectAtIndex:i]).bounds.size.width != 0)
			anyVisibleLayer = [imgLayers objectAtIndex:i];
		i++;
	}
	
	if (!anyVisibleLayer) return;
	
	CGRect unionRect = anyVisibleLayer.globalBounds;
	
	if (unionRect.size.width == 0 || unionRect.size.height == 0) return;
	
	for (StackImageMainLayer *layer in imgLayers) {
		if (layer.bounds.size.width != 0)
			unionRect = CGRectIntersection(unionRect, layer.globalBounds);
		//NSLog(@"layer bounds %3.3f %3.3f    %3.3f %3.3f - union %3.3f %3.3f    %3.3f %3.3f", 
		//	  layer.globalBounds.origin.x, layer.globalBounds.origin.y, layer.globalBounds.size.width, layer.globalBounds.size.height,
		//	  unionRect.origin.x, unionRect.origin.y, unionRect.size.width, unionRect.size.height);
		
	}
	
	// the size constraints cannot be fulfilled 
	if (unionRect.size.width == 0 || unionRect.size.height == 0) return;
	
	// if both slice points are in this rect, return.
	if (CGRectContainsPoint(unionRect, NSPointToCGPoint(sliceStartPoint)) && CGRectContainsPoint(unionRect, NSPointToCGPoint(sliceEndPoint)))
		return;
	
	// if none of the points are in this rect, set default fallback points.
	if (!CGRectContainsPoint(unionRect, NSPointToCGPoint(sliceStartPoint)) && !CGRectContainsPoint(unionRect, NSPointToCGPoint(sliceEndPoint)) ) {
		
		NSLog(@"none of the slice points located in union rect");
		
		sliceStartPoint.x = 0.2 * CGRectGetMinX(unionRect) + 0.8 * CGRectGetMaxX(unionRect);
		sliceStartPoint.y = 0.2 * CGRectGetMinY(unionRect) + 0.8 * CGRectGetMaxY(unionRect);
		
		sliceEndPoint.x = 0.8 * CGRectGetMinX(unionRect) + 0.2 * CGRectGetMaxX(unionRect);
		sliceEndPoint.y = 0.8 * CGRectGetMinY(unionRect) + 0.2 * CGRectGetMaxY(unionRect);
		
		Stack *selectedStack = [[stackArrayController selectedObjects] objectAtIndex:0];
		
		if (![selectedStack isDeleted]) {
			
			[selectedStack setValue:[NSNumber numberWithFloat:sliceStartPoint.x] forKey:@"sliceStartX"];
			[selectedStack setValue:[NSNumber numberWithFloat:sliceStartPoint.y] forKey:@"sliceStartY"];
			[selectedStack setValue:[NSNumber numberWithFloat:sliceEndPoint.x] forKey:@"sliceEndX"];
			[selectedStack setValue:[NSNumber numberWithFloat:sliceEndPoint.y] forKey:@"sliceEndY"];
			
		}
		
		// update ALL layers
		[self updateBezierPathLayers];
		
		return;
		
	}
	
	//NSLog(@"start: %@, end: %@", NSStringFromPoint(sliceStartPoint), NSStringFromPoint(sliceEndPoint));
	//NSLog(@"union rect %f %f    %f %f", unionRect.origin.x, unionRect.origin.y, unionRect.size.width, unionRect.size.height);
	
	// else, restore the point that is invalid, or both if they are both invalid.
	CGFloat slope;
	BOOL xSlope;		// we use a y-slope if the x-slope is too high ( almost vertical line)
	
	if (((sliceStartPoint.y - sliceEndPoint.y) / (sliceStartPoint.x - sliceEndPoint.x)) < 5) {
		
		xSlope = YES;
		slope = ((sliceStartPoint.y - sliceEndPoint.y) / (sliceStartPoint.x - sliceEndPoint.x));
		
	} else {
		
		xSlope = NO;
		slope = ((sliceStartPoint.x - sliceEndPoint.x) / (sliceStartPoint.y - sliceEndPoint.y));
		
	}
	
	NSInteger nbIterations = 0;
	
	while (!CGRectContainsPoint(unionRect, NSPointToCGPoint(sliceStartPoint))) {
		if (sliceStartPoint.x < CGRectGetMinX(unionRect) || (sliceStartPoint.y < CGRectGetMinY(unionRect) && slope > 0) || (sliceStartPoint.y > CGRectGetMaxY(unionRect) && slope < 0)) {
			
			sliceStartPoint.x += xSlope ? 1 : slope;
			sliceStartPoint.y += xSlope ? slope : 1;
			
			
		} else {
			
			sliceStartPoint.x -= xSlope ? 1 : slope;
			sliceStartPoint.y -= xSlope ? slope : 1;
			
		}
		
		nbIterations++;
		
		if (nbIterations > 3000) {
			NSLog(@"Slice points break");
			break;
		}
		
	}
	
	nbIterations = 0;
	
	while (!CGRectContainsPoint(unionRect, NSPointToCGPoint(sliceEndPoint))) {
		if (sliceEndPoint.x < CGRectGetMinX(unionRect) || (sliceEndPoint.y < CGRectGetMinY(unionRect) && slope > 0) || (sliceEndPoint.y > CGRectGetMaxY(unionRect) && slope < 0)) {
			
			sliceEndPoint.x += xSlope ? 1 : slope;
			sliceEndPoint.y += xSlope ? slope : 1;
			
		} else {
			
			sliceEndPoint.x -= xSlope ? 1 : slope;
			sliceEndPoint.y -= xSlope ? slope : 1;
			
		}
		
		nbIterations++;
		
		if (nbIterations > 3000) {
			NSLog(@"Slice points break");
			break;
		}
	}
	
	// update ALL layers
	[self updateBezierPathLayers];
	
	
	// update model
	Stack *selectedStack = [[stackArrayController selectedObjects] objectAtIndex:0];
	
	if (![selectedStack isDeleted]) {
		
		[selectedStack setValue:[NSNumber numberWithFloat:sliceStartPoint.x] forKey:@"sliceStartX"];
		[selectedStack setValue:[NSNumber numberWithFloat:sliceStartPoint.y] forKey:@"sliceStartY"];
		[selectedStack setValue:[NSNumber numberWithFloat:sliceEndPoint.x] forKey:@"sliceEndX"];
		[selectedStack setValue:[NSNumber numberWithFloat:sliceEndPoint.y] forKey:@"sliceEndY"];
		
	}

	
	
}


- (NSSize)dimensionsForCurrentSlice
{
	void *pixels = NULL;
	NSInteger count;
	[(StackImageMainLayer *)[[stackLayer sublayers] objectAtIndex:0] slicePixels:&pixels count:&count fromPoint:sliceStartPoint toPoint:sliceEndPoint];
	
	if (pixels)
		free(pixels);
	
	NSSize size;
	size.height = [[stackLayer sublayers] count];
	size.width = count;
	
	return size;
}

- (void)composeSliceInContext:(CGContextRef)ctx
{
	// will composite an image into a cgcontext using the slices from the given layers, with the first slice at the bottom of the image.
	NSSize dimensions = [self dimensionsForCurrentSlice];
	
	
	NSInteger w = ceilf(dimensions.width);
	NSInteger h = ceilf(dimensions.height);
	//NSInteger bpr = 4 * w;
	
	// return if invalid dimensions
	if (w == 0 || h == 0)
		return;
	
	// return if no images
	if ([[stackLayer sublayers] count] == 0) 
        return;
    
    
	void *memData = malloc(4 * h * w);
	
	// create a context for this composite image.
	CGContextRef compositeContext = CGBitmapContextCreate(memData, w, h, 8, 4 * w, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	
	
	unsigned char *dst = (unsigned char *)CGBitmapContextGetData(compositeContext);
	
	
	
	NSArray *sublayers = [stackLayer sortedSublayers];
	
	
	//NSLog(@"---------- START -------------");

    NSInteger x = 0;
	for (StackImageMainLayer *layer in [sublayers reverseObjectEnumerator]) {
		
		unsigned char *src;
		NSInteger srcW;
		[(StackImageMainLayer *)layer slicePixels:(void **)&src count:&srcW fromPoint:sliceStartPoint toPoint:sliceEndPoint];
		
		BOOL fillTransparent = (src == NULL);
		
		// make sure we don't go out of bounds with a larger image
		srcW = fmin(srcW, w);
				
		for (int y = 0; y < srcW; y++) {
			
			unsigned char *dstPix = &dst[4 * (w * x + y)];
			unsigned char *srcPix = &src[4 * y];
			
			if (fillTransparent) {
				
                dstPix[0] = dstPix[1] = dstPix[2] = dstPix[3] = 0;

				
			} else {
                
                dstPix[0] = srcPix[0];
                dstPix[1] = srcPix[1];
                dstPix[2] = srcPix[2];
                dstPix[3] = 255;
				
			}
			
		}
		
		free(src);
		
		x++;
	}
	
	
	//NSLog(@"---------- STOP -------------");

	
	CGImageRef composite = CGBitmapContextCreateImage(compositeContext);
    CGContextSetInterpolationQuality(ctx, kCGInterpolationNone);
	
	CGContextDrawImage(ctx, CGContextGetClipBoundingBox(ctx), composite);


	CGContextRelease(compositeContext);
	free(memData);
	CGImageRelease(composite);
}




- (void)threaded_composeFullResSlice
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	
	// get the highres dimensions
	NSInteger pixelsWide;
	NSInteger pixelsHigh = [[stackLayer sublayers] count];
	
	void *pixelData;
	
	[(StackImageMainLayer *)[[stackLayer sublayers] objectAtIndex:0] fullResSlicePixels:(void **)&pixelData count:&pixelsWide fromPoint:sliceStartPoint toPoint:sliceEndPoint];

	free(pixelData);

	NSInteger bytesPerRow = 4 * pixelsWide;
	
	// return if invalid dimensions
	if (pixelsWide == 0 || pixelsHigh == 0) {		
		return;
	}
	
	
	void *memData = malloc(bytesPerRow * pixelsHigh);
	
	// create a context for this composite image.
	CGContextRef compositeContext = CGBitmapContextCreate(memData, pixelsWide, pixelsHigh, 8, bytesPerRow, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	
	CGContextSetInterpolationQuality(compositeContext, kCGInterpolationNone);
	
	unsigned char *contextPixelData = (unsigned char *) CGBitmapContextGetData(compositeContext);
	
	NSInteger row = 0;
	
	
	NSArray *sublayers = [stackLayer sortedSublayers];
	
	for (StackImageMainLayer *layer in [sublayers reverseObjectEnumerator]) {
		
		unsigned char *pixelData;
		NSInteger count;
		[(StackImageMainLayer *)layer fullResSlicePixels:(void **)&pixelData count:&count fromPoint:sliceStartPoint toPoint:sliceEndPoint];
		
		BOOL fillTransparent = (pixelData == NULL);
		
		NSInteger column = 0;
		while (column != count) {
			
			unsigned char *currentContextPixel = contextPixelData + 4 * (pixelsWide * row + column);
			unsigned char *currentLayerPixel = pixelData + 4 * column;
			
			if (fillTransparent) {
				
				*currentContextPixel = 0;
				*(currentContextPixel + 1) = 0;
				*(currentContextPixel + 2) = 0;
				*(currentContextPixel + 3) = 0;
				
			} else {
				
				*currentContextPixel = *currentLayerPixel;
				*(currentContextPixel + 1) = *(currentLayerPixel + 1);
				*(currentContextPixel + 2) = *(currentLayerPixel + 2);
				*(currentContextPixel + 3) = 255;
				
			}
			
			column ++;
		}
		
		free(pixelData);
		
		row++;
	}
	
	if (fullResSlice)
		CGImageRelease(fullResSlice);
	
	fullResSlice = CGBitmapContextCreateImage(compositeContext);
	
	CGContextRelease(compositeContext);
	free(memData);

	[self performSelectorOnMainThread:@selector(sliceFullResCompositionDidEnd) withObject:nil waitUntilDone:NO];
	
	for (StackImageMainLayer *layer in [sublayers reverseObjectEnumerator]) {

		[layer clearFullResSliceResources];
	}
	
	[pool release];
}





- (void)updateBezierPathLayers
{
	NSRect invalidRect;
	
	// account for the buttons at the end points!
	invalidRect.origin.x = fmin(sliceStartPoint.x, sliceEndPoint.x) - 6;
	invalidRect.origin.y = fmin(sliceStartPoint.y, sliceEndPoint.y) - 6;
	invalidRect.size.width = fabs(sliceStartPoint.x - sliceEndPoint.x) + 12;
	invalidRect.size.height = fabs(sliceStartPoint.y - sliceEndPoint.y) + 12;
	
	// only update visible layers
	for (StackImageMainLayer *sublayer in [stackLayer sublayers]) {
		
		if (sublayer.contentsVisible)
			[sublayer.toolLayer setNeedsDisplay];
		
	}
	
}


- (void)refreshSliceLayer
{
	// return if the slice image is not visible.
	if (!sliceModeOn) return;
	NSSize newPreviewSize = [self dimensionsForCurrentSlice];
	
	CGRect maxAvailableRect = CGRectInset([backgroundLayer bounds], 20, 20);
	maxAvailableRect.size.height = 200;
	
	CGRect constrainedNewRect = maxAvailableRect;
	
	// determine the restricting edge to fit the new preview size in the available space
	if (newPreviewSize.width/maxAvailableRect.size.width > newPreviewSize.height/maxAvailableRect.size.height) {
		
		// scale down to fit the width
		constrainedNewRect.size.height = newPreviewSize.height * maxAvailableRect.size.width/newPreviewSize.width;
		constrainedNewRect.size.width = maxAvailableRect.size.width;
		
		// center the slice in its max available space.
		constrainedNewRect.origin.y = (maxAvailableRect.size.height - constrainedNewRect.size.height)/2.0 + 20;
		
		
	} else {
		
		// scale down to fit the height
		constrainedNewRect.size.width = newPreviewSize.width * maxAvailableRect.size.height/newPreviewSize.height;
		constrainedNewRect.size.height = maxAvailableRect.size.height;
		
		// center the slice in its max available space.
		constrainedNewRect.origin.x = (maxAvailableRect.size.width - constrainedNewRect.size.width)/2.0 + 20;
		
	}
		
	//[CATransaction setValue:[NSNumber numberWithBool:YES] forKey:kCATransactionDisableActions];
	//slicePreviewLayer.frame = constrainedNewRect;
	
	[slicePreviewLayer setNeedsDisplay];

}

- (void)clearAllSliceResources;
{
	for (StackImageMainLayer *layer in [stackLayer sublayers]) {
		
		[layer clearSliceResources];
		
	}
	
	
}

#pragma mark -
#pragma mark Movie
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Movie
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)updateMovieDuration:(CGFloat)newDurationPerImage
{
	// change the duration, but no other properties, of the current movie, and update the view.
	[movie deleteSegment:QTMakeTimeRange(QTMakeTime(0.0, 0.0), [movie duration])];
	
	for (NSImage *img in movieImages)
		[movie addImage:img forDuration:QTMakeTime((long)newDurationPerImage, 1000.0) withAttributes:[NSDictionary dictionaryWithObjectsAndKeys: @"jpeg", QTAddImageCodecType, nil]];
	
	
	[movie setCurrentTime:QTMakeTime(0, 1.0)];
	
	[movieView play:self];
	
	
}

- (void)composeMovie
{
	if (!movie) {
		
		// Create an empty movie that writes to mutable data in memory
		NSError *error;
		
		movie = [[QTMovie alloc] initToWritableData:[NSMutableData data] error:&error];
		if (!movie) {
			[[NSAlert alertWithError:error] runModal];
			return;
		}
		
	}
	[self performSelectorInBackground:@selector(threaded_loadMovieImages) withObject:nil];
	
}

- (void)cleanupMovie
{
	// invoked when the movie is not needed anymore.
	[movie release];
	movie = nil;
	[movieView setMovie:nil];
	
	[movieImages release];
	movieImages = nil;
}


- (void)loadMovieImages
{
	[self performSelectorInBackground:@selector(threaded_loadMovieImages) withObject:nil];
}

- (void)threaded_loadMovieImages
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; 

	if (movieImages)
		[movieImages release];
	
	movieImages = [[NSMutableArray array] retain];
	
	for (StackImageMainLayer *layer in [self arrangedStackImagelayers]) {
		
		NSImage *image = [layer.stackImage valueForKeyPath:@"image.filteredImage"];
		
		[movieImages addObject:image];
				
	}
	
	[self performSelectorOnMainThread:@selector(loadMovieImagesDidEnd) withObject:nil waitUntilDone:NO];
	
	[pool release];
}


- (void)loadMovieImagesDidEnd
{
	QTTime defaultDuration = QTMakeTime([[[movieSlideDurationPopUp selectedItem] title] floatValue], 1000.0);

	
	// assigning the images must be done on main; otherwise crash.
	for (NSImage *image in movieImages) {
						
		[movie addImage:image forDuration:defaultDuration withAttributes:[NSDictionary dictionaryWithObjectsAndKeys: @"jpeg", QTAddImageCodecType, nil]];
		
	}
	
	// set the movie as the movieViews movie	
	[movieView setMovie:movie];
	[movieView setNeedsDisplay:YES];
	
	
	// set the controls to disabled while the data is loaded asynchronously
	[movieSaveButton setEnabled:YES];
	[movieCancelButton setEnabled:YES];
	[movieIndicator setHidden:YES];
	
	[movieView setHidden:NO];
	[movieDurationView setHidden:NO];
	[movieLoadingMessage setHidden:YES];
	[movieIndicator setHidden:YES];
	[movieIndicator stopAnimation:self];
		
	
}

- (NSSize)windowWillResize:(NSWindow *)sender toSize:(NSSize)frameSize
{
	if (sender != movieWindow)
		return frameSize;
	
	
	return [self windowSizeWithOldSize:frameSize];
}

// to preserve aspect ratio for the movie window
- (NSSize)windowSizeWithOldSize:(NSSize)oldSize
{
	if (movieAspectRatio == -1)
		return oldSize;
	
	// the height is more important than the width (because it is more likely to go offscreen)
	CGFloat otherWindowContentHeight = [movieWindow frame].size.height - [movieView frame].size.height + [movieView movieControllerBounds].size.height;
	CGFloat otherWindowContentWidth = [movieWindow frame].size.width - [movieView frame].size.width;
	
	CGFloat newImageWidth = oldSize.width - otherWindowContentWidth;
	
	CGFloat newImageHeight = newImageWidth * movieAspectRatio;
	
	NSSize newWindowSize = oldSize;
	newWindowSize.height = newImageHeight + otherWindowContentHeight;
	
	
	return newWindowSize;
	
	
}




#pragma mark -
#pragma mark EFI
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EFI
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (IBAction)showEFISheet
{
	if (!efImager) {
		efImager = [[MGEFImager alloc] init];
	}
	
	efImager.stackName = [[[stackArrayController selectedObjects] objectAtIndex:0] valueForKey:@"name"];
	
	NSSortDescriptor *sortDescDate = [[[NSSortDescriptor alloc] initWithKey:@"image.creationDate" ascending:YES] autorelease];

	NSArray *images = [stackImageArrayController arrangedObjects];
	images = [images sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescDate]];
	
	
	[efImager showEFISheetForImages:images];
}


@end
