//
//  NSImage_MGAdditions.m
//  LightTable
//
//  Created by Dennis Lorson on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "NSImage_MGAdditions.h"


@implementation NSImage (MGAdditions)

- (CGImageRef)cgImage
{
	NSBitmapImageRep *bitmap = [[self representations] objectAtIndex:0];
	return [bitmap CGImage];
}

- (NSSize)bestRepresentationSize
{
	if ([[self representations] count] < 1) return NSZeroSize;

	
	// returns the size (pixelsWide/High) of the best rep of this image.  These values are far more reliable and are not scaled to the device, etc.
	NSImageRep *imageRep = [[self representations] objectAtIndex:0];
	NSSize repSize = NSMakeSize([imageRep pixelsWide], [imageRep pixelsHigh]);
	
	return repSize;
}

- (NSImageRep *)representation
{
	// returns the first rep in this image (in our cases: almost always the best and only one)
	if ([[self representations] count] < 1) return nil;
	
	return [[self representations] objectAtIndex:0];
}

- (void)setSizeIgnoringDPI
{
	if ([[self representations] count] == 0) return;
	
	NSImageRep *myRep = [[self representations] objectAtIndex:0];
	
    // note that these aren't necessarily identical.
    float hres = [myRep pixelsWide]/[self size].width;
    float vres = [myRep pixelsHigh]/[self size].height;
	
	// If the resolution isn't 1.0 (i.e. 72dpi) we must resize the 
	//image to its pixel dimensions
	// to get the desired size
    if (hres != 1.0 || vres != 1.0)
    {		
		[self setSize:NSMakeSize((float)[myRep pixelsWide], (float)[myRep pixelsHigh])];
    }
	
	
}

@end
