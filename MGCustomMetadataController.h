//
//  CustomMetadataController.h
//  Filament
//
//  Created by Dennis Lorson on 24/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MGCustomMetadataController;


@protocol MGCustomMetadataObserver <NSObject>

- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)controller;

@end




@interface MGCustomMetadataController : NSObject
{
	NSMutableArray *observers_;
}


+ (MGCustomMetadataController *)sharedController;


- (void)addObserver:(id <MGCustomMetadataObserver>)observer;
- (void)removeObserver:(id <MGCustomMetadataObserver>)observer;

- (NSArray *)customAttributes;

- (void)changeAttributeWithName:(NSString *)old to:(NSString *)new;
- (void)addAttributeWithName:(NSString *)name;
- (void)removeAttributeWithPath:(NSString *)path;

- (BOOL)isUniqueAttribute:(NSString *)name;
- (NSString *)attributePathWithName:(NSString *)name;
- (NSString *)attributeNameWithPath:(NSString *)path;

@end
