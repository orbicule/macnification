//
//  IPTCController.h
//  IPTC Controller
//
//  Created by Peter Schols on 02/06/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>




@interface IPTCController : NSObject {

}

//- (IBAction)getImageInfo:(id)sender;


// Get and set IPTC tags
- (NSDictionary *)IPTCTagsForImageAtPath:(NSString *)imagePath;
- (void)setIPTCTags:(NSDictionary *)iptcDictionary forImageAtPath:(NSString *)imagePath;
- (void)setString:(NSString *)aString forTag:(NSString *)aTag atPath:(NSString *)imagePath;


// Get and set the keywords
- (NSArray *)IPTCKeywordsForImageAtPath:(NSString *)imagePath;
- (void)setIPTCKeywords:(NSArray *)arrayOfKeywords forImageAtPath:(NSString *)imagePath;






@end
