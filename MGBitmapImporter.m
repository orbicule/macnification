//
//  MGBitmapImporter.m
//  Filament
//
//  Created by Dennis Lorson on 12/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MGBitmapImporter.h"
#import "MGImageFormats.h"
#import "NSBitmapImageRep_Conversion.h"

#import <vImage/vImage.h>


@implementation MGBitmapImporter

void MGFactor(int prod, int *factor1, int *factor2)
{
    *factor1 = prod; *factor2 = 1;
    
    for (int i = 2; i < prod; i++) {
        
        while (*factor1 % i == 0) {
            *factor1 = *factor1 / i;
            *factor2 = *factor2 * i;
        }
    }
}

void MGByteSwap(unsigned char *src, unsigned char *dst, size_t dataLength, size_t typeSize) 
{
    if (typeSize == 1) 
        return;
    
    if (!src || !dst)
        return;
    
    assert(typeSize == 2 || typeSize == 4);
    
    int vImageLength = dataLength / 4;
    int remainingLength = dataLength % 4;

    int w, h;
    MGFactor(vImageLength, &w, &h);
    
        
    vImage_Buffer srcBuff, dstBuff;
    srcBuff.data = src;
    srcBuff.height = h;
    srcBuff.width = w;
    srcBuff.rowBytes = w * 4;
    
    dstBuff = srcBuff;
    dstBuff.data = dst;
    
    uint8_t map[4];
    
    if (typeSize == 2) {
        map[0] = 1; map[1] = 0; map[2] = 3; map[3] = 2;
    }
    else {
        map[0] = 3; map[1] = 2; map[2] = 1; map[3] = 0;

    }
    
    
    
    vImagePermuteChannels_ARGB8888(&srcBuff, &dstBuff, map, 0);
    
    // swap the remaining stuff (at most 2 bytes)
    if (remainingLength > 0) {
        assert(typeSize == 2);
        unsigned char tmp = dst[dataLength - 2];
        dst[dataLength - 2] = dst[dataLength - 1];
        dst[dataLength - 1] = tmp;
    }
    
    
}


+ (NSBitmapImageRep *)importRepWithMetadata:(NSDictionary *)metadata
{
	int fileType = [[metadata objectForKey:@"fileType"] intValue];
	
	//NSLog(@"Image %@:", [metadata objectForKey:@"filePath"]);
	
	NSBitmapImageRep *rep = nil;
	
	switch (fileType) {
			
		case MG_IMAGE_GRAY8:
			NSLog(@"MG_IMAGE_GRAY8");
			break;
		case MG_IMAGE_BITMAP:
			NSLog(@"MG_IMAGE_BITMAP");
			break;

			
		case MG_IMAGE_GRAY16_SIGNED:
			rep = [[self class] readGray16bitImageWithMetadata:metadata];
			break;
		case MG_IMAGE_GRAY16_UNSIGNED:
			rep = [[self class] readGray16bitImageWithMetadata:metadata];
			break;

			
		case MG_IMAGE_GRAY32_INT:
			rep = [[self class] readGray32bitImageWithMetadata:metadata];
			break;
		case MG_IMAGE_GRAY32_UNSIGNED:
			rep = [[self class] readGray32bitImageWithMetadata:metadata];
			break;
		case MG_IMAGE_GRAY32_FLOAT:
			rep = [[self class] readGray32bitImageWithMetadata:metadata];
			break;
			
			
		case MG_IMAGE_RGB:
			rep = [[self class] readComponentImageWithMetadata:metadata];
			break;
		case MG_IMAGE_ARGB:
			rep = [[self class] readComponentImageWithMetadata:metadata];
			break;
	}
	
	return rep;
}

+ (NSBitmapImageRep *)readGray32bitImageWithMetadata:(NSDictionary *)metadata
{
	
	NSString *path = [metadata objectForKey:@"filePath"];
	const char *cfilename = [[path stringByExpandingTildeInPath] cStringUsingEncoding:NSISOLatin1StringEncoding];
	FILE *file = fopen(cfilename, "r");	
	
	long offset = [[metadata objectForKey:@"imageOffset"] longValue];
	int width = [[metadata objectForKey:@"imageWidth"] longValue];
	int height = [[metadata objectForKey:@"imageHeight"] longValue];
	int fileType = [[metadata objectForKey:@"fileType"] intValue];
	BOOL littleEndian = [[metadata objectForKey:@"littleEndian"] boolValue];
	long minVal = [[metadata objectForKey:@"imageBrightnessRangeMin"] longValue];
	long maxVal = [[metadata objectForKey:@"imageBrightnessRangeMax"] longValue];
	
	//NSLog(@"width %i  height %i  fileType %i  littleEndian: %i", path, width, height, fileType, littleEndian);
	
	
	// compute the buffer size
	int byteCount = 4 * width * height;
	
	unsigned char *buf = (unsigned char *)malloc(byteCount * sizeof(unsigned char));
	unsigned char *leBuff = (unsigned char *)malloc(byteCount * sizeof(unsigned char));
	
    // read
	fseek(file, offset, SEEK_SET);
	fread(buf, sizeof(unsigned char), byteCount, file);
    
    
    // Byte swap if needed
    if (!littleEndian)
        MGByteSwap(buf, leBuff, byteCount, 4);
    else
        memcpy(leBuff, buf, byteCount);
    
    
    free(buf);

	
	// create new buffer to hold the 32 bit image
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																	 pixelsWide:width
																	 pixelsHigh:height
																  bitsPerSample:16
																samplesPerPixel:1
																	   hasAlpha:NO
																	   isPlanar:YES
																 colorSpaceName:NSCalibratedWhiteColorSpace 
                                                                   bitmapFormat:0
																	bytesPerRow:2*width
																   bitsPerPixel:16] autorelease];
    
    
    unsigned char *dstBuf = [rep bitmapData];
    

    
    
    
	
    if (fileType == MG_IMAGE_GRAY32_INT) {
        
        double range = (double)(maxVal - minVal);
        int i;
        for (i = 0; i < width * height; i++) {
            
            int *data = (int *)&leBuff[i*4];
            int offset = (float)((*data - minVal)/range) * UINT16_MAX;
            
            // clamp, because the range may not be covering all of the pixels
            offset = MAX(MIN(UINT16_MAX, offset), 0);
            
            
            dstBuf[2*i] = ((unsigned char *)&offset)[0];
            dstBuf[2*i + 1] = ((unsigned char *)&offset)[1];
            
        }
        
    }
    
    else if (fileType == MG_IMAGE_GRAY32_FLOAT) {
        
        double range = (double)(maxVal - minVal);
        int i;
        for (i = 0; i < width * height; i++) {
            
            float *data = (float *)&leBuff[i*4];
            
            int offset = (float)((*data - minVal)/range) * UINT16_MAX;
            
            // clamp, because the range may not be covering all of the pixels
            offset = MAX(MIN(UINT16_MAX, offset), 0);
            
            dstBuf[2*i] = ((unsigned char *)&offset)[0];
            dstBuf[2*i + 1] = ((unsigned char *)&offset)[1];            
        }
        
    }
    
    
    else {
        
        NSLog(@"MGBitmapImporter: 32-bit format not yet supported!");
    }
    
	
    
    free(leBuff);
	
	//[[rep TIFFRepresentation] writeToFile:[NSHomeDirectory() stringByAppendingFormat:@"/Desktop/%@.tiff", [path lastPathComponent]] atomically:YES];
	
	return rep;
	
	
	
}


+ (NSBitmapImageRep *)readGray16bitImageWithMetadata:(NSDictionary *)metadata
{
	
	NSString *path = [metadata objectForKey:@"filePath"];
	const char *cfilename = [[path stringByExpandingTildeInPath] cStringUsingEncoding:NSISOLatin1StringEncoding];
	FILE *file = fopen(cfilename, "r");	
	
	long offset = [[metadata objectForKey:@"imageOffset"] longValue];
	int width = [[metadata objectForKey:@"imageWidth"] longValue];
	int height = [[metadata objectForKey:@"imageHeight"] longValue];
	int fileType = [[metadata objectForKey:@"fileType"] intValue];
	BOOL littleEndian = [[metadata objectForKey:@"littleEndian"] boolValue];
	long minVal = [[metadata objectForKey:@"imageBrightnessRangeMin"] longValue];
	long maxVal = [[metadata objectForKey:@"imageBrightnessRangeMax"] longValue];
		
	
	// compute the buffer size
	int byteCount = 2 * width * height;
	
	unsigned char *buf = (unsigned char *)malloc(byteCount * sizeof(unsigned char));
	
	fseek(file, offset, SEEK_SET);
	fread(buf, sizeof(unsigned char), byteCount, file);
	
    
	unsigned char *leBuf = (unsigned char *)malloc(byteCount * sizeof(unsigned char));
    
    // Byte swap if needed
    if (!littleEndian)
        MGByteSwap(buf, leBuf, byteCount, 4);
    else
        memcpy(leBuf, buf, byteCount);
    
    
	
	// create the CG buffer
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:&leBuf
																	 pixelsWide:width
																	 pixelsHigh:height
																  bitsPerSample:16
																samplesPerPixel:1
																	   hasAlpha:NO
																	   isPlanar:YES
																 colorSpaceName:NSCalibratedWhiteColorSpace 
																	bytesPerRow:2*width
																   bitsPerPixel:16] autorelease];
	
	unsigned char *bmpData = [rep bitmapData];
	
	
	// if the data is signed, we need to convert it to unsigned data
    if (fileType == MG_IMAGE_GRAY16_SIGNED) {
        
        for (int i = 0; i < width * height; i++) {
            
            int16_t *val = &((int16_t *)bmpData)[i];
            
            int val_int = *val;
            val_int += 32768;
            
            ((uint16_t *)bmpData)[i] = val_int;
        }
    }
	
	
	//int i;
	
	
	
	if (maxVal == 0 && minVal == 0)
		rep = [rep imageRepWithScaledRange];
	else if (fileType == MG_IMAGE_GRAY16_SIGNED)
		rep = [rep imageRepWithScaledRangeWithMin:minVal + 32768 max:maxVal + 32768];
    else
        rep = [rep imageRepWithScaledRangeWithMin:minVal max:maxVal];

	
	free(buf);
	free(leBuf);
	
	return rep;
}



+ (NSBitmapImageRep *)readComponentImageWithMetadata:(NSDictionary *)metadata
{
	// used for BARG, ARGB, RGB, BGR, RGB_PLANAR
	// use the standard AppKit methods but reshuffle the bytes if necessary
	NSString *path = [metadata objectForKey:@"filePath"];
	const char *cfilename = [[path stringByExpandingTildeInPath] cStringUsingEncoding:NSISOLatin1StringEncoding];
	FILE *file = fopen(cfilename, "r");	
	
	long offset = [[metadata objectForKey:@"imageOffset"] longValue];
	int width = [[metadata objectForKey:@"imageWidth"] longValue];
	int height = [[metadata objectForKey:@"imageHeight"] longValue];
	int fileType = [[metadata objectForKey:@"fileType"] intValue];
	BOOL littleEndian = [[metadata objectForKey:@"littleEndian"] boolValue];
	
	//NSLog(@"Importing image %@\nwidth %i  height %i  fileType %i  littleEndian: %i", path, width, height, fileType, littleEndian);
	
	// bps always 8-bit per component
	int bps = 1;
	
	// spp dependent on the type
	int spp;
	if (fileType == MG_IMAGE_ARGB || fileType == MG_IMAGE_BARG)
		spp = 4;
	else
		spp = 3;
	
	
	// compute the buffer size
	int byteCount = bps * spp * width * height;
	
	unsigned char *buf = (unsigned char *)calloc(byteCount, sizeof(unsigned char));
	
	fseek(file, offset, SEEK_SET);
	fread(buf, sizeof(unsigned char), byteCount, file);

	
	// change byte order
	if (littleEndian) {
		
		int i;
		
		if (spp == 3) {
			
			for (i = 0; i < width * height; i++) {
				int baseLoc = i*spp*bps;
				unsigned char tmp = buf[baseLoc];
				buf[baseLoc] = buf[baseLoc + 2*bps];
				buf[baseLoc + 2*bps] = tmp;
			}
			
		} else if (spp == 4) {
			
			for (i = 0; i < width * height; i++) {
				int baseLoc = i*spp*bps;
				unsigned char tmp = buf[baseLoc];
				buf[baseLoc] = buf[baseLoc + 3*bps];
				buf[baseLoc + 3*bps] = tmp;
				
				tmp = buf[baseLoc + 1 * bps];
				buf[baseLoc + 1 * bps] = buf[baseLoc + 2*bps];
				buf[baseLoc + 2*bps] = tmp;
			}
			
		}
		
	}
	
	
	
	// reshuffle in case of stupid byte orders
	if (fileType == MG_IMAGE_BGR) {
		int i;
		for (i = 0; i < width * height; i++) {
			int baseLoc = i*spp*bps;
			unsigned char tmp = buf[baseLoc];
			buf[baseLoc] = buf[baseLoc + 2*bps];
			buf[baseLoc + 2*bps] = tmp;
		}
		
	} else if (fileType == MG_IMAGE_BARG) {
		// make it ARGB
		int i;
		for (i = 0; i < width * height; i++) {
			int baseLoc = i*spp*bps;
			unsigned char tmp = buf[baseLoc];
			buf[baseLoc] = 0xff;	// ignore alpha (often 0)
			buf[baseLoc + 1*bps] = buf[baseLoc + 2*bps];
			buf[baseLoc + 2*bps] = buf[baseLoc + 3*bps];
			buf[baseLoc + 3*bps] = tmp;
		}
		
	} else if (fileType == MG_IMAGE_ARGB) {
		
		int i;
		for (i = 0; i < width * height; i++) {
			int baseLoc = i*spp*bps;
			buf[baseLoc] = 0xff;	// ignore alpha (often 0)
		}
	}
	
	
	NSBitmapImageRep *rep;
	// if the data is planar, we need create an array of pointers
	if (fileType == MG_IMAGE_RGB_PLANAR) {
	
		// copy the data over to the CG structure
		rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													   pixelsWide:width
													   pixelsHigh:height
													bitsPerSample:bps*8
												  samplesPerPixel:spp
														 hasAlpha:NO
														 isPlanar:YES
												   colorSpaceName:NSCalibratedRGBColorSpace 
													  bytesPerRow:bps*spp*width
													 bitsPerPixel:8*bps*spp] autorelease];
		
		// TODO: only supports RGB24
		unsigned char *planes[3];
		[rep getBitmapDataPlanes:planes];
		int i;
		for (i = 0; i < width * height; i++) {
			
			(planes[0])[i] = buf[i];
			(planes[1])[i] = buf[i + width*height];
			(planes[2])[i] = buf[i + width*height*2];
			
		}
		
		
	} else {
		
		
		BOOL hasAlpha = (fileType == MG_IMAGE_ARGB || fileType == MG_IMAGE_BARG);
		
		rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													   pixelsWide:width
													   pixelsHigh:height
													bitsPerSample:bps*8
												  samplesPerPixel:spp
														 hasAlpha:hasAlpha
														 isPlanar:NO
												   colorSpaceName:NSCalibratedRGBColorSpace 
													 bitmapFormat:(NSAlphaFirstBitmapFormat)
													  bytesPerRow:bps*spp*width
													 bitsPerPixel:8*bps*spp] autorelease];
				
		unsigned char *bitmapData = [rep bitmapData];
		
		int i;
		for (i = 0; i < byteCount; i++)
			bitmapData[i] = buf[i];
		
	}
	
	
	free(buf);
	return rep;
}



@end
