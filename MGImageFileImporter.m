//
//  MGImageFileImporter.m
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGImageFileImporter.h"

#import "MGMetadataSet.h"

#import "MGDM3FileImporter.h"
#import "MGFEIFileImporter.h"
#import "MGJEOLFileImporter.h"
#import "MGOMEFileImporter.h"
#import "MGOMECompatibleFileImporter.h"
#import "MGGenericImageFileImporter.h"
#import "MGImageFileImporter_Private.h"
#import "NSBitmapImageRep_Conversion.h"

@interface MGImageFileImporter ()


+ (NSArray *)_importerClassNames;
- (MGMetadataSet *)_generateMetadata;


@end


@implementation MGImageFileImporter


#pragma mark -
#pragma mark Choosing a file importer


+ (NSArray *)_importerClassNames;
{
	// Ordered based on specificness and cheapness to evaluate
	
	return [NSArray arrayWithObjects:
			@"MGFluoViewTIFFFileImporter",
			@"MGOMEFileImporter",
			@"MGOMECompatibleFileImporter",
			@"MGDM3FileImporter",
			@"MGFEIFileImporter",
			@"MGJEOLFileImporter",
			@"MGGenericImageFileImporter",
			nil];
}


+ (MGImageFileImporter *)fileImporterWithPath:(NSString *)filePath;
{
	for (NSString *className in [MGImageFileImporter _importerClassNames]) {
		if ([NSClassFromString(className) _canOpenFilePath:filePath]) { 
            
			return [[[NSClassFromString(className) alloc] initWithFilePath:filePath] autorelease];

		}
	}

    

	
	NSLog(@"No suitable importer found for file %@", filePath);
	
	return nil;
}

+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	return NO;
}


+ (NSArray *)supportedPathExtensions;
{
	NSMutableArray *extensions = [NSMutableArray array];
	
	for (NSString *className in [MGImageFileImporter _importerClassNames])
		[extensions addObjectsFromArray:[NSClassFromString(className) supportedPathExtensions]];
	
	return extensions;
}

+ (BOOL)supportsExtension:(NSString *)extension
{
	return [[self supportedPathExtensions] containsObject:[extension lowercaseString]];
}

- (NSArray *)subImporters;
{
	return nil;
}

- (MGImageFileImporter *)superImporter;
{
	return nil;
}

- (float)fractionOfImportTimeForNonRepresentationWork;
{
	return 0.0;
}

#pragma mark -
#pragma mark Init


- (id)initWithFilePath:(NSString *)filePath
{
	if ((self = [super init])) {
		filePath_ = [filePath retain];

        
        
	}
	
	return self;
}

- (void) dealloc
{
	[filePath_ release];
	[metadata_ release];
	
	[super dealloc];
}


#pragma mark -
#pragma mark Paths


- (NSString *)originalFilePath;
{
	return filePath_;
}

- (NSString *)representationFilePath;
{
	return filePath_;
}


#pragma mark -
#pragma mark Metadata


- (MGMetadataSet *)metadata;
{
	if (!metadata_)
		metadata_ = [[self _generateMetadata] retain];
		
	return metadata_;
}

- (MGMetadataSet *)_generateMetadata;
{
	MGMetadataSet *set = [[[MGMetadataSet alloc] init] autorelease];
	
	// Set some values that we can infer from the most generic file level
	// they will be replaced by subclasses if more info is available from the file format
	
    if ([[filePath_ lastPathComponent] stringByDeletingPathExtension])
        [set setValue:[[filePath_ lastPathComponent] stringByDeletingPathExtension] forKey:MGMetadataImageNameKey];
    
	[set setValue:[NSDate date] forKey:MGMetadataImageImportDateKey];
	
	NSDate *creationDate = [[[NSFileManager defaultManager] attributesOfItemAtPath:filePath_ error:nil] objectForKey:NSFileCreationDate];
	if (creationDate)
        [set setValue:creationDate forKey:MGMetadataImageCreationDateKey];
	
	return set;

}

#pragma mark -
#pragma mark Representations


- (int)numberOfImageRepresentations;
{
	NSAssert(NO, @"No ImageFileImporter subclass found");
	return 0;
}

- (NSBitmapImageRep *)imageRepresentationAtIndex:(int)index;
{
	NSAssert(NO, @"No ImageFileImporter subclass found");
	return nil;
}


#pragma mark -
#pragma mark Info for copying the file/representations

- (NSString *)destinationPathExtension;
{
	return @"tiff";
}

- (BOOL)destinationNeedsCompression;
{
	return NO;
}

- (NSString *)compressedDestinationPathExtension;
{
    if (![self destinationNeedsCompression])
        return [self destinationPathExtension];
    
    return @"tiff";
}

- (NSString *)destinationName;
{
	return [[self metadata] valueForKey:MGMetadataImageNameKey];
}


#pragma mark -
#pragma mark Representation postprocessing

- (NSBitmapImageRep *)_postProcessRepresentationBitmapData:(NSBitmapImageRep *)rep
{
	
	if ([metadata_ valueForKey:MGMetadataImageLittleEndianKey]) {
		
		BOOL littleEndian = [[metadata_ valueForKey:MGMetadataImageLittleEndianKey] boolValue];
		
		rep = [rep imageRepWithHostEndiannessFromSourceEndianness:littleEndian ? NS_LittleEndian : NS_BigEndian];
	}
	
	if ([metadata_ valueForKey:MGMetadataImageIntensityRangeMinKey] && [metadata_ valueForKey:MGMetadataImageIntensityRangeMaxKey])
		rep = [rep imageRepWithScaledRangeWithMin:[[metadata_ valueForKey:MGMetadataImageIntensityRangeMinKey] longValue] 
											  max:[[metadata_ valueForKey:MGMetadataImageIntensityRangeMaxKey] longValue]];	
	else
		rep = [rep imageRepWithScaledRange];
	
	return rep;
}



@end
