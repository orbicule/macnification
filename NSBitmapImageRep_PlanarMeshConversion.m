//
//  NSBitmapImageRep_PlanarMeshConversion.m
//  Filament
//
//  Created by Dennis Lorson on 22/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "NSBitmapImageRep_PlanarMeshConversion.h"


@implementation NSBitmapImageRep (PlanarMeshConversion)


+ (NSBitmapImageRep *)imageRepWithMergedChannels:(NSArray *)channels;
{

	// check if we have at least one valid channel
	NSInteger nbChannels = 0;
	for (NSBitmapImageRep *rep in channels)
		nbChannels += ((id)rep != [NSNull null] ? 1 : 0);
	
	if (nbChannels == 0) {
		
		//NSLog(@"No channels found");
		return nil;
	}
	
	// check if all images have the same size
	BOOL firstChannelDimensionsSet = NO;
	NSInteger globalW, globalH;
	for (NSBitmapImageRep *rep in channels) {
		
		if ((id)rep != [NSNull null]) {
			
			if (!firstChannelDimensionsSet) {
				
				globalW = [rep pixelsWide];
				globalH = [rep pixelsHigh];
				firstChannelDimensionsSet = YES;
				
			} else {
				
				if (globalH != [rep pixelsHigh] || globalW != [rep pixelsWide]) {
					
					//NSLog(@"Not all image dimensions agree");
					return nil;
				}
			}
		}
	}
	
	// create the new empty image
	NSInteger mergedBps = 8;
	NSInteger mergedBpr = globalW * 3;
	NSInteger mergedSpp = 3;
	NSBitmapImageRep *merged = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		pixelsWide:globalW
																		pixelsHigh:globalH 
																	 bitsPerSample:mergedBps
																   samplesPerPixel:mergedSpp
																		  hasAlpha:NO
																		  isPlanar:NO
																	colorSpaceName:NSCalibratedRGBColorSpace 
																	  bitmapFormat:0
																	   bytesPerRow:mergedBpr 
																	  bitsPerPixel:24] autorelease];
	
	unsigned char *mergedData = [merged bitmapData];
	
	
	// make sure that we have all data per image in one plane, then merge it immediately into the destination image
	for (NSBitmapImageRep *rep in channels) {
		
		if ((id)rep != [NSNull null]) {
			
			NSInteger channelIndex = [channels indexOfObject:rep];
			
			NSInteger w = [rep pixelsWide];
			NSInteger h = [rep pixelsHigh];
			NSInteger bpr = [rep bytesPerRow];
			NSInteger spp = [rep samplesPerPixel];
			
			BOOL alphaFirst = ([rep bitmapFormat] & NSAlphaFirstBitmapFormat) && [rep hasAlpha];
			unsigned char *bitmapData = [rep bitmapData];
			
			int x, y;
			for (y = 0; y < h; y++) {
				for (x = 0; x < w; x++) {
					
					unsigned char *addr = &(bitmapData[y * bpr + x * spp + (alphaFirst ? 1 : 0)]);
					
					unsigned char val;
					
					if (spp != 1) {
						// compute the luminance
						val = (addr[0] + addr[1] + addr[2])/3;

					} else {
						
						val = addr[0];
						
					}
					
					mergedData[y * mergedBpr + x * mergedSpp + channelIndex] = val; 
					
				}
			}
		}
	}
	
	return merged;
}


- (NSArray *)splitChannels;
{
	unsigned char **planarData;
	NSInteger x, y, i;
	
	NSInteger nonZeroPixelsInChannel[5] = {0, 0, 0, 0, 0};
	
	NSInteger spp = [self samplesPerPixel];
	NSInteger bpr = [self bytesPerRow];
	NSInteger w = [self pixelsWide];
	NSInteger h = [self pixelsHigh];
	NSInteger bps = [self bitsPerSample];

	if ([self isPlanar]) {
		NSLog(@"isPlanar");
		// fill the planar data array with the channels that are given to us by the getDataPlanes method
		[self getBitmapDataPlanes:planarData];
		
	} else {
		
		unsigned char *bitmapData = [self bitmapData];
		
		planarData = calloc(5, sizeof(unsigned char *));
		
		// create the new channels
		for (i = 0; i < spp; i++) {
			planarData[i] = malloc(w * h * sizeof(unsigned char));
		}
				
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				for (i = 0; i < spp; i++) {
					if (bitmapData[y * bpr + x * spp + i] > 0 && nonZeroPixelsInChannel[i] < 10)
						nonZeroPixelsInChannel[i]++;
					planarData[i][y * w + x * 1] = bitmapData[y * bpr + x * spp + i];
					
				}
			}
		}
	}
	
	
	NSMutableArray *channelReps = [NSMutableArray array];
	
	for (i = 0; i < 5; i++) {
		
		if (planarData[i] != NULL) {
			
			// disabled this check; at least now we can be sure there will always be three outputs
			if (YES/*nonZeroPixelsInChannel[i] > 0*/) {
				
				NSBitmapImageRep *imageRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																					  pixelsWide:w
																					  pixelsHigh:h
																				   bitsPerSample:bps
																				 samplesPerPixel:1
																						hasAlpha:NO
																						isPlanar:YES
																				  colorSpaceName:NSCalibratedWhiteColorSpace
																					bitmapFormat:0
																					 bytesPerRow:w
																					bitsPerPixel:bps * 1] autorelease];
				
				
				
				unsigned char *newData = [imageRep bitmapData];
				
				for (y = 0; y < h; y++) {
					for (x = 0; x < w; x++) {
						
						newData[y * w + x * 1] = planarData[i][y * w + x * 1];
					}
				}
				
				[channelReps addObject:imageRep];
				
			}
		
			if (![self isPlanar])
				free(planarData[i]);
		}
		
	}
	
	free(planarData);
	
	
	if ([channelReps count] == 1)
		[channelReps removeLastObject];
	
	return channelReps;
	
}



@end
