//
//  CaptureDevice.m
//  Filament
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureDevice.h"
#import "CaptureQTDevice.h"
#import "CaptureICDevice.h"
#import "CapturePRDevice.h"
#import "CaptureDC1394Device.h"

NSString *CaptureAdjustmentGain = @"gain";
NSString *CaptureAdjustmentBrightness = @"brightness";
NSString *CaptureAdjustmentGamma = @"gamma";
NSString *CaptureAdjustmentSaturation = @"saturation";
NSString *CaptureAdjustmentExposure = @"exposure";

#define EXP_1_MINUS_1_INV 0.581976706869327


@implementation CaptureDevice

@synthesize delegate = delegate_;
@dynamic originalDevice;

#pragma mark -
#pragma mark Init

+ (CaptureDevice *)deviceWithQTDevice:(QTCaptureDevice *)originalDevice;
{
	CaptureQTDevice *device = [[[CaptureQTDevice alloc] initWithDevice:originalDevice] autorelease];
	return device;
}

+ (CaptureDevice *)deviceWithICDevice:(ICCameraDevice *)originalDevice;
{
	CaptureICDevice *device = [[[CaptureICDevice alloc] initWithDevice:(id)originalDevice] autorelease];
	return device;
}

+ (CaptureDevice *)deviceWithPRDeviceID:(pr_device_id)guid;
{
	return nil;
	//CapturePRDevice *device = [[[CapturePRDevice alloc] initWithDeviceID:guid] autorelease];
	//return device;
}

+ (CaptureDevice *)deviceWithDC1394DeviceID:(dc1394_device_id)guid controller:(dc1394_t *)ctl;
{
	return [[[CaptureDC1394Device alloc] initWithDeviceID:guid controller:ctl] autorelease];
}


- (id) init
{
	self = [super init];
	if (self != nil) {
        indexSuffix_ = -1;
	}
	return self;
}


- (void) dealloc
{
	self.delegate = nil;
	
	[super dealloc];
}

#pragma mark -
#pragma mark General info

- (NSString *)name;					{ return nil; }
- (BOOL)canTakePicture;				{ return NO; } 
- (BOOL)hasStorage;					{ return NO; }
- (BOOL)hasStoredItems;				{ return NO; }
- (NSInteger)numberOfStoredItems;	{ return 0; }
- (NSImage *)iconImage;				{ return nil; }
- (BOOL)isValid;                    { return YES; }

- (NSString *)uniqueName;
{
    if ([self indexSuffix] == -1)
        return [self name];
    
    return [NSString stringWithFormat:@"%@ (%i)", [self name], [self indexSuffix] + 1];
}

- (int)indexSuffix;
{
    return indexSuffix_;
}

- (void)setIndexSuffix:(int)suffix;
{
    indexSuffix_ = suffix;
}

- (NSString *)uidString;
{
    return [NSString stringWithFormat:@"%@:%@", NSStringFromClass([self class]), [self name]];
}

#pragma mark -
#pragma mark Actions

- (void)takePicture;
{
	NSSound *shutter = [[NSSound alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shutter" ofType:@"wav"] byReference:YES];
	[shutter setDelegate:self];
	[shutter setVolume:0.25];
	[shutter play];
	
}

- (void)sound:(NSSound *)sound didFinishPlaying:(BOOL)finishedPlaying
{
	[sound release];
}

- (void)downloadCurrentItem; {}
- (void)downloadAllItems; {}
- (void)cancelDownload; {}

#pragma mark -
#pragma mark Sessions

- (void)startSession; {}
- (void)stopSession; {}


#pragma mark -
#pragma mark Preview

- (void)connectPreviewToView:(NSView *)view; {}
- (void)disconnectPreviewFromView:(NSView *)view; {}
- (CapturePreviewType)previewType; { return CapturePreviewNone; }


#pragma mark -
#pragma mark Adjustments

- (CaptureAdjustmentRange)exposureRange { CaptureAdjustmentRange range = {0, 0, YES}; return range; }
- (void)resetExposure {}
- (CGFloat)absoluteExposure { return 0; }
- (CaptureExposureMode)exposureModes { return CaptureExposureModeNone; }
- (void)setExposureAbsolute:(CGFloat)exposure {}
- (void)setAutoExposure:(BOOL)autoExposure {}
- (BOOL)autoExposure { return NO; }

- (void)setExposure:(CGFloat)exposure 
{
    if (!([self exposureModes] & CaptureExposureModeManual))
        return;
    
    CaptureAdjustmentRange range = [self exposureRange];
    
    if (range.min == range.max)
        return;

    float mappedVal = exposure;
    
    // perform nonlinear compression if needed
    if (!range.linear) {
        mappedVal = powf(mappedVal, 3.0);
    }
    
    // then map to the desired range
    mappedVal = range.min + mappedVal * (range.max - range.min);
  
    [self setExposureAbsolute:mappedVal];

}

- (CGFloat)exposure
{
    CaptureAdjustmentRange range = [self exposureRange];
    
    CGFloat exp = [self absoluteExposure];
    
    float unmappedVal = exp/(range.max - range.min) - range.min;
    
    if (!range.linear)
        unmappedVal = powf(unmappedVal, 1.0/3.0);
    
    return unmappedVal;
}

- (NSString *)exposureDescription
{
    return [NSString stringWithFormat:@"%1.0f ms", [self absoluteExposure]];
}


@end
