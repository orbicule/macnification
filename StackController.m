//
//  StackController.m
//  Filament
//
//  Created by Peter Schols on 21/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "StackController.h"
#import "StackView.h"
#import "StackImageMainLayer.h"
#import "StackView_Layout.h"
#import "MGAppDelegate.h"
#import "ImageArrayController.h"
#import "MGFilterAndSortMetadataController.h"
#import "MGLibraryController.h"

@interface StackController ()



- (BOOL)hasNonDefaultSortOrderOrKey;
- (void)setSortKey:(NSString *)key;
- (void)setSortOrder:(BOOL)descendingOrAscending;
- (void)_refreshSortControls;
- (void)_refreshSortKeysPopup;
- (void)setSortControlsVisible:(BOOL)flag;

- (void)animateSortControlsAndBackButton;
- (void)_updateSearchFieldCategories;

@end




@implementation StackController


#pragma mark -
#pragma mark Init

- (id)initWithSuperview:(NSView *)superview;
{
	self = [super init];
	if(self)
	{
		// Load nib, using this object as the file's owner...
		
		if([NSBundle loadNibNamed:@"Stack" owner:self])
		{
			// By the time the nib is loaded, view's awakeFromNib will have been called and m_view outlet set in this object
			// Add m_view to superview
			[m_view setFrame:[superview frame]];
			[superview addSubview:m_view];
			
			
			
			
			[toggleMDButton setTarget:MGLibraryControllerInstance];
			[toggleMDButton setAction:@selector(toggleMdSplitView:)];
			
			[[MGCustomMetadataController sharedController] addObserver:self];
			[self _updateSearchFieldCategories];
			
			// Do whatever other initialization you need here
		}
		else
		{
			// Nib loading failed - this object will be invalid, autorelease & raise exception
			[self autorelease];
			self = nil;
			[NSException raise:@"MyViewController-init-exception" format:@" - could not load nib from bundle"];
		}
	}
	return self;
}

- (void)awakeFromNib
{	
	[[NSPasteboard pasteboardWithName:NSDragPboard] declareTypes:[NSArray arrayWithObject:@"StackImagesDragType"] owner:self];
	
	[self _refreshSortControls];
	
    [imageBrowser setDraggingDestinationDelegate:self];
	[imageBrowser setCellsStyleMask:IKCellsStyleTitled | IKCellsStyleSubtitled];
	[imageBrowser setZoomValue:0.20];
	[imageBrowser setDelegate:self];
	[imageBrowser setDataSource:self];
	
	[imageBrowser setIntercellSpacing:NSMakeSize(7, 7)];
	
	[imageBrowser setValue:[NSColor colorWithCalibratedRed:0.18 green:0.18 blue:0.18 alpha:1] forKey:IKImageBrowserBackgroundColorKey];
	[imageBrowser setValue:[NSColor colorWithCalibratedRed:80./255. green:125./255. blue:170./255. alpha:1] forKey:IKImageBrowserSelectionColorKey];
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
	[style setLineBreakMode:NSLineBreakByTruncatingMiddle];
	[style setAlignment:NSCenterTextAlignment];
	
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSFont fontWithName:@"Lucida Grande" size:10], NSFontAttributeName, 
							[NSColor whiteColor], NSForegroundColorAttributeName,
							style, NSParagraphStyleAttributeName,
							nil] 
					forKey:IKImageBrowserCellsTitleAttributesKey];
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSFont fontWithName:@"Lucida Grande" size:10], NSFontAttributeName,
							[NSColor whiteColor], NSForegroundColorAttributeName,
							style, NSParagraphStyleAttributeName,
							nil] 
					forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSFont fontWithName:@"Lucida Grande" size:9], NSFontAttributeName,
							[NSColor lightGrayColor], NSForegroundColorAttributeName, 
							nil] 
					forKey:IKImageBrowserCellsSubtitleAttributesKey];
	
	
	
	
	
	[[[imageBrowser enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[imageBrowser enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
			
	[stackImageArrayController addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	[stackImageArrayController addObserver:self forKeyPath:@"arrangedObjects" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	
	[stackArrayController addObserver:self forKeyPath:@"selection.browserZoom" options:0 context:nil];
		
	
}

- (void)bindToStack:(Stack *)stack
{
	//[stackImageArrayController bind:@"contentSet" toObject:stack withKeyPath:@"stackImages" options:nil];
	[stackArrayController setContent:[NSArray arrayWithObject:stack]];
	
	[stackTitleField setStringValue:[stack valueForKey:@"name"]];
	
	// set the correct view
	MGStackLayoutType newType;
	NSString *modelValue = [stack valueForKey:@"layoutType"];
	
	if ([modelValue isEqualToString:@"TimeMachine"])
		newType = MGTimeMachineStackLayout;

	else if ([modelValue isEqualToString:@"Reorder"])
		newType = MGReorderStackLayout;

	else if ([modelValue isEqualToString:@"Perpendicular"])
		newType = MGPerpendicularStackLayout;

	else if ([modelValue isEqualToString:@"Browser"])
		newType = MGBrowserStackLayout;
	
	// default
	else
		newType = MGTimeMachineStackLayout;
	
	
	[stackView changeStackLayoutType:newType];
	
	
	[stackView initializeToolStripForLayoutType:newType];
	
	
	if (newType != MGBrowserStackLayout) {
		// show the stack view
		[[imageBrowser enclosingScrollView] setHidden:YES];
		[stackView setHidden:NO];
		
		// change the stack layout itself, too
		[stackView updateControls];
		
		[browserViewModeButton setState:NSOffState];
	} else {
		// show the image browser
		[[imageBrowser enclosingScrollView] setHidden:NO];
		[stackView setHidden:YES];
		
		[stackView updateControls];
		
		[browserViewModeButton setState:NSOnState];
	}
	
	
	[self animateSortControlsAndBackButton];
	[stackTitleField setAlphaValue:1.0];


}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object == stackImageArrayController && [keyPath isEqualToString:@"selection"]) {
		
		NSArray *selection = [stackImageArrayController selectedObjects];
		
		NSMutableArray *selectedImages = [NSMutableArray array];
		
		for (StackImage *stackImage in selection)
			if ([stackImage valueForKey:@"image"])
				[selectedImages addObject:[stackImage valueForKey:@"image"]];
		
		[imageArrayController setSelectedObjects:selectedImages];

	}
	
	else if (object == stackImageArrayController && [keyPath isEqualToString:@"arrangedObjects"]) {
		[imageBrowser reloadData];
	}
	
	else if (object == stackArrayController && [keyPath isEqualToString:@"selection.browserZoom"]) {
		
		id numberObj = [stackArrayController valueForKeyPath:@"selection.browserZoom"];
		if ([numberObj isKindOfClass:[NSNumber class]]) {
			CGFloat zoomValue = [numberObj floatValue];
			[imageBrowser setZoomValue:zoomValue];
		}
		else
			[imageBrowser setZoomValue:-1.0];

		
	}

	
	else
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}


- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)controller;
{
	// refresh the search menu
	[self searchCurrentFolderWithPredicate:nil naturalLanguageString:@""];
	[self _updateSearchFieldCategories];
	
	// refresh the sort menu
	[self _refreshSortControls];
}

#pragma mark -
#pragma mark Accessors

- (NSArrayController *)contentController;
{
	return stackImageArrayController;
	
}

- (NSArray *)sortedImages;
{
	NSMutableArray *images = [NSMutableArray array];
	for (StackImageMainLayer *imageLayer in [stackView sortedSublayers]) {
		if ([imageLayer.stackImage valueForKeyPath:@"image"])
			[images addObject:[imageLayer.stackImage valueForKeyPath:@"image"]];
	}
	
	return images;
	
}


- (NSButton *)backButton;
{
	return backButton;
}

- (void)setImageArrayController:(ImageArrayController *)ctl;
{
	imageArrayController = ctl;
}

- (NSArray *)draggedImages;
{
	return draggedItems;
}

#pragma mark -
#pragma mark Filtering

- (IBAction)clearFilterPredicate:(id)sender
{
	[self removeFilterPredicate];
}

- (void)removeFilterPredicate
{
	[stackImageArrayController setFilterPredicate:nil];
	[self hideSearchBar];
	
}

# pragma mark -
# pragma mark Searching / filter predicates

- (void)_updateSearchFieldCategories
{	
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] searchAttributeKeyPaths]) {
		
		NSMenuItem *item = nil;
		
		if ((id)keyPath != [NSNull null]) {
			
			item  = [[[NSMenuItem alloc] initWithTitle:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath]
												action:@selector(changeSearchCategory:) 
										 keyEquivalent:@""] autorelease];
			[item setTarget:self];
			[item setRepresentedObject:keyPath];
			
		}
		else {
			
			item = [NSMenuItem separatorItem];
			
		}
		
		[menu addItem:item];
		
	}
	
	[[stackSearchField cell] setSearchMenuTemplate:menu];
	
	[self changeSearchCategory:[menu itemAtIndex:0]];
	
}



- (void)changeSearchCategory:(id)sender
{
	NSMenuItem *item = (NSMenuItem *)sender;
	
	NSMenu *menuCopy = [[[item menu] copy] autorelease];
	
	for (NSMenuItem *otherItem in [menuCopy itemArray])
		[otherItem setState:NSOffState];		
	
	
	[[menuCopy itemAtIndex:[[item menu] indexOfItem:item]] setState:NSOnState];
	
	[[stackSearchField cell] setSearchMenuTemplate:menuCopy];
	
	[[stackSearchField cell] setPlaceholderString:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:[item representedObject]]];
	
	[self changeSearchString:stackSearchField];

}

- (IBAction)changeSearchString:(id)sender;
{
	NSString *string = [(NSSearchField *)sender stringValue];
	
	NSMenuItem *selectedItem = nil;
	for (NSMenuItem *item in [[[(NSSearchField *)sender cell] searchMenuTemplate] itemArray])
		if ([item state] == NSOnState)
			selectedItem = item;
	
	if (![string length] || !selectedItem)
		[stackImageArrayController setFilterPredicate:nil];
	
	else 
		[stackImageArrayController setFilterPredicate:[[MGFilterAndSortMetadataController sharedController] searchPredicateForAttributeKeyPath:(NSString *)[selectedItem representedObject] 
																																		prefix:@"image"
																																  searchString:string]];
	
	
}


- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;
{
	
	if (filterPredicate) {
		[self setSortControlsVisible:NO];
		
		[self showSearchBarWithMessage:description];
	}
	else {
		
		[self setSortControlsVisible:YES];
		
		[self hideSearchBar];
		[stackSearchField setStringValue:@""];
		[stackSearchField resignFirstResponder];
	}
	
	
	[stackImageArrayController setFilterPredicate:filterPredicate];
	
}

- (void)showSearchBarWithMessage:(NSString *)message;
{
	[[stackTitleField animator] setAlphaValue:0.0];
	
	[searchMessage setAlphaValue:0.0];
	[searchMessage setStringValue:[message stringByReplacingOccurrencesOfString:@"Search" withString:@"Searched"]];
	[doneButton setAlphaValue:0.0];
	[doneButton setHidden:NO];
	[[searchMessage animator] setAlphaValue:1.0];
	[[doneButton animator] setAlphaValue:1.0];
}

- (void)hideSearchBar;
{
	[[stackTitleField animator] setAlphaValue:1.0];

	[[searchMessage animator] setAlphaValue:0.0];
	[[doneButton animator] setAlphaValue:0.0];
	[searchMessage setStringValue:@""];
	[doneButton setHidden:YES];
}

#pragma mark -
#pragma mark Switch StackView<->IKIB

- (IBAction)switchStackView:(id)sender
{

	
	if ([sender tag] < 3) {
		// show the stack view
		BOOL otherViewWasActive = [stackView isHidden];

				
		[CATransaction begin];
		
		[CATransaction setDisableActions:otherViewWasActive];
		// change the stack layout itself, too
		[stackView switchStackLayout:sender];
		[CATransaction commit];
		[stackView updateControls];

		[browserViewModeButton setState:NSOffState];
		
		// reset the controller
		if ([[stackImageArrayController selectedObjects] count])
			[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:[[stackImageArrayController selectedObjects] objectAtIndex:0]]];
		
		else if ([[stackImageArrayController arrangedObjects] count])
			[stackImageArrayController setSelectedObjects:[NSArray arrayWithObject:[[stackImageArrayController arrangedObjects] objectAtIndex:0]]];

		else
			[stackImageArrayController setSelectedObjects:[NSArray array]];
		
		
		
		
		[[imageBrowser enclosingScrollView] setHidden:YES];
		[stackView setHidden:NO];
		

	} else {
		// show the image browser
		[[imageBrowser enclosingScrollView] setHidden:NO];
		
		[imageBrowser setSelectionIndexes:[stackImageArrayController selectionIndexes] byExtendingSelection:NO];
		
		[stackView setHidden:YES];
		
		[stackView switchStackLayout:sender];
		[stackView updateControls];
		
		[browserViewModeButton setState:NSOnState];
				
	}
	// disable animations first
	[stackView setAnimatesLayout:NO];
	[stackView setNeedsLayout];
	[stackView setAnimatesLayout:YES];
	

}


# pragma mark -
# pragma mark IKImageBrowserView datasource

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)aBrowser;
{
	return [[stackImageArrayController arrangedObjects] count];
}


- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index;
{
	return [[[stackImageArrayController arrangedObjects] objectAtIndex:index] valueForKey:@"image"];
}

- (NSUInteger) imageBrowser:(IKImageBrowserView *) aBrowser writeItemsAtIndexes:(NSIndexSet *) itemIndexes toPasteboard:(NSPasteboard *)pasteboard;
{
	[pasteboard setString:@"Images" forType:@"StackImagesDragType"];
	
	[draggedItems release];
	draggedItems = [[NSMutableArray array] retain];
	
	NSInteger index = [itemIndexes firstIndex];
	while (index != NSNotFound) {
		[draggedItems addObject:[self imageBrowser:nil itemAtIndex:index]];
		index = [itemIndexes indexGreaterThanIndex:index];
	}
	
	return [draggedItems count];
}

# pragma mark -
# pragma mark IKImageBrowserView delegate

- (void) imageBrowserSelectionDidChange:(IKImageBrowserView *) aBrowser;
{
	[stackImageArrayController setSelectionIndexes:[aBrowser selectionIndexes]];
}


- (void)imageBrowser:(IKImageBrowserView *)aBrowser cellWasDoubleClickedAtIndex:(NSUInteger) index;
{
	[(ImageArrayController *)[MGLibraryControllerInstance imageArrayController] enterFullScreen];
}



#pragma mark -
#pragma mark Sorting (+UI)

- (void)animateSortControlsAndBackButton
{
	if (!initialSortControlOriginsSet) {
		initialSortControlOriginsSet = YES;
		initialSortOrderControlOrigin = [sortOrderControl frame].origin;
		initialSortKeyPopUpButtonOrigin = [sortKeyPopUpButton frame].origin;
	}
	
	[sortKeyPopUpButton setFrameOrigin:initialSortKeyPopUpButtonOrigin];
	[sortOrderControl setFrameOrigin:initialSortOrderControlOrigin];
	
	NSPoint offsetPopUpOrigin = initialSortKeyPopUpButtonOrigin;
	NSPoint offsetControlOrigin = initialSortOrderControlOrigin;
	CGFloat increase = 57;
	offsetPopUpOrigin.x += increase;
	offsetControlOrigin.x += increase;
	
	[[sortKeyPopUpButton animator] setFrameOrigin:offsetPopUpOrigin];
	[[sortOrderControl animator] setFrameOrigin:offsetControlOrigin];
	
	
	
}

- (IBAction)changeSortKey:(id)sender
{
	[self setSortKey:[sender representedObject]];
}


- (IBAction)changeSortOrder:(id)sender
{
	
	[self setSortOrder:[sender selectedSegment] == 1];
	
	
}

- (BOOL)hasNonDefaultSortOrderOrKey
{
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ImageArraySortOrder"]) return YES;
	if (!([[[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"] isEqualToString:@"creationDate"])) return YES;
	
	return NO;
}


- (void)setSortKey:(NSString *)key
{	
	if (![[[MGFilterAndSortMetadataController sharedController] sortAttributeKeyPaths] containsObject:key])
		key = @"name";
	
	[[NSUserDefaults standardUserDefaults] setValue:key forKey:@"ImageArraySortKey"];
	
	NSString *sortKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"];
	BOOL sortOrder = ![[NSUserDefaults standardUserDefaults] boolForKey:@"ImageArraySortOrder"];
	
	NSString *prefixedKey = [@"image." stringByAppendingString:sortKey];

	
	// Sort ourselves based on the primary key
	NSSortDescriptor *sortD = [[[NSSortDescriptor alloc] initWithKey:prefixedKey ascending:sortOrder] autorelease];
	
	// set a backup key in case the first one isn't discriminating enough
	NSSortDescriptor *sortD2 = [[[NSSortDescriptor alloc] initWithKey:@"image.name" ascending:YES] autorelease];
	[stackImageArrayController setSortDescriptors:[NSArray arrayWithObjects:sortD, sortD2, nil]];
	
	
	// set the popup item and segmented control to reflect the new settings
	
	for (NSMenuItem *item in [[sortKeyPopUpButton menu] itemArray])
		if ([[item representedObject] isEqualTo:key])
			[sortKeyPopUpButton selectItem:item];
	
	[sortOrderControl setSelectedSegment:sortOrder ? 0 : 1];
	
	[imageBrowser reloadData];
	

}

- (void)setSortOrder:(BOOL)descendingOrAscending
{
	[[NSUserDefaults standardUserDefaults] setBool:descendingOrAscending forKey:@"ImageArraySortOrder"];
	
	
	[self setSortKey:[[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"]];
	
}



- (void)_refreshSortControls
{
	
	[self setSortControlsVisible:YES];
	
	
	[self _refreshSortKeysPopup];
	
	// Setup the sort controls
	NSImage *sortAscending = [NSImage imageNamed:@"popup_sort_ascending"];
	[sortAscending setTemplate:YES];
	[sortOrderControl setImage:sortAscending forSegment:0];
	NSImage *sortDescending = [NSImage imageNamed:@"popup_sort_descending"];
	[sortDescending setTemplate:YES];
	[sortOrderControl setImage:sortDescending forSegment:1];	
	
	
	
}

- (void)_refreshSortKeysPopup
{
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] sortAttributeKeyPaths]) {
		
		if ((id)keyPath != [NSNull null]) {
			
			NSMenuItem *keyItem = [[[NSMenuItem alloc] init] autorelease];
			[keyItem setAttributedTitle:[[[NSAttributedString alloc] initWithString:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath] 
																		 attributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11], NSFontAttributeName, nil]] autorelease]];
			[keyItem setRepresentedObject:keyPath];
			[keyItem setTarget:self];
			[keyItem setAction:@selector(changeSortKey:)];
			[menu addItem:keyItem];
			
		} else {
			
			[menu addItem:[NSMenuItem separatorItem]];
			
		}
		
	}
	
	[sortKeyPopUpButton setMenu:menu];
	
	
	NSString *primaryKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"];
	
	// sort order will be done automatically with this invocation
	[self setSortKey:primaryKey];
}




- (void)setSortControlsVisible:(BOOL)flag
{
	if (flag) {
		[sortKeyPopUpButton setHidden:NO];
		[sortOrderControl setHidden:NO];
		
		[[sortKeyPopUpButton superview] addSubview:sortKeyPopUpButton positioned:NSWindowAbove relativeTo:nil];
		[[sortOrderControl superview] addSubview:sortOrderControl positioned:NSWindowAbove relativeTo:nil];
		
	} else {
		
		[sortKeyPopUpButton setHidden:YES];
		[sortOrderControl setHidden:YES];
		
		[[sortKeyPopUpButton superview] addSubview:sortKeyPopUpButton positioned:NSWindowBelow relativeTo:nil];
		[[sortOrderControl superview] addSubview:sortOrderControl positioned:NSWindowBelow relativeTo:nil];
		
		
	}
	
	
}




@end
