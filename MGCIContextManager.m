//
//  MGCIContextManager.m
//  Filament
//
//  Created by Dennis Lorson on 04/09/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGCIContextManager.h"

static MGCIContextManager *sharedContextManager = nil;

@implementation MGCIContextManager

+ (MGCIContextManager *)sharedContextManager;
{
	if (!sharedContextManager) {
		sharedContextManager = [[MGCIContextManager alloc] init];
	}
	return sharedContextManager;
}

- (id) init
{
	self = [super init];
	if (self != nil) {
		currentContextExtent = CGRectZero;
	}
	return self;
}


- (CIContext *)contextWithExtent:(CGRect)extent
{
	
	//return [CIContext contextWithCGContext:[[[NSApp mainWindow] graphicsContext] graphicsPort] options:nil];
	[currentContext reclaimResources];
	[currentContext clearCaches];
	
	if (currentContextExtent.size.width >= extent.size.width && currentContextExtent.size.width >= extent.size.width)
		return currentContext;
	
	// we have larger dimensions; create a new context
	
	// remove the old one
	[self clearContext];
	
	NSInteger width = (NSInteger)ceil(MAX(extent.size.width, currentContextExtent.size.width)) + 1;
	NSInteger height = (NSInteger)ceil(MAX(extent.size.height, currentContextExtent.size.width)) + 1;
	//NSLog(@"Creating larger context: %ld x %ld", width, height);
	
	currentContextExtent = CGRectMake(0, 0, width, height);
	
	width *= 0.2;
	height *= 0.2;

	
	backingContextData = calloc(width * height * 4, 1);
	
	backingContext = CGBitmapContextCreate(backingContextData, 
										   width, 
										   height,
										   8,
										   4 * width,
										   CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), 
										   kCGImageAlphaPremultipliedFirst);
	
	
	currentContext = [[CIContext contextWithCGContext:backingContext options:nil] retain];
	
	return currentContext;
}

- (void)renderImage:(CIImage *)image inRect:(CGRect)rect
{
	CIContext *ciCtx = [[NSGraphicsContext currentContext] CIContext];

	[ciCtx drawImage:image inRect:rect fromRect:[image extent]];
}

- (CGImageRef)imageRefFromCIImage_10A432_WORKAROUND:(CIImage *)ciImg
{
	CIContext *ciCtx = [self contextWithExtent:[ciImg extent]];

	CGImageRef result = [ciCtx createCGImage:ciImg fromRect:[ciImg extent] format:kCIFormatARGB8 colorSpace:CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB)];
	
	CGImageRef resultDeepCopy = [self copyOfImage:result];
		
	CGImageRelease(result);
	
	return (CGImageRef)[(id)resultDeepCopy autorelease];
}


- (CGImageRef)imageRefFromCIImage:(CIImage *)ciImg
{
	CIContext *ciCtx = [self contextWithExtent:[ciImg extent]];
	
	CGImageRef result = [ciCtx createCGImage:ciImg fromRect:[ciImg extent] format:kCIFormatARGB8 colorSpace:CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB)];
	
	return (CGImageRef)[(id)result autorelease];
}


- (CGImageRef)copyOfImage:(CGImageRef)original
{
	NSInteger w = CGImageGetWidth(original);
	NSInteger h = CGImageGetHeight(original);
	
	void *data = calloc(4 * w * h, 1);
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();

	CGContextRef ctx = CGBitmapContextCreate(data, w, h, 8, 4 * w, colorspace, kCGImageAlphaPremultipliedFirst);
	
	CGColorSpaceRelease(colorspace);

	
	CGContextDrawImage(ctx, CGRectMake(0, 0, w, h), original);
	
	CGImageRef result = CGBitmapContextCreateImage(ctx);
	
	CGContextRelease(ctx);
	free(data);
	
	return result;
}


- (void)clearContext
{
	if (currentContext) {
		[currentContext release];
		currentContext = nil;
		
		CGContextRelease(backingContext);
		backingContext = NULL;
		
		free(backingContextData);
		backingContextData = NULL;
	}	
}

- (void) dealloc
{
	[self clearContext];
	
	[super dealloc];
}



@end
