//
//  ROIArrayController.h
//  Macnification
//
//  Created by Peter Schols on 19/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "StatisticsController.h"


@interface ROIArrayController : NSArrayController {
    StatisticsController *statisticsController;
	
	IBOutlet id imageArrayController;
	IBOutlet NSTextView *roiCommentTextView;
	IBOutlet NSWindow *roiHud;
	IBOutlet NSMenu *actionMenu;
}


- (void)updateSpreadsheetMenu;
- (void)showROIHUD;


@end
