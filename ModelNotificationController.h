//
//  ModelNotificationController.h
//  Filament
//
//  Created by Dennis Lorson on 30/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ModelNotificationController : NSObject {
	
	NSArrayController *receivingController;
	BOOL postNotifications;

}

@property(readwrite) BOOL postNotifications;

+ (ModelNotificationController *)sharedNotificationController;

- (void)postSelectionChangeForKeyPath:(NSString *)keyPath ofObject:(id)object;

@end
