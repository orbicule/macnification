/*
 *  ImageAlignmentTypes.h
 *  ImageAlignment
 *
 *  Created by Dennis Lorson on 05/11/09.
 *  Copyright 2009 Orbicule. All rights reserved.
 *
 */

#import <vImage/vImage.h>
#import <Quartz/Quartz.h>
#import <stdlib.h>

typedef float IFSample;

typedef struct _IAImage {
	
	int w;
	int h;
	unsigned char *data;
	
	vImagePixelCount **histogram;
	
} IAImage;

typedef struct _IFFusionImage {
	
	int wr;		// real dimensions
	int hr;
	int w;		// dimensions after padding to powers of two (but NOT including convolution padding!!)
	int h;
	int pad;
	IFSample *data[4];
	
} IFFusionImage;

typedef struct _IFLaplacian {
	
	IFFusionImage *images[11];		// allows for 32 * 2^9 pixel images, which should be more than enough...
	size_t nLevels;
	
} IFLaplacian;


typedef struct _IAParameters {
	
	CGFloat tx;
	CGFloat ty;
	CGFloat sxy;
	
} IAParameters;

void IFImageRelease(IFFusionImage *image);
void IAImageRelease(IAImage *image);
void IFLaplacianRelease(IFLaplacian *lpt);
IAImage *IAImageCopy(IAImage *original);


