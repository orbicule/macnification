//
//  CalibrationArrayController.m
//  Filament
//
//  Created by Peter Schols on 24/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "CalibrationArrayController.h"
#import "Image.h"

@implementation CalibrationArrayController



# pragma mark -
# pragma mark Calibration


// This method is invoked when assigning an existing calibration to an image. 
// The existing calibration is chosen from a popup menu in the calibration panel

- (IBAction)assignExistingCalibration:(id)sender;
{
	// Assign the selected calibration to the currently selected image
	Image *selectedImage = [[imageArrayController selectedObjects] objectAtIndex:0];
	[selectedImage setValue:[[self selectedObjects] objectAtIndex:0] forKey:@"calibration"];

	// Hide the calibration panel
	[calibrationPanel orderOut:self];
    
	
	// TEST
	//NSLog(@"Assigned calibration: %@", [[self selectedObjects] objectAtIndex:0]);
}

- (NSArray *)arrangeObjects:(NSArray *)objects
{
	return [super arrangeObjects:objects];
		
}


@end
