//
//  LightTableImageContentView.m
//  LightTable
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableImageContentView.h"
#import "NSImage_MGAdditions.h"
#import <QuartzCore/QuartzCore.h>

@implementation LightTableImageContentView

@dynamic image;


- (id)initWithFrame:(NSRect)frame
{	
	if ((self = [super initWithFrame:frame])) {
		
		opacity = 1.0;
		
	}
	return self;
}

- (void)setOpacity:(CGFloat)val
{
	opacity = val;
	[self setNeedsDisplay:YES];
	
}

- (CGFloat)opacity
{
	return opacity;
}


- (void)setLayer:(CALayer *)layer
{
	[super setLayer:layer];
	
	layer.needsDisplayOnBoundsChange = YES;
	
}



- (void)drawRect:(NSRect)rect
{
	[[self image] drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:opacity];
}


@end
