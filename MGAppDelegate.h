//
//  Macnification_AppDelegate.h
//  Macnification
//
//  Created by Peter Schols on 30/10/06.
//  Copyright Orbicule 2006 . All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class PreferenceController;
@class TimeOutController;
@class MGLibraryController;
@class PreferenceController;

#import "MGLibrary.h"


@interface MGAppDelegate : NSObject <MGLibraryDelegate>
{
    MGLibrary *library_;
    MGLibraryController *libraryController_;
    
    NSOperationQueue *sharedOperationQueue;

    IBOutlet NSWindow *welcomeWindow_;
    
    IBOutlet NSWindow *selectLibraryWindow_;
    IBOutlet NSTextField *selectLibraryCurrentLocationField_;
    IBOutlet NSButton *selectLibraryNewOrOpenButton_;
    IBOutlet NSTextField *selectLibraryNewOrOpenField_;
    
    TimeOutController *timeOutController;
    PreferenceController *preferenceController;

	// Library refactoring
	IBOutlet NSWindow *refactoringWindow;
	IBOutlet NSProgressIndicator *refactoringIndicator;
    
    NSMutableDictionary *analysisPlugins;

    // files that should open as soon as the library controller is available
    // (can be postponed due to licensing flow and others)
	NSArray *pendingOpenFiles_;
}



@property (retain, readwrite) NSOperationQueue *sharedOperationQueue;
@property (retain, readwrite) NSMutableDictionary *analysisPlugins;



- (MGLibraryController *)libraryController;

- (IBAction)showPreferencePanel:(id)sender;
- (IBAction)showRegistrationPreferences:(id)sender;

- (void)showTimeOutScreenIfUnregistered;

- (IBAction)showHelp:(id)sender;
- (IBAction)createEmptyLibrary:(id)sender;
- (IBAction)createDemoLibrary:(id)sender;

- (IBAction)chooseLibrary:(id)sender;
- (IBAction)createOrOpenDefaultLibrary:(id)sender;

- (IBAction)showRegistrationPanel:(id)sender;


- (void)prepareForRestart;
- (void)restart;





@end
