//
//  MGSliderCell.m
//  Filament
//
//  Created by Dennis Lorson on 11/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGSliderCell.h"


@implementation MGSliderCell


- (void)drawKnob:(NSRect)knobRect
{
	if (![(NSControl *)[self controlView] isEnabled]) return;
	NSImage *knob = pressed ? [NSImage imageNamed:@"slider_knob_pressed"] : [NSImage imageNamed:@"slider_knob"];
	
	NSSize knobSize = [knob size];
	
	NSRect destRect = NSMakeRect(NSMidX(knobRect) - knobSize.width/2, NSMidY(knobRect) - knobSize.height/2, knobSize.width, knobSize.height);
	
	[knob drawInRect:destRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	//[knob compositeToPoint:NSMakePoint(NSMidX(knobRect) - knobSize.width/2, NSMidY(knobRect) - knobSize.height/2) operation:NSCompositeSourceOver];
	
}

- (void)drawBarInside:(NSRect)aRect flipped:(BOOL)flipped
{
	NSImage *barLeft = [NSImage imageNamed:@"slider_bar_left"];
	NSImage *barMiddle = [NSImage imageNamed:@"slider_bar_middle"];
	NSImage *barRight = [NSImage imageNamed:@"slider_bar_right"];
	
	CGFloat barEndWidth = [barLeft size].width;
	CGFloat barHeight = [barLeft size].height;
	
	NSRect totalRect = NSInsetRect(aRect, 2, 0);
	
	NSRect leftRect = NSMakeRect(NSMinX(totalRect), NSMidY(totalRect) - barHeight/2, barEndWidth, barHeight);
	NSRect midRect = NSMakeRect(NSMinX(totalRect) + barEndWidth, NSMidY(totalRect) - barHeight/2, totalRect.size.width - 2*barEndWidth, barHeight);
	NSRect endRect = NSMakeRect(NSMaxX(totalRect) - barEndWidth, NSMidY(totalRect) - barHeight/2, barEndWidth, barHeight);
	
	[barLeft drawInRect:leftRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[barMiddle drawInRect:midRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[barRight drawInRect:endRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];

	
	
}


- (void)stopTracking:(NSPoint)lastPoint at:(NSPoint)stopPoint inView:(NSView *)controlView mouseIsUp:(BOOL)flag
{
	pressed = NO;
	[super stopTracking:lastPoint at:stopPoint inView:controlView mouseIsUp:flag];
}

- (BOOL)startTrackingAt:(NSPoint)startPoint inView:(NSView *)controlView
{
	pressed = YES;
	return [super startTrackingAt:startPoint inView:controlView];
}



- (BOOL)_usesCustomTrackImage
{
	return YES;
}

@end
