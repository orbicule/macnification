//
//  MGCoreImageMutex.h
//  Filament
//
//  Created by Dennis Lorson on 21/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGCoreImageMutex : NSObject 
{

}

+ (MGCoreImageMutex *)sharedMutex;

@end
