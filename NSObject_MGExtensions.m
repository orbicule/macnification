//
//  NSObject_MGExtensions.m
//  Filament
//
//  Created by Dennis Lorson on 22/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "NSObject_MGExtensions.h"

@implementation NSObject (MGExtensions)

- (BOOL)isNull;
{
    return self == [NSNull null];
}

@end
