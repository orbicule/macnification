//
//  ROIExtractor.m
//  BlobExtraction
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "ROIExtractor.h"

#import "BlobExtractOperation.h"


@interface ROIExtractor (Private)


- (void)initializeGlobals;

- (void)anyThread_operationDidEnd:(NSNotification *)note;
- (void)operationDidEnd:(NSNotification *)note;



@end



@implementation ROIExtractor

//static NSString *BlobExtractionOperationDidEndNotification = @"BlobExtractionDidEndNotification";

@synthesize image, minimumROIArea, allowsImageEdgeROIs, searchesInnerROIs, maxInterpolationError;


- (id)initWithImage:(NSBitmapImageRep *)theImage
{
	if (self = [super init]) {
		
		self.image = theImage;
		self.minimumROIArea = 20;
		self.allowsImageEdgeROIs = YES;
		self.searchesInnerROIs = YES;
		self.maxInterpolationError = 1.0;
		queue = [[NSOperationQueue alloc] init];
		[queue setMaxConcurrentOperationCount:1];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(anyThread_operationDidEnd:) name:BlobExtractionOperationDidEndNotification object:nil];

		
	}
	return self;
}

/*
- (void)setMaxInterpolationError:(CGFloat)maxError
{
	CGFloat maxErrorAdjusted = maxError;
	if (self.image) {
		
		CGFloat dimension = ([self.image pixelsWide] + [self image pixelsHigh])
		
	}
	
}*/

- (void)extract
{
	// prevent all the other threads on the queue from finishing
	[queue cancelAllOperations];
	
	[queue waitUntilAllOperationsAreFinished];
	
	// start the main extraction (of ROIs in the main image)
	BlobExtractOperation *op = [[[BlobExtractOperation alloc] initWithImage:self.image] autorelease];
	
	op.minimumBlobArea = self.minimumROIArea;
	//op.allowsImageEdgeBlobs = self.allowsImageEdgeROIs;
	op.maxInterpolationError = self.maxInterpolationError;
	op.searchesInnerBlobs = self.searchesInnerROIs;
		
	// start the operation.	
	[queue addOperation:op];
	
	// now the main thread work ends until a notification is received by this object on the separate thread.
}


- (void)anyThread_operationDidEnd:(NSNotification *)note
{
	[self performSelectorOnMainThread:@selector(operationDidEnd:) withObject:note waitUntilDone:NO];	
}

- (void)operationDidEnd:(NSNotification *)note
{	
		// send the ROIs back in a notification, along with a ref to the image.  Return.
		NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[[note userInfo] objectForKey:@"results"], @"rois", nil];
		NSNotification *returnNote = [NSNotification notificationWithName:ROIExtractionOperationDidEndNotification object:self userInfo:userInfo];
		[[NSNotificationCenter defaultCenter] postNotification:returnNote];
		return;
		
}

- (void)dealloc
{
	// remove ourself as observer for all notifications
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[queue cancelAllOperations];
	
	[queue release];
	[image release];
	
	[super dealloc];
}

@end
