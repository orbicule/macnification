//
//  MigrationPolicy_v10_to_v18_Stack.h
//  Filament
//
//  Created by Dennis Lorson on 20/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MigrationPolicy_v10_to_v18_Stack : NSEntityMigrationPolicy
{

}

@end




@interface MigrationPolicy_v10_to_v18_StackImage : NSEntityMigrationPolicy
{
	
}

@end




@interface MigrationPolicy_v10_to_v18_Image : NSEntityMigrationPolicy
{
	
}

@end
