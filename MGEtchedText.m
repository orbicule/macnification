#import "MGEtchedText.h"
#import "MGEtchedTextCell.h"

@implementation MGEtchedText

+ (Class)cellClass
{
	return [MGEtchedTextCell class];
}

- initWithCoder: (NSCoder *)origCoder
{
	if(![origCoder isKindOfClass: [NSKeyedUnarchiver class]]){
		self = [super initWithCoder: origCoder]; 
	} else {
		NSKeyedUnarchiver *coder = (id)origCoder;
		
		NSString *oldClassName = [[[self superclass] cellClass] className];
		Class oldClass = [coder classForClassName: oldClassName];
		if(!oldClass)
			oldClass = [[super superclass] cellClass];
		[coder setClass: [[self class] cellClass] forClassName: oldClassName];
		self = [super initWithCoder: coder];
		[coder setClass: oldClass forClassName: oldClassName];
		
		[self setShadowColor:[NSColor colorWithCalibratedWhite:0.85 alpha:1.0]];
	}
	
	return self;
}

-(void)setShadowColor:(NSColor *)color
{
	MGEtchedTextCell *cell = [self cell];
	[cell setShadowColor:color];
}

@end
