//
//  ChannelMergeBinView.m
//  Filament
//
//  Created by Dennis Lorson on 25/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ChannelMergeBinView.h"
#import "Image.h"
#import "ChannelMergeBinImageLayer.h"
#import "ChannelMergeController.h"

@implementation ChannelMergeBinView


@synthesize channel;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}


- (void) dealloc
{
	[binImages release];
	
	[super dealloc];
}


- (void)awakeFromNib
{
	//binImages = [[NSMutableArray array] retain];
	
	
	[self setWantsLayer:YES];
	
	binLayer = [CALayer layer];
	binLayer.layoutManager = self;

	
	CALayer *root = [self layer];
	binLayer.frame = root.bounds;
	
	[root addSublayer:binLayer];
	
	CGRect frame = CGRectInset(root.bounds, 30, 20);
	//frame.origin.y -= 12;
	
	placeholderLayer = [CALayer layer];
	placeholderLayer.frame = frame;

	placeholderLayer.opacity = 0.4;

	placeholderLayer.delegate = self;
	
	[placeholderLayer setNeedsDisplay];
	
	[root addSublayer:placeholderLayer];
	
	
	// drag destination initialization
	[self registerForDraggedTypes:[NSArray arrayWithObject:@"DRAGGED_IMAGES_TYPE"]];
	[self registerForDraggedTypes:[NSArray arrayWithObject:@"ImageObjects"]];
	
}


- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
	if (layer == [self layer]) return;
	
	
	CGRect bounds = CGContextGetClipBoundingBox(ctx);
	NSGraphicsContext *nsCtx = [NSGraphicsContext graphicsContextWithGraphicsPort:ctx flipped:NO];
	
	NSRect boundsRect = NSRectFromCGRect(bounds);
	
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:nsCtx];
	 
	NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:NSInsetRect(boundsRect, 2, 2) xRadius:12 yRadius:12];
	
	const CGFloat pattern[2] = {6, 6};
	
	[path setLineWidth:3];
	[path setLineDash:pattern count:2 phase:0];
	
	[[NSColor darkGrayColor] set];
	[path stroke];
	
	
	[NSGraphicsContext restoreGraphicsState];
	
	
}

- (void)addImageLayerForCompositeImage:(NSBitmapImageRep *)composite
{
	CGRect destRect;

	// compute the correct aspect ratio
	{
		CGRect srcRect = CGRectMake(0, 0, [composite pixelsWide], [composite pixelsHigh]);
		destRect = CGRectInset(binLayer.bounds, 0, 0);
		
		float heightRatio = srcRect.size.height/destRect.size.height;
		float widthRatio = srcRect.size.width/destRect.size.width;
		
		CGSize newSize = destRect.size;
		
		if (heightRatio > widthRatio) {
			newSize.width = srcRect.size.width / heightRatio;
		} else {
			newSize.height = srcRect.size.height / widthRatio;
		}
		
		destRect.origin.x += (destRect.size.width - newSize.width)/2.;
		destRect.origin.y += (destRect.size.height - newSize.height)/2.;
		destRect.size = newSize;
		
	}

	
	ChannelMergeBinImageLayer *layer = [ChannelMergeBinImageLayer layer];
	layer.compositeImage = composite;
	layer.frame = destRect;
	//layer.borderColor = CGColorCreateGenericGray(1., 0.5);
	//layer.borderWidth = 2.0;
	
	[binLayer addSublayer:layer];
	
	[binLayer setNeedsLayout];
	
	placeholderLayer.opacity = 0.0;
	
}

- (void)addImageLayerForImage:(Image *)image
{
	
	CIImage *filtered = [image filteredCIImage];
	CGRect destRect;
	
	// compute the correct aspect ratio
	{
		CGRect srcRect = [filtered extent];
		destRect = CGRectInset(binLayer.bounds, 0, 0);
		
		float heightRatio = srcRect.size.height/destRect.size.height;
		float widthRatio = srcRect.size.width/destRect.size.width;
		
		CGSize newSize = destRect.size;
		
		if (heightRatio > widthRatio) {
			newSize.width = srcRect.size.width / heightRatio;
		} else {
			newSize.height = srcRect.size.height / widthRatio;
		}
		
		destRect.origin.x += (destRect.size.width - newSize.width)/2.;
		destRect.origin.y += (destRect.size.height - newSize.height)/2.;
		destRect.size = newSize;
		
	}

	ChannelMergeBinImageLayer *layer = [ChannelMergeBinImageLayer layer];
	layer.image = image;
	layer.frame = destRect;

	//layer.borderColor = CGColorCreateGenericGray(1., 0.5);
	//layer.borderWidth = 2.0;
	
	[binLayer addSublayer:layer];
	
	[binLayer setNeedsLayout];
	
	placeholderLayer.opacity = 0.0;
}


- (void)removeAllImageLayers
{
	for (ChannelMergeBinImageLayer *layer in [[[binLayer sublayers] copy] autorelease]) {
		layer.image = nil;
		[layer removeFromSuperlayer];

	}

	[binLayer setNeedsLayout];
	
	placeholderLayer.opacity = 0.5;
	
}



- (void)setImages:(NSArray *)images
{
	if (binImages != images) {
		
		[binImages release];
		binImages = [images retain];
	}
	
	
	[self removeAllImageLayers];

	int i;
		
	for (i = 0; i < MIN(5, [binImages count]); i++) {
		
		[self addImageLayerForImage:[binImages objectAtIndex:i]];
	}
	
}

- (NSArray *)images
{
	return binImages;
}


#pragma mark -
#pragma mark Layout

- (void)layoutSublayersOfLayer:(CALayer *)layer
{
	NSArray *sublayers = [layer sublayers];
	
	NSInteger index = 0;
	
	for (CALayer *sublayer in sublayers) {
		
		sublayer.zPosition = - index * 150;
		sublayer.opacity = powf(1.0 - ((float)index)/4., 1.8);
		
		index++;
	}
	
	
}


#pragma mark -
#pragma mark Drag and drop


- (NSImage *)dragImage
{
	NSImage *image = [[[self images] objectAtIndex:0] image];
	CALayer *firstThumbnailLayer = [[[[[self layer] sublayers] objectAtIndex:0] sublayers] objectAtIndex:0];
	CGRect bounds = firstThumbnailLayer.bounds;
	bounds = [[self layer] convertRect:bounds fromLayer:firstThumbnailLayer];
		
	// make the image more transparent
	NSImage *newImage = [[[NSImage alloc] initWithSize:NSSizeFromCGSize(bounds.size)] autorelease];
	[newImage lockFocus];
	[image drawInRect:NSMakeRect(0, 0, [newImage size].width, [newImage size].height)  fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:0.6];
	[newImage unlockFocus];
	
	return newImage;
}


- (void)mouseDown:(NSEvent *)theEvent
{
	
	if ([[self images] count] == 0) return;
	
	NSPasteboard *pboard = [NSPasteboard pasteboardWithName:NSDragPboard];
    [pboard declareTypes:[NSArray arrayWithObject:@"ImageObjects"]  owner:self];
	
	// get the click point in terms of the thumbnail layer
	CALayer *firstThumbnailLayer = [[[[[self layer] sublayers] objectAtIndex:0] sublayers] objectAtIndex:0];
	CGPoint origin = CGPointMake(0, 0);
	origin = [firstThumbnailLayer convertPoint:origin toLayer:[self layer]];
	
	[self dragImage:[self dragImage]
				 at:NSPointFromCGPoint(origin)
			 offset:NSZeroSize
			  event:theEvent
		 pasteboard:pboard
			 source:self
		  slideBack:YES];
}


- (void)mouseDragged:(NSEvent *)theEvent
{
	
}

- (NSDragOperation)draggingEntered:(id < NSDraggingInfo >)sender
{
	if (self.channel == MergeChannelComposite) return NSDragOperationNone;
	
	//placeholderLayer.backgroundColor = CGColorCreateGenericGray(1.0, 0.5);
	placeholderLayer.opacity = [[self images] count] > 0 ? 0 : 1.0;

	
	return NSDragOperationCopy;
	
}

- (NSDragOperation)draggingUpdated:(id < NSDraggingInfo >)sender
{
	if (self.channel == MergeChannelComposite) return NSDragOperationNone;

	return NSDragOperationCopy;

}


- (void)draggingExited:(id < NSDraggingInfo >)sender
{
	if (self.channel == MergeChannelComposite) 
		return;
	
	//placeholderLayer.shadowOpacity = 0.0;
	//placeholderLayer.backgroundColor = CGColorCreateGenericGray(1.0, 0.05);
	placeholderLayer.opacity = [[self images] count] > 0 ? 0 : 0.4;
}

- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender
{
	placeholderLayer.shadowOpacity = 0.0;
	//placeholderLayer.backgroundColor = CGColorCreateGenericGray(1.0, 0.05);

	NSArray *temp = [[[[sender draggingSource] images] copy] autorelease];
	[[sender draggingSource] setImages:[self images]];
	[self setImages:temp];
	
	placeholderLayer.opacity = [[self images] count] > 0 ? 0 : 0.4;
	
	//[self setImages:[[MGAppDelegateInstance imageArrayController] selectedObjects]];
	[controller updateCompositeImage];
	return YES;
}




@end
