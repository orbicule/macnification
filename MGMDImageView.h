//
//  MGMDImageView.h
//  Filament
//
//  Created by Dennis Lorson on 1/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "MGMDView.h"

@interface MGMDImageView : NSImageView <MGMDManualObserver> {
	
	NSString *observableKeyPath;
	id observableController;
	
	NSImage *threadedDrawImage;
	
	CIImage *ciImage;

	BOOL ignoreViewUpdates;
}

@property(nonatomic, retain) CIImage *ciImage;

@end
