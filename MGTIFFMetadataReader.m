//
//  MGTIFFMetadataReader.m
//  Filament
//
//  Created by Dennis Lorson on 17/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGTIFFMetadataReader.h"
#import "MGByteAccessTools.h"

#import "MGMetadataSet.h"


#define MG_LITTLE_ENDIAN 0x4949
#define MG_BIG_ENDIAN 0x4d4d

#define MG_MAGIC_NUMBER 0x494a494a
#define MG_INFO 0x696e666f
#define MG_LABELS 0x6c61626c
#define MG_RANGES 0x72616e67
#define MG_LUTS 0x6c757473


#define MG_NEW_SUBFILE_TYPE		254 
#define MG_IMAGE_WIDTH			256 
#define MG_IMAGE_LENGTH			257 
#define MG_BITS_PER_SAMPLE		258 
#define MG_COMPRESSION			259 
#define MG_PHOTO_INTERP			262 
#define MG_IMAGE_DESCRIPTION	270 
#define MG_STRIP_OFFSETS		273 
#define MG_ORIENTATION			274 
#define MG_SAMPLES_PER_PIXEL	277 
#define MG_ROWS_PER_STRIP		278 
#define MG_STRIP_BYTE_COUNT		279 
#define MG_X_RESOLUTION			282 
#define MG_Y_RESOLUTION			283 
#define MG_PLANAR_CONFIGURATION 284 
#define MG_RESOLUTION_UNIT		296 
#define MG_SOFTWARE				305 
#define MG_DATE_TIME			306 
#define MG_PREDICTOR			317 
#define MG_COLOR_MAP			320 
#define MG_SAMPLE_FORMAT		339 
#define MG_JPEG_TABLES			347 
#define MG_METAMORPH1			33628 
#define MG_METAMORPH2			33629 
#define MG_IPLAB				34122 
#define MG_NIH_IMAGE_HDR		43314 
#define MG_META_DATA_BYTE_COUNTS 50838  // private tag registered with Adobe
#define MG_META_DATA			50839  // private tag registered with Adobe




@implementation MGTIFFMetadataReader

+ (NSDictionary *)metadataFromFile:(NSString *)filePath
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setup
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	NSMutableDictionary *metadata = [NSMutableDictionary dictionary];
	BOOL littleEndian;
	
	const char *cfilename = [[filePath stringByExpandingTildeInPath] cStringUsingEncoding:NSISOLatin1StringEncoding];
	FILE *file = fopen(cfilename, "r");
		
	if (!file)
		return [[metadata copy] autorelease];
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Header
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	int byteOrder = MGTIFFReadShort(file, YES);
	
	
	if (byteOrder == MG_LITTLE_ENDIAN) {
		littleEndian = YES;
	} else if (byteOrder == MG_BIG_ENDIAN) {
		littleEndian = NO;
	} else {
		fclose(file);
		return [[metadata copy] autorelease];
	}
	
	[metadata setObject:[NSNumber numberWithBool:littleEndian] forKey:MGMetadataImageTIFFLittleEndianKey];
	
	
	fclose(file);
	
	return [[metadata copy] autorelease];
	
}




@end
