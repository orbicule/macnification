//
//  MGGradientView.m
//  MetaData
//
//  Created by Dennis Lorson on 24/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGGradientView.h"


@implementation MGGradientView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    
	[super drawRect:rect];
	
	NSGradient *grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:237.0/255.0 alpha:1.0] 
													  endingColor:[NSColor colorWithCalibratedWhite:237.0/255.0 alpha:1.0]] autorelease];
	
	[grad drawInRect:[self bounds] angle:270];
	
	
		
}

@end
