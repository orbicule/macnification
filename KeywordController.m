//
//  KeywordController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 16 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import "KeywordController.h"
#import "MGAppDelegate.h"


@implementation KeywordController


- (id)initWithImageArrayController:(ImageArrayController *)controller;
{
	if ((self = [super initWithWindowNibName:@"Keywords"])) {
		[self setImageArrayController:controller];
		return self;
	}
	return nil;
}


- (void)awakeFromNib
{
	[[keywordView tableColumnWithIdentifier:@"name"] setEditable:NO];

}


- (IBAction)applyKeywordsToSelectedImages:(id)sender;
{
	[[self imageArrayController] applyKeywordsToSelectedImages:[keywordsTreeController selectedObjects]];
}


- (IBAction)toggleEditMode:(id)sender;
{
	editModeOn = !editModeOn;
	
	if (editModeOn) {
		
		for (NSButton *button in [NSArray arrayWithObjects:addButton, addChildButton, removeButton, nil])
			[button setHidden:NO];
		
		[applyButton setHidden:YES];
		[editButton setImage:[NSImage imageNamed:@"unlocked.tiff"]];

		[[keywordView tableColumnWithIdentifier:@"name"] setEditable:YES];

	} else {
		
		for (NSButton *button in [NSArray arrayWithObjects:addButton, addChildButton, removeButton, nil])
			[button setHidden:YES];
		
		[editButton setImage:[NSImage imageNamed:@"locked.tiff"]];
		[editButton setNeedsDisplay:YES];
		[applyButton setHidden:NO];

		[[keywordView tableColumnWithIdentifier:@"name"] setEditable:NO];

	}
    
	
	/*if ([editTabView indexOfTabViewItem:[editTabView selectedTabViewItem]] == 0) {
		[editTabView selectLastTabViewItem:self];
		[editButton setImage:[NSImage imageNamed:@"unlocked.tiff"]];
		[editButton setState:NSOnState];
		
		[[keywordView tableColumnWithIdentifier:@"name"] setEditable:YES];
		
	} else {
		[editTabView selectFirstTabViewItem:self];
		[editButton setImage:[NSImage imageNamed:@"locked.tiff"]];
		[editButton setState:NSOnState];
		
		[[keywordView tableColumnWithIdentifier:@"name"] setEditable:NO];

	}*/
	
}



- (ImageArrayController *)imageArrayController {
    return [[imageArrayController retain] autorelease];
}

- (void)setImageArrayController:(ImageArrayController *)value {
    if (imageArrayController != value) {
        [imageArrayController release];
        imageArrayController = [value retain];
    }
}





@end
