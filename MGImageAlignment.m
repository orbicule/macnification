//
//  ImageAlignment.m
//  ImageAlignment
//
//  Created by Dennis Lorson on 18/10/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "DistanceFilter.h"
#import "MGImageAlignment.h"

#define POW2(A) ((A) * (A))

//#define ALIGNMENT_DEBUG


CGFloat IAParametersNorm(IAParameters params)
{
	return sqrtf(POW2(params.tx) + POW2(params.ty) + POW2(params.sxy));
}

CGAffineTransform IACGTransformFromNSTransform(NSAffineTransformStruct tr)
{
	CGAffineTransform t;
	t.a = tr.m11; t.b = tr.m21; t.c = tr.m12; t.d = tr.m22; t.tx = tr.tX; t.ty = tr.tY;
	
	return t;
}

IAParameters IAParametersAdd(IAParameters one, IAParameters two)
{
	IAParameters sum;
	sum.sxy = one.sxy + two.sxy;
	sum.tx = one.tx + two.tx;
	sum.ty = one.ty + two.ty;
	
	return sum;
}

IAParameters IAParametersScale(IAParameters one, CGFloat factor)
{
	IAParameters scaled;
	scaled.sxy = one.sxy * factor;
	scaled.tx = one.tx * factor;
	scaled.ty = one.ty * factor;
	
	return scaled;
}

IAParameters IAParametersMake(CGFloat tx, CGFloat ty, CGFloat sxy)
{
	IAParameters new;
	new.tx = tx;
	new.ty = ty;
	new.sxy = sxy;
	
	return new;
}

@implementation MGImageAlignment






- (void)setup
{	
	
	void *data = malloc(8);
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();

	CGContextRef ctx = CGBitmapContextCreate(data, 1, 1, 16, 8, colorspace, kCGBitmapByteOrder16Host | kCGImageAlphaPremultipliedLast);
	ciCtx = [[CIContext contextWithCGContext:ctx options:nil] retain];
	
	CGColorSpaceRelease(colorspace);


	NSAssert(ciCtx, @"CIContext is NULL -- image alignment distance will not be correct!");
}



#pragma mark -
#pragma mark Alignment

static float distanceMeasurementTime = 0;


- (NSAffineTransform *)alignImage:(CIImage *)image toImage:(CIImage *)reference
{
	if (!ciCtx)
		[self setup];
	
	IAParameters init;
	init.tx = init.ty = 0;
	init.sxy = 1.0;
	
	distanceMeasurementTime = 0;
	
	IAParameters params = [self alignImage:reference withImage:image centerParameters:init];
	
#ifdef ALIGNMENT_DEBUG
	NSLog(@"Distance measurements: %f sec", distanceMeasurementTime);
#endif
		  
	return [self trafoWithParameters:params forImage:image];
	
}

static int i = 0;

- (CGFloat)distanceBetweenImage:(CIImage *)first andImage:(CIImage *)second withTrafo:(NSAffineTransform *)trafo andScale:(CGFloat)scale display:(BOOL)display
{	

	// ---------------------------------------------------------------------------------------------------------------------
	// Apply the suggested transformation to one of the images
	// ---------------------------------------------------------------------------------------------------------------------

	CGAffineTransform t = IACGTransformFromNSTransform([trafo transformStruct]);
	CIImage *transformed = [second imageByApplyingTransform:t];
	
	// ---------------------------------------------------------------------------------------------------------------------
	// Scale both images (multires)
	// ---------------------------------------------------------------------------------------------------------------------
	
	CIImage *firstScaled, *secondScaled;
	
	if (scale != 1.0) {
		
		
		NSAffineTransform *scaleTrafo = [NSAffineTransform transform];
		[scaleTrafo scaleBy:scale];
		CGAffineTransform t = IACGTransformFromNSTransform([scaleTrafo transformStruct]);
		
		firstScaled = [first imageByApplyingTransform:t];
		secondScaled = [transformed imageByApplyingTransform:t];
		
	} else {
		
		firstScaled = first;
		secondScaled = transformed;
		
	}
	

	
	// ---------------------------------------------------------------------------------------------------------------------
	// Compute the distance
	// ---------------------------------------------------------------------------------------------------------------------
	
	
	if (!distanceFilter)
		distanceFilter = [CIFilter filterWithName:@"DistanceFilter"];
	
	[distanceFilter setValue:firstScaled forKey:@"inputBackgroundImage"];
	[distanceFilter setValue:secondScaled forKey:@"inputImage"];
	
	CIImage *diffImage = [distanceFilter valueForKey:@"outputImage"];
	
	
	// ---------------------------------------------------------------------------------------------------------------------
	// Crop this
	// ---------------------------------------------------------------------------------------------------------------------
	
	CGRect firstExtent = [firstScaled extent];
	CGRect secondExtent = [secondScaled extent];
	CGRect intersection = CGRectIntersection(firstExtent, secondExtent);
	intersection = CGRectInset(intersection, intersection.size.width * 0.2, intersection.size.height * 0.2);
	
#ifdef ALIGNMENT_DEBUG
	NSLog(@"eval %i", i);
	//NSLog(@"eval %i: intersection: %@", i, NSStringFromRect(NSRectFromCGRect(intersection)));
#endif
	
	
	// ---------------------------------------------------------------------------------------------------------------------
	// Integrate over the image
	// ---------------------------------------------------------------------------------------------------------------------
	
	if (!averageFilter)
		averageFilter = [CIFilter filterWithName:@"CIAreaAverage"];
	[averageFilter setValue:diffImage forKey:@"inputImage"];
	[averageFilter setValue:[CIVector vectorWithX:intersection.origin.x Y:intersection.origin.y Z:intersection.size.width W:intersection.size.height] forKey:@"inputExtent"];
	
	// ---------------------------------------------------------------------------------------------------------------------
	// Download the distance value
	// ---------------------------------------------------------------------------------------------------------------------
	
	CIImage *diffInt = [averageFilter valueForKey:@"outputImage"];
	
	assert(ciCtx);
	
	CGRect bounds = CGRectMake(0.0, 0.0, 1, 1);
	uint16 data[4];
	data[0] = data[1] = data[2] = data[2] = 0;
	
	NSDate *date = [NSDate date];

	
	[ciCtx render:diffInt toBitmap:data rowBytes:8 bounds:bounds format:kCIFormatRGBA16 colorSpace:NULL];
	
	distanceMeasurementTime += [[NSDate date] timeIntervalSinceDate:date];
	
	CGFloat distance = (data[0] + data[1] + data[2]) / (3.0 * (CGFloat)UINT16_MAX);
	
	
	// ---------------------------------------------------------------------------------------------------------------------
	// Display (debug)
	// ---------------------------------------------------------------------------------------------------------------------
	
	
	if (NO) {
		
		CIFilter *cropFilter = [CIFilter filterWithName:@"CICrop"];
		[cropFilter setValue:diffImage forKey:@"inputImage"];
		[cropFilter setValue:[CIVector vectorWithX:intersection.origin.x Y:intersection.origin.y Z:intersection.size.width W:intersection.size.height] forKey:@"inputRectangle"];
		
		NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithCIImage:[cropFilter valueForKey:@"outputImage"]] autorelease];
		
		
		[[rep TIFFRepresentation] writeToFile:[NSHomeDirectory() stringByAppendingFormat:@"/Desktop/fusion/diff_%i.tiff", i] atomically:YES];
	}
	
	i++;
	
	return distance;
}


- (IAParameters)alignImage:(CIImage *)first withImage:(CIImage *)second centerParameters:(IAParameters)centerParams;
{
	
	// ---------------------------------------------------------------------------------------------------------
	
	
	CGFloat downsampleFactorIncrease = 2.0;
	// determine the lowest LoD
	CGSize imageSize = [first extent].size;
	int maxDownsampleFactor = downsampleFactorIncrease;
	while ( sqrtf(imageSize.width * imageSize.height / (float)POW2(maxDownsampleFactor)) > 20.0) {
		maxDownsampleFactor *= downsampleFactorIncrease;
	}
	
	
	// compute the initial range and resolution
	IAParameters range;
	range.tx = imageSize.width * 0.25;
	range.ty = imageSize.height * 0.25;
	range.sxy = 0.4;
	
	IAParameters x = centerParams;
	IAParameters bestX = x;
	
	CGFloat bestDistance;
	CGFloat distance;
	
	float currentFactor;
	for (currentFactor = maxDownsampleFactor; currentFactor >= 1.0; currentFactor *= 1.0/downsampleFactorIncrease) {
		
#ifdef ALIGNMENT_DEBUG
		NSLog(@"--------------------------------------------------------------------------------------------------------");
		NSLog(@"--------------------------------------------------------------------------------------------------------");
		NSLog(@"factor %f", currentFactor);
		NSLog(@"--------------------------------------------------------------------------------------------------------");
		NSLog(@"--------------------------------------------------------------------------------------------------------");

#endif
		
		BOOL scaleDidChange = YES;
		while (scaleDidChange) {
			
			// determine the best translation for this level (iterative x/y optimization)
			// ------------------------------------------------
			BOOL translationDidChange = YES;
			while (translationDidChange) {
				
				translationDidChange = NO;
				
				// determine the best x offset for this level
				// ------------------------------------------------
				
				bestDistance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:x forImage:second] andScale:1.0/(float)currentFactor display:YES];
				bestX.tx = x.tx;
				
#ifdef ALIGNMENT_DEBUG
				NSLog(@"try tx:    tx %f   ty %f    sxy %f    (distance %1.10f)", bestX.tx, bestX.ty, bestX.sxy, bestDistance); 
#endif
				
				for (int xoff = -1; xoff < 2; xoff++) {
					
					// was already handled one level above
					if (xoff == 0) continue;
					
					IAParameters temp = x;
					temp.tx += (xoff * range.tx / 3.);
					
					distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:temp forImage:second] andScale:1.0/(float)currentFactor display:YES];

					
#ifdef ALIGNMENT_DEBUG
					NSLog(@"try tx:    tx %f   ty %f    sxy %f    (distance %1.10f)", temp.tx, temp.ty, temp.sxy, distance); 
#endif
		
					if (distance < bestDistance) {
						bestDistance = distance;
						bestX = temp;
						translationDidChange = YES;
					}
				}
				x = bestX;
				
				// determine the best y offset for this level
				// ------------------------------------------------
				
				bestDistance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:x forImage:second] andScale:1.0/(float)currentFactor display:YES];
				bestX.ty = x.ty;
				
				// determine the best x offset for this level
				for (int yoff = -1; yoff < 2; yoff++) {
					
					// was already handled one level above
					if (yoff == 0) continue;
					
					IAParameters temp = x;
					temp.ty += (yoff * range.ty / 3.);
					
					distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:temp forImage:second] andScale:1.0/(float)currentFactor display:YES];

			
#ifdef ALIGNMENT_DEBUG
					NSLog(@"try ty:    tx %f   ty %f    sxy %f    (distance %1.10f)", temp.tx, temp.ty, temp.sxy, distance); 
#endif
				
					if (distance < bestDistance) {
						bestDistance = distance;
						bestX = temp;
						translationDidChange = YES;
					}
				}
				x = bestX;
			}
		
#ifdef ALIGNMENT_DEBUG
			NSLog(@"--------------------------------------------------------------------------------------------------------");
			NSLog(@"tried translation adjustment: new == %f, %f", x.tx, x.ty);
			NSLog(@"--------------------------------------------------------------------------------------------------------");

#endif
			
			translationDidChange = NO;
			
			// determine the best scale for this level
			// ------------------------------------------------
			
			scaleDidChange = NO;
			
			// the ideal scale resolution
			CGFloat maxDimension = MAX([second extent].size.width, [second extent].size.height) / currentFactor;
			CGFloat scaleResolution = (maxDimension + 0.5)/maxDimension - 1.0;
			
			bestDistance = distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:x forImage:second] andScale:1.0/(float)currentFactor display:YES];
			
			IAParameters temp = bestX = x;
			
			
			// test for scale increase
			while (distance <= bestDistance) {
				
				temp.sxy += scaleResolution;
				distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:temp forImage:second] andScale:1.0/(float)currentFactor display:YES];
				
#ifdef ALIGNMENT_DEBUG
				NSLog(@"try scale up:    tx %f   ty %f    sxy %f    (distance %1.10f)", temp.tx, temp.ty, temp.sxy, distance); 
#endif
				
				if (distance < bestDistance) {
					bestDistance = distance;
					bestX = temp;
					scaleDidChange = YES;
#ifdef ALIGNMENT_DEBUG
					NSLog(@"scale change %f", temp.sxy);
#endif
					
				}
			}
			
			CGFloat bestDistance2 = distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:x forImage:second] andScale:1.0/(float)currentFactor display:YES];
			
			// test for scale decrease
			temp = x;
			while (distance <= bestDistance2) {
				
				temp.sxy -= scaleResolution;
				distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:temp forImage:second] andScale:1.0/(float)currentFactor display:YES];
				
#ifdef ALIGNMENT_DEBUG
				NSLog(@"try scale down:    tx %f   ty %f    sxy %f    (distance %1.10f)", temp.tx, temp.ty, temp.sxy, distance); 
#endif
				
				if (distance < bestDistance2) {
					
					bestDistance2 = distance;
					if (bestDistance2 < bestDistance) {
						bestX = temp;
						scaleDidChange = YES;
#ifdef ALIGNMENT_DEBUG
						NSLog(@"scale change %f", temp.sxy);
#endif
						
					}
				}
			}
			
			x = bestX;
			if (scaleDidChange) {
#ifdef ALIGNMENT_DEBUG
				NSLog(@"--------------------------------------------------------------------------------------------------------");
				NSLog(@"tried scale adjustment: new == %f", x.sxy);
				NSLog(@"--------------------------------------------------------------------------------------------------------");

#endif
			}
			
		}
		
		range.tx *= 1.0/downsampleFactorIncrease;
		range.ty *= 1.0/downsampleFactorIncrease;
		range.sxy *= 1.0/downsampleFactorIncrease;
	}
	
	
	
	// final alignment
	// ------------------------------------------------
	// ------------------------------------------------
	
	
	BOOL translationDidChange = YES;
	while (translationDidChange) {
		
		translationDidChange = NO;
		
		// determine the best x offset for this level
		// ------------------------------------------------
		
		bestDistance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:x forImage:second] andScale:1.0/(float)currentFactor display:YES];
		bestX.tx = x.tx;
		
		for (int xoff = -1; xoff < 2; xoff++) {
			
			// was already handled one level above
			if (xoff == 0) continue;
			
			IAParameters temp = x;
			temp.tx += (xoff * range.tx / 3.);
			
			//NSLog(@"try:    tx %f   ty %f    sxy %f    (distance %f)", temp.tx, temp.ty, temp.sxy, bestDistance); 
			
			distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:temp forImage:second] andScale:1.0/(float)currentFactor display:YES];
			if (distance < bestDistance) {
				bestDistance = distance;
				bestX = temp;
				translationDidChange = YES;
			}
		}
		x = bestX;
		
		// determine the best y offset for this level
		// ------------------------------------------------
		
		bestDistance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:x forImage:second] andScale:1.0/(float)currentFactor display:YES];
		bestX.ty = x.ty;
		
		// determine the best x offset for this level
		for (int yoff = -1; yoff < 2; yoff++) {
			
			// was already handled one level above
			if (yoff == 0) continue;
			
			IAParameters temp = x;
			temp.ty += (yoff * range.ty / 3.);
			
			//NSLog(@"try:    tx %f   ty %f    sxy %f    (distance %f)", temp.tx, temp.ty, temp.sxy, bestDistance); 
			
			distance = [self distanceBetweenImage:first andImage:second withTrafo:[self trafoWithParameters:temp forImage:second] andScale:1.0/(float)currentFactor display:YES];
			if (distance < bestDistance) {
				bestDistance = distance;
				bestX = temp;
				translationDidChange = YES;
			}
		}
		x = bestX;
		
	}
	
	
	
	//NSLog(@"best x:    tx %f   ty %f    sxy %f    (distance %f)", x.tx, x.ty, x.sxy, bestDistance); 
	
	
	
	
	return x;
	
}


#pragma mark -
#pragma mark Utility


- (NSAffineTransform *)trafoWithParameters:(IAParameters)params forImage:(CIImage *)image
{
	NSAffineTransform *trafo = [NSAffineTransform transform];
	CGRect extent = [image extent];
	
	// scale
	[trafo translateXBy:0.5 * extent.size.width yBy:0.5 * extent.size.height];
	[trafo scaleBy:params.sxy];
	[trafo translateXBy:-0.5 * extent.size.width yBy:-0.5 * extent.size.height];
	
	// translation
	[trafo translateXBy:params.tx yBy:params.ty];
	
	return trafo;
}



@end
