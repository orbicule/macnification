//
//  Experiment.m
//  Filament
//
//  Created by Peter Schols on 20/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "Experiment.h"


@implementation Experiment

- (void)awakeFromInsert
{
	[super awakeFromInsert];
	
	[self setValue:[NSDate date] forKey:@"date"];
	
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	// this is the new dependent key notification trigger system in 10.5.  The old setKeys:triggerNotificationsFor...: is deprecated.
	if ([key isEqualToString:@"summary"])
		return [NSSet setWithObjects:@"id", @"experimentName", @"type", @"date", @"comment", nil];
	else
		return [super keyPathsForValuesAffectingValueForKey:key];
}



- (NSString *)summary;
{
	NSString *expID = [self valueForKey:@"id"];
	if (!expID || [expID length] == 0) expID = @"No ID";
	
	NSString *name = [self valueForKey:@"experimentName"];
	if (!name || [name length] == 0) name = @"No name";
	
	NSString *type = [self valueForKey:@"type"];
	if (!type || [type length] == 0) type = @"No type";
	
	NSString *date = [[self valueForKey:@"date"] descriptionWithCalendarFormat:@"%d/%m/%y" timeZone:nil locale:nil];;
	if (!date || [date length] == 0) date = @"No date";
	
	NSString *comment = [self valueForKey:@"comment"];
	if (!comment || [comment length] == 0) comment = @"No comment";
	
	return [NSString stringWithFormat:@"%@ - %@\n%@\n%@\n%@", expID, name, type, date, comment];
}


@end
