//
//  MGMDTokenField.m
//  MetaData
//
//  Created by Dennis Lorson on 7/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import "MGMDTokenField.h"

#import "MGMDFieldView.h"
#import "MGMDConstants.h"

#import "MGMDKeywordView.h"

#import "ImageArrayController.h"


@implementation MGMDTokenField

@synthesize raisedEditingActive;


- (void)tokenFieldCellDidTokenizeString:(NSTokenFieldCell*)tokenFieldCell
{
	if([[self delegate] respondsToSelector:@selector(tokenFieldDidTokenizeString:)])
	{
		[[self delegate] performSelector:@selector(tokenFieldDidTokenizeString:) withObject:self];
	}
	
	
	// set the key in our model
	// fix: we cant do this here because calling [self objectValue] causes the editing to validate,which recursively calls this method again...infinite loop.
	// solution: do it when the editing has ended only.
	//[controller setValue:[NSSet setWithArray:[self objectValue]] forKeyPath:@"selection.keywords"];
}

- (void)setEnabled:(BOOL)flag
{
	[super setEnabled:flag];
}


- (void)setRaisedEditingActive:(BOOL)flag
{
	raisedEditingActive = flag;
	
}

- (BOOL)resignFirstResponder
{	
	return NO;
}


- (BOOL)becomeFirstResponder
{	
	return NO;
}

- (BOOL)acceptsFirstResponder 
{
    return NO;
}

- (BOOL)isEditable
{
	
	return NO;
}

- (BOOL)isSelectable
{
	
	return NO;
}

- (BOOL)textShouldBeginEditing:(NSText *)textObject
{
	return NO;
	
}
- (NSSet *)draggedKeywords
{
	return [(MGMDKeywordView *)[self delegate] draggedKeywordsForTokenField:self];
}

- (void)mouseDown:(NSEvent *)theEvent
{
	[super mouseDown:theEvent];

	
}


@end
