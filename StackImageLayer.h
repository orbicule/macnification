//
//  StackImageLayer.h
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <QuickTime/QuickTime.h>

@class StackImage;

@interface StackImageLayer : CATiledLayer 
{	
	StackImage *stackImage;
	BOOL contentsVisible;	
}

@property(readwrite, retain) StackImage *stackImage;
@property(readwrite) BOOL contentsVisible;


- (NSSize)dimensionsForImageAtPath:(NSString *)path;
- (CGImageRef)reflectionWithImage:(CGImageRef)original heightFraction:(CGFloat)frac;
- (CGImageRef)image;

@end
