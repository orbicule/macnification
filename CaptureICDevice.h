//
//  CaptureICDevice.h
//  Filament
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "CaptureDevice.h"

@interface CaptureICDevice : CaptureDevice <ICDeviceDelegate, ICCameraDeviceDownloadDelegate> 
{

	ICCameraDevice *originalDevice_;
	
	ICCameraItem *lastItem_;
    
    BOOL didBecomeReady_;
	
	NSImageView *preview_;
	
	// so we know when the download is completed
	NSMutableArray *filesInDownloadProgress_;
	
	// so we know which files to pass on to the delegate
	NSMutableArray *downloadedFiles_;
}

- (id)initWithDevice:(ICCameraDevice *)device;


@end
