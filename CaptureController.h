//
//  CaptureController.h
//  Filament
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>
#import <ImageCaptureCore/ImageCaptureCore.h>
#import <ImageKit/ImageKit.h>

#import "CaptureDevice.h"
#import "CaptureOpenGLPreview.h"
#import "MGHUDSlider.h"
#import "MGImageBatchImporter.h"
#import "Experiment.h"

@class MGContextualToolStrip;
@class SourceListTreeController;
@class CaptureOpenGLPreview;
@class MAAttachedWindow;
@class CaptureAdjustmentsController;
@class MGCaptureAdjustmentsWindow;


@interface CaptureController : NSObject <CaptureDeviceDelegate, NSWindowDelegate, ViewPickerDelegate, MGHUDSliderDelegate, MGImageBatchImporterDelegate>
{
	CaptureDevice *device_;
	SourceListTreeController *sourceListTreeController_;
		
	IBOutlet NSView *view_;
	
	IBOutlet NSView *controlsEmbeddingView_;
			
	IBOutlet QTCaptureView *qtCapturePreview_;
	IBOutlet CaptureOpenGLPreview *oglCaptureView_;
	IBOutlet NSImageView *historyImageView_;
	
	IBOutlet NSView *infoBarView_;
	IBOutlet NSTextField *infoBarText_;
	
	IBOutlet NSButton *liveViewButton_;
	
	IBOutlet IKImageBrowserView *historyBrowserView_;
	
	IBOutlet MGContextualToolStrip *toolStrip_;
	NSButton *adjustmentsButton_;
	
	
	// metadata
	
	IBOutlet NSWindow *metadataSheet_;
	IBOutlet NSTextField *metadataNameField_;
	IBOutlet NSButton *metadataNameUseSeriesNumberButton_;
	NSString *metadataName_;
	NSInteger metadataSeriesNumber_;
	BOOL metadataNameUseSeriesNumber_;
    
    IBOutlet NSPopUpButton *metadataExperimentsPopup_;
    
    Experiment *selectedExperiment_;
	
	
	// adjustments
	
	BOOL adjustmentsHUDisVisible_;
	IBOutlet MGCaptureAdjustmentsWindow *adjustmentsHUD_;
	IBOutlet NSView *adjustmentsView_;

	CaptureAdjustmentsController *adjustmentsController_;
	
	IBOutlet MGHUDSlider *exposureSlider_, *brightnessSlider_, *contrastSlider_, *saturationSlider_;
	IBOutlet NSButton *exposureAutoCheckbox_, *whitePointAutoCheckbox_;
	IBOutlet NSButton *whitePointButton_;
	
	BOOL isPickingWhitePoint_;
	
	BOOL isOpen_;
    
    NSMutableArray *filesBeingImported_;

}

@property (nonatomic, retain) SourceListTreeController *sourceListTreeController;


- (IBAction)showMetadataSheet:(id)sender;
- (IBAction)endMetadataSheet:(id)sender;
- (IBAction)showLivePreview:(id)sender;

- (IBAction)toggleMDSplitView:(id)sender;

- (void)setDevice:(CaptureDevice *)device;

- (id)initWithSuperview:(NSView *)superview;
- (void)removeFilterPredicate;

- (void)stopSessions;
- (void)close;
- (void)open;

- (void)closeAdjustmentsHUD;
- (void)updateAdjustmentsHUDPosition;


- (IBAction)setBrightness:(id)sender;
- (IBAction)setContrast:(id)sender;
- (IBAction)setSaturation:(id)sender;
- (IBAction)setExposure:(id)sender;
- (IBAction)setAutoExposure:(id)sender;
- (IBAction)chooseWhitePoint:(id)sender;

- (IBAction)resetAdjustmentValues:(id)sender;


- (IBAction)takePicture:(id)sender;
- (IBAction)downloadAll:(id)sender;
- (IBAction)downloadCurrent:(id)sender;
- (IBAction)showMetadataSheet:(id)sender;

@end
