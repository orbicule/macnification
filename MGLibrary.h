//
//  MGLibrary.h
//  Filament
//
//  Created by Dennis Lorson on 03/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Album;



#define LIBRARY_COLLECTION_RANK						30000

#define LAST_IMPORT_COLLECTION_RANK					30001
#define LAST_MONTH_COLLECTION_RANK					30002

#define LAST_CAPTURE_COLLECTION_RANK				30003
#define PROJECTS_COLLECTION_RANK					30004




@class MGLibrary;


@protocol MGLibraryDelegate <NSObject>

- (void)libraryWillStartConversion:(MGLibrary *)library;
- (void)libraryDidCompleteConversion:(MGLibrary *)library;

@end



@interface MGLibrary : NSObject 
{

    NSString *bundlePath_;
    
    NSManagedObjectContext *ctx_;
    NSManagedObjectModel *model_;
    NSPersistentStoreCoordinator *coordinator_;
    
    BOOL libraryDidExist_;
    BOOL libraryNeedsPathConversion_;
    
    id <MGLibraryDelegate> delegate_;
    
    BOOL didStartConversion_;
    
    
    
    // the default Core Data Album objects; cached
	Album * projectsCollection_;
	Album * libraryGroup_;
	Album * lastImportGroup_;
	Album * lastMonthGroup_;
	Album * lastCaptureGroup_;
}

+ (void)setPrefsLibraryPath:(NSString *)path;
+ (NSString *)prefsLibraryPath;
+ (BOOL)libraryExistsAtPath:(NSString *)path;
+ (BOOL)legacyLibraryExists;
+ (NSString *)defaultLibraryPath;

- (id)initWithPath:(NSString *)path;
- (id)initWithDefaultPathUseDemoContents:(BOOL)populateWithDemoContents;
- (id)initWithPrefsPath;


- (void)setDelegate:(id <MGLibraryDelegate>)delegate;

- (NSManagedObjectModel *)managedObjectModel;
- (NSManagedObjectContext *)managedObjectContext;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;

- (NSString *)imageDataPath;
- (NSString *)editedDataPath;
- (NSString *)thumbnailDataPath;

- (NSArray *)executeRequestWithEntityName:(NSString *)name prefetchPaths:(NSArray *)paths returnsObjectsAsFaults:(BOOL)flag error:(NSError **)error limit:(NSUInteger)limit predicate:(NSPredicate *)pred;

// Fixed lists
- (Album *)libraryGroup;
- (Album *)lastImportGroup;
- (Album *)lastMonthGroup;
- (Album *)projectsCollection;
- (Album *)lastCaptureGroup;


@end
