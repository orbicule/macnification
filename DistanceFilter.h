//
//  SquareFilter.h
//  ImageAlignment
//
//  Created by Dennis Lorson on 19/10/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface DistanceFilter : CIFilter {
	
	CIImage *inputImage;
	CIImage *inputBackgroundImage;
	
}

@end
