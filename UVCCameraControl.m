#import "UVCCameraControl.h"



#define UVC_INPUT_TERMINAL_ID				0x01
#define UVC_PROCESSING_UNIT_ID				0x03
#define UVC_INTERFACE_VIRTUAL_ID			0x00

#define UVC_CONTROL_INTERFACE_CLASS			0x0E
#define UVC_CONTROL_INTERFACE_SUBCLASS		0x01

#define UVC_SET_CUR							0x01
#define UVC_GET_CUR							0x81
#define UVC_GET_MIN							0x82
#define UVC_GET_MAX							0x83
#define UVC_GET_RES							0x84
#define UVC_GET_DEF							0x87
#define UVC_GET_INFO						0x86

#define UVC_ERROR_CONTROL_SELECTOR			0x02




typedef struct 
{
	int min, max;
} 
uvc_range_t;

typedef struct 
{
	int unit;
	int selector;
	int size;
	BOOL sign;
	BOOL supported;
	BOOL hasRange;
	uvc_range_t range;
	int resolution;
	int def;
} 
uvc_control_info_t;

typedef struct 
{
	uvc_control_info_t autoExposure;
	uvc_control_info_t exposure;
    uvc_control_info_t exposureRelative;
	uvc_control_info_t whiteBalance;
	uvc_control_info_t autoWhiteBalance;
} 
uvc_controls_t ;



enum UVCControlCapabilities
{
	UVCControlGet =		1 << 0,
	UVCControlSet =		1 << 1
};

enum UVCControlExposureModes
{
	UVCControlExposureManualTimeManualIris =		1 << 0,
	UVCControlExposureAutoTimeAutoIris =			1 << 1,
	UVCControlExposureManualTimeAutoIris =			1 << 2,
	UVCControlExposureAutoTimeManualIris =			1 << 3

};


typedef enum UVCControlError_
{
	UVCControlNoError =			0x00,
	UVCControlNotReady =		0x01,
	UVCControlWrongState =		0x02,
	UVCControlPower =			0x03,
	UVCControlOutOfRange =		0x04,
	UVCControlInvalidUnit =		0x05,
	UVCControlInvalidRequest =	0x06,
	UVCControlUnknownError =	0xFF,
	
	UVCControlNoInterface =		0xFFFF
}
UVCControlError;



uvc_controls_t uvc_controls = {
	.autoExposure = {
		.unit = UVC_INPUT_TERMINAL_ID,
		.selector = 0x02,
		.size = 1,
		.sign = NO,
		.hasRange = NO,
	},
	.exposure = {
		.unit = UVC_INPUT_TERMINAL_ID,
		.selector = 0x04,
		.size = 4,
		.sign = NO,
		.hasRange = YES,
	},

    .exposureRelative = {
		.unit = UVC_INPUT_TERMINAL_ID,
		.selector = 0x05,
		.size = 1,
		.sign = NO,
		.hasRange = YES,
	},

};


NSString *BUIOKitErrorMessageString(kern_return_t err);
NSString *BUUVCControlErrorMessageString(UVCControlError err);


@interface UVCCameraControl ()

- (void)_retrieveControlProperties;
- (void)_retrievePropertiesForControl:(uvc_control_info_t *)control;

- (BOOL)_setData:(long)value forControl:(uvc_control_info_t *)control;
- (int)_getData:(int)type forControl:(uvc_control_info_t *)control;


- (int)_getDefaultValueForControl:(uvc_control_info_t *)control;
- (int)_getResolutionForControl:(uvc_control_info_t *)control;
- (uvc_range_t)_getRangeForControl:(uvc_control_info_t *)control;
- (float)_getValueForControl:(uvc_control_info_t *)control;

- (BOOL)_setValue:(float)value forControl:(uvc_control_info_t *)control;
- (BOOL)_supportsControl:(uvc_control_info_t *)control;

- (float)_mapValue:(float)value fromMin:(float)fromMin max:(float)fromMax toMin:(float)toMin max:(float)toMax;



- (IOUSBInterfaceInterface190 **)_getControlInterfaceWithDeviceInterface:(IOUSBDeviceInterface **)deviceInterface;

- (UVCControlError)_sendControlRequest:(IOUSBDevRequest)controlRequest;
- (UVCControlError)_currentError;

- (IOUSBDevRequest)_requestWithType:(int)type control:(uvc_control_info_t *)control dstValue:(void *)value;
- (IOUSBDevRequest)_requestWithType:(int)type direction:(int)direction length:(int)length selector:(int)selector unit:(int)unit dstValue:(void *)value;




@end



@implementation UVCCameraControl

#pragma mark -
#pragma mark Function discovery

- (void)_retrieveControlProperties
{
	[self _retrievePropertiesForControl:&uvc_controls.exposure];
	[self _retrievePropertiesForControl:&uvc_controls.autoExposure];
	
	NSLog(@"Device supports setting exposure modes: %@", uvc_controls.autoExposure.supported ? @"YES" : @"NO");
	
	if (!uvc_controls.autoExposure.supported)
		return;
	
	int modes = uvc_controls.autoExposure.resolution;
	
	NSLog(@"-----------------------");
	NSLog(@"Supported modes:");

	if (modes & UVCControlExposureAutoTimeAutoIris)
		NSLog(@"Auto shutter, auto iris");
	if (modes & UVCControlExposureAutoTimeManualIris)
		NSLog(@"Auto shutter, manual iris");
	if (modes & UVCControlExposureManualTimeAutoIris)
		NSLog(@"Manual shutter, auto iris");
	if (modes & UVCControlExposureManualTimeManualIris)
		NSLog(@"Manual shutter, manual iris");
	
	NSLog(@"-----------------------");
	
	NSLog(@"Will use manual exposure setting: %@", [self supportsManualExposure] ? @"YES" : @"NO");
	

}

- (void)_retrievePropertiesForControl:(uvc_control_info_t *)control
{
	control->supported = [self _supportsControl:control];
	
	if (!control->supported)
		return;
	
	control->resolution = [self _getResolutionForControl:control];
	
	if (control->hasRange)
		control->range = [self _getRangeForControl:control];
	
	control->def = [self _getDefaultValueForControl:control];
}





#pragma mark -
#pragma mark Public

- (BOOL)supportsManualExposure;
{
    static BOOL didLogDeviceType = NO;
    
    if (vendor_ == 0x0ac8 && product_ == 0x3420) {
        
        if (!didLogDeviceType) {
            NSLog(@"[CC] Blue Eyeball device found, disabling manual exposure");
            didLogDeviceType = YES;
        }
        return NO;
        
    }
    
    
	if (!uvc_controls.autoExposure.supported)
		return NO;
    
    
	
	int modes = uvc_controls.autoExposure.resolution;
    
	return modes & UVCControlExposureManualTimeManualIris;
}


- (BOOL)setAutoExposure:(BOOL)enabled 
{
	if (![self supportsManualExposure])
		return NO;
	
	if (!enabled)
		return [self _setData:UVCControlExposureManualTimeManualIris forControl:&uvc_controls.autoExposure];
	
	int modes = uvc_controls.autoExposure.resolution;

	if (modes & UVCControlExposureAutoTimeAutoIris)
		return [self _setData:UVCControlExposureAutoTimeAutoIris forControl:&uvc_controls.autoExposure];

	if (modes & UVCControlExposureAutoTimeManualIris)
		return [self _setData:UVCControlExposureAutoTimeManualIris forControl:&uvc_controls.autoExposure];
	
	if (modes & UVCControlExposureManualTimeAutoIris)
		return [self _setData:UVCControlExposureManualTimeAutoIris forControl:&uvc_controls.autoExposure];
	
	return NO;
}

- (BOOL)getAutoExposure 
{
	if (![self supportsManualExposure])
		return YES;
	
	int val = [self _getData:UVC_GET_CUR forControl:&uvc_controls.autoExposure];
	
	return (val != UVCControlExposureManualTimeManualIris);
}

- (BOOL)setExposure:(int)value 
{
    return [self _setData:(long)value forControl:&uvc_controls.exposure];
}

- (void)resetExposure
{
    [self setExposure:[self getExposureDefault]];
}

- (int)getExposure
{    
    return [self _getData:UVC_GET_CUR forControl:&uvc_controls.exposure];
}

- (int)getExposureMax;
{
    return [self _getData:UVC_GET_MAX forControl:&uvc_controls.exposure];
}

- (int)getExposureMin;
{
    return [self _getData:UVC_GET_MIN forControl:&uvc_controls.exposure];
}

- (int)getExposureDefault;
{
    return [self _getData:UVC_GET_DEF forControl:&uvc_controls.exposure];
}

- (int)getExposureRelative
{    
    return [self _getData:UVC_GET_CUR forControl:&uvc_controls.exposureRelative];
}


#pragma mark -
#pragma mark Initialization



- (id)initWithDevice:(QTCaptureDevice *)device;
{
	NSString *uid = [device uniqueID];
	
	NSRange videoRange = [uid rangeOfString:@"-video" options:NSCaseInsensitiveSearch];
	
	if (videoRange.location != NSNotFound) {
		NSMutableString *adjusted = [[uid mutableCopy] autorelease];
		[adjusted deleteCharactersInRange:videoRange];
		uid = adjusted;
	}
	
	unsigned int locationID = 0;
	sscanf([uid UTF8String], "0x%8x", &locationID);
	
	self = [self initWithLocationID:(UInt32)locationID];
    
    return self;
}



- (id)initWithLocationID:(UInt32)locationID 
{
	if ((self = [super init])) {
		
		interface = NULL;
		
		// Find All USB Devices, get their locationId and check if it matches the requested one
		CFMutableDictionaryRef matchingDict = IOServiceMatching(kIOUSBDeviceClassName);
		io_iterator_t serviceIterator;
		IOServiceGetMatchingServices( kIOMasterPortDefault, matchingDict, &serviceIterator );
		
		io_service_t camera;
		while ((camera = IOIteratorNext(serviceIterator))) {
			
			// Get DeviceInterface
			IOUSBDeviceInterface **deviceInterface = NULL;
			IOCFPlugInInterface	**plugInInterface = NULL;
			SInt32 score;
			kern_return_t kr = IOCreatePlugInInterfaceForService( camera, kIOUSBDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &plugInInterface, &score );
			if( (kIOReturnSuccess != kr) || !plugInInterface ) {
				NSLog(@"IOCreatePlugInInterfaceForService error (%@)", BUIOKitErrorMessageString(kr));
				continue;
			}
			
			HRESULT res = (*plugInInterface)->QueryInterface(plugInInterface, CFUUIDGetUUIDBytes(kIOUSBDeviceInterfaceID), (LPVOID*) &deviceInterface );
			(*plugInInterface)->Release(plugInInterface);
			if( res || deviceInterface == NULL ) {
				NSLog(@"QueryInterface error %d.\n", (int)res );
				continue;
			}
			
			UInt32 currentLocationID = 0;
			(*deviceInterface)->GetLocationID(deviceInterface, &currentLocationID);
			
			if(currentLocationID == locationID) {
				interface = [self _getControlInterfaceWithDeviceInterface:deviceInterface];
				[self _retrieveControlProperties];
                
                
                (*deviceInterface)->GetDeviceVendor(deviceInterface, &vendor_);
                (*deviceInterface)->GetDeviceProduct(deviceInterface, &product_);
                
				return self;
			}
			
			
		}
		
		if (!interface)
			NSLog(@"Couldn't create device interface for the requested location ID");
		
		
	}
	return self;
}





- (void)dealloc
{
	if(interface) {
		(*interface)->USBInterfaceClose(interface);
		(*interface)->Release(interface);
	}
	[super dealloc];
}


- (IOUSBInterfaceInterface190 **)_getControlInterfaceWithDeviceInterface:(IOUSBDeviceInterface **)deviceInterface
{
	IOUSBInterfaceInterface190 **controlInterface;
	
	io_iterator_t interfaceIterator;
	IOUSBFindInterfaceRequest interfaceRequest;
	interfaceRequest.bInterfaceClass = UVC_CONTROL_INTERFACE_CLASS;
	interfaceRequest.bInterfaceSubClass = UVC_CONTROL_INTERFACE_SUBCLASS;
	interfaceRequest.bInterfaceProtocol = kIOUSBFindInterfaceDontCare;
	interfaceRequest.bAlternateSetting = kIOUSBFindInterfaceDontCare;
	
	IOReturn success = (*deviceInterface)->CreateInterfaceIterator( deviceInterface, &interfaceRequest, &interfaceIterator );
	if( success != kIOReturnSuccess ) {
		NSLog(@"Unable to list interfaces;  CreateInterfaceIterator error (%@)", BUIOKitErrorMessageString(success));
		return NULL;
	}
	
	io_service_t usbInterface;
	HRESULT result;
	
	
	if((usbInterface = IOIteratorNext(interfaceIterator))) {
		IOCFPlugInInterface **plugInInterface = NULL;
		
		//Create an intermediate plug-in
		SInt32 score;
		IOCreatePlugInInterfaceForService( usbInterface, kIOUSBInterfaceUserClientTypeID, kIOCFPlugInInterfaceID, &plugInInterface, &score );
		
		//Release the usbInterface object after getting the plug-in
		kern_return_t kr = IOObjectRelease(usbInterface);
		if( (kr != kIOReturnSuccess) || !plugInInterface ) {
			NSLog(@"Unable to create a plug-in;  IOCreatePlugInInterfaceForService error (%@)", BUIOKitErrorMessageString(kr));
			return NULL;
		}
		
		//Now create the device interface for the interface
		result = (*plugInInterface)->QueryInterface( plugInInterface, CFUUIDGetUUIDBytes(kIOUSBInterfaceInterfaceID), (LPVOID *) &controlInterface );
		
		//No longer need the intermediate plug-in
		(*plugInInterface)->Release(plugInInterface);
		
		if( result || !controlInterface ) {
			NSLog(@"Couldn’t create a device interface for the interface, QueryInterface error (%@)", BUIOKitErrorMessageString(kr));
			return NULL;
		}
		
		return controlInterface;
	}
	
	NSLog(@"No device interface found");

	
	return NULL;
}


#pragma mark -
#pragma mark Control request wrappers





- (int)_getDefaultValueForControl:(uvc_control_info_t *)control 
{	
	return [self _getData:UVC_GET_DEF forControl:control];
}

- (int)_getResolutionForControl:(uvc_control_info_t *)control
{	
	return [self _getData:UVC_GET_RES forControl:control];
}

- (uvc_range_t)_getRangeForControl:(uvc_control_info_t *)control 
{
	uvc_range_t range = { 0, 0 };
	range.min = [self _getData:UVC_GET_MIN forControl:control];
	range.max = [self _getData:UVC_GET_MAX forControl:control];
	return range;
}



- (float)_getValueForControl:(uvc_control_info_t *)control 
{
	uvc_range_t range = [self _getRangeForControl:control];
	
	int intval = [self _getData:UVC_GET_CUR forControl:control];
	return [self _mapValue:intval fromMin:range.min max:range.max toMin:0 max:1];
}


- (BOOL)_setValue:(float)value forControl:(uvc_control_info_t *)control 
{
	uvc_range_t range = [self _getRangeForControl:control];
	
	int intval = [self _mapValue:value fromMin:0 max:1 toMin:range.min max:range.max];
	return [self _setData:intval forControl:control];
}

- (BOOL)_supportsControl:(uvc_control_info_t *)control
{
	// find out if the control supports SET requests
	int flags = [self _getData:UVC_GET_INFO forControl:control];
	
	return (flags & UVCControlGet) && (flags & UVCControlSet);
}



- (float)_mapValue:(float)value fromMin:(float)fromMin max:(float)fromMax toMin:(float)toMin max:(float)toMax 
{
	return toMin + (toMax - toMin) * ((value - fromMin) / (fromMax - fromMin));
}


#pragma mark -
#pragma mark Control requests



- (BOOL)_setData:(long)value forControl:(uvc_control_info_t *)control 
{
	long ioVal = value;
	IOUSBDevRequest req = [self _requestWithType:UVC_SET_CUR control:control dstValue:&ioVal];
	
	UVCControlError err = [self _sendControlRequest:req];
	
	return err == UVCControlNoError;
}


- (int)_getData:(int)type forControl:(uvc_control_info_t *)control
{
	int value = 0;
	
	IOUSBDevRequest req = [self _requestWithType:type control:control dstValue:&value];
	UVCControlError err = [self _sendControlRequest:req];
	
	return ( err == UVCControlNoError ? value : 0 );
}



- (IOUSBDevRequest)_requestWithType:(int)type control:(uvc_control_info_t *)control dstValue:(void *)value
{
	int dir = (type == UVC_SET_CUR) ? kUSBOut : kUSBIn;
	
	return [self _requestWithType:type direction:dir length:control->size selector:control->selector unit:control->unit dstValue:value];
}


- (IOUSBDevRequest)_requestWithType:(int)type direction:(int)direction length:(int)length selector:(int)selector unit:(int)unit dstValue:(void *)value
{
	IOUSBDevRequest controlRequest;
	controlRequest.bmRequestType = USBmakebmRequestType( direction, kUSBClass, kUSBInterface );
	controlRequest.bRequest = type;
	controlRequest.wValue = (selector << 8) | 0x00;
	controlRequest.wIndex = (unit << 8) | 0x00;
	controlRequest.wLength = length;
	controlRequest.wLenDone = 0;
	controlRequest.pData = value;
	
	return controlRequest;
}


- (UVCControlError)_sendControlRequest:(IOUSBDevRequest)controlRequest 
{
	if(!interface ) {
		NSLog(@"No interface to send USB request");
		return UVCControlNoInterface;
	}
	
	kern_return_t kr = (*interface)->ControlRequest(interface, 0, &controlRequest);
	
	if (kr != kIOReturnSuccess) {
		
		if (kr != kIOUSBPipeStalled) {
			NSLog(@"USB request failed: error (%@)", BUIOKitErrorMessageString(kr));
			return UVCControlUnknownError;
		} else {
			UVCControlError err = [self _currentError];
			NSLog(@"USB request failed: error (%@)", BUUVCControlErrorMessageString(err));
			return err;
		}
		
	}
	
	return UVCControlNoError;
}

- (UVCControlError)_currentError;
{
	int retVal = 0;
	
	IOUSBDevRequest req = [self _requestWithType:UVC_GET_CUR direction:kUSBIn length:1 selector:UVC_ERROR_CONTROL_SELECTOR unit:UVC_INTERFACE_VIRTUAL_ID dstValue:&retVal];
	
	if(!interface ) {
		NSLog(@"Failed to get error code: No interface to send USB request");
		return UVCControlNoInterface;
	}
	
	kern_return_t kr = (*interface)->ControlRequest(interface, 0, &req);
	
	if (kr != kIOReturnSuccess) {
		NSLog(@"Failed to get error code (nested error %@)", BUIOKitErrorMessageString(kr));
		
	}
	
	return (UVCControlError)retVal;
}


#pragma mark -
#pragma mark Errors

NSString *BUUVCControlErrorMessageString(UVCControlError err)
{
	switch (err) {
		case UVCControlNoError:
			return nil;
		case UVCControlNotReady:
			return @"UVCControlNotReady";
		case UVCControlWrongState:
			return @"UVCControlWrongState";
		case UVCControlPower:
			return @"UVCControlPower";
		case UVCControlOutOfRange:
			return @"UVCControlOutOfRange";
		case UVCControlInvalidUnit:
			return @"UVCControlInvalidUnit";
		case UVCControlInvalidRequest:
			return @"UVCControlInvalidRequest";
		case UVCControlUnknownError:
		default:
			return @"UVCControlUnknownError";
		case UVCControlNoInterface:
			return @"UVCControlNoInterface";
	}
}


NSString *BUIOKitErrorMessageString(kern_return_t err)
{
	if (err == KERN_SUCCESS)
		return nil;
	
	NSString *errStr = nil;
	
	switch (err) {
			
		case kIOReturnError:
			errStr = @"kIOReturnError";
			break;
		case kIOReturnNoMemory:
			errStr = @"kIOReturnNoMemory";
			break;
		case kIOReturnNoResources:
			errStr = @"kIOReturnNoResources";
			break;
		case kIOReturnIPCError:
			errStr = @"kIOReturnIPCError";
			break;
		case kIOReturnNoDevice:
			errStr = @"kIOReturnNoDevice";
			break;
		case kIOReturnNotPrivileged:
			errStr = @"kIOReturnNotPrivileged";
			break;
		case kIOReturnBadArgument:
			errStr = @"kIOReturnBadArgument";
			break;
		case kIOReturnLockedRead:
			errStr = @"kIOReturnLockedRead";
			break;
		case kIOReturnLockedWrite:
			errStr = @"kIOReturnLockedWrite";
			break;
		case kIOReturnExclusiveAccess:
			errStr = @"kIOReturnExclusiveAccess";
			break;
		case kIOReturnBadMessageID:
			errStr = @"kIOReturnBadMessageID";
			break;
		case kIOReturnUnsupported:
			errStr = @"kIOReturnUnsupported";
			break;
		case kIOReturnVMError:
			errStr = @"kIOReturnVMError";
			break;
		case kIOReturnInternalError:
			errStr = @"kIOReturnInternalError";
			break;
		case kIOReturnIOError:
			errStr = @"kIOReturnIOError";
			break;
		case kIOReturnCannotLock:
			errStr = @"kIOReturnCannotLock";
			break;
		case kIOReturnNotOpen:
			errStr = @"kIOReturnNotOpen";
			break;
		case kIOReturnNotReadable:
			errStr = @"kIOReturnNotReadable";
			break;
		case kIOReturnNotWritable:
			errStr = @"kIOReturnNotWritable";
			break;
		case kIOReturnNotAligned:
			errStr = @"kIOReturnNotAligned";
			break;
		case kIOReturnBadMedia:
			errStr = @"kIOReturnBadMedia";
			break;
		case kIOReturnStillOpen:
			errStr = @"kIOReturnStillOpen";
			break;
		case kIOReturnRLDError:
			errStr = @"kIOReturnRLDError";
			break;
		case kIOReturnDMAError:
			errStr = @"kIOReturnDMAError";
			break;
		case kIOReturnBusy:
			errStr = @"kIOReturnBusy";
			break;
		case kIOReturnTimeout:
			errStr = @"kIOReturnTimeout";
			break;
		case kIOReturnOffline:
			errStr = @"kIOReturnOffline";
			break;
		case kIOReturnNotReady:
			errStr = @"kIOReturnNotReady";
			break;
		case kIOReturnNotAttached:
			errStr = @"kIOReturnNotAttached";
			break;
		case kIOReturnNoChannels:
			errStr = @"kIOReturnNoChannels";
			break;
		case kIOReturnNoSpace:
			errStr = @"kIOReturnNoSpace";
			break;
		case kIOReturnPortExists:
			errStr = @"kIOReturnPortExists";
			break;
		case kIOReturnCannotWire:
			errStr = @"kIOReturnCannotWire";
			break;
		case kIOReturnNoInterrupt:
			errStr = @"kIOReturnNoInterrupt";
			break;
		case kIOReturnNoFrames:
			errStr = @"kIOReturnNoFrames";
			break;
		case kIOReturnMessageTooLarge:
			errStr = @"kIOReturnMessageTooLarge";
			break;
		case kIOReturnNotPermitted:
			errStr = @"kIOReturnNotPermitted";
			break;
		case kIOReturnNoPower:
			errStr = @"kIOReturnNoPower";
			break;
		case kIOReturnNoMedia:
			errStr = @"kIOReturnNoMedia";
			break;
		case kIOReturnUnformattedMedia:
			errStr = @"kIOReturnUnformattedMedia";
			break;
		case kIOReturnUnsupportedMode:
			errStr = @"kIOReturnUnsupportedMode";
			break;
		case kIOReturnUnderrun:
			errStr = @"kIOReturnUnderrun";
			break;
		case kIOReturnOverrun:
			errStr = @"kIOReturnOverrun";
			break;
		case kIOReturnDeviceError:
			errStr = @"kIOReturnDeviceError";
			break;
		case kIOReturnNoCompletion:
			errStr = @"kIOReturnNoCompletion";
			break;
		case kIOReturnAborted:
			errStr = @"kIOReturnAborted";
			break;
		case kIOReturnNoBandwidth:
			errStr = @"kIOReturnNoBandwidth";
			break;
		case kIOReturnNotResponding:
			errStr = @"kIOReturnNotResponding";
			break;
		case kIOReturnIsoTooOld:
			errStr = @"kIOReturnIsoTooOld";
			break;
		case kIOReturnIsoTooNew:
			errStr = @"kIOReturnIsoTooNew";
			break;
		case kIOReturnNotFound:
			errStr = @"kIOReturnNotFound";
			break;
		case kIOReturnInvalid:
			errStr = @"kIOReturnInvalid";
			break;
		default:
			errStr = [NSString stringWithFormat:@"Unknown error %08x", err];
			
	}
	
	return errStr;

}


@end
