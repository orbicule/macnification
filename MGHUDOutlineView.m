//
//  MGHUDOutlineView.m
//  Filament
//
//  Created by Dennis Lorson on 30/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDOutlineView.h"


@interface NSOutlineView (PrivateAdditions)

- (void)_sendDelegateWillDisplayCell:(id)cell forColumn:(id)column row:(int)row;

@end


@implementation MGHUDOutlineView




- (void)highlightSelectionInClipRect:(NSRect)clipRect
{
	NSIndexSet *rows = [self selectedRowIndexes];
	
	NSInteger index = 0;
	while (YES) {
		
		index = [rows indexGreaterThanOrEqualToIndex:index];
		
		if (index == NSNotFound) break;
		
		NSRect rowRect = [self rectOfRow:index];
		
		NSGradient *gradient = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.5 alpha:1.0]
															  endingColor:[NSColor colorWithCalibratedWhite:0.5 alpha:1.0]] autorelease];
		
		[gradient drawInRect:rowRect angle:90];
		index++;
		
	}
	
	
}


- (void)selectRowIndexes:(NSIndexSet *)indexes byExtendingSelection:(BOOL)extend
{
	NSInteger index = [indexes firstIndex];
	
	if (index != NSNotFound)
		[self scrollRowToVisible:index];
	
	[super selectRowIndexes:indexes byExtendingSelection:extend];
	
}



- (id)_alternatingRowBackgroundColors
{
	static NSArray* _altColors = nil;
    if (!_altColors) {
        _altColors = [[NSArray arrayWithObjects:
					   [NSColor colorWithCalibratedWhite:0.085 alpha:1.0], 
					   [NSColor colorWithCalibratedWhite:0.05 alpha:1.0], 
					   nil] retain];
    }
    
    return _altColors;
	
	
	
}


- (id)_highlightColorForCell:(id)cell
{
    static NSColor* _highlightCellColor = nil;
    if (!_highlightCellColor) {
        _highlightCellColor = [[NSColor colorWithCalibratedWhite:0.1f alpha:1.0] retain];
    }
    
    return _highlightCellColor;
}


- (void)_sendDelegateWillDisplayCell:(id)cell forColumn:(id)column row:(int)row
{
	// avoid the compiler warning
    [super _sendDelegateWillDisplayCell:cell forColumn:column row:row];
    
    // Set text color
    if ([cell respondsToSelector:@selector(setTextColor:)]) {
        [cell setTextColor:[NSColor whiteColor]];
    }
}



@end
