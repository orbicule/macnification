//
//  MGMDFieldView.m
//  MetaDataView
//
//  Created by Dennis on 8/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGMDFieldView.h"
#import "MGMDView.h"
#import "MGMDTextField.h"
#import "MGMDRatingView.h"
#import "MGMDTokenField.h"
#import "MGMDBooleanView.h"
#import "MGMDReadOnlyTextField.h"
#import "MGMDExperimentView.h"
#import "ImageArrayController.h"
#import "MGMDKeywordView.h"
#import "MGLibraryController.h"
#import "MGAppDelegate.h"

#import <QuartzCore/QuartzCore.h>


static const CGFloat MGTextFieldHeight = 15.0;
static const CGFloat labelContentsMargin = 5.0;

static const CGFloat MGFieldHorizontalMarginLeft = 15;
static const CGFloat MGFieldHorizontalMarginRight = 10;
static const CGFloat MGFieldVerticalMargin = 5;

static const CGFloat MGFieldEditorPadding = 2.0;


@implementation MGMDFieldView

 
@dynamic minimumHeight, minimumLabelWidth;
@synthesize isBeingDragged, isPlaceholder, editModeOn, raisedEditingActive, fieldIdentifier, titleMenu, fieldContents, modelValue;




#pragma mark -
#pragma mark Initialization 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dealloc
{
	self.fieldIdentifier = nil;
	self.titleMenu = nil;
	
	[observedObject release];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	
	// release the top level objects of the nib if we are a channel view container
	if (channelTopLevelObjects)
		for (id topLevelObj in channelTopLevelObjects) {
			if (![topLevelObj isMemberOfClass:[NSApplication class]]) {
				//NSLog(@"releasing %@", topLevelObj);
				[topLevelObj release];
			}
		}
		

	if (experimentTopLevelObjects)
		for (id topLevelObj in experimentTopLevelObjects) {
			if (![topLevelObj isMemberOfClass:[NSApplication class]]) {
				//NSLog(@"releasing %@", topLevelObj);
				[topLevelObj release];
			}
		}
	
	
	
	[super dealloc];
}

- (NSString *)description
{
	return [[super description] stringByAppendingFormat:@" -- identifier = %@", self.fieldIdentifier];
	
}

- (id)initAsSeparator
{
	MGMDFieldView *newField = [self initWithFrame:NSMakeRect(0,0,200, 25)];
	
	self.fieldIdentifier = @"MGSeparator";
	
	fieldContents = nil;
	
	[newField setAutoresizingMask:NSViewWidthSizable];
	
	[newField setAlphaValue:0.0];
	[[newField animator] setAlphaValue:1.0];
	
	NSBox *box = [[[NSBox alloc] initWithFrame:NSMakeRect(MGFieldHorizontalMarginLeft + 10,
														  12, 
														  200 - 2 * MGFieldHorizontalMarginLeft - 20, // use twice the same margin to center the separator
														  1)] autorelease];
	[box setBoxType:NSBoxSeparator];
	[box setBorderColor:[NSColor blackColor]];
	[box setFillColor:[NSColor blackColor]];
	
	[box setAutoresizingMask:NSViewWidthSizable];
	
	[newField addSubview:box];
	
	return newField;
	
}

- (id)initAsPlaceholderForField:(MGMDFieldView *)field
{
	MGMDFieldView *newField = [self initWithFrame:[field frame]];
	newField.isPlaceholder = YES;
	
	return newField;
}

- (id)initWithIdentifier:(NSString *)theIdentifier title:(NSString *)title
{
	NSString *arrow = @"";//[NSString stringWithUTF8String:"\u25BE "];
		
	channelView = nil;
	channelTopLevelObjects = nil;
	
	experimentView = nil;
	experimentTopLevelObjects = nil;

	keywordView = nil;
	keywordTopLevelObjects = nil;
	
	self.fieldIdentifier = theIdentifier;
	
	// title is needed only when using a custom attr.
	
	MGMDFieldView *newField = [self initWithFrame:NSMakeRect(0,0,200 + MGFieldHorizontalMarginLeft + MGFieldHorizontalMarginRight, MGTextFieldHeight + 2 * MGFieldVerticalMargin)];
	
	fieldLabel = [[[NSTextField alloc] initWithFrame:NSMakeRect(MGFieldHorizontalMarginLeft,
																MGFieldVerticalMargin, 
																100, 
																MGTextFieldHeight)] autorelease];
	[fieldLabel setAutoresizingMask:(NSViewWidthSizable | NSViewMinYMargin)];
	[fieldLabel setFont:[NSFont boldSystemFontOfSize:10]];
	[fieldLabel setAlignment:NSRightTextAlignment];
	[fieldLabel setBezeled:NO];
	[fieldLabel setSelectable:NO];
	[[fieldLabel cell] setDrawsBackground:NO];
	[newField addSubview:fieldLabel];
	
	
	labelTrackingArea = [[NSTrackingArea alloc] initWithRect:[fieldLabel frame] 
													 options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp)
													   owner:self 
													userInfo:nil];
	
	
	if ([fieldIdentifier isEqualToString:@"instrument.channels"]) {
        
		[fieldLabel setStringValue:@"channels"];
        
        
        NSNib *channelNib = [[[NSNib alloc] initWithNibNamed:@"MetadataChannels.nib" bundle:[NSBundle mainBundle]] autorelease];
        [channelNib instantiateNibWithOwner:self topLevelObjects:&channelTopLevelObjects];
        [channelTopLevelObjects retain];
        
        fieldContents = (NSView *)channelView;
        
        NSRect contentsFrame;
        contentsFrame.origin.x = 100 + MGFieldHorizontalMarginLeft;
        contentsFrame.origin.y = MGFieldVerticalMargin;
        contentsFrame.size.height = [fieldContents frame].size.height;
        contentsFrame.size.width = 100;
        NSRect ownFrame = [self frame];
        ownFrame.size.height = contentsFrame.size.height + 2 * MGFieldVerticalMargin;
        
        [self setFrame:ownFrame];
        [fieldContents setFrame:contentsFrame];
        
        //[fieldContents setFrameOrigin:NSMakePoint(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin)];
        
        
    } 
    
    
    else if ([fieldIdentifier isEqualToString:@"experiment"]) {
        
        [fieldLabel setStringValue:@"experiment"];
        hasMenu =YES;
		
		NSNib *channelNib = [[[NSNib alloc] initWithNibNamed:@"MetadataExperiment.nib" bundle:[NSBundle mainBundle]] autorelease];
		[channelNib instantiateNibWithOwner:self topLevelObjects:&experimentTopLevelObjects];
		[experimentTopLevelObjects retain];
		
		fieldContents = (NSView *)experimentView;
		
		NSRect contentsFrame;
		contentsFrame.origin.x = 100 + MGFieldHorizontalMarginLeft;
		contentsFrame.origin.y = MGFieldVerticalMargin;
		contentsFrame.size.height = [fieldContents frame].size.height;
		contentsFrame.size.width = 100;
		NSRect ownFrame = [self frame];
		ownFrame.size.height = contentsFrame.size.height + 2 * MGFieldVerticalMargin;
		
		[self setFrame:ownFrame];
		[fieldContents setFrame:contentsFrame];
		
		[fieldContents bind:@"experimentsController" toObject:MGLibraryControllerInstance withKeyPath:@"managedObjectContext" options:nil];

		
	}
	
	else if ([fieldIdentifier isEqualToString:@"keywords"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"keywords"]];
		hasMenu = YES;
		
		NSNib *expNib = [[[NSNib alloc] initWithNibNamed:@"MetadataKeywords.nib" bundle:[NSBundle mainBundle]] autorelease];
		[expNib instantiateNibWithOwner:self topLevelObjects:&keywordTopLevelObjects];
		[keywordTopLevelObjects retain];
		
		fieldContents = (NSView *)keywordView;
		
		NSRect contentsFrame;
		contentsFrame.origin.x = 100 + MGFieldHorizontalMarginLeft;
		contentsFrame.origin.y = MGFieldVerticalMargin;
		contentsFrame.size.height = [fieldContents frame].size.height;
		contentsFrame.size.width = 100;
		NSRect ownFrame = [self frame];
		ownFrame.size.height = contentsFrame.size.height + 2 * MGFieldVerticalMargin;
		
		[self setFrame:ownFrame];
		[fieldContents setFrame:contentsFrame];
		
		
	}
	
	
	else if ([fieldIdentifier isEqualToString:@"comment"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"comments"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	} 
	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"rating"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"rating"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDRatingView alloc] initWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, [newField bounds].size.height)] autorelease];
				
	}
	
	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"isCalibrated"]) {
		
		hasMenu = YES;

		[fieldLabel setStringValue:@"calibrated"];
		
		fieldContents = [[[MGMDBooleanView alloc] initWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, [newField bounds].size.height)] autorelease];
		
	}
	
	

	
	else if ([fieldIdentifier isEqualToString:@"hasFilters"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"adjustments"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDBooleanView alloc] initWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, [newField bounds].size.height)] autorelease];
		
	}
	
	else if ([fieldIdentifier isEqualToString:@"roiCount"]) {
		
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"roi count"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDReadOnlyTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setFormat:@"0;0;-0"];
		[numberFormatter setMinimum:0];
		[numberFormatter setMaximumFractionDigits:0];
		[(MGMDTextField *)fieldContents setFormatter:numberFormatter];
		
		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
		
	}
	
	
	else if ([fieldIdentifier isEqualToString:@"pixelSizeDescription"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"pixel size"]];
		hasMenu = NO;
		
		fieldContents = [[[MGMDReadOnlyTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"experimenter.name"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"experimenter"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	

	
	else if ([fieldIdentifier isEqualToString:@"instrument.immersion"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"immersion"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.instrumentName"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"instrument"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.objectiveName"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"objective"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.magnification"]) {
		
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"magnification"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setHasThousandSeparators:YES];
		[numberFormatter setThousandSeparator:@","];
		[numberFormatter setFormat:@"0x;0x;-0x"];
		//[numberFormatter setPositiveSuffix:@"x"];
		[numberFormatter setMinimum:0];
		[numberFormatter setMaximumFractionDigits:5];
		[(MGMDTextField *)fieldContents setFormatter:numberFormatter];
		
		
		[(MGMDTextField *)fieldContents setDelegate:self];

		
	}
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.numericAperture"]) {
		
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"num. aperture"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setFormat:@"0;0;-0"];
		[numberFormatter setMinimum:0];
		[numberFormatter setMaximumFractionDigits:5];
		[(MGMDTextField *)fieldContents setFormatter:numberFormatter];
		
		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
		
	}

	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.voltage"]) {
		
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"voltage"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setFormat:@"0kV;0kV;-0kV"];
		[(MGMDTextField *)fieldContents setFormatter:numberFormatter];
		
		
		[(MGMDTextField *)fieldContents setDelegate:self];

		
	}
	
	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.spotSize"]) {
		
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"spot size"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setFormat:@"0nm;0nm;-0nm"];
		[numberFormatter setMinimum:0];
		[numberFormatter setMaximumFractionDigits:5];
		[(MGMDTextField *)fieldContents setFormatter:numberFormatter];
		
		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	
	
	
	
	else if ([fieldIdentifier isEqualToString:@"instrument.workingDistance"]) {
		
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"working dist."]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setFormat:@"0µm;0µm;-0µm"];
		[numberFormatter setMinimum:0];
		[numberFormatter setMaximumFractionDigits:5];
		[(MGMDTextField *)fieldContents setFormatter:numberFormatter];
		
		
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	else if ([fieldIdentifier isEqualToString:@"keywords"]) {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:@"keywords"]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTokenField alloc] initWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTokenField *)fieldContents cell] setWraps:YES];		
		[(MGMDTokenField *)fieldContents setDelegate:self];
		
		[(MGMDTokenField *)fieldContents setFocusRingType:NSFocusRingTypeNone];
		[(MGMDTokenField *)fieldContents setAutoresizingMask:(NSViewWidthSizable | NSViewHeightSizable)];
		[(MGMDTokenField *)fieldContents setFont:[NSFont systemFontOfSize:11]];
		[(MGMDTokenField *)fieldContents setBordered:NO];
		[(MGMDTokenField *)fieldContents setBezeled:NO];
		
		//[(MGMDTokenField *)fieldContents setSelectable:NO];
		
		[[(NSTokenField *)fieldContents cell] setDrawsBackground:NO];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenFieldEditorDidEndEditing:) name:@"MGTokenFieldResignFirstResponderNotification" object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenFieldEditorDidBeginEditing:) name:@"MGTokenFieldBecomeFirstResponderNotification" object:nil];
		
	}
	
	
	
	
	
	else {
		
		[fieldLabel setStringValue:[arrow stringByAppendingString:[title lowercaseString]]];
		hasMenu = YES;
		
		fieldContents = [[[MGMDTextField alloc] initContentsFieldWithFrame:NSMakeRect(100 + MGFieldHorizontalMarginLeft, MGFieldVerticalMargin, 100, MGTextFieldHeight)] autorelease];
		
		[[(MGMDTextField *)fieldContents cell] setWraps:YES];	
		[(MGMDTextField *)fieldContents setDelegate:self];
		
	}
	
	[newField addSubview:fieldContents];
	
	[newField updateContentsSize];
	
	
	[self performSelector:@selector(updateContentsSize) withObject:nil afterDelay:0.2];
	
	return newField;
}


#pragma mark -
#pragma mark Bindings 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bindings
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	
	if ([binding isEqualToString:@"value"]) {
		
		observedObject = [observableController retain];
		observedKeyPath = [keyPath copy];
		//[observableController addManualObserver:self forKeyPath:keyPath];
		
		// add an extra observer to track the selection.
		//[observableController addObserver:self forKeyPath:@"selection" options:0 context:nil];
		
		[self.fieldContents bind:@"value" toObject:observableController withKeyPath:keyPath options:nil];
		//[self.fieldContents bind:@"value" toObject:self withKeyPath:@"modelValue" options:nil];
		
	} else {
		
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
		
	}
	
}




- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
	
		[(ImageArrayController *)observedObject removeManualObserver:self];
		[observedObject release];
		[observedKeyPath release];
		observedObject = nil;
		observedKeyPath = nil;

	} else {
		
		[super unbind:binding];
		
	}
	
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"selection"]) {
		[self manualObserveValueForKeyPath:observedKeyPath ofObject:observedObject];
		
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}


- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object
{
	// gather the data and tell the fieldContents to display them in some manner.
	id value = [object valueForKeyPath:keyPath];
	
	//[fieldContents setDisplayValueWithModelValue:value atKeyPath:keyPath ofObject:object];
	self.modelValue = value;
	
	//NSLog(@"content changed %@", self.identifier);
	
	[self updateContentsSize];
		
}

- (void)pushValue:(id)value toModelKeyPath:(NSString *)keyPath;
{
	// our manual method to push changes in the fieldContents to the model
	[observedObject setValue:value forKeyPath:keyPath];
	
}




#pragma mark -
#pragma mark Accessors 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)setRaisedEditingActive:(BOOL)flag
{
	
	raisedEditingActive = flag;
	
	[self setNeedsDisplay:YES];
	
}


- (void)setEditModeOn:(BOOL)flag
{
	editModeOn = flag;
	
	[(NSControl *)fieldContents setEnabled:!flag];
		
	[self setNeedsDisplay:YES];
	
}

- (CGFloat)minimumHeight
{
	return [fieldContents frame].size.height + 2 * MGFieldVerticalMargin;
	
}

- (CGFloat)minimumLabelWidth
{																	 
	// returns the minimum width for the label of this field.  The line between label and contents of the field is set here, and the parent view uses the max of all these minima
	// to define the global minimum label width for a label.  This minimum label width is then applied on each of the label fields.
	
	return [[fieldLabel cell] cellSize].width;
	
}


- (NSMenu *)fieldMenu
{
	return [(MGMDView *)[self superview] menuForContentsOfField:self withIdentifier:fieldIdentifier];
	
}


- (NSString *)contentDescription
{
	if (![self.fieldContents respondsToSelector:@selector(contentDescription)])
		return @"";
	return [((MGMDFieldView *)self.fieldContents) contentDescription];
	
	
}

- (NSString *)contentValue
{
	if (![self.fieldContents respondsToSelector:@selector(contentValue)])
		return @"";
	return [((MGMDFieldView *)self.fieldContents) contentValue];
	
	
}



#pragma mark -
#pragma mark Drawing 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)drawRect:(NSRect)frame
{
	if (!isPlaceholder && self.editModeOn) {
		
		// draw background
		
		[[NSColor colorWithCalibratedWhite:0.6 alpha:1.0] set];
		
		NSRect upperRect = [self bounds];
		upperRect.origin.y += [self bounds].size.height/2;
		upperRect.size.height /= 2;
		
		NSRect lowerRect = [self bounds];
		lowerRect.size.height /= 2;
		
		[[NSColor colorWithCalibratedWhite:.950 alpha:1.0] set];
		
		NSRectFill(lowerRect);
		
		
		NSGradient *grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.653/*0.953*/ alpha:1.0]
														  endingColor:[NSColor colorWithCalibratedWhite:0.892/*0.992*/ alpha:1.0]] autorelease];

		NSGradient *gloss = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:1.0/*0.953*/ alpha:0.0]
														  endingColor:[NSColor colorWithCalibratedWhite:1.0/*0.992*/ alpha:0.35]] autorelease];
		
		[grad drawInRect:[self bounds] angle:-90];
		[gloss drawInRect:upperRect angle:-90];
		
		// draw bounding lines
		
		NSBezierPath *lines = [NSBezierPath bezierPath];
		[lines moveToPoint:[self bounds].origin];
		[lines lineToPoint:NSMakePoint(NSMaxX([self bounds]), NSMinY([self bounds]))]; 
		
		[lines moveToPoint:NSMakePoint(NSMinX([self bounds]), NSMaxY([self bounds]))];
		[lines lineToPoint:NSMakePoint(NSMaxX([self bounds]), NSMaxY([self bounds]))]; 
		
		
		[[NSColor colorWithCalibratedWhite:0.592 alpha:1.0] set];
		[lines stroke];
		
		// draw knob lines
		
		NSImage *knob = [NSImage imageNamed:@"DividerKnob"];
		NSRect knobRect;
		knobRect.origin.x = 10;
		knobRect.origin.y = ([self bounds].size.height - [knob size].height)/2;
		knobRect.size.width = [knob size].width;
		knobRect.size.height = [knob size].height;

		[knob drawInRect:knobRect 
				fromRect:NSMakeRect(0,0,knobRect.size.width, knobRect.size.height)
			   operation:NSCompositeSourceOver fraction:1.0];
		
		NSBezierPath *knobLines = [NSBezierPath bezierPath];
		
		CGFloat knobWidth = 10.0;
		CGFloat distanceBetweenKnobLines = 3.0;
		CGFloat knobStartX = 5.0;
		
		CGFloat knobMinY = floorf(([self bounds].size.height - 3 * distanceBetweenKnobLines)/2.0) - 0.5;
				
		[knobLines moveToPoint:NSMakePoint(knobStartX, knobMinY + distanceBetweenKnobLines)];
		[knobLines lineToPoint:NSMakePoint(knobStartX + knobWidth, knobMinY + distanceBetweenKnobLines)];
		
		[knobLines moveToPoint:NSMakePoint(knobStartX, knobMinY + 2 *distanceBetweenKnobLines)];
		[knobLines lineToPoint:NSMakePoint(knobStartX + knobWidth, knobMinY + 2 * distanceBetweenKnobLines)];
		
		[knobLines moveToPoint:NSMakePoint(knobStartX, knobMinY + 3 *distanceBetweenKnobLines)];
		[knobLines lineToPoint:NSMakePoint(knobStartX + knobWidth, knobMinY + 3 *distanceBetweenKnobLines)];
		
		[[NSColor colorWithCalibratedWhite:0.361 alpha:1.0] set];
		
	} else {
		
		[[NSColor whiteColor] set];
		//NSRectFill([self bounds]);
	}
	
	
	if (self.raisedEditingActive) {
		
		
		[self lockFocus];
		
		NSBezierPath *shadowPath = [NSBezierPath bezierPathWithRect:[fieldContents frame]];
		
		[NSGraphicsContext saveGraphicsState];
		
		NSShadow *shadow = [[[NSShadow alloc] init] autorelease];
		[shadow setShadowOffset: NSMakeSize(0, 0)];
		[shadow setShadowBlurRadius: 4.5];
		[shadow setShadowColor:[NSColor colorWithCalibratedWhite:0.0 alpha:1]];
		
		[[NSColor whiteColor] set];
		[shadow set];
		
		// extend the clipping path
		//NSBezierPath *extendedPath = [NSBezierPath bezierPathWithRect:NSInsetRect([fieldContents frame], -10, -10)];
		//[extendedPath addClip];
		
		// Fill.  The shadow will accumulate outside of the path.
		// ... and later, we'll leave transparency instead of 'whiteColor' by knocking a hole out of the middle.
		[shadowPath fill];
		
		[NSGraphicsContext restoreGraphicsState];
		
		[[NSGraphicsContext currentContext]setShouldAntialias: NO];
		[[NSColor colorWithCalibratedRed:0.22 green:0.47 blue:0.83 alpha:1.0] set];
		[shadowPath setLineWidth: 1.0];
		[shadowPath stroke];
		
		
		[self unlockFocus];
		
		
	}
	
	
	if (drawLabelContour & hasMenu) {
		
		CGFloat xRadius = 8.0;
		
		float textWidth = [[(MGMDTextField *)fieldLabel cell] cellSizeForBounds: NSMakeRect(0.0, 0.0, NSWidth([fieldLabel frame]), 100000.0)].width;
		
		NSRect frame = [fieldLabel frame];
		frame.origin.x += (frame.size.width - textWidth) - 3;
		frame.origin.y += 1; // better vertical centering
		frame.size.width = textWidth + 3;
		
		NSBezierPath *roundedPath = [NSBezierPath bezierPathWithRoundedRect:frame xRadius:xRadius yRadius:xRadius];
		frame.origin.x += xRadius;
		frame.size.width -= xRadius;
		NSBezierPath *straightPath = [NSBezierPath bezierPathWithRect:frame];
		
		NSGradient *grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedRed:0.22 green:0.47 blue:0.73 alpha:1.0]
														  endingColor:[NSColor colorWithCalibratedRed:0.22 green:0.47 blue:0.73 alpha:1.0]] autorelease];
		
		//NSGradient *grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.3 alpha:1.0]
		//												  endingColor:[NSColor colorWithCalibratedWhite:0.4 alpha:1.0]] autorelease];
		
		[grad drawInBezierPath:roundedPath angle:90.0];
		[grad drawInBezierPath:straightPath angle:90.0];
		
	}
	
	
	
}




#pragma mark -
#pragma mark Layout 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSRect)contentsFrame
{
	// returns the contents frame in coordinate system of self.
	return [fieldContents frame];
}



- (void)updateTrackingAreas
{
	if (!fieldLabel) return;
	
	[self removeTrackingArea:labelTrackingArea];
	
	[labelTrackingArea release];
	
	labelTrackingArea = [[NSTrackingArea alloc] initWithRect:[fieldLabel frame] 
													 options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp)
													   owner:self 
													userInfo:nil];
	
	[self addTrackingArea:labelTrackingArea];
}



- (void)updateLabelEdge
{
	// makes sure that the minimum label width is respected, by moving the border between fieldContents and fieldLabel to the location shared by all fields.
	
	if (![self superview]) return;
	
	CGFloat minimumLabelWidth = [(MGMDView *)[self superview] globalMinimumLabelWidth];
	
	[fieldLabel setFrame:NSMakeRect(MGFieldHorizontalMarginLeft, 
									[self bounds].size.height - [fieldLabel frame].size.height - MGFieldVerticalMargin - 1,
									minimumLabelWidth, 
									[fieldLabel frame].size.height)];
	
	[fieldContents setFrame:NSMakeRect(minimumLabelWidth + labelContentsMargin + MGFieldHorizontalMarginLeft, 
									   MGFieldVerticalMargin, 
									   [self bounds].size.width - minimumLabelWidth - labelContentsMargin - MGFieldHorizontalMarginRight - MGFieldHorizontalMarginLeft, 
									   [fieldContents frame].size.height)];
	
	[raisedEditorView setFrame:NSInsetRect([fieldContents frame], -1, -1)];
	
	
	// update the drawing of the raised editor
	[self setNeedsDisplay:YES];
	[[self superview] setNeedsDisplay:YES];
	
}

- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize
{
	// invoked when our bounds size changes
	
	[self updateLabelEdge];
	
	if ([fieldContents isKindOfClass:[MGMDTextField class]] || [fieldContents isKindOfClass:[MGMDTokenField class]])
		if (!ignoreSubviewResize) {
			
			[self updateContentsSize];
			
		} else {
			
			//NSLog(@"ignored subview resize request.");
		}
			
	
	else if (!fieldLabel) {
		[super resizeSubviewsWithOldSize:oldBoundsSize];
	}
		
}

- (void)updateContentsSize
{
	// invoked whenever the size of the contents (textfield) needs to be recalculated.
	
	if ([fieldContents isKindOfClass:[MGMDTextField class]]) {
		//NSLog(@"%@ update contents size", self.identifier);
		float oldHeight = NSHeight([fieldContents frame]);
		float editorHeight = [[(MGMDTextField *)fieldContents cell] cellSizeForBounds: NSMakeRect(0.0, 0.0, NSWidth([fieldContents frame]), 100000.0)].height;
		
		if(oldHeight != editorHeight + MGFieldEditorPadding) {
			
			if (![(NSControl *)fieldContents currentEditor])
				[self layoutWithContentsHeight:editorHeight  + MGFieldEditorPadding];
			
			
		}
		
		
	} else if ([fieldContents isKindOfClass:[MGMDExperimentView class]] || [fieldContents isKindOfClass:[MGMDKeywordView class]]) {
		
		
		[self layoutWithContentsHeight:[(MGMDExperimentView *)fieldContents contentsHeight]];
		
		
	} else {
		
		// do nothing

	
	}
}

- (void)layoutWithContentsHeight:(CGFloat)contentsHeight
{
	// change our size first to fit the contents with the given height
	NSRect newFrame = [self frame];
	newFrame.size.height = contentsHeight + 2 * MGFieldVerticalMargin;
	// set our frame, but DO NOT layout again as part of the resizeSubviewsWithOldSize: call.
	ignoreSubviewResize = YES;
	[self setFrame:NSIntegralRect(newFrame)];
	ignoreSubviewResize = NO;
				  
				  
	// change the contents size to the given height
	NSRect fieldFrame = [fieldContents frame];
	fieldFrame.size.height = roundf(contentsHeight);
	[fieldContents setFrameSize:fieldFrame.size];
	
	[raisedEditorView setFrame:NSInsetRect([fieldContents frame], -1, -1)];

	
	[(MGMDView *)[self superview] subviewDidUpdateFrame:self];
}





#pragma mark -
#pragma mark Events 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSView *)hitTest:(NSPoint)aPoint
{
	NSPoint pointInOwnSystem = [self convertPoint:aPoint fromView:[self superview]];
	if (fieldLabel && NSPointInRect(pointInOwnSystem, [fieldLabel frame])) {
		
		return self;
	}
	
	return [super hitTest:aPoint];
}



- (void)mouseDown:(NSEvent *)theEvent
{
	if (self.editModeOn) {
		
		isBeingDragged = YES;
		[(MGMDView *)[self superview] startDraggingField:self withEvent:theEvent];
		
	} else if (drawLabelContour) {
		
		NSEvent *currentEvent = [NSApp currentEvent];
		
		
		NSPoint location = NSMakePoint(NSMaxX([fieldLabel frame]), NSMaxY([fieldLabel frame]));
		
		NSEvent *adjustedEvent = [NSEvent mouseEventWithType:[currentEvent type]
													location:[self convertPoint:location toView:nil]
											   modifierFlags:[currentEvent modifierFlags] 
												   timestamp:[currentEvent timestamp] 
												windowNumber:[currentEvent windowNumber] 
													 context:[currentEvent context] 
												 eventNumber:[currentEvent eventNumber] 
												  clickCount:[currentEvent clickCount]
													pressure:[currentEvent pressure]];
		
		[NSMenu popUpContextMenu:[self fieldMenu] withEvent:adjustedEvent forView:self];	
		
		
	} else {
		
		[super mouseDown:theEvent];
		
	}
	
}

- (void)mouseEntered:(NSEvent *)theEvent
{
	if (editModeOn) {
		
		[super mouseEntered:theEvent];
		return;
		
	}
	
	if (hasMenu) [fieldLabel setTextColor:[NSColor whiteColor]];
	
	drawLabelContour = YES;
	[self setNeedsDisplay:YES];
}

- (void)mouseExited:(NSEvent *)theEvent
{
	if (editModeOn) {
		
		[super mouseEntered:theEvent];
		return;
		
	}
	
	[fieldLabel setTextColor:[NSColor blackColor]];
	
	drawLabelContour = NO;
	[self setNeedsDisplay:YES];
	
	
}



#pragma mark -
#pragma mark Event delegate methods 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Event delegate methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)controlTextDidBeginEditing:(NSNotification *)aNotification
{
	
	//NSLog(@"controlDidBegin %@", [aNotification object]);
	
	//if([aNotification object] != fieldContents) return;
	
	//((MGMDTextField *)fieldContents).raisedEditingActive = YES;
}



- (void)controlTextDidChange:(NSNotification *)aNotification
{

	// is it for us?
	//NSLog(@"controlDidChange %@", [aNotification object]);
	if([aNotification object] != fieldContents) return;
	
	if ([[aNotification object] isMemberOfClass:[MGMDTokenField class]]) {
		
		[self updateContentsSize];

	}
	
	
	NSTextView *fieldEditor = [[aNotification userInfo] objectForKey: @"NSFieldEditor"];
	if(!fieldEditor) {
		NSLog (@"no fieldeditor");
		return;
	}
	
	NSLayoutManager *lm = [fieldEditor layoutManager];
	unsigned idx = [lm numberOfGlyphs];
	NSRect boundingRect = [lm boundingRectForGlyphRange: NSMakeRange(0,idx) inTextContainer: [fieldEditor textContainer]];
	
	CGFloat oldHeight = NSHeight([fieldContents frame]);
	CGFloat editorHeight = NSHeight(boundingRect);
	
	
	
	if (oldHeight != editorHeight + MGFieldEditorPadding) {
		
		//NSLog(@"didChange layout with cellbounds: %f", editorHeight);
		[self layoutWithContentsHeight:editorHeight + MGFieldEditorPadding];
		//[self updateContentsSize];
	}
	
	// update drawing
	[self setNeedsDisplay: YES];
	
}


- (void)controlTextShouldEndEditing:(NSNotification *)aNotification
{
	
	//NSLog(@"controlShouldEnd %@", [aNotification object]);
	// is it for us?
	if([aNotification object] != fieldContents) return;
	
	//((MGMDTextField *)fieldContents).raisedEditingActive = NO;
	//self.raisedEditingActive = NO;
}


- (void)controlTextDidEndEditing:(NSNotification *)aNotification
{
	//NSLog(@"controlDidEnd %@", [[aNotification object] description]);
	// is it for us?
	if([aNotification object] != fieldContents) return;
	
	// if the field is a token field, trigger the setObjectValue: method in order to invoke bindings.
	if ([[aNotification object] isMemberOfClass:[MGMDTokenField class]]) {
		
		MGMDTokenField *field = (MGMDTokenField *)[aNotification object];
		
		[field setObjectValue:[field objectValue]];

	}
	

	
	((MGMDTextField *)fieldContents).raisedEditingActive = NO;
	self.raisedEditingActive = NO;
	

}

- (BOOL)control:(NSControl *)control textShouldBeginEditing:(NSText *)fieldEditor
{
	//NSLog(@"controlShouldBegin %@", [control description]);
	//((MGMDTextField *)fieldContents).raisedEditingActive = YES;
	//self.raisedEditingActive = YES;
	return YES;
}


- (void)tokenFieldEditorDidBeginEditing:(NSNotification *)not
{
	//NSLog(@"tokenFieldDidBegin");
	if ([fieldContents isMemberOfClass:[MGMDTokenField class]]) {
		
		((MGMDTextField *)fieldContents).raisedEditingActive = YES;
		self.raisedEditingActive = YES;
		
	}
}

- (void)tokenFieldEditorDidEndEditing:(NSNotification *)not
{	
	
	if ([fieldContents isMemberOfClass:[MGMDTokenField class]]) {

		//NSLog(@"tokenFieldDidEnd");

		
		((MGMDTextField *)fieldContents).raisedEditingActive = NO;
		self.raisedEditingActive = NO;
		
		// we can ONLY invoke the controller update AFTER end of editing, because otherwise objectValue: triggers unwanted behaviour (committing)
		
		[observedObject setValue:[NSSet setWithArray:[(MGMDTokenField *)fieldContents objectValue]] forKeyPath:@"selection.keywords"];


	}

}


@end
