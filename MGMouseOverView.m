//
//  MGMouseOverView.m
//  StackTable
//
//  Created by Dennis Lorson on 3/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGMouseOverView.h"


@implementation MGMouseOverView


- (id)init
{
	if ((self = [super init])) {
		
		[self updateTrackingAreas];
	}
	
	return self;
}


- (void)updateTrackingAreas
{
	[self removeTrackingArea:area];
	
	
	area = [[[NSTrackingArea alloc] initWithRect:[self bounds]
										 options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp | NSTrackingInVisibleRect)
										   owner:self
										userInfo:nil] autorelease];
	[self addTrackingArea:area];
}

- (void)mouseEntered:(NSEvent *)theEvent
{
	
	[[[self subviews] objectAtIndex:0] mouseEntered:theEvent];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"MGMouseEnteredNotification" object:self];
	
}


- (void)mouseExited:(NSEvent *)theEvent
{
	[[[self subviews] objectAtIndex:0] mouseExited:theEvent];

	[[NSNotificationCenter defaultCenter] postNotificationName:@"MGMouseExitedNotification" object:self];
	
}



- (void)dealloc
{
	[super dealloc];
}
					
								 

								 
@end
