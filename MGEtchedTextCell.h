/* MyTitleCell */

#import <Cocoa/Cocoa.h>

@interface MGEtchedTextCell : NSTextFieldCell
{
	NSColor *mShadowColor;
}

-(void)setShadowColor:(NSColor *)color;

@end
