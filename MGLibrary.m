//
//  MGLibrary.m
//  Filament
//
//  Created by Dennis Lorson on 03/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGLibrary.h"
#import "Album.h"
#import "StringExtensions.h"
#import "SourceListTreeController.h"

#define LIBRARY_VERSION_KEY @"LibraryVersion"

#define CURRENT_MACNIFICATION_VERSION 684




@interface MGLibrary ()

+ (NSString *)_defaultBundlePath;
+ (NSString *)_defaultBundleName;
- (NSString *)_databasePath;

- (void)_createBundleIfNeeded;

- (void)_refactorImagePaths;
- (void)_refactorEditedPaths;


- (void)_generateFromAppSupportLocationIfAvailable;
- (void)_postRefactorEditedImages;

- (void)_replaceOldDemoLibrary;
- (void)_moveEditedImagesWithinBundle;

+ (NSString *)_oldAppSupportFolder;
- (NSString *)_oldImageDataFolder;

- (NSDictionary *)_storeMetadata;
- (void)_migrateStoreIfNeeded;


- (void)_updateLibraryIfNeeded;


- (NSString *)_backupLibrary;
- (void)_restoreLibraryFromPath:(NSString *)backupPath;
- (BOOL)_updateLibrary;
- (void)_removeOrphanedObjects;

- (void)_loadDemoStoreIfEmpty;


- (void)_updateFilters;

- (void)_refactorHardcodedCollections;
- (Album *)_findLegacyFixedCollectionWithEntity:(NSString *)entityName name:(NSString *)name rank:(int)rank parent:(id)parent;

- (id)_fixedCollectionWithEntity:(NSString *)entityName name:(NSString *)name rank:(int)rank;


@end



@implementation MGLibrary



- (id)initWithPath:(NSString *)path
{
    self = [super init];
    if (self) {
        bundlePath_ = [[path stringByExpandingTildeInPath] copy];
        [self _createBundleIfNeeded];
    }
    
    return self;
}


- (id)initWithDefaultPathUseDemoContents:(BOOL)populateWithDemoContents;
{
    self = [self initWithPath:[[self class] _defaultBundlePath]];
    
    if (populateWithDemoContents)
        [self _loadDemoStoreIfEmpty];
    
    return self;
}

- (id)initWithPrefsPath;
{
    self = [self initWithPath:[[self class] prefsLibraryPath]];
    
    return self;
}



- (void)setDelegate:(id <MGLibraryDelegate>)delegate;
{
    delegate_ = delegate;
}

#pragma mark -


+ (BOOL)libraryExistsAtPath:(NSString *)path;
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[path stringByExpandingTildeInPath]])
        return NO;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[[path stringByExpandingTildeInPath] stringByAppendingPathComponent:@"library.db"]])
          return NO;
    
    return YES;
}


+ (BOOL)legacyLibraryExists;
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[[[self _oldAppSupportFolder] stringByExpandingTildeInPath] stringByAppendingPathComponent:@"Macnification.db"]];
}


#pragma mark -

+ (NSString *)_defaultBundlePath
{
    return [[[NSHomeDirectory() stringByExpandingTildeInPath] stringByAppendingPathComponent:@"Pictures"] stringByAppendingPathComponent:[self _defaultBundleName]];
}


+ (NSString *)defaultLibraryPath;
{
    return [[self class] _defaultBundlePath];
}

+ (NSString *)_defaultBundleName
{
    return @"Macnification Library.mnlibrary";
}

+ (NSString *)prefsLibraryPath;
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"libraryPath"];
}


+ (void)setPrefsLibraryPath:(NSString *)path;
{
    [[NSUserDefaults standardUserDefaults] setValue:[path stringByAbbreviatingWithTildeInPath] forKey:@"libraryPath"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (NSString *)imageDataPath
{
    return [bundlePath_ stringByAppendingPathComponent:@"Images"];
}

- (NSString *)thumbnailDataPath
{
    return [bundlePath_ stringByAppendingPathComponent:@"Thumbnails"];
}


- (NSString *)editedDataPath
{
    return [bundlePath_ stringByAppendingPathComponent:@"Edited"];
}

- (NSString *)_databasePath;
{
    return [bundlePath_ stringByAppendingPathComponent:@"library.db"];

}

- (void)_createBundleIfNeeded
{
    NSFileManager *m = [NSFileManager defaultManager];
    [m createDirectoryAtPath:bundlePath_ withIntermediateDirectories:YES attributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSFileExtensionHidden, nil] error:nil];
    [m createDirectoryAtPath:[self imageDataPath] withIntermediateDirectories:YES attributes:nil error:nil];
    [m createDirectoryAtPath:[self thumbnailDataPath] withIntermediateDirectories:YES attributes:nil error:nil];
    [m createDirectoryAtPath:[self editedDataPath] withIntermediateDirectories:YES attributes:nil error:nil];

}



- (NSManagedObjectModel *)managedObjectModel 
{
	if (model_)
		return model_;
	
	NSMutableSet *allBundles = [[NSMutableSet alloc] init];
	[allBundles addObject:[NSBundle mainBundle]];
	[allBundles addObjectsFromArray:[NSBundle allFrameworks]];
	
	model_ = [[NSManagedObjectModel mergedModelFromBundles: [allBundles allObjects]] retain];
	[allBundles release];
	
	return model_;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	if (coordinator_) 
		return coordinator_;
	
	NSError *error;
    
    [self _generateFromAppSupportLocationIfAvailable];

    
    
    NSManagedObjectModel *model = [self managedObjectModel];

    coordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];

	
	NSURL *url = [NSURL fileURLWithPath:[self _databasePath]];
    
	libraryDidExist_ = [[NSFileManager defaultManager] fileExistsAtPath:[self _databasePath]];
    
    
    [self _migrateStoreIfNeeded];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
	
	if (![coordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:options error:&error])
		[[NSApplication sharedApplication] presentError:error];
	
	
	return coordinator_;
}



- (NSManagedObjectContext *)managedObjectContext 
{
	if (ctx_)
		return ctx_;
	
    ctx_ = [[NSManagedObjectContext alloc] init];
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    [ctx_ setPropagatesDeletesAtEndOfEvent:YES];
    
    if (coordinator)
        [ctx_ setPersistentStoreCoordinator: coordinator];
    
    [self _updateLibraryIfNeeded];
    
    if (libraryNeedsPathConversion_)
        [self _refactorImagePaths];
    
    // an extra refactoring step not initially available in 2.0.
    // it will only be executed once ever on the user's system
    [self _postRefactorEditedImages];
    
    [ctx_ save:nil];
    
    if (didStartConversion_) {
        if ([delegate_ respondsToSelector:@selector(libraryDidCompleteConversion:)])
            [delegate_ libraryDidCompleteConversion:self];
        didStartConversion_ = NO;
    }

    	
	return ctx_;
}


#pragma mark -
#pragma mark Database migration


- (NSDictionary *)_storeMetadata
{
    NSDictionary *metadata = nil;
    NSError *error = nil;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self _databasePath]])
		if (!(metadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:[NSURL fileURLWithPath:[self _databasePath]] error:&error]))
			[[NSApplication sharedApplication] presentError:error];
    
    return metadata;
}


- (void)_migrateStoreIfNeeded
{
    
    NSDictionary *metadata = [self _storeMetadata];
	
	if (!metadata || [[self managedObjectModel] isConfiguration:nil compatibleWithStoreMetadata:metadata])
        return;
    
    return;
    
    if (!didStartConversion_) {
        if ([delegate_ respondsToSelector:@selector(libraryWillStartConversion:)])
            [delegate_ libraryWillStartConversion:self];
        didStartConversion_ = YES;
    }
    
    NSLog(@"Migrating library....");
    
    NSURL *srcModelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Filament_DataModel" ofType:@"mom" inDirectory:@"Filament_DataModel.momd"]];
    NSManagedObjectModel *srcModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:srcModelURL] autorelease];
    NSMappingModel *map = [NSMappingModel mappingModelFromBundles:[NSArray arrayWithObject:[NSBundle mainBundle]] forSourceModel:srcModel destinationModel:[self managedObjectModel]];
    NSMigrationManager *mgr = [[NSMigrationManager alloc] initWithSourceModel:srcModel destinationModel:[self managedObjectModel]];
    
    NSString *tmpPath = [[[[self _databasePath] stringByDeletingPathExtension] stringByAppendingString:@"_migrated"] stringByAppendingPathExtension:[[self _databasePath] pathExtension]];
    
    NSURL *tmpURL = [NSURL fileURLWithPath:tmpPath];
    NSURL *url = [NSURL fileURLWithPath:[self _databasePath]];
    
    NSError *error = nil;
    if (![mgr migrateStoreFromURL:url type:NSSQLiteStoreType options:nil withMappingModel:map toDestinationURL:tmpURL destinationType:NSSQLiteStoreType destinationOptions:nil error:&error])
        NSLog(@"Library migration failed! (%@)", [error localizedDescription]);
    
    NSLog(@"Done.");
    
    // backup the old library
    [self _backupLibrary];
    
    // delete the old library at its original location
    [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
    
    // move the migrated library back to its original location
    [[NSFileManager defaultManager] moveItemAtURL:tmpURL toURL:url error:&error];
    
}

#pragma mark -
#pragma mark Demo store


- (void)_loadDemoStoreIfEmpty;
{
    NSFileManager *fm = [NSFileManager defaultManager];

    if ([fm fileExistsAtPath:[self _databasePath]])
        return;
    
    
	NSString *librarySourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Demo Library.mnlibrary"];
	
	
    if ([fm fileExistsAtPath:bundlePath_])
        [fm removeItemAtPath:bundlePath_ error:nil];
	
	// Copy the files from the app bundle to the Application Support folder
	[fm copyItemAtPath:librarySourcePath toPath:bundlePath_ error:nil];
	
	// Set the library creation date to now
	[fm setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSDate date], NSFileCreationDate, [NSNumber numberWithInt:YES], NSFileExtensionHidden, nil] ofItemAtPath:bundlePath_ error:nil];
    
    
	[fm setAttributes:[NSDictionary dictionaryWithObject:[NSDate date] forKey:NSFileCreationDate] ofItemAtPath:[self _databasePath] error:nil];
}




#pragma mark -
#pragma mark Library structure conversion

- (void)_generateFromAppSupportLocationIfAvailable
{
    NSFileManager *m = [NSFileManager defaultManager];

    if ([m fileExistsAtPath:[self _databasePath]])
        return;
    
    
    NSLog(@"Moving DB from application support....");
    
    [self _replaceOldDemoLibrary];
    
    NSString *oldDBPath = [[[self class] _oldAppSupportFolder] stringByAppendingPathComponent:@"Macnification.db"];
    

    if ([m fileExistsAtPath:oldDBPath]) {
        [m moveItemAtPath:oldDBPath toPath:[self _databasePath] error:nil];
        
        libraryNeedsPathConversion_ = YES;
    }
    
    NSString *oldImageDataPath = [self _oldImageDataFolder];
    
    if ([m fileExistsAtPath:oldImageDataPath]) {
        [m removeItemAtPath:[self imageDataPath] error:nil];
        [m moveItemAtPath:oldImageDataPath toPath:[self imageDataPath] error:nil];
    }

}


- (void)_moveEditedImagesWithinBundle
{
    NSFileManager *m = [NSFileManager defaultManager];
    
    if (![m fileExistsAtPath:[self _databasePath]])
        return;
    
    NSString *originalEditedPath = [[self imageDataPath] stringByAppendingPathComponent:@"Edited"];
    
    NSString *newEditedPath = [self editedDataPath];
    
    if (![m fileExistsAtPath:newEditedPath])
        [m createDirectoryAtPath:newEditedPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    for (NSString *file in [m contentsOfDirectoryAtPath:originalEditedPath error:nil]) {
        
        NSString *filename = [file lastPathComponent];
        NSString *filePath = [originalEditedPath stringByAppendingPathComponent:filename];
        
        NSString *dstPath = [newEditedPath stringByAppendingPathComponent:filename];
        
        if (![m fileExistsAtPath:dstPath])
            [m moveItemAtPath:filePath toPath:dstPath error:nil];
    }
    
}

- (void)_replaceOldDemoLibrary
{
    NSString *applicationSupportFolder = [[self class] _oldAppSupportFolder];
    
    NSString *demoPath = [applicationSupportFolder stringByAppendingPathComponent:@"MacnificationDemo.db"];
	NSString *actualPath = [applicationSupportFolder stringByAppendingPathComponent:@"Macnification.db"];
	NSString *demoFlagPath = [applicationSupportFolder stringByAppendingPathComponent:@"demo"];
	
    NSFileManager *m = [NSFileManager defaultManager];

    
	if ([m fileExistsAtPath:demoPath] && ![m fileExistsAtPath:actualPath]) {
		[m moveItemAtPath:demoPath toPath:actualPath error:nil];
		[m removeItemAtPath:demoFlagPath error:nil];
	}

}

+ (NSString *)_oldAppSupportFolder 
{
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
	NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();
	return [basePath stringByAppendingPathComponent:@"Macnification"];
}


- (NSString *)_oldImageDataFolder 
{
    return [[NSHomeDirectory() stringByAppendingPathComponent:@"Pictures"] stringByAppendingPathComponent:@"Macnification"];
}

- (void)_refactorImagePaths
{
    if (!didStartConversion_) {
        if ([delegate_ respondsToSelector:@selector(libraryWillStartConversion:)])
            [delegate_ libraryWillStartConversion:self];
        didStartConversion_ = YES;
    }   
    
    
    
    
    NSString *imagePrefix = [[[NSHomeDirectory() stringByAppendingPathComponent:@"Pictures"] stringByAppendingPathComponent:@"Macnification"] stringByExpandingTildeInPath];
    NSString *imagePrefixAbbrev = [imagePrefix stringByAbbreviatingWithTildeInPath];
    
    NSArray *possiblePrefixes = [NSArray arrayWithObjects:
                                 [imagePrefix stringByAppendingString:@"/"],
                                 [imagePrefixAbbrev stringByAppendingString:@"/"],
                                 imagePrefix,
                                 imagePrefixAbbrev,
                                 nil];
    
    

    
    NSLog(@"Refactoring image paths...");
    NSArray *images = [self executeRequestWithEntityName:@"Image" 
                                           prefetchPaths:[NSArray arrayWithObjects:@"imagePath", @"editedImagePath", nil]
                                  returnsObjectsAsFaults:NO
                                                   error:nil limit:0 predicate:nil];
    
    NSLog(@"%i images found", (int)[images count]);
    
    for (Image *image in images) {
        
        NSString *tmp = nil;
        
        if ((tmp = [image valueForKey:@"imagePath"])) {
            for (NSString *prefix in possiblePrefixes)
                tmp = [tmp stringByReplacingOccurrencesOfString:prefix withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tmp length])];
            
            [image setValue:tmp forKey:@"imagePath"];
        }
        
        if ((tmp = [image valueForKey:@"editedImagePath"])) {
            for (NSString *prefix in possiblePrefixes)
                tmp = [tmp stringByReplacingOccurrencesOfString:prefix withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tmp length])];
            
            NSString *editedPrefix = @"Edited/";
            
            if ([tmp hasPrefix:editedPrefix])
                tmp = [tmp stringByReplacingOccurrencesOfString:editedPrefix withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tmp length])];

            [image setValue:tmp forKey:@"editedImagePath"];
        }
        
    }
    
    [self _refactorEditedPaths];
    
}


- (void)_refactorEditedPaths
{
    NSArray *images = [self executeRequestWithEntityName:@"Image" 
                                           prefetchPaths:[NSArray arrayWithObjects:@"imagePath", @"editedImagePath", nil]
                                  returnsObjectsAsFaults:NO
                                                   error:nil limit:0 predicate:nil];

    
    for (Image *image in images) {
        
        NSString *tmp = nil;
        
        if ((tmp = [image valueForKey:@"editedImagePath"])) {

            NSString *editedPrefix = @"Edited/";
            
            if ([tmp hasPrefix:editedPrefix])
                tmp = [tmp stringByReplacingOccurrencesOfString:editedPrefix withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tmp length])];
            
            [image setValue:tmp forKey:@"editedImagePath"];
        }
        
    }

}


- (void)_postRefactorEditedImages
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"didRefactorEditedImagePaths"])
        return;
    
    NSLog(@"Refactoring edited paths...");

    [self _refactorEditedPaths];
    [self _moveEditedImagesWithinBundle];
    
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"didRefactorEditedImagePaths"];
}

#pragma mark -
#pragma mark Cleanup and fix

- (void)_updateLibraryIfNeeded
{		
	// check for the version string of the library to see if we have updated it	
	NSPersistentStoreCoordinator *coord = [self persistentStoreCoordinator];
	NSPersistentStore *store = [coord persistentStoreForURL:[NSURL fileURLWithPath:[self _databasePath]]];
	NSMutableDictionary *md = [[[coord metadataForPersistentStore:store] mutableCopy] autorelease];
	
	NSInteger libraryVersion = [md objectForKey:LIBRARY_VERSION_KEY] ? [[md objectForKey:LIBRARY_VERSION_KEY] integerValue] : 0; 
	
	if (libraryVersion > CURRENT_MACNIFICATION_VERSION) {
		
		NSAlert *alert = [NSAlert alertWithMessageText:@"Your Library was created with a newer version of Macnification.  Do you still want to open it?"
										 defaultButton:@"Quit" alternateButton:@"Open" otherButton:nil 
							 informativeTextWithFormat:@"This version of Macnification may be unable to open your data correctly.  A backup has been made.  Please use the version you used to create or modify your Library."];
		[alert setAlertStyle:NSWarningAlertStyle];
		
		if ([alert runModal] == NSAlertDefaultReturn)
			[NSApp terminate:self];
		
		[self _backupLibrary];
		
		return;
		
	}
	
	if (!libraryDidExist_) {
		
		// save the library version
		[md setObject:[NSNumber numberWithInt:CURRENT_MACNIFICATION_VERSION] forKey:LIBRARY_VERSION_KEY];
		[coord setMetadata:md forPersistentStore:store];
		
		return;
		
	}
	
	
	if (libraryVersion == CURRENT_MACNIFICATION_VERSION)
		return;
    
    
    if (!didStartConversion_) {
        if ([delegate_ respondsToSelector:@selector(libraryWillStartConversion:)])
            [delegate_ libraryWillStartConversion:self];
        didStartConversion_ = YES;
    }
    

	
	NSLog(@"updating library...");
    
	// backup the library
	NSString *libraryBackupPath;
	if (!(libraryBackupPath = [self _backupLibrary])) 
		return;
	
	sleep(3);
	
	
	// update the library
	BOOL succeeded = [self _updateLibrary];
	
	
	if (!succeeded) {
		
		[self _restoreLibraryFromPath:libraryBackupPath];
		NSLog(@"Library update failed, reverting");
		
	} else {
		
		// save the library version
		[md setObject:[NSNumber numberWithInt:CURRENT_MACNIFICATION_VERSION] forKey:LIBRARY_VERSION_KEY];
		[coord setMetadata:md forPersistentStore:store];
		
		NSLog(@"library update complete");
		
	}
	

    
}


- (NSString *)_uniqueBackupPath;
{
    NSString *originalPath = [self _databasePath];
    
    
    NSString *date = [[NSDate date] descriptionWithCalendarFormat:@"%Y%m%d_%H%M%S" timeZone:nil locale:[[NSUserDefaults standardUserDefaults] dictionaryRepresentation]];
    
    NSString *mod = [originalPath stringByAppendingFilenameSuffix:[NSString stringWithFormat:@"_backup_%@", date]];
        
    return [mod uniquePath];
}



- (NSString *)_backupLibrary
{
	// returns the path of the library backup

	NSError *error = nil;
    
    NSString *backupPath = [self _uniqueBackupPath];
	
	[[NSFileManager defaultManager] copyItemAtPath:[self _databasePath] toPath:backupPath error:&error];
	
	
	if (error) {
		NSLog(@"Backup of library failed with error %@", [error localizedDescription]);
		return nil;
	} else {
		NSLog(@"Backup of library succeeded (%@)", backupPath);
        
		
	}
	
	return backupPath;
	
}


- (void)_restoreLibraryFromPath:(NSString *)backupPath
{
	
	NSError *error = nil;
	
	[[NSFileManager defaultManager] copyItemAtPath:backupPath toPath:[self _databasePath] error:&error];
	
	if (error) {
		NSLog(@"Restore of library failed with error %@", [error localizedDescription]);
	} else {
		NSLog(@"Restore of library succeeded (from %@)", backupPath);
	}	
	
	
}


- (BOOL)_updateLibrary
{
	[self _removeOrphanedObjects];
	[self _updateFilters];
	[self _refactorHardcodedCollections];
    
	
	return YES;
}

- (void)_removeOrphanedObjects
{
	NSError *error = nil;;
	
	// should log amt of deleted objects
	NSLog(@"Deleting orphaned objects");
	
	// instruments w/o images
	NSArray *instruments = [self executeRequestWithEntityName:@"Instrument" prefetchPaths:[NSArray arrayWithObject:@"images"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:nil];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *instrument in instruments) {
		
		if ([[instrument valueForKey:@"images"] count] == 0) {
			[[self managedObjectContext] deleteObject:instrument];
			NSLog(@"Deleted orphaned instrument %@", [[instrument objectID] URIRepresentation]);
		}
		
	}
	
	// experimenters w/o images
	NSArray *experimenters = [self executeRequestWithEntityName:@"Experimenter" prefetchPaths:[NSArray arrayWithObject:@"images"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:nil];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *experimenter in experimenters) {
		
		if ([[experimenter valueForKey:@"images"] count] == 0) {
			[[self managedObjectContext] deleteObject:experimenter];
			NSLog(@"Deleted orphaned experimenter %@", [[experimenter objectID] URIRepresentation]);
		}
	}
	
	// filters w/o images
	NSArray *filters = [self executeRequestWithEntityName:@"Filter" prefetchPaths:[NSArray arrayWithObject:@"image"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:[NSPredicate predicateWithFormat:@"image == nil"]];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *filter in filters) {
		
		if (![filter valueForKey:@"image"]) {
			[[self managedObjectContext] deleteObject:filter];
			NSLog(@"Deleted orphaned filter %@", [[filter objectID] URIRepresentation]);
		}
		
	}
	
	// light table images w/o images
	NSArray *ltImages = [self executeRequestWithEntityName:@"LightTableImage" prefetchPaths:[NSArray arrayWithObject:@"image"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:[NSPredicate predicateWithFormat:@"image == nil"]];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *ltImage in ltImages) {
		
		if (![ltImage valueForKey:@"image"]) {
			[[self managedObjectContext] deleteObject:ltImage];
			NSLog(@"Deleted orphaned LTImage %@", [[ltImage objectID] URIRepresentation]);
		}
		
	}
	
	
	// stack images w/o images
	NSArray *stImages = [self executeRequestWithEntityName:@"StackImage" prefetchPaths:[NSArray arrayWithObject:@"image"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:[NSPredicate predicateWithFormat:@"image == nil"]];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *stImage in stImages) {
		
		if (![stImage valueForKey:@"image"]) {
			[[self managedObjectContext] deleteObject:stImage];
			NSLog(@"Deleted orphaned STImage %@", [[stImage objectID] URIRepresentation]);
		}
		
	}
	
	// channels w/o instrument
	NSArray *channels = [self executeRequestWithEntityName:@"Channel" prefetchPaths:[NSArray arrayWithObject:@"instrument"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:[NSPredicate predicateWithFormat:@"instrument == nil"]];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *channel in channels) {
		
		if (![channel valueForKey:@"instrument"]) {
			[[self managedObjectContext] deleteObject:channel];
			NSLog(@"Deleted orphaned channel %@", [[channel objectID] URIRepresentation]);
		}
		
	}
	
	
	// scale bars without images
	NSArray *scalebars = [self executeRequestWithEntityName:@"ScaleBar" prefetchPaths:[NSArray arrayWithObject:@"image"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:[NSPredicate predicateWithFormat:@"image == nil"]];
	if (error) {
		NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *scalebar in scalebars) {
		
		if (![scalebar valueForKey:@"image"]) {
			[[self managedObjectContext] deleteObject:scalebar];
			NSLog(@"Deleted orphaned scalebar %@", [[scalebar objectID] URIRepresentation]);
		}
		
	}
	
	// ROIs without images
	long nRoisRemoved = 0;
	
	while (YES) {
		
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		//NSPredicate *predicate = [NSPredicate predicateWithFormat:@"image = nil"];
		
		NSArray *rois = [self executeRequestWithEntityName:@"ROI" prefetchPaths:nil returnsObjectsAsFaults:YES error:&error limit:7000 predicate:[NSPredicate predicateWithFormat:@"image == nil"]];
        
		if (error) {
			NSLog(@"Orphan removal failed with error: %@", [error localizedDescription]);
			[pool release];
			return;
		}
		
		for (NSManagedObject *roi in rois) {
			
			//if (![roi valueForKey:@"image"]) {
            [[self managedObjectContext] deleteObject:roi];
            // don't log this, we may have thousands
            //NSLog(@"Deleted orphaned roi %@", [[roi objectID] URIRepresentation]);
			//}
			
		}
        
		if ([rois count] == 0) break;
        
		
		nRoisRemoved += 7000;
		NSLog(@"Removed %ld ROIs", nRoisRemoved);
		
		[pool release];
		
		
	}
    
	
	// leave calibrations, experiments, keywords because they can be manipulated without having any images!
	
	NSLog(@"orphaned object removal succeeded");
}


- (NSArray *)executeRequestWithEntityName:(NSString *)name prefetchPaths:(NSArray *)paths returnsObjectsAsFaults:(BOOL)flag error:(NSError **)error limit:(NSUInteger)limit predicate:(NSPredicate *)pred
{
	NSManagedObjectContext *ctx = [self managedObjectContext];
	
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	
	[req setEntity:[NSEntityDescription entityForName:name inManagedObjectContext:ctx]];
	if (paths)
		[req setRelationshipKeyPathsForPrefetching:paths];
	
	// BUGFIX 16/10/09: Enabling this leads to a Core Data SQLCore arrayIndexOOBException on some machines.
	//[req setReturnsObjectsAsFaults:flag];
	
	[req setIncludesPropertyValues:NO];
	
	if (limit != 0)
		[req setFetchLimit:limit];
	
	if (pred)
		[req setPredicate:pred];
	
	NSArray *results = [ctx executeFetchRequest:req error:error];
	
	return results;
}

- (void)_updateFilters
{
	
	NSError *error = nil;
	NSArray *images = [self executeRequestWithEntityName:@"Image" prefetchPaths:[NSArray arrayWithObject:@"filters"] returnsObjectsAsFaults:NO error:&error limit:0 predicate:nil];
	
	if (error) {
		NSLog(@"Filter update failed with error: %@", [error localizedDescription]);
		return;
	}
	
	for (NSManagedObject *image in images) {
		
		NSMutableSet *allFilters = [image mutableSetValueForKey:@"filters"];
		
		for (NSManagedObject *filter in allFilters) {
			
			// remove the threshold filter
			if ([[filter valueForKey:@"name"] isEqualTo:@"ThresholdFilter"]) {
				[allFilters removeObject:filter];
				[[self managedObjectContext] deleteObject:filter];
				
				NSLog(@"Deleted threshold filter in image %@", [[image objectID] URIRepresentation]);
			}
			
			
			// remove the invert filter
			if ([[filter valueForKey:@"name"] isEqualTo:@"InvertFilter"]) {
				[allFilters removeObject:filter];
				[[self managedObjectContext] deleteObject:filter];
				
				NSLog(@"Deleted invert filter in image %@", [[image objectID] URIRepresentation]);
			}
			
			
			// rename the blur filter
			if ([[filter valueForKey:@"name"] isEqualTo:@"CIGaussianBlur"]) {
				[filter setValue:@"DODGaussianBlurFilter" forKey:@"name"];
				
				NSLog(@"Renamed gaussian blur filter in image %@", [[image objectID] URIRepresentation]);
                
			}
		}
		
		
		// remove duplicates
		NSMutableArray *filtersCopy = [[[allFilters allObjects] mutableCopy] autorelease];
		for (id filter in allFilters) {
			for (id otherFilter in allFilters) {
				if (otherFilter != filter && [[otherFilter valueForKey:@"name"] isEqualToString:[filter valueForKey:@"name"]]) {
					[filtersCopy removeObject:otherFilter];
					NSLog(@"Removed duplicate filter with name %@ in image %@", [otherFilter valueForKey:@"name"], [[image objectID] URIRepresentation]);
				}
			}
		}
        
		if ([filtersCopy count] != [allFilters count])
			[image setValue:[NSSet setWithArray:filtersCopy] forKey:@"filters"];
		
	}
}

#pragma mark -
#pragma mark Hardcoded collections change

- (void)_refactorHardcodedCollections
{
	// remove the allImagesCollection
	// but remove its children from the relationship first! (otherwise cascade)
	Album *allImagesCollection = [[self _findLegacyFixedCollectionWithEntity:@"SourceListCollection" name:@"ALL IMAGES" rank:0 parent:nil] retain];
	[allImagesCollection setValue:nil forKey:@"children"];
    
    if (allImagesCollection)
        [[self managedObjectContext] deleteObject:allImagesCollection];
	
	
	// Give the other collections new ranks
	Album *projectsCollection = [[self _findLegacyFixedCollectionWithEntity:@"SourceListCollection" name:@"PROJECTS" rank:10 parent:nil] retain];
	[projectsCollection setValue:[NSNumber numberWithShort:PROJECTS_COLLECTION_RANK] forKey:@"rank"];
	
	Album *libraryGroup = [[self _findLegacyFixedCollectionWithEntity:@"Album" name:@"Library" rank:0 parent:nil] retain];
	[libraryGroup setValue:[NSNumber numberWithShort:LIBRARY_COLLECTION_RANK] forKey:@"rank"];
	
	Album *lastImportGroup = [[self _findLegacyFixedCollectionWithEntity:@"Album" name:@"Last Import" rank:8 parent:nil] retain];
	[lastImportGroup setValue:[NSNumber numberWithShort:LAST_IMPORT_COLLECTION_RANK] forKey:@"rank"];
	
	Album *lastMonthGroup = [[self _findLegacyFixedCollectionWithEntity:@"SmartAlbum" name:@"Last Month" rank:12 parent:nil] retain];
	[lastMonthGroup setValue:[NSNumber numberWithShort:LAST_MONTH_COLLECTION_RANK] forKey:@"rank"];
	
	
	
	// Capture was not released before this code came into effect, so ignore it
	//Album *lastCaptureGroup = [[self _findLegacyFixedCollectionWithEntity:@"Album" name:@"__LAST_CAPTURE__" rank:8 parent:nil] retain];
    
}



- (Album *)_findLegacyFixedCollectionWithEntity:(NSString *)entityName name:(NSString *)name rank:(int)rank parent:(id)parent;
{
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ && rank == %i", name, rank];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	[fetch setPredicate:predicate];
	[fetch setEntity:entity];
	NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:nil];
	[fetch release];
	if ([results count] == 0) {
		NSLog(@"Warning: no fixed collection found with name %@, rank %i -- continuing", name, rank);	
		return nil;
	}
	
	if ([results count] != 1) {
		NSLog(@"Warning: found more than one fixed collection with name %@, rank %i:\n", name, rank);	
		for (NSManagedObject *obj in results)
			NSLog(@"%@", [obj description]);
	}
    
	id list = [results objectAtIndex:0];
	return list;
}

#pragma mark -
#pragma mark Other

- (void)_cleanupEmptyFolders
{
    // Cleans up all non-root folders that are empty
    // TODO
    
}




# pragma mark -
# pragma mark Fixed collections (Library, Last import, ...)


- (id)_fixedCollectionWithEntity:(NSString *)entityName name:(NSString *)name rank:(int)rank;
{
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"rank == %i", rank];
	NSFetchRequest *fetch = [[[NSFetchRequest alloc] init] autorelease];
	[fetch setPredicate:predicate];
	[fetch setEntity:entity];
	
	NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:nil];
    
	if ([results count] > 0)
		return [results objectAtIndex:0];
	
	// create it
	NSLog(@"Creating new fixed collection: %@", name);
	
	id list = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[self managedObjectContext]];
	[list setValue:name forKey:@"name"];
	[list setValue:[NSNumber numberWithInt:rank] forKey:@"rank"];
	
	return list;
}


- (Album *)projectsCollection;
{
	if (!projectsCollection_)
		projectsCollection_ = [[self _fixedCollectionWithEntity:@"SourceListCollection" name:@"PROJECTS" rank:PROJECTS_COLLECTION_RANK] retain];
	
	return projectsCollection_;
}

// Checks for a Library list and creates one if not present
- (Album *)libraryGroup;
{
	if (!libraryGroup_)
		libraryGroup_ = [[self _fixedCollectionWithEntity:@"Album" name:@"Library" rank:LIBRARY_COLLECTION_RANK] retain];
	
	return libraryGroup_;
}


// Checks for a Last Import list and creates one if not present
- (Album *)lastImportGroup;
{
	if (!lastImportGroup_)
		lastImportGroup_ = [[self _fixedCollectionWithEntity:@"Album" name:@"Last Import" rank:LAST_IMPORT_COLLECTION_RANK] retain];
	
	return lastImportGroup_;
}


// Checks for a Last month list and creates one if not present
- (Album *)lastMonthGroup;
{
	if (!lastMonthGroup_)
		lastMonthGroup_ = [[self _fixedCollectionWithEntity:@"SmartAlbum" name:@"Last Month" rank:LAST_MONTH_COLLECTION_RANK] retain];
	
	return lastMonthGroup_;
}

- (Album *)lastCaptureGroup
{
	if (!lastCaptureGroup_)
		lastCaptureGroup_ = [[self _fixedCollectionWithEntity:@"Album" name:@"__LAST_CAPTURE__" rank:LAST_CAPTURE_COLLECTION_RANK] retain];
	
	return lastCaptureGroup_;
}







@end
