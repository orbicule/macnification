#import "PluginController.h"
#import "MGEFImager.h"

@implementation PluginController


- (id)initWithAPIManager:(id<PROAPIAccessing>)apiManager
{
	/*NSDateComponents *comps = [[[NSDateComponents alloc] init] autorelease];
	[comps setYear:2012];
	[comps setMonth:03];
	[comps setDay:01];
	
	NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
	
	if ([[NSDate date] earlierDate:date] == date) {
		
		// expired
		NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Beta period expired", @"")
										 defaultButton:NSLocalizedString(@"Close plugin", @"")
									   alternateButton:nil
										   otherButton:nil
							 informativeTextWithFormat:NSLocalizedString(@"This beta version of the Extended Depth plugin has expired.  Please email support@orbicule.com to obtain a newer version.", @"")];
		
		[alert runModal];
		
		return nil;
		
	}*/
	
	if (self = [super init])
	{
		apiManager_	= apiManager;
		editManager_ = [[apiManager_ apiForProtocol:@protocol(ApertureEditManager)] retain];

		if (editManager_ == nil)
		{
			[self release];
			return nil;
		}
				
	}
	
	return self;
}

- (void)dealloc
{
	[topLevelNibObjects_ makeObjectsPerformSelector:@selector(release)];
	[topLevelNibObjects_ release];

	[editManager_ release];
	
	[imageURLs_ release];
	
	[super dealloc];
}

- (void)awakeFromNib
{

}

#pragma mark -
#pragma mark ApertureEditPlugIn Methods

- (NSWindow *)editWindow
{
	if (efiWindow_ == nil)
	{
		NSBundle *myBundle = [NSBundle bundleForClass:[self class]];
		NSNib *myNib = [[NSNib alloc] initWithNibNamed:@"PluginUI" bundle:myBundle];
		
		if ([myNib instantiateNibWithOwner:self topLevelObjects:&topLevelNibObjects_])
		{
			[topLevelNibObjects_ retain];
		}
		[myNib release];
		
		[imager_ setImageSize:imageSize_];
		[imager_ setPluginController:self];

	}
	
	return efiWindow_;
}

- (void)beginEditSession
{
	NSArray *versionIDs = [editManager_ selectedVersionIds];
	
	if ([versionIDs count] < 2) {
		
		// expired
		NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Select at least 2 images", @"")
										 defaultButton:NSLocalizedString(@"Close plugin", @"")
									   alternateButton:nil
										   otherButton:nil
							 informativeTextWithFormat:NSLocalizedString(@"The extended depth plugin needs at least 2 images to merge.  Please select more images at the same time.", @"")];
		
		[alert runModal];

		
		[editManager_ cancelEditSession];
		return;
	}
	
	NSString *midID = [versionIDs objectAtIndex:[versionIDs count]/2];
	
	//NSLog([[editManager_ propertiesWithoutThumbnailForVersion:midID] description]);
	//NSLog(kCGImagePropertyExifShutterSpeedValue);
		  
	//	We need the size of the reference image so that we can properly scale to fit the thumbnail
	NSDictionary *properties = [editManager_ propertiesWithoutThumbnailForVersion:midID];
	imageSize_ = [[properties objectForKey:kExportKeyImageSize] sizeValue];
	
	// will load the window!
	[editManager_ editableVersionsOfVersions:versionIDs requestedFormat:kApertureImageFormatTIFF8 stackWithOriginal:YES];
	
	imageURLs_ = [[NSMutableArray array] retain];
	for (NSString *versionID in [editManager_ editableVersionIds]) {
		NSString *path = [editManager_ pathOfEditableFileForVersion:versionID];
		[imageURLs_ addObject:[NSURL fileURLWithPath:[path stringByExpandingTildeInPath]]];
	}
	
	// the window is loaded at this time
	[imager_ setImageURLs:imageURLs_];
	[imager_ startEFIOnBackgroundThread];
}



#pragma mark -
#pragma mark Action Methods

- (void)cancel;
{
	[editManager_ cancelEditSession];
}

- (void)doneWithImageRep:(NSBitmapImageRep *)rep;
{
	NSString *editableVersionId = [[editManager_ editableVersionIds] objectAtIndex:0];
	NSString *imagePath = [[editManager_ pathOfEditableFileForVersion:editableVersionId] stringByExpandingTildeInPath];
	
	CGImageSourceRef isrc = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:imagePath], NULL);
	CFStringRef type = CGImageSourceGetType(isrc);
	
	NSDictionary *metadata = (NSDictionary *)CGImageSourceCopyPropertiesAtIndex(isrc, 0, NULL);
	
	
	NSMutableData *data = [NSMutableData data];
	CGImageDestinationRef idst = CGImageDestinationCreateWithData((CFMutableDataRef)data, type, 1, NULL);
	CGImageDestinationAddImage(idst, [rep CGImage], (CFDictionaryRef)metadata);
	CGImageDestinationFinalize(idst);
	CFRelease(idst);
	
	[data writeToURL:[NSURL fileURLWithPath:imagePath] atomically:YES];
		
	[editManager_ addHierarchicalKeywords:[NSArray arrayWithObject:[NSArray arrayWithObject:@"Extended Depth Of Field"]] toVersions:[NSArray arrayWithObject:editableVersionId]];
	
	// remove all the unneeded versions
	NSMutableArray *unneededVersions = [[[editManager_ editableVersionIds] mutableCopy] autorelease];
	[unneededVersions removeObjectAtIndex:0];
	[editManager_ deleteVersions:unneededVersions];
	
	[editManager_ endEditSession];
}

//	Our plugin doesn't need to import so these import methods are left empty
- (void)editManager:(id<ApertureEditManager>)editManager didImportImageAtPath:(NSString *)path versionUniqueID:(NSString *)versionUniqueID{}
- (void)editManager:(id<ApertureEditManager>)editManager didNotImportImageAtPath:(NSString *)path error:(NSError *)error{}

@end
