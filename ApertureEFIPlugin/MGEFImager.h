//
//  MGEFICreator.h
//  Filament
//
//  Created by Dennis Lorson on 11/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <QuartzCore/QuartzCore.h>
#import <vImage/vImage.h>

#import "MGImageAlignmentTypes.h"

@class MGProgressIndicator;
@class PluginController;

@interface MGEFImager : NSObject 
{
	// Plugin manager
	PluginController *pluginController;

	// Internal

	IBOutlet NSWindow *efiWindow;
	IBOutlet NSImageView *imageView;
	//IBOutlet NSProgressIndicator *progressIndicator;
	IBOutlet NSButton *saveButton;
	IBOutlet NSButton *cancelButton;
	IBOutlet NSButton *alignmentCheckbox;
	IBOutlet NSButton *colorMatchCheckbox;
	IBOutlet MGProgressIndicator *progressIndicator;
	IBOutlet NSTextField *progressTextField;
	IBOutlet NSView *progressBackgroundView;
	
	IBOutlet NSSlider *sharpenSlider;

	NSMutableArray *imageURLs;
	
	BOOL efiWasCancelled;
	BOOL efiThreadInProgress;

	NSString *stackName;
	
	BOOL alignImages;
	BOOL colorMatchImages;
	
	
	CIImage *referenceImage;
	CIImage *otherImage;
	
	IAImage *referenceImageData;
	IAImage *otherImageData;
	
	IAImage *viewImageData;
}

@property (readwrite, copy) NSString *stackName;
@property (readwrite) BOOL alignImages;
@property (readwrite) BOOL colorMatchImages;

// INTERFACE FOR PluginController
- (void)setImageSize:(NSSize)size;
- (void)setPluginController:(PluginController *)ctl;
- (void)setImageURLs:(NSArray *)urls;
- (void)startEFIOnBackgroundThread;

// UI
- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)toggleAlignment:(id)sender;
- (IBAction)toggleColorMatch:(id)sender;
- (IBAction)changeSharpness:(id)sender;


@end
