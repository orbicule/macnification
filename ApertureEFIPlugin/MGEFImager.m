//
//  MGEFICreator.m
//  Filament
//
//  Created by Dennis Lorson on 11/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGEFImager.h"
#import "MGImageFusion.h"
#import "MGImageAlignment.h"
#import "MGProgressIndicator.h"
#import "PluginController.h"


@interface MGEFImager (Private)

- (void)startEFIOnBackgroundThread;

- (void)setEFIProgress:(NSNumber *)processedImageNumber;
- (void)setEFIProgressVisible:(BOOL)visible animated:(BOOL)animated;


// GENERAL
- (IAImage *)loadImage:(NSURL *)imageURL;
- (void)performEFI;
- (void)efiDidFinish;

// VIEW
- (void)updateView;
- (NSImage *)imageRepWrapperForImage:(IAImage *)img;
- (NSSize)windowSizeWithOldSize:(NSSize)oldSize adjustedForNewSize:(NSSize)size;



// COLOR MATCHING
- (void)colorMatchImage:(IAImage *)image toImage:(IAImage *)reference;
- (void)matchImage:(IAImage *)image toHistogram:(vImagePixelCount **)histogram;
- (vImagePixelCount **)histogramFromImage:(IAImage *)image;



// UTILITY
- (CIImage *)CIImageWithImage:(IAImage *)image;

@end




@implementation MGEFImager

@synthesize stackName;
@dynamic alignImages,colorMatchImages;


- (void) dealloc
{
	[pluginController release];
	[imageURLs release];
	
	IAImageRelease(viewImageData);
	[super dealloc];
}


- (void)awakeFromNib
{
	alignImages = YES;
	colorMatchImages = YES;
	
	[imageView addSubview:progressBackgroundView];
	NSRect newFrame = [imageView bounds];
	newFrame.size.height = [progressBackgroundView frame].size.height;
	[progressBackgroundView setFrame:newFrame];
	
	[self setEFIProgressVisible:NO animated:NO];
	[saveButton setEnabled:NO];
	[imageView setImage:nil];
	
	efiWasCancelled = NO;
	
	[self setEFIProgress:[NSNumber numberWithFloat:0]];	
}




- (void)setAlignImages:(BOOL)flag
{
	if (alignImages != flag) {
				
		alignImages = flag;
		
		efiWasCancelled = YES;
		// call the EFI method again, it will block until the previous one has ended
		[self startEFIOnBackgroundThread];
		
	}
	
}

- (void)setColorMatchImages:(BOOL)flag
{
	if (colorMatchImages != flag) {
		
		colorMatchImages = flag;
		
		efiWasCancelled = YES;
		// call the EFI method again, it will block until the previous one has ended
		[self startEFIOnBackgroundThread];
		
	}
	
}

#pragma mark -
#pragma mark PluginController interface

- (void)setPluginController:(PluginController *)ctl;
{
	if (ctl != pluginController) {
		[pluginController release];
		pluginController = [ctl retain];
	}
	
}

- (void)setImageSize:(NSSize)size
{
	NSSize newWindowSize = [self windowSizeWithOldSize:[efiWindow frame].size adjustedForNewSize:size];
	
	NSRect newFrame = [efiWindow frame];
	newFrame.size = newWindowSize;
	[efiWindow setFrame:newFrame display:YES];
}

- (void)setImageURLs:(NSArray *)urls
{
	if (imageURLs != urls) {
		[imageURLs release];
		imageURLs = [urls mutableCopy];
	}
}

#pragma mark -
#pragma mark UI

- (IBAction)cancel:(id)sender;
{
	efiWasCancelled = YES;
	
	[pluginController cancel];
	
}


- (IBAction)save:(id)sender;
{	
	[pluginController doneWithImageRep:[[[self imageRepWrapperForImage:viewImageData] representations] objectAtIndex:0]];
}

- (void)setEFIProgress:(NSNumber *)processedImageNumber
{
	NSInteger processedImage = [processedImageNumber integerValue];
	NSInteger imageCount = [imageURLs count];
	
	progressIndicator.progress = (float)processedImage/(float)imageCount;
	[progressTextField setStringValue:[NSString stringWithFormat:@"Focusing image %i of %i...", processedImage, imageCount]];

}

- (void)setEFIProgressVisible:(BOOL)visible animated:(BOOL)animated
{
	if (!animated)
		[CATransaction setValue:[NSNumber numberWithBool:YES] forKey:kCATransactionDisableActions];
	
	[progressIndicator layer].opacity = visible ? 1. : 0.;
	[progressBackgroundView layer].opacity = visible ? 1. : 0.;
	[progressTextField layer].opacity = visible ? 1. : 0.;
}

- (IBAction)toggleAlignment:(id)sender;
{
	[self setAlignImages:[alignmentCheckbox state] == NSOnState];
}

- (IBAction)toggleColorMatch:(id)sender;
{
	[self setColorMatchImages:[colorMatchCheckbox state] == NSOnState];
}

- (IBAction)changeSharpness:(id)sender;
{
	if (!viewImageData)
		return;
	
	NSImage *image = [self imageRepWrapperForImage:viewImageData];
	
	[imageView setImage:image];
}

#pragma mark -
#pragma mark General

- (void)efiDidFinish
{
	[saveButton setEnabled:YES];
	[self setEFIProgressVisible:NO animated:YES];

}

- (void)efiDidStart
{
	[saveButton setEnabled:NO];
	[self setEFIProgressVisible:YES animated:YES];
	
}

- (void)startEFIOnBackgroundThread
{
	[self performSelectorInBackground:@selector(performEFI) withObject:nil];
}

static CGFloat fusionTime;
static CGFloat alignmentTime;

- (void)performEFI
{
	while (efiThreadInProgress) {
		[NSThread sleepForTimeInterval:0.1];
	}
	
	[self performSelectorOnMainThread:@selector(efiDidStart) withObject:nil waitUntilDone:NO];

	
	efiThreadInProgress = YES;
	efiWasCancelled = NO;
	
	NSAutoreleasePool *threadPool = [[NSAutoreleasePool alloc] init];
	
	int referenceIndex = [imageURLs count]/2;
	
	// load the reference
	referenceImageData = [self loadImage:[imageURLs objectAtIndex:referenceIndex]];
	referenceImage = [[self CIImageWithImage:referenceImageData] retain];
	
	fusionTime = alignmentTime = 0.;
	
	int numImagesProcessed = 1;
	
	[self performSelectorOnMainThread:@selector(setEFIProgress:) 
						   withObject:[NSNumber numberWithInt:numImagesProcessed] waitUntilDone:YES];

	
	// main loop: for every other image, align and perform EFI
	for (int i = 0; i < [imageURLs count]; i++) {
		
		if (i == referenceIndex)
			continue;
		
		//NSLog(@"adding %@", [[imageURLs objectAtIndex:i] description]);
		
		if (efiWasCancelled) {
			NSLog(@"Canceling alignment");
			break;
		}
		
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		[self performSelectorOnMainThread:@selector(updateView) withObject:nil waitUntilDone:YES];
		
		otherImageData = [self loadImage:[imageURLs objectAtIndex:i]];
		
		if (colorMatchImages)
			[self colorMatchImage:otherImageData toImage:referenceImageData];
		
		otherImage = [[self CIImageWithImage:otherImageData] retain];
		
		//NSLog(@"starting alignment...");
		
		NSDate *date = [NSDate date];

		NSAffineTransform *trafo = [NSAffineTransform transform];
		
		if (alignImages) {
			MGImageAlignment *alignment = [[MGImageAlignment alloc] init];
			trafo = [alignment alignImage:otherImage toImage:referenceImage];
			[alignment release];
		}
		
		if (efiWasCancelled) {
			NSLog(@"Canceling alignment");
			break;
		}


		alignmentTime += [[NSDate date] timeIntervalSinceDate:date];
		
		// apply the trafo to the image
		CIFilter *trafoFilter = [CIFilter filterWithName:@"CIAffineTransform"];
		[trafoFilter setDefaults];
		[trafoFilter setValue:trafo forKey:@"inputTransform"];
		[trafoFilter setValue:otherImage forKey:@"inputImage"];
		
		CIImage *alignedImage = [trafoFilter valueForKey:@"outputImage"];
		
		//NSLog(@"starting fusion...");
		
		date = [NSDate date];

		MGImageFusion *fusion = [[MGImageFusion alloc] initWithImage:alignedImage referenceImage:referenceImage];
		
		// empty the existing images
		// replace the reference image with the fused one
		IAImageRelease(referenceImageData);
		IAImageRelease(otherImageData);
		
		
		IAImage *fused = [fusion fusionImage];
		
		fusionTime += [[NSDate date] timeIntervalSinceDate:date];

		
		//NSLog(@"fusion complete");
		
		
		referenceImageData = fused;
		referenceImageData->histogram = [self histogramFromImage:referenceImageData];
		
		[referenceImage release];
		referenceImage = [[self CIImageWithImage:referenceImageData] retain];
		
		[otherImage release];
		

		[self performSelectorOnMainThread:@selector(updateView) withObject:nil waitUntilDone:YES];
		
		
		[fusion release];
		[pool release];
		
		numImagesProcessed++;
		
		[self performSelectorOnMainThread:@selector(setEFIProgress:) 
							   withObject:[NSNumber numberWithInt:numImagesProcessed] waitUntilDone:YES];
		
	}
	
	NSLog(@"fusion time: %f -- alignment time: %f", fusionTime, alignmentTime);
	
	[referenceImage release];
	IAImageRelease(referenceImageData);
	
	[threadPool release];
	
	efiThreadInProgress = NO;
	efiWasCancelled = NO;
	[self performSelectorOnMainThread:@selector(efiDidFinish) withObject:nil waitUntilDone:NO];
}



- (IAImage *)loadImage:(NSURL *)imageURL
{
	// Load as bitmap data
	CGImageSourceRef src = CGImageSourceCreateWithURL((CFURLRef)imageURL, NULL);
	CGImageRef img = CGImageSourceCreateImageAtIndex(src, 0, NULL);
	
	NSInteger w = CGImageGetWidth(img);
	NSInteger h = CGImageGetHeight(img);
	
	IAImage *image = malloc(sizeof(IAImage));
	
	image->data = malloc(sizeof(unsigned char) * 4 * w * h);
	image->w = w;
	image->h = h;
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();

	CGContextRef tempCtx = CGBitmapContextCreate(image->data, w, h, 8, 4 * w, colorspace, kCGImageAlphaPremultipliedFirst);
	CGContextDrawImage(tempCtx, CGRectMake(0, 0, w, h), img);
	
	CGContextRelease(tempCtx);
	CGColorSpaceRelease(colorspace);
	CGImageRelease(img);
	CFRelease(src);
	
	// compute the histogram
	image->histogram = [self histogramFromImage:image];
	
	return image;
}





#pragma mark -
#pragma mark Color matching


- (void)colorMatchImage:(IAImage *)image toImage:(IAImage *)reference
{
	unsigned long refComponentSums[3];
	unsigned long componentSums[3];
	
	// compute component sums for all images/channels (remember, ARGB!)
	for (int j = 0; j < 3; j++) {
		long compSum = 0;
		long refCompSum = 0;
		for (int k = 0; k < 256; k++) {
			compSum += image->histogram[j + 1][k] * k;
			refCompSum += reference->histogram[j + 1][k] * k;
			
		}
		componentSums[j] = compSum;
		refComponentSums[j] = refCompSum;
		
	}
	
	// scale the components to those of the middle frame
	int newComponents[3];
	
	// precompute the scale factors
	int lookup[3][256];
	
	for (int j = 0; j < 3; j++) {
		
		for (int k = 0; k < 256; k++) {
			lookup[j][k] = k * (float)refComponentSums[j] / (float)componentSums[j];
			lookup[j][k] = MIN(lookup[j][k], 255);
		}
	}
	
	
	// use the lookup table to scale component-wise
	for (int y = 0; y < image->h; y++) {
		for (int x = 0; x < image->w; x++) {
			
			unsigned char *pixel = &(image->data[(y * image->w + x) * 4]);
			
			// ARGB
			newComponents[0] = lookup[0][pixel[1]];
			newComponents[1] = lookup[1][pixel[2]];
			newComponents[2] = lookup[2][pixel[3]];
			
			// make sure not to change the chroma by saturation of one component (tone down the luminance far enough)
			if (newComponents[0] > 255 || newComponents[0] > 255 || newComponents[0] > 255) {
				int limitingFactor = MAX(newComponents[0], MAX(newComponents[1], newComponents[2]));
				newComponents[0] = MIN(255, newComponents[0] * 255. / (float)limitingFactor);
				newComponents[1] = MIN(255, newComponents[1] * 255. / (float)limitingFactor);
				newComponents[2] = MIN(255, newComponents[2] * 255. / (float)limitingFactor);
				
			}
			
			pixel[1] = newComponents[0];
			pixel[2] = newComponents[1];
			pixel[3] = newComponents[2];
			
		}
	}
	
}


- (void)matchImage:(IAImage *)image toHistogram:(vImagePixelCount **)histogram
{
	NSInteger w = image->w;
	NSInteger h = image->h;	
	
	vImage_Buffer inImage;
	inImage.data = image->data;
	inImage.width = image->w;
	inImage.height = image->h;
	inImage.rowBytes = 4 * image->w;
	
	unsigned char *outBitmap = (unsigned char *)malloc(4 * w * h);
	vImage_Buffer outImage;
	outImage.data = outBitmap;
	outImage.width = inImage.width;
	outImage.height = inImage.height;
	outImage.rowBytes = inImage.rowBytes;
	
	const vImagePixelCount *histogram2[4];
	for (int i = 0; i < 4; i++)
		histogram2[i] = histogram[i];
	
	vImageHistogramSpecification_ARGB8888(&inImage, &outImage, histogram2, 0);
	
	free(image->data);
	image->data = outBitmap;
	
}

- (vImagePixelCount **)histogramFromImage:(IAImage *)image
{
	vImage_Buffer inImage;
	inImage.data = image->data;
	inImage.width = image->w;
	inImage.height = image->h;
	inImage.rowBytes = 4 * image->w;
	
	vImagePixelCount *histogram[4];
	for (int i = 0; i < 4; i++)
		histogram[i] = malloc(256 * sizeof(vImagePixelCount));
	
	vImageHistogramCalculation_ARGB8888(&inImage, histogram, 0);
	
	vImagePixelCount **histogram2 = malloc(4 * sizeof(vImagePixelCount *));
	for (int i = 0; i < 4; i++)
		histogram2[i] = histogram[i];
	
	return histogram2;
}





#pragma mark -
#pragma mark Utility


- (CIImage *)CIImageWithImage:(IAImage *)image
{
	NSData *data = [NSData dataWithBytesNoCopy:image->data length:4 * image->w * image->h freeWhenDone:NO];
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();

	CIImage *ciImage = [CIImage imageWithBitmapData:data
										bytesPerRow:image->w * 4
											   size:CGSizeMake(image->w, image->h) 
											 format:kCIFormatARGB8 colorSpace:colorspace];
	CGColorSpaceRelease(colorspace);

	return ciImage;
	
}


#pragma mark -
#pragma mark View

- (NSImage *)imageRepWrapperForImage:(IAImage *)img
{
	// first, apply a sharpen filter
	NSData *data = [NSData dataWithBytesNoCopy:img->data length:img->w * img->h * 4 freeWhenDone:NO];
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	CIImage *ciImg = [CIImage imageWithBitmapData:data
									  bytesPerRow:4 * img->w
											 size:CGSizeMake(img->w, img->h)
										   format:kCIFormatARGB8
									   colorSpace:colorspace];
	CGColorSpaceRelease(colorspace);
	
	CIFilter *sharpen = [CIFilter filterWithName:@"CISharpenLuminance"];
	[sharpen setValue:ciImg forKey:@"inputImage"];
	[sharpen setValue:[NSNumber numberWithFloat:[sharpenSlider floatValue]] forKey:@"inputSharpness"];
	
	CIImage *outImg = [sharpen valueForKey:@"outputImage"];
	
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithCIImage:outImg] autorelease];
	
	NSImage *image = [[[NSImage alloc] initWithSize:NSMakeSize(img->w, img->h)] autorelease];
	[image addRepresentation:rep];
	
	return image;
}


- (void)updateView
{
	IAImageRelease(viewImageData);
	viewImageData = IAImageCopy(referenceImageData);
	
	NSImage *image = [self imageRepWrapperForImage:viewImageData];
	
	[imageView setImage:image];
	[imageView display];
	[imageView setNeedsDisplay:NO];
}

- (NSSize)windowWillResize:(NSWindow *)sender toSize:(NSSize)frameSize
{
	return [self windowSizeWithOldSize:frameSize adjustedForNewSize:[[imageView image] size]];
}


- (NSSize)windowSizeWithOldSize:(NSSize)oldSize adjustedForNewSize:(NSSize)size
{	
	CGFloat aspectRatio = size.height / size.width;
	
	// the height is more important than the width (because it is more likely to go offscreen)
	CGFloat otherWindowContentHeight = [efiWindow frame].size.height - [imageView frame].size.height;
	CGFloat otherWindowContentWidth = [efiWindow frame].size.width - [imageView frame].size.width;
	
	CGFloat newImageWidth = oldSize.width - otherWindowContentWidth;
	
	CGFloat newImageHeight = newImageWidth * aspectRatio;
	
	NSSize newWindowSize = oldSize;
	newWindowSize.height = newImageHeight + otherWindowContentHeight;
	
	
	return newWindowSize;
	
	
}

@end
