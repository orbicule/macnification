//
//  MGSheetViews.m
//  Filament
//
//  Created by Dennis Lorson on 17/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGSheetViews.h"



@implementation MGSheetDividerView

static NSImage *SheetDividerImage = nil;

- (void)drawRect:(NSRect)rect
{
	if (!SheetDividerImage) {
		NSString *path = [[NSBundle bundleWithIdentifier:@"com.orbicule.extendeddepth"] pathForResource:@"sheet_divider" ofType:@"tiff"];
		SheetDividerImage = [[NSImage alloc] initWithContentsOfFile:path];

	}
	
	[SheetDividerImage drawInRect:[self bounds] fromRect:NSMakeRect(0, 0, [SheetDividerImage size].width, [SheetDividerImage size].height) operation:NSCompositeSourceOver
						 fraction:1.0];
}


@end


@implementation MGSheetFooterView

static NSImage *SheetFooterImage = nil;


- (void)drawRect:(NSRect)rect
{
	if (!SheetFooterImage) {

		NSString *path = [[NSBundle bundleWithIdentifier:@"com.orbicule.extendeddepth"] pathForResource:@"sheet_footer" ofType:@"tiff"];
		SheetFooterImage = [[NSImage alloc] initWithContentsOfFile:path];

	}
	
	[SheetFooterImage drawInRect:[self bounds] fromRect:NSMakeRect(0, 0, [SheetFooterImage size].width, [SheetFooterImage size].height) operation:NSCompositeSourceOver
						 fraction:1.0];
}



@end


