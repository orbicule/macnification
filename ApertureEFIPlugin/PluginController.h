#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import "ApertureEditPlugIn.h"

@class MGEFImager;

@interface PluginController : NSObject <ApertureEditPlugIn>
{
	id apiManager_; 
	
	NSObject<ApertureEditManager, PROAPIObject> *editManager_; 
		
	NSArray *topLevelNibObjects_;
	IBOutlet NSWindow *efiWindow_;
	
	IBOutlet MGEFImager *imager_;
	
	NSSize imageSize_;
	
	NSMutableArray *imageURLs_;
	
}

- (void)cancel;
- (void)doneWithImageRep:(NSBitmapImageRep *)rep;

@end
