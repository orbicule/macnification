//
//  CustomMDController.h
//  Filament
//
//  Created by Dennis Lorson on 09/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGCustomMetadataController.h"


@interface MGCustomMetadataManagementController : NSObject <MGCustomMetadataObserver>
{
	
	IBOutlet NSArrayController *metadataFieldsController;
	
	IBOutlet NSButton *addMDButton, *removeMDButton;
	
	IBOutlet NSWindow *removeMDSheet, *addMDSheet, *renameMDSheet;
	
	IBOutlet NSWindow *prefsWindow;
	
	IBOutlet NSTextField *addMDNameField;
	
	IBOutlet NSTextField *changeMDNameField;

	IBOutlet NSProgressIndicator *changeMDNameIndicator;
	IBOutlet NSTextField *changeMDNameProgressText;
	
	IBOutlet NSTableView *tableView;
}

- (void)refreshContent;

- (IBAction)addAttribute:(id)sender;
- (void)closeAddAttributeSheet:(id)sender;
- (IBAction)removeAttribute:(id)sender;

- (void)changeAttributeName:(id)sender;
- (void)endChangeAttributeNameSheet:(id)sender;


@end
