//
//  CaptureICDevice.m
//  Filament
//
//  Wrapper around an ImageCapture.framework-based device (mostly still cameras)
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureICDevice.h"
#import "MGLibraryController.h"
#import "MGAppDelegate.h"

@interface CaptureICDevice ()

- (NSImage *)nsImageFromCGImage:(CGImageRef)ref;

- (void)retrieveInitialLastItemFromCamera;

- (void)cameraDevice:(ICCameraDevice*)camera didAddItem:(ICCameraItem*)item;

- (void)downloadCameraItems:(NSArray *)items;

@end



@implementation CaptureICDevice

@synthesize originalDevice = originalDevice_;

- (id)initWithDevice:(ICCameraDevice *)device
{
	if ((self = [super init])) {
		originalDevice_ = [device retain];
		originalDevice_.delegate = self;
	}
	return self;
}

#pragma mark -
#pragma mark Info

- (NSString *)name
{
	return originalDevice_.name;
}

- (BOOL)canTakePicture;
{
	return [originalDevice_.capabilities containsObject:ICCameraDeviceCanTakePicture];
}

- (BOOL)hasStorage;
{
	return YES;
}

- (BOOL)hasStoredItems;
{
	return [originalDevice_.mediaFiles count] > 0;
}

- (NSInteger)numberOfStoredItems;
{
	return [originalDevice_.mediaFiles count];
}


#pragma mark -
#pragma mark Session

- (void)startSession;
{
    didBecomeReady_ = NO;
	[originalDevice_ requestOpenSession];

}

- (void)stopSession;
{
	if (filesInDownloadProgress_)
		[self cancelDownload];
	
	[lastItem_ release];
	lastItem_ = nil;
	
	[originalDevice_ requestCloseSession];
}


#pragma mark -
#pragma mark Preview

- (void)connectPreviewToView:(NSView *)view;
{
	[preview_ release];
	preview_ = (NSImageView *)[view retain];
	
	
}

- (void)disconnectPreviewFromView:(NSView *)view;
{
	[preview_ release];
	preview_ = nil;
}

- (void)retrieveInitialLastItemFromCamera
{
	return;
	
	// we should at all times be able to show a preview from the last taken pic with the camera (the one that will be imported).
	// when the device is initially connected, no pic is taken during connection so we don't have anything to show, unless we retrieve it manually from the files
	// already present on the camera.
	
	// the session is presumed to be started at this time!
	
	NSArray *items = originalDevice_.mediaFiles;
	
	if ([items count] == 0)
		return;
	
	ICCameraItem *lastProducedItem = [items objectAtIndex:0];
	
	for (ICCameraItem *item in items) {
		if ([item.creationDate earlierDate:lastProducedItem.creationDate] == lastProducedItem.creationDate)
			lastProducedItem = item;
	}
	
	[lastItem_ release];
	lastItem_ = [lastProducedItem retain];
	
	// simulate the delegate call from the device, as if the lastItem was a newly taken photo
	[self cameraDevice:originalDevice_ didAddItem:lastItem_];
}

#pragma mark -
#pragma mark Download


- (void)takePicture
{
	[super takePicture];
	
	// the item should be added to the contents of the device.  We'll get a delegate message and show the preview.
	[originalDevice_ requestTakePicture];
}


- (void)downloadCurrentItem;
{
	if (lastItem_)
		[self downloadCameraItems:[NSArray arrayWithObject:lastItem_]];
}

- (void)downloadAllItems;
{
	if ([originalDevice_.mediaFiles count] == 0)
		NSLog(@"BUG: no items on the device!");
	
	[self downloadCameraItems:originalDevice_.mediaFiles];
}

- (void)downloadCameraItems:(NSArray *)items
{
	// sanity check
	if (filesInDownloadProgress_)
		[self cancelDownload];
	
	filesInDownloadProgress_ = [items mutableCopy];
	downloadedFiles_ = [[NSMutableArray array] retain];
	
	NSString *fileDir = @"/var/tmp/MGCameraDownloads/";
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:fileDir])
		[[NSFileManager defaultManager] createDirectoryAtPath:fileDir withIntermediateDirectories:YES attributes:nil error:nil];
	
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 [NSURL fileURLWithPath:fileDir],	ICDownloadsDirectoryURL,
							 [NSNumber numberWithBool:YES],		ICOverwrite,
							 [NSNumber numberWithBool:NO],		ICDeleteAfterSuccessfulDownload,
							 nil];
	
	if ([items count] > 0) {
		[MGLibraryControllerInstance setProgressTitle:@"Importing captured images..."];
		[MGLibraryControllerInstance setProgress:0];
		[MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Importing image 1 from %i", (int)[items count]]];
		[MGLibraryControllerInstance showProgressSheet];
		
		[originalDevice_ requestDownloadFile:(ICCameraFile *)[items objectAtIndex:0]
									 options:options 
							downloadDelegate:self 
						 didDownloadSelector:@selector(didDownloadFile:error:options:contextInfo:)
								 contextInfo:nil];	
	}
	
		

}

- (void)didDownloadFile:(ICCameraFile*)file error:(NSError*)error options:(NSDictionary*)options contextInfo:(void*)contextInfo
{
	// remove the item from the files in progress
	[filesInDownloadProgress_ removeObject:file];
	
	NSString *filePath = [options objectForKey:ICSavedFilename];
	filePath = [@"/var/tmp/MGCameraDownloads/" stringByAppendingString:filePath];
	[downloadedFiles_ addObject:filePath];
	
	NSInteger totalFileCount = [filesInDownloadProgress_ count] + [downloadedFiles_ count];
	
	CGFloat ratio = 1. - (float)[filesInDownloadProgress_ count]/(float)totalFileCount;
	
	[MGLibraryControllerInstance setProgress:ratio*100];
	[MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Importing image %i from %i", (int)[downloadedFiles_ count], (int)totalFileCount]];
	
	
	if ([filesInDownloadProgress_ count] == 0) {
		[filesInDownloadProgress_ release];
		filesInDownloadProgress_ = nil;
		
		// indeterminate
		[MGLibraryControllerInstance setProgress:-1];
		[MGLibraryControllerInstance setProgressMessage:@"Processing images..."];

		// send delegate message
		if ([self.delegate respondsToSelector:@selector(captureDevice:didReceivePicturesForImport:)])
			[self.delegate captureDevice:self didReceivePicturesForImport:downloadedFiles_];
		
		// delegate is responsible for removing files
		
		// close modal sheet
		[MGLibraryControllerInstance hideProgressSheet];

	} else {
		
		NSString *fileDir = @"/var/tmp/MGCameraDownloads/";
		
		NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSURL fileURLWithPath:fileDir],	ICDownloadsDirectoryURL,
								 [NSNumber numberWithBool:YES],		ICOverwrite,
								 [NSNumber numberWithBool:NO],		ICDeleteAfterSuccessfulDownload,
								 nil];
		
		[originalDevice_ requestDownloadFile:(ICCameraFile *)[filesInDownloadProgress_ objectAtIndex:0]
									 options:options 
							downloadDelegate:self 
						 didDownloadSelector:@selector(didDownloadFile:error:options:contextInfo:)
								 contextInfo:nil];	
		
	}
}

- (void)cancelDownload
{
	[originalDevice_ cancelDownload];
	
	NSString *filePath = @"/var/tmp/MGCameraDownloads/";
	[[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];

	[filesInDownloadProgress_ release];
	filesInDownloadProgress_ = nil;

	[downloadedFiles_ release];
	downloadedFiles_ = nil;
	
	
	[MGLibraryControllerInstance hideProgressSheet];

}

#pragma mark -
#pragma mark Delegate

- (void)deviceDidBecomeReady:(ICDevice*)device;
{
    didBecomeReady_ = YES;
    
	//NSLog(@"device didBecomeReady");
	if ([self.delegate respondsToSelector:@selector(captureDeviceDidOpenSession:)])
		[self.delegate captureDeviceDidOpenSession:self];
	
	[self retrieveInitialLastItemFromCamera];
    
    
    if ([device isKindOfClass:[ICCameraDevice class]] && [self canTakePicture])
        [(ICCameraDevice *)device requestEnableTethering];
}

- (void)device:(ICDevice*)device didReceiveButtonPress:(NSString*)buttonType;
{
    //NSLog(@"button press %@", buttonType);

}

- (void)cameraDevice:(ICCameraDevice*)camera didAddItem:(ICCameraItem*)item;
{
	if (camera.contentCatalogPercentCompleted < 100) {
		return;
	}
    
    if (!didBecomeReady_)
        return;

	[lastItem_ release];
	lastItem_ = [item retain];
	
	// request the thumbnail, or add it if already available
	CGImageRef thumb;
	
	if (preview_ && (thumb = lastItem_.thumbnailIfAvailable))
		[preview_ setImage:[self nsImageFromCGImage:thumb]];
	
    NSLog(@"added item");
    
	[self downloadCurrentItem];
	
}

- (void)cameraDevice:(ICCameraDevice*)camera didReceiveThumbnailForItem:(ICCameraItem*)item;
{
	if (preview_)
		[preview_ setImage:[self nsImageFromCGImage:item.thumbnailIfAvailable]];
}


- (void)didRemoveDevice:(ICDevice*)removedDevice
{
	
}


#pragma mark -
#pragma mark Utility


- (NSImage *)nsImageFromCGImage:(CGImageRef)ref
{
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithCGImage:ref] autorelease];
	NSImage *img = [[[NSImage alloc] initWithSize:NSMakeSize([rep pixelsWide], [rep pixelsHigh])] autorelease];
	[img addRepresentation:rep];
	
	return img;
}

- (NSImage *)iconImage;
{
	NSImage *iconImage = [NSImage imageNamed:@"capture_device_ic"];
	
	[iconImage setSize:NSMakeSize(16, 16)];
	return iconImage;
}


@end
