//
//  MGMainImageBrowserCell.h
//  Filament
//
//  Created by Dennis Lorson on 10/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>

@interface MGStackImageBrowserCell : IKImageBrowserCell 
{
	NSInteger numberOfImages_;
	CGFloat imageWidth_;
}

@property (readwrite) NSInteger numberOfImages;
@property (readwrite) CGFloat imageWidth;
@end
