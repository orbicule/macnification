//
//  MGSheetViews.m
//  Filament
//
//  Created by Dennis Lorson on 17/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGSheetViews.h"



@implementation MGSheetDividerView

static NSImage *SheetDividerImage = nil;

- (void)drawRect:(NSRect)rect
{
	if (!SheetDividerImage)
		SheetDividerImage = [[NSImage imageNamed:@"sheet_divider"] retain];
	
	[SheetDividerImage drawInRect:[self bounds] fromRect:NSMakeRect(0, 0, [SheetDividerImage size].width, [SheetDividerImage size].height) operation:NSCompositeSourceOver
						 fraction:1.0];
}


@end


@implementation MGSheetFooterView

static NSImage *SheetFooterImage = nil;


- (void)drawRect:(NSRect)rect
{
	if (!SheetFooterImage)
		SheetFooterImage = [[NSImage imageNamed:@"sheet_footer"] retain];
	
	[SheetFooterImage drawInRect:[self bounds] fromRect:NSMakeRect(0, 0, [SheetFooterImage size].width, [SheetFooterImage size].height) operation:NSCompositeSourceOver
						 fraction:1.0];
}



@end


