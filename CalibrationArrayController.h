//
//  CalibrationArrayController.h
//  Filament
//
//  Created by Peter Schols on 24/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface CalibrationArrayController : NSArrayController {

	IBOutlet NSArrayController *imageArrayController;
	IBOutlet NSPanel *calibrationPanel;
	
}


- (IBAction)assignExistingCalibration:(id)sender;


@end
