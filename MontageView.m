//
//  StackTableMontageView.m
//  StackTable
//
//  Created by Dennis Lorson on 09/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MontageView.h"
#import "NSImage_MGAdditions.h"

#import "Montage.h"
#import "MontageGridLayout.h"
#import "MontageUnitView.h"

static NSSize pageMargin = {10, 15};
static CGFloat headerElementsHeight = 50;



@interface MontageView (Private)


- (NSRect)contentFrame;
- (NSSize)pageSize;

- (BOOL)knowsPageRange:(NSRangePointer)range;
- (NSRect)rectForPage:(int)pageNumber;


@end


@implementation MontageView



@dynamic dataSource;
@synthesize rendersPreview;


- (id)init
{	
    if ((self = [super initWithFrame:NSMakeRect(-5,-5,100000,5000)])) 
		layoutParameters = [[NSMutableDictionary dictionary] retain];
	
    return self;
}


- (void)dealloc
{	
	[layoutParameters release];
	
	[super dealloc];
}

- (void)setDataSource:(id <MontageViewDataSource>)dataSource
{
	if (dataSource_ == dataSource)
		return;
	
	if (![dataSource conformsToProtocol:@protocol(MontageViewDataSource)]) {
		NSLog(@"Proposed montage view datasource does not conform to protocol!");
		return;
	}
	
	dataSource_ = dataSource;
}

- (id <MontageViewDataSource>)dataSource
{
	return dataSource_;
}



- (BOOL)isFlipped
{
	return YES;
}

- (PDFDocument *)montagePDF
{
	
	NSRange pageRange;
	NSRangePointer pageRangePointer = &pageRange;
	if(![self knowsPageRange:pageRangePointer]) return nil;
	
	NSInteger page = pageRange.location;
	
	PDFDocument *doc = [[[PDFDocument alloc] init] autorelease];
	
	
	while (page < pageRange.location + pageRange.length) {
		NSData *pdfData = [self dataWithPDFInsideRect:[self rectForPage:page]];
		PDFDocument *montageDoc = [[[PDFDocument alloc] initWithData:pdfData] autorelease];
		PDFPage *montagePage = [montageDoc pageAtIndex:0];
		[doc insertPage:montagePage atIndex:[doc pageCount]];
		
		page++;
	}
	
	
	return doc;
	
}



- (void)updateLayout
{
	for (NSView *subview in [[[self subviews] copy] autorelease])
		[subview removeFromSuperview];
	
	int nImages = [self.dataSource montageViewNumberOfImages:self];
	int nMetadataFields = [self.dataSource montageViewNumberOfMetadataFields:self];
	int nImagesPerRow = [self.dataSource montageViewImagesPerRow:self];
	
	NSColor *backgroundColor = [self.dataSource backgroundColor];
	
	if (nImages < 1) return;
	
	// draw the images and text
	
	// an estimate for the ideal h/w ratio for the images.  Since the algorithm assumes this is a constant, take the first image ratio in the array.  If ratios differ strongly,
	// this might cause a very non-optimal result.
	NSImageRep *firstImage = [[self.dataSource montageView:self imageAtIndex:0] representation];
	NSSize desiredImageSize = NSMakeSize([firstImage pixelsWide], [firstImage pixelsHigh]);

	// construct the dictionary with global properties for the layout method
	//NSMutableDictionary *layoutParameters = [NSMutableDictionary dictionary];
	
	[layoutParameters removeAllObjects];

	[layoutParameters setObject:[NSValue valueWithSize:NSMakeSize(3,5)] forKey:@"unitMargin"];
	[layoutParameters setObject:[NSValue valueWithSize:[self contentFrame].size] forKey:@"compositeSize"];
	[layoutParameters setObject:[NSValue valueWithSize:desiredImageSize] forKey:@"desiredImagedSize"];
	[layoutParameters setObject:[NSNumber numberWithInt:nImagesPerRow] forKey:@"unitsPerRow"];
	[layoutParameters setObject:[NSNumber numberWithInt:nImages] forKey:@"nbUnits"];
	[layoutParameters setObject:[NSNumber numberWithInt:[MontageUnitView heightForAttributes:nMetadataFields]] forKey:@"attributeHeight"];
		
	for (int i = 0; i < nImages; i++) {
		
		NSDictionary *positionDict = [MontageGridLayout pageAndFrameForUnit:i withParameters:layoutParameters];
		
		// !!! this position is WITHIN the page contents rect on the FIRST page
		// so scale to page coords first using [self contentsFrame], then to document coords using the page nb
		NSRect imageFrame = [[positionDict valueForKey:@"unitFrame"] rectValue];
		
		NSRect contentFrame = [self contentFrame];
		imageFrame.origin.x += contentFrame.origin.x;
		imageFrame.origin.y += contentFrame.origin.y;
		
		NSInteger pageNumber = [[positionDict valueForKey:@"pageNumber"] intValue];
		imageFrame.origin.x += [self pageSize].width * (pageNumber);
		
		NSImage *img = [self.dataSource montageView:self imageAtIndex:i];
		
		// extract the metadata that the user has selected.
		NSMutableArray *attrs = [NSMutableArray array];
		for (int j = 0; j < nMetadataFields; j++) {
			NSString *val = [self.dataSource montageView:self valueForMetadataFieldAtIndex:j imageIndex:i];
			if (!val || [val length] == 0) val = @"－";
			[attrs addObject:val];
		}


		NSColor *textColor = ([backgroundColor greenComponent] < 0.7 ||
							  [backgroundColor redComponent] < 0.7 || 
							  [backgroundColor blueComponent] < 0.7) ? [NSColor whiteColor] : [NSColor blackColor];
		
		MontageUnitView *unitView = [[[MontageUnitView alloc] initWithFrame:imageFrame 
																	  image:img 
																 attributes:attrs 
																  attrStyle:[NSDictionary dictionaryWithObject:textColor forKey:@"color"]] autorelease];
		[self addSubview:unitView];
		
	}
	
	
	
}

- (void)drawRect:(NSRect)rect 
{
	NSColor *backgroundColor = [self.dataSource backgroundColor];
	
	[backgroundColor set];
	NSRectFill(rect);
	
	
	NSColor *textColor = ([backgroundColor greenComponent] < 0.7 ||
						  [backgroundColor redComponent] < 0.7 || 
						  [backgroundColor blueComponent] < 0.7) ? [NSColor whiteColor] : [NSColor blackColor];
	
		
	// draw the header + date
	// we'll assume the rect passed into this method is a page rect, as this view has no other use than for print.
	if ([self.dataSource displaysTitle]) {
		
		NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:15], NSFontAttributeName, textColor, NSForegroundColorAttributeName, nil];
		
		NSPoint textOrigin = NSMakePoint(rect.origin.x + pageMargin.width, rect.origin.y + pageMargin.height);
		[[self.dataSource title] drawAtPoint:textOrigin withAttributes:attrs];
		
	}
		
	if ([self.dataSource displaysDate]) {
		
		NSDate *date = [NSDate date];
		NSDictionary *dateAttrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:8], NSFontAttributeName, 
								   textColor, NSForegroundColorAttributeName, nil];
		NSString *dateString = [date descriptionWithCalendarFormat:@"%Y-%m-%d %H:%M" timeZone:nil locale:nil];
				
		NSPoint textOrigin = NSMakePoint(rect.origin.x + pageMargin.width, rect.origin.y + 20 + pageMargin.height);
		
		[dateString drawAtPoint:textOrigin withAttributes:dateAttrs];
		
	}
	
}


#pragma mark -
#pragma mark Pagination
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pagination
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)knowsPageRange:(NSRangePointer)range 
{
	int nImages = [self.dataSource montageViewNumberOfImages:self];
	
	if (nImages == 0) return NO;
	
	NSInteger nbPages = [[[MontageGridLayout pageAndFrameForUnit:nImages - 1 withParameters:layoutParameters] valueForKey:@"pageNumber"] intValue] + 1;

	range->location = 1;
	range->length = nbPages;
		
	return YES;
}


- (NSRect)rectForPage:(int)pageNumber 
{
	
	NSRect destRect;
	
	destRect.origin.x = [self pageSize].width * (pageNumber - 1);
	destRect.origin.y = 0;
	destRect.size = [self pageSize];
	
	return destRect;
	
}

- (NSSize)pageSize
{
	// returns the printable size of the user selected page type, taking into account margin and paper orientation.
	NSPrintInfo *info = [NSPrintInfo sharedPrintInfo];
	NSSize paperSize = [info paperSize];
	
	CGFloat margin = 15.0;
	
	paperSize.width -= 2 * margin;
	paperSize.height -= 2 * margin;
	
	return paperSize;
}

- (NSRect)contentFrame
{
	// returns the content frame (being the frame used for the image grid) in the coordinate system of the page.
	
	// start with the full frame
	NSRect contentFrame;
	contentFrame.origin = NSZeroPoint;
	contentFrame.size = [self pageSize];
	
	// now substract according to the user selected header features etc
	contentFrame = NSInsetRect(contentFrame, pageMargin.width, pageMargin.height);
	
	if ([self.dataSource displaysDate] || [self.dataSource displaysTitle]) {
		
		contentFrame.origin.y += headerElementsHeight;
		contentFrame.size.height -= headerElementsHeight;
		
	}
	
	return contentFrame;
	
}




@end
