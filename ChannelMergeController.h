//
//  ChannelMergeController.h
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>

@class ChannelMergeBinView;
@class Album;

@interface ChannelMergeController : NSWindowController {
	
	IBOutlet NSArrayController *rController, *gController, *bController, *cController;
	IBOutlet IKImageBrowserView *rView, *gView, *bView, *cView;;
	
	NSArray *draggedItems;
	NSArrayController *draggedItemsSource;
	
	IBOutlet NSView *channelMergeView;
	
	IBOutlet ChannelMergeBinView *rBin, *gBin, *bBin, *cBin;
	
	IBOutlet NSButton *cancelButton, *mergeButton;
	
	NSDate *lastOpenDate;
    
    Album *destinationFolder;
}

@property (readwrite, retain) Album *destinationFolder;


- (void)openMergeSheet:(id)sender;
- (void)closeMergeSheet:(id)sender;

- (BOOL)distributeImagesInBins;

- (void)updateCompositeImage;
- (void)addMergedImagesToLibrary;


@end
