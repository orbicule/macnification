//
//  BlackBackgroundImageView.m
//  Filament
//
//  Created by Dennis Lorson on 04/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "BlackBackgroundImageView.h"


@implementation BlackBackgroundImageView

- (id)initWithCoder:(NSCoder *)coder 
{
    self = [super initWithCoder:coder];
    if (self) {
        
        NSImage *shadow = [NSImage imageNamed:@"capturePreviewShadow"];
        int h = [shadow size].height;
        
        NSRect dstFrame = [self bounds];
        dstFrame.origin.y = NSMaxY(dstFrame) - h;
        dstFrame.size.height = h;
        
        NSImageView *shadowView = [[[NSImageView alloc] initWithFrame:dstFrame] autorelease];
        [shadowView setAutoresizingMask:NSViewWidthSizable | NSViewMinYMargin];
        [shadowView setImage:shadow];
        [shadowView setImageScaling:NSImageScaleAxesIndependently];
        [self addSubview:shadowView];
        
    }
    return self;
}

- (void)drawRect:(NSRect)rect
{
	[[NSColor colorWithDeviceRed:60./255. green:60./255. blue:60./255. alpha:1.] set];
	NSRectFill([self bounds]);
	
	[super drawRect:rect];
}


@end
