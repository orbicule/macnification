//
//  MGFunctionAdditions.h
//  LightTable
//
//  Created by Dennis Lorson on 30/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


BOOL MGEqualRects(NSRect first, NSRect second);

CGFloat sign(CGFloat value);

NSSize MGScaleSize(NSSize size, CGFloat scale);
NSPoint MGRectCenter(NSRect rect);
NSPoint MGPointLinearCombination(NSPoint first, NSPoint second, CGFloat weight);

CGFloat MGDeviceIntegralValue(CGFloat value);
NSPoint MGDeviceIntegralPoint(NSPoint point);
NSRect MGDeviceIntegralRect(NSRect rect);

void MGImageGetSlicePixels(CGImageRef image, unsigned char **pixels, NSInteger *pixelCount, NSPoint fromPoint, NSPoint toPoint);

NSSize MGGetDimensionsForImage(NSString *path);
NSPoint MGRectLineIntersection(NSPoint lineStartPoint, NSPoint lineEndPoint, NSRect rect);



@interface MGFunctionAdditions : NSObject {

}

@end
