//
//  MGHUDScrollView.h
//  Filament
//
//  Created by Dennis Lorson on 27/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGHUDScrollView : NSScrollView {

}

+ (Class)_verticalScrollerClass;


@end
