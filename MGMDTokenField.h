//
//  MGMDTokenField.h
//  MetaData
//
//  Created by Dennis Lorson on 7/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGMDTokenField : NSTokenField {
	
	id observableController;
	NSString *observableKeyPath;
	
	BOOL raisedEditingActive;


}

@property(nonatomic) BOOL raisedEditingActive;


- (NSSet *)draggedKeywords;


@end
