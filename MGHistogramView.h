//
//  HistogramView.h
//  ImageHistogram
//
//  Created by Dennis Lorson on 23/06/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <vImage/vImage.h>
#import <QuartzCore/QuartzCore.h>

typedef enum _IHColorspace {
	
	IHRGBColorspace = 1,
	IHHSLColorspace = 2,
	IHMonochromeColorspace = 3,
	
} IHColorspace;


@interface MGHistogramView : NSView {
	
	NSInteger histogramValues[4][256];
	NSInteger nbChannels;
	
	IHColorspace activeColorspace;
	NSInteger activeChannel;
		
	CGFloat lowerRangeBound[3], upperRangeBound[3];
	
	// transition
	BOOL isInAnimationBlock;
	CGFloat fadeProgress;
	NSImage *fromImage, *toImage;

}


- (void)setLowerRangeBound:(CGFloat)bound forChannel:(NSInteger)channel;
- (void)setUpperRangeBound:(CGFloat)bound forChannel:(NSInteger)channel;
- (void)resetRanges;

- (void)setCIImage:(CIImage *)img;


- (NSGradient *)hueGradient;

- (void)setColorspace:(IHColorspace)cs;
- (void)setActiveChannel:(NSInteger)channel;

- (NSBezierPath *)pathWithDatapointsFromChannel:(NSInteger)channel withMaximumHistogramValue:(NSInteger)maxVal;


- (void)beginAnimationBlock;
- (void)endAnimationBlock;
- (void)setFromImage:(NSImage *)image;
- (void)setToImage:(NSImage *)image;
- (void)setFadeProgress:(CGFloat)progress;



void IHComputeHistogram(unsigned char *data, NSInteger bins[4][256], NSInteger bpr, NSInteger spp, NSInteger w, NSInteger h, NSInteger componentOffset, NSInteger channelCount, IHColorspace colorspace);
void IHPostProcessHistogram(NSInteger bins[4][256], NSInteger channelCount, NSInteger *correctedChannelCount);


@end
