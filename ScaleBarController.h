//
//  ScaleBarController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 4 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "ImageArrayController.h"
#import "ScaleBarPositionView.h"


@interface ScaleBarController : NSWindowController {
	
	ImageArrayController *imageArrayController;
	IBOutlet id fontNameButton;
	IBOutlet ScaleBarPositionView *scaleBarPositionView;
}

- (void)setup;
- (IBAction)addScaleBar:(id)sender;
- (id)initWithImageArrayController:(ImageArrayController *)controller;
- (IBAction)showFontPanel:(id)sender;
- (void)changeFont:(id)sender;
- (IBAction)closeScaleBarPanel:(id)sender;



// Accessors
- (ImageArrayController *)imageArrayController;
- (void)setImageArrayController:(ImageArrayController *)value;



@end
