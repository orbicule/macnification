//
//  MGMDView.h
//  MetaDataView
//
//  Created by Dennis on 8/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGCustomMetadataController.h"

@class MGMDFieldView;
@class MGMDChannelView;


@protocol MGMDManualObserver

- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object;

@end


																										#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface MGMDView : NSView <MGMDManualObserver, MGCustomMetadataObserver, NSWindowDelegate> 
																										#else
@interface MGMDView : NSView <MGMDManualObserver, MGCustomMetadataObserver>
																										#endif

{
	
	id delegate;
	
	NSMutableArray *fields;
		
	IBOutlet NSView *headerView, *footerView;
	
	id controller;
	
	MGMDFieldView *draggedField, *draggedFieldPlaceholder;
	NSUInteger originalFieldPosition;
	
	BOOL editModeOn;
	
	NSPoint dragMouseImageOffset;
	
	IBOutlet NSPopUpButton *addFieldButton;
	IBOutlet NSButton *toggleEditModeButton;
	IBOutlet NSMenu *addFieldMenu;
	IBOutlet NSMenuItem *imagePlaceholderMenuItem, *instrumentPlaceholderMenuItem, *otherPlaceholderMenuItem, *customPlaceholderMenuItem;
	
	IBOutlet MGMDChannelView *channelView;
	
	
	NSDictionary *instrumentAttributes, *imageAttributes, *otherAttributes;
	NSDictionary *builtInAttributes;
	NSDictionary *customAttributes;
	NSMutableArray *customMenuItems;					// the items that are currently in the menu (and are not builtin)

	NSMutableArray *insertedFieldIdentifiers;			// the fields that are inserted

	
	
	NSArray *defaultFields;							// the fields that are inserted by default
	
	NSMutableSet *fieldsRegisteredAsSharedObserver;

}


@property(nonatomic) BOOL editModeOn;
@property(nonatomic, assign) id delegate;

// API
- (IBAction)toggleEditMode:(id)sender;
- (IBAction)endEditingMode:(id)sender;

- (MGMDFieldView *)addFieldWithIdentifier:(NSString *)identifier;
- (MGMDFieldView *)addSeparator;

- (void)removeFieldWithIdentifier:(NSString *)identifier;


- (void)setCustomAttributes:(NSArray *)customAttrs;
- (void)refreshCustomAttributesList;
- (void)saveFieldsToPrefs;


// Methods for subviews etc.
- (CGFloat)globalMinimumLabelWidth;

- (NSMenu *)menuForContentsOfField:(MGMDFieldView *)field withIdentifier:(NSString *)identifier;

- (void)startDraggingField:(MGMDFieldView *)field withEvent:(NSEvent *)theEvent;

- (MGMDChannelView *)channelView;

- (void)subviewDidUpdateFrame:(MGMDFieldView *)subview;


@end



