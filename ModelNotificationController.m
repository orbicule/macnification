//
//  ModelNotificationController.m
//  Filament
//
//  Created by Dennis Lorson on 30/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ModelNotificationController.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"
#import "ImageArrayController.h"

static ModelNotificationController *sharedController;

@implementation ModelNotificationController

@synthesize postNotifications;


- (id) init
{
	self = [super init];
	if (self != nil) {
		
		receivingController = [MGLibraryControllerInstance imageArrayController];
		self.postNotifications = YES;
	}
	return self;
}


+ (ModelNotificationController *)sharedNotificationController
{
	if (!sharedController) {
		
		sharedController = [[ModelNotificationController alloc] init];
		
	}
	
	return sharedController;
	
}


- (void)postSelectionChangeForKeyPath:(NSString *)keyPath ofObject:(id)object;
{
	if (!postNotifications) return;
	
	NSString *prefix = @"";
	if ([object isKindOfClass:[NSManagedObject class]]) {
		
		NSManagedObject *managedObject = (NSManagedObject *)object;
		
		NSString *entityName = [[managedObject entity] name];
		
		if ([entityName isEqualToString:@"Instrument"])
			prefix = @"instrument.";
		else if ([entityName isEqualToString:@"Channel"])
			prefix = @"instrument.channel.";
		else if ([entityName isEqualToString:@"Experiment"])
			prefix = @"experiment.";
		else if ([entityName isEqualToString:@"Experimenter"])
			prefix = @"experimenter.";

		
	}
	
	NSString *absoluteKeyPath = [prefix stringByAppendingString:keyPath];
	
	
	NSNotificationQueue *queue = [NSNotificationQueue defaultQueue];
	
	// include the keypath in the name, so the notification queue can coalesce notifications with the same keyPath
	NSString *name = [@"ModelPropertyChanged/" stringByAppendingString:absoluteKeyPath];
	
	NSNotification *not = [NSNotification notificationWithName:name object:receivingController userInfo:[NSDictionary dictionaryWithObjectsAndKeys:object, @"source", absoluteKeyPath, @"keyPath", nil]];
	
	[queue enqueueNotification:not postingStyle:NSPostASAP coalesceMask:NSNotificationCoalescingOnName forModes:nil];
	
}

@end
