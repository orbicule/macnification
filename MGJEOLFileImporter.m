//
//  MGJEOLFileImporter.m
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGJEOLFileImporter.h"

#import "MGMetadataSet.h"
#import "MGImageFileImporter_Private.h"


@interface MGJEOLFileImporter ()

- (NSString *)_valueForTag:(NSString *)tag inString:(NSString *)entireString;
+ (NSArray *)_supportedMetadataTags;

@end


@implementation MGJEOLFileImporter

+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	NSString *pathToTextFile = [[path stringByDeletingPathExtension] stringByAppendingPathExtension:@"TXT"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:pathToTextFile])
        return NO;
    
    NSString *textFileContents = [NSString stringWithContentsOfFile:pathToTextFile encoding:NSISOLatin1StringEncoding error:nil];

    for (NSString *key in [self _supportedMetadataTags])
        if ([textFileContents rangeOfString:key].location != NSNotFound)
            return YES;
        
    return NO;
}

+ (NSArray *)_supportedMetadataTags
{
    return [NSArray arrayWithObjects:@"CM_DATE", @"CM_TIME", @"CM_COMMENT", @"CM_OPERATOR", @"CM_OPERATOR", @"CM_INSTRUMENT", @"CM_ACCEL_VOLT",
            @"CM_MAG", @"SM_MICRON_MARKER", @"SM_WD", @"SM_SPOT_SIZE", @"SM_TITLE", nil];
}

+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:@"tiff", @"tif", @"jpg", nil];
}

- (float)fractionOfImportTimeForNonRepresentationWork;
{
	return 0.2;
}


- (MGMetadataSet *)_generateMetadata
{
	MGMetadataSet *set = [super _generateMetadata];
	
	
	NSString *pathToTextFile = [[filePath_ stringByDeletingPathExtension] stringByAppendingPathExtension:@"TXT"];
	NSString *textFileContents = [NSString stringWithContentsOfFile:pathToTextFile encoding:NSISOLatin1StringEncoding error:nil];
	
    // default attribute
    [set setValue:[NSDate date] forKey:MGMetadataImageCreationDateKey];
    
	
	if ([textFileContents length] == 0)
        return set;
    
    
    NSString *cds = [self _valueForTag:@"$CM_DATE" inString:textFileContents];
    NSString *cts = [self _valueForTag:@"$CM_TIME" inString:textFileContents];
    NSCalendarDate *creationDate = nil;
    if (cds)
        [NSCalendarDate dateWithString:[NSString stringWithFormat:@"%@, %@", cds, cts] calendarFormat:@"%Y-%m-%d, %I:%M:%S %p"];
    
    NSString *title = [self _valueForTag:@"$SM_TITLE" inString:textFileContents];
    NSString *comment = [self _valueForTag:@"$CM_COMMENT" inString:textFileContents];
    NSString *operator = [self _valueForTag:@"$CM_OPERATOR" inString:textFileContents];
    NSString *instrumentName = [self _valueForTag:@"$CM_INSTRUMENT" inString:textFileContents];
    NSString *voltage = [self _valueForTag:@"$CM_ACCEL_VOLT" inString:textFileContents];
    NSString *magnification = [self _valueForTag:@"$CM_MAG" inString:textFileContents];
    NSString *micronMarker = [self _valueForTag:@"$$SM_MICRON_MARKER" inString:textFileContents];
    NSString *workingDistance = [self _valueForTag:@"$$SM_WD" inString:textFileContents];
    NSString *spotSize = [self _valueForTag:@"$$SM_SPOT_SIZE" inString:textFileContents];
    NSString *unit = nil;
    if ([micronMarker length] > 1)
        unit = [micronMarker substringWithRange:NSMakeRange([micronMarker length]-2, 2)];
    
    if ([unit isEqualTo:@"um"]) 
        unit = [NSString stringWithFormat:@"%cm", 181];
    
    if (creationDate)
        [set setValue:creationDate forKey:MGMetadataImageCreationDateKey];
    
    if (comment)
        [set setValue:comment forKey:MGMetadataImageCommentKey];
    
    // title as comment
    if (title)
        [set setValue:title forKey:MGMetadataImageCommentKey];
    
    if (micronMarker)
        [set setValue:[NSNumber numberWithFloat:[micronMarker floatValue]] forKey:MGMetadataImageSuggestedScaleBarLengthKey];
    
    if (voltage)
        [set setValue:[NSNumber numberWithInt:1000*[voltage intValue]] forKey:MGMetadataInstrumentVoltageKey];
    
    if (magnification)
        [set setValue:[NSNumber numberWithInt:[magnification intValue]] forKey:MGMetadataInstrumentMagnificationKey];
    
    if (workingDistance)
        [set setValue:[NSNumber numberWithInt:[workingDistance intValue]] forKey:MGMetadataInstrumentWorkingDistanceKey];
    
    if (spotSize)
        [set setValue:[NSNumber numberWithInt:[spotSize intValue]] forKey:MGMetadataInstrumentSpotSizeKey];
    
    if (instrumentName)
        [set setValue:instrumentName forKey:MGMetadataInstrumentNameKey];
    
    if (operator)
        [set setValue:operator forKey:MGMetadataExperimenterNameKey];
    
    
    // Calibration
    
    NSImage *sourceImage = [[[NSImage alloc] initByReferencingFile:[filePath_ stringByExpandingTildeInPath]] autorelease]; 
    NSBitmapImageRep *TIFFRep = [NSBitmapImageRep imageRepWithData:[sourceImage TIFFRepresentation]];
    NSSize imageSize = NSMakeSize([TIFFRep pixelsWide], [TIFFRep pixelsHigh]);
    float scaleFactor = imageSize.width/640;
    
    
    // Calculate the pixels per unit
    double pixelsPerUnit = 0.00494 * [magnification doubleValue] * scaleFactor;
    
    [set setValue:[NSNumber numberWithDouble:pixelsPerUnit] forKey:MGMetadataCalibrationHorizontalPixelsForUnitKey];
    
    if (unit)
        [set setValue:unit forKey:MGMetadataCalibrationUnitKey];

    
    
	return set;
	
}




- (NSString *)_valueForTag:(NSString *)tag inString:(NSString *)entireString;
{
	NSArray *entireStringSeparatedByTag = [entireString componentsSeparatedByString:[NSString stringWithFormat:@"%@ ", tag]];
	
    if ([entireStringSeparatedByTag count] != 2)
        return nil;
    
    NSString *tempString = [entireStringSeparatedByTag objectAtIndex:1];
    NSCharacterSet *cSet = [NSCharacterSet newlineCharacterSet];
    NSArray *results = [tempString componentsSeparatedByCharactersInSet:cSet];
    
    if ([results count] == 0)
        return nil;
    
    return [results objectAtIndex:0];
}



@end
