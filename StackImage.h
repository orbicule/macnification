//
//  StackTableImage.h
//  StackTable
//
//	The model object for a StackTable image.
//
//  Created by Dennis Lorson on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KSExtensibleManagedObject.h"


static const CGFloat defaultImageWidth = 120.0;			// the dimensions used when an image object is added to the light table for the first time
static const CGFloat defaultImageHeight = 90.0;


@interface StackImage : KSExtensibleManagedObject 
{

}

@end
