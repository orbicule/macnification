//
//  CaptureAdjustmentsController.m
//  Filament
//
//  Created by Dennis Lorson on 01/03/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureAdjustmentsController.h"


@interface CaptureAdjustmentsController ()

- (void)initializeFilters;

@end



@implementation CaptureAdjustmentsController

@synthesize brightness = brightness_;
@synthesize contrast = contrast_;
@synthesize saturation = saturation_;
@synthesize exposure = exposure_;
@synthesize whitePoint = whitePoint_;

- (id) init
{
	self = [super init];
	if (self != nil) {
		[self initializeFilters];
	}
	return self;
}

- (void) dealloc
{
	[colorControlsFilter_ release];
	[whitePointFilter_ release];
	[exposureFilter_ release];
	[super dealloc];
}


- (void)initializeFilters
{
	colorControlsFilter_ = [[CIFilter filterWithName:@"CIColorControls"] retain];
	whitePointFilter_ = [[CIFilter filterWithName:@"WhiteBalanceCorrectionFilter"] retain];
	exposureFilter_ = [[CIFilter filterWithName:@"CIExposureAdjust"] retain];
	
	[self resetDefaults];
}


#pragma mark -


- (void)resetDefaults;
{
	self.contrast = 1.0;
	self.brightness = 0.0;
	self.saturation = 1.0;
	self.exposure = 0.5;
	self.whitePoint = NSZeroPoint;
}

- (void)setAutoWhitePoint:(BOOL)onOrOff;
{
	
}

- (void)setUsesExposure:(BOOL)onOrOff;
{
	useExposure_ = onOrOff;
}


- (void)setBrightness:(CGFloat)val
{
	brightness_ = val;
	[colorControlsFilter_ setValue:[NSNumber numberWithFloat:val] forKey:@"inputBrightness"];
}

- (void)setSaturation:(CGFloat)val
{
	saturation_ = val;
	[colorControlsFilter_ setValue:[NSNumber numberWithFloat:val] forKey:@"inputSaturation"];
}

- (void)setContrast:(CGFloat)val
{
	contrast_ = val;
	[colorControlsFilter_ setValue:[NSNumber numberWithFloat:val] forKey:@"inputContrast"];
}

- (void)setExposure:(CGFloat)val
{
	exposure_ = val;
    
    CGFloat absoluteExposure = -10 + exposure_ * 20;
    
	[exposureFilter_ setValue:[NSNumber numberWithFloat:absoluteExposure] forKey:@"inputEV"];
}

- (void)setWhitePoint:(NSPoint)point
{	
	[whiteColor_ release];
	whiteColor_ = nil;
	
	// to trigger an update next frame
	[whitePointFilter_ setValue:nil forKey:@"inputColor"];

	whitePoint_ = point;

}


#pragma mark -



- (CIImage *)filterImage:(CIImage *)inImage;
{
	// get the color of an area of 3x3 around the given white point
	// this white point is in image coordinates: [0..1]
	if (!NSEqualPoints(whitePoint_, NSZeroPoint) && ![whitePointFilter_ valueForKey:@"inputColor"]) {
		// need to update the color
		NSPoint absolutePoint;
		absolutePoint.x = [inImage extent].origin.x + whitePoint_.x * [inImage extent].size.width;
		absolutePoint.y = [inImage extent].origin.y + whitePoint_.y * [inImage extent].size.height;

		//NSLog(@"point: %@", NSStringFromPoint(absolutePoint));
		
		CIFilter *avg = [CIFilter filterWithName:@"CIAreaAverage"];
		[avg setValue:inImage forKey:@"inputImage"];
		[avg setValue:[CIVector vectorWithX:absolutePoint.x-1 Y:absolutePoint.y-1 Z:3 W:3] forKey:@"inputExtent"];
		
		CIImage *avgImage = [avg valueForKey:@"outputImage"];
		
		CIContext *ctx = [[[NSApp mainWindow] graphicsContext] CIContext];
		
		CGRect bounds = CGRectMake(0.0, 0.0, 1, 1);
		uint8 data[4];
		data[0] = data[1] = data[2] = data[2] = 0;
		[ctx render:avgImage toBitmap:data rowBytes:8 bounds:bounds format:kCIFormatARGB8 colorSpace:NULL];
		
		if ((float)(data[3] + data[1] + data[2]) * 0.33333 < 10.0)
			data[3] = data[1] = data[2] = 0.1;
		
		//NSLog(@"data: %i  %i    %i   %i ", data[0], data[1], data[2], data[3]);
		
		CIColor *color = [CIColor colorWithRed:(float)data[1]/255. green:(float)data[2]/255. blue:(float)data[3]/255. alpha:1.0];
		
		[whitePointFilter_ setValue:color forKey:@"inputColor"];
				
	}
	
	CIImage *outImg = inImage;
	
	if ([whitePointFilter_ valueForKey:@"inputColor"]) {
		[whitePointFilter_ setValue:outImg forKey:@"inputImage"];
		outImg = [whitePointFilter_ valueForKey:@"outputImage"];
	}
	
	if (useExposure_) {
		[exposureFilter_ setValue:outImg forKey:@"inputImage"];
		outImg = [exposureFilter_ valueForKey:@"outputImage"];
	}
	
	[colorControlsFilter_ setValue:outImg forKey:@"inputImage"];
	outImg = [colorControlsFilter_ valueForKey:@"outputImage"];
	
	return outImg;
}


@end


