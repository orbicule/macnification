//
//  MGRedButtonCell.m
//  Filament
//
//  Created by Dennis Lorson on 12/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGRedButtonCell.h"


@implementation MGRedButtonCell

- (void)drawBezelWithFrame:(NSRect)frame inView:(NSView *)controlView
{
	[[NSGraphicsContext currentContext] saveGraphicsState];

	NSAffineTransform *trafo = [NSAffineTransform transform];
	[trafo translateXBy:0 yBy:frame.size.height];
	[trafo scaleXBy:1 yBy:-1];
	
	[trafo concat];
	
	BOOL highlighted = [self isHighlighted];
	
	NSImage *leftImage = highlighted ? [NSImage imageNamed:@"red_button_left_highlighted"] : [NSImage imageNamed:@"red_button_left"];
	NSImage *middleImage = highlighted ? [NSImage imageNamed:@"red_button_middle_highlighted"] : [NSImage imageNamed:@"red_button_middle"];
	NSImage *rightImage = highlighted ? [NSImage imageNamed:@"red_button_right_highlighted"] : [NSImage imageNamed:@"red_button_right"];
	
	NSRect leftPart, middlePart, rightPart;
	
	NSDivideRect(frame, &leftPart, &middlePart, [leftImage size].width, NSMinXEdge);
	NSDivideRect(middlePart, &rightPart, &middlePart, [rightImage size].width, NSMaxXEdge);
	
	[leftImage drawInRect:leftPart fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[middleImage drawInRect:middlePart fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[rightImage drawInRect:rightPart fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];

	
	[[NSGraphicsContext currentContext] restoreGraphicsState];
	
}

- (NSRect)drawTitle:(NSAttributedString *)title withFrame:(NSRect)frame inView:(NSView *)controlView
{
	NSRect adjFrame = frame;
	adjFrame.origin.y -= 1;
	
	NSMutableAttributedString *str = [title mutableCopy];
	[str addAttributes:[NSDictionary dictionaryWithObject:[NSColor colorWithCalibratedWhite:0.9 alpha:1.0] forKey:NSForegroundColorAttributeName] range:NSMakeRange(0, [str length])];
	
	return [super drawTitle:str withFrame:adjFrame inView:controlView];
}


@end
