//
//  MGImageLayer.m
//  Filament
//
//  Created by Dennis Lorson on 06/09/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <OpenGL/gl.h>

#import "MGImageLayer.h"
#import "MGCoreImageMutex.h"


@implementation MGImageLayer

@synthesize image = _image;

- (NSInteger)maximumTextureSize
{
	GLint texSize; 
    
    glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE_EXT, &texSize);

    if (glGetError() == GL_INVALID_ENUM)
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, &texSize);
	
	return (NSInteger)texSize;
}

- (id < CAAction >)actionForKey:(NSString *)aKey
{
	if ([aKey isEqualToString:@"contents"] || [aKey isEqualToString:@"bounds"] || [aKey isEqualToString:@"frame"] || [aKey isEqualToString:@"hidden"])
		return nil;

    return [super actionForKey:aKey];
	
}

#pragma mark -
#pragma mark Drawing


-(CGLContextObj)copyCGLContextForPixelFormat:(CGLPixelFormatObj)pixelFormat
{
	CGLContextObj obj = [super copyCGLContextForPixelFormat:pixelFormat];
	
	if (_ciContext)
		[_ciContext release];
	
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 (id)CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCIContextOutputColorSpace,
							 //(id)CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCIContextWorkingColorSpace,    // Don't set the working color space -- causes incorrect adjustments
							 nil];
	
	_ciContext = [[CIContext contextWithCGLContext:obj pixelFormat:pixelFormat colorSpace:NULL options:options] retain];
	
	return obj;
}


-(void)drawInCGLContext:(CGLContextObj)glContext pixelFormat:(CGLPixelFormatObj)pixelFormat forLayerTime:(CFTimeInterval)timeInterval displayTime:(const CVTimeStamp *)timeStamp
{
	CGRect bounds = [self bounds];

	CGRect dstRect = bounds;
	dstRect.size.width *= 0.5;
	dstRect.size.height *= 0.5;
	
	
	CGLSetCurrentContext(glContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	glViewport(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height);
	glOrtho(bounds.origin.x, bounds.size.width-1, bounds.origin.y, bounds.size.height-1, -1, 1);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	
	if (self.image) {
		
		MGCoreImageMutex *mutex = [MGCoreImageMutex sharedMutex];
		
		@synchronized(mutex) {
			[_ciContext drawImage:self.image atPoint:bounds.origin fromRect:[self.image extent]];
			
			[_ciContext reclaimResources];
			[_ciContext clearCaches];
		}
				

	}
	
	[super drawInCGLContext:glContext pixelFormat:pixelFormat forLayerTime:timeInterval displayTime:timeStamp];
	
}




@end
