//
//  Image.m
//  Macnification
//
//  Created by Peter Schols on 30/10/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import "Image.h"
#import "Keyword.h"
#import "CIFilterExtensions.h"
#import "ROI.h"
#import "LineROI.h"
#import "RectangleROI.h"
#import "CircleROI.h"
#import "PolygonROI.h"
#import "CurvedLineROI.h"
#import "NSManagedObjectExtensions.h"
#import "IPTCController.h"
#import "ScaleBar.h"
#import "NSImage_MGAdditions.h"
#import "FormatterController.h"
#import "MGFunctionAdditions.h"
#import "NSBitmapImageRep_Conversion.h"
#import "MGAppDelegate.h"
#import "StringExtensions.h"
#import "MGCIContextManager.h"
#import "SourceListTreeController.h"
#import "MGImageCache.h"
#import "MGLibraryController.h"
#import "MGLibrary.h"
#import "Stack.h"


@interface Image ()


- (NSString *)newUnitToAccomodateLength:(double)oldLength unit:(NSString *)oldUnit withScaleFactor:(double *)scaleFactor;


- (void)generateEditPath;
- (NSString *)tmpEditPathExpanded:(BOOL)expanded;

@end



@implementation Image


@dynamic name, instrument;


# pragma mark -
# pragma mark Housekeeping


+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	NSMutableSet *kps = [[[super keyPathsForValuesAffectingValueForKey:key] mutableCopy] autorelease];

	
	if ([key isEqualToString:@"filteredImage"])
		[kps addObject:@"filters"];
	
	if ([key isEqualToString:@"filteredImageRepWithScaleBar"]) {
		[kps addObject:@"scaleBar"];
		[kps addObject:@"scaleBar.length"];		
	}
	
	if ([key isEqualToString:@"roiCount"])
		[kps addObject:@"rois"];
	
	if ([key isEqualToString:@"hasFilters"])
		[kps addObject:@"filters"];
	
	if ([key isEqualToString:@"isCalibrated"])
		[kps addObject:@"calibration"];
	
	if ([key isEqualToString:@"scaleBar"])
		[kps addObject:@"imageSubtitle"];
	
	if ([key isEqualToString:@"allKeywords"])
		[kps addObject:@"keywords"];
	
	if ([key isEqualToString:@"hasComment"])
		[kps addObject:@"comment"];
	
	if ([key isEqualToString:@"ratingString"])
		[kps addObject:@"rating"];
	
	return kps;
	
}




+ (NSSet *)keyPathsForValuesAffectedByValueForKey:(NSString *)key
{
	// this is the inverse of the previous method for our own KVO system. Returns keys dependent on a given key
	if ([key isEqualToString:@"filters"])
		return [NSSet setWithObjects:@"filteredImage", @"hasFilters", nil];
	else if ([key isEqualToString:@"rois"] )
		return [NSSet setWithObjects:@"roiCount", nil];
	else if ([key isEqualToString:@"calibration"] )
		return [NSSet setWithObjects:@"isCalibrated", nil];
	
	else
		return [super keyPathsForValuesAffectingValueForKey:key];
}

- (void)awakeFromInsert;
{
	[super awakeFromInsert];
	[self setBrowserVersion:0]; 
	
	colorSpaceName = nil;
}


- (void)dealloc
{
	[colorSpaceName release];
	
	[super dealloc];
}


# pragma mark -
# pragma mark Props override (Automatic Experimenter creation)


- (void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
	// check if the requested path contains "experimenter" or "instrument"
	// if so, create this item for us
	if ([keyPath hasPrefix:@"instrument"]) {
		if (![self valueForKey:@"instrument"]) {
			KSExtensibleManagedObject *instrument = [[[KSExtensibleManagedObject alloc] initWithEntity:[NSEntityDescription entityForName:@"Instrument" 
																												   inManagedObjectContext:[self managedObjectContext]]
																		insertIntoManagedObjectContext:[self managedObjectContext]] autorelease];
			[super setValue:instrument forKeyPath:@"instrument"];
			NSLog(@"Created instrument on-the-fly");
		}
		[super setValue:value forKeyPath:keyPath];
		
		return;
	}
	
	if ([keyPath hasPrefix:@"experimenter"]) {
		if (![self valueForKey:@"experimenter"]) {
			KSExtensibleManagedObject *experimenter = [[[KSExtensibleManagedObject alloc] initWithEntity:[NSEntityDescription entityForName:@"Experimenter"
																												   inManagedObjectContext:[self managedObjectContext]]
																		insertIntoManagedObjectContext:[self managedObjectContext]] autorelease];
			[super setValue:experimenter forKeyPath:@"experimenter"];
			NSLog(@"Created experimenter on-the-fly");
		}
		[super setValue:value forKeyPath:keyPath];
		
		return;
	}
	
	
	
	if ([keyPath hasPrefix:@"customMetadata"]) {

		NSString *kp = [keyPath stringByReplacingOccurrencesOfString:@"customMetadata." withString:@""];
		
		// Bugfix 26/09: Didn't remove custom metadata values...
		
		//if (value) {
		[self willChangeValueForKey:keyPath];
		[self setCustomMetadata:value forKey:kp];
		[self didChangeValueForKey:keyPath];
		//}
		
	} else {
		
		
		//NSLog(@"Changing value for keyPath: %@", keyPath);
		[super setValue:value forKeyPath:keyPath];
		
	}
	
	
}


- (void)setValue:(id)value forKey:(NSString *)key
{
	[super setValue:value forKey:key];

	if ([key isEqualToString:@"imagePath"] || [key isEqualToString:@"editedImagePath"])
		[[MGImageCache sharedCache] updateThumbnailForImage:self];
	
	
	if ([key isEqualToString:@"calibration"]) {
		[(ScaleBar *)[self valueForKey:@"scaleBar"] recalculateLength];
        
        for (ROI *roi in [self valueForKey:@"rois"])
            [roi updateMeasurements];
	}
	
}

- (id)valueForKeyPath:(NSString *)keyPath
{
	if ([keyPath hasPrefix:@"customMetadata"]) {
		
		NSString *kp = [keyPath stringByReplacingOccurrencesOfString:@"customMetadata." withString:@""];
		
		NSDictionary *customMetadata = [self customMetadataDict];
		
		return [customMetadata objectForKey:kp]; 
		
		
		
	} else {
		
		return [super valueForKeyPath:keyPath];
		
	}
}


- (void)prepareForDeletion;
{	
	[[MGImageCache sharedCache] removeThumbnailForImage:self];

	[super prepareForDeletion];
	
	// These objects are really removed from the ManagedObjectContext
	
	// these are covered by the delete rules
	//[self removeAllFilters];
	//[self removeAllROIs];
	[self removeScaleBar];
	[self removeExperimenter];
	[self removeExperiment];
	[self removeInstrument];
	[self removeAllStackImages];
	[self removeAllLightTableImages];
	
	// For the keywords, the relationship is simply set to nil (as we still need those keywords for other images)
	[self removeAllKeywords];
	
	// Remove the original image file and the edited image file
	if (![[self valueForKey:@"hasDuplicate"] boolValue]) {
		// Don't delete the original file if we have duplicates!
		[[NSFileManager defaultManager] removeItemAtPath:[self originalImagePathExpanded:YES] error:nil];
		[[NSFileManager defaultManager] removeItemAtPath:[self editedImagePathExpanded:YES] error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:[self tmpEditPathExpanded:YES] error:nil];

	}
	
}



- (void)removeExperimenter;
{
	id experimenter = [self valueForKey:@"experimenter"];
	if (experimenter) {
		if ([[experimenter valueForKey:@"images"] count] == 1)
			[[self managedObjectContext] deleteObject:experimenter];
		
		[self setValue:nil forKey:@"experimenter"];
	}
}


- (void)removeExperiment;
{
	id experiment = [self valueForKey:@"experiment"];
	if (experiment) {
		if ([[experiment valueForKey:@"images"] count] == 1)
			[[self managedObjectContext] deleteObject:experiment];
		
		[self setValue:nil forKey:@"experiment"];
	}
}

- (void)removeInstrument;
{	
	id instrument = [self valueForKey:@"instrument"];
	if (instrument) {
		if ([[instrument valueForKey:@"images"] count] == 1)
			[[self managedObjectContext] deleteObject:instrument];
		
		//[self setValue:nil forKey:@"instrument"];
	}
}


- (void)removeAllStackImages;
{	
	// Get rid of all related stackImages
	id stackImage = [self valueForKey:@"stackImage"];
	
	if (stackImage)
		[[self managedObjectContext] deleteObject:stackImage];
}


- (void)removeAllLightTableImages;
{	
	// Get rid of all related lightTableImages
	NSMutableSet *lightTableImages = [self mutableSetValueForKey:@"lightTableImages"];
	for (id lightTableImage in lightTableImages) {
		[[self managedObjectContext] deleteObject:lightTableImage];
	}
	[lightTableImages removeAllObjects];
}




# pragma mark -
# pragma mark IKImageBrowserItem


- (NSString *)imageRepresentationType
{
	if ([self hasFilters])
		return IKImageBrowserNSImageRepresentationType;
	else
		return IKImageBrowserPathRepresentationType;
}

- (id)imageRepresentation
{
    if ([self hasFilters])
		return (id)[self filteredImage];
	else
		return [self activeImagePathExpanded:YES];
}

- (NSString *)imageUID
{
    return [[[self objectID]URIRepresentation]absoluteString];
}

- (NSString *)imageTitle;
{
	return [self valueForKey:@"name"];
}

- (NSString *)imageSubtitle
{
	return [NSString stringWithFormat:@"%@ %@ %@", [self ratingString], [self scaleBarString], [self keywordString]];
	
}


- (NSUInteger)imageVersion;
{
	return [self browserVersion];
}

// Marks the image as updated, so that the update is picked up by the image browser
- (void)increaseBrowserVersion;
{
	[[MGImageCache sharedCache] removeCachedCIImagesForImage:self];
	[self setBrowserVersion:[self browserVersion]+1];	
}




# pragma mark -
# pragma mark Subtitle strings

- (NSString *)ratingString;
{
	if ([[self valueForKey:@"rating"] intValue]	> 0) {
		return [NSString stringWithFormat:@"%@%C", [self valueForKey:@"rating"], (unsigned short)0x2605];
	}
	else {
		return @"";
	}
}


- (NSString *)scaleBarString;
{
	if ([self valueForKey:@"scaleBar"]) {
		return @"-";
	}
	else {
		return @"";
	}
}


- (NSString *)keywordString;
{
	if ([[self valueForKey:@"keywords"] count] > 0) {
		return [NSString stringWithFormat:@"%C", (unsigned short)0x270E];
	}
	else {
		return @"";
	}
}



# pragma mark -
# pragma mark Scale bar

- (void)addScaleBarWithProperties:(NSDictionary *)properties;
{
	[self removeScaleBar];
	if ([self isCalibrated]) {
		id scaleBar = [NSEntityDescription insertNewObjectForEntityForName:@"ScaleBar" inManagedObjectContext:[self managedObjectContext]];
		[self setValue:scaleBar forKey:@"scaleBar"];
		[scaleBar setupWithProperties:properties];
	}
	[self increaseBrowserVersion];
}


- (void)removeScaleBar;
{
	id scaleBar = [self valueForKey:@"scaleBar"];
	if (scaleBar) {
		[[self managedObjectContext] deleteObject:scaleBar];
		[self setValue:nil forKey:@"scaleBar"];
	}
}



# pragma mark -
# pragma mark Albums and stacks

// Allows us to search image based on the albums they are in
- (NSString *)listOfAlbums;
{
	NSMutableString *result = [NSMutableString stringWithCapacity:60];
	NSSet *albums = [self primitiveValueForKey:@"albums"];
	
	for (id album in albums) {
		NSString *albumName = [album primitiveValueForKey:@"name"];
		[result appendString:albumName];
		[result appendString:@"\r"];
	}
	return [[result copy] autorelease];
	
}


- (BOOL)isPartOfStack;
{
	if ([self valueForKey:@"stackImage"])
		return YES;

	return NO;
}


- (Image *)stack;
{
	return [self valueForKeyPath:@"stackImage.stack"];	
}



# pragma mark -
# pragma mark Keywords

// Retrieves all keywords for this image. We need this when searching (using the search field)
- (NSString *)allKeywords;
{
	NSMutableString *allKeywordsString = [NSMutableString stringWithCapacity:100];
	
	[self willAccessValueForKey:@"allKeywords"];	
	NSSet *allKeywords = [self mutableSetValueForKey:@"keywords"];
	for (id keyword in allKeywords) {
		NSString *entireAncestryForKeyword = [keyword entireAncestry];
		[allKeywordsString appendString:entireAncestryForKeyword];
	}
	[self didAccessValueForKey:@"allKeywords"];	
	return [[allKeywordsString copy] autorelease];
}


// Returns an array that contains every keyword only once
- (NSArray *)uniqueKeywordsArray;
{
	NSMutableArray *allKeywordsArray = [NSMutableArray arrayWithCapacity:5];
	NSMutableDictionary *allKeywordsDict = [NSMutableDictionary dictionaryWithCapacity:5];
	NSSet *allKeywords = [self mutableSetValueForKey:@"keywords"];
	for (id keyword in allKeywords) {
		[allKeywordsArray addObjectsFromArray:[keyword entireAncestryArray]];
	}
	
	for (NSString *keywordString in allKeywordsArray) {
		[allKeywordsDict setObject:keywordString forKey:keywordString];
	}
	return [allKeywordsDict allKeys];
}


// Returns a string in which every keyword is listed only once, separated by a comma
- (NSString *)listOfUniqueKeywords;
{
	NSString *listOfUniqueKeywords = [[self uniqueKeywordsArray] componentsJoinedByString:@", "];
	return listOfUniqueKeywords;
}


// Adds an array of keywords to the image
- (void)addKeywords:(NSArray *)keywords;
{
	NSMutableSet *existingKeywords = [self mutableSetValueForKey:@"keywords"];
	
	// Add the dragged keywords and set the new set
	for (Keyword *keyword in keywords) {
		[existingKeywords addObject:keyword];
	}
	[self setValue:existingKeywords forKey:@"keywords"];
	[self increaseBrowserVersion];
}



- (void)copyKeywordsFromImage:(Image *)image;
{
	if (image == self)
		return;
	
	NSMutableSet *keywordsToCopy = [image mutableSetValueForKey:@"keywords"];
	NSMutableSet *existingKeywords = [self mutableSetValueForKey:@"keywords"];
	[existingKeywords unionSet:keywordsToCopy];
}


- (void)removeAllKeywords;
{
	// Get rid of all keywords
	NSMutableSet *keywordsForImage = [self mutableSetValueForKey:@"keywords"];
	[keywordsForImage removeAllObjects];	
}



# pragma mark -
# pragma mark Comment
- (BOOL)hasComment;
{
	if ([(NSString *)[self valueForKey:@"comment"] length] > 0) {
		return YES;
	}
	return NO;
}

# pragma mark -
# pragma mark Calibration

- (void)copyCalibrationFromImage:(Image *)image;
{
	if (image == self)
		return;
	
	id calibration = [image valueForKey:@"calibration"];
	
	// EDIT 11/02/09: calibration copy should not perform a deep copy.
	[self setValue:[calibration clone] forKey:@"calibration"];
}


- (void)removeCalibration
{
	id calibration = [self valueForKey:@"calibration"];
	if (calibration) {
		if ([[calibration valueForKey:@"images"] count] == 1)
			[[self managedObjectContext] deleteObject:calibration];
		else {
			[self setValue:nil forKey:@"calibration"];
		}

	}
	
}



# pragma mark -
# pragma mark Metadata


// Metadata For Montage
- (NSDictionary *)metadata;
{
	// Create a mutable dictionary that will hold all metadata
	NSMutableDictionary *metadata = (NSMutableDictionary *)[NSMutableDictionary dictionaryWithCapacity:30];
	
	// Add all built-in attributes
	NSArray *builtInProperties = [NSArray arrayWithObjects:
								  @"name",
								  @"comment",
								  @"instrument.voltage",
								  @"instrument.workingDistance", 
								  @"instrument.magnification", 
								  @"instrument.spotSize", 
								  @"instrument.instrumentName",
								  @"creationDate", 
								  @"importDate",
								  @"modificationDate",
								  @"suggestedScaleBarLength",
								  nil];
	
	for (NSString *property in builtInProperties) {
		
		NSString *value = [self valueForKeyPath:property];
		value = [[FormatterController sharedFormatterController] formattedStringForValue:value ofKeyPath:property];
		
		if (value) {
			[metadata setObject:value forKey:property];
		}
	}
	
	// Add all custom metadata
	
	// Return it
	return [[metadata copy] autorelease];
}



- (NSDictionary *)spotlightMetadata;
{
	// Create a mutable dictionary that will hold all metadata
	NSMutableDictionary *metadata = (NSMutableDictionary *)[NSMutableDictionary dictionaryWithCapacity:30];
	
	// Add all built-in attributes
	NSArray *builtInProperties = [NSArray arrayWithObjects:
								  @"name",
								  @"comment",
								  @"imagePath",
								  @"instrument.voltage", 
								  @"instrument.workingDistance",
								  @"instrument.magnification", 
								  @"instrument.spotSize", 
								  @"instrument.immersion", 
								  @"instrument.instrumentName",
								  @"instrument.numericAperture", 
								  @"instrument.objectiveName",
								  @"experiment.summary",
								  @"experimenter.name", 
								  @"allKeywords",
								  @"allROIComments",
								  @"channelMetadata",
								  nil];
	
	for (NSString *property in builtInProperties) {
		NSString *value = [[self valueForKeyPath:property]description];
		if (value) {
			[metadata setObject:value forKey:property];
		}
	}
	
	// Add the image URI to the metadata
	[metadata setObject:[[[self objectID] URIRepresentation] description] forKey:@"uri"];
	
	// Return it
	return [[metadata copy] autorelease];
}



// If this image, or one of its related objects have been changed, we need to return YES
- (BOOL)hasBeenChanged;
{
	// First check ourselves and our to-one relationships
	if ([self isInserted] || [self isUpdated])
		return YES;	

    if (![self isKindOfClass:[Stack class]])
         if ([[self valueForKey:@"experimenter"] isUpdated] || [[self valueForKey:@"experiment"]isUpdated] || [[self valueForKey:@"instrument"] isUpdated])
             return YES;


	// Then check our to-many relationships
	for (id channel in [self valueForKeyPath:@"instrument.channels"]) {
		if ([channel isUpdated]) {
			return YES;	
		}
	}
	for (id channel in [self valueForKey:@"rois"]) {
		if ([channel isUpdated]) {
			return YES;	
		}
	}
	return NO;
}




# pragma mark -
# pragma mark Path tools


- (NSString *)originalImagePathExpanded:(BOOL)expanded;
{
    NSString *path = [self valueForKey:@"imagePath"];
    
    if (expanded)
        path = [self expandOriginalDataPath:path];
    
    return path;
}
    
- (NSString *)editedImagePathExpanded:(BOOL)expanded;
{
    NSString *path = [self valueForKey:@"editedImagePath"];
    
    if (expanded)
        path = [self expandEditedDataPath:path];
    
    return path;  
}

- (NSString *)activeImagePathExpanded:(BOOL)expanded;
{
    if ([self valueForKey:@"editedImagePath"])
        return [self editedImagePathExpanded:expanded];
    else
        return [self originalImagePathExpanded:expanded];
}


- (NSString *)expandOriginalDataPath:(NSString *)path;
{
    if (!path)
        return nil;
    
    return [[[MGLibraryControllerInstance library] imageDataPath] stringByAppendingPathComponent:path];
}

- (NSString *)expandEditedDataPath:(NSString *)path;
{
    if (!path)
        return nil;
    
    return [[[MGLibraryControllerInstance library] editedDataPath] stringByAppendingPathComponent:path];
}




- (NSString *)applyOriginalImagePathWithFileName:(NSString *)name extension:(NSString *)ext;
{
	NSString *currentDateString = [[NSCalendarDate calendarDate] descriptionWithCalendarFormat:@"%d-%m-%Y"];
    
    NSString *basePath = [[MGLibraryControllerInstance library] imageDataPath];
    
    NSString *relativePath = [[currentDateString stringByAppendingPathComponent:name] stringByAppendingPathExtension:ext];
    
    NSString *uniqueRelativePath = [relativePath uniquePathByUsingBasePath:basePath];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:[basePath stringByAppendingPathComponent:currentDateString] withIntermediateDirectories:YES attributes:nil error:nil];
    
    [self setValue:uniqueRelativePath forKey:@"imagePath"];
    
    return uniqueRelativePath;
}

# pragma mark -
# pragma mark Info


// Returns the image's pixel size
- (NSSize)pixelSize;
{
	if (pixelSize.width == 0 && pixelSize.height == 0)
		pixelSize = MGGetDimensionsForImage([self activeImagePathExpanded:YES]);

	return pixelSize;
}

- (NSString *)pixelSizeDescription
{
	NSSize size = [self pixelSize];
	return [NSString stringWithFormat:@"%ix%i", (int)(size.width), (int)(size.height)];
}


- (NSString *)colorSpaceName;
{
	if (!colorSpaceName)
		colorSpaceName = [[self imageRep] colorSpaceName];	

	return colorSpaceName;
}



# pragma mark -
# pragma mark Bitmaps

- (void)updateThumbnail
{
    [[MGImageCache sharedCache] updateThumbnailForImage:self];  
}

#pragma mark -


- (NSImage *)image;
{	
	return [[[NSImage alloc] initWithContentsOfFile:[self activeImagePathExpanded:YES]] autorelease];
}


- (CIImage *)ciImage;
{
	if (![[NSFileManager defaultManager] fileExistsAtPath:[self activeImagePathExpanded:YES]])
		return nil;
	
	CIImage *ciImage = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[self activeImagePathExpanded:YES]]
											   options:[NSDictionary dictionaryWithObject:(id)CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB) forKey:kCIImageColorSpace]];
	if (ciImage) {
		return ciImage;
	}
	else {
		return [CIImage emptyImage];	
	}
}


- (NSData *)imageData;
{
	return [[self image] TIFFRepresentation];
}


- (CGImageRef)imageRef;
{
	NSString *imgPath = [self activeImagePathExpanded:YES];
	NSString *fullImgPath = [imgPath stringByExpandingTildeInPath];
	NSURL *imgURL = [NSURL fileURLWithPath:fullImgPath];
	
	CFURLRef url = (CFURLRef)imgURL;
	CGImageSourceRef imgSrc = CGImageSourceCreateWithURL(url, nil);
	CGImageRef img = CGImageSourceCreateImageAtIndex(imgSrc, 0, NULL);

	if (imgSrc) CFRelease(imgSrc);
	[(id)img autorelease];
	return img;
	
}

- (NSBitmapImageRep *)imageRep;
{
	NSImage *image = [self image];
	if ([[image representations] count] == 0) return nil;
	return [[image representations] objectAtIndex:0];
}




#pragma mark -

- (NSImage *)filteredImage
{
	// shortcut this if possible, otherwise loading becomes very expensive
	if (![self hasFilters])
		return [self image];
	
	CIImage *image = [self filteredCIImage];
	
	if (!image)
		return nil;
	
	// Convert it to an NSImage for now, because NSImageView cannot handle CIImage
	NSImage *nsImage = [[[NSImage alloc] init] autorelease];
	[nsImage addRepresentation:[NSCIImageRep imageRepWithCIImage:image]];
	
	return nsImage;
}


- (CIImage *)filterImage:(CIImage *)original
{
	if (![self hasFilters]) return original;

	if (!original)
		return nil;
	
	
	NSMutableArray *filters = [NSMutableArray arrayWithCapacity:5];
	int ciFilterRank = 0;
	
	for (CIFilter *filter in [self ciFilters]) {
		int numberOfAppliedFilters = [filters count];				// The numberOfAppliedFilters only counts enabled (= applied) filters
		ciFilterRank ++;											// The ciFilterRank counts all CIFilters based on all CDFilters for this image (enabled and disabled)
		
		// If it's the first filter, the inputImage is the original image
		CIImage *inputImage;
		if (numberOfAppliedFilters == 0) {
			inputImage = original;
		} 
		else {
			inputImage = [[filters objectAtIndex:(numberOfAppliedFilters-1)] valueForKey:@"outputImage"];  
		}
		
		// Bind the inputImage to the outputImage of the previous filter
		[filter setValue:inputImage forKey:@"inputImage"];
		
		// Add the CIFilter to the filters array
		[filters addObject:filter];
	}
	
	// Get the outputImage of the last filter
	return [[filters lastObject] valueForKey:@"outputImage"];
	
}


- (CIImage *)filteredCIImage;
{
	// uses a cache!
	
	CIImage *filteredImage = [[MGImageCache sharedCache] retrieveCIImageForImage:self];
	
	if (filteredImage)
		return filteredImage;
	
	//NSLog(@"Image %@: reload CIImage", [self name]);
	
	filteredImage = [self filterImage:[self ciImage]];
	
	[[MGImageCache sharedCache] cacheCIImage:filteredImage forImage:self];

	return filteredImage;
}



// Returns an NSData object based on the filtereImage
- (NSData *)filteredImageData;
{
	return [[self filteredImage] TIFFRepresentation];
}

- (CGImageRef)filteredImageRef;
{
    if ([[self valueForKey:@"filters"]count] == 0) {
		return [self imageRef];
	}
	
	//return [[MGCIContextManager sharedContextManager] imageRefFromCIImage:[self filteredCIImage]];
	
	CIImage *filteredCIImage = [self filteredCIImage];
	CGRect extent = [filteredCIImage extent];
	NSInteger w = (int)extent.size.width;
	NSInteger h = (int)extent.size.height;
	
	void *data = calloc(sizeof(unsigned char), 4 * w * h);
	
	CGContextRef ctx = CGBitmapContextCreate(data, w, h, 8, 4 * w,CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaNoneSkipLast);
	
	CIContext *ciCtx = [CIContext contextWithCGContext:ctx options:nil];
	[ciCtx drawImage:filteredCIImage atPoint:CGPointZero fromRect:extent];
	
	CGImageRef result = CGBitmapContextCreateImage(ctx);
	
	CGContextRelease(ctx);
	free(data);
	[(id)result autorelease];
	
	return result;
}


- (NSBitmapImageRep *)filteredImageRep;
{
	if ([[self valueForKey:@"filters"]count] == 0) {
		return [self imageRep];
	}
	
	CIImage *filteredCIImage = [self filteredCIImage];
	
	NSInteger w = [filteredCIImage extent].size.width;
	NSInteger h = [filteredCIImage extent].size.height;
		
	CIContext *ciCtx = [[MGCIContextManager sharedContextManager] contextWithExtent:[filteredCIImage extent]];

	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																	 pixelsWide:w
																	 pixelsHigh:h
																  bitsPerSample:8
																samplesPerPixel:4
																	   hasAlpha:YES
																	   isPlanar:NO
																 colorSpaceName:NSDeviceRGBColorSpace
																   bitmapFormat:NSAlphaFirstBitmapFormat
																	bytesPerRow:4 * w
																   bitsPerPixel:32] autorelease];
	
	
	unsigned char *bitmapData = [rep bitmapData];
	
	[ciCtx render:filteredCIImage
		 toBitmap:bitmapData
		 rowBytes:w * 4
		   bounds:CGRectMake(0, 0, w, h) 
		   format:kCIFormatARGB8 colorSpace:NULL];
	
	return rep;
}



- (NSBitmapImageRep *)filteredImageRepWithScaleBar;
{
	ScaleBar *scaleBar = [self valueForKey:@"scaleBar"];	
	
	if (!scaleBar)
		return [self filteredImageRep];
	
	NSBitmapImageRep *rep = [self filteredImageRep];
    
    if (!rep) return nil;
	
	if ([rep hasAlpha]) {
		NSGraphicsContext *ctx = [NSGraphicsContext graphicsContextWithBitmapImageRep:rep];
		NSGraphicsContext *oldCtx = [NSGraphicsContext currentContext];
		
		[NSGraphicsContext setCurrentContext:ctx];
		[scaleBar draw];
		[NSGraphicsContext setCurrentContext:oldCtx];
		
		return rep;
	}
	
	else {
		
		CGContextRef ctx = CGBitmapContextCreate(NULL, [rep pixelsWide], [rep pixelsHigh], 8, 4 * [rep pixelsWide], CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedFirst);
		CGContextDrawImage(ctx, CGContextGetClipBoundingBox(ctx), [rep CGImage]);
		
		NSGraphicsContext *nsctx = [NSGraphicsContext graphicsContextWithGraphicsPort:ctx flipped:NO];
		
		NSGraphicsContext *oldCtx = [NSGraphicsContext currentContext];
		
		[NSGraphicsContext setCurrentContext:nsctx];
		
		[scaleBar draw];
		[NSGraphicsContext setCurrentContext:oldCtx];
		
		CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
		
		NSBitmapImageRep *copy = [[[NSBitmapImageRep alloc] initWithCGImage:cgimg] autorelease];
		
		CGImageRelease(cgimg);
		
		return copy;
	}
	
	

	
}





#pragma mark -

- (NSImage *)thumbnail;
{
	return [[MGImageCache sharedCache] thumbnailForImage:self];
}


- (CIImage *)filteredThumbnail;
{
	return [[MGImageCache sharedCache] filteredThumbnailForImage:self];
}



# pragma mark -
# pragma mark Get filtered image file with or without a scale bar, for the mini browser

- (NSString *)temporaryPathToFilteredImage;
{
	// Check for a ~/Library/Caches/Macnification folder
	NSFileManager *fm = [NSFileManager defaultManager];
	if (![fm fileExistsAtPath:[@"~/Library/Caches/Macnification" stringByExpandingTildeInPath]]) {
		[fm createDirectoryAtPath:[@"~/Library/Caches/Macnification" stringByExpandingTildeInPath] withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	// Write the filteredImage to disk
	NSString *tempPath = [[NSString stringWithFormat:@"~/Library/Caches/Macnification/%@.tiff", 
						   [[[[self objectID] URIRepresentation] relativePath] lastPathComponent]] stringByExpandingTildeInPath];

	[[self filteredImageData] writeToFile:tempPath atomically:NO];
	return tempPath;
}



- (NSString *)temporaryPathToFilteredImageWithScaleBar;
{
	// Check for a ~/Library/Caches/Macnification folder	
	NSFileManager *fm = [NSFileManager defaultManager];
	if (![fm fileExistsAtPath:[@"~/Library/Caches/Macnification" stringByExpandingTildeInPath]]) {
		[fm createDirectoryAtPath:[@"~/Library/Caches/Macnification" stringByExpandingTildeInPath] withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	// Write the filteredImage to disk
	NSString *tempPath = [[NSString stringWithFormat:@"~/Library/Caches/Macnification/%@.tiff",
						   [[[[self objectID] URIRepresentation] relativePath] lastPathComponent]] stringByExpandingTildeInPath];
	
	[[[self filteredImageRepWithScaleBar] TIFFRepresentation] writeToFile:tempPath atomically:NO];
	return tempPath;
}




# pragma mark -
# pragma mark Export to a folder


// Export ourselves to a folder and add our IPTC tags
- (NSString *)exportToFolderAtPath:(NSString *)folderPath withOptions:(IKSaveOptions *)options;
{
	NSDictionary *fileTypesAndExtensions = [NSDictionary dictionaryWithObjectsAndKeys:
											@"jpg", kUTTypeJPEG,
											@"jpg", kUTTypeJPEG2000,
											@"gif", kUTTypeGIF,
											@"bmp", kUTTypeBMP,
											@"exr", @"com.ilm.openexr-image",
											@"pdf", kUTTypePDF,
											@"psd", @"com.adobe.photoshop-​image",
											@"pic", kUTTypePICT,
											@"png", kUTTypePNG,
											@"sgi", @"com.sgi.sgi-image",
											@"tga", @"com.truevision.tga-image",
											@"tif", kUTTypeTIFF,
											nil];
											
	NSString *extension = [fileTypesAndExtensions objectForKey:[options imageUTType]];
	if (!extension) extension = @"tif";
	
	
	// Write the filtered image to the specified folder on disk
	NSString *finalPath = [[[folderPath stringByAppendingPathComponent:[self valueForKey:@"name"]] stringByAppendingPathExtension:extension] uniquePath];
	
	// Write the NSBitmapImageRep to disk
	NSBitmapImageRep *imageRepToWrite = [self filteredImageRepWithScaleBar];
	
	NSString *imageType = [options imageUTType];
	if (!imageType) imageType = @"public.jpeg";
	
	CGImageRef image = [imageRepToWrite CGImage];
	
	NSURL *url = [NSURL fileURLWithPath:finalPath];
	CGImageDestinationRef dest = CGImageDestinationCreateWithURL((CFURLRef)url, (CFStringRef)imageType, 1, NULL);
	CGImageDestinationAddImage(dest, image, (CFDictionaryRef)[options imageProperties]);
	CGImageDestinationFinalize(dest);
	if (dest) CFRelease(dest);
	
	if ([[options imageUTType] isEqualToString:@"public.tiff"]) {
		
		//NSLog(@"saving tiff metadata");
		// Write the IPTC data to the same path
		IPTCController *c = [[IPTCController alloc] init];
		[c setString:[self valueForKeyPath:@"experimenter.name"] forTag:@"Credit" atPath:finalPath];
		[c setString:[NSString stringWithFormat:@"%c %@ %@", 169, [[self valueForKey:@"creationDate"]descriptionWithCalendarFormat:@"%Y" timeZone:nil locale:nil], 
					  [self valueForKeyPath:@"experimenter.name"]] 
			  forTag:@"CopyrightNotice" atPath:finalPath];
		[c setString:[[self valueForKey:@"creationDate"]descriptionWithCalendarFormat:@"%Y-%m-%d" timeZone:nil locale:nil] forTag:@"DateCreated" atPath:finalPath];
		[c setString:[self valueForKey:@"comment"] forTag:@"Caption/Abstract" atPath:finalPath];
		[c setString:[self valueForKeyPath:@"instrument.instrumentName"] forTag:@"Source" atPath:finalPath];
		[c setIPTCKeywords:[self uniqueKeywordsArray] forImageAtPath:finalPath];
		[c release];
		
	}
	return finalPath;
}




# pragma mark -
# pragma mark External editing and analysis

- (NSString *)pathForEditingApplication;
{	
    // generate data first!
    NSData *data = [self filteredImageData];

    // then, generate edit path (otherwise we'll try to use the edit path as the source path for the data)
    [self generateEditPath];

    [data writeToFile:[self editedImagePathExpanded:YES] atomically:YES];
    	
	// Remove all filters
	[self removeAllFilters];
	
	// Get rid of any existing pathForImageEditor
	[[NSFileManager defaultManager] removeItemAtPath:[self tmpEditPathExpanded:YES] error:nil];
	
	// Copy the pathForEditedImage to the pathForImageEditor
	[[NSFileManager defaultManager] copyItemAtPath:[self editedImagePathExpanded:YES] toPath:[self tmpEditPathExpanded:YES] error:nil];
	
	
	return [self tmpEditPathExpanded:YES];
}



- (void)updateExternallyEditedImage;
{
	NSFileManager *fm = [NSFileManager defaultManager];
    
	if (![fm fileExistsAtPath:[self tmpEditPathExpanded:YES]])
        return;
    
    [fm removeItemAtPath:[self editedImagePathExpanded:YES] error:nil];
    [fm copyItemAtPath:[self tmpEditPathExpanded:YES] toPath:[self editedImagePathExpanded:YES] error:nil];
    
    [[MGImageCache sharedCache] updateThumbnailForImage:self];
    
    
	
}

- (void)generateEditPath
{    
    if ([self editedImagePathExpanded:NO])
        return;
 
    // Generate and apply edit filename

    NSString *uri = [[[[self objectID] URIRepresentation] relativePath] lastPathComponent];
    
	NSString *relativePath = [[NSString stringWithFormat:@"%@.tiff", uri] uniquePathByUsingBasePath:[[MGLibraryControllerInstance library] editedDataPath]];
	
	[self setValue:relativePath forKey:@"editedImagePath"];

}


- (NSString *)pathForAnalysisApplication;
{
    return [self activeImagePathExpanded:YES];
}

- (NSString *)tmpEditPathExpanded:(BOOL)expanded;
{
    NSString *path = [self editedImagePathExpanded:expanded];
    
    if (!path)
        return path;
    
    return [path stringByAppendingFilenameSuffix:@"~"];
    
}



# pragma mark -
# pragma mark Filters


// Returns an NSArray of CIFilters. They are created based on the Core Data Filter objects associated with this image.
- (NSArray *)ciFilters;
{
	[self updateDeprecatedFilters];
	
	
	NSArray *ciFilters;
	NSMutableArray *filtersArray = [NSMutableArray arrayWithCapacity:5];
	NSSet *filters = [self valueForKey:@"filters"];
	if ([filters count] == 0) {
		// No Core Data filters are present
		ciFilters = [NSArray array];
	}
	
	else {
		// Core Data filters are available
		// Create an Array from the NSSet of filter
		NSMutableArray *filterArray = [NSMutableArray arrayWithCapacity:5];
		for (id filter in filters) {
			[filterArray addObject:filter];
		}
		// Sort the array by rank
		NSSortDescriptor *sortD = [[[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES] autorelease];
		NSArray *sortedFilters = [filterArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortD]];
		
		// Read the Core Data filters
		for (id aFilter in sortedFilters) {
			// Get the attributes from the Core Data Filter
			NSString *filterName = [aFilter valueForKey:@"name"];
			
			NSDictionary *filterAttributes = [NSKeyedUnarchiver unarchiveObjectWithData:[aFilter valueForKey:@"dictionary"]];
			
			
			// Create a new CIFilter based on the name and attributes in the Core Data filter
			CIFilter *ciFilter = [self ciFilterFromString:filterName withAttributes:filterAttributes];
			
			// Add the CIFilter to our filtersArray
			[filtersArray addObject:ciFilter];
			
			
			
		}
		ciFilters = [[filtersArray copy] autorelease];
	}
	return ciFilters;
}


- (NSArray *)ciFiltersWithThreshold
{
	NSMutableArray *filters = [[[self ciFilters] mutableCopy] autorelease];
	
	//if ([filters count] > 0)
	//	[[filters objectAtIndex:0] setValue:nil forKey:@"inputImage"];
	
	if (!thresholdCompositeFilter) return filters;
		
	[filters addObject:thresholdCompositeFilter];
	
	return filters;
}


- (void)updateDeprecatedFilters
{
	
	NSMutableSet *allFilters = [self mutableSetValueForKey:@"filters"];
	for (id filter in allFilters) {
		
		// remove the threshold filter
		if ([[filter valueForKey:@"name"] isEqualTo:@"ThresholdFilter"]) {
			[allFilters removeObject:filter];
			[[self managedObjectContext] deleteObject:filter];
		}
		
		// rename the blur filter
		if ([[filter valueForKey:@"name"] isEqualTo:@"CIGaussianBlur"]) {
			[filter setValue:@"DODGaussianBlurFilter" forKey:@"name"];
			
		}
		
	}
}




- (void)setCIFilters:(NSArray *)newCiFilters;
{
	// Get all existing CDFilters for the selected image and remove them
	NSMutableSet *filtersForImage = [self mutableSetValueForKey:@"filters"];
	
	for (id filter in filtersForImage) {
		[[self managedObjectContext] deleteObject:filter];
	}
	
	[filtersForImage removeAllObjects];
	
	// Iterate all CIFilters in our local filtersArray and create a Core Data filter for it
	for (CIFilter *ciFilter in newCiFilters) {
		id cdFilter = [NSEntityDescription insertNewObjectForEntityForName:@"Filter" inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]];
		[cdFilter setValue:[[ciFilter attributes] valueForKey:kCIAttributeFilterName] forKey:@"name"];
		
		
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"CIExposureAdjust"]) {
			[cdFilter setValue:[NSNumber numberWithInt:1] forKey:@"rank"];
		}
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"CIColorControls"]) {
			[cdFilter setValue:[NSNumber numberWithInt:2] forKey:@"rank"];
		}
		
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"CISharpenLuminance"]) {
			[cdFilter setValue:[NSNumber numberWithInt:3] forKey:@"rank"];
		}
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"DODGaussianBlurFilter"]) {
			[cdFilter setValue:[NSNumber numberWithInt:4] forKey:@"rank"];
		}
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"WhiteBalanceCorrectionFilter"]) {
			[cdFilter setValue:[NSNumber numberWithInt:5] forKey:@"rank"];
		}
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"InvertFilter"]) {
			// Give the invert filter a higher rank
			[cdFilter setValue:[NSNumber numberWithInt:6] forKey:@"rank"];
		}

		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"LUTColormapFilter"]) {
			// Give the LUT filter a higher rank, so that it's always applied after the other filters (but before threshold)
			[cdFilter setValue:[NSNumber numberWithInt:9] forKey:@"rank"];
		}
		
		if ([[cdFilter valueForKey:@"name"] isEqualTo:@"ThresholdFilter"]) {
			// Give the threshold filter a higher rank, so that it's always applied after the other filters
			[cdFilter setValue:[NSNumber numberWithInt:10] forKey:@"rank"];
		}
		[cdFilter setValue:[NSKeyedArchiver archivedDataWithRootObject:[ciFilter dictionary]]  forKey:@"dictionary"];
		[filtersForImage addObject:cdFilter];
	}
	
	// Set the newly created Core Data filters for the selected image
	[self setValue:filtersForImage forKey:@"filters"];
	
	// Notify our observers that our filteredImageData has changed
	[self willChangeValueForKey:@"filteredImageData"];
	[self didChangeValueForKey:@"filteredImageData"];

}









// Get a CIFilter based on its name and attributes
- (CIFilter *)ciFilterFromString:(NSString *)filterName withAttributes:(NSDictionary *)filterAttributes;
{
	CIFilter *filter = [CIFilter filterWithName:filterName];
	[filter setDefaults];
	
	if (filterAttributes)
		[filter setDictionary:filterAttributes];
    
	return filter;
}


- (void)copyFiltersFromImage:(Image *)image;
{
	if (image == self)
		return;
	
	NSMutableSet *filters = [self mutableSetValueForKey:@"filters"];
	for (id filter in filters) {
		[[self managedObjectContext] deleteObject:filter];
	}
	
	NSMutableSet *filtersToCopy = [image mutableSetValueForKey:@"filters"];
	NSMutableSet *newFilters = [NSMutableSet setWithCapacity:20];
	for (id filter in filtersToCopy) {
		[newFilters addObject:[filter clone]];
	}
	[self setValue:newFilters forKey:@"filters"];
}



- (void)removeAllFilters;
{
	// Get rid of all filters
	NSMutableSet *filtersForImage = [self mutableSetValueForKey:@"filters"];
	for (id filter in filtersForImage) {
		[[self managedObjectContext] deleteObject:filter];
	}
	[filtersForImage removeAllObjects];	
	[self setValue:filtersForImage forKey:@"filters"];
	
	// Notify our observers that our filteredImageData has changed
	[self willChangeValueForKey:@"filteredImageData"];
	[self didChangeValueForKey:@"filteredImageData"];
}


- (BOOL)hasFilters;
{
	return [[self valueForKey:@"filters"] count] > 0;
}



- (void)setThresholdCompositeFilter:(CIFilter *)filter;
{	
	if (filter != thresholdCompositeFilter) {
		[thresholdCompositeFilter release];
		thresholdCompositeFilter = [filter retain];
	}
}




# pragma mark -
# pragma mark Create a new version from the original image / Duplicate an image


- (void)newVersionFromOriginal;
{
	Image *newImage = (Image *)[self clone];
	
	// Set the albums and calibration relationships to be identical
	[newImage setValue:[self valueForKey:@"albums"] forKey:@"albums"];
	[newImage setValue:[self valueForKey:@"calibration"] forKey:@"calibration"];
	
	// Remove the editedImagePath, so that we fall back on the original image
	[newImage setValue:nil forKey:@"editedImagePath"];
}



- (void)duplicate;
{
	[self setValue:[NSNumber numberWithBool:YES] forKey:@"hasDuplicate"];
	
	Image *newImage = (Image *)[self clone];
	
	// Set the albums and calibration relationships to be identical
	[newImage setValue:[self valueForKey:@"albums"] forKey:@"albums"];
	[newImage setValue:[self valueForKey:@"calibration"] forKey:@"calibration"];
	
	// Set the filters to be identical
	NSSet *existingFilters = [self valueForKey:@"filters"];
	NSMutableSet *newFilters = [NSMutableSet setWithCapacity:5];
	for (id filter in existingFilters) {
		[newFilters addObject:[filter clone]];
	}
	[newImage setValue:newFilters forKey:@"filters"];
}



- (void)revertToOriginal;
{
	// Get rid of all filters
	[self removeAllFilters];
	
	// Remove the editedImagePath, so that we fall back on the original image
	[self setValue:nil forKey:@"editedImagePath"];
	
	// TODO: need to remove image at edited path!
}



# pragma mark -
# pragma mark Split/Merge Channels


- (NSArray *)splitChannels
{
	// returns an array of Image objects that represent (in order) the R, G and B components.
	NSMutableArray *channelImages = [NSMutableArray array];
	
	// create the separate channels
	NSBitmapImageRep *original = [self imageRep];
	
	if ([original samplesPerPixel] == 1)
		return nil;
	
	NSArray *channelReps = [original splitChannels];
	
	NSString *originalPath = [self activeImagePathExpanded:YES];
	
	int i = 0;

	for (NSBitmapImageRep *rep in channelReps) {
		
		// copy the new image
		NSString *extension = [originalPath pathExtension];
		
		// Create the image entity
		Image *image = (Image *)[self clone];
        
        NSString *name = [[self valueForKey:@"name"] stringByAppendingFormat:@"_ch%i", (int)i];
		[image setValue:name forKey:@"name"];
		
        [image applyOriginalImagePathWithFileName:name extension:extension];
        
        
        [[rep TIFFRepresentation] writeToFile:[image originalImagePathExpanded:YES] atomically:YES];

        [image updateThumbnail];

		
		// Set the calibration relationships to be identical
		[image setValue:[self valueForKey:@"calibration"] forKey:@"calibration"];
		[image setValue:[self valueForKey:@"instrument"] forKey:@"instrument"];
		[image setValue:[self valueForKey:@"experiment"] forKey:@"experiment"];
		
		id libraryCollection = [[MGLibraryControllerInstance library] libraryGroup];
		[image setValue:[NSSet setWithObject:libraryCollection] forKey:@"albums"];

		// Set the image name

		
		[image setValue:[NSDate date] forKey:@"creationDate"];
		[image setValue:[NSDate date] forKey:@"importDate"];
		
		[image increaseBrowserVersion];
		
		[channelImages addObject:image];
				
		i++;
	}
	
	return channelImages;
}


# pragma mark -
# pragma mark ROIs


// Convert Pixel length to real length
- (float)realLengthForPixelLength:(float)pixelLength;
{
	float horizontalPixelsForUnit = [[self valueForKeyPath:@"calibration.horizontalPixelsForUnit"]floatValue];
	return (pixelLength / horizontalPixelsForUnit);
}



// Convert Pixel area to real area
- (float)realAreaForPixelArea:(float)pixelArea;
{
	float horizontalPixelsForUnit = [[self valueForKeyPath:@"calibration.horizontalPixelsForUnit"]floatValue];
	return (pixelArea / pow(horizontalPixelsForUnit, 2));
}



// Add a new LinROI, RectangleROI or CircleROI
- (id)addROI:(NSString *)roiType startPoint:(NSPoint)startPoint endPoint:(NSPoint)endPoint;
{
	// Create a new ROI
	id roi = [NSEntityDescription insertNewObjectForEntityForName:roiType inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]];
	
	// Link it to this image and set the ROI rank
	NSMutableSet *rois = [self mutableSetValueForKey:@"rois"];
	[roi setValue:[NSNumber numberWithInt:[rois count]] forKey:@"rank"];
	[rois addObject:roi];
	
	// Set the start and end point
	[roi setStartPoint:startPoint endPoint:endPoint];
	
	return roi;
}



- (id)addROI:(NSString *)roiType withPoints:(NSArray *)pointsArray;
{
	NSArray *result =  [self addROIsWithPoints:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjectsAndKeys:roiType, @"type", pointsArray, @"points", nil]]];
	
	return ([result count] > 0) ? [result objectAtIndex:0] : nil;
}


- (NSArray *)addROIsWithPoints:(NSArray *)roiDictionaries
{
	NSMutableArray *roiObjects = [NSMutableArray array];
	
	NSMutableSet *existingRois = [self mutableSetValueForKey:@"rois"];

	int currentRank = [existingRois count];
	
	for (NSDictionary *roiDict in roiDictionaries) {
		
		id roi = [NSEntityDescription insertNewObjectForEntityForName:[roiDict objectForKey:@"type"] inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]];
		[roi setValue:[NSNumber numberWithInt:currentRank] forKey:@"rank"];
				
		if ([[roiDict objectForKey:@"type"] isEqualTo:@"PolygonROI"])
			[(PolygonROI *)roi setPolygonPoints:[roiDict objectForKey:@"points"]];
		else
			[(CurvedLineROI *)roi setPolygonPoints:[roiDict objectForKey:@"points"]];
		
		
		[roiObjects addObject:roi];
	}
	
	[[self mutableSetValueForKey:@"rois"] addObjectsFromArray:roiObjects];
    
    // now that we've added the ROIs to the image, call updateMeasurements (it's dependent on the image calibration!)
    for (ROI *roi in roiObjects)
        [roi updateMeasurements];
	
	
	return roiObjects;
}


- (void)copyROIsFromImage:(Image *)image removeExisting:(BOOL)remove;
{
	if (image == self)
		return;
	
	NSMutableSet *roisToCopy = [image valueForKey:@"rois"];
	NSMutableSet *newRois = [NSMutableSet set];
	
	for (ROI *roi in roisToCopy)
		[newRois addObject:[roi clone]];
	
	if (remove)
		[self removeAllROIs];

	[[self mutableSetValueForKey:@"rois"] unionSet:newRois];

	
	for (ROI *roi in [self valueForKey:@"rois"]) {
		[roi calculateColorContents];
		[roi calculateLength];
		[roi calculateArea];
		[roi calculatePerimeter];
	}
}



- (void)removeAllROIs;
{	
	// This method uses the setPrimitiveValue technique for setting relationships as explained in the Core Data docs
	// See the comments on setPrimitiveValue for more information.
	NSMutableSet *roisForImage = [[self primitiveValueForKey:@"rois"]mutableCopy];
	for (id roi in roisForImage) {
		[[self managedObjectContext] deleteObject:roi];
	}
	if (roisForImage != nil) {
		[self willChangeValueForKey:@"rois"];
		[roisForImage removeAllObjects];
		[self setPrimitiveValue:roisForImage forKey:@"rois"];
		[self didChangeValueForKey:@"rois"];
	}
}



// Adds a calibration to the image, with the pixels per unit, the unit, and the point that was clicked (in case of 1CC)
- (void)calibratePoint:(NSPoint)point withPixels:(double)pixels perUnit:(NSString *)unit calibrationName:(NSString *)name;
{
	id calibration;
	if ([self isCalibrated]) {
		calibration = [self valueForKey:@"calibration"];
	}
	else {
		calibration = [NSEntityDescription insertNewObjectForEntityForName:@"Calibration" inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]];
	}
	
	// UPDATE 28/04/2010
	// first, determine if we'll be able to use the given unit
	// the amount (unitsPerImageWidth/6.0), which is the target scale bar length, should be well over 1.0 for the given unit, otherwise we need to scale unit and quantity
	double imagePixelWidth = [self pixelSize].width;
	double unitsPerImageWidth = imagePixelWidth/pixels;
	double scaleFactor;
	NSString *newUnit = [self newUnitToAccomodateLength:unitsPerImageWidth/6.0 unit:unit withScaleFactor:&scaleFactor];
	
	
	[calibration setValue:[NSNumber numberWithDouble:pixels / scaleFactor] forKey:@"horizontalPixelsForUnit"];
	[calibration setValue:newUnit forKey:@"unit"];
	[calibration setValue:[NSNumber numberWithFloat:point.x] forKey:@"x"];
	[calibration setValue:[NSNumber numberWithFloat:point.x] forKey:@"y"];
	[calibration setValue:name forKey:@"name"];
	[self setValue:calibration forKey:@"calibration"];
	
	// Update all ROIs to recalculate their measurements
	NSMutableSet *allROIs = [self mutableSetValueForKey:@"rois"];
	for (ROI *roi in allROIs) {
		[roi updateMeasurements];
	}
	
	// recalculate the scale bar if there is one
	[(ScaleBar *)[self valueForKey:@"scaleBar"] recalculateLength];
}

- (NSString *)newUnitToAccomodateLength:(double)oldLength unit:(NSString *)oldUnit withScaleFactor:(double *)scaleFactor;
{
	// this dictionary contains the available units and the *relative* orders of magnitude of their quantities
	// in other words, (x * 10^oom_unit1) [unit1] == (x * 10^oom_unit2) [unit2]
	NSDictionary *units = [NSDictionary dictionaryWithObjectsAndKeys:
						  	[NSNumber numberWithInt:10], @"Å",
						    [NSNumber numberWithInt:9], @"nm",
						    [NSNumber numberWithInt:6], @"µm",
						    [NSNumber numberWithInt:3], @"mm",
						    [NSNumber numberWithInt:2], @"cm",
						    [NSNumber numberWithInt:0], @"m",
						   nil];
	
	
	double proposedNewLength = oldLength;
	int numMagnitudeIncreases = 0;
	while (proposedNewLength < 1.0) {
		proposedNewLength *= 10;
		numMagnitudeIncreases++;
	}
	
	
	// get the relative o.o.m of the current unit
	int currentOOM = [[units objectForKey:oldUnit] intValue];
	int targetOOM = currentOOM + numMagnitudeIncreases;
	
	// find the unit belonging to the target OOM
	int smallestOOM_equalOrGreaterThanTarget = 1e3;
	NSString *smallestPossibleUnit = nil;
	
	for (NSString *unit in [units allKeys]) {
		
		int oom = [[units objectForKey:unit] intValue];
		
		if (oom >= targetOOM && oom < smallestOOM_equalOrGreaterThanTarget) {
			smallestOOM_equalOrGreaterThanTarget = oom;
			smallestPossibleUnit = unit;
		}
	}
	
	// if the target OOM is just too large, there will be no "smallest possible unit".
	// in that case, fail (the scale bar will be invalid) but this is an extreme edge case.
	if (!smallestPossibleUnit) {
		*scaleFactor = 1;
		//NSLog(@"in: %lf %@ --- out: %lf %@", oldLength, oldUnit, *scaleFactor * oldLength, oldUnit);
		return oldUnit;
	}
	
	
	// get the effective OOM that the unit represents w.r.t the current unit
	int effectiveOOM = [[units objectForKey:smallestPossibleUnit] intValue] - currentOOM;
	
	*scaleFactor = powf(10, effectiveOOM);
	
	//NSLog(@"in: %lf %@ --- out: %lf %@", oldLength, oldUnit, *scaleFactor * oldLength, smallestPossibleUnit);

	
	return smallestPossibleUnit;
	
}


// Do we have a calibration?
- (BOOL)isCalibrated;
{
	if ([self valueForKey:@"calibration"]) {
		return YES;
	}
	return NO;
}


- (int)roiCount;
{
	return [[self mutableSetValueForKey:@"rois"] count];	
}



// Export to tab delimited text
- (NSString *)tabText;
{
	NSMutableString *result = [NSMutableString stringWithCapacity:60];
	NSMutableSet *rois = [self mutableSetValueForKey:@"rois"];
	NSArray *roiArray = [rois allObjects];	
	NSSortDescriptor *sortD = [[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortD];
	NSArray *sortedRois = [roiArray sortedArrayUsingDescriptors:sortDescriptors];
	[sortD release];
	
	if ([rois count] > 0) {
		[result appendString:[NSString stringWithFormat:@"%@", [self valueForKey:@"name"]]];
		[result appendString:@"\r"];
		
		for (id roi in sortedRois) {
			[result appendString:[roi tabText]];
			[result appendString:@"\r"];
		}
		return [[result copy] autorelease];
	}
	else {
		return @"";	
	}
}



// Return all ROI comments
- (NSString *)allROIComments;
{
	NSMutableString *result = [NSMutableString stringWithCapacity:60];
	NSMutableSet *rois = [self mutableSetValueForKey:@"rois"];
	NSArray *roiArray = [rois allObjects];	
	
	for (id roi in roiArray) {
		NSString *comment = ([roi primitiveValueForKey:@"comment"]) ? [roi primitiveValueForKey:@"comment"]: @"";
		[result appendString:comment];
		[result appendString:@"\r"];
	}
	//NSLog(@"setting ROI comments: %@", [[result copy] autorelease]);
	
	return [[result copy] autorelease];
}





# pragma mark -
# pragma mark Channel information

- (NSString *)channelMetadata;
{
	NSMutableString *result = [NSMutableString stringWithCapacity:60];
	NSMutableSet *channels = [self mutableSetValueForKeyPath:@"instrument.channels"];
	NSArray *channelArray = [channels allObjects];	
	
	for (id channel in channelArray) {
		NSString *comment = [channel description];
		[result appendString:comment];
		[result appendString:@"\r"];
	}
	return [[result copy] autorelease];
}




# pragma mark -
# pragma mark Custom Metadata


// On-demand get accessor for the customMetadata
- (NSDictionary *)customMetadataDict;
{
    [self willAccessValueForKey:@"customMetadata"];
    NSDictionary *customMetadata = [self primitiveValueForKey:@"customMetadata"];
    [self didAccessValueForKey:@"customMetadata"];
    if (customMetadata == nil) {
        NSData *customMetadataData = [self valueForKey:@"customMetadataData"];
        if (customMetadataData != nil) {
            customMetadata = [NSKeyedUnarchiver unarchiveObjectWithData:customMetadataData];
            [self setPrimitiveValue:customMetadata forKey:@"customMetadata"];
        }
    }
    return customMetadata;
}



// Immediate-Update Set Accessor
- (void)setCustomMetadataDict:(NSDictionary *)aDict;
{
    [self willChangeValueForKey:@"customMetadata"];
    [self setPrimitiveValue:aDict forKey:@"customMetadata"];
    [self didChangeValueForKey:@"customMetadata"];
    [self setValue:[NSKeyedArchiver archivedDataWithRootObject:aDict] forKey:@"customMetadataData"];
}



// Add a Custom metadata key or change the value for an existing key
- (void)setCustomMetadata:(NSString *)metadata forKey:(NSString *)key;
{
	NSLog(@"adding custom metadata for key %@ (self == %@)", key, [self description]);
	
	NSMutableDictionary *mutableCustomMetadata = [NSMutableDictionary dictionary];
	[mutableCustomMetadata addEntriesFromDictionary:[self customMetadataDict]];
	if (!metadata)
		[mutableCustomMetadata removeObjectForKey:key];
	else
		[mutableCustomMetadata setObject:metadata forKey:key];
	
	[self setCustomMetadataDict:mutableCustomMetadata];	
}


# pragma mark -
# pragma mark Accessors

- (NSUInteger)browserVersion 
{
    return browserVersion;
}

- (void)setBrowserVersion:(NSUInteger)value 
{
	browserVersion = value;
}

#pragma mark -
#pragma mark Stack/Image related

- (int)numberOfContainedImages;
{
	return 1;
}

- (Image *)displayImage;
{
	return self;
}

@end
