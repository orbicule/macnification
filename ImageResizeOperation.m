//
//  ImageResizeOperation.m
//  StackTable
//
//  Created by Dennis Lorson on 03/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "ImageResizeOperation.h"


@implementation ImageResizeOperation


- (CGImageRef)imageWithOriginal:(CGImageRef)original size:(CGSize)newSize
{
	// to be released by caller
	NSInteger width = newSize.width;
	NSInteger height = newSize.height;
	size_t bytesPerRow = 4 * width;
	
	void *memData = malloc(bytesPerRow * height);
	
	CGContextRef context = CGBitmapContextCreate(memData, width, height, 8, bytesPerRow, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), original);
	
	CGImageRef result = CGBitmapContextCreateImage(context);
	
	CGContextRelease(context);
	free(memData);
	
	return result;
}



@end
