//
//  MGHUDButton.m
//  Filament
//
//  Created by Dennis Lorson on 25/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDButtonCell.h"


@implementation MGHUDButtonCell

- (void)drawImage:(NSImage*)image withFrame:(NSRect)frame inView:(NSView*)controlView
{
	BOOL imageAndTextButton = ([self image] && ![[self title] isEqualToString:@" "]);

	NSSize size = [image size];
	//float addX = imageAndTextButton ? 4 : 2;
	float y = NSMaxY(frame) - (frame.size.height-size.height)/2.0 + (imageAndTextButton ? 3 : 2);
	float x = frame.origin.x + (imageAndTextButton ? 1 : 0);
	
	[image compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:[self isEnabled] ? 1.0 : 0.35];
	
	return;
}




- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{

	CGFloat alpha = [self isEnabled] ? 1.0 : 0.35;
	
	// check for the cell attrs in order to find out when to highlight (toggle or momentary change?)
	NSString *state = [self isHighlighted] ? @"highlighted" : ([self state] == NSOnState  && !([self highlightsBy] & NSChangeBackgroundCell) ? @"highlighted" : @"normal");
	BOOL rectButton = ([self image] != NULL /*&& [[self title] isEqualToString:@" "]*/);
	NSString *imageString = rectButton ? @"_image" : @"";
	NSImage *leftImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_button%@_left_%@", imageString, state]];
	NSImage *fillImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_button%@_middle_%@", imageString, state]];
	NSImage *rightImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_button%@_right_%@", imageString, state]];
	
	NSSize size = [leftImage size];
	float addX = size.width / 2.0;
	float y = NSMaxY(cellFrame) - (cellFrame.size.height-size.height)/2.0 - 1;
	y = floor(y);
	float x = cellFrame.origin.x+addX;
	x = floor(x);
	float fillX = x + size.width;
	float fillWidth = cellFrame.size.width - size.width - addX;
	
	[leftImage compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:alpha];
	
	size = [rightImage size];
	addX = size.width / 2.0;
	x = NSMaxX(cellFrame) - size.width - addX;
	x = floor(x);
	fillWidth -= size.width+addX;
	
	[rightImage compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:alpha];
	
	[fillImage setScalesWhenResized:YES];
	[fillImage setSize:NSMakeSize(fillWidth, [fillImage size].height)];
	[fillImage compositeToPoint:NSMakePoint(fillX, y) operation:NSCompositeSourceOver fraction:alpha];
	
	if (![self image]) {
		cellFrame.origin.y -= 2;
		cellFrame.size.height += 2;
		cellFrame.origin.x += 1;
		
	} else {
		
		cellFrame.origin.y -= 3;
		cellFrame.size.height += 2;
		cellFrame.origin.x -= 1.5;
	}

	
	[self drawInteriorWithFrame:cellFrame inView:controlView];
}



@end
