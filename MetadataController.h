//
//  MetadataController.h
//  Filament
//
//  Created by Dennis Lorson on 28/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@class MGMDView;


@interface MetadataController : NSObject {

	// "super" variables: in the nib where this object is a part of
	
	IBOutlet NSView *metadataViewContainer;
	IBOutlet NSArrayController *imageController;
	
	// sub variables: in the nib that this object is the owner of
	
	IBOutlet MGMDView *metadataView;
	IBOutlet NSView *m_view;

	
}

- (void)awakeWithSuperview:(NSView *)superview;
- (void)setupMetadataView;
- (void)bindToKeyPath:(NSString *)keyPath ofObject:(id)obj;

- (MGMDView *)metadataView;

@end
