//
//  NSMigrationManager_LeopardWorkaround.h
//  Filament
//
//  Created by Dennis Lorson on 20/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// See file:///Developer/Documentation/DocSets/com.apple.adc.documentation.AppleSnowLeopard.CoreReference.docset/Contents/Resources/Documents/releasenotes/Cocoa/MigrationCrashBuild106Run105/index.html

// To suppress warning: defaults write com.apple.CoreData.mapc SuppressUnsupportedRelationshipExpressionWarning -bool YES

@interface NSMigrationManager (LeopardWorkaround)

+ (void)addRelationshipMigrationMethodIfMissing;

@end
