//
//  ExperimentArrayController.m
//  Filament
//
//  Created by Dennis Lorson on 09/11/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "ExperimentArrayController.h"


@implementation ExperimentArrayController


- (NSArray *)arrangeObjects:(NSArray *)objects
{
	if ([[self sortDescriptors] count] == 0)
		return [super arrangeObjects:objects];

	NSSortDescriptor *sd = [[self sortDescriptors] objectAtIndex:0];
	
	if (![[sd key] isEqual:@"id"])
		return [super arrangeObjects:objects];
	
	
	// sort the entries according to ID, but respect integer order
	NSMutableArray *integerEntries = [NSMutableArray array];
	NSMutableArray *otherEntries = [NSMutableArray array];
	
	for (id object in objects) {
		
		NSString *idString = [object valueForKey:@"id"];
		
		if ([[NSString stringWithFormat:@"%i", [idString intValue]] isEqual:idString])
			[integerEntries addObject:object];
		else
			[otherEntries addObject:object];
		
	}
	

	
	// sort the entries separately
	[integerEntries sortUsingComparator:^(id obj1, id obj2) 
	{
		int int1 = [[obj1 valueForKey:@"id"] intValue];
		int int2 = [[obj2 valueForKey:@"id"] intValue];
		
		if (int1 > int2)
			return (NSComparisonResult)NSOrderedDescending;
		
		if (int1 < int2)
			return (NSComparisonResult)NSOrderedAscending;

		return (NSComparisonResult)NSOrderedSame;
		
	}];
	
	
	[otherEntries sortUsingDescriptors:[self sortDescriptors]];
	
	return [integerEntries arrayByAddingObjectsFromArray:otherEntries];
	
}






@end
