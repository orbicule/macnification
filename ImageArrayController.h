//
//  ImageArrayController.h
//  Filament
//
//  
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Quartz/Quartz.h>
#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>
#import "PSImageView.h"
#import "BlobController.h"
#import "PSImageBrowserView.h"
#import "MGCustomMetadataController.h"

@class SourceListTreeController;
@class FullScreenController;
@class MetadataController;
@class ChannelMergeController;

@interface ImageArrayController : NSArrayController <MGCustomMetadataObserver>
{
	
	IBOutlet NSTableView *tableView;
	IBOutlet SourceListTreeController *sourceListTreeController;
	IBOutlet NSArrayController *roiArrayController;
	IBOutlet NSTextView *commentTextView;
	IBOutlet NSImageView *commentImageView;
	NSArray *droppedItems;
	
	// batch comments
	IBOutlet NSWindow *batchCommentWindow;
	IBOutlet NSTextField *batchCommentTextField;
	IBOutlet NSPopUpButton *batchCommentPopupButton;
	
	// browser menu
	IBOutlet NSMenu *browserMenu;
	IBOutlet NSMenuItem *stackMenuItem;
	IBOutlet NSMenuItem *unstackMenuItem;
	IBOutlet NSMenuItem *stackSeparatorItem;
	

	// Image browser
	IBOutlet PSImageBrowserView *imageBrowser;
	IBOutlet NSSlider *imageSizeSlider;
	IBOutlet NSView *imageBrowserContainerView;
	
	// sorting
	IBOutlet NSPopUpButton *sortKeyPopUpButton;
	IBOutlet NSSegmentedControl *sortOrderControl;
	
	IBOutlet NSSearchField *browserSearchField;
	
	// Metadata
	IBOutlet MetadataController *mdController;
	
	// Fullscreen
	IBOutlet FullScreenController *fullScreenController;
	IBOutlet id fullScreenImageBrowser;
	IBOutlet PSImageView *psImageView;
		
	// Blob detection
	IBOutlet BlobController *blobController;
	
	// Predicates
	IBOutlet NSWindow *predicateRemovalPanel;
	
	Image *imageToCopyROIsFrom;
	Image *imageToCopyFiltersFrom;
	Image *imageToCopyKeywordsFrom;
	Image *imageToCopyCalibrationFrom;
		

	// controller vars
	NSMutableDictionary *manualObservers;
	
	IKSaveOptions *saveOptions;
	
	ChannelMergeController *channelMergeController;

	BOOL showsStacks;
}


// Non-destructive editing
- (IBAction)newVersionFromMaster:(id)sender;
- (IBAction)duplicate:(id)sender;
- (IBAction)revertToOriginal:(id)sender;

- (IBAction)mergeChannels:(id)sender;
- (IBAction)splitChannels:(id)sender;


- (IBAction)addCommentForSelectedImage:(id)sender;
- (IBAction)zoomSliderDidChange:(id)sender;

- (IBAction)openTDF:(id)sender;

- (IBAction)emailImages:(id)sender;

- (IBAction)removePredicate:(id)sender;



// ImageBrowser refreshing
- (void)refreshImageBrowser;
- (void)refreshAndReloadImageBrowser;
- (void)setBrowserAnimates:(BOOL)flag;


// Manual "KVO" through notifications
- (void)observeManualModelPropertyChange:(NSNotification *)notification;
- (void)addManualObserver:(id)observer forKeyPath:(NSString *)keyPath;
- (void)removeManualObserver:(id)observer;

// Housekeeping
// This is a dangerous method that might lead to data loss!
// The caller of this method is responsible for presenting the user with an NSAlert!
- (void)removeImagesFromLibrary:(NSArray *)images;


// External editing and analyzing
- (IBAction)editInExternalApplication:(id)sender;
- (void)updateImagesModifiedByExternalEditor;
- (IBAction)analyzeInExternalApplication:(id)sender;


// Exporting
- (IBAction)exportImages:(id)sender;
- (IBAction)exportToTDF:(id)sender;
- (IBAction)exportImageMetadata:(id)sender;
- (NSString *)TDFStringForImages:(NSArray *)images;
- (NSString *)TDFStringForImagesSortedPerROI:(NSArray *)images;
- (NSArray *)exportImagesToFolder:(NSString *)folderPath;


// Montage
- (IBAction)createMontage:(id)sender;

// Stack
- (IBAction)stackImages:(id)sender;
- (IBAction)unstackImages:(id)sender;
- (BOOL)selectionContainsStack;
- (BOOL)numberOfStacksInSelection;
- (NSArray *)selectedObjectsWithStacksExpanded;
- (NSArray *)arrangedObjectsWithStacksExpanded;


// Copying image properties
- (IBAction)removeAllROIs:(id)sender;
- (IBAction)copyROIs:(id)sender;
- (IBAction)pasteROIs:(id)sender;
- (IBAction)applyROIsToEntireStack:(id)sender;
- (void)refreshROIArrayController;


// Keywords
- (void)applyKeywordsToSelectedImages:(NSArray *)keywords;
- (IBAction)copyKeywords:(id)sender;
- (IBAction)pasteKeywords:(id)sender;
- (IBAction)removeAllKeywords:(id)sender;

// calibration
- (IBAction)copyCalibration:(id)sender;
- (IBAction)pasteCalibration:(id)sender;
- (IBAction)removeCalibration:(id)sender;


// Ratings
- (IBAction)setRating:(id)sender;

// comments
- (IBAction)addCommentForSelectedImage:(id)sender;
- (IBAction)appendBatchCommentToSelectedImages:(id)sender;
- (IBAction)endAppendBatchCommentSheet:(id)sender;
- (void)appendCommentToSelectedImages:(NSString *)comment withSeparator:(NSString *)separator;


// Filters
- (void)filtersDidChange;
- (void)updateThumbnailForImage:(Image *)image;
- (void)updateThumbnailForImages:(NSArray *)images;
- (IBAction)copyFilters:(id)sender;
- (IBAction)pasteFilters:(id)sender;
- (IBAction)removeAllFilters:(id)sender;
- (IBAction)applyFiltersToEntireStack:(id)sender;

// full screen
- (void)enterFullScreen;
- (void)setShowsStacks:(BOOL)flag;

// Scale bar
- (BOOL)exportsWithScaleBar;
- (void)setExportsWithScaleBar:(BOOL)value;
- (IBAction)setScaleBarExport:(id)sender;
- (IBAction)removeScaleBar:(id)sender;
- (void)startScaleBarAnimation;


- (void)updateImagesModifiedByExternalEditor;


// Searching
- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;
- (IBAction)changeSearchString:(id)sender;
- (void)changeSearchCategory:(id)sender;

// Sorting
- (IBAction)changeSortKey:(id)sender;
- (IBAction)changeSortOrder:(id)sender;

- (void)setSortKey:(NSString *)key;
- (void)setSortOrder:(BOOL)descendingOrAscending;
- (void)setSortControlsVisible:(BOOL)flag;
- (BOOL)hasNonDefaultSortOrderOrKey;

// Spotlight
- (void)selectSpotlightedImages:(NSArray *)images;



- (MetadataController *)metadataController;
- (PSImageView *)fullscreenView;

// Accessors
@property(retain, readwrite) Image *imageToCopyROIsFrom;
@property(retain, readwrite) Image *imageToCopyFiltersFrom;
@property(retain, readwrite) Image *imageToCopyKeywordsFrom;
@property(retain, readwrite) Image *imageToCopyCalibrationFrom;



@end
