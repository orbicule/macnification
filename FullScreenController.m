//
//  FullScreenController.m
//  Filament
//
//  Created by Peter Schols on 27/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "FullScreenController.h"

#import "MGAppDelegate.h"
#import "ImageKitExtensions.h"
#import "ImageArrayController.h"
#import "BlobController.h"
#import "AdjustmentsController.h"
#import "FullScreenImageArrayController.h"

#import "MAAttachedWindow.h"
#import "MTFullScreenWindow.h"
#import "RoundedView.h"
#import "PSImageView.h"
#import "MGLibraryController.h"


#define SEPARATE_SCREEN_PREFERENCE @"showDetailWindowOnSeparateScreen"

@interface FullScreenController ()

- (NSScreen *)mainWindowScreen;
- (NSScreen *)detailWindowScreen;

- (BOOL)hasMultipleScreensAvailable;
- (BOOL)wantsDetailWindowOnSeparateScreen;
- (BOOL)hasDetailOnSeparateScreen;

- (void)correctScreenForWindow:(NSWindow *)window;

@end


@implementation FullScreenController


# pragma mark -
# pragma mark Screen info

- (BOOL)hasMultipleScreensAvailable;
{
	return [[NSScreen screens] count] > 1;
}

- (BOOL)wantsDetailWindowOnSeparateScreen;
{
	// if not set yet: default value "YES"
	if (![[NSUserDefaults standardUserDefaults] objectForKey:SEPARATE_SCREEN_PREFERENCE])
		return YES;
	
	return [[NSUserDefaults standardUserDefaults] boolForKey:SEPARATE_SCREEN_PREFERENCE];
}

- (BOOL)hasDetailOnSeparateScreen
{
	return [self hasMultipleScreensAvailable] && [self wantsDetailWindowOnSeparateScreen];
}

- (NSScreen *)mainWindowScreen;
{
	return mainWindowScreen;
}

- (NSScreen *)detailWindowScreen;
{
	if (![self hasDetailOnSeparateScreen])
		return [self mainWindowScreen];
		
	for (NSScreen *screen in [NSScreen screens])
		if (screen != [self mainWindowScreen])
			return screen;
	
	return nil;
}

- (void)updateScreens;
{
	// track the position of the main window (because otherwise, if it's not visible we won't be able to tell)
	[mainWindowScreen release];
	mainWindowScreen = [[mainWindow screen] retain];
}

# pragma mark -
# pragma mark Going fullscreen 

- (void)awakeFromNib;
{
	// don't remove: prevents strange behavior on quit
	[toolstrip performSelector:@selector(orderOut:) withObject:self afterDelay:0.2];
	
	[imageBrowser setAnimates:NO];
	[imageBrowser setAllowsEmptySelection:NO];
	[imageBrowser setZoomValue:1.0];
  

	[self updateScreens];
}


- (IBAction)enterFullScreen:(id)sender;
{
	if (isInFullScreen)
		return;
	
	isInFullScreen = YES;
	
	// If the imageView is not visible, it does not observe the image selection in ImageArrayController.
	// We need to change that now and invoke a change so that the image is properly displayed once we appear on scene
	[imageView setShouldObserveSelectedImage:YES];
	[imageView observeValueForKeyPath:@"selection" ofObject:fullScreenImageArrayController change:nil context:NULL];
	[fullScreenImageArrayController addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial context:nil];
	
	// temp save the splitview position
	CGFloat splitViewPosition = NSMaxX([fsSplitView convertRect:[(NSView *)[[fsSplitView subviews] objectAtIndex:0] frame] fromView:[[fsSplitView subviews] objectAtIndex:0]]);
	
	// Go fullscreen
	fsWindow = [[MTFullScreenWindow alloc] initWithDelegate: self];
	[fsWindow setScreen:[self detailWindowScreen]];
	[fsWindow setContentView:[fullScreenWindow contentView]];
    

	// restore the splitview position, which has changed due to adding the splitview to the new window
	[fsSplitView setPosition:splitViewPosition ofDividerAtIndex:0];
	
	// Zoom to fit for first image
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	BOOL alwaysZoomToFit = [[userDefaults valueForKey:@"alwaysZoomToFit"]boolValue];
	if (alwaysZoomToFit) {
		[imageView zoomImageToFitWithAnimation:NO];
	}

	[imageView zoomOutWithAnimation:NO];
	[imageView zoomInWithAnimation:NO];
	[fsWindow enterFullScreen];
	
	[self correctScreenForWindow:toolstrip];
	[self correctScreenForWindow:[imageView navigatorWindow]];
	
	// Check if the ROIHUD is already open and store its state
	ROIHUDWasOpen = [ROIHUD isVisible];

	[self fadeInToolstrip];
	[imageView showNavigatorIfNeeded];
  
	
	if (![self hasDetailOnSeparateScreen]) {
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"DidEnterFullscreen" object:self];
		[mainWindow performSelector:@selector(orderOut:) withObject:nil afterDelay:.3];
		
	} else {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"DidEnterFullscreenSeparate" object:self];

	}
	
}





- (void)fadeInToolstrip;
{

	if (isInFullScreen) {
		[toolstrip setAlphaValue:0.0];
		[toolstrip orderFront:self];
		[[toolstrip animator] setAlphaValue:1.0];
		[[toolstrip contentView] display];
	}
}





- (IBAction)exitFullScreen:(id)sender;
{
	imageView.shouldObserveSelectedImage = NO;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"DidLeaveFullscreen" object:self];

	[(ImageArrayController *)imageArrayController setShowsStacks:YES];
	
	isInFullScreen = NO;
	

    
	if ([toolstrip isVisible]) {
		[(RoundedView *)[toolstrip contentView] closeAllTooltips];
		[toolstrip orderOut:self];
	}
	
	[mainWindow makeKeyAndOrderFront:self];

	// Close all FS windows
	if (!ROIHUDWasOpen) {
		[ROIHUD orderOut:self];
	}
	[imageView closeInfoPanel];
	[calibrationHUD close];
	[imageView closeNavigatorWindow:self];
	[adjustmentsController close];
	[blobController close];
	[imageView closeROICommentHUD:self];
  
	
	// Let our observers know that we are leaving full-screen mode
	NSNotification *willExitFullScreenModeNotification = [NSNotification notificationWithName:@"willExitFullScreenMode" object:self userInfo:NULL];
	[[NSNotificationCenter defaultCenter] postNotification:willExitFullScreenModeNotification];
	
	// Close our fullscreen window
	[fsWindow exitFullScreen];
	
	// The imageView no longer has to observe selections in ImageArrayController
	[imageView setShouldObserveSelectedImage:NO];
	
	[imageView setImage:nil];
	
	// Auto-save
	[MGLibraryControllerInstance save];
}






- (BOOL)isInFullScreen
{
	return isInFullScreen;
}



- (void)correctScreenForWindow:(NSWindow *)window
{
	NSScreen *originalScreen = [window screen];
	NSScreen *newScreen = [self detailWindowScreen];
	
	if (!newScreen)
		return;
	
	if (originalScreen == newScreen)
		return;
    

	
	NSRect originalScreenFrame = [originalScreen frame];
	NSRect newScreenFrame = [newScreen frame];
	
	NSRect frame = [window frame];
	
	NSPoint screenOriginOffsetOld = NSMakePoint(NSMidX(frame) - NSMinX(originalScreenFrame), NSMidY(frame) - NSMinY(originalScreenFrame));
	
	NSPoint screenOriginOffsetNew = NSMakePoint(screenOriginOffsetOld.x * NSWidth(newScreenFrame) / NSWidth(originalScreenFrame), screenOriginOffsetOld.y * NSHeight(newScreenFrame) / NSHeight(originalScreenFrame));
	
	NSPoint newFrameOrigin = NSMakePoint(NSMinX(newScreenFrame) + screenOriginOffsetNew.x - 0.5 * NSWidth(frame), NSMinY(newScreenFrame) + screenOriginOffsetNew.y - 0.5 * NSHeight(frame));
	
	[window setFrameOrigin:newFrameOrigin];
    

	
	
	
}


# pragma mark -
# pragma mark Splitview delegate

- (CGFloat)splitView:(NSSplitView *)sender constrainSplitPosition:(CGFloat)proposedPosition ofSubviewAt:(NSInteger)offset
{
	CGFloat screenWidth = [[fsWindow screen] frame].size.width;

	return MIN(MAX(100, proposedPosition), screenWidth / 2.0);
	
	return proposedPosition;
	
}




# pragma mark -
# pragma mark IBActions

- (IBAction)selectNext:(id)sender;
{
    
    ;

	[fullScreenImageArrayController selectNext:self];
	[imageBrowser scrollIndexToVisible:[[fullScreenImageArrayController selectionIndexes]firstIndex]];
}


- (IBAction)selectPrevious:(id)sender;
{
	[fullScreenImageArrayController selectPrevious:self];
	[imageBrowser scrollIndexToVisible:[[fullScreenImageArrayController selectionIndexes]firstIndex]];
}

- (IBAction)toggleROIs:(id)sender;
{
    
    ;

	// Adjust the text of the Hide ROIs toolbar button
	if ([[toggleROITextField stringValue] isEqualTo:@"Hide ROIs"]) {
		[toggleROITextField setStringValue:@"Show ROIs"];
	}
	else {
		[toggleROITextField setStringValue:@"Hide ROIs"];
	}
	
	[imageView toggleROIs:self];
}




# pragma mark -
# pragma mark Adjustments



- (IBAction)adjustSelectedImage:(id)sender;
{
	[self enterFullScreen:self];
	[self showAdjustmentsPanel:self];
}



- (IBAction)showAdjustmentsPanel:(id)sender;
{
	if ([[adjustmentsController window] isVisible]) {
		[adjustmentsController close];
		return;
	}
	
	if (!adjustmentsController) {
		adjustmentsController = [[AdjustmentsController alloc] init];
	}
	
	
	[self correctScreenForWindow:[adjustmentsController window]];
	
	//[[adjustmentsController window] setAlphaValue:0.0];
	[adjustmentsController showWindow:self];
	//[[[adjustmentsController window] animator] setAlphaValue:1.0];
}


- (IBAction)showBlobExtractionPanel:(id)sender;
{
	if (!blobController) {
		blobController = [[BlobController alloc] init];
	}
	
	if (![[blobController window] isVisible])
		[self correctScreenForWindow:[blobController window]];

		
	[blobController toggleBlobDetectionWindow:sender];
}


# pragma mark -
# pragma mark IKImageBrowserView datasource

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"selection"] && object == fullScreenImageArrayController) {
		[imageBrowser setSelectionIndexes:[fullScreenImageArrayController selectionIndexes] byExtendingSelection:NO];
		
		if ([[imageBrowser selectionIndexes] count])
			[imageBrowser scrollIndexToVisible:[[imageBrowser selectionIndexes] firstIndex]];
		
	} else {
		
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)aBrowser;
{
	return [[fullScreenImageArrayController arrangedObjects] count];
}


- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index;
{
	return [[fullScreenImageArrayController arrangedObjects] objectAtIndex:index];
}


- (void)imageBrowser:(IKImageBrowserView *)view removeItemsAtIndexes:(NSIndexSet *)indexes
{
	//[imageArrayController remove:self];
}




# pragma mark -
# pragma mark IKImageBrowserView delegate

- (void)imageBrowserSelectionDidChange:(IKImageBrowserView *)aBrowser;
{
	// if there's no image selected, the image selection must have originated from the IAC
	if ([[aBrowser selectionIndexes] count] == 0)
		return;
		
	// only do this if in full screen, otherwise the user can't have touched the FS browser selection and this invocation would only cost extra cycles.
	if (isInFullScreen)
		[(NSArrayController *)fullScreenImageArrayController setSelectionIndexes:[aBrowser selectionIndexes]];
	
	// Adjust the text of the Hide ROIs toolbar button
	[toggleROITextField setStringValue:@"Hide ROIs"];
}



# pragma mark -
# pragma mark Menu validation

- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
    SEL theAction = [anItem action];
	
    if (theAction == @selector(enterFullScreen:) || theAction == @selector(adjustSelectedImage:))    {
        if ([[fullScreenImageArrayController selectedObjects] count] > 0 && !isInFullScreen) {
            return YES;
        }
        return NO;
    } 
	return YES;
}



@end
