//
//  MGHUDScroller.m
//  Filament
//
//  Created by Dennis Lorson on 27/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDScroller.h"



static NSImage* _backVTImage = nil;
static NSImage* _backVMImage = nil;
static NSImage* _backVBUpImage = nil;
static NSImage* _backVBUpSelectedImage = nil;
static NSImage* _backVBDownImage = nil;
static NSImage* _backVBDownSelectedImage = nil;

static NSImage* _knobVTImage = nil;
static NSImage* _knobVMImage = nil;
static NSImage* _knobVBImage = nil;

static NSRect   _backVTRect = {{0, 0}, {0, 0}};
static NSRect   _backVMRect = {{0, 0}, {0, 0}};
static NSRect   _backVBUpRect = {{0, 0}, {0, 0}};
static NSRect   _backVBDownRect = {{0, 0}, {0, 0}};
static NSRect   _knobHLRect = {{0, 0}, {0, 0}};
static NSRect   _knobHMRect = {{0, 0}, {0, 0}};
static NSRect   _knobHRRect = {{0, 0}, {0, 0}};
static NSRect   _knobVTRect = {{0, 0}, {0, 0}};
static NSRect   _knobVMRect = {{0, 0}, {0, 0}};
static NSRect   _knobVBRect = {{0, 0}, {0, 0}};



static NSImage* _knobHLImage = nil;
static NSImage* _knobHMImage = nil;
static NSImage* _knobHRImage = nil;

enum {
    NSScrollerPartWhole = 0, 
    NSScrollerPartUpperExtra = 1, 
    NSScrollerPartKnob = 2, 
    NSScrollerPartLowerExtra = 3, 
    NSScrollerPartUpArrow = 4, 
    NSScrollerPartDownArrow = 5, 
    NSScrollerPartWithoutArrow = 6, 
};



@implementation MGHUDScroller



+ (CGFloat)scrollerWidth
{
	return 14.0f;
}

+ (CGFloat)scrollerWidthForControlSize:(NSControlSize)controlSize
{
	return 14.0f;
}



//--------------------------------------------------------------//
#pragma mark -- Initialize --
//--------------------------------------------------------------//

+ (void)load
{
    NSAutoreleasePool*  pool;
    pool = [[NSAutoreleasePool alloc] init];
    
	// private API to enforce the arrows at the bottom
	[[self class] _setArrowsConfig:3];
	
    // Get resources
    if (!_backVTImage) {
        NSBundle*   bundle;
        bundle = [NSBundle bundleForClass:[self class]];
		
        _backVTImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_top"]];
        _backVMImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_track"]];
        _backVBUpImage = [[NSImage alloc] initWithContentsOfFile:
						  [bundle pathForImageResource:@"hud_scroller_arrows_top"]];
        _backVBUpSelectedImage = [[NSImage alloc] initWithContentsOfFile:
								  [bundle pathForImageResource:@"hud_scroller_arrows_top_highlighted"]];
        _backVBDownImage = [[NSImage alloc] initWithContentsOfFile:
							[bundle pathForImageResource:@"hud_scroller_arrows_bottom"]];
        _backVBDownSelectedImage = [[NSImage alloc] initWithContentsOfFile:
									[bundle pathForImageResource:@"hud_scroller_arrows_bottom_highlighted"]];
        _knobHLImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_knob_top"]];
        _knobHMImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_knob_middle"]];
        _knobHRImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_knob_bottom"]];
        _knobVTImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_knob_top"]];
        _knobVMImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_knob_middle"]];
        _knobVBImage = [[NSImage alloc] initWithContentsOfFile:
						[bundle pathForImageResource:@"hud_scroller_knob_bottom"]];
        
        _backVTRect.size = [_backVTImage size];
        _backVMRect.size = [_backVMImage size];
        _backVBUpRect.size = [_backVBUpImage size];
        _backVBDownRect.size = [_backVBDownImage size];
        _knobHLRect.size = [_knobHLImage size];
        _knobHMRect.size = [_knobHMImage size];
        _knobHRRect.size = [_knobHRImage size];
        _knobVTRect.size = [_knobVTImage size];
        _knobVMRect.size = [_knobVMImage size];
        _knobVBRect.size = [_knobVBImage size];
    }
    
    [pool release];
}


//--------------------------------------------------------------//
#pragma mark -- Drawing --
//--------------------------------------------------------------//

- (void)drawArrow:(NSScrollerArrow)arrow highlightPart:(NSInteger)part
{
    // Get bounds
    NSRect  bounds;
    bounds = [self bounds];
    
    // Check flip
    BOOL    flipped;
    flipped = [self isFlipped];
	
    // Draw back
    NSRect  rect, imageRect;
    if (arrow == NSScrollerIncrementArrow) {
        // Down arrow
        NSImage*    image;
        if (part == 0) {
            image = _backVBDownSelectedImage;
        }
        else {
            image = _backVBDownImage;
        }
        
        rect.origin.x = 0;
        rect.origin.y = bounds.size.height - _backVBDownRect.size.height + 4;
        rect.size = _backVBDownRect.size;
		rect.size.height += 0;

        imageRect.origin = NSZeroPoint;
        imageRect.size = _backVBDownRect.size;
        if ([image isFlipped] != flipped) {
            [image setFlipped:flipped];
        }
        [image drawInRect:rect fromRect:imageRect operation:NSCompositeCopy fraction:1.0f];
        
          // Up arrow
        if (part == 1) {
            image = _backVBUpSelectedImage;
        }
        else {
            image = _backVBUpImage;
        }
        
        rect.origin.y -= _backVBUpRect.size.height;
        rect.size = _backVBUpRect.size;
        imageRect.origin = NSZeroPoint;
        imageRect.size = _backVBUpRect.size;
		//rect.size.width += 1;

        if ([image isFlipped] != flipped) {
            [image setFlipped:flipped];
        }
        [image drawInRect:rect fromRect:imageRect operation:NSCompositeCopy fraction:1.0f];
    }
    else if (arrow == NSScrollerDecrementArrow) {
        rect.origin.x = 0;
        rect.origin.y = -8;
        rect.size = [_backVTImage size];
        imageRect.origin = NSZeroPoint;
        imageRect.size = _backVTRect.size;
		//rect.size.width += 1;
		//rect.origin.x -= 1;
		
        if ([_backVTImage isFlipped] != flipped) {
            [_backVTImage setFlipped:flipped];
        }
        [_backVTImage drawInRect:rect fromRect:imageRect operation:NSCompositeCopy fraction:1.0f];
		
		[[NSColor colorWithCalibratedWhite:0.08 alpha:1.0] set];
		
		NSRectFill(NSMakeRect(0, 0, rect.size.width, 1));
    }
}

- (void)drawKnob
{
    // Check flip
    BOOL    flipped;
    flipped = [self isFlipped];
    
    NSRect  rect, imageRect;
    
    //
    // Draw knob
    //
    
    // Get knob rect
    rect = [self rectForPart:NSScrollerPartKnob];

    
    // Draw knob bottom
    imageRect.origin.x = rect.origin.x + 1;
    imageRect.origin.y = flipped ? rect.origin.y + rect.size.height - _knobVBRect.size.height - 4 : rect.origin.y + 4;
    imageRect.size = _knobVBRect.size;
	//imageRect.size.width += 1;

    if ([_knobVBImage isFlipped] != flipped) {
        [_knobVBImage setFlipped:flipped];
    }
    [_knobVBImage drawInRect:imageRect fromRect:_knobVBRect operation:NSCompositeSourceOver fraction:1.0f];
    
    // Draw knob middle
    imageRect.origin.x = rect.origin.x + 1;
    imageRect.origin.y = flipped ? 
	rect.origin.y + _knobVTRect.size.height + 2 : 
	rect.origin.y + rect.size.height - _knobVTRect.size.height;
    imageRect.size.width = _knobVMRect.size.width;
	//imageRect.size.width += 1;

    imageRect.size.height = rect.size.height - _knobVBRect.size.height - _knobVTRect.size.height - 6;
    if ([_knobVMImage isFlipped] != flipped) {
        [_knobVMImage setFlipped:flipped];
    }
    [_knobVMImage drawInRect:imageRect fromRect:_knobVMRect operation:NSCompositeSourceOver fraction:1.0f];
    
    // Draw knob top
    imageRect.origin.x = rect.origin.x + 1;
    imageRect.origin.y = flipped ? rect.origin.y + 2 : rect.origin.y + rect.size.height - _knobVTRect.size.height - 2;
    imageRect.size = _knobVTRect.size;
	//imageRect.size.width += 1;
    if ([_knobVTImage isFlipped] != flipped) {
        [_knobVTImage setFlipped:flipped];
    }
    [_knobVTImage drawInRect:imageRect fromRect:_knobVTRect operation:NSCompositeSourceOver fraction:1.0f];
}

- (void)drawKnobSlotInRect:(NSRect)rect highlight:(BOOL)highlight
{	
    // Get frame
    NSRect  frame;
    frame = [self frame];
    
    // Check flip
    BOOL    flipped;
    flipped = [self isFlipped];
    
    NSRect  imageRect;
    
    // Draw background image
    imageRect.origin = NSZeroPoint;
    imageRect.size = [_backVMImage size];
	
	NSRect destRect = rect;
	//destRect.origin.x += 1;
	//destRect.size.width -= 1;
    
    
    //[[NSColor blackColor] set];
    //NSRectFill(rect);
	
    [_backVMImage drawInRect:destRect fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0f];
}

- (BOOL)_hasSeparateArrows
{
	
	return NO;
}


@end
