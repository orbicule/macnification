//
//  MiniBrowserController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 12 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <ImageKit/ImageKit.h>

@class MGSourceOutlineView;
@class MiniSourceListTreeController;

@interface MiniBrowserController : NSWindowController 
{
	
		
	IBOutlet IKImageBrowserView *imageBrowser;
	IBOutlet MGSourceOutlineView *sourceList;
	IBOutlet NSSplitView *splitView;
	IBOutlet MiniSourceListTreeController *sourceListController;
	
	CGFloat sourceListWidth;
}


@end
