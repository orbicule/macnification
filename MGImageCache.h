//
//  MGImageCache.h
//  Filament
//
//  Created by Dennis Lorson on 24/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <openssl/sha.h>
#import <QuartzCore/QuartzCore.h>

@class Image;

typedef enum _MGImageCacheThumbnailOperationType {
	
	MGImageCacheOperationTypeGeneration,
	MGImageCacheOperationTypeRemoval
	
} MGImageCacheThumbnailOperationType;


@interface MGImageCacheThumbnailOp : NSOperation
{
	MGImageCacheThumbnailOperationType opType_;
	NSString *sourcePath_;
	NSString *destinationPath_;
}

@property (readwrite, retain) NSString *sourcePath;
@property (readwrite, retain) NSString *destinationPath;

@property (readwrite) MGImageCacheThumbnailOperationType opType;

@end




typedef enum _MGCachedImageSize {
	
	MGCachedImageSizeThumbnail,
	MGCachedImageSizeSmall,
	MGCachedImageSizeFull
	
} MGCachedImageSize;



@interface MGImageCache : NSObject 
{
	NSOperationQueue *thumbnailGenerationQueue_;
	
	NSMutableDictionary *activeThumbnailOperations_;	// sets of all ops belonging to one path
	
	NSMutableArray *ciImageCache_;
}

+ (MGImageCache *)sharedCache;


// thumbnails

- (void)performAppStartThumbnailGeneration;

- (void)updateThumbnailForImage:(Image *)image;
- (void)removeThumbnailForImage:(Image *)image;

- (NSImage *)thumbnailForImage:(Image *)image;
- (CIImage *)filteredThumbnailForImage:(Image *)image;


// CIImage (full-size) caching
// although the CIImages themselves are lightweight, not releasing them also preserves texture caches etc.

- (void)cacheCIImage:(CIImage *)filtered forImage:(Image *)image;
- (CIImage *)retrieveCIImageForImage:(Image *)image;
- (void)removeCachedCIImagesForImage:(Image *)image;

@end
