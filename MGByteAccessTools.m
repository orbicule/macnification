//
//  MGByteAccessTools.m
//  Filament
//
//  Created by Dennis Lorson on 11/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MGByteAccessTools.h"

char MGTIFFReadByte(FILE *file) 
{
	char byte;
	fread(&byte, sizeof(char), 1, file);
	return byte;
}

int MGTIFFReadInt(FILE *file, BOOL littleEndian)
{
	unsigned char bytes[4];
	
	fread(&bytes, sizeof(unsigned char), 4, file);
	
	if (littleEndian)
		return ((int)(bytes[3]) << 24) + ((int)(bytes[2]) << 16) + ((int)(bytes[1]) << 8) + ((int)(bytes[0]) << 0);
	else
		return ((int)(bytes[0]) << 24) + ((int)(bytes[1]) << 16) + ((int)(bytes[2]) << 8) + ((int)(bytes[3]) << 0);
	
}

short MGTIFFReadShort(FILE *file, BOOL littleEndian)
{
	unsigned char bytes[2];
	
	fread(&bytes, sizeof(unsigned char), 2, file);
	
	if (littleEndian)
		return ((int)(bytes[1]) << 8) + ((int)(bytes[0]) << 0);
	else
		return ((int)(bytes[0]) << 8) + ((int)(bytes[1]) << 0);
	
}

long long MGTIFFReadLongLong(FILE *file, BOOL littleEndian)
{
	if (littleEndian) {
		
		return ((long long)MGTIFFReadInt(file, littleEndian)&0xffffffffL) + ((long long)MGTIFFReadInt(file, littleEndian)<<32);

	}
	else
		return ((long long)MGTIFFReadInt(file, littleEndian)<<32) + ((long long)MGTIFFReadInt(file, littleEndian)&0xffffffffL);
	
	
}

double MGTIFFReadDouble(FILE *file, BOOL littleEndian)
{
	long long val = MGTIFFReadLongLong(file, littleEndian);
	double result = *(double *)&val;
	return result;
}

int MGTIFFReadValue(FILE *file, BOOL littleEndian, int fieldType, int count)
{
	int value = 0;
	int unused;
	
	if (fieldType == MG_SHORT && count == 1) {
		value = MGTIFFReadShort(file, littleEndian);
		unused = MGTIFFReadShort(file, littleEndian);
	} else {
		value = MGTIFFReadInt(file, littleEndian);
	}
	
	return value;
}


float MGTIFFReadFloat(FILE *file, BOOL littleEndian)
{
	// float conversion: just interpret the 4 int bytes as a float
	int anInteger = MGTIFFReadInt(file, littleEndian);
	float aFloat;
	
	char *bytesFloat = (char *)&aFloat;
	char *bytesInteger = (char *)&anInteger;
	
	bytesFloat[0] = bytesInteger[0];
	bytesFloat[1] = bytesInteger[1];
	bytesFloat[2] = bytesInteger[2];
	bytesFloat[3] = bytesInteger[3];
	
	return aFloat;
}




@implementation MGByteAccessTools

@end
