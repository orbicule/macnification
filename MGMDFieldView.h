//
//  MGMDFieldView.h
//  MetaDataView
//
//  Created by Dennis on 8/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MGMDChannelView;
@class MGMDExperimentView;
@class MGMDKeywordView;

#import "MGMDView.h"

															#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface MGMDFieldView : NSView <MGMDManualObserver, NSTextFieldDelegate, NSTokenFieldDelegate> 
															#else
@interface MGMDFieldView : NSView <MGMDManualObserver> 
															#endif
{
	///////
	// these vars only active if this field contains a channel view
	///////
	IBOutlet MGMDChannelView *channelView;		// only used if this field represents the channels.	
	NSArray *channelTopLevelObjects;
	
	IBOutlet MGMDExperimentView *experimentView;
	NSArray *experimentTopLevelObjects;
		
	IBOutlet MGMDKeywordView *keywordView;
	NSArray *keywordTopLevelObjects;
	
	NSTextField *fieldLabel;
	NSString *fieldIdentifier;
	NSView *raisedEditorView;
	NSView *fieldContents;
	
	BOOL isBeingDragged;
	BOOL isPlaceholder;
	BOOL editModeOn;
	BOOL raisedEditingActive;
	BOOL ignoreSubviewResize;			// to avoid a resizeSubviewsWithOldSize: callback at certain times when it would create a loop.
	BOOL drawLabelContour;
	BOOL hasMenu;

	NSObject *observedObject;
	NSString *observedKeyPath;
	
	NSMenu *titleMenu;
	NSTrackingArea *labelTrackingArea;
		
	id modelValue;
	

}

@property(readonly) NSView *fieldContents;

@property(readonly) CGFloat minimumHeight;
@property(readonly) CGFloat minimumLabelWidth;
@property(nonatomic) BOOL isBeingDragged;
@property(nonatomic) BOOL isPlaceholder;
@property(nonatomic) BOOL editModeOn;
@property(nonatomic) BOOL raisedEditingActive;
@property(nonatomic, retain) NSString *fieldIdentifier;
@property(nonatomic, retain) NSMenu *titleMenu;
@property(nonatomic, retain) id modelValue;

- (id)initWithIdentifier:(NSString *)identifier title:(NSString *)title;

- (void)layoutWithContentsHeight:(CGFloat)contentsHeight;

- (NSRect)contentsFrame;

- (void)updateContentsSize;

- (id)initAsPlaceholderForField:(MGMDFieldView *)field;

- (id)initAsSeparator;
- (void)updateLabelEdge;

- (NSMenu *)fieldMenu;
- (NSString *)contentDescription;
- (NSString *)contentValue;


- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object;


@end
