//
//  ImageLayer.m
//  StackTable
//
//  Created by Dennis Lorson on 25/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "StackImageMainLayer.h"

#import "MGBitmapImageRep.h"
#import "NSImage_MGAdditions.h"
#import "StackLayer.h"
#import "StackView_Layout.h"
#import "Image.h"
#import "StackImageReflectionLayer.h"


@implementation StackImageMainLayer

@synthesize selected, thumbnailRep, fullResRep, reflectionLayer, toolLayer, showReflections;

@dynamic layerOffset, contentsOffset, globalBounds;

+ (CFTimeInterval)fadeDuration
{
	return 0.20;
}

- (id)init
{
	if ((self = [super init])) {
		
		fullImageSize = NSMakeSize(0, 0);
		
		self.tileSize = CGSizeMake(2000,2000);
		self.levelsOfDetail = 1;
		self.backgroundColor = CGColorCreateGenericGray(1, 0.02);
		
		self.contentsGravity = kCAGravityResizeAspect;
		self.opaque = NO;
		
		self.needsDisplayOnBoundsChange = NO;
		
		//[self createReflectionLayer];
		
	}
	return self;
}

- (void)dealloc
{
	self.reflectionLayer = nil;
	self.toolLayer = nil;
	
	// if stack selection changes while in slice mode, stackview doesn't get the opportunity to clear our resources, so lets do it ourself
	[self clearSliceResources];
	
	self.thumbnailRep = nil;
	self.fullResRep = nil;
	//NSLog(@"%@ dealloc", [self description]);
	[super dealloc];
}


- (void)setSelected:(BOOL)flag
{
	selected = flag;
	
}


- (void)setContentsVisible:(BOOL)flag
{
	if (contentsVisible == flag) return;
	
	contentsVisible = flag;
	self.reflectionLayer.contentsVisible = flag;
	
	self.hidden = !flag;
	self.reflectionLayer.hidden = !flag;
	self.toolLayer.hidden = !flag;

	[self setHierarchyNeedsDisplay];

}

- (void)setHierarchyNeedsDisplay
{
	// will mark self and the appropriate sublayers as invalid.
	if (self.contentsVisible) {
		
		[self setNeedsDisplay];
		[reflectionLayer setNeedsDisplay];
		[toolLayer setNeedsDisplay];
		
	}
}

- (void)setShowReflections:(BOOL)flag
{
	if (flag && !reflectionLayer)
		[self createReflectionLayer];
	if (!flag && reflectionLayer)
		[self removeReflectionlayer];
	
	showReflections = flag;
}


#pragma mark -
#pragma mark Thumbnail + nonscaled image info, drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Thumbnail + nonscaled image info, drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (CGFloat)imageDataSize
{
	// return an estimation for the raw size of the thumbnail we use, in order for the stack view to know how many images can be loaded in memory at the same time.
	return self.bounds.size.height * self.bounds.size.width * 24;
	
}

- (NSSize)fullImageSize
{	
	// return an acceptable value for the size of the contents of this image.  This method is called a lot, so it should be as efficient as possible.
	if (fullImageSize.width == 0) 
		fullImageSize = [self dimensionsForImageAtPath:[(Image *)[self.stackImage valueForKeyPath:@"image"] originalImagePathExpanded:YES]];
	
	return fullImageSize;
}





- (void)drawInContext:(CGContextRef)ctx
{
	//NSLog(@"draw %@", NSStringFromRect(NSRectFromCGRect(CGContextGetClipBoundingBox(ctx))));
    
    
    __block CGImageRef thumb = nil;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        thumb = (CGImageRef)[(id)[self image] retain];
    }); 
    
    [(id)thumb autorelease];
	
    if (!thumb)
        return;
	
	@synchronized(self) {
		
		if (!self.contentsVisible)
			return;		
		
		
		CGRect boundsRect = CGContextGetClipBoundingBox(ctx);
				
	
		CGContextSetInterpolationQuality(ctx, kCGInterpolationNone);


		CGContextDrawImage(ctx, boundsRect, thumb);
		
		CGContextBeginPath (ctx);
		CGContextMoveToPoint(ctx, CGRectGetMinX(boundsRect), CGRectGetMinY(boundsRect));
		CGContextAddLineToPoint(ctx, CGRectGetMinX(boundsRect), CGRectGetMaxY(boundsRect));
		CGContextAddLineToPoint(ctx, CGRectGetMaxX(boundsRect), CGRectGetMaxY(boundsRect));
		CGContextAddLineToPoint(ctx, CGRectGetMaxX(boundsRect), CGRectGetMinY(boundsRect));
		CGContextAddLineToPoint(ctx, CGRectGetMinX(boundsRect), CGRectGetMinY(boundsRect));
		
		CGContextSetLineWidth(ctx, 10);
		CGContextSetStrokeColorWithColor(ctx, CGColorCreateGenericGray(1.0, 0.2));
		CGContextStrokePath(ctx);
		
	}
	
}

#pragma mark -
#pragma mark Reflection
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Reflection
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)createReflectionLayer
{
	// add the reflection layer...
	self.reflectionLayer = [StackImageReflectionLayer layer];
	[self addSublayer:reflectionLayer];
	
	reflectionLayer.autoresizingMask = (kCALayerWidthSizable);
	//CATransform3D transform = CATransform3DMakeScale(1,-1,1);
	
	reflectionLayer.levelsOfDetail = 1;
	reflectionLayer.tileSize = CGSizeMake(5000,5000);
	reflectionLayer.opaque = NO;
	//reflectionLayer.delegate = self;
	reflectionLayer.name = @"reflectionLayer";
	reflectionLayer.stackImage = self.stackImage;
		
	reflectionLayer.frame = CGRectMake(0, 0, 100, 100);
	
	
}


- (void)removeReflectionlayer
{
	@synchronized (self) {
		
		[self.reflectionLayer removeFromSuperlayer];
		self.reflectionLayer = nil;
		
	}
}



#pragma mark -
#pragma mark Geometry
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Geometry
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSPoint)contentsOffset
{
	// returns the (integer) offset of the image contents of this layer.
	CGFloat horizontal = [[self.stackImage valueForKey:@"offsetX"] floatValue];
	CGFloat vertical = [[self.stackImage valueForKey:@"offsetY"] floatValue];
	
	return NSMakePoint(horizontal, vertical);
	
}

- (NSPoint)layerOffset
{
	// avoid loading the image size if our offsets are zero
	if ([[self.stackImage valueForKey:@"offsetX"] floatValue] == 0 && [[self.stackImage valueForKey:@"offsetY"] floatValue] == 0) return NSZeroPoint;
	
	
	// returns the offset of this image layer, as a percentage of the image dimensions.
	CGFloat horizontal = [[self.stackImage valueForKey:@"offsetX"] floatValue] / [self fullImageSize].width;
	CGFloat vertical = [[self.stackImage valueForKey:@"offsetY"] floatValue] / [self fullImageSize].height;
	
	return NSMakePoint(horizontal, vertical);
}

- (CGRect)globalBounds
{
	// returns the frame of this image layer in "global" coordinates, that is, including offsets.
	CGRect bounds = self.bounds;
	NSPoint offset = self.layerOffset;
	offset.x *= self.bounds.size.width;
	offset.y *= self.bounds.size.height;
	
	bounds.origin.x += offset.x;
	bounds.origin.y += offset.y;
	
	return bounds;
	
}


#pragma mark -
#pragma mark Slicing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Slicing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)clearSliceResources
{
	self.thumbnailRep = nil;
	
}

- (void)slicePixels:(void **)pixels count:(NSInteger *)count fromPoint:(NSPoint)first toPoint:(NSPoint)second
{	
	// create the thumbnail if we haven't got one
	if (!self.thumbnailRep) {
		
		
		CGImageRef thumbnail = [self image];
		
		self.thumbnailRep = [[[MGBitmapImageRep alloc] initWithCGImage:thumbnail] autorelease];

		
	}
	
	
	
	// points are in layer coordinates!!!! need to convert them to image coordinates
	// here, we use the pixel coords of the thumbnail, as it will be used to generate the preview.
	NSInteger pixelsWide = [self.thumbnailRep pixelsWide];
	NSInteger pixelsHigh = [self.thumbnailRep pixelsHigh];
	
	StackImageMainLayer *selectedLayer = ((StackLayer *)[self superlayer]).selectedLayer;
	
	CGFloat layerPixelsWide = selectedLayer.bounds.size.width;
	CGFloat layerPixelsHigh = selectedLayer.bounds.size.height;
	
	NSPoint firstPointConverted = NSMakePoint(first.x * (float)pixelsWide / layerPixelsWide, first.y * (float)pixelsHigh / layerPixelsHigh);
	NSPoint secondPointConverted = NSMakePoint(second.x * (float)pixelsWide / layerPixelsWide, second.y * (float)pixelsHigh / layerPixelsHigh);
	
	// offset the points if needed, this is done AFTER conversion because offsets are in image coordinate space.
	CGFloat offsetX = [[self.stackImage valueForKey:@"offsetX"] floatValue];
	CGFloat offsetY = [[self.stackImage valueForKey:@"offsetY"] floatValue];
	
	NSPoint firstPointConvertedWithOffset = firstPointConverted;
	NSPoint secondPointConvertedWithOffset = secondPointConverted;
	
	firstPointConvertedWithOffset.x -= offsetX;
	firstPointConvertedWithOffset.y -= offsetY;
	secondPointConvertedWithOffset.x -= offsetX;
	secondPointConvertedWithOffset.y -= offsetY;
	
	[self.thumbnailRep pixelData:(unsigned char **)pixels count:count fromPoint:firstPointConvertedWithOffset toPoint:secondPointConvertedWithOffset];
	
}

- (void)fullResSlicePixels:(void **)pixels count:(NSInteger *)count fromPoint:(NSPoint)first toPoint:(NSPoint)second;
{
	// create the thumbnail if we haven't got one
	if (!self.fullResRep) {
		
		
		CGImageRef thumbnail = [self image];
		
		self.fullResRep = [[[MGBitmapImageRep alloc] initWithCGImage:thumbnail] autorelease];

		
	}
	
	
	
	// points are in layer coordinates!!!! need to convert them to image coordinates
	// here, we use the pixel coords of the thumbnail, as it will be used to generate the preview.
	NSInteger pixelsWide = [self.fullResRep pixelsWide];
	NSInteger pixelsHigh = [self.fullResRep pixelsHigh];
	
	StackImageMainLayer *selectedLayer = ((StackLayer *)[self superlayer]).selectedLayer;
	
	CGFloat layerPixelsWide = selectedLayer.bounds.size.width;
	CGFloat layerPixelsHigh = selectedLayer.bounds.size.height;
	
	NSPoint firstPointConverted = NSMakePoint(first.x * (float)pixelsWide / layerPixelsWide, first.y * (float)pixelsHigh / layerPixelsHigh);
	NSPoint secondPointConverted = NSMakePoint(second.x * (float)pixelsWide / layerPixelsWide, second.y * (float)pixelsHigh / layerPixelsHigh);
	
	// offset the points if needed, this is done AFTER conversion because offsets are in image coordinate space.
	CGFloat offsetX = [[self.stackImage valueForKey:@"offsetX"] floatValue];
	CGFloat offsetY = [[self.stackImage valueForKey:@"offsetY"] floatValue];
	
	NSPoint firstPointConvertedWithOffset = firstPointConverted;
	NSPoint secondPointConvertedWithOffset = secondPointConverted;
	
	firstPointConvertedWithOffset.x -= offsetX;
	firstPointConvertedWithOffset.y -= offsetY;
	secondPointConvertedWithOffset.x -= offsetX;
	secondPointConvertedWithOffset.y -= offsetY;
	
	[self.fullResRep pixelData:(unsigned char **)pixels count:count fromPoint:firstPointConvertedWithOffset toPoint:secondPointConvertedWithOffset];
	
	
}

- (void)clearFullResSliceResources;
{
	self.fullResRep = nil;
}


- (CALayer *)hitTest:(CGPoint)thePoint
{
	// hide sublayers (e.g. the layer that draws the beziers) from user events
	CALayer *normalHitLayer = [super hitTest:thePoint];
	
	if ([[self sublayers] containsObject:normalHitLayer]) return self;
	
	return nil;
}

@end
