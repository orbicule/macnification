//
//  LightTablePrintContentView.m
//  LightTable
//
//  Created by Dennis on 28/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTablePrintContentView.h"
#import "LightTableImageView.h"
#import "LightTableContentView.h"
#import "LightTableToolView.h"
#import "NSImage_MGAdditions.h"
#import "NSImageRep_MGAdditions.h"


@interface LightTablePrintContentView (Private)


- (NSRect)boundingBoxOfImageViews:(NSArray *)imageViews;

- (void)addCopyOfSubview:(LightTableImageView *)subview;
- (NSArray *)subviewsInRect:(NSRect)rect;



@end



@implementation LightTablePrintContentView

- (id)initWithContentView:(LightTableContentView *)originalView 
{
    self = [super initWithFrame:[originalView frame]];
    if (self) {
		[self setAutoresizesSubviews:YES];
		[self setBounds:[originalView bounds]];
		
		for (LightTableImageView *view in [originalView subviews]) {
			[self addCopyOfSubview:view];
		}
		
    }
    return self;
}

- (void)drawRect:(NSRect)rect
{
	//[[NSColor whiteColor] set];
	//NSRectFill([self bounds]);
}

- (void)addCopyOfSubview:(LightTableImageView *)subview
{
	// will create a copy the given subview and add it to selfs subviews.
	LightTableImageView *copy = (LightTableImageView *)[[subview copyWithZone:nil] autorelease];
	[self addSubview:copy];	
}

- (NSRect)boundingBoxOfImageViews:(NSArray *)imageViews
{
	// returns the bounding box that encloses all of this content views subviews.
	if ([imageViews count] == 0) return NSZeroRect;
	
	CGFloat minX,minY,maxX,maxY;
	minX = minY = 10000;
	maxX = maxY = -10000;
	
	for (LightTableImageView *view in imageViews) {
		NSRect viewFrame = [view frame];
		minX = fmin(minX, NSMinX(viewFrame));
		minY = fmin(minY, NSMinY(viewFrame));
		maxX = fmax(maxX, NSMaxX(viewFrame));
		maxY = fmax(maxY, NSMaxY(viewFrame));
	}
	
	return NSMakeRect(minX, minY, maxX-minX, maxY-minY);
}


- (NSArray *)subviewsInRect:(NSRect)rect
{
	NSMutableArray *views = [NSMutableArray array];
	for (LightTableImageView *view in [self subviews]) {
		if (NSIntersectsRect([view frame], rect)) {
			[views addObject:view];
		}
	}
	return views;
}

- (NSImage *)compositeImageFromRect:(NSRect)rect constrainToImageBounds:(BOOL)constrain sideMargins:(NSSize)margins
{
	// returns a NSImage with the view contents in the rect that encloses all the image views.
	// if rect == NSZeroRect, uses the bounding box off the image collection.
	
	NSRect destRect = NSEqualRects(rect, NSZeroRect) ? NSInsetRect([self boundingBoxOfImageViews:[self subviews]], -margins.width, -margins.height) : NSInsetRect(rect, -margins.width, -margins.height);
	
	if (constrain && !NSEqualRects(NSZeroRect, rect)) {
		
		// override the margin as this makes no sense when constraining to image bounding box is desired.
		NSRect imageBounds = [self boundingBoxOfImageViews:[self subviewsInRect:rect]];
		destRect = NSIntersectionRect(imageBounds, rect);
	}
		
		
	// we've got our destination rect, now let's change our frame so that it fits that rect.  This will restrict drawing to the necessary area.
	[self setFrameSize:destRect.size];
	[self setBounds:destRect];
	
	// compute our view scale factor.
	// we need to scale our view so that each image is presented in the composite with its original pixel size.
	// the scale defining image is the one that has the smallest (screen pixel size / real pixel size) ratio.
	LightTableImageView *scaleDefiningView = nil;
	CGFloat minimumScale = 1.0;
	CGFloat maximumDPI = 72.0;
	
	for (LightTableImageView *view in [self subviewsInRect:destRect]) {

		
		CGFloat realPixelSize = [[view image] bestRepresentationSize].width;
		CGFloat framePixelSize = [view frame].size.width;
		
		CGFloat scale = framePixelSize / realPixelSize;
		
		if (scale < minimumScale) {
			minimumScale = scale;
			scaleDefiningView = view;
			
			// now get the dpi from this image. We will use this dpi as the dpi of the composite.
			maximumDPI = [[[view image] representation] dpiX];
		}
	}
	// we now need to scale up our view so that this defining image gets displayed with a 1:1 scale so it contains all its original pixels.
	// As other image scales are equal or larger, they will be upscaled too until they have at least a 1:1 ratio relative to their originals.
	CGFloat viewScaleFactor = 1 / minimumScale;
	
	for (LightTableImageView *view in [self subviewsInRect:destRect]) {
		
		// scale up the frame of these views...
		NSRect frame = [view frame];
		frame.origin.x *= viewScaleFactor;
		frame.origin.y *= viewScaleFactor;
		frame.size.width *= viewScaleFactor;
		frame.size.height *= viewScaleFactor;
		
		[view setFrame:frame];
		//NSLog(@"view frame %@", NSStringFromRect(frame));
	}
	
	// we need to adjust our bounds/frame to make the new scale effective :
	NSRect frame = [self frame];
    NSRect bounds = [self bounds];
	
	bounds.size.height *= viewScaleFactor;
	bounds.size.width *= viewScaleFactor;
	bounds.origin.x *= viewScaleFactor;
	bounds.origin.y *= viewScaleFactor;
	
    frame.size.width *= viewScaleFactor;
    frame.size.height *= viewScaleFactor;
	
	[self setFrame: frame];   
	[self setBounds:bounds];

	// composite to an image.
	NSData *pdfData = [self dataWithPDFInsideRect:[self bounds]];
		
	NSImage *pdfImage = [[[NSImage alloc] initWithData:pdfData] autorelease];
	
	// convert into a flat tiff, otherwise DPI changes won't work.
	NSImage *tiffImage = [[[NSImage alloc] initWithData:[pdfImage TIFFRepresentation]] autorelease];
	
	// set the correct image dpi in order to keep the same physical size as on the screen (that is, multiply the dpi by the scaling factor...
	[[tiffImage representation] setDpi:(NSMakeSize(((int)maximumDPI), ((int)maximumDPI)))];

	return tiffImage;
}

- (CGFloat)zoomLevel
{
	// dummy method for subview drawing that won't really take effect.
	return 1.0;
}





@end
