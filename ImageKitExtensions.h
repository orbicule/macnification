//
//  ImageKitExtensions.h
//  Filament
//
//  Created by Peter Schols on 22/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <ImageKit/ImageKit.h>


// Added to disable warnings when using private API in ImageKit

@interface IKImageBrowserView (IKImageBrowserViewExtensions) 

- (void)setBackgroundColor:(NSColor *)color;
- (void)setAllowsDropOnItems:(BOOL)flag;
- (void)collapseGroup:(id)group;
- (void)expandGroup:(id)group;
- (NSRange)range;
- (id)layoutManager;
- (NSArray *)groups;
- (BOOL)expanded;

@end


@interface IKImageView (IKImageViewExtensions) 

- (IBAction)zoomIn:(id)sender;
- (IBAction)zoomOut:(id)sender;
- (void)setFilterArray:(NSArray *)filterArray;
- (CALayer *)imageLayer;

@end
