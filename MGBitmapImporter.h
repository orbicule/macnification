//
//  MGBitmapImporter.h
//  Filament
//
//  Created by Dennis Lorson on 12/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGBitmapImporter : NSObject {

}


+ (NSBitmapImageRep *)readGray32bitImageWithMetadata:(NSDictionary *)metadata;
+ (NSBitmapImageRep *)readGray16bitImageWithMetadata:(NSDictionary *)metadata;
+ (NSBitmapImageRep *)readComponentImageWithMetadata:(NSDictionary *)metadata;

+ (NSBitmapImageRep *)importRepWithMetadata:(NSDictionary *)metadata;


@end
