//
//  StackImageReflectionLayer.m
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "StackImageReflectionLayer.h"


@implementation StackImageReflectionLayer


- (void)drawInContext:(CGContextRef)ctx
{
    
    __block CGImageRef thumb = nil;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        thumb = (CGImageRef)[(id)[self image] retain];
    }); 
    
    [(id)thumb autorelease];
	
    if (!thumb)
        return;
    
	@synchronized(self) {
		
		if (!self.contentsVisible)
			return;
				
		CGRect boundsRect = CGContextGetClipBoundingBox(ctx);
		
		int width = (int)boundsRect.size.width;
		int height = (int)boundsRect.size.height;
		
		

		
		CGContextSetInterpolationQuality(ctx, kCGInterpolationNone);
		
		

		// flip the image
		CGContextScaleCTM(ctx, 1.0, -1.0);
		CGContextTranslateCTM (ctx, 0, -height);
		
		CGContextDrawImage(ctx, CGRectMake(0, 0, width, CGImageGetHeight(thumb)), thumb);
				
		// gradient
		const CGFloat components[] = {255, 255, 255, 1.0,
			255, 255, 255, 0.6};
		const CGFloat locations[] = {0, 1};
		
		CGGradientRef grad = CGGradientCreateWithColorComponents(CGBitmapContextGetColorSpace(ctx), components, locations, 2);
		
		// blending mode == dest out -> 100% alpha mask will make completely transparent, 0% will retain the masked image alpha.
		CGContextSetBlendMode(ctx, kCGBlendModeDestinationOut);
		CGContextDrawLinearGradient(ctx, grad, CGPointMake(0, height), CGPointMake(0, 0), (CGGradientDrawingOptions)0);
				
		CGGradientRelease(grad);
	
		
	}
	
}



@end
