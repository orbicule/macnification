//
//  RolloverButton.m
//  Tracker
//
//  Created by Peter Schols on 10/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "RolloverButton.h"
#import "MGGradientAttachedWindow.h"


@implementation RolloverButton



- (void)awakeFromNib;
{
	NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:[self frame]
																options:NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp | NSTrackingInVisibleRect
																  owner:self
															   userInfo:nil];
	[self addTrackingArea:trackingArea];
	[trackingArea release];
	tooltipWindow = nil;
}



- (void)mouseEntered:(NSEvent *)event;
{
	[self showTooltipWindow];
}



- (void)showTooltipWindow;
{
	
	NSPoint pointToAttach = NSMakePoint([self frame].origin.x + [self frame].size.width/2, [[self superview]frame].size.height - 8);
	
	// Create the text field we will use to draw the tooltip label
	NSTextField *textField = [[[NSTextField alloc] init] autorelease];
	[textField setEditable:NO];
	[textField setSelectable:NO];
	[textField setDrawsBackground:NO];
	[textField setBordered:NO];
	[textField setBezeled:NO];
	[textField setFont:[NSFont systemFontOfSize:11.0]];
	[textField setTextColor:[NSColor whiteColor]];
	[textField setStringValue:[self title]];
	[textField sizeToFit];
	
	NSView *enclosingView = [[[NSView alloc] initWithFrame:NSInsetRect([textField frame], -2, -1)] autorelease];
	
	[textField setFrameOrigin:NSMakePoint(2, 1)];
	[enclosingView addSubview:textField];
	
	// Create the MAAttachedWindow
	if (!tooltipWindow) {
		tooltipWindow = [[MGGradientAttachedWindow alloc] initWithView:enclosingView attachedToPoint:pointToAttach 
															  inWindow:[self window] onSide:MAPositionTop atDistance:3.0];
		[tooltipWindow setReleasedWhenClosed:NO];
		[tooltipWindow setBorderWidth:0.0];
	}
	
	[tooltipWindow setPoint:pointToAttach];
	
	// Show the window
	[tooltipWindow makeKeyAndOrderFront:self];
}


- (BOOL)isOpaque
{
	
	return NO;
}

- (void)mouseExited:(NSEvent *)event;
{
	if (tooltipWindow) {
		[tooltipWindow close];
	}
	// We don't need to release the tooltipWindow: it is released on close
}




- (void)closeToolTip;
{
	if (tooltipWindow) {
		[tooltipWindow close];
	}
}


- (void)dealloc
{
	if (tooltipWindow) {
		[tooltipWindow release];
	}
	[super dealloc];	
}


@end
