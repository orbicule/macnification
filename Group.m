//
//  Group.m
//  Filament
//
//  Created by Peter Schols on 17/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "Group.h"


@implementation Group


- (NSImage *)iconImage { return [NSImage imageNamed:@"projectSmall"]; }
- (NSImage *)largeIconImage { return [NSImage imageNamed:@"projectLarge"]; }



- (id)parentGroup { return self; }

- (BOOL)acceptsDroppedKeywords { return NO; }
- (BOOL)acceptsDroppedImages { return NO; }
- (BOOL)acceptsDroppedImagesFromFinder { return YES; }

- (BOOL)canEdit { return YES; }
- (BOOL)canBeDragged { return YES; }

- (BOOL)needsZoomSlider { return NO; }

- (BOOL)canHaveAlbumAsChild:(Album *)proposedChild;
{
	return ![proposedChild isKindOfClass:[Group class]];
}


@end
