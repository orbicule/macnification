//
//  StackController.h
//  Filament
//
//  Created by Peter Schols on 21/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>
#import <ImageKit/ImageKit.h>

#import "MGCustomMetadataController.h"

@class StackView;
@class Stack;
@class ImageArrayController;

@interface StackController : NSObject <MGCustomMetadataObserver>
{
	IBOutlet NSView * m_view;
	
	IBOutlet StackView *stackView;
	IBOutlet NSButton *toggleMDButton;
	IBOutlet NSSearchField *stackSearchField;
	IBOutlet NSArrayController *stackArrayController;
	IBOutlet NSArrayController *stackImageArrayController;
	
	ImageArrayController *imageArrayController;
	
	IBOutlet NSTextField *searchMessage;
	IBOutlet NSButton *doneButton;
	
	IBOutlet QTMovieView *movieView;
	
	IBOutlet NSButton *backButton;
	IBOutlet NSTextField *stackTitleField;
	
	IBOutlet IKImageBrowserView *imageBrowser;
	
	IBOutlet NSButton *browserViewModeButton;
	
	NSMutableArray *draggedItems;
	
	
	IBOutlet NSPopUpButton *sortKeyPopUpButton;
	IBOutlet NSSegmentedControl *sortOrderControl;
	
	BOOL initialSortControlOriginsSet;
	NSPoint initialSortKeyPopUpButtonOrigin;
	NSPoint initialSortOrderControlOrigin;
}

- (id)initWithSuperview:(NSView *)superview;

- (void)setImageArrayController:(ImageArrayController *)ctl;

-(void)bindToStack:(Stack *)stack;

- (NSArray *)sortedImages;
- (NSArray *)draggedImages;

- (void)removeFilterPredicate;
- (NSArrayController *)contentController;
- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;

- (IBAction)clearFilterPredicate:(id)sender;
- (IBAction)switchStackView:(id)sender;
- (IBAction)changeSortKey:(id)sender;
- (IBAction)changeSortOrder:(id)sender;

- (void)showSearchBarWithMessage:(NSString *)message;
- (void)hideSearchBar;
- (void)changeSearchCategory:(id)sender;
- (IBAction)changeSearchString:(id)sender;

- (NSButton *)backButton;

@end
