//
//  MGHUDButton.m
//  Filament
//
//  Created by Dennis Lorson on 25/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDButton.h"
#import "MGHUDButtonCell.h"
#import "NSAttributedString_MGExtensions.h"

@implementation MGHUDButton



+ (Class)cellClass
{
	return [MGHUDButtonCell class];
}

- initWithCoder: (NSCoder *)origCoder
{
	if(![origCoder isKindOfClass: [NSKeyedUnarchiver class]]){
		self = [super initWithCoder: origCoder]; 
	} else {
		NSKeyedUnarchiver *coder = (id)origCoder;
		
		NSString *oldClassName = [[[self superclass] cellClass] className];
		Class oldClass = [coder classForClassName: oldClassName];
		if(!oldClass)
			oldClass = [[super superclass] cellClass];
		[coder setClass: [[self class] cellClass] forClassName: oldClassName];
		self = [super initWithCoder: coder];
		[coder setClass: oldClass forClassName: oldClassName];
		
        NSDictionary *props = [NSDictionary dictionaryWithObject:[NSColor colorWithCalibratedWhite:1.0 alpha:1.0] forKey:NSForegroundColorAttributeName];
        [self setAttributedTitle:[[self attributedTitle] attributedStringByAddingOrReplacingAttributes:props]];

	}
	
	return self;
}

@end
