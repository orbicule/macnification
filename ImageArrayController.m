//
//  ImageArrayController.m
//  Filament
//
//  
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "ImageArrayController.h"
#import "MGAppDelegate.h"
#import "Image.h"
#import "KeywordTreeController.h"
#import "KeywordAnimationWindow.h"
#import "Album.h"
#import "ROI.h"
#import "FullScreenController.h"
#import "StringExtensions.h"
#import "ImageKitExtensions.h"
#import "Montage.h"
#import "PSImageBrowserView.h"
#import "MGMDView.h"
#import "MetadataController.h"
#import "ChannelMergeController.h"
#import "Stack.h"
#import "SourceListTreeController.h"
#import "MGFilterAndSortMetadataController.h"
#import "MGMetadataExportController.h"
#import "MGLibraryController.h"

NSString *FilamentImagesType = @"Filament Images Type For Pasteboard";
NSString *MovedRowsType = @"MOVED_ROWS_TYPE";
NSString *CopiedRowsType = @"COPIED_ROWS_TYPE";


@interface ImageArrayController ()

- (void)_updateSearchFieldCategories;
- (void)_refreshSortKeysPopup;
- (void)_refreshSortControls;

@end




@implementation ImageArrayController


@synthesize imageToCopyROIsFrom;
@synthesize imageToCopyFiltersFrom;
@synthesize imageToCopyKeywordsFrom;
@synthesize imageToCopyCalibrationFrom;

#pragma mark -

- (id) init
{
	self = [super init];
	if (self != nil) {
		manualObservers = [[NSMutableDictionary alloc] init];
	}
	return self;
}


- (id) initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	if (self != nil) {
		manualObservers = [[NSMutableDictionary alloc] init];
	}
	return self;
}


- (void)awakeFromNib
{
	showsStacks = YES;
	
	[[MGCustomMetadataController sharedController] addObserver:self];

	[imageBrowser registerForDraggedTypes:[NSArray arrayWithObjects:@"keywordsPboardType", NSFilenamesPboardType, nil]];
	
	[self _refreshSortControls];
	
    [imageBrowser setDraggingDestinationDelegate:self];
	[imageBrowser setCellsStyleMask:IKCellsStyleTitled | IKCellsStyleSubtitled];
	[imageBrowser setValue:[NSColor colorWithCalibratedRed:0.18 green:0.18 blue:0.18 alpha:1] forKey:IKImageBrowserBackgroundColorKey];
	[imageBrowser setValue:[NSColor colorWithCalibratedRed:80./255. green:125./255. blue:170./255. alpha:1] forKey:IKImageBrowserSelectionColorKey];

	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSFont fontWithName:@"Lucida Grande" size:10], NSFontAttributeName, 
							[NSColor whiteColor], @"NSColor", nil] 
					forKey:IKImageBrowserCellsTitleAttributesKey];
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSFont fontWithName:@"Lucida Grande" size:10], NSFontAttributeName,
							[NSColor whiteColor], @"NSColor", nil] 
					forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSFont fontWithName:@"Lucida Grande" size:11], NSFontAttributeName, 
							[NSColor lightGrayColor], @"NSColor", nil] 
					forKey:IKImageBrowserCellsSubtitleAttributesKey];
	[imageBrowser setZoomValue:0.20];
	[imageBrowser setAllowsDropOnItems:YES];
	[imageBrowser setAnimates:YES];
	
	[[[imageBrowser enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[imageBrowser enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
	
	[[imageSizeSlider cell] setControlTint:NSGraphiteControlTint];
	
	[self addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	[self addObserver:self forKeyPath:@"arrangedObjects" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];

	[self _updateSearchFieldCategories];
	
	// register for model notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeManualModelPropertyChange:) name:nil object:self];
	
	
}

// show no stacks if we're in full screen mode

- (NSArray *)arrangeObjects:(NSArray *)objects
{
	// remove all images that belong to a stack; they will be displayed within the subview of that stack.
	
	NSArray *sorted = [super arrangeObjects:objects];
	
	if (showsStacks)
		return sorted;
	
	NSMutableArray *sortedWithoutStackImages = [NSMutableArray arrayWithCapacity:[sorted count]];
	
	for (id image in sorted)
		if (![image isKindOfClass:[Stack class]])
			[sortedWithoutStackImages addObject:image];
	
	return sortedWithoutStackImages;
}


- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)controller;
{
	// refresh the selection
	NSArray *selectedObjects = [[[self selectedObjects] copy] autorelease];
	[self setSelectedObjects:nil];
	[self setSelectedObjects:selectedObjects];
	
	
	// refresh the search menu
	[self searchCurrentFolderWithPredicate:nil naturalLanguageString:@""];
	[self _updateSearchFieldCategories];
	
	// refresh the sort menu
	[self _refreshSortControls];
	
}

# pragma mark -
# pragma mark IKImageBrowserView datasource

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)aBrowser;
{
	return [[self arrangedObjects] count];
}


- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index;
{
	return [[self arrangedObjects] objectAtIndex:index];
}



- (void)imageBrowser:(IKImageBrowserView *)view removeItemsAtIndexes:(NSIndexSet *)indexes
{
	[self remove:self];
}



- (NSUInteger)imageBrowser:(IKImageBrowserView *)aBrowser writeItemsAtIndexes:(NSIndexSet *)itemIndexes toPasteboard:(NSPasteboard *)pasteboard;
{
	NSString *draggedImageType = @"DRAGGED_IMAGES_TYPE";
	NSArray *typesArray = [NSArray arrayWithObjects: draggedImageType, NSFileContentsPboardType, NSFilenamesPboardType, nil];
	[pasteboard declareTypes:typesArray owner:self];
	
	// Write the image's filename to the pasteboard, so the images can be dragged to an app that accepts filenames
	NSArray *imagesToWrite = [[self arrangedObjects] objectsAtIndexes:itemIndexes];
	
	NSMutableArray *imagesToWriteWOStacks = [NSMutableArray array];
	for (Image *image in imagesToWrite)
		if (![image isKindOfClass:[Stack class]])
			[imagesToWriteWOStacks addObject:image];
	
	//imagesToWrite = imagesToWriteWOStacks;
	
	NSMutableArray *filePaths = [NSMutableArray arrayWithCapacity:5];
	for (Image *image in imagesToWrite) {
		NSString *filePath;
		if ([self exportsWithScaleBar]) {
			// Get a temporary path to the image file with a scale bar
			filePath = [image temporaryPathToFilteredImageWithScaleBar];	
			
		} else {
			if ([image hasFilters]) {
				// Get a temporary path to the filtered image
				filePath = [image temporaryPathToFilteredImage];	
			} else {
				// If there are no filters, simply return the activeImagePath
				filePath = [image activeImagePathExpanded:YES];
			}
		}
		[filePaths addObject:filePath];
	}
	[pasteboard setPropertyList:filePaths forType:NSFilenamesPboardType];
	
	return [imagesToWrite count];
	
}


# pragma mark -
# pragma mark IKImageBrowserView delegate

- (void)imageBrowserSelectionDidChange:(IKImageBrowserView *)aBrowser;
{
	[self setSelectionIndexes:[imageBrowser selectionIndexes]];
	
}

- (void) imageBrowser:(IKImageBrowserView *) aBrowser cellWasRightClickedAtIndex:(NSUInteger) index withEvent:(NSEvent *) event;
{
	if ([sourceListTreeController selectedCollection] == [[MGLibraryControllerInstance library] libraryGroup]) {
		
		// decide whether the Stack/unstack items are to be shown
		NSArray *images = [self selectedObjects];
		
		BOOL selectionIsUnstackable = ([images count] == 1 && [[images objectAtIndex:0] isKindOfClass:[Stack class]]);
		
		BOOL selectionIsStackable = YES;
		
		if ([images count] < 2)
			selectionIsStackable = NO;
		
		for (Image *image in images) {
			if ([image isKindOfClass:[Stack class]])
				selectionIsStackable = NO;
		}
		
		[stackMenuItem setHidden:!selectionIsStackable];
		[unstackMenuItem setHidden:!selectionIsUnstackable];
		
		[stackSeparatorItem setHidden:(!selectionIsUnstackable && !selectionIsStackable)];		
	}
	
	else {
		
		[stackMenuItem setHidden:YES];
		[unstackMenuItem setHidden:YES];
		
		[stackSeparatorItem setHidden:YES];
		
	}
	

	
	
	[NSMenu popUpContextMenu:browserMenu withEvent:event forView:aBrowser];
}


- (void)imageBrowser:(IKImageBrowserView *)aBrowser cellWasDoubleClickedAtIndex:(NSUInteger)index;
{
	if (aBrowser == imageBrowser) {
		
		id image = [[self arrangedObjects] objectAtIndex:index];

		if ([image isMemberOfClass:[Stack class]]) {
			[sourceListTreeController showStack:image];
		} else {
			[self setSelectionIndex:index];
			[fullScreenController enterFullScreen:self];
		}
	}
}



- (void)setBrowserAnimates:(BOOL)flag
{
	[imageBrowser setAnimates:flag];
}


#pragma mark -
#pragma mark Stacking + unstacking

- (IBAction)stackImages:(id)sender
{
	NSAlert *alert = [NSAlert alertWithMessageText:@"Do you want to stack these images?"
									 defaultButton:@"Stack"
								   alternateButton:@"Cancel"
									   otherButton:nil
						 informativeTextWithFormat:@"The individual images will be removed from all albums."];
	[alert setShowsSuppressionButton:YES];
	
	
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"suppressStackImagesWarning"] || [alert runModal] == NSAlertDefaultReturn) {
		
		// first, set the suppression value in the prefs (only do this if "Cancel" wasn't chosen)
		if ([[alert suppressionButton] state] == NSOnState)
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressStackImagesWarning"];
		
		
		// remove stack images that may be present
		NSArray *images = [self selectedObjects];
		NSMutableArray *imagesWOStacks = [NSMutableArray array];
		for (Image *image in images)
			if (![image isKindOfClass:[Stack class]])
				[imagesWOStacks addObject:image];
		
		NSSet *imageSet = [NSSet setWithArray:imagesWOStacks];
				
		// remove all images from all other groups
		// since there are potentially hundreds of images, avoid triggering the KVO notifications for every single image
		NSSet *imageAlbums = [self valueForKeyPath:@"selection.@distinctUnionOfSets.albums"];
		
		for (Album *album in imageAlbums)
			[[album mutableSetValueForKey:@"images"] minusSet:imageSet];
		
		//for (id image in images)
		//	[image setValue:nil forKey:@"albums"];
		
		Stack *stack = [NSEntityDescription insertNewObjectForEntityForName:@"Stack" inManagedObjectContext:[self managedObjectContext]];
		[stack setValue:[[imagesWOStacks objectAtIndex:0] valueForKey:@"name"] forKey:@"name"];
		
		[stack addImages:imagesWOStacks];	
		
		NSMutableSet *albums = [stack mutableSetValueForKey:@"albums"];
		[albums addObject:[[MGLibraryControllerInstance library] libraryGroup]];
	
	}
		

	
}


- (IBAction)unstackImages:(id)sender
{
	NSAlert *alert = [NSAlert alertWithMessageText:@"Do you want to unstack this stack?"
									 defaultButton:@"Unstack"
								   alternateButton:@"Cancel"
									   otherButton:nil
						 informativeTextWithFormat:@"The stack will be removed from all albums."];
	
	[alert setShowsSuppressionButton:YES];
	[[alert suppressionButton] setTitle:@"Do not show this warning again"];
	
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"suppressUnstackImagesWarning"] || [alert runModal] == NSAlertDefaultReturn) {
		
		// first, set the suppression value in the prefs (only do this if "Cancel" wasn't chosen)
		if ([[alert suppressionButton] state] == NSOnState)
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressUnstackImagesWarning"];
		
		
		
		
		
		NSArray *stacks = [self selectedObjects];
		
		NSMutableArray *woRegularImages = [NSMutableArray array];
		for (Image *image in [self selectedObjects])
			if ([image isKindOfClass:[Stack class]])
				[woRegularImages addObject:image];
		
		stacks = woRegularImages;
		
		
		for (Stack *stack in [[stacks copy] autorelease]) {
			
			NSSet *images = [stack valueForKeyPath:@"stackImages.@distinctUnionOfObjects.image"];
			
			for (id image in images)
				[image setValue:nil forKey:@"stackImage"];
			
			// remove the stack (it will cascade to the stackImages, which will cascade to the Images -- but they are not part of that relationship anymore)
			[[self managedObjectContext] deleteObject:stack];

			// add the images from the stack back to the library
			NSMutableSet *libraryImages = [[[MGLibraryControllerInstance library] libraryGroup] mutableSetValueForKey:@"images"];
			[libraryImages unionSet:images];
			
			

		}
				
	}
}



- (BOOL)selectionContainsStack
{
	for (Image *image in [self selectedObjects])
		if ([image isKindOfClass:[Stack class]])
			return YES;
	
	return NO;
}

- (BOOL)numberOfStacksInSelection
{
	int nStacks = 0;
	
	for (Image *image in [self selectedObjects])
		if ([image isKindOfClass:[Stack class]])
			nStacks++;
	
	return nStacks;
}

- (NSArray *)selectedObjectsWithStacksExpanded;
{
	NSArray *selection = [self selectedObjects];
	
	NSMutableArray *selectionWithStacksExpanded = [NSMutableArray array];
	for (Image *image in selection) {
		if ([image isKindOfClass:[Stack class]])
			[selectionWithStacksExpanded addObjectsFromArray:[[(Stack *)image images] sortedArrayUsingDescriptors:[self sortDescriptors]]];
		else
			[selectionWithStacksExpanded addObject:image];
	}

	return selectionWithStacksExpanded;
		
}

- (NSArray *)arrangedObjectsWithStacksExpanded;
{
	NSArray *arranged = [self arrangedObjects];
	
	NSMutableArray *arrangedWithStacksExpanded = [NSMutableArray array];
	for (Image *image in arranged) {
		if ([image isKindOfClass:[Stack class]])
			[arrangedWithStacksExpanded addObjectsFromArray:[[(Stack *)image images] sortedArrayUsingDescriptors:[self sortDescriptors]]];
		else
			[arrangedWithStacksExpanded addObject:image];
	}
	
	return arrangedWithStacksExpanded;
}

#pragma mark -
#pragma mark Sorting (+UI)

- (IBAction)changeSortKey:(id)sender
{		
	[self setSortKey:[sender representedObject]];
}

- (IBAction)changeSortOrder:(id)sender
{
	
	[self setSortOrder:[sender selectedSegment] == 1];
	
	
	// change all the items in the IAC that are stacks (they need to change the order of their preview)
	for (Image *image in [self arrangedObjects])
		if ([image isKindOfClass:[Stack class]])
			[image increaseBrowserVersion];
	
	[imageBrowser reloadData];
}

- (BOOL)hasNonDefaultSortOrderOrKey
{
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ImageArraySortOrder"]) return YES;
	if (!([[[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"] isEqualToString:@"creationDate"])) return YES;
	
	return NO;
}


- (void)setSortKey:(NSString *)key
{
	if (![[[MGFilterAndSortMetadataController sharedController] sortAttributeKeyPaths] containsObject:key])
		key = @"name";
	
	[[NSUserDefaults standardUserDefaults] setValue:key forKey:@"ImageArraySortKey"];
	
	NSString *sortKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"];
	BOOL sortOrder = ![[NSUserDefaults standardUserDefaults] boolForKey:@"ImageArraySortOrder"];
	
	// Sort ourselves based on the primary key
	NSSortDescriptor *sortD = [[[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortOrder] autorelease];
	
	// set a backup key in case the first one isn't discriminating enough
	NSSortDescriptor *sortD2 = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];
	[self setSortDescriptors:[NSArray arrayWithObjects:sortD, sortD2, nil]];

	
	// set the popup item and segmented control to reflect the new settings
	
	for (NSMenuItem *item in [[sortKeyPopUpButton menu] itemArray])
		if ([[item representedObject] isEqualTo:key])
			[sortKeyPopUpButton selectItem:item];
	
	[sortOrderControl setSelectedSegment:sortOrder ? 0 : 1];

	[imageBrowser reloadData];
}

- (void)setSortOrder:(BOOL)descendingOrAscending
{
	[[NSUserDefaults standardUserDefaults] setBool:descendingOrAscending forKey:@"ImageArraySortOrder"];
	
	
	[self setSortKey:[[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"]];
	
}

- (void)_refreshSortControls
{
	
	[self setSortControlsVisible:YES];

	
	[self _refreshSortKeysPopup];
	
	// Setup the sort controls
	NSImage *sortAscending = [NSImage imageNamed:@"popup_sort_ascending"];
	[sortAscending setTemplate:YES];
	[sortOrderControl setImage:sortAscending forSegment:0];
	NSImage *sortDescending = [NSImage imageNamed:@"popup_sort_descending"];
	[sortDescending setTemplate:YES];
	[sortOrderControl setImage:sortDescending forSegment:1];	
	

	
}

- (void)_refreshSortKeysPopup
{
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] sortAttributeKeyPaths]) {
		
		if ((id)keyPath != [NSNull null]) {
			
			NSMenuItem *keyItem = [[[NSMenuItem alloc] init] autorelease];
			[keyItem setAttributedTitle:[[[NSAttributedString alloc] initWithString:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath] 
																		 attributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11], NSFontAttributeName, nil]] autorelease]];
			[keyItem setRepresentedObject:keyPath];
			[keyItem setTarget:self];
			[keyItem setAction:@selector(changeSortKey:)];
			[menu addItem:keyItem];
			
		} else {
			
			[menu addItem:[NSMenuItem separatorItem]];
			
		}
				
	}
	
	[sortKeyPopUpButton setMenu:menu];
	
	
	NSString *primaryKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"ImageArraySortKey"];
	
	// sort order will be done automatically with this invocation
	[self setSortKey:primaryKey];
}


- (void)setSortControlsVisible:(BOOL)flag
{
	if (flag) {
		[sortKeyPopUpButton setHidden:NO];
		[sortOrderControl setHidden:NO];
		
		[[sortKeyPopUpButton superview] addSubview:sortKeyPopUpButton positioned:NSWindowAbove relativeTo:nil];
		[[sortOrderControl superview] addSubview:sortOrderControl positioned:NSWindowAbove relativeTo:nil];
		
	} else {
		
		[sortKeyPopUpButton setHidden:YES];
		[sortOrderControl setHidden:YES];
		
		[[sortKeyPopUpButton superview] addSubview:sortKeyPopUpButton positioned:NSWindowBelow relativeTo:nil];
		[[sortOrderControl superview] addSubview:sortOrderControl positioned:NSWindowBelow relativeTo:nil];
		
		
	}
	
	
}


#pragma mark -
#pragma mark Fullscreen

- (void)enterFullScreen
{
	[fullScreenController enterFullScreen:self];
}

- (void)setShowsStacks:(BOOL)flag
{
	if (showsStacks != flag) {
		showsStacks = flag;
		[self rearrangeObjects];
	}
}



#pragma mark -
#pragma mark Drag & drop 


- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender;
{
	if ([sender draggingSource] == imageBrowser) {
		// images on top of stacks
		
		// avoid dropping stacks on other stacks
		for (id image in [self selectedObjects])
			if ([image isKindOfClass:[Stack class]])
				 return NSDragOperationNone;
		
		NSInteger index = [imageBrowser indexOfItemAtPoint:[imageBrowser convertPoint:[sender draggingLocation] fromView:nil]];
		if (index == NSNotFound)
			return NSDragOperationNone;
		
		id obj = [self imageBrowser:imageBrowser itemAtIndex:index];
		if (![obj isKindOfClass:[Stack class]])
			return NSDragOperationNone;
		
		return NSDragOperationMove;
		
	}	
	
	if ([sender draggingSource] == nil) {
		// We receive a drag from the Finder
		if ([[[sender draggingPasteboard] types] containsObject:NSFilenamesPboardType]) {
			return NSDragOperationCopy;
		} else {
			return NSDragOperationNone;	
		}
	}
	else {
		// We receive a drag from the Keywords panel
		NSPoint locationInBrowser = [imageBrowser convertPoint:[sender draggingLocation] fromView:nil];
		int indexOfDroppedOnImage = [imageBrowser indexOfItemAtPoint:locationInBrowser];
		
		// Only allow drops ON images, not between images. If a keyword is dropped between images, the index will be a large number
		if (indexOfDroppedOnImage > [[self arrangedObjects] count]) {
			return NSDragOperationNone;	
		} 
		else {
			
			return NSDragOperationCopy;
		}
	}
}


/* Drag'n drop support, accept any kind of drop */
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender;
{
	if ([sender draggingSource] == imageBrowser) {
		
		// drop on stacks -> same impl as in the draggingUpdated
		return [self draggingUpdated:sender];
	}		
	
	return NSDragOperationCopy;
}



- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender;
{	
	if ([sender draggingSource] == imageBrowser) {
		
		// dragging images to a stack
		NSSet *draggedImages = [NSSet setWithArray:[self selectedObjects]];
		
		NSInteger index = [imageBrowser indexOfItemAtPoint:[imageBrowser convertPoint:[sender draggingLocation] fromView:nil]];
		Stack *stack = [self imageBrowser:imageBrowser itemAtIndex:index];
		
		// remove all images from all old albums
		NSSet *imageAlbums = [self valueForKeyPath:@"selection.@distinctUnionOfSets.albums"];
		
		for (Album *album in imageAlbums) {
			NSMutableSet *existingImages = [album mutableSetValueForKey:@"images"];
			[existingImages minusSet:draggedImages];
		}
		
		
		[stack addImages:[draggedImages allObjects]];
		
		NSError *error = nil;
		[[self managedObjectContext] save:&error];
		
#ifdef _DEBUG
		if (error)
			[[NSApplication sharedApplication] presentError:error];
#endif
				
		return YES;
		
	}
	
	else if ([sender draggingSource] == nil) {
		// We receive a drag from the Finder
		NSPasteboard *pboard = [sender draggingPasteboard];
		if ([[pboard types] containsObject:NSFilenamesPboardType]) {
			NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
            
            // don't do this -- it screws up the drag session...
            //[NSApp activateIgnoringOtherApps:YES];
            //[[[NSApplication sharedApplication] mainWindow] makeKeyAndOrderFront:self];
            
			[(SourceListTreeController *)sourceListTreeController importFiles:files];
			return YES;
		}
		return NO;
	}
	else {
		// We receive a drag from the Keywords panel
		NSPoint locationInBrowser = [imageBrowser convertPoint:[sender draggingLocation] fromView:nil];
		int indexOfDroppedOnImage = [imageBrowser indexOfItemAtPoint:locationInBrowser];
		//NSLog(@"Dropped on image: %d", [imageBrowser indexOfItemAtPoint:locationInBrowser]);
		
		// Get the image the keywords were dropped on
		Image *image = [[self arrangedObjects] objectAtIndex:indexOfDroppedOnImage];
		
		// Get the keywords that are dragged from the keywordTreeController
		NSMutableSet *draggedKeywords = [(KeywordTreeController *)[[sender draggingSource]delegate] draggedKeywords];
		NSArray *draggedKeywordsArray = [draggedKeywords allObjects];
		
		// Add them to the image
		[image addKeywords:draggedKeywordsArray];
		
		return YES;
	}
}


- (void)concludeDragOperation:(id < NSDraggingInfo >)sender
{
	
}


# pragma mark -
# pragma mark Manual KVO (NSNotification)


- (void)observeManualModelPropertyChange:(NSNotification *)notification
{	
	// This method is invoked whenever a notification isposted by the model objects.
	// This is done to avoid bindings when selecting: the controller should just check if the changed values belong to objects in the current
	// selection, and if so, forward it to all interested objects.
	NSDictionary *info = [notification userInfo];
	
	//id postingObject = [info objectForKey:@"source"];
	
	// the keypath, absolute from image.  This was previously addressed as self.selection.<absoluteKeyPath>
	NSString *absoluteKeyPath = [info objectForKey:@"keyPath"];
	
	//NSLog(@"received notification for %@", absoluteKeyPath);
	
	
	NSArray *registeredObservers = [manualObservers objectForKey:absoluteKeyPath];
	
	NSString *kpRelativeToObserver = [@"selection." stringByAppendingString:absoluteKeyPath];
	
	for (id observer in registeredObservers) {
		
		[observer manualObserveValueForKeyPath:kpRelativeToObserver ofObject:self];
		
	}
	
}

- (void)addManualObserver:(id)observer forKeyPath:(NSString *)keyPath
{
	// manualObsrvers is a dictionary with the keyPaths as keys.  Each keyPath has an array as value, containing the observers registered for that keyPath.
	
	NSMutableString *kpRelativeToUs = [[keyPath mutableCopy] autorelease];
	[kpRelativeToUs replaceOccurrencesOfString:@"selection." withString:@"" options:0 range:NSMakeRange(0, [keyPath length])];
	
	// the following array can be nil
	NSMutableArray *observersForKeyPath = [[[manualObservers objectForKey:kpRelativeToUs] mutableCopy] autorelease];
	
	// add the new observer
	NSArray *updatedObservers = [[NSArray arrayWithObject:observer] arrayByAddingObjectsFromArray:observersForKeyPath];
	
	[manualObservers setObject:updatedObservers forKey:kpRelativeToUs];
}

- (void)removeManualObserver:(id)observer
{
	// remove the observer from all keyPath keys which it registered for.
	NSArray *allKeyPathsBelongingToObserver = [manualObservers allKeysForObject:observer];
	
	for (NSString *key in allKeyPathsBelongingToObserver) {
		
		NSMutableArray *observersForKeyPath = [[[manualObservers objectForKey:key] mutableCopy] autorelease];
		
		[observersForKeyPath removeObject:observer];
		
		[manualObservers setObject:observersForKeyPath forKey:key];
		
	}
}


# pragma mark -
# pragma mark Automatic KVO



- (void)didChangeValueForKey:(NSString *)key
{
	if ([key isEqualToString:@"selection"] && [[self selectedObjects] count] > 0) {
		
		if ([self managedObjectContext]) {
			
			NSFetchRequest *batchFaultReq = [[[NSFetchRequest alloc] init] autorelease];
			
			[batchFaultReq setEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:[self managedObjectContext]]];
			[batchFaultReq setReturnsObjectsAsFaults:NO];
			[batchFaultReq setPredicate:[NSPredicate predicateWithFormat:@"self in %@", [self selectedObjects]]];
			
			[[self managedObjectContext] executeFetchRequest:batchFaultReq error:nil];
			
		}
		
	}
	
	[super didChangeValueForKey:key];
	
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
{
	// The user is changing the selection
	if ([keyPath isEqualTo:@"selection"]) {
		
		if ([fullScreenController isInFullScreen]) {
			[imageBrowser setSelectionIndexes:[self selectionIndexes] byExtendingSelection:NO];	
			if ([[self selectionIndexes] count] > 0)
				[imageBrowser scrollIndexToVisible:[[self selectionIndexes] firstIndex]];
		}
				
		// turn off the MD edit mode, if it is turned on.
		[[mdController metadataView] endEditingMode:self];
		
	}
	if ([keyPath isEqualTo:@"arrangedObjects"]) {
		@try {
			[imageBrowser reloadData];
		} @catch (NSException *e) {
			NSLog(@"catching IKImageBrowser bug:\n		%@", [e description]);
		}
		
	} else {
		// SUPER OBSERVEVALUE CHANGED HERE
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
	
}




# pragma mark -
# pragma mark IKImageBrowserView actions and refreshing


- (IBAction)zoomSliderDidChange:(id)sender;
{
	// Update the zoom value to scale images and redisplay
	[imageBrowser setZoomValue:[sender floatValue]];
	[imageBrowser setNeedsDisplay:YES];
}

// Called when applying changes
- (void)refreshImageBrowser;
{
	[imageBrowser setNeedsDisplay:YES];	
}


// Called when undoing changes
- (void)refreshAndReloadImageBrowser;
{
	[imageBrowser setNeedsDisplay:YES];	
	for (Image *image in [self selectedObjects]) {
		[self updateThumbnailForImage:image];
	}
}


# pragma mark -
# pragma mark Searching / filter predicates

- (void)_updateSearchFieldCategories
{	
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] searchAttributeKeyPaths]) {
		
		NSMenuItem *item = nil;
		
		if ((id)keyPath != [NSNull null]) {
			
			item  = [[[NSMenuItem alloc] initWithTitle:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath]
												action:@selector(changeSearchCategory:) 
										 keyEquivalent:@""] autorelease];
			[item setTarget:self];
			[item setRepresentedObject:keyPath];
			
		}
		else {
			
			item = [NSMenuItem separatorItem];
			
		}
		
		[menu addItem:item];
		
	}
	
	[[browserSearchField cell] setSearchMenuTemplate:menu];
	
	[self changeSearchCategory:[menu itemAtIndex:0]];
	
}



- (void)changeSearchCategory:(id)sender
{
	NSMenuItem *item = (NSMenuItem *)sender;
	
	NSMenu *menuCopy = [[[item menu] copy] autorelease];
		
	for (NSMenuItem *otherItem in [menuCopy itemArray])
		[otherItem setState:NSOffState];		

	
	[[menuCopy itemAtIndex:[[item menu] indexOfItem:item]] setState:NSOnState];
	
	[[browserSearchField cell] setSearchMenuTemplate:menuCopy];
		
	[[browserSearchField cell] setPlaceholderString:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:[item representedObject]]];
	
	
	[self changeSearchString:browserSearchField];
}

- (IBAction)changeSearchString:(id)sender;
{
	NSString *string = [(NSSearchField *)sender stringValue];
	
	NSMenuItem *selectedItem = nil;
	for (NSMenuItem *item in [[[(NSSearchField *)sender cell] searchMenuTemplate] itemArray])
		if ([item state] == NSOnState)
			selectedItem = item;
	
	if (![string length] || !selectedItem)
		[self setFilterPredicate:nil];
	
	else 
		[self setFilterPredicate:[[MGFilterAndSortMetadataController sharedController] searchPredicateForAttributeKeyPath:(NSString *)[selectedItem representedObject] 
																												   prefix:nil
																											 searchString:string]];
	
	
}

- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;
{
	if ([imageBrowser respondsToSelector:@selector(hideSearchBar)]) {
		
		if (filterPredicate) {
			[self setSortControlsVisible:NO];

			[imageBrowser showSearchBarWithMessage:description];
		}
		else {
			
			[self setSortControlsVisible:YES];

			[imageBrowser hideSearchBar];
			[browserSearchField setStringValue:@""];
		}
	}
	
	
	[self setFilterPredicate:filterPredicate];
}



- (IBAction)removePredicate:(id)sender;
{
	[self searchCurrentFolderWithPredicate:nil naturalLanguageString:nil];	
}


- (void)setFilterPredicate:(NSPredicate *)filterPredicate;
{
	[imageBrowser setAnimates:YES];
	
	
	if ([imageBrowser respondsToSelector:@selector(hideSearchBar)]) {
		
		if (filterPredicate == nil) {
			[imageBrowser hideSearchBar];
		}
	}
	
	[super setFilterPredicate:filterPredicate];
	
	[imageBrowser setAnimates:NO];
	
}


# pragma mark -
# pragma mark Inserting comments  

- (IBAction)addCommentForSelectedImage:(id)sender;
{
	Image *image = [[self selectedObjects]objectAtIndex:0];
	
	// We need to fake a change in our selection key, in order to immediately update the views
	[self willChangeValueForKey:@"selection"];
	[image setValue:@" " forKey:@"comment"];
	[self didChangeValueForKey:@"selection"];
	[[MGLibraryControllerInstance window] makeFirstResponder:commentTextView];
}

- (IBAction)appendBatchCommentToSelectedImages:(id)sender
{
	[NSApp beginSheet:batchCommentWindow modalForWindow:[NSApp mainWindow] modalDelegate:self didEndSelector:nil contextInfo:nil];
	
	
}

- (IBAction)endAppendBatchCommentSheet:(id)sender;
{
	if ([sender tag] == 0) {
		// OK
		NSString *separator = [[batchCommentPopupButton selectedItem] title];
		NSString *comment = [batchCommentTextField stringValue];
		
		if ([comment length] > 0)
			[self appendCommentToSelectedImages:comment withSeparator:separator];
		
	} else {
		// cancel
		
	}
	
	[NSApp endSheet:batchCommentWindow];
	[batchCommentWindow orderOut:self];
	
}


- (void)appendCommentToSelectedImages:(NSString *)comment withSeparator:(NSString *)separator
{
	NSArray *images = [self selectedObjects];
	for (Image *image in images) {
		
		NSString *oldComment = [image valueForKey:@"comment"];
		if (!oldComment || [oldComment length] == 0) {
			
			[image setValue:comment forKey:@"comment"];

		} else {
			
			NSString *separatorWithSpaces = [[@" " stringByAppendingString:separator] stringByAppendingString:@" "];
			
			[image setValue:[[oldComment stringByAppendingString:separatorWithSpaces] stringByAppendingString:comment] forKey:@"comment"];

			
		}
		
	}
}



# pragma mark -
# pragma mark Adding and removing images  


// When adding an image, make sure to add it to the Library
// CHANGED: do not do this...we might want to add images to this controller that aren't part of the library
- (void)addObject:(id)object
{
	[super addObject:object];	
}



// When deleting an image from the library, make sure to remove it from every album
- (void)remove:(id)sender;
{
	BOOL multipleSelection = ([[self selectedObjects] count] > 1);
	
	BOOL hasStack = NO;
	
	for (id image in [self selectedObjects])
		if ([image isKindOfClass:[Stack class]])
			hasStack = YES;
	
	if ([[sourceListTreeController selectedCollection] isEqualTo:[[MGLibraryControllerInstance library] libraryGroup]] ||
		[sourceListTreeController selectedItemIsDevice]) {
		
		NSString *imagesString = (multipleSelection ? @"items" : @"item");
		
		// The library folder is selected, create the alert panel and show it
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"Delete"];
        [alert addButtonWithTitle:@"Cancel"];
        [alert setMessageText:[NSString stringWithFormat:@"Are you sure you want to delete the selected %@ from the Library?", imagesString]];
        [alert setInformativeText:[NSString stringWithFormat:@"This will permanently remove the %@ from all albums and cannot be undone.%@", 
								   imagesString, hasStack ? @"\nDeleting a stack will delete all of its images." : @""]];
		[alert setShowsSuppressionButton:YES];
		[[alert suppressionButton] setTitle:@"Do not show this warning again"];
		
        [alert setAlertStyle:NSWarningAlertStyle];
		if ([[NSUserDefaults standardUserDefaults] boolForKey:@"suppressRemoveImagesWarning"] || [alert runModal] == NSAlertFirstButtonReturn) {
			
			// first, set the suppression value in the prefs (only do this if "Cancel" wasn't chosen)
			if ([[alert suppressionButton] state] == NSOnState)
				[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressRemoveImagesWarning"];
			
			
			// Get the selected images
			NSArray *selectedImages = [[[self selectedObjects]copy]autorelease];
			[self removeImagesFromLibrary:selectedImages];
		}
		
		[alert release]; 
		
	}
	else {
		if ([sourceListTreeController smartAlbumIsSelected]) {
			NSBeep();	
		}
		else {
			[super remove:sender];	
		}
	}
}



// This is a dangerous method that might lead to data loss!
// The caller of this method is responsible for presenting the user with an NSAlert!
- (void)removeImagesFromLibrary:(NSArray *)images;
{
	int countOfSelectedImages = 0;
	for (Image *image in images)
		countOfSelectedImages += [image numberOfContainedImages];
	
	// Remove the imageArrayController as an observer of its own arrangedObjects and selectedObjects
	[self removeObserver:self forKeyPath:@"arrangedObjects"];
	[self removeObserver:self forKeyPath:@"selection"];
	
	// Show a progress sheet if we are removing more than 100 images
	if (countOfSelectedImages > 100) {
		[MGLibraryControllerInstance setProgressTitle:@"Removing images..."];
		[MGLibraryControllerInstance showProgressSheet];
		[MGLibraryControllerInstance setProgress:0.00];
	}
	
	[fullScreenImageBrowser setAnimates:NO];
	
	[super remove:self];

	
	// Remove them
	int numRemovedImages = 0;
	
	[MGLibraryControllerInstance setProgress:0.0];
	[MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Removing image %d of %d...", 1, countOfSelectedImages]];
	
	for (id image in images) {

		// if its a stack, remove all contained images
		if([image isKindOfClass:[Stack class]])
			[(Stack *)image removeImages];
				
		// Remove the selected images from the managedObjectContext
		[[self managedObjectContext] deleteObject:image];
				
		// Adjust the progress, but only so the indicator updates ~100 times per removal
		numRemovedImages += [image numberOfContainedImages];
		
		if (fmod(numRemovedImages, fmax(floor((float)countOfSelectedImages/100.0), 1)) == 0.0) {
			[MGLibraryControllerInstance setProgress:(double)numRemovedImages*100.00/countOfSelectedImages];
			[MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Removing image %d of %d...", numRemovedImages, countOfSelectedImages]];
		}
	}
	
	[[self managedObjectContext] processPendingChanges];
	
	// Re-register the imageArrayController as an observer for its own arrangedObjects
	[self addObserver:self forKeyPath:@"arrangedObjects" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	[self addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	[self observeValueForKeyPath:@"arrangedObjects" ofObject:self change:nil context:NULL];
	
		
	// Hide the Progress sheet
	[MGLibraryControllerInstance hideProgressSheet];
	
}


# pragma mark -
# pragma mark Duplicating images


// This method is being invoked by the New From Master command
- (IBAction)newVersionFromMaster:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image newVersionFromOriginal];
	
	// save.
	// if we don't do this and the created images are removed again before any save occurred, the "after deletion" save wil fail.
	NSError *error;
	if (![[self managedObjectContext] save:&error])
		[[NSApplication sharedApplication] presentError:error];
}


- (IBAction)duplicate:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image duplicate];
	
	// save.
	// if we don't do this and the created images are removed again before any save occurred, the "after deletion" save wil fail.
	NSError *error;
	if (![[self managedObjectContext] save:&error])
		[[NSApplication sharedApplication] presentError:error];
}

- (IBAction)mergeChannels:(id)sender;
{
	// check if the selection is valid, otherwise raise an error message
	
	BOOL selectionOK = YES;
	
	if ([[self selectedObjects] count] == 0)
		selectionOK = NO;
	
	if ([[self selectedObjects] count] > 3) {
		
		// UPDATE 27/04/2010
		// Not needed anymore for stacks, because they are single images now
		// --> not allowed
		selectionOK = NO;		
		
	}
	
	// check if the images are of the same type (all images or all stacks with the same amt of images)
	if ([[self selectedObjects] count] < 4 && [[self selectedObjects] count] > 0) {
		
		// values for the first item
		id stackImages = [[[self selectedObjects] objectAtIndex:0] valueForKey:@"stackImages"];
		int numStackImages = [stackImages count];
		
		for (Image *img in [self selectedObjects]) {
			
			// values for the other items
			// if either changes for a certain image, they are not mergeable
			if ((stackImages == NULL) != ([img valueForKey:@"stackImages"] == NULL))
				selectionOK = NO;
			
			if (numStackImages != [[img valueForKey:@"stackImages"] count])
				selectionOK = NO;
		}
	}

	if (!selectionOK) {
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"OK"];
		[alert setMessageText:[NSString stringWithFormat:@"Macnification cannot merge the selected images"]];
		[alert setInformativeText:
		 @"You can only merge 1 to 3 images, or 1 to 3 stacks containing the same amount of images.\nEach image or stack represents one channel."];
		[alert setAlertStyle:NSWarningAlertStyle];
		if ([alert runModal] == NSAlertFirstButtonReturn) {
		}
		[alert release];
			return;
	}
	

	
	if (!channelMergeController) {
		channelMergeController = [[ChannelMergeController alloc] init];
	} 
    
    Album *selectedFolder = [sourceListTreeController selectedCollectionIsFolder] ? [sourceListTreeController selectedCollection] : nil;
    channelMergeController.destinationFolder = selectedFolder;
	
	[channelMergeController openMergeSheet:self];
}

- (IBAction)splitChannels:(id)sender;
{        
    // non-nil if a folder is selected; merged images should be added to this folder too.
    Album *selectedFolder = [sourceListTreeController selectedCollectionIsFolder] ? [sourceListTreeController selectedCollection] : nil;
    
	int nbSelectedImages = [[self selectedObjects] count];
	
    int nbExpandedImages = [[self selectedObjectsWithStacksExpanded] count];
    
	if (nbSelectedImages == 1) {
		
		NSArray *resultImages = [(Image *)[[self selectedObjects] objectAtIndex:0] splitChannels];
        
        if (selectedFolder)
            for (Image *image in resultImages)
                [[image mutableSetValueForKey:@"albums"] addObject:selectedFolder];
        
		[self rearrangeObjects];
		return;
		
	} 
    
    
    
    BOOL showProgressSheet = nbExpandedImages > 5;
    
    
    if (showProgressSheet) {
        
        [MGLibraryControllerInstance setProgressTitle:@"Splitting channels"];
        [MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Splitting image 1 from %i", nbSelectedImages]];
        [MGLibraryControllerInstance setProgress:0];
        
        [MGLibraryControllerInstance showProgressSheet];
        
    }
    
    BOOL isPartOfStack = [[[self selectedObjects] objectAtIndex:0] isPartOfStack];
    
    // disconnect the IAC
    [sourceListTreeController disconnectImageArrayController];
    
    int i = 1;
    
    // do the regular split for all images
    NSMutableArray *imageComponents = [NSMutableArray array];
    for (Image *image in [self selectedObjects]) {
        
        if (showProgressSheet) {
            
            [MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Splitting image %i from %i", i, nbSelectedImages]];
            [MGLibraryControllerInstance setProgress:100 * (float)i/(float)nbSelectedImages];
            
            
        }
        
        NSArray *comps = [image splitChannels];
        [imageComponents addObject:comps];
        i++;
    }
    
    [MGLibraryControllerInstance setProgress:-1];
    
    
    
    // create stacks if necessary
    NSMutableArray *stacks = [NSMutableArray array];
    
    if (isPartOfStack) {
        
        NSString *oldStackName = [[[[self selectedObjects] objectAtIndex:0] stack] valueForKey:@"name"];
        
        int i;
        for (i = 0; i < 3; i++) {
            
            id stack = [NSEntityDescription insertNewObjectForEntityForName:@"Stack" inManagedObjectContext:[self managedObjectContext]];
            [stack setValue:[NSString stringWithFormat:@"%@_ch%i", oldStackName, i] forKey:@"name"];
            //[stack setValue:[NSNumber numberWithInt:20] forKey:@"rank"];
            [stack setValue:[[MGLibraryControllerInstance library] projectsCollection] forKey:@"parent"];
            
            [stacks addObject:stack];
            
        }
        
    }
    
    // give the components creation dates that are slightly different so they are grouped together
    NSDate *date = [NSDate date];
    NSDate *dates[3] = {date, [[[NSDate alloc] initWithTimeInterval:.001 sinceDate:date] autorelease] , 
        [[[NSDate alloc] initWithTimeInterval:.002 sinceDate:date] autorelease]};
    
    
    for (NSArray *comps in imageComponents) {
        
        for (i = 0; i < [comps count]; i++) {
            
            [(Image *)[comps objectAtIndex:i] setValue:dates[i] forKey:@"creationDate"];
            [(Image *)[comps objectAtIndex:i] setValue:dates[i] forKey:@"importDate"];
            
            if (isPartOfStack) {
                [[stacks objectAtIndex:i] addImages:[NSArray arrayWithObject:[comps objectAtIndex:i]]];
                
                if (selectedFolder)
                    [[[stacks objectAtIndex:i] mutableSetValueForKey:@"albums"] addObject:selectedFolder];
            }
            
            else {
                
                if (selectedFolder)
                    [[[comps objectAtIndex:i] mutableSetValueForKey:@"albums"] addObject:selectedFolder];
                
            }
            
            
        }
        
    }
    
    [sourceListTreeController connectImageArrayController];
    
    if (showProgressSheet) {
        
        [MGLibraryControllerInstance hideProgressSheet];
        
    }
    
    
	
}


# pragma mark -
# pragma mark Editing / copying ROIs


- (IBAction)removeAllROIs:(id)sender;
{
	for (Image *image in [self selectedObjects]) {
		[image removeAllROIs];	
	}
	// Update te roiArrayController (otherwise the ROIs are not refreshed and we don't see anything)
	[self refreshROIArrayController];
}


- (IBAction)copyROIs:(id)sender;
{
	self.imageToCopyROIsFrom = [[self selectedObjects] objectAtIndex:0];
}



- (IBAction)pasteROIs:(id)sender;
{
	// Copy the ROIs to every selected image
	for (Image *image in [self selectedObjects]) {
		[image copyROIsFromImage:self.imageToCopyROIsFrom removeExisting:NO];	
	}
	
	// Update te roiArrayController (otherwise the ROIs are not refreshed and we don't see anything)
	[self refreshROIArrayController];
}



- (IBAction)applyROIsToEntireStack:(id)sender;
{
	BOOL stackHasRois = NO;
	
	Image *image = [[self selectedObjects] objectAtIndex:0];
	
	
	if ([image stack]) {
		// Get the stack and all images in the stack
		id stack = [image stack];
		NSMutableSet *allImagesInStack = [NSMutableSet setWithCapacity:20];
		[allImagesInStack unionSet:[stack valueForKey:@"images"]];
		// Remove the original image we are using to copy ROIs from
		[allImagesInStack removeObject:image];
		
		// Apply the ROIs to all images in the stack
		for (Image *stackImage in allImagesInStack) {
			if ([[stackImage valueForKey:@"rois"] count] > 0) {
				
				stackHasRois = YES;
				break;
			}
		}
	}
	
	BOOL replaceExisting;
	
	if (stackHasRois) {
		
		NSAlert *alert = [NSAlert alertWithMessageText:@"Keep existing ROIs in this stack?"
										 defaultButton:@"Keep" alternateButton:@"Cancel" otherButton:@"Replace" 
							 informativeTextWithFormat:@"Some images in this stack already contain ROIs. Do you want to keep the existing ROIs, or replace them with the newly copied ROIs?"];
		[alert setAlertStyle:NSWarningAlertStyle];
		
		int returnCode = [alert runModal];
		
		if (returnCode == NSAlertAlternateReturn)
			return;file://localhost/Users/peter/Desktop/Picture%202.png
		
		replaceExisting = (returnCode == NSAlertOtherReturn);
	}
	
	
	
	
	NSDate *date = [NSDate date];
	
	NSLog(@"Start copying ROIs");
	
	[[MGAppDelegateInstance sharedOperationQueue] setSuspended:YES];
	
	self.imageToCopyROIsFrom = image;
	// Check to see if the current image is part of a stack
	if ([image stack]) {
		// Get the stack and all images in the stack
		id stack = [image stack];
		NSMutableSet *allImagesInStack = [NSMutableSet setWithCapacity:20];
		[allImagesInStack unionSet:[stack valueForKey:@"images"]];
		// Remove the original image we are using to copy ROIs from
		[allImagesInStack removeObject:image];
		
		// Apply the ROIs to all images in the stack
		for (Image *stackImage in allImagesInStack) {
			[stackImage copyROIsFromImage:imageToCopyROIsFrom removeExisting:replaceExisting];	
		}

		// Update the roiArrayController (otherwise the ROIs are not refreshed and we don't see anything)
		[self refreshROIArrayController];
	}
	
	[[MGAppDelegateInstance sharedOperationQueue] setSuspended:NO];

	[[MGAppDelegateInstance sharedOperationQueue] waitUntilAllOperationsAreFinished];
	
	NSLog(@"End copying ROIs: %f seconds", [[NSDate date] timeIntervalSinceDate:date]);

}



- (void)refreshROIArrayController;
{
	// Update te roiArrayController (otherwise the ROIs are not refreshed and we don't see anything)
	if ([[self selectedObjects] count] < 2) {
		[roiArrayController setContent:[self valueForKeyPath:@"self.selection.rois"]];
	} 
	else {
		[roiArrayController setContent:[self valueForKeyPath:@"self.selection.@unionOfSets.rois"]];
	}	
}




# pragma mark -
# pragma mark Exporting and emailing


- (IBAction)exportImageMetadata:(id)sender;
{
	MGMetadataExportController *exporter = [[[MGMetadataExportController alloc] init] autorelease];
	
	[exporter setImages:[self selectedObjects]];
	[exporter showSheet];
	
	// exporter will retain itself
}





- (IBAction)openTDF:(id)sender;
{
	// Get the exportImages we will export data for. If we are working on an image in a stack, the exportImages are the entire stack
	// If we are working with individual images, the exportImages are our selectedObjects
	NSArray *exportImages;
	Image *image = [[self selectedObjects] lastObject];
	if ([image isPartOfStack]) {
		exportImages = [[[image stack]valueForKey:@"images"]allObjects];
	}
	else {
		exportImages = [self selectedObjects];
	}
	
	
	// Open it in Numbers or Excel or whatever app the user has selected in the prefs 
	NSString *tempPath = [[@"~/Library/Caches/exportedMeasurements.txt" stringByExpandingTildeInPath] uniquePath];
	if ([sender tag] == 0) {
		[[self TDFStringForImages:exportImages] writeToFile:tempPath atomically:YES encoding:NSMacOSRomanStringEncoding error:NULL];
	}
	else {
		[[self TDFStringForImagesSortedPerROI:exportImages] writeToFile:tempPath atomically:YES encoding:NSMacOSRomanStringEncoding error:NULL];
	}
	
	NSString *fullSpreadSheetPath = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"spreadsheetApplicationPath"];
	
	if (fullSpreadSheetPath) {
		
		NSString *applicationName = [[fullSpreadSheetPath lastPathComponent]stringByDeletingPathExtension];
		[[NSWorkspace sharedWorkspace] openFile:tempPath withApplication:applicationName];
	}
	else {
		
		
		[[NSAlert alertWithMessageText:@"No spreadsheet application set" defaultButton:@"OK" alternateButton:nil otherButton:nil 
			 informativeTextWithFormat:@"Macnification could not find any spreadsheet application to open the file.  You can specify an application manually in the Preferences window."] runModal];
		
	}

}



- (IBAction)exportToTDF:(id)sender;
{
	if ([self selectedObjects] > 0) {
		NSSavePanel *sp;
		int runResult;
		// Save everything to a text file
		sp = [NSSavePanel savePanel];
		[sp setRequiredFileType:@"txt"];
		runResult = [sp runModalForDirectory:[@"~/Documents" stringByExpandingTildeInPath] file:@"Exported measurements"];
		if (runResult == NSOKButton) {
			// Get the exportImages we will export data for. If we are working on an image in a stack, the exportImages are the entire stack
			// If we are working with individual images, the exportImages are our selectedObjects
			NSArray *exportImages;
			Image *image = [[self selectedObjects] lastObject];
			if ([image isPartOfStack]) {
				exportImages = [[[image stack]valueForKey:@"images"]allObjects];
			}
			else {
				exportImages = [self selectedObjects];
			}
			
			if ([sender tag] == 0) {
				if (![[self TDFStringForImages:exportImages] writeToFile:[sp filename] atomically:YES encoding:NSMacOSRomanStringEncoding error:NULL])
					NSBeep();
			} else {
				if (![[self TDFStringForImagesSortedPerROI:exportImages] writeToFile:[sp filename] atomically:YES encoding:NSMacOSRomanStringEncoding error:NULL])
					NSBeep();
			}
		}
	}
	else {
		NSRunAlertPanel(@"No images are selected", @"Macnification only exports measurements for selected images. Make sure to select at least one image.", @"OK", nil, nil);
	}	
}


- (NSString *)TDFStringForImages:(NSArray *)images;
{
	NSMutableString *exportString = [NSMutableString stringWithCapacity:500];
	[exportString appendString:[NSString stringWithFormat:@"Measurements exported from Macnification on %@", [NSCalendarDate calendarDate]]];
	[exportString appendString:@"\r"];  
	NSArray *roiProperties = [ROI ROIProperties];
	NSString *propertiesJoinedByTab = [roiProperties componentsJoinedByString:@"\t"];
	[exportString appendString:propertiesJoinedByTab];
	[exportString appendString:@"\r"];
	
	for (Image *image in images) {
		[exportString appendString:[image tabText]];
		[exportString appendString:@"\r"];
	}
	return exportString;
}



- (NSString *)TDFStringForImagesSortedPerROI:(NSArray *)images;
{
	// Get all ROIs for all images and sort these ROIs per rank and then per image
	NSMutableArray *allRoisForAllImages = [NSMutableArray arrayWithCapacity:20];
	for (Image *image in images) {
		NSSet *allRoisForImage = [image valueForKey:@"rois"];
		[allRoisForAllImages addObjectsFromArray:[allRoisForImage allObjects]];
	}
	NSSortDescriptor *sortD = [[[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES] autorelease];
	NSSortDescriptor *sortD2 = [[[NSSortDescriptor alloc] initWithKey:@"image.name" ascending:YES] autorelease];
	NSArray *sortDescriptors = [NSArray arrayWithObjects:sortD, sortD2, nil];
	NSArray *sortedRois = [allRoisForAllImages sortedArrayUsingDescriptors:sortDescriptors];
	
	// Export the ROI properties for every ROI
	NSMutableString *exportString = [NSMutableString stringWithCapacity:500];
	[exportString appendString:[NSString stringWithFormat:@"Measurements exported from Macnification on %@", [NSCalendarDate calendarDate]]];
	[exportString appendString:@"\r"];  
	NSArray *roiProperties = [ROI ROIProperties];
	NSString *propertiesJoinedByTab = [roiProperties componentsJoinedByString:@"\t"];
	[exportString appendString:propertiesJoinedByTab];
	[exportString appendString:@"\r"];
	
	int rankOfCurrentRoi = 0;
	for (ROI *roi in sortedRois) {
		if ([[roi valueForKey:@"rank"]intValue] > rankOfCurrentRoi) {
			[exportString appendString:@"\r"];
		}
		[exportString appendString:[roi tabText]];
		[exportString appendString:@"\r"];
		rankOfCurrentRoi = [[roi valueForKey:@"rank"]intValue];
	}
	return exportString;
}


// Get a folder to export image to
- (IBAction)exportImages:(id)sender;
{
    
	id selectedCollection = [sourceListTreeController selectedCollection];
	
	NSArray *imagesToInclude = nil;
	
	
	if ([[self selectedObjects] count] == 0 && selectedCollection != [MGLibraryControllerInstance library])
		imagesToInclude = [self arrangedObjectsWithStacksExpanded];
	else
		imagesToInclude = [self selectedObjectsWithStacksExpanded];	
	
	
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	[panel setCanChooseDirectories:YES];
	[panel setCanCreateDirectories:YES];
	[panel setCanChooseFiles:NO];
	[panel setAllowsMultipleSelection:NO];
	[panel setMessage:[NSString stringWithFormat:@"Export %ld images", (unsigned long)[imagesToInclude count]]];
	[panel setPrompt:@"Choose Folder"];
	
	if (!saveOptions)
		saveOptions = [[IKSaveOptions alloc] initWithImageProperties:nil imageUTType:nil];
	
	[saveOptions addSaveOptionsAccessoryViewToSavePanel:panel];
	
	[panel beginSheetForDirectory:nil 
							 file:nil 
							types:nil 
				   modalForWindow:[MGLibraryControllerInstance window] 
					modalDelegate:self
				   didEndSelector:@selector(openPanelDidEnd:returnCode:contextInfo:)
					  contextInfo:nil];
}


// Export the images to that folder
- (void)openPanelDidEnd:(NSOpenPanel *)openPanel returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{
	if (returnCode == NSOKButton) {
		
		
		NSString *folderPath = [[openPanel filenames]objectAtIndex:0];
		[openPanel close];
		[[MGLibraryControllerInstance window] makeKeyAndOrderFront:self];
		[self exportImagesToFolder:folderPath];
	}
}


- (NSArray *)exportImagesToFolder:(NSString *)folderPath;
{
	id selectedCollection = [sourceListTreeController selectedCollection];
	
	NSArray *imagesToInclude = nil;
	
	if ([[self selectedObjects] count] == 0 && selectedCollection != [MGLibraryControllerInstance library])
		imagesToInclude = [self arrangedObjectsWithStacksExpanded];
	else
		imagesToInclude = [self selectedObjectsWithStacksExpanded];	
	
	
	
	// Show the progress sheet
	[MGLibraryControllerInstance showProgressSheet];
	[MGLibraryControllerInstance setProgress:0.00];
	[MGLibraryControllerInstance setProgressMessage:@"Preparing image export"];
	
	// Prepare an NSMutableArray to store the image paths
	NSMutableArray *imagePaths = [NSMutableArray arrayWithCapacity:5];
	int i = 0;
	int numberOfImagesToExport = [imagesToInclude count];
	
	
	// Export the selected files
	for (Image *image in imagesToInclude) {
		
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		// Write the filtered image to file and write the IPTC data to file
		[imagePaths addObject:[image exportToFolderAtPath:folderPath withOptions:saveOptions]];
		
		[pool release];
		
		// Setting progress
		i++;
		[MGLibraryControllerInstance setProgress:(double)i*100.00/numberOfImagesToExport];
		[MGLibraryControllerInstance setProgressMessage:[NSString stringWithFormat:@"Exporting image %d of %d", i, numberOfImagesToExport]];
	}
	
	
	[MGLibraryControllerInstance hideProgressSheet];
	return [[imagePaths copy] autorelease];
}



- (IBAction)emailImages:(id)sender;
{
	// Create the images in a temporary folder
	NSArray *imagePaths = [self exportImagesToFolder:[@"~/Library/Caches" stringByExpandingTildeInPath]];
	// Create a string that holds an AppleScript list of image paths, as well as a string that holds an AppleScript list of image names
	NSMutableString *allPaths = [NSMutableString stringWithCapacity:40];
	NSMutableString *allCaptions = [NSMutableString stringWithCapacity:40];
	for (NSString *path in imagePaths) {
		NSString *formattedPath = [NSString stringWithFormat:@"\"%@\",", path];
		[allPaths appendString:formattedPath];
		NSString *formattedCaption = [NSString stringWithFormat:@"\"%@\",", [[path lastPathComponent]stringByDeletingPathExtension]];
		[allCaptions appendString:formattedCaption];
	}
	// Remove the trailing commas
	[allPaths deleteCharactersInRange:NSMakeRange([allPaths length]-1, 1)];
	[allCaptions deleteCharactersInRange:NSMakeRange([allCaptions length]-1, 1)];
	// Initialise and run the AppleScript
	NSAppleScript *script = [[NSAppleScript alloc] init];
	[script initWithSource:[NSString stringWithFormat:@"on build_message(subj, messageBody, attachCaptions, attachfiles)\n\ntell application \
							\"Mail\"\nset msg to make new outgoing message at beginning of outgoing messages\ntell msg\nset the subject to subj\nset the \
							content to messageBody\n\ntell content\nrepeat with i from 1 to (count of attachfiles)\nset attch to item i of attachfiles\nmake new attachment \
							with properties {file name:attch} at after the last paragraph\nset caption to item i of attachCaptions\nif i is not equal to 1 \
							then\nmake new paragraph at end with data \"\r\"\nend if\nmake new paragraph at end with data caption\nmake new paragraph at end \
							with data \"\r\"\nmake new paragraph at end with data \"\r\"\nend repeat\nend tell\nset visible to true\nactivate\nend tell\nend tell\nend \
							build_message\n\nbuild_message(\"Images from Macnification\", \"\", {%@}, {%@})", allCaptions, allPaths]];
	[script executeAndReturnError:nil];
	[script release];
	
}



# pragma mark -
# pragma mark Montage


- (IBAction)createMontage:(id)sender;
{	
	NSArray *imagesToInclude = nil;
	
	if ([[self selectedObjects] count] == 0 && [sourceListTreeController selectedCollection] != [[MGLibraryControllerInstance library] libraryGroup])
		imagesToInclude = [self arrangedObjects];
	else
		imagesToInclude = [self selectedObjects];
	
	Montage *montage = [[[Montage alloc] init] autorelease]; // retains itself
	montage.title = @"Untitled Montage";
	
	
	for (Image *image in imagesToInclude) {
		[montage addImage:image];
		//if ([self exportsWithScaleBar])
		//	[montage addImageRep:[image filteredImageRepWithScaleBar] withMetadata:[image metadata]]; 
		//else
		//	[montage addImageRep:[image filteredImageRep] withMetadata:[image metadata]]; 

	}
	
	[montage showPreviewForWindow:[NSApp mainWindow] delegate:self didEndSelector:nil];
}



# pragma mark -
# pragma mark External editing and analyzing

- (IBAction)editInExternalApplication:(id)sender;
{	
	NSString *fullPathToExternalEditor = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"externalEditorPath"];
	// Iterate every image, get its editedImagePath and feed it to the external editing application
	for (Image *image in [self selectedObjects]) {
		NSString *pathForImageToBeEdited = [image pathForEditingApplication];
		[[NSWorkspace sharedWorkspace] openFile:pathForImageToBeEdited withApplication:[fullPathToExternalEditor lastPathComponent] andDeactivate:YES];
	}
}



- (void)updateImagesModifiedByExternalEditor;
{
	if ([[self selectedObjects] count] > 0) {
		// Update every image's version so the browser knows they are externally modified
		for (Image *image in [self selectedObjects]) {
			[image updateExternallyEditedImage];
			[image increaseBrowserVersion];
		}
		[imageBrowser reloadData];
        [fullScreenImageBrowser reloadData];
	}
}




- (IBAction)analyzeInExternalApplication:(id)sender;
{
	NSString *fullPathToExternalAnalyzer = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"externalAnalyzerPath"];
	// Iterate every image, get its editedImagePath and feed it to the external editing application
	for (Image *image in [self selectedObjects]) {
		NSString *pathForImageToBeEdited = [image pathForAnalysisApplication];
		[[NSWorkspace sharedWorkspace] openFile:pathForImageToBeEdited withApplication:[fullPathToExternalAnalyzer lastPathComponent] andDeactivate:YES];
	}
}





# pragma mark -
# pragma mark Keywords


- (void)applyKeywordsToSelectedImages:(NSArray *)keywords;
{
	for (Image *image in [self selectedObjects]) {
		[image addKeywords:keywords];
	}
	if ([sourceListTreeController selectedCollectionIsFolder] || [sourceListTreeController selectedCollectionIsSmartFolder]) {
		[imageBrowser startAnimation:@"Standard" withSymbol:[NSString stringWithFormat:@"%C", (unsigned short)0x270E] forImagesAtIndexes:[self selectionIndexes]];
	}
	[self refreshImageBrowser];
}



- (IBAction)copyKeywords:(id)sender;
{
	[self setImageToCopyKeywordsFrom:[[self selectedObjects] objectAtIndex:0]];
}


- (IBAction)pasteKeywords:(id)sender;
{
	// Copy the ROIs to every selected image
	for (Image *image in [self selectedObjects])
		[image copyKeywordsFromImage:[self imageToCopyKeywordsFrom]];

		[self refreshImageBrowser];
}


- (IBAction)removeAllKeywords:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image removeAllKeywords];

	[self refreshImageBrowser];
}



# pragma mark -
# pragma mark Ratings


- (IBAction)setRating:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image setValue:[NSNumber numberWithInt:[sender tag]] forKey:@"rating"];

	if ([sourceListTreeController selectedCollectionIsFolder] || [sourceListTreeController selectedCollectionIsSmartFolder])
		[imageBrowser startAnimation:@"Standard" withSymbol:[(NSMenuItem *)sender title] forImagesAtIndexes:[self selectionIndexes]];	

	[self refreshImageBrowser];
}



# pragma mark -
# pragma mark Calibration



- (IBAction)copyCalibration:(id)sender;
{
	if ([[self selectedObjects] count] == 1)
		[self setImageToCopyCalibrationFrom:[[self selectedObjects] objectAtIndex:0]];
}

- (IBAction)applyCalibration:(id)sender;
{
	NSMenuItem *item = (NSMenuItem *)sender;
	
	id calibration = [item representedObject];
	for (Image *image in [self selectedObjects])
		[image setValue:calibration forKey:@"calibration"];
	
	

}


- (IBAction)pasteCalibration:(id)sender;
{
	if (![self imageToCopyCalibrationFrom]) return;

	// Copy the ROIs to every selected image
	for (Image *image in [self selectedObjects])
		[image copyCalibrationFromImage:[self imageToCopyCalibrationFrom]];

	[self refreshImageBrowser];
}


- (IBAction)removeCalibration:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image removeCalibration];

	[self refreshImageBrowser];
}


# pragma mark -
# pragma mark Filters


- (void)filtersDidChange;
{
	[psImageView filtersDidChange];
}


- (void)updateThumbnailForImage:(Image *)image;
{
	[image increaseBrowserVersion];
	[imageBrowser reloadData];
	[fullScreenImageBrowser reloadData];
}


- (void)updateThumbnailForImages:(NSArray *)images;
{
	for (Image *image in images)
		[image increaseBrowserVersion];
	
	[imageBrowser reloadData];
	[fullScreenImageBrowser reloadData];
}

# pragma mark -
# pragma mark Editing / copying filters

- (IBAction)copyFilters:(id)sender;
{
	[self setImageToCopyFiltersFrom:[[self selectedObjects] objectAtIndex:0]];
}


- (IBAction)pasteFilters:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image copyFiltersFromImage:imageToCopyFiltersFrom];
		
	[self updateThumbnailForImages:[self selectedObjects]];
	
	[self filtersDidChange];
}


- (IBAction)removeAllFilters:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image removeAllFilters];

	[self updateThumbnailForImages:[self selectedObjects]];
	[self filtersDidChange];
}


- (IBAction)revertToOriginal:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image revertToOriginal];
	
	[self updateThumbnailForImages:[self selectedObjects]];
}


- (IBAction)applyFiltersToEntireStack:(id)sender;
{
	if ([[self selectedObjects] count] != 1) return;
	
	Image *src = [[self selectedObjects] objectAtIndex:0];

	id stack = [src stack];
	
	if (!stack) return;
	
	
	
	NSArray *stackImages = [stack valueForKeyPath:@"stackImages.image"];
	
	
	for (Image *image in stackImages)
		if (![image isEqualTo:imageToCopyFiltersFrom])
			[image copyFiltersFromImage:src];
		
	[self updateThumbnailForImages:stackImages];
}




# pragma mark -
# pragma mark Scale bar adding and exporting

- (IBAction)setScaleBarExport:(id)sender;
{
	// handled in user defaults
	return;
}

- (BOOL)exportsWithScaleBar 
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"exportImagesWithScalebar"];
}

- (void)setExportsWithScaleBar:(BOOL)value 
{
	[[NSUserDefaults standardUserDefaults] setBool:value forKey:@"exportImagesWithScalebar"];
}


- (IBAction)removeScaleBar:(id)sender;
{
	for (Image *image in [self selectedObjects])
		[image removeScaleBar];	

	[imageBrowser reloadData];
}


- (void)startScaleBarAnimation;
{
	if ([sourceListTreeController selectedCollectionIsFolder] || [sourceListTreeController selectedCollectionIsSmartFolder])
		[imageBrowser startAnimation:@"Standard" withSymbol:[NSString stringWithFormat:@"%C", (unsigned short)0x00AD] forImagesAtIndexes:[self selectionIndexes]];	
}

# pragma mark -
# pragma mark Spotlight: selecting an image

- (void)selectSpotlightedImages:(NSArray *)images;
{
	[self setSelectedObjects:images];	
	[imageBrowser setSelectionIndexes:[self selectionIndexes] byExtendingSelection:NO];
	[imageBrowser scrollIndexToVisible:[[self selectionIndexes]firstIndex]];
	[imageBrowser startAnimation:@"Spotlight" withSymbol:nil forImagesAtIndexes:[self selectionIndexes]];
}


- (PSImageView *)fullscreenView;
{
	return psImageView;
}

- (MetadataController *)metadataController;
{
	return mdController;
}

- (void)dealloc
{
	[droppedItems release];
	[manualObservers release];
	
	[super dealloc];
}



@end
