//
//  MGHUDTextFieldCell.m
//  Filament
//
//  Created by Dennis Lorson on 18/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDTextFieldCell.h"


@implementation MGHUDTextFieldCell


- (void)awakeFromNib
{
	//[self setBackgroundColor:[NSColor colorWithCalibratedWhite:110./255. alpha:1.0]];
	[self setBordered:YES];
	[self setTextColor:[NSColor whiteColor]];
	//[self setFont:[NSFont systemFontOfSize:9]];
	
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	[super drawWithFrame:cellFrame inView:controlView];
	//[self drawInteriorWithFrame:cellFrame inView:controlView];
}

- (void)drawInteriorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	NSRect destRect = NSInsetRect(cellFrame, 1, 1);
	
	NSBezierPath *path = [NSBezierPath bezierPathWithRect:destRect];
	
	[[NSColor colorWithCalibratedWhite:91./255. alpha:1.0] set];
	[path fill];	
	
	
	NSRect whiteRect = NSInsetRect(destRect, 1, 0);
	whiteRect.size.height = 1;
	whiteRect.origin.y = NSMaxY(destRect) - 1;

	NSRect blackRect = NSInsetRect(whiteRect, -1, 0);
	blackRect.origin.y = NSMinY(destRect);
	
	[[NSColor colorWithCalibratedWhite:1.0 alpha:0.2] set];
	[[NSBezierPath bezierPathWithRect:whiteRect] fill];
	[[NSColor colorWithCalibratedWhite:0.0 alpha:0.2] set];
	[[NSBezierPath bezierPathWithRect:blackRect] fill];
	
	NSRect textFrame = cellFrame;
	textFrame.origin.y += 1;
	[super drawInteriorWithFrame:textFrame inView:controlView];

}

@end
