//
//  NSWindow_MGAdditions.m
//  Filament
//
//  Created by Dennis Lorson on 01/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "NSWindow_MGAdditions.h"


@implementation NSWindow (MGAdditions)

- (void)setContentView:(NSView *)contentView andResizeAnimated:(BOOL)animated;
{
    NSRect newFrame = [self frameRectForContentRect:[contentView bounds]];
    NSRect currentFrame = [self frame];
    
    newFrame.origin.x = currentFrame.origin.x - ((newFrame.size.width - currentFrame.size.width) * 0.5);
    newFrame.origin.y = currentFrame.origin.y - ((newFrame.size.height - currentFrame.size.height) * 0.5);

    [self setContentView:contentView];
    [self setFrame:newFrame display:YES animate:animated];
}

@end
