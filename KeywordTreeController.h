//
//  KeywordTreeController.h
//  Macnification
//
//  Created by Peter Schols on 08/12/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface KeywordTreeController : NSTreeController {
	NSMutableSet *draggedKeywords;
	IBOutlet NSOutlineView *keywordOutlineView;
}



- (NSMutableSet *)draggedKeywords;
- (void)setDraggedKeywords:(NSMutableSet *)value;




@end
