//
//  ImageFusion.m
//  ImageAlignment
//
//  Created by Dennis Lorson on 30/10/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGImageFusion.h"
#import "MGImageAlignment.h"
#import <vecLib/vBLAS.h>

//#define FUSION_DEBUG
#define FAST_FILTERS


#define IMAGING_BIAS			0.5f

#ifdef FUSION_DEBUG
#define SUBS_IMAGING_BIAS		- IMAGING_BIAS
#else
#define SUBS_IMAGING_BIAS		
#endif

#ifdef FUSION_DEBUG
#define ADD_IMAGING_BIAS		+ IMAGING_BIAS
#else
#define ADD_IMAGING_BIAS		
#endif


#define DOWN_FILTER_SCALING		1.0f//1.414213562373095f
#define UP_FILTER_SCALING		1.0f//0.707106781186547f


// LPT Filters
// ---------------------------------------------------------------------------------


uint32_t DownFilterKernelLength = 9;
uint32_t UpFilterKernelLength = 7;

const float DownFilterKernelF[9] = { 
	+0.026748757410810 * DOWN_FILTER_SCALING,
	-0.016864118442875 * DOWN_FILTER_SCALING,
	-0.078223266528988 * DOWN_FILTER_SCALING,
	+0.266864118442872 * DOWN_FILTER_SCALING,
	+0.602949018236358 * DOWN_FILTER_SCALING,
	+0.266864118442872 * DOWN_FILTER_SCALING,
	-0.078223266528988 * DOWN_FILTER_SCALING,
	-0.016864118442875 * DOWN_FILTER_SCALING,
	+0.026748757410810 * DOWN_FILTER_SCALING
};

const float UpFilterKernelF[7] = { 
	-0.091271763114249 * UP_FILTER_SCALING,
	-0.057543526228500 * UP_FILTER_SCALING,
	+0.591271763114247 * UP_FILTER_SCALING,
	+1.115087052456994 * UP_FILTER_SCALING,
	+0.591271763114247 * UP_FILTER_SCALING,
	-0.057543526228500 * UP_FILTER_SCALING,
	-0.091271763114249 * UP_FILTER_SCALING 
} ;

// SML Filters
// ---------------------------------------------------------------------------------

uint32_t SMLFilterKernelLength = 3;
uint32_t SmoothFilterKernelLength = 3;

const float SMLFilterKernelF[3] = {
	-1.,
	+2.,
	-1.
};

const float SmoothFilterKernelF[3] = {
	+0.3333333333,
	+0.3333333333,
	+0.3333333333,
};




// ---------------------------------------------------------------------------------

void IFPadImage(IFFusionImage *image);


@interface MGImageFusion (Debug)

- (void)printStatisticsForImage:(IFFusionImage *)img c0:(BOOL)c0 c1:(BOOL)c1 c2:(BOOL)c2 c3:(BOOL)c3 withName:(NSString *)name;
- (void)printStatisticsForImage:(IFFusionImage *)img channels:(BOOL[4])channels withName:(NSString *)name;
- (void)printStatisticsForChannel:(int)channel ofImage:(IFFusionImage *)img;

@end

@implementation MGImageFusion


- (id)initWithImage:(CIImage *)img referenceImage:(CIImage *)refImg;
{
	if ((self = [super init])) {
		referenceImage = [self imageFromCIImage:refImg referenceImage:refImg];
		image = [self imageFromCIImage:img referenceImage:refImg];
	}

	return self;
}

- (void) dealloc
{
	for (int i = 0; i < 4; i++) {
		free(scratchOne[i]);
		free(scratchTwo[i]);
	}

	
	// should not free these here: they are part of the LPT
	//free(referenceImage->data);
	//free(image->data);
	
	[super dealloc];
}


- (void)printStatisticsForImage:(IFFusionImage *)img c0:(BOOL)c0 c1:(BOOL)c1 c2:(BOOL)c2 c3:(BOOL)c3 withName:(NSString *)name
{
	return;

	BOOL c[4];
	c[0] = c0; c[1] = c1; c[2] = c2; c[3] = c3;
	
	[self printStatisticsForImage:img channels:c withName:name];
}

- (void)printStatisticsForImage:(IFFusionImage *)img channels:(BOOL[4])channels withName:(NSString *)name
{
	NSLog(@"---------------------------------------------------");
	NSLog(@"%@ -- Image statistics:", name);
	for (int i = 0; i < 4; i++)
		if (channels[i])
			[self printStatisticsForChannel:i ofImage:img];
	
}

- (void)printStatisticsForChannel:(int)channel ofImage:(IFFusionImage *)img
{
	double min = 1e10;
	double max = -1e10;
	double mean = 0;
	
	for (int y = 0; y < img->h; y++) {
		for (int x = 0; x < img->w; x++) {
			int index = y * img->w + x;
			float val = (img->data[channel])[index];
			min = MIN(val, min);
			max = MAX(val, max);
			mean += val;
		}
	}
	
	mean /= (float)(img->h*img->w);
	
	NSLog(@"Channel %i --- min = %2.3f --- max = %2.3f --- mean = %2.3f", channel, min, max, mean);
	
}

- (IAImage *)fusionImage
{
	IFFusionImage *fi1 = referenceImage;
	IFFusionImage *fi2 = image;
	
	[self convertImageToKRGB:fi1];
	[self padImageForSubsampling:fi1];
	
	[self convertImageToKRGB:fi2];
	[self padImageForSubsampling:fi2];
	
	//[self saveImage:fi1 withName:[NSString stringWithFormat:@"%10.3f 2", [[NSDate date] timeIntervalSince1970]]];
	[self saveImage:fi2 withName:[NSString stringWithFormat:@"input_%10.3f", [[NSDate date] timeIntervalSince1970]]];
	
	// allocate two extra buffers (16-byte aligned)
	for (int i = 0; i < 4; i++) {
		scratchOne[i] = malloc((fi1->w + 2 * MAX_CONV_PADDING) * (fi1->h + 2 * MAX_CONV_PADDING) * sizeof(IFSample));
		scratchTwo[i] = malloc((fi1->w + 2 * MAX_CONV_PADDING) * (fi1->h + 2 * MAX_CONV_PADDING) * sizeof(IFSample));
	}

	
	int nLevels = 5;
	
	IFLaplacian *lpt1 = [self constructLPTWithLevelCount:nLevels fromImage:fi1];
	IFLaplacian *lpt2 = [self constructLPTWithLevelCount:nLevels fromImage:fi2];
	
	
	for (int i = 0; i < lpt1->nLevels + 1; i++) {
		[self computeSMLForLevel:i ofLPT:lpt1];
		//[self saveImage:lpt1->images[i] withName:[NSString stringWithFormat:@"lpt1_%i", i]];
	}
	
	for (int i = 0; i < lpt2->nLevels + 1; i++) {
		[self computeSMLForLevel:i ofLPT:lpt2];
		//[self saveImage:lpt2->images[i] withName:[NSString stringWithFormat:@"lpt2_%i", i]];
	}
	
	for (int i = 0; i < lpt2->nLevels + 1; i++)  {
		[self mergeLPT:lpt1 toLPT:lpt2 onLevel:i];
		//[self saveImage:lpt2->images[i] withName:[NSString stringWithFormat:@"%10.3f", [[NSDate date] timeIntervalSince1970]]];

	}
	
	
	
	IFFusionImage *result = [self collapseLPT:lpt2];
	
	//[self saveImage:result withName:[NSString stringWithFormat:@"result %10.3f", [[NSDate date] timeIntervalSince1970]]];
	
	IAImage *unpadded = [self unpaddedImageFromImage:result size:CGSizeMake(referenceImage->wr, referenceImage->hr)];
	
	// "result" is the topmost image of lpt2, so it will be freed along with the LPT
	IFLaplacianRelease(lpt1);
	IFLaplacianRelease(lpt2);
	
	// freed along with the LPT
	referenceImage = image = NULL;
		 	
	return unpadded;
}



#pragma mark -
#pragma mark SML


- (void)computeSMLForLevel:(size_t)level ofLPT:(IFLaplacian *)lpt
{

	// will replace the alpha channel
	IFFusionImage *inImage = lpt->images[level];
	
	vImage_Buffer inBuff, outBuff, tempBuff;
	
	inBuff.width = inImage->w + 2 * MAX_CONV_PADDING;
	inBuff.height = inImage->h + 2 * MAX_CONV_PADDING;
	inBuff.rowBytes = (inImage->w + 2 * MAX_CONV_PADDING) * sizeof(IFSample);
	inBuff.data = inImage->data[3];
	
	outBuff.width = inBuff.width;
	outBuff.height = inBuff.height;
	outBuff.rowBytes = inBuff.rowBytes;
	outBuff.data = scratchOne[0];
	
	tempBuff.width = inBuff.width;
	tempBuff.height = inBuff.height;
	tempBuff.rowBytes = inBuff.rowBytes;
	tempBuff.data = scratchTwo[0];
	
	IFPadImage(inImage);
	
	
	// convolve to the two buffers
	vImageConvolve_PlanarF(&inBuff, &tempBuff, NULL, ROI_OFFSET, ROI_OFFSET, SMLFilterKernelF, SMLFilterKernelLength, 1, 0, kvImageCopyInPlace);
	vImageConvolve_PlanarF(&tempBuff, &outBuff , NULL, ROI_OFFSET, ROI_OFFSET, SMLFilterKernelF, 1, SMLFilterKernelLength, 0, kvImageCopyInPlace);
	
	// add the result in one of the buffers
	for (int y = 0; y < inBuff.height; y++) {
		for (int x = 0; x < inBuff.width; x++) {
			long index = (inBuff.width * y + x);
			((IFSample *)outBuff.data)[index] = ABS(((IFSample *)tempBuff.data)[index]) + ABS(((IFSample *)outBuff.data)[index]);
		}
	}
	
	inImage->data[3] = outBuff.data;
	IFPadImage(inImage);
	
	
	// convolve the result (smoothing) by a 3x3 ones kernel
	vImageConvolve_PlanarF(&outBuff, &tempBuff, NULL, ROI_OFFSET, ROI_OFFSET, SmoothFilterKernelF, SmoothFilterKernelLength, 1, 0, kvImageCopyInPlace);
	vImageConvolve_PlanarF(&tempBuff, &outBuff , NULL, ROI_OFFSET, ROI_OFFSET, SmoothFilterKernelF, 1, SmoothFilterKernelLength, 0, kvImageCopyInPlace);
	
	// put the sum of the absolutes back in inBuff, but only for one channel!
	for (int y = 0; y < inBuff.height; y++) {
		for (int x = 0; x < inBuff.width; x++) {
			long index = (inBuff.width * y + x);
			((IFSample *)inBuff.data)[index] = (((IFSample *)outBuff.data)[index]);
			
		}
	}
	
	inImage->data[3] = inBuff.data;

}



- (void)mergeLPT:(IFLaplacian *)from toLPT:(IFLaplacian *)to onLevel:(size_t)level
{
	IFFusionImage *src = from->images[level];
	IFFusionImage *dest = to->images[level];
	
	//[self saveImage:src withName:[NSString stringWithFormat:@"lpt_merge_before1_%i", level]];
	//[self saveImage:dest withName:[NSString stringWithFormat:@"lpt_merge_before2_%i", level]];

	
	IFSample **srcData = src->data;
	IFSample **dstData = dest->data;
	
	int w = src->w + 2 * MAX_CONV_PADDING;
	int h = src->h + 2 * MAX_CONV_PADDING;
	
	//NSLog(@"h == %i", h);
	
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			
			int index = (y * w + x);
			
			float Fk = srcData[3][index] / (srcData[3][index] + dstData[3][index]);
						
			if (Fk < 0.4)
				Fk = 0.;
			else if (Fk >= 0.6)
				Fk = 1.;
			
			Fk = MIN(MAX(Fk, 0.0), 1.0);
									
			float Fkinv = 1. - Fk;
							
			dstData[0][index] = srcData[0][index] * Fk + dstData[0][index] * Fkinv;
			dstData[1][index] = srcData[1][index] * Fk + dstData[1][index] * Fkinv;
			dstData[2][index] = srcData[2][index] * Fk + dstData[2][index] * Fkinv;
			
		}
	}
	
	//[self saveImage:dest withName:[NSString stringWithFormat:@"lpt_merge_%i", level]];
	
	//IFPadImage(dest);
	
}


#pragma mark -
#pragma mark LPT construction

- (IFLaplacian *)constructLPTWithLevelCount:(size_t)nLevels fromImage:(IFFusionImage *)img;
{
	IFLaplacian *lpt = malloc(sizeof(IFLaplacian));
	lpt->nLevels = nLevels;
	lpt->images[0] = img;
	
	for (int i = 0; i < nLevels; i++)
		[self performConstructionStep:i onLPT:lpt];
	
	return lpt;
}

- (void)performConstructionStep:(size_t)stepIndex onLPT:(IFLaplacian *)lpt;
{
	// one step does the following:
	//		- take the LP version (call it x) from the LPT at stepIndex
	//		- convolve x twice and subsample; put this version on the next index of the LPT
	//		- supersample the subsampled version and convolve twice again (with another filter)
	//		- subtract this from x and replace x (in the LPT) by the result
	IFFusionImage *currentImage = lpt->images[stepIndex];
	
	[self printStatisticsForImage:currentImage c0:YES c1:YES c2:YES c3:YES withName:[NSString stringWithFormat:@"LPT lowpass at index %i", (int)stepIndex]];
	
	
	// buffer vars reused for multiple vImage ops
	vImage_Buffer inBuff[4], outBuff[4], tempBuff[4];
	
	// input buffer == inImage, others == temp allocated
	
	for (int i = 0; i < 4; i++) {
		
		inBuff[i].width = currentImage->w + 2 * MAX_CONV_PADDING;
		inBuff[i].height = currentImage->h + 2 * MAX_CONV_PADDING;
		inBuff[i].rowBytes = (currentImage->w + 2 * MAX_CONV_PADDING) * sizeof(IFSample);
		inBuff[i].data = currentImage->data[i];
		
		outBuff[i].width = inBuff[i].width;
		outBuff[i].height = inBuff[i].height;
		outBuff[i].rowBytes = inBuff[i].rowBytes;
		outBuff[i].data = scratchOne[i];
		
		tempBuff[i].width = inBuff[i].width;
		tempBuff[i].height = inBuff[i].height;
		tempBuff[i].rowBytes = inBuff[i].rowBytes;
		tempBuff[i].data = scratchTwo[i];
		
	}


	// --------------------------------------------------------------------------------------------
	// FILTER AND DOWNSAMPLE
	// --------------------------------------------------------------------------------------------
	
	//[self saveImage:currentImage withName:[NSString stringWithFormat:@"C_start_%i", stepIndex]];

	
	// convolve horizontal, vertical
	for (int i = 0; i < 4; i++) {
		vImageConvolve_PlanarF(&inBuff[i], &tempBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, DownFilterKernelF, DownFilterKernelLength, 1, 0, kvImageCopyInPlace);
		vImageConvolve_PlanarF(&tempBuff[i], &outBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, DownFilterKernelF, 1, DownFilterKernelLength, 0, kvImageCopyInPlace);
		
	}
	
	IFPadImage(currentImage);
	
	for (int i = 0; i < 4; i++)
		currentImage->data[i] = outBuff[i].data;
	
	//[self saveImage:currentImage withName:[NSString stringWithFormat:@"C_blurred_%i", stepIndex]];

	
	// the image is blurred, now set the 3/4th of the samples to zero and copy the other ones to the next level
	
	// create next level image
	IFFusionImage *nextLevelImage = malloc(sizeof(IFFusionImage));
	lpt->images[stepIndex+1] = nextLevelImage;
	nextLevelImage->w = currentImage->w/2;
	nextLevelImage->h = currentImage->h/2;
	nextLevelImage->wr = currentImage->wr/2;
	nextLevelImage->hr = currentImage->hr/2;
	//posix_memalign((void **)&(nextLevelImage->data), 16, 4 * (nextLevelImage->w + 2 * MAX_CONV_PADDING) * (nextLevelImage->h + 2 * MAX_CONV_PADDING) * sizeof(IFSample));
	for (int i = 0; i < 4; i++)
		nextLevelImage->data[i] = calloc((nextLevelImage->w + 2 * MAX_CONV_PADDING) * (nextLevelImage->h + 2 * MAX_CONV_PADDING), sizeof(IFSample));
	
	// iterate over the current image
	int padding = MAX_CONV_PADDING;
	
	for (int y = 0; y < currentImage->h; y++) {
		for (int x = 0; x < currentImage->w; x++) {
			
			int indexImg = ((y + padding) * (currentImage->w + 2 * padding) + (x + padding));
						
			if (x % 2 && y % 2) {
				// copy over and leave alone
				int indexSubImg = ((y/2 + padding) * (nextLevelImage->w + 2 * padding) + (x/2 + padding));
								
				nextLevelImage->data[0][indexSubImg] = currentImage->data[0][indexImg];
				nextLevelImage->data[1][indexSubImg] = currentImage->data[1][indexImg];
				nextLevelImage->data[2][indexSubImg] = currentImage->data[2][indexImg];
				nextLevelImage->data[3][indexSubImg] = currentImage->data[3][indexImg];

			} else {
				// set to zero
				currentImage->data[0][indexImg] = 0.f;
				currentImage->data[1][indexImg] = 0.f;
				currentImage->data[2][indexImg] = 0.f;
				currentImage->data[3][indexImg] = 0.f;
			}
			
		}
	}
	IFPadImage(nextLevelImage);
	
	[self saveImage:nextLevelImage withName:[NSString stringWithFormat:@"C_nextlevel_%i", (int)stepIndex]];

	
	// decimated image should be padded too for next blur op
	IFPadImage(currentImage);

	//[self saveImage:currentImage withName:[NSString stringWithFormat:@"C_decimated_%i", stepIndex]];

	
	// --------------------------------------------------------------------------------------------
	// UPSAMPLE AND FILTER
	// --------------------------------------------------------------------------------------------
	
	// the dimensions from the old image buffers are still valid; the data from the downsampled image is still in outBuff
	for (int i = 0; i < 4; i++) {
		vImageConvolve_PlanarF(&outBuff[i], &tempBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, UpFilterKernelF, UpFilterKernelLength, 1, 0, kvImageCopyInPlace);
		vImageConvolve_PlanarF(&tempBuff[i], &outBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, UpFilterKernelF, 1, UpFilterKernelLength, 0, kvImageCopyInPlace);	
	}
	
	//[self saveImage:currentImage withName:[NSString stringWithFormat:@"C_upsampled_%i", stepIndex]];

	
	// --------------------------------------------------------------------------------------------
	// COMPUTE LAPLACIAN
	// --------------------------------------------------------------------------------------------
	
	// just subtract the upsampled version (in outBuff) from the original (which is still in inBuff)
	for (int y = 0; y < currentImage->h + 2 * MAX_CONV_PADDING; y++) {

		for (int x = 0; x < currentImage->w + 2 * MAX_CONV_PADDING; x++) {
			
			int index = (y * (currentImage->w + 2 * MAX_CONV_PADDING) + x);

			((IFSample *)inBuff[0].data)[index] -= ((IFSample *)outBuff[0].data)[index] SUBS_IMAGING_BIAS;
			((IFSample *)inBuff[1].data)[index] -= ((IFSample *)outBuff[1].data)[index] SUBS_IMAGING_BIAS;
			((IFSample *)inBuff[2].data)[index] -= ((IFSample *)outBuff[2].data)[index] SUBS_IMAGING_BIAS;
			((IFSample *)inBuff[3].data)[index] -= ((IFSample *)outBuff[3].data)[index] SUBS_IMAGING_BIAS;
			
		}
	}
	
	for (int i = 0; i < 4; i++)
		currentImage->data[i] = inBuff[i].data;
	
	
	[self printStatisticsForImage:currentImage c0:YES c1:YES c2:YES c3:YES withName:[NSString stringWithFormat:@"LPT highpass at index %i", (int)stepIndex]];

	
	//[self saveImage:currentImage withName:[NSString stringWithFormat:@"C_laplacian_%i", stepIndex]];
	
	

}




#pragma mark -
#pragma mark LPT collapse

- (IFFusionImage *)collapseLPT:(IFLaplacian *)lpt;
{
	//[self saveImage:lpt->images[lpt->nLevels] withName:[NSString stringWithFormat:@"%10.3f", [[NSDate date] timeIntervalSince1970]]];

	// start with the highest level (lowest res) and decrease
	for (int i = lpt->nLevels - 1; i > -1; i--) {
		[self performCollapseStep:i onLPT:lpt];
		//[self saveImage:lpt->images[i] withName:[NSString stringWithFormat:@"collapse_%10.3f", [[NSDate date] timeIntervalSince1970]]];
	}
	
	return lpt->images[0];
}

- (void)performCollapseStep:(size_t)stepIndex onLPT:(IFLaplacian *)lpt;
{
	// INDICES:
	// -the lowpass image we want is always one step further down the pyramid (stepIndex+1)
	// -the detail image is at stepIndex
	// PADDING:
	// -all images have their padding space already
	// -the lowpass image should be padded at this point; the previous collapse step should have done it at the end!
	// -the highpass image isn't padded yet
	
	IFFusionImage *lowpass = lpt->images[stepIndex+1];
	IFFusionImage *highpass = lpt->images[stepIndex];
	
	//[self saveImage:lowpass withName:[NSString stringWithFormat:@"R_restored_%i", stepIndex]];
	
	
	// BUFFERS
	// input buffer == highpass, others == temp allocated (size of highpass)
	vImage_Buffer inBuff[4], outBuff[4], tempBuff[4];

	for (int i = 0; i < 3; i++) {
		
		inBuff[i].width = highpass->w + 2 * MAX_CONV_PADDING;
		inBuff[i].height = highpass->h + 2 * MAX_CONV_PADDING;
		inBuff[i].rowBytes = (highpass->w + 2 * MAX_CONV_PADDING) * sizeof(IFSample);
		inBuff[i].data = highpass->data[i];
		
		outBuff[i].width = inBuff[i].width;
		outBuff[i].height = inBuff[i].height;
		outBuff[i].rowBytes = inBuff[i].rowBytes;
		outBuff[i].data = scratchOne[i];
		
		tempBuff[i].width = inBuff[i].width;
		tempBuff[i].height = inBuff[i].height;
		tempBuff[i].rowBytes = inBuff[i].rowBytes;
		tempBuff[i].data = scratchTwo[i];
		
		
	}

	
	
	// --------------------------------------------------------------------------------------------
	// FILTER (DOWN) AND SUBTRACT FROM LOWPASS
	// --------------------------------------------------------------------------------------------
	
	IFPadImage(highpass);
	
	//[self saveImage:highpass withName:[NSString stringWithFormat:@"R_start_%i", stepIndex]];

	
	// convolve highpass with the downsampling kernel -- inBuff->tempBuf->outBuff
	for (int i = 0; i < 3; i++) {
		vImageConvolve_PlanarF(&inBuff[i], &tempBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, DownFilterKernelF, DownFilterKernelLength, 1, 0, kvImageCopyInPlace);
		vImageConvolve_PlanarF(&tempBuff[i], &outBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, DownFilterKernelF, 1, DownFilterKernelLength, 0, kvImageCopyInPlace);
		
		highpass->data[i] = outBuff[i].data;
	}

	
	
	//[self saveImage:highpass withName:[NSString stringWithFormat:@"R_filtered_%i", stepIndex]];
	//[self saveImage:lowpass withName:[NSString stringWithFormat:@"R_lp_%i", stepIndex]];

	// subtract the lowpass image from the blurred highpass image (only for 1/4th of the pixels -- set the others to zero!
	int padding = MAX_CONV_PADDING;
	
	for (int y = 0; y < highpass->h; y++) {
		for (int x = 0; x < highpass->w; x++) {
			
			int superImgIndex = ((y + padding) * (highpass->w + 2 * padding) + (x + padding));
						
			if (x % 2 && y % 2) {
				// subtract the highpass pixel from the lowpass one (and save in the highpass image)
				int subImgIndex = ((y/2 + padding) * (lowpass->w + 2 * padding) + (x/2 + padding));
		
				highpass->data[0][superImgIndex] = lowpass->data[0][subImgIndex] - highpass->data[0][superImgIndex] ADD_IMAGING_BIAS;
				highpass->data[1][superImgIndex] = lowpass->data[1][subImgIndex] - highpass->data[1][superImgIndex] ADD_IMAGING_BIAS;
				highpass->data[2][superImgIndex] = lowpass->data[2][subImgIndex] - highpass->data[2][superImgIndex] ADD_IMAGING_BIAS;

			} else {
				// set to zero
				highpass->data[0][superImgIndex] = 0.f;
				highpass->data[1][superImgIndex] = 0.f;
				highpass->data[2][superImgIndex] = 0.f;
				highpass->data[3][superImgIndex] = 0.f;
			}
			
		}
	}
	
	
	// --------------------------------------------------------------------------------------------
	// FILTER (UP) 
	// --------------------------------------------------------------------------------------------
	
	IFPadImage(highpass);
	
	//[self saveImage:highpass withName:[NSString stringWithFormat:@"R_subtracted_%i", stepIndex]];

	
	// convolve highpass with the upsampling kernel -- outBuff->tempBuf->outBuff
	for (int i = 0; i < 3; i ++) {
		vImageConvolve_PlanarF(&outBuff[i], &tempBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, UpFilterKernelF, UpFilterKernelLength, 1, 0, kvImageCopyInPlace);
		vImageConvolve_PlanarF(&tempBuff[i], &outBuff[i], NULL, ROI_OFFSET, ROI_OFFSET, UpFilterKernelF, 1, UpFilterKernelLength, 0, kvImageCopyInPlace);
	}

	
	//[self saveImage:highpass withName:[NSString stringWithFormat:@"R_filteredup_%i", stepIndex]];

	
	// set the highpass image back to the inbuff
	for (int i = 0; i < 3; i ++)
		highpass->data[i] = inBuff[i].data;
	
	// add the image from outBuff to the original highpass image (still in inBuff)
	for (int y = 0; y < highpass->h; y++) {
		for (int x = 0; x < highpass->w; x++) {
			
			int originalIndex = ((y + padding) * (highpass->w + 2 * padding) + (x + padding));
			int outBuffIndex = ((y + padding) * (highpass->w + 2 * padding) + (x + padding));
			
			highpass->data[0][originalIndex] -= ADD_IMAGING_BIAS - ((IFSample *)outBuff[0].data)[outBuffIndex];
			highpass->data[1][originalIndex] -= ADD_IMAGING_BIAS - ((IFSample *)outBuff[1].data)[outBuffIndex];
			highpass->data[2][originalIndex] -= ADD_IMAGING_BIAS - ((IFSample *)outBuff[2].data)[outBuffIndex];

			
			//pOriginal[0] = MIN(MAX(pOriginal[0] - IMAGING_BIAS + pOutBuff[0], 0.0), 1.0);
			//pOriginal[1] = MIN(MAX(pOriginal[1] - IMAGING_BIAS + pOutBuff[1], 0.0), 1.0);
			//pOriginal[2] = MIN(MAX(pOriginal[2] - IMAGING_BIAS + pOutBuff[2], 0.0), 1.0);

			
		}
	}
	
	//[self saveImage:highpass withName:[NSString stringWithFormat:@"R_filteredup2_%i", stepIndex]];
	
	// do not forget to pad the resulting image one last time (for the next step)
	IFPadImage(highpass);
	
	
	//if (stepIndex == 0)
		[self saveImage:highpass withName:[NSString stringWithFormat:@"R_restored_%i", (int)(stepIndex + 1)]];


}


#pragma mark -
#pragma mark Conversion


- (void)convertImageToKRGB:(IFFusionImage *)img;
{
	// use the A channel for the image luminance
	for (int y = 0; y < img->h; y++) {
		
		long coord = y * img->w;
		
		for (int x = 0; x < img->w; x++) {
			
			int index = (coord + x);
			img->data[3][index] = (img->data[0][index] + img->data[1][index] + img->data[2][index]) * 0.333333333333f;
		
		}
	}
}

- (int)padImageForSubsampling:(IFFusionImage *)img
{
	// In order for the LPT to succeed, we need to be able to downsample by 2 until we reach a certain minimum size (for the lowpass band).
	// Hence, we need to determine this minimum size and pad the image so this iterative downsampling can be performed.
	
	// ALSO:
	// Secondly, pads the image for the convolution kernel.  There is a separate routine for this, but it's wasteful to allocate the image space twice
	// so we do it in the same run.  This is only done for the full size image (subsequent levels are padded by the LPT methods)
	
	// determine the minimum size, the smallest level will be between minSize and minSize/2
	int minimumSize = 8;
	int nLevels = 0;
	float currentPixelsX = img->wr;
	float currentPixelsY = img->hr;
	float minCurrentPixels = MIN(currentPixelsX, currentPixelsY);
		
	while (minCurrentPixels > minimumSize + 1e-5) {
		minCurrentPixels /= 2;
		currentPixelsX /= 2;
		currentPixelsY /= 2;
		nLevels++;
	}
	
	//if (nLevels == 0 || currentPixelsX == (float)image->w || currentPixelsY == (float)image->h)
	//return nLevels;
	
	
	int newPixelsX = (int)ceilf(currentPixelsX) << nLevels;
	int newPixelsY = (int)ceilf(currentPixelsY) << nLevels;
	
	// save the padded height
	img->w = newPixelsX;
	img->h = newPixelsY;
	
	//NSLog(@"padding image to %ix%i", newPixelsX, newPixelsY);
	
	//int paddedX = newPixelsX - image->w;
	//int paddedY = newPixelsY - image->h;
	
	// create the padded image, copy the pixels into it but make sure to make the padded area symmetric!
	
	int padding = MAX_CONV_PADDING;
		
	// DO NOT USE malloc/posix_memalign here, we _NEED_ empty memory here!!!
	//IFSample *newImage;
	//posix_memalign((void **)&newImage, 16, sizeof(IFSample) * 4 * (img->w + 2 * padding) * (img->h + 2 * padding));
	IFSample *newImage[4];
	for (int i = 0; i < 4; i++)
		newImage[i] = calloc((img->w + 2 * padding) * (img->h + 2 * padding), sizeof(IFSample));
		
	for (int y = -padding; y < img->h + padding; y++) {
		
		for (int x = -padding; x < img->w + padding; x++) {
			
			// copy the image itself
					
			int xCoord, yCoord;
			
			if (x < 0)								// on the left of the src image -> reflect
				xCoord = -x;
			else if (x >= img->wr)				// on the right of the src image -> reflect
				xCoord = 2 * img->wr - x - 2;
			else
				xCoord = x;
			
			if (y < 0)								// on the bottom of the src image -> reflect
				yCoord = -y;
			else if (y >= img->hr)				// on the top of the src image -> reflect
				yCoord = 2 * img->hr - y - 2;
			else
				yCoord = y;
			
			
			int newIndex = ((y + padding)*(img->w + 2 * padding)  + (x+padding));
			int oldIndex = (yCoord*img->wr+ xCoord);
			
			newImage[0][newIndex] = img->data[0][oldIndex];
			newImage[1][newIndex] = img->data[1][oldIndex];
			newImage[2][newIndex] = img->data[2][oldIndex];
			newImage[3][newIndex] = img->data[3][oldIndex];
			
				
		}
	}
	
	// replace the old image by the new one
	for (int i = 0; i < 4; i++) {
		free(img->data[i]);
		img->data[i] = newImage[i];
	}

	return nLevels;
}




#pragma mark -
#pragma mark Utilities

void IFPadImage(IFFusionImage *image)
{
	// Does not allocate anything (assumes this has been done already), but changes the pixel values.
	
	int w = image->w;
	int h = image->h;
	IFSample *data[4];
	for (int i = 0; i < 4; i++)
		data[i] = image->data[i];
	
	int padding = MAX_CONV_PADDING;
	
	for (int y = -padding; y < h + padding; y++) {
		
		for (int x = -padding; x < w + padding; x++) {
			
			if (y > 0 && y < h && x > 0 && x < w)
				continue;
								
			int xCoord, yCoord;
			
			if (x < 0)								// on the left of the src image -> reflect
				xCoord = -x;
			else if (x >= w)				// on the right of the src image -> reflect
				xCoord = 2 * w - x - 2;
			else
				xCoord = x;
			
			if (y < 0)								// on the bottom of the src image -> reflect
				yCoord = -y;
			else if (y >= h)				// on the top of the src image -> reflect
				yCoord = 2 * h - y - 2;
			else
				yCoord = y;
			
			
			int newIndex = ((y + padding)*(w + 2 * padding) + (x + padding));
			int oldIndex = ((yCoord + padding)*(w + 2 * padding)+ (xCoord + padding));
			
			data[0][newIndex] = data[0][oldIndex];
			data[1][newIndex] = data[1][oldIndex];
			data[2][newIndex] = data[2][oldIndex];
			data[3][newIndex] = data[3][oldIndex];

			
		}
	}
	
}




- (IAImage *)unpaddedImageFromImage:(IFFusionImage *)img size:(CGSize)size
{	
	int w = (int)size.width;
	int h = (int)size.height;
	
	IAImage *copy = (IAImage *)malloc(sizeof(IAImage));
	copy->h = h;
	copy->w = w;
	
	// remove the padding
	
	copy->data = calloc(4 * w * h, sizeof(unsigned char));
	
	unsigned char *newData = copy->data;
	
	
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			
			int newIndex = 4 * (y * w + x);
			int oldIndex = ((y + MAX_CONV_PADDING) * (img->w + 2 * MAX_CONV_PADDING) + (x + MAX_CONV_PADDING));
			
			// switch RGBAFFFF to ARGB8888
			newData[newIndex + 0] = 255;//MIN(MAX(255. * img->data[oldIndex + 3], 0), 255);
			newData[newIndex + 1] = MIN(MAX(255. * (img->data[0])[oldIndex], 0), 255);
			newData[newIndex + 2] = MIN(MAX(255. * (img->data[1])[oldIndex], 0), 255);
			newData[newIndex + 3] = MIN(MAX(255. * (img->data[2])[oldIndex], 0), 255);
			
		}
	}
	
	return copy;
}


- (void)saveImage:(IFFusionImage *)anImg withName:(NSString *)name
{
	return;
	
	IFSample *tempData = malloc(4 * sizeof(IFSample) * (anImg->w + 2 * MAX_CONV_PADDING) * (anImg->h + 2 * MAX_CONV_PADDING));
	
	int maxIndex = (anImg->w + 2 * MAX_CONV_PADDING) * (anImg->h + 2 * MAX_CONV_PADDING);
	for (int i = 0; i < maxIndex; i++) {
		tempData[i * 4 + 0] = anImg->data[0][i];
		tempData[i * 4 + 1] = anImg->data[1][i];
		tempData[i * 4 + 2] = anImg->data[2][i];
		tempData[i * 4 + 3] = anImg->data[3][i];
	}
	
	
	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, 
															  tempData, 
															  sizeof(IFSample) * 4 * (anImg->w + 2 * MAX_CONV_PADDING) * (anImg->h + 2 * MAX_CONV_PADDING),
															  NULL);
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	
	
	CGImageRef img = CGImageCreate(anImg->w + 2 * MAX_CONV_PADDING,
								   anImg->h + 2 * MAX_CONV_PADDING,
								   8 * sizeof(IFSample),
								   8 * 4 * sizeof(IFSample),
								   sizeof(IFSample) * 4 * (anImg->w + 2 * MAX_CONV_PADDING),
								   colorspace,
								   kCGImageAlphaNoneSkipLast | kCGBitmapFloatComponents | kCGBitmapByteOrder32Little, 
								   provider,
								   NULL,
								   NO,
								   kCGRenderingIntentDefault);
	CGColorSpaceRelease(colorspace);
	
	
	CGImageDestinationRef dest = CGImageDestinationCreateWithURL((CFURLRef)[NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Desktop/fusionold/%@.tiff", name]],
																 kUTTypeTIFF,
																 1,
																 NULL);
	
	CGImageDestinationAddImage(dest, img, NULL);
	
	CGImageDestinationFinalize(dest);
	
	free(tempData);
	CFRelease(dest);
	CGImageRelease(img);
	CGDataProviderRelease(provider);
	
}



- (IFFusionImage *)imageFromCIImage:(CIImage *)img referenceImage:(CIImage *)refImg
{
	CGSize refSize = [refImg extent].size;
	
	NSInteger w = (int)roundf(refSize.width);
	NSInteger h = (int)roundf(refSize.height);
	
	IFFusionImage *result = malloc(sizeof(IFFusionImage));
	result->w = w;
	result->h = h;
	result->wr = result->w;
	result->hr = result->h;
	
	for (int i = 0; i < 4; i++)
		result->data[i] = calloc(result->w * result->h, sizeof(IFSample));
	
	IFSample *tempData = calloc(4 * result->w * result->h, sizeof(IFSample));
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	
	// ARGB8888; let CG do the conversion for us
	CGContextRef ctx = CGBitmapContextCreate(tempData, 
											 result->w, 
											 result->h, 
											 8 * sizeof(IFSample),
											 4 * result->w * sizeof(IFSample),
											 colorspace, 
											 kCGImageAlphaPremultipliedLast | kCGBitmapFloatComponents | kCGBitmapByteOrder32Little);
	
	CGColorSpaceRelease(colorspace);
	
	
	CIContext *ciCtx = [CIContext contextWithCGContext:ctx options:NULL];
	
	// draw the reference image to avoid black blocks in the union ROI
	//if (!CGRectEqualToRect([img extent], [refImg extent]))
	[ciCtx drawImage:refImg inRect:CGRectMake(0,0,w,h) fromRect:CGRectMake(0,0,w,h)];
	
	CGFloat margin = 0;
	
	[ciCtx drawImage:img inRect:CGRectMake(margin,margin,w-2*margin,h-2*margin) fromRect:CGRectMake(margin,margin,w-2*margin,h-2*margin)];
	
	
	// meshed to planar
	int maxIndex = result->w * result->h;
	for (int i = 0; i < maxIndex; i++) {
		
		(result->data[0])[i] = tempData[i * 4 + 0];
		(result->data[1])[i] = tempData[i * 4 + 1];
		(result->data[2])[i] = tempData[i * 4 + 2];
		(result->data[3])[i] = tempData[i * 4 + 3];
	}
	
	CGContextRelease(ctx);
	free(tempData);
	
	return result;
	
}


@end
