//
//  MGGenericImageFileImporter.h
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGImageFileImporter.h"

@interface MGGenericImageFileImporter : MGImageFileImporter
{
	CGImageSourceRef isrc_;
}

@end
