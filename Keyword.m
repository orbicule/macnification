//
//  Keyword.m
//  Macnification
//
//  Created by Peter Schols on 14/03/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "Keyword.h"


@implementation Keyword


- (NSString *)entireAncestry;
{
	NSMutableString *ancestors = [NSMutableString stringWithCapacity:100];
	[ancestors appendString:[self valueForKey:@"name"]];
	[ancestors appendString:@" "];
	id currentKeyword = self;
	
	// Iterate the entire parent hierarchy to get all keywords
	while (currentKeyword) {
		Keyword *parent = [currentKeyword valueForKey:@"parent"];
		if (parent) {
			NSString *keyword = [parent valueForKey:@"name"];
			[ancestors appendString:keyword];
			[ancestors appendString:@" "];
			currentKeyword = parent;
		}
		else {
			break;
		}
	}
	return [[ancestors copy]autorelease];
}


// Return an NSArray of all keywords in the ancestry of this keyword
- (NSArray *)entireAncestryArray;
{
	NSMutableArray *entireAncestryArray = [NSMutableArray arrayWithCapacity:5];
	id currentKeyword = self;
	[entireAncestryArray addObject:[self valueForKey:@"name"]];
	while (currentKeyword) {
		Keyword *parent = [currentKeyword valueForKey:@"parent"];
		if (parent) {
			NSString *keyword = [parent valueForKey:@"name"];
			[entireAncestryArray addObject:keyword];
			currentKeyword = parent;
		}
		else {
			break;
		}
	}
	return [[entireAncestryArray copy]autorelease];
}

// Return an NSArray of all keywords in the ancestry of this keyword
- (NSArray *)entireAncestryObjectArray;
{
	NSMutableArray *entireAncestryArray = [NSMutableArray arrayWithCapacity:5];
	id currentKeyword = self;
	[entireAncestryArray addObject:self];
	while (currentKeyword) {
		Keyword *parent = [currentKeyword valueForKey:@"parent"];
		if (parent) {
			[entireAncestryArray addObject:parent];
			currentKeyword = parent;
		}
		else {
			break;
		}
	}
	return [[entireAncestryArray copy]autorelease];
}


- (NSString *)description
{
	return [[super description] stringByAppendingString:[NSString stringWithFormat:@"  ----  %@", [self valueForKey:@"name"]]];
	
}

@end
