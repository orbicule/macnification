//
//  SourceListTreeController.h
//  Filament
//
//  Created by Peter Schols on 14/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ImageArrayController.h"
#import "PSBackgroundView.h"
#import "StackController.h"
#import "LightTableController.h"
#import "CaptureController.h"
#import "CaptureDeviceBrowser.h"
#import "MGSourceOutlineView.h"
#import "MGCustomMetadataController.h"

@class NSCoreUIImageRep;
@class Stack;
@class MetadataController;
@class CaptureController;



//																		#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface SourceListTreeController : NSTreeController <NSOutlineViewDelegate, NSOutlineViewDataSource, CaptureDeviceBrowserDelegate, MGSourceOutlineViewDelegate, MGCustomMetadataObserver>
//																		#else
//@interface SourceListTreeController : NSTreeController <CaptureDeviceBrowserDelegate>
//																		#endif

{
	
	IBOutlet ImageArrayController	*imageArrayController; 
	IBOutlet MetadataController		*metadataController;

	IBOutlet MGSourceOutlineView	*sourceOutlineView;
	
	IBOutlet NSPredicateEditor		*predicateEditor;
	IBOutlet PSBackgroundView		*predicateView;
	
	IBOutlet NSWindow				*mainWindow;
	
	IBOutlet NSTabView				*mainTabView;
	
	IBOutlet NSView					*importAccessoryView;
	IBOutlet NSButton				*importSplitChannelsButton;
		
	NSArray *itemsBeingDragged;
	
	MAAttachedWindow *predicateWindow;
	
	StackController *stackController;
	LightTableController *lightTableController;
	CaptureController *captureController;
	
	BOOL initialSourceListExpansionDone;
	BOOL isShowingStack;
	BOOL isShowingLightTable;
	BOOL isShowingSmartAlbum;
	
	// Main items for the outline view
	// These are retained as global ivars, so that they remain the same during one application run.
	NSMutableArray *mainSourceListItems;
	NSString *allImagesGroupItem;
	NSString *recentGroupItem;
	NSString *devicesGroupItem;
	NSString *projectsGroupItem;
	
	NSArray *sortDescriptors;
	
	

	
}


// Importing
- (IBAction)importImages:(id)sender;
- (void)importFiles:(NSArray *)files;
- (void)disconnectImageArrayController;
- (void)connectImageArrayController;

// Expanding, selecting and editing collections
- (void)expandStandardGroups;
- (void)expandCollection:(id)aFolder;
- (void)selectItem:(id)aFolder;
- (void)editCollection:(id)aFolder;


// Adding collections
- (IBAction)addGroup:(id)sender;
- (IBAction)addSubCollection:(id)sender;

- (IBAction)addFolderWithSelectedImages:(id)sender;
- (IBAction)addLightTableWithSelectedImages:(id)sender;

// stacks
- (void)showStack:(Stack *)stack;
- (void)hideStack:(id)sender;
- (void)hideStack;
- (BOOL)isShowingStack;

// Status
- (Album *)selectedCollection;
- (BOOL)selectedCollectionIsGroup;
- (BOOL)selectedCollectionIsFolder;
- (BOOL)selectedCollectionIsSmartFolder;
- (BOOL)selectedCollectionIsLightTable;
- (BOOL)selectedItemIsDevice;


- (IBAction)runSlideshow:(id)sender;


// Smart Albums
- (IBAction)savePredicateWindow:(id)sender;
- (IBAction)closePredicateWindow:(id)sender;
- (NSWindow *)predicateWindow;
- (IBAction)setPredicate:(id)sender;
- (BOOL)smartAlbumIsSelected;
- (void)showPredicateWindowAtPoint:(NSPoint)point forRow:(NSInteger)row;
- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;




- (NSArray *)itemsBeingDragged;
- (void)setItemsBeingDragged:(NSArray *)value;


- (StackController *)stackController;
- (CaptureController *)captureController;


@end
