//
//  LightTablePrintContentView.h
//  LightTable
//
//	A class which handles the printing of the given (light table) content view.
//	It will perform any custom drawing needed for printing/saving to an image.
//	It doesn't need any bindings ore content updates because it gets created at print time with th right imageview positions etc...
//
//  Created by Dennis on 28/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class LightTableImageView;
@class LightTableContentView;

@interface LightTablePrintContentView : NSView
{

}


- (id)initWithContentView:(LightTableContentView *)originalView;

- (NSImage *)compositeImageFromRect:(NSRect)rect constrainToImageBounds:(BOOL)constrain sideMargins:(NSSize)margins;


@end
