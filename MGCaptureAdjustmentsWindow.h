//
//  MGCaptureAdjustmentsWindow.h
//  Filament
//
//  Created by Dennis Lorson on 05/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>



@interface MGCaptureAdjustmentsWindow : NSPanel
{
	NSPanel *arrowWindow_;
}

- (void)showWithArrowAtPoint:(NSPoint)point;
- (void)hideArrow;
- (BOOL)isAttached;


@end
