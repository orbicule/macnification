//
//  BlobController.h
//  Filament
//
//  Created by Peter Schols on 08/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

#import "MGHistogramView.h"

@class MGHistogramView;
@class MGHUDRangeSlider;
@class Image;
@class ImageArrayController;
@class PSImageView;
@class MGHUDSlider;


//																		#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface BlobController : NSWindowController <NSWindowDelegate>
//																		#else
//@interface BlobController : NSWindowController 
//																		#endif
{

	ImageArrayController *imageArrayController;
	
	// Blob analysis
	
	IBOutlet MGHUDRangeSlider *rSlider, *gSlider, *bSlider, *hSlider, *sSlider, *lSlider, *kSlider;
	IBOutlet NSView *rgbSliderContainer, *hslSliderContainer, *kSliderContainer;
	IBOutlet NSView *containerPlaceholder;
		
	IBOutlet NSPopUpButton *colorspacePopup;
	
	IBOutlet MGHUDSlider *blurSlider, *interpolationAccuracySlider;
	
	IBOutlet NSButton *borderBlobCheckbox;
	IBOutlet NSButton *innerBlobCheckbox;
	IBOutlet NSTextField *minimumBlobSizeTextField;
	IBOutlet NSProgressIndicator *blobProgressIndicator;
	IBOutlet NSTextField *unitTextField;
	
	IBOutlet NSButton *setMinimumBlobSizeActiveCheckbox, *saveBlobsButton, *helpButton;
	
	IBOutlet NSWindow *blobWindow;
	
		
	IBOutlet MGHistogramView *histogramView;
	
	IBOutlet PSImageView *fullscreenImageView;
	
	Image *lastSelectedImage;
	
	CIImage *inputImage;		// the input image (with traditional filters applied, no conversion yet)
	IHColorspace activeColorspace;
	
	CGFloat downsamplingFactor;
	
	
	CIFilter *thresholdFilter;
	BOOL thresholdFilterActive;
	
	NSOperationQueue *extractionQueue;
	
	
	NSArray *lastBlobresults;
    
    BOOL isObservingSelection;
}

@property (nonatomic, retain) CIImage *inputImage;
@property (nonatomic, retain) Image *lastSelectedImage;
@property (nonatomic) IHColorspace activeColorspace;
@property (nonatomic) BOOL thresholdFilterActive;


- (void)close;


// Blob detection

- (IBAction)loadContextHelp:(id)sender;

- (IBAction)toggleBlobDetectionWindow:(id)sender;
- (IBAction)startBlobDetection:(id)sender;
- (IBAction)saveBlobs:(id)sender;
- (NSArray *)upsampleBlobs:(NSArray *)originalBlobs;

- (void)adjustInterpolationAccuracyBounds;
- (IBAction)changeThresholdValue:(id)sender;
- (IBAction)changeColorspace:(id)sender;
- (IBAction)changeActiveChannel:(id)sender;
- (IBAction)changeBlur:(id)sender;

- (void)displaySlidersForColorspace:(IHColorspace)space;

- (void)saveAnalysisParametersToImage:(Image *)image;
- (void)resetAnalysisParameters;
- (void)loadAnalysisParametersFromImage:(Image *)image;

- (void)updateHistogram;
- (void)updateThresholdedImage;
- (void)setThresholdActive:(BOOL)flag forImage:(Image *)image;

- (CIImage *)binaryImageFromImage:(CIImage *)original;
- (CIImage *)colorConvertedImage;
- (NSBitmapImageRep *)renderCIImage:(CIImage *)image;


- (void)performCloseCleanup;

@end
