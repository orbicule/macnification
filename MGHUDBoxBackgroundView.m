//
//  MGHUDBoxBackgroundView.m
//  Filament
//
//  Created by Dennis Lorson on 13/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDBoxBackgroundView.h"


@implementation MGHUDBoxBackgroundView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
	
	[[NSColor colorWithCalibratedWhite:1.0 alpha:0.06] set];
	
	[[NSBezierPath bezierPathWithRect:[self bounds]] fill];
	
    // Drawing code here.
}

@end
