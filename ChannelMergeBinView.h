//
//  ChannelMergeBinView.h
//  Filament
//
//  Created by Dennis Lorson on 25/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@class ChannelMergeController;


typedef enum _MergeChannel {
	
	MergeChannelRed = 0,
	MergeChannelGreen = 1,
	MergeChannelBlue = 2,
	MergeChannelComposite = 3,
	
} MergeChannel;




@interface ChannelMergeBinView : NSView {
	
	MergeChannel channel;
	
	IBOutlet ChannelMergeController *controller;
	
	NSArray *binImages;
	
	CALayer *binLayer;
	
	CALayer *placeholderLayer;
	
	BOOL didRespondToMouseDrag;

}

@property(readwrite) MergeChannel channel;


- (void)setImages:(NSArray *)images;
- (NSArray *)images;

- (void)addImageLayerForCompositeImage:(NSBitmapImageRep *)composite;
- (void)removeAllImageLayers;



@end
