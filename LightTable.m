//
//  LightTable.m
//  LightTable
//
//  Created by Dennis on 13/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTable.h"

#import "Image.h"
#import "LightTableImage.h"
#import "LightTableContentView.h"


@implementation LightTable



#pragma mark LTImage insertion
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LTImage insertion
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (id)lightTableImages;
{
	return [self primitiveValueForKey:@"lightTableImages"];
}


- (void)insertImageObjects:(NSArray *)images atLocation:(NSPoint)location inView:(LightTableContentView *)theView
{
	// insert the given array of Images in the given view.
	// this method is intended to be used only when dropping images (whose pasteboard data is the Image object) from the main application into the light table content view.
	
	[theView setOriginPositionsForImages:images gridCenterPoint:location];
	
	for (LightTableImage *image in images) {
		NSMutableSet *lightTables = [image mutableSetValueForKey:@"lightTables"];
		[lightTables addObject:self];
	}
}
	
- (NSArray *)insertImages:(NSArray *)fileNames atLocation:(NSPoint)location inView:(LightTableContentView *)view
{
	// TEMPORARY: for testing using external images.
	
	// insert images by filename and return their newly created objects.
	
	// the images here will be given a default size (200, x) when inserted.
	// we need to supply their origin positions and set them, before acutally adding them to the light table (and thus its view)
	
	NSMutableArray *lightTableImages = [NSMutableArray array];
	
	for (NSString *file in fileNames) {
		
		// make our object but don't add it to the controller yet...
		
		Image *newImage = [NSEntityDescription insertNewObjectForEntityForName:@"Image" inManagedObjectContext:[self managedObjectContext]];
		[newImage setValue:file forKey:@"path"];
		LightTableImage *newLightTableImage = [NSEntityDescription insertNewObjectForEntityForName:@"LightTableImage" inManagedObjectContext:[self managedObjectContext]];
		[newLightTableImage setValue:newImage forKey:@"image"];

		[lightTableImages addObject:newLightTableImage];
	}
	
	[view setOriginPositionsForImages:lightTableImages gridCenterPoint:location];
	
	for (LightTableImage *image in lightTableImages) {
		NSMutableSet *lightTables = [image mutableSetValueForKey:@"lightTables"];
		[lightTables addObject:self];
	}
	return lightTableImages;
}


- (LightTableImage *)insertImage:(NSString *)filePath
{
	// TEMPORARY: for testing using external images.
	// inserts an image with the given path into the image array
	
	Image *newImage = [NSEntityDescription insertNewObjectForEntityForName:@"Image" inManagedObjectContext:[self managedObjectContext]];
	[newImage setValue:filePath forKey:@"path"];
	
	LightTableImage *newLightTableImage = [NSEntityDescription insertNewObjectForEntityForName:@"LightTableImage" inManagedObjectContext:[self managedObjectContext]];
	[newLightTableImage setValue:newImage forKey:@"image"];
	
	NSMutableSet *lightTables = [newLightTableImage mutableSetValueForKey:@"lightTables"];
	[lightTables addObject:self];
	
	return newLightTableImage;
}
		
- (void)insertImageObject:(id)theObject
{
	NSMutableSet *lightTables = [theObject mutableSetValueForKey:@"lightTables"];
	[lightTables addObject:self];
}


- (void)addImages:(NSArray *)images;
{
	for (Image *image in images) {
		// Create a new LightTableImage and set image as its image
		LightTableImage *newLTImage = [NSEntityDescription insertNewObjectForEntityForName:@"LightTableImage" inManagedObjectContext:[self managedObjectContext]];
		[newLTImage setValue:image forKey:@"image"];
		
		// Add the newly created LightTableImage to the Image
		[[image mutableSetValueForKey:@"lightTableImages"] addObject:newLTImage];
		
		// Set ourselves as one of the Light Tables for this LightTableImage
		NSMutableSet *lightTables = [newLTImage mutableSetValueForKey:@"lightTables"];
		[lightTables addObject:self];
		[newLTImage setValue:lightTables forKey:@"lightTables"];
		
		// Set ourselves as one of the Light Tables for this Image
		NSMutableSet *albums = [image mutableSetValueForKey:@"albums"];
		[albums addObject:self];
		[image setValue:albums forKey:@"albums"];
	}
}



- (NSImage *)iconImage;
{
	NSImage *iconImage;
	iconImage = [NSImage imageNamed:@"lightTableSmall"];
	//iconImage = [NSImage imageNamed:NSImageNameFolderBurnable];
	//iconImage = [[NSWorkspace sharedWorkspace] iconForFile:@"/etc"];
	return iconImage;
}


- (NSImage *)largeIconImage;
{
	return [NSImage imageNamed:@"lightTableLarge"];
}




@end
