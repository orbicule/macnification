//
//  Album.h
//  Macnification
//
//  Created by Peter Schols on 22/11/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import "KSExtensibleManagedObject.h"


@interface Album : KSExtensibleManagedObject 
{

}


- (Album *)parentGroup;




- (void)addImages:(NSArray *)images;
- (void)addKeywords:(NSArray *)keywords;

- (BOOL)acceptsDroppedKeywords;
- (BOOL)acceptsDroppedImages;
- (BOOL)acceptsDroppedImagesFromFinder;

- (BOOL)canHaveAlbumAsChild:(Album *)proposedChild;
- (BOOL)canHaveAlbumsAsChildren:(NSArray *)proposedChildren;

- (BOOL)canBeDragged;
- (BOOL)needsZoomSlider;
- (BOOL)canEdit;

- (NSImage *)iconImage;

- (BOOL)isLightTable;
- (BOOL)isSmartAlbum;
- (BOOL)isFolder;
- (BOOL)isGroup;


// Rank related

- (NSArray *)sortedChildren;

- (int)indexOfChild:(Album *)child;
- (Album *)childAtIndex:(int)index;

- (void)addChildToEnd:(Album *)child;
- (void)addChild:(Album *)child atIndex:(int)index;

@end
