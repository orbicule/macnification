//
//  Channel.m
//  Filament
//
//  Created by Peter Schols on 27/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "Channel.h"


@implementation Channel


- (NSString *)description;
{
	return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@", 
			[self primitiveValueForKey:@"channelName"], 
			[self primitiveValueForKey:@"contrastMethod"], 
			[self primitiveValueForKey:@"gain"], 
			[self primitiveValueForKey:@"lightSourceName"], 
			[self primitiveValueForKey:@"lightSourceType"], 
			[self primitiveValueForKey:@"medium"], 
			[self primitiveValueForKey:@"mode"], 
			[self primitiveValueForKey:@"offset"], 
			[self primitiveValueForKey:@"pinholeSize"], 
			[self primitiveValueForKey:@"power"], 
			[self primitiveValueForKey:@"wavelength"]];
}



@end
