//
//  MGImageBrowserCell.m
//  Filament
//
//  Created by Dennis Lorson on 11/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGCaptureImageBrowserCell.h"


@implementation MGCaptureImageBrowserCell



- (CALayer *)layerForType:(NSString *)type;
{
    CALayer *contentLayer = nil;
    
	if (type == IKImageBrowserCellSelectionLayer) {
				
		NSRect selectionFrame = [self selectionFrame];
		selectionFrame = NSInsetRect(selectionFrame, 0, 2);
		
		NSRect titleFrame = [self titleFrame];
		titleFrame = NSInsetRect(titleFrame, -3, 0);

		
		NSRect frame = [self selectionFrame];
	
		
		CALayer *layer = [CALayer layer]; 
		layer.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
		//layer.backgroundColor = CGColorCreateGenericRGB(1, 0, 1, 0.5);

		
		NSRect relativeSelectionFrame = selectionFrame;
		relativeSelectionFrame.origin.x -= frame.origin.x;
		relativeSelectionFrame.origin.y -= frame.origin.y;
		
		
		NSRect relativeTitleFrame = titleFrame;
		relativeTitleFrame.origin.x -= frame.origin.x;
		relativeTitleFrame.origin.y -= frame.origin.y;
		
		
		
		CALayer *selectionBGLayer = [CALayer layer];
		selectionBGLayer.backgroundColor = CGColorCreateGenericRGB(1, 1, 1, 0.65);
		selectionBGLayer.frame = NSRectToCGRect(relativeSelectionFrame);
		selectionBGLayer.cornerRadius = 7;
		
		CALayer *titleBGLayer = [CALayer layer];
		titleBGLayer.backgroundColor = CGColorCreateGenericRGB(0, 0, 1, 0.5);
		titleBGLayer.frame = NSRectToCGRect(relativeTitleFrame);
		titleBGLayer.cornerRadius = 7;

		[layer addSublayer:selectionBGLayer];
		//[layer addSublayer:titleBGLayer];
		
		
		contentLayer = layer;
		
	}	
    
    else
        contentLayer = [super layerForType:type];
    
    
    return contentLayer;
    
    CGRect parentFrame = contentLayer.frame;
    parentFrame.size.height += 10;
    
    CALayer *parentLayer = [CALayer layer];
    parentLayer.frame = parentFrame;
    
    CGRect contentFrame = contentLayer.frame;
    contentFrame.origin.y = 10;
    
    [parentLayer addSublayer:contentLayer];
    
    return parentLayer;
}


- (NSRect)frame
{
    return NSOffsetRect([super frame], 0, 0);
}




@end
