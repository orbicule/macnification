//
//  BlobExtractOperation.h
//  BlobExtraction
//
//  Created by Dennis Lorson on 02/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


extern NSString *BlobExtractionOperationDidEndNotification;



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// algorithm functions and datatypes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct REData_ REData;
typedef struct RECluster_ RECluster;
typedef struct RERegion_ RERegion;
typedef struct REPixel_ REPixel;
typedef struct REInterpolationSegment_ REInterpolationSegment;



void REApplyMorphOperators(REData *data);
void REGrowRegions(REData *data);
void REPruneRegions(REData *data);
void RERestructurePixels(REData *processData, int restructure);
void RESortRegions(REData *data, int retainInnerBlobs, CGFloat maxInterpolationError);
void REReclaimResources(REData *data);

void REDilate(REData *data);
void REErode(REData *processData);


// interpolator

void REInterpolatePoints(CFArrayRef points, CGFloat errorThreshold);
void REInterpolatePointSegment(REInterpolationSegment *segment, CGFloat errorThreshold);
void REFindMaxError(REInterpolationSegment *segment, CGFloat *maxError, NSInteger *maxErrorIndex);



REData * REDataCreate(NSInteger w, NSInteger h, NSInteger bpr, NSInteger spp, unsigned char *imageData);

void REPixelSort(CFMutableArrayRef pixels, CFMutableArrayRef *leftovers, REData *processData);
void REMergeRegions(RERegion *toMergeFrom, RERegion *toMergeInto);
void REClusterSetRegion(RECluster *cluster, RERegion *region);
//int REPixelIsBackground(unsigned char *firstComponent);




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// objc interface
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface BlobExtractOperation : NSOperation {
	
	
	NSInteger minimumBlobArea;					// the minimum size of a blob to be considered.  Default is (0,0).
	BOOL allowsImageEdgeBlobs;				// determines whether or not blobs that reach the edge of the image are allowed. Default is YES.
	BOOL searchesInnerBlobs;
	CGFloat maxInterpolationError;

	
	NSBitmapImageRep *image;							// the image to be processed. Not optional.
		
	REData *data;
	
}
@property (readwrite, retain) NSBitmapImageRep *image;
@property (readwrite) NSInteger minimumBlobArea;
@property (readwrite) BOOL allowsImageEdgeBlobs, searchesInnerBlobs;
@property (readwrite) CGFloat maxInterpolationError;

- (id)initWithBinaryImage:(NSBitmapImageRep *)theImage;
- (void)extractBlobs;
- (NSArray *)convertRegionData:(REData *)processData minimumArea:(NSInteger)minArea innerBlobs:(BOOL)retainInnerBlobs;
- (NSBezierPath *)bezierWithPoints:(CFArrayRef)points;
- (NSInteger)pixelAreaOfRegion:(CFArrayRef)pixels;



@end
