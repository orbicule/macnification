//
//  MGContextualToolStrip.h
//  toolbarTest
//
//  Created by Dennis Lorson on 20/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


typedef enum _MGViewAlignment
{
	
	MGLeftViewAlignment = 0,
	MGCenterViewAlignment = 1,
	MGRightViewAlignment = 2
	
} MGViewAlignment;


@interface MGContextualToolStrip : NSView {

	// left views are left aligned. (index 0 the leftmost view)
	// right views are right aligned. (index 0 the rightmost view)
	// centered views are centered within the whitespace between the left and right collections. (index 0 the leftmost view)
	
	// ellipsedViews contains duplicate entries of the other view arrays.  
	// Those views are ellipsed (setHidden:); the first one in the array determines the frame that displays the ellipse symbol
	
	// this behavior holds when there is enough space to contain all of the views.  If not, views dissappear starting from the right.
	
	NSMutableArray *leftViews;
	NSMutableArray *centeredViews;
	NSMutableArray *rightViews;
	
	NSMutableArray *ellipsedViews;
	
	NSPopUpButton *ellipseButton;
	
	NSInteger firstEllipseIndex;
	MGViewAlignment firstEllipseAlignment;
	
	

}


- (void)addToolView:(NSView *)theView withAlignment:(MGViewAlignment)alignment;
- (void)addSeparatorWithAlignment:(MGViewAlignment)alignment;
- (void)addWhitespaceWithAlignment:(MGViewAlignment)alignment;

- (void)removeItems;


- (CGFloat)minEdgeForViewWithAlignment:(MGViewAlignment)alignment atIndex:(NSInteger)viewIndex;
- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize;
- (void)layoutView:(NSView *)theView withAlignment:(MGViewAlignment)alignment;
- (void)updateViewTiling;
- (CGFloat)centerViewBoxMinEdge;
- (CGFloat)rightViewBoxAbsoluteMinEdge;

- (void)ellipseViewsIfNeeded;

- (NSMenu *)menuWithEllipsedOptions;


- (NSMutableArray *)viewsWithAlignment:(MGViewAlignment)alignment;
- (MGViewAlignment)alignmentOfView:(NSView *)view;

@end
