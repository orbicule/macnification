//
//  MontageUnitView.m
//  Stack
//
//  Created by Dennis Lorson on 2/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import "MontageUnitView.h"

static CGFloat textHeight = 6.0;
static CGFloat textMargin = 3.0;


@implementation MontageUnitView

- (id)initWithFrame:(NSRect)frame image:(NSImage *)image attributes:(NSArray *)attributes attrStyle:(NSDictionary *)style
{
    self = [super initWithFrame:frame];
	
    if (self) {
		
		attrs = [attributes retain];
		attrStyle = [style retain];
		
		CGFloat heightForImage = [self frame].size.height - [[self class] heightForAttributes:[attrs count]];
		
		NSImageView *imageView = [[[NSImageView alloc] initWithFrame:NSMakeRect(0, 0, [self bounds].size.width, heightForImage)] autorelease];
		[imageView setImage:image];
		[imageView setImageScaling:NSScaleToFit];
		[self addSubview:imageView];
		
    }
	
    return self;
}


- (void)dealloc
{
	[attrs release];
	[attrStyle release];
	
	[super dealloc];
}

- (void)drawRect:(NSRect)rect 
{
    CGFloat yPos = [self frame].size.height - [[self class] heightForAttributes:[attrs count]] + textMargin;
		
	int rowNumber = 0;
	
	NSColor *textColor = [attrStyle valueForKey:@"color"];
		
	for (NSString *fieldValue in attrs) {
		
		NSColor *bgColor = [NSColor colorWithCalibratedWhite:[textColor whiteComponent] alpha:0.1];
		
		NSRect stringFrame = NSMakeRect(0, yPos - 1, [self bounds].size.width, textHeight);
		
		if (fmod(rowNumber, 2)) {
			
			NSRect overlayFrame = stringFrame;
			overlayFrame.origin.y -= 2;
			overlayFrame.size.height += 2;
			
			[bgColor set];
			NSRectFill(overlayFrame);
			
		}
		
		NSMutableParagraphStyle *style = [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
		[style setLineBreakMode:NSLineBreakByTruncatingMiddle];
		
		NSFont *font = [NSFont systemFontOfSize:5];
		
		NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
							  style, NSParagraphStyleAttributeName, 
							  font, NSFontAttributeName, 
							  [attrStyle valueForKey:@"color"], NSForegroundColorAttributeName, nil];
		
		NSAttributedString *name = [[[NSAttributedString alloc] initWithString:fieldValue attributes:info] autorelease];
		
		[name drawInRect:stringFrame];
		
		yPos += textHeight + textMargin;
		
		rowNumber ++;
	}
}
																					   
- (BOOL)isFlipped
{
	return YES;
	
}
																					   
+ (CGFloat)heightForAttributes:(NSInteger)nbAttributes
{
	return nbAttributes * (textHeight + textMargin) + textMargin;
}
								  
								  
@end
