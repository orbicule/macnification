//
//  MGPDFHUDView.m
//  StackTable
//
//  Created by Dennis Lorson on 2/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGPDFHUDView.h"


@implementation MGPDFHUDView



- (id)init
{
	if ((self = [super init])) {
		
	}
		
	return self;
	
}

- (void)awakeFromNib
{
	CALayer *layer = [self layer];
	
	layer.opacity = 0.0;
	layer.backgroundColor = CGColorCreateGenericGray(0.0, 0.6);
	layer.cornerRadius = 10.0;
	
	
}



- (void)mouseEntered:(NSEvent *)theEvent
{
	CALayer *layer = [self layer];
	layer.opacity = 1.0;
	
}


- (void)mouseExited:(NSEvent *)theEvent
{
	CALayer *layer = [self layer];
	layer.opacity = 0.0;
	
}



@end
