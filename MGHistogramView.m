//
//  HistogramView.m
//  ImageHistogram
//
//  Created by Dennis Lorson on 23/06/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHistogramView.h"
#import "NSView_MGAdditions.h"


@implementation MGHistogramView

+ (id)defaultAnimationForKey:(NSString *)key {
    if ([key isEqualToString:@"fadeProgress"]) {
        // By default, animate border color changes with simple linear interpolation to the new color value.
        CABasicAnimation *anim = [CABasicAnimation animation];
		[anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
		return anim;
    } else {
        // Defer to super's implementation for any keys we don't specifically handle.
        return [super defaultAnimationForKey:key];
    }
}


- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)awakeFromNib
{
	nbChannels = 0;
	activeColorspace = IHRGBColorspace;
	
	[self resetRanges];	
	
	activeChannel = -1;
	
	
	[self setWantsLayer:YES];
}

- (void)setLowerRangeBound:(CGFloat)bound forChannel:(NSInteger)channel
{
	lowerRangeBound[channel] = MIN(MAX(bound, 0), 1);
	[self setNeedsDisplay:YES];
}

- (void)setUpperRangeBound:(CGFloat)bound forChannel:(NSInteger)channel
{
	upperRangeBound[channel] = MIN(MAX(bound, 0), 1);
	[self setNeedsDisplay:YES];
}

- (void)resetRanges
{
	lowerRangeBound[0] = 0., lowerRangeBound[1] = 0., lowerRangeBound[2] = 0.;
	upperRangeBound[0] = 1., upperRangeBound[1] = 1., upperRangeBound[2] = 1.;

}

- (void)setColorspace:(IHColorspace)cs;
{
	activeColorspace = cs;
	[self setNeedsDisplay:YES];
}

- (void)setActiveChannel:(NSInteger)channel
{
	activeChannel = channel;
	[self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)rect 
{
	// always draw the border
	NSBezierPath *bounds = [NSBezierPath bezierPathWithRect:[self bounds]];//NSMakeRect(0, 0, 256.5, [self bounds].size.height-0.5)];
	[[NSColor colorWithCalibratedWhite:0.8 alpha:1.0] set];
	[bounds stroke];
	//[[NSColor colorWithCalibratedWhite:1.0 alpha:0.2] set];
	//[bounds fill];
	
	// if we're in an animation block, then don't do anything
	if (isInAnimationBlock) {
		//NSLog(@"returning:animblock");
		return;
	}
	
	// if in the course of a transition, draw the composite of the two images
	if (fadeProgress > 0. && fadeProgress < 1. && fromImage && toImage) {
		
		[fromImage drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:(1 + .3 - fadeProgress)];
		[toImage drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:.3 + fadeProgress];
		//NSLog(@"returning:has composited (progress %f)", fadeProgress);

		return;
	}
	//NSLog(@"normal draw");
	
	// the normal drawing routine
	
	// draw BG
	//NSBezierPath *bounds = [NSBezierPath bezierPathWithRect:NSMakeRect(0.5, 0.5, 258, [self bounds].size.height-1)];
	[[NSColor colorWithCalibratedWhite:0.8 alpha:1.0] set];
	[bounds stroke];
	[[NSColor colorWithCalibratedWhite:1.0 alpha:0.15] set];
	[bounds fill];
	
	int i, j;

	//if (!histogramValues) return;
	
	// compute the maximum for each channel
	NSInteger maxHist = 0;
	
	for (i = 0; i < nbChannels; i++) {
		for (j = 1; j < 255; j++) {
			maxHist = (maxHist > histogramValues[i][j]) ? maxHist : histogramValues[i][j];
		}
	}
	//NSLog(@"maxhist = %ld", maxHist);
	
	if (maxHist == 0) return;
	
	// draw the plots
	if (activeColorspace == IHRGBColorspace) {
		
		
		if (nbChannels == 1) {
			
			[[NSColor colorWithCalibratedRed:1 green:1 blue:1 alpha:0.5] set];
			[[self pathWithDatapointsFromChannel:0 withMaximumHistogramValue:maxHist] fill];
			
		} else {
			
			if (activeChannel == 0 || activeChannel == -1) {
				NSBezierPath *path = [self pathWithDatapointsFromChannel:0 withMaximumHistogramValue:maxHist];
				[[NSColor colorWithCalibratedRed:1 green:0 blue:0 alpha:0.6] set];
				[path fill];
				
			}
			
			if (activeChannel == 1 || activeChannel == -1) {
				[[NSColor colorWithCalibratedRed:0 green:1 blue:0 alpha:0.5] set];
				NSBezierPath *path = [self pathWithDatapointsFromChannel:1 withMaximumHistogramValue:maxHist];
				[path fill];				
			}
			
			if (activeChannel == 2 || activeChannel == -1) {
				[[NSColor colorWithCalibratedRed:0 green:0 blue:1 alpha:0.5] set];
				NSBezierPath *path = [self pathWithDatapointsFromChannel:2 withMaximumHistogramValue:maxHist];
				[path fill];				
			}
		}
		
	} else if (activeColorspace == IHHSLColorspace) {
		
		if (nbChannels == 1) {
			
			[[NSColor colorWithCalibratedRed:1 green:1 blue:1 alpha:0.5] set];
			[[self pathWithDatapointsFromChannel:0 withMaximumHistogramValue:maxHist] fill];
			
			
		} else {
			
			if (activeChannel == 0 || activeChannel == -1) {		
				[[self hueGradient] drawInBezierPath:[self pathWithDatapointsFromChannel:0 withMaximumHistogramValue:maxHist] angle:0];
			}
			
			if (activeChannel == 1 || activeChannel == -1) {
				[[NSColor colorWithCalibratedRed:0 green:0 blue:0 alpha:0.5] set];
				NSBezierPath *path = [self pathWithDatapointsFromChannel:1 withMaximumHistogramValue:maxHist];
				[path fill];
			}
			
			if (activeChannel == 2 || activeChannel == -1) {
				[[NSColor colorWithCalibratedRed:1 green:1 blue:1 alpha:0.5] set];
				NSBezierPath *path = [self pathWithDatapointsFromChannel:2 withMaximumHistogramValue:maxHist];
				[path fill];
			}
		}
		
		
		
	} else if (activeColorspace == IHMonochromeColorspace) {
		
		if (nbChannels == 1) {
			
			[[NSColor colorWithCalibratedRed:1 green:1 blue:1 alpha:0.5] set];
			NSBezierPath *path = [self pathWithDatapointsFromChannel:0 withMaximumHistogramValue:maxHist];
			[path fill];			
		} else {
			
			int k;
			for (k = 0; k < 256; k++)
				histogramValues[3][k] = histogramValues[0][k] + histogramValues[1][k] + histogramValues[2][k];
			
			// compute the maximum for the combined channel
			maxHist = 0;
			
			for (i = 0; i < nbChannels; i++) {
				for (j = 1; j < 255; j++) {
					maxHist = (maxHist > histogramValues[3][j]) ? maxHist : histogramValues[3][j];
				}
			}
			
			[[NSColor colorWithCalibratedRed:1 green:1 blue:1 alpha:0.5] set];
			NSBezierPath *path = [self pathWithDatapointsFromChannel:3 withMaximumHistogramValue:maxHist];
			[path fill];			
		}
	}
		
	if (activeChannel != -1) {
		
		// draw the range bounds
		if (lowerRangeBound[activeChannel] > 0) {
			NSRect lowerRangeRect = NSInsetRect([self bounds], 1, 1);
			lowerRangeRect.size.width = lowerRangeBound[activeChannel] * lowerRangeRect.size.width;
			
			[[NSColor colorWithDeviceWhite:.0 alpha:.5] set];
			[[NSBezierPath bezierPathWithRect:lowerRangeRect] fill];
			
		}
		
		if (upperRangeBound[activeChannel] < 1) {
			
			NSRect upperRangeRect = NSInsetRect([self bounds], 0, 1);
			
			upperRangeRect.origin.x = upperRangeRect.origin.x + upperRangeBound[activeChannel] * upperRangeRect.size.width;
			upperRangeRect.size.width = (1. - upperRangeBound[activeChannel]) * upperRangeRect.size.width - 1;
			
			[[NSColor colorWithDeviceWhite:.0 alpha:.5] set];
			[[NSBezierPath bezierPathWithRect:upperRangeRect] fill];
			
			
		}	
		
		
	}
	
	[[NSColor colorWithCalibratedWhite:0.8 alpha:1.0] set];
	[bounds stroke];
}


- (NSGradient *)hueGradient
{
	NSArray *colors = [NSArray arrayWithObjects:
					   
					   [NSColor redColor],
					   [NSColor yellowColor],
					   [NSColor greenColor],
					   [NSColor cyanColor],
					   [NSColor blueColor],
					   [NSColor purpleColor],
					   [NSColor redColor],
					   
					   nil];
	
	return [[[NSGradient alloc] initWithColors:colors] autorelease];
	
	
}

- (NSBezierPath *)pathWithDatapointsFromChannel:(NSInteger)channel withMaximumHistogramValue:(NSInteger)maxVal;
{
	NSBezierPath *path = [NSBezierPath bezierPath];
	[path moveToPoint:NSMakePoint(1,1)];
	
	int j;
	for (j = 0; j < 253; j+=1) {
		CGFloat destination = ((CGFloat)histogramValues[channel][j] + (CGFloat)histogramValues[channel][j+1] + (CGFloat)histogramValues[channel][j+2] + (CGFloat)histogramValues[channel][j+3])/4.;
		CGFloat fraction = destination/((CGFloat)maxVal);
		
		CGFloat height = ([self bounds].size.height-2) * MIN(fraction, 1);
		
		[path lineToPoint:NSMakePoint(j+1, MAX(height, 1))];
		
	}
	[path lineToPoint:NSMakePoint(j+1, 1)];
	[path lineToPoint:NSMakePoint(1, 1)];
	
	return path;
}

- (void)setCIImage:(CIImage *)img
{
	CGRect extent = [img extent];
	void *buff = malloc((int)extent.size.width * (int)extent.size.height * 4);
	
	//CIContext *ciCtx = [[[NSApp mainWindow] graphicsContext] CIContext];
	
	CGContextRef ctx = CGBitmapContextCreate(buff,
											 (int)extent.size.width, 
											 (int)extent.size.height, 
											 8, 
											 4 * (int)extent.size.width, 
											 CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), 
											 kCGImageAlphaNoneSkipFirst);
	
	CIContext *ciCtx = [CIContext contextWithCGContext:ctx options:nil];
	[ciCtx drawImage:img inRect:CGRectMake(0, 0, extent.size.width, extent.size.height) fromRect:extent];
	
	//[ciCtx render:img toBitmap:buff rowBytes:(ptrdiff_t)(extent.size.width * 4) bounds:extent format:kCIFormatARGB8 colorSpace:CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB)];
	
	vImage_Buffer *vimbuff = malloc(sizeof(vImage_Buffer));
	vimbuff->data = buff;
	vimbuff->width = (vImagePixelCount)(extent.size.width);
	vimbuff->height = extent.size.height;
	vimbuff->rowBytes = extent.size.width * 4;
	
	vImagePixelCount *histogram[4];
	histogram[0] = (vImagePixelCount *) malloc(256 * sizeof(vImagePixelCount));
	histogram[1] = (vImagePixelCount *) malloc(256 * sizeof(vImagePixelCount));
	histogram[2] = (vImagePixelCount *) malloc(256 * sizeof(vImagePixelCount));
	histogram[3] = (vImagePixelCount *) malloc(256 * sizeof(vImagePixelCount));
	
	vImageHistogramCalculation_ARGB8888(vimbuff, histogram, 0);
	//IHPostProcessHistogram(histogramValues, nbChannels, &nbChannels);
	
	int i;
	for (i = 0; i < 256; i++) {
		
		histogramValues[0][i] = (NSInteger)(histogram[1])[i];
		histogramValues[1][i] = (NSInteger)(histogram[2])[i];
		histogramValues[2][i] = (NSInteger)(histogram[3])[i];
		histogramValues[3][i] = (NSInteger)(histogram[0])[i];
		
		//NSLog(@" %ld    ----- %ld      -----      %ld      ------   %ld", histogramValues[0][i], histogramValues[1][i], histogramValues[2][i], histogramValues[3][i]);
		
	}
	
	nbChannels = 3;
	
	IHPostProcessHistogram(histogramValues, nbChannels, &nbChannels);

	free(buff);
	free(vimbuff);
	
	[self setNeedsDisplay:YES];
	
	
}

#pragma mark Image Transitions


- (void)beginAnimationBlock
{
	//NSLog(@"start animation block");
	isInAnimationBlock = NO;
	//NSLog(@"drawing view image");
	[self setNeedsDisplay:YES];
	[self setFromImage:[self viewImage]];
	//NSLog(@"end drawing view image");
	isInAnimationBlock = YES;
	
	// cancel out any transitions that are underway
	[self setFadeProgress:0.00];
	//[[self animator] setFadeProgress:0.0];

}

- (void)endAnimationBlock
{
	//if (!isInAnimationBlock) return;
	
	isInAnimationBlock = NO;
	[self setNeedsDisplay:YES];
	[self setToImage:[self viewImage]];
	
	[self setFadeProgress:0.01];
	[CATransaction begin];
	[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
	[[self animator] setFadeProgress:1.0];
	[CATransaction commit];
	
	//NSLog(@"end animation block");

}

- (void)setFromImage:(NSImage *)image
{
	if (fromImage != image) {
		
		[fromImage release];
		fromImage = [image retain];
		
	}
	
}

- (void)setToImage:(NSImage *)image
{
	if (toImage != image) {
		
		[toImage release];
		toImage = [image retain];
		
	}
	
}

- (void)setFadeProgress:(CGFloat)progress
{
	fadeProgress = progress;
	//NSLog(@"progress = %f", fadeProgress);
	if (fadeProgress > .99) {
		[self setFromImage:nil];
		[self setToImage:nil];
		isInAnimationBlock = NO;
		//NSLog(@"end fade");
		
	}
	//NSLog(@"progress: %f", fadeProgress);
	[self setNeedsDisplay:YES];
}


#pragma mark Histogram generation

// will do the following:
// - equalize the histogram graph
// - if a zero-valued bin is next to two non-zero bins, the bin gets one fourth of the count of both.
// - interpolate between 2 consecutive values
// - correct the number of channels and color space, if the image turns out to be grayscale.
void IHPostProcessHistogram(NSInteger bins[4][256], NSInteger channelCount, NSInteger *correctedChannelCount)
{
	int i, j;

	/*for (i = 0; i < channelCount; i++) {
		
		for (j = 0; j < 256; j++)
			printf ("%i - ", bins[i][j]);
		printf("\n");
		
	}*/
	
	
	// copy data
	NSInteger binsCopy[4][256];
	for (i = 0; i < *correctedChannelCount; i++) {
		for (j = 0; j < 256; j++) {
			binsCopy[i][j] = bins[i][j];
		}
	}
	
	// lowpass filtering
	for (i = 0; i < *correctedChannelCount; i++) {
		
		bins[i][0] = binsCopy[i][0] * 0.8 + binsCopy[i][1] * 0.2;
		for (j = 1; j < 255; j++) {
			bins[i][j] = binsCopy[i][j-1] * 0.2 + binsCopy[i][j] * 0.6 + binsCopy[i][j + 1] * 0.2;
			
		}
		bins[i][255] = binsCopy[i][255] * 0.8 + binsCopy[i][254] * 0.2;

	}

	
	// equalize values
	for (i = 0; i < *correctedChannelCount; i++) {
		for (j = 1; j < 255; j++) {
			bins[i][j] = pow(bins[i][j], 0.8);
		}
	}
}


void IHComputeHistogram(unsigned char *data, NSInteger bins[4][256], NSInteger bpr, NSInteger spp, NSInteger w, NSInteger h, NSInteger componentOffset, NSInteger channelCount, IHColorspace colorspace)
{
	// if <rgb> == 1, RGB colors are computed, otherwise HSB colors. It is ignored if nbChannels != 3
	// component offset is the offset of the useful channels (for example, if the bitmap format is ARGB, the offset is 1, otherwise it is 0)
	// bins is a pointer to an array with nbChannel entries, each entry containing a pointer to an array with 256 bins.
	
	// clear the bins
	int i, j;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 256; j++) {
			bins[i][j] = 0;
		}
	}
	
	
	int x, y;
	
	CGFloat max, min, diff;
	
	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			
			unsigned char *pix = &(data[y * bpr + x * spp + componentOffset]);
			
			
			if (channelCount == 1) {
				
				// grayscale
				unsigned char gray = pix[0];
				
				// increase the bin
				bins[0][(NSInteger)gray] += 1;
				
				
			} else {
				
				// image is rgb -> do we need HSB or RGB values?
				
				unsigned char r = pix[0];
				unsigned char g = pix[1];
				unsigned char b = pix[2];
				
				if (YES) { //colorspace == IHRGBColorspace) {
					
					//if ( x == 300) NSLog(@"r %ld -- g %ld -- b %ld", r, g, b);
					
					bins[0][(NSInteger)r] += 1;
					bins[1][(NSInteger)g] += 1;
					bins[2][(NSInteger)b] += 1;
					
					
				} else {
					
					// conversion RGB -> HSL
					// values for these properties can range between 0-360 (H) and 0-1 (S, L) but are mapped to 0-255 because it offers an reasonable differentation
					// and is easier to use (same as RGB)
					
					CGFloat rc = (CGFloat)r / 255.;
					CGFloat gc = (CGFloat)g / 255.;
					CGFloat bc = (CGFloat)b / 255.;
					
					CGFloat h,s,l;
					
					max = MAX(MAX(rc, gc), bc);
					min = MIN(MIN(rc, gc), bc);
					
					diff = max - min;
					
					l = (max + min)/2.;
					
					if (max == min) {
						
						h = 0.;
						s = 0.;
						
					} else {
						
						s = (l < 0.5) ? (diff/(max + min)) : (diff/(2. - max - min));
						
						if (max == rc)
							h = (gc - bc)/diff;
						else if (max == gc)
							h = (2. + (bc - rc)/diff);
						else
							h = (4. + (rc - gc)/diff);
												
					}
					
					if (h < 0)
						h+= 6.;						
					else if (h > 6)
						h-= 6.;
					
					// fill bins(h range = 0 - 6)
					bins[0][(NSInteger)(h * 255./6.)] += 1;
					bins[1][(NSInteger)(s * 255.)] += 1;
					bins[2][(NSInteger)(l * 255.)] += 1;
				}
			}
		}
	}
}

@end
