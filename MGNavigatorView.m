//
//  MGNavigatorView.m
//  ImageNavigator
//
//  Created by Dennis Lorson on 6/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGNavigatorView.h"

#import "PSImageView.h"
#import "MGCIContextManager.h"


@implementation MGNavigatorView


@synthesize thumbnailSize, visibleRect, image;


- (id)init
{
	if ((self = [super init])) {
		
		thumbnail = NULL;
		visibleRect = NSMakeRect(.25,.25,.5,.5);
		
	}
	return self;
}

- (void)awakeFromNib
{
	thumbnail = NULL;
	visibleRect = NSMakeRect(.25,.25,.5,.5);
}


- (void)dealloc
{
	self.image = nil;
	
	if (thumbnail)
		free(thumbnail);
	
	[super dealloc];
}

- (void)centerVisibleRectOnLocation:(NSPoint)location
{
    
	
	// move rectangle to center cursor in it (if possible)
	NSRect adjustedVisibleRect = [self adjustedVisibleRect];
	
	NSRect desiredRect = NSMakeRect(location.x - adjustedVisibleRect.size.width/2, location.y - adjustedVisibleRect.size.height/2, adjustedVisibleRect.size.width, adjustedVisibleRect.size.height);
	
	NSRect normalizedRect = desiredRect;
	normalizedRect.origin.x /= [self bounds].size.width;
	normalizedRect.origin.y /= [self bounds].size.height;
	normalizedRect.size.width = self.visibleRect.size.width;
	normalizedRect.size.height = self.visibleRect.size.height;
	
	// make sure the rect is in normalized rect bounds (0-1)
	if (1 < NSMaxX(normalizedRect)) {
		
		normalizedRect.origin.x = 1 - normalizedRect.size.width;
		
	}
	
	if (1 < NSMaxY(normalizedRect)) {
		
		normalizedRect.origin.y = 1 - normalizedRect.size.height;
		
	}
	
	
	if (0 > NSMinX(normalizedRect)) {
		
		normalizedRect.origin.x = 0;
		
	}
	
	if (0 > NSMinY(normalizedRect)) {
		
		normalizedRect.origin.y = 0;
		
	}
	
	self.visibleRect = normalizedRect;
	
	[imageView scrollToNormalizedRect:normalizedRect];
	
}

- (void)mouseDown:(NSEvent *)theEvent
{
	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	// if in the rect, will handle when the mouse drags.
	if (NSPointInRect(location, [self adjustedVisibleRect])) return;

	[self centerVisibleRectOnLocation:location];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	
	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	[self centerVisibleRectOnLocation:location];
	
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}


- (BOOL)isOpaque
{
	
	return YES;
}

- (BOOL)mouseDownCanMoveWindow
{
	return NO;
}

- (void)setVisibleRect:(NSRect)rect
{
	visibleRect = rect;
	[self setNeedsDisplay:YES];
	
	
}


- (void)setImage:(CIImage *)anImage forView:(PSImageView *)view;
{
	if (anImage != self.image || !imageView) {	
		
		if (thumbnail) {
			CGImageRelease(thumbnail);
			thumbnail = NULL;
		}

		imageView = view;
		
		self.image = anImage;
		
		thumbnailSize = [self thumbnailSizeForImage:self.image];
		[self setNeedsDisplay:YES];
	}

}

- (CGImageRef)thumbnailWithImage:(CGImageRef)img
{
	NSInteger w = ceil(self.bounds.size.width);
	NSInteger h = ceil(self.bounds.size.height);
	
	void *data = malloc(4 * h * w);
	
	CGContextRef ctx = CGBitmapContextCreate(data, w, h, 8, 4 * w, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	CGContextDrawImage(ctx, CGRectMake(0, 0, w, h), img);
	
	CGImageRef thumb = CGBitmapContextCreateImage(ctx);
	
	CGContextRelease(ctx);
	free(data);
	
	return (CGImageRef)[(id)thumb autorelease];
	
}

- (void)drawRect:(NSRect)frame
{
	CGContextRef ctx = [[NSGraphicsContext currentContext] graphicsPort];
	
	if (image) {
				
		if (!thumbnail) {
			
			CGImageRef cgImage = [[MGCIContextManager sharedContextManager] imageRefFromCIImage_10A432_WORKAROUND:image];
			
			if (cgImage) {
				thumbnail = [self thumbnailWithImage:cgImage];
				CGImageRetain(thumbnail);
			}
			
		}
		CGContextDrawImage (ctx, CGContextGetClipBoundingBox(ctx), thumbnail);
	}
	
	NSRect visRect = visibleRect;
	visRect.origin.x *= [self bounds].size.width;
	visRect.origin.y *= [self bounds].size.height;
	visRect.size.width *= [self bounds].size.width;
	visRect.size.height *= [self bounds].size.height;
	
	NSRect boundsRect = [self bounds];
	
	// create the clipping path for the white overlay: the view minus the visible rect
	NSBezierPath *visibleRectPath = [NSBezierPath bezierPathWithRect:visRect];
	NSBezierPath *boundsPath = [NSBezierPath bezierPathWithRect:NSInsetRect(boundsRect, -3, -3)];
	[boundsPath setWindingRule:NSEvenOddWindingRule];
	[boundsPath appendBezierPath:visibleRectPath];
	[boundsPath addClip];
	
	
	[[NSColor colorWithCalibratedWhite:1 alpha:.3] set];
	[boundsPath fill];
	

	[NSGraphicsContext restoreGraphicsState];

	visRect = NSIntersectionRect(visRect, NSMakeRect(0, 0, [self bounds].size.width - 1, [self bounds].size.height - 1));
	
	
	// draw the border
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];

	[[NSColor colorWithCalibratedWhite:1 alpha:1] set];
	//NSFrameRect(NSInsetRect(visRect, 0, 0));
	
	NSBezierPath *path = [NSBezierPath bezierPathWithRect:visRect];
	[path setLineWidth:0.0];
	[path stroke];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];

}

- (NSRect)adjustedVisibleRect
{
	NSRect visibleRectInThumbnail = visibleRect;
	visibleRectInThumbnail.origin.x *= thumbnailSize.width;
	visibleRectInThumbnail.origin.y *= thumbnailSize.height;
	visibleRectInThumbnail.size.width *= thumbnailSize.width;
	visibleRectInThumbnail.size.height *= thumbnailSize.height;
	
	visibleRectInThumbnail = NSIntersectionRect(visibleRectInThumbnail, NSMakeRect(0, 0, thumbnailSize.width, thumbnailSize.height));
	
	return visibleRectInThumbnail;
}


- (CGSize)thumbnailSizeForImage:(CIImage *)img
{
	CGFloat w = [img extent].size.width;
	CGFloat h = [img extent].size.height;
	
	CGFloat maxDimension = 200;
	
	if (w > maxDimension) {
		
		h = h * maxDimension / w;
		w = maxDimension;
		
	}
	
	if (h > maxDimension) {
		
		w = w * maxDimension / h;
		h = maxDimension;
		
	}
	
	return CGSizeMake(w, h);
}

@end
