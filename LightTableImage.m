//
//  LightTableImage.m
//  LightTable
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableImage.h"
#import "NSImage_MGAdditions.h"
#import "Image.h"


@implementation LightTableImage


- (void)setImage:(id)image
{
	// when the image relationship is established for the first time, this method will initialize the light table properties of this image,
	// such as its dimensions in the light table.
	
	[self willChangeValueForKey:@"image"];
	[self setPrimitiveValue:image forKey:@"image"];
	[self didChangeValueForKey:@"image"];
	
	if (![self valueForKey:@"width"] && ![self valueForKey:@"height"]) {
		
		// set default values for height and width of the image.  Only do this if the image has no dimensions in this light table yet.
		NSSize imageSize = [(NSImage *)[self valueForKeyPath:@"image.filteredImage"] bestRepresentationSize];
		NSSize defaultSize = NSMakeSize(defaultImageWidth,defaultImageHeight);
		
		NSSize newImageSize;
		
		// we make sure every image is (proportionally) scaled within a bounding box with size "defaultSize".
		if ( imageSize.width < imageSize.height * defaultSize.width/defaultSize.height ) {
			// height determines dimensions
			newImageSize.height = defaultSize.height;
			newImageSize.width = newImageSize.height * imageSize.width/imageSize.height;
		} else {
			newImageSize.width = defaultSize.width;
			newImageSize.height = newImageSize.width * imageSize.height/imageSize.width;
		}
		
		[self setValue:[NSNumber numberWithFloat:newImageSize.height] forKey:@"height"];
		[self setValue:[NSNumber numberWithFloat:newImageSize.width] forKey:@"width"];
	}
}


- (CGFloat)imageMagnification
{
	// return the scale factor of this image, that is, its current size relative to its actual size (the size defined by the NSImageRep)
	NSSize originalImageSize = [[self valueForKeyPath:@"image.filteredImage"] bestRepresentationSize];
	CGFloat currentHeight = [[self valueForKeyPath:@"height"] floatValue];
	CGFloat currentWidth = [[self valueForKeyPath:@"width"] floatValue];
	return fmin(currentWidth/originalImageSize.width, currentHeight/originalImageSize.height);
}

- (NSRect)frame
{
	return NSMakeRect([[self valueForKey:@"x"] floatValue],
					  [[self valueForKey:@"y"] floatValue],
					  [[self valueForKey:@"width"] floatValue],
					  [[self valueForKey:@"height"] floatValue]);
	
}

- (void)setX:(CGFloat)x
{
	// frame observers need to be told of an update too
	[self willChangeValueForKey:@"frame"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:x] forKey:@"x"];
	[self didChangeValueForKey:@"frame"];
}

- (void)setY:(CGFloat)y
{
	// frame observers need to be told of an update too
	[self willChangeValueForKey:@"frame"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:y] forKey:@"y"];
	[self didChangeValueForKey:@"frame"];
}

- (void)setWidth:(CGFloat)width
{
	// frame observers need to be told of an update too
	[self willChangeValueForKey:@"frame"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:width] forKey:@"width"];
	[self didChangeValueForKey:@"frame"];
}

- (void)setHeight:(CGFloat)height
{
	// frame observers need to be told of an update too
	[self willChangeValueForKey:@"frame"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:height] forKey:@"height"];
	[self didChangeValueForKey:@"frame"];
}

- (void)setFrame:(NSRect)frame
{
	[self willChangeValueForKey:@"frame"];

	[self willChangeValueForKey:@"x"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:frame.origin.x] forKey:@"x"];
	[self didChangeValueForKey:@"x"];
	
	[self willChangeValueForKey:@"y"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:frame.origin.y] forKey:@"y"];
	[self didChangeValueForKey:@"y"];
	
	[self willChangeValueForKey:@"width"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:frame.size.width] forKey:@"width"];
	[self didChangeValueForKey:@"width"];
	
	[self willChangeValueForKey:@"height"];
	[self setPrimitiveValue:[NSNumber numberWithFloat:frame.size.height] forKey:@"height"];
	[self didChangeValueForKey:@"height"];
	
	[self didChangeValueForKey:@"frame"];
}

- (NSSize)actualSize
{
	// returns the actual size of the image, taking into account the magnification property of the Image.
	NSImage *image = [self valueForKeyPath:@"image.filteredImage"];
	NSSize originalSize = [image bestRepresentationSize];
	
	return NSMakeSize(originalSize.width/[[self valueForKey:@"magnification"] floatValue],originalSize.height/[[self valueForKey:@"magnification"] floatValue]);
}



- (CGFloat)opacity
{
	NSString *semiUID = [(Image *)[self valueForKeyPath:@"image"] originalImagePathExpanded:NO];
	
	// fetch the key from our image
	id lightTable = [[self valueForKey:@"lightTables"] anyObject];
	NSDictionary *opacities = [lightTable valueForKeyPath:@"ltImageOpacities"];
	if (!opacities)
		return 1.0;
	
	NSNumber *opacity = [opacities objectForKey:semiUID];
	
	if (!opacity) return 1.0;
	
	return [opacity floatValue];
	
}

- (void)setOpacity:(CGFloat)opacity
{
	
	[self willChangeValueForKey:@"opacity"];

	id lightTable = [[self valueForKey:@"lightTables"] anyObject];
	NSMutableDictionary *opacities = [lightTable valueForKeyPath:@"ltImageOpacities"];
		
	if (!opacities)
		opacities = [NSMutableDictionary dictionary];
	
	NSString *semiUID = [(Image *)[self valueForKeyPath:@"image"] originalImagePathExpanded:NO];

	[opacities setObject:[NSNumber numberWithFloat:opacity] forKey:semiUID];
	[lightTable setValue:opacities forKeyPath:@"ltImageOpacities"];
	
	[self didChangeValueForKey:@"opacity"];
	
}


@end
