kernel vec4 rgb2hsl(sampler image)
{
	vec4 src = sample(image, floor(samplerCoord(image)));
	
	float h = 0, s = 0, l = 0;	
					
	float maximum = max(max(src.r, src.g), src.b);
	float minimum = min(min(src.r, src.g), src.b);
					
	float diff = maximum - minimum;
	float sum  = maximum + minimum;
					
	l = (sum) * 0.5;
			
	s = (diff < 0.01) ? 0. : 1.;
	h = (diff < 0.01) ? 0. : 1.;

	// the following statements will always be executed but only have effect when s or h != 0
	s = (l <= 0.5) ? s*(diff/sum) : s*(diff/(2. - sum));
	
	h = (maximum == src.r) ? h*(src.g - src.b)/diff : h;
	h = (maximum == src.g) ? h*(2. + (src.b - src.r)/diff) : h;
	h = (maximum == src.b) ? h*(4. + (src.r - src.g)/diff) : h;
	
	h = (h < 0.) ? h + 6. : h;
	h = (h > 6.) ? h - 6. : h;

	return vec4(h/6.,s,l,src.a);
}



float hue2rgb(float p, float q, float t)
{
	// constants
	float oneTwelfth = 1./12.;

	t = (t < 0.) ? t + 1. : t;
	t = (t > 1.) ? t - 1. : t;
	
	float res = 0;
	
	res = (t < 2. * oneTwelfth) ? 
			p + (t * 6. * (q - p))                
			: res;
			
	res = (abs(t - 4. * oneTwelfth) < 2. * oneTwelfth) ?
			q                        
			: res;
			
	res = (abs(t - 7. * oneTwelfth) < oneTwelfth) ?
			p + ((8. * oneTwelfth - t) * 6. * (q - p))   
			: res;
			
	res = (t > 8. * oneTwelfth) ?
			p                                     
			: res;
	
	return res;
}


kernel vec4 hsl2rgb(sampler image)
{
	// constants
	float oneThird = 0.3333333333;

	vec4 src = sample(image, samplerCoord(image));
	
	float h = src.r, s = src.g, l = src.b;
	
	float q = (l < .5) ? l * (1. + s) : l + s - (l * s);
	
	float p = 2. * l - q;
	
	float tr = h + oneThird;
	float tg = h;
	float tb = h - oneThird;
	
	tr = (tr < 0.) ? tr + 1. : tr;
	tg = (tg < 0.) ? tg + 1. : tg;
	tb = (tb < 0.) ? tb + 1. : tb;
	
	tr = (tr > 1.) ? tr - 1. : tr;
	tg = (tg > 1.) ? tg - 1. : tg;
	tb = (tb > 1.) ? tb - 1. : tb;
	
	float r = hue2rgb(p, q, h + 1./3.);
	float g = hue2rgb(p, q, h);
	float b = hue2rgb(p, q, h - 1./3.);
	
	return vec4(r, g , b , 1.);
}

kernel vec4 rgb2gray(sampler image)
{
	vec4 src = sample(image, samplerCoord(image));
	
	float dest = (src.r + src.g + src.b) * 0.333333333;
	
	return vec4(dest,dest,dest,src.a);
}
