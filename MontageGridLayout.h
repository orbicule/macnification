//
//  MontageGridLayout.h
//  Stack
//
//	This class is responsible for layouting a given unit (image), when supplied parameters.
//
//  Created by Dennis Lorson on 2/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class Montage;

@interface MontageGridLayout : NSObject {

}


// The <parameters> dictionary contains the following entries (their name is the key):
//		NSSize unitMargin = the margin between units (hor/ver)
//		NSInteger unitsPerRow
//		NSInteger nbUnits = total nb units to layout

//		CGFloat attributeHeight = height of the items that the unit is composed of, excluding the image
//		NSSize compositeSize = size of one frame to layout the units in
//		NSSize desireImagedSize = h/w ratio for the images (the same for all)

+ (NSDictionary *)pageAndFrameForUnit:(int)unitIndex withParameters:(NSDictionary *)parameters;


@end
