//
//  LightTableImageToolView.m
//  LightTable
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableImageToolView.h"

#import "LightTableView.h"
#import "LightTableImageView.h"
#import "LightTableContentView.h"
#import "MGFunctionAdditions.h"

@interface LightTableImageToolView (Private)

- (void)initializeTrackingAreas;

- (void)drawControlPoints;
- (void)drawContextInfo;
- (void)drawCommentIndicator;

- (void)setCommentIndicator:(NSBezierPath *)bezier;


@end


@implementation LightTableImageToolView


@synthesize imageSizeInfoBoxOpacity, selectionRectangleOpacity, controlPointsOpacity, showsScalebarIfAvailable;



- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
	
    if (self) {
		self.imageSizeInfoBoxOpacity = 0.0;
		[self initializeTrackingAreas];
		
    }
    return self;
}


- (void)dealloc
{
	[self setCommentIndicator:nil];
	[super dealloc];
}

+ (id)defaultAnimationForKey:(NSString *)key 
{
	return [CABasicAnimation animation];
}


#pragma mark Control Points
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Control Points and bounding box
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSBezierPath *)pathForControlPoint:(ControlPoint)thePoint
{
	CGFloat zoomLevel = ((LightTableContentView *)[[self superview] superview]).zoomLevel;
	
	CGFloat margin = 6.0 / zoomLevel;
	CGFloat size = 8.0 / zoomLevel;
	NSBezierPath *path = [NSBezierPath bezierPath];
	NSPoint origin;
	switch (thePoint) {
	
		case LowerLeftControlPoint:
			origin = NSMakePoint([self bounds].origin.x + margin, [self bounds].origin.y + margin);
			break;
			
		case MiddleLeftControlPoint:
			origin = NSMakePoint([self bounds].origin.x + margin, [self bounds].origin.y + [self bounds].size.height/2 - size/2);
			break;
			
		case UpperLeftControlPoint:
			origin = NSMakePoint([self bounds].origin.x + margin, [self bounds].origin.y + [self bounds].size.height - margin - size);
			break;
			
		case UpperMiddleControlPoint:
			origin = NSMakePoint([self bounds].origin.x + [self bounds].size.width/2 - size/2, [self bounds].origin.y + [self bounds].size.height - margin - size);
			break;
			
		case UpperRightControlPoint:
			origin = NSMakePoint([self bounds].origin.x + [self bounds].size.width - margin - size, [self bounds].origin.y + [self bounds].size.height - margin - size);
			break;
			
		case MiddleRightControlPoint:
			origin = NSMakePoint([self bounds].origin.x + [self bounds].size.width - margin - size, [self bounds].origin.y + [self bounds].size.height/2 - size/2);
			break;
		
		case LowerRightControlPoint:
			origin = NSMakePoint([self bounds].origin.x + [self bounds].size.width - margin - size, [self bounds].origin.y + margin);
			break;
		
		case LowerMiddleControlPoint:
			origin = NSMakePoint([self bounds].origin.x + [self bounds].size.width/2 - size/2, [self bounds].origin.y + margin);
			break;
			
		case NoControlPoint:
			return path;
			
	}
	
	NSRect controlPointRect = NSMakeRect(origin.x, origin.y, size, size);
	
	NSRect controlPointRectInBase = [self convertRectToBase:controlPointRect];
	controlPointRectInBase = NSIntegralRect(controlPointRectInBase);
	controlPointRectInBase.origin.x += 0.5;
	controlPointRectInBase.origin.y += 0.5;
	
	controlPointRect = [self convertRectFromBase:controlPointRectInBase];
	
	[path appendBezierPathWithRect:controlPointRect];
	//[path appendBezierPathWithOvalInRect:NSMakeRect(centerPoint.x - (margin - 1)/2, centerPoint.y - (margin - 1)/2, margin - 1, margin - 1)];
	return path;
}

- (ControlPoint)controlPointAtPoint:(NSPoint)point
{
	// used to determine the control point (if there is one) at the cursor position.
	
	if ([[self pathForControlPoint:LowerLeftControlPoint] containsPoint:point]) return LowerLeftControlPoint;
	if ([[self pathForControlPoint:MiddleLeftControlPoint] containsPoint:point]) return MiddleLeftControlPoint;
	if ([[self pathForControlPoint:UpperLeftControlPoint] containsPoint:point]) return UpperLeftControlPoint;
	if ([[self pathForControlPoint:UpperMiddleControlPoint] containsPoint:point]) return UpperMiddleControlPoint;
	if ([[self pathForControlPoint:UpperRightControlPoint] containsPoint:point]) return UpperRightControlPoint;
	if ([[self pathForControlPoint:MiddleRightControlPoint] containsPoint:point]) return MiddleRightControlPoint;
	if ([[self pathForControlPoint:LowerRightControlPoint] containsPoint:point]) return LowerRightControlPoint;
	if ([[self pathForControlPoint:LowerMiddleControlPoint] containsPoint:point]) return LowerMiddleControlPoint;

	return NoControlPoint;
}

- (NSArray *)controlPoints
{
	return [NSArray arrayWithObjects:[NSNumber numberWithInteger:LowerLeftControlPoint],
									 [NSNumber numberWithInteger:MiddleLeftControlPoint],
									 [NSNumber numberWithInteger:UpperLeftControlPoint],
									 [NSNumber numberWithInteger:UpperMiddleControlPoint],
									 [NSNumber numberWithInteger:UpperRightControlPoint],
									 [NSNumber numberWithInteger:MiddleRightControlPoint],
									 [NSNumber numberWithInteger:LowerRightControlPoint],
									 [NSNumber numberWithInteger:LowerMiddleControlPoint],nil];
}



#pragma mark Drawing - properties
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing - properties
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)drawRect:(NSRect)rect
{
	[self drawSelectionRectangle];
	[self drawContextInfo];
	//[self drawCommentIndicator];
	
	[self drawScalebar];
	[self drawControlPoints];
}

- (void)drawScalebar
{
	if (!self.showsScalebarIfAvailable) return;
	
	// Get the image size
	NSSize imageSize = [self bounds].size;
	
	// Convert our properties to primitive types
	
	// TODO: replace these by model values (Peter)
	float xOriginOnImage = 0.1 * imageSize.width;
	float yOriginOnImage = 0.1 * imageSize.height;
	float strokeOnImage = 1.0 / 100.0 * imageSize.height;
	float lengthOnImage = 30.0;
	NSString *scaleBarStyle = @"Zebra";
	BOOL drawsLabel = NO;
	NSString *fontName = @"Arial";
	float fontSize = 10.0;
	
	
	/*float xOriginOnImage = [[self valueForKey:@"xProportion"]floatValue] * imageSize.width;
	float yOriginOnImage = [[self valueForKey:@"yProportion"]floatValue] * imageSize.height;
	float strokeOnImage = [[self valueForKey:@"stroke"] floatValue] / 100.0 * imageSize.height;
	float lengthOnImage = [[self valueForKey:@"length"]floatValue] * [[self valueForKeyPath:@"image.calibration.horizontalPixelsForUnit"]floatValue];
	NSString *scaleBarStyle = [self valueForKey:@"style"];
	BOOL drawsLabel = [[self valueForKey:@"showsLabel"]boolValue];
	NSString *fontName = [self valueForKey:@"fontName"];
	float fontSize = [[self valueForKey:@"fontSize"] floatValue] * 0.0015625 * imageSize.width;*/
	
	// Draw the scale bar
	NSColor *mainColor = [NSColor whiteColor];
	//NSColor *mainColor = [self color]; // replace by this line to get the original implementation, self should be the scale bar object though

	[mainColor set];	
	
	NSRect scaleBarRect = NSMakeRect(xOriginOnImage - (lengthOnImage / 2.0), yOriginOnImage, lengthOnImage, strokeOnImage);
	[NSBezierPath fillRect:scaleBarRect];
	
	if ([scaleBarStyle isEqualTo:@"Zebra"]) {
		if ([mainColor isEqualTo:[NSColor blackColor]]) {
			[[NSColor whiteColor] set];
		}
		else {
			[[NSColor blackColor] set];
		}
		NSRect zebraRect = NSOffsetRect(scaleBarRect, 0.0, strokeOnImage/2.0);
		[NSBezierPath fillRect:zebraRect];
	}
	
	// Draw the label
	if (drawsLabel) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSFont fontWithName:fontName size:fontSize], NSFontAttributeName, mainColor, NSForegroundColorAttributeName, nil];
		NSString *string = [NSString stringWithFormat:@"%@ %@", [self valueForKey:@"length"], [self valueForKeyPath:@"image.calibration.unit"]];
		NSSize stringSize = [string sizeWithAttributes:dict];
		[string drawAtPoint:NSMakePoint(xOriginOnImage - (stringSize.width/2), yOriginOnImage - stringSize.height) withAttributes:dict];
	}	
}

- (void)drawCommentIndicator
{	
	NSPoint commentOriginPoint = NSMakePoint(10,10);
	CGFloat radius = 5.0;
	NSRect commentRect = NSMakeRect(commentOriginPoint.x, commentOriginPoint.y, radius * 2, radius * 2);
	NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:commentRect];
	
	[self setCommentIndicator:path];
	
	[[NSColor colorWithCalibratedWhite:0.7 alpha:1.0] set];
	[path stroke];
	[path fill];

}

- (void)drawSelectionRectangle
{	
	CGFloat zoomLevel = ((LightTableContentView *)[[self superview] superview]).zoomLevel;
	
	CGFloat lineWidth = ( 4.0 / zoomLevel);
	if (selectionRectangleOpacity == 0) return;
	
	NSPoint upperLeft = NSMakePoint([self bounds].origin.x + lineWidth/2,[self bounds].origin.y + [self bounds].size.height - lineWidth/2);
	NSPoint lowerLeft = NSMakePoint([self bounds].origin.x + lineWidth/2,[self bounds].origin.y + lineWidth/2);
	NSPoint upperRight = NSMakePoint([self bounds].origin.x + [self bounds].size.width - lineWidth/2,[self bounds].origin.y + [self bounds].size.height - lineWidth/2);
	NSPoint lowerRight = NSMakePoint([self bounds].origin.x + [self bounds].size.width - lineWidth/2,[self bounds].origin.y + lineWidth/2);
	
	
	NSBezierPath *boundsPath = [NSBezierPath bezierPath];
	
	[boundsPath moveToPoint:(upperLeft)];
	[boundsPath lineToPoint:(lowerLeft)];
	[boundsPath lineToPoint:(lowerRight)];
	[boundsPath lineToPoint:(upperRight)];
	[boundsPath lineToPoint:(upperLeft)];
	[boundsPath closePath]; 
	
	[boundsPath setLineWidth:lineWidth];

	[[NSColor colorWithCalibratedWhite:1.0 alpha:self.selectionRectangleOpacity] set];
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	[boundsPath stroke];
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
	
	
	
}

- (void)drawControlPoints
{
	CGFloat zoomLevel = ((LightTableContentView *)[[self superview] superview]).zoomLevel;

	
	if (controlPointsOpacity == 0) return;
	
	NSBezierPath *controlPointsPath = [NSBezierPath bezierPath];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:LowerLeftControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:MiddleLeftControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:UpperLeftControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:UpperMiddleControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:UpperRightControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:MiddleRightControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:LowerRightControlPoint]];
	[controlPointsPath appendBezierPath:[self pathForControlPoint:LowerMiddleControlPoint]];
	
	[controlPointsPath setLineWidth:0.5 / zoomLevel];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	[[NSColor colorWithCalibratedWhite:0.4 alpha:fmin(0.5, self.controlPointsOpacity)] set];
	[controlPointsPath fill];
	[[NSColor colorWithCalibratedWhite:1.0 alpha:self.controlPointsOpacity] set];
	[controlPointsPath stroke];
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
	
}

- (void)drawContextInfo
{
	if (self.imageSizeInfoBoxOpacity == 0) return;
	
	CGFloat zoomLevel = ((LightTableContentView *)[[self superview] superview]).zoomLevel;
	
	// image size / magnification / info...
	
	CGFloat magn = [[((LightTableImageView *)[self superview]).lightTableImage valueForKeyPath:@"imageMagnification"] floatValue];
	NSString *magnification = [NSString stringWithFormat:@"%1.1f%%",(magn * 100)];
	
	NSColor *textColor = [NSColor colorWithCalibratedWhite:1.0 alpha: self.imageSizeInfoBoxOpacity == 0 ? 0 : self.imageSizeInfoBoxOpacity+0.2];
	NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:textColor, NSForegroundColorAttributeName, [NSFont systemFontOfSize:12.0/zoomLevel], NSFontAttributeName,nil];
	
	NSSize boxSize = [magnification sizeWithAttributes:attrs];
	CGFloat margin = 10.0/zoomLevel;
	NSBezierPath *imageSizeBox = [NSBezierPath bezierPathWithRoundedRect:NSMakeRect([self bounds].size.width - boxSize.width - 2 * margin, margin, boxSize.width + margin, boxSize.height) 
																 xRadius:5.0/zoomLevel yRadius:5.0/zoomLevel];
	
	[[NSColor colorWithCalibratedWhite:0.3 alpha:self.imageSizeInfoBoxOpacity] set];
	[imageSizeBox fill];
	
	[magnification drawAtPoint:NSMakePoint([self bounds].size.width - boxSize.width - 1.5 * margin, margin) withAttributes:attrs];
	
}

- (void)setCommentIndicator:(NSBezierPath *)bezier
{
	if (commentIndicator != bezier) {
		[commentIndicator release];
		commentIndicator = [bezier retain];
	}
}

- (void)setControlPointsOpacity:(CGFloat)opacity
{
	controlPointsOpacity = opacity;
	[self setNeedsDisplay:YES];
}

- (void)setImageSizeInfoBoxOpacity:(CGFloat)opacity
{
	imageSizeInfoBoxOpacity = opacity;
	[self setNeedsDisplay:YES];
}

- (void)setSelectionRectangleOpacity:(CGFloat)opacity
{
	if (selectionRectangleOpacity != opacity) {
		
		/*NS_DURING
		[NSException raise:@"debug" format:@"debug"];
		NS_HANDLER
		[localException printStackTrace];
		NS_ENDHANDLER
		*/
		
	}
	selectionRectangleOpacity = opacity;
	[self setNeedsDisplay:YES];
}

- (void)setShowsScalebarIfAvailable:(BOOL)flag
{
	showsScalebarIfAvailable = flag;
	[self setNeedsDisplay:YES];
}


#pragma mark Mouse events and tracking areas
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse events and tracking areas
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	return [(LightTableImageView *)[self superview] menuForEvent:theEvent];
}


- (void)initializeTrackingAreas
{
	[self removeTrackingArea:viewTrackingArea];
	NSRect bounds = [self bounds];
	viewTrackingArea = [[[NSTrackingArea alloc] initWithRect:bounds 
													 options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow | NSTrackingInVisibleRect)
													   owner:self 
													userInfo:nil] autorelease];
	[self addTrackingArea:viewTrackingArea];
}

- (void)updateTrackingAreas
{
	[self initializeTrackingAreas];
}



@end
