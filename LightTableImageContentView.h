//
//  LightTableImageContentView.h
//  LightTable
//	
//	This class displays the image pixels of a light table image.
//	It is a subclass of NSImageView, modified to scale an image to fit.
//	It handles no events.
//	Its superview is a LightTableImageView.
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ExpandableImageView.h"


@interface LightTableImageContentView : ExpandableImageView 
{
	
	NSImage *image;
	CGFloat opacity;
    

}

@property(retain, readwrite) NSImage *image;


- (void)setOpacity:(CGFloat)val;
- (CGFloat)opacity;

@end
