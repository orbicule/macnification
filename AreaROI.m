//
//  AreaROI.m
//  Filament
//
//  Created by Peter Schols on 17/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "AreaROI.h"


@implementation AreaROI


- (NSNumber *)circularity;
{
	float area = [[self valueForKey:@"pixelArea"]floatValue];
	float perimeter = [[self valueForKey:@"pixelPerimeter"]floatValue];
	float circularity = 4*pi*(area/pow(perimeter, 2));
	return [NSNumber numberWithFloat:circularity];
}


- (NSNumber *)integratedDensityWhite;
{
	float area = [[self valueForKey:@"pixelArea"]floatValue];
	float averageWhite = [[self valueForKey:@"averageWhite"]floatValue];
	float integratedDensityWhite = area * averageWhite;
	return [NSNumber numberWithFloat:integratedDensityWhite];
}


- (NSNumber *)integratedDensityRed;
{
	float area = [[self valueForKey:@"pixelArea"]floatValue];
	float averageRed = [[self valueForKey:@"averageRed"]floatValue];
	float integratedDensityRed = area * averageRed;
	return [NSNumber numberWithFloat:integratedDensityRed];
}


- (NSNumber *)integratedDensityGreen;
{
	float area = [[self valueForKey:@"pixelArea"]floatValue];
	float averageGreen = [[self valueForKey:@"averageGreen"]floatValue];
	float integratedDensityGreen = area * averageGreen;
	return [NSNumber numberWithFloat:integratedDensityGreen];
}


- (NSNumber *)integratedDensityBlue;
{
	float area = [[self valueForKey:@"pixelArea"]floatValue];
	float averageBlue = [[self valueForKey:@"averageBlue"]floatValue];
	float integratedDensityBlue = area * averageBlue;
	return [NSNumber numberWithFloat:integratedDensityBlue];
}





@end
