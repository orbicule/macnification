//
//  PSGradientView.m
//  Filament
//
//  Created by Peter Schols on 09/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "PSGradientView.h"


@implementation PSGradientView


@synthesize startColor, endColor;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		self.startColor = [NSColor colorWithDeviceWhite:0.9 alpha:1.0];
		self.endColor = [NSColor colorWithDeviceWhite:0.7 alpha:1.0];
    }
    return self;
}

- (void)setStartColor:(NSColor *)aColor
{
	if (startColor != aColor) {
		
		[startColor release];
		startColor = [aColor retain];
	
		[self setNeedsDisplay:YES];

	}
	
}

- (void)setEndColor:(NSColor *)aColor
{
	if (endColor != aColor) {
		
		[endColor release];
		endColor = [aColor retain];
		
		[self setNeedsDisplay:YES];
	}
	
}

- (void)drawRect:(NSRect)rect 
{
	if (!self.startColor || !self.endColor) return;
	NSGradient *gradient = [[NSGradient alloc] initWithStartingColor:self.startColor endingColor:self.endColor];
	[gradient drawInRect:[self bounds] angle:-90];
	[gradient release];
}

- (void) dealloc
{
	self.startColor = nil;
	self.endColor = nil;
	
	[super dealloc];
}


@end
