//
//  CaptureOpenGLPreview.m
//  Filament
//
//  Created by Dennis Lorson on 25/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <OpenGL/gl.h>

#import "CaptureOpenGLPreview.h"


@interface CaptureOpenGLPreview ()

- (void)updateFrameTextureSizeWithImage:(unsigned char *)image width:(NSInteger)w height:(NSInteger)h;
- (void)updateFrameTextureWithImage:(unsigned char *)image width:(NSInteger)w height:(NSInteger)h;


- (NSRect)frameDestinationRect;

- (void)_generateTextures;

@end

@implementation CaptureOpenGLPreview


@synthesize delegate = delegate_;

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if ((self = [super initWithCoder:aDecoder])) {
				
		[[self openGLContext] makeCurrentContext];
		
		GLint swapInterval = 1;
		
		[[self openGLContext] setValues:&swapInterval forParameter:NSOpenGLCPSwapInterval];
		
		ciCtx_ = [[CIContext contextWithCGLContext:(CGLContextObj)[[self openGLContext] CGLContextObj] pixelFormat:NULL colorSpace:NULL options:NULL] retain];
        
        [self _generateTextures];
		
	}
	return self;
}

- (void)_generateTextures
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glGenTextures(1, &shadowTexture_);
    glBindTexture(GL_TEXTURE_2D, shadowTexture_);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    
    NSImage *shadow = [NSImage imageNamed:@"capturePreviewShadow"];
    
    NSBitmapImageRep *rep = [[shadow representations] count] ? [[shadow representations] objectAtIndex:0] : nil;
    
    CGImageRef img = [rep CGImage];
    
    CGFloat w = CGImageGetWidth(img);
    CGFloat h = CGImageGetHeight(img);
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, w, h, 8, w * 4, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedLast);
    
    CGContextDrawImage(ctx, CGRectMake(0, 0, w, h), img);
    
    if (ctx)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, CGBitmapContextGetData(ctx));
    
    CGContextRelease(ctx);
}

static int i = 0;
- (void)setImage:(CIImage *)image;
{
	if (image != image_) {
        
        if (imageBacking_)
            CVBufferRelease(imageBacking_);
        
        imageBacking_ = nil;
        
		[image_ release];
		image_ = [image retain];
		
		i++;
		if (i%3)
		[self drawFrame];
	}	
}

- (void)setImage:(CIImage *)image backingBuffer:(CVImageBufferRef)buffer;
{
    [self setImage:image];
    
    CVBufferRetain(buffer);
    
    imageBacking_ = buffer;
}


- (void)drawRect:(NSRect)rect
{
	//[self drawFrame];
}


- (NSRect)frameDestinationRect
{
	NSRect bounds = [self bounds];

	NSSize imgSize = NSMakeSize([image_ extent].size.width, [image_ extent].size.height);
	NSSize destinationSize;
	
	// compute the coordinates for the capture frame
	if (imgSize.width / bounds.size.width < imgSize.height / bounds.size.height) {
		
		destinationSize.height = bounds.size.height;
		destinationSize.width = bounds.size.height * imgSize.width / imgSize.height;
		
	} else {
		
		destinationSize.width = bounds.size.width;
		destinationSize.height = bounds.size.width * imgSize.height / imgSize.width;
	}
	
	NSRect destinationRect;
	destinationRect.size = destinationSize;
	destinationRect.origin.x = NSMinX(bounds) + (bounds.size.width - destinationSize.width) * 0.5;
	destinationRect.origin.y = NSMinY(bounds) + (bounds.size.height - destinationSize.height) * 0.5;
	
    destinationRect = NSIntegralRect(destinationRect);
    
	destinationRect.origin.x = floorf(destinationRect.origin.x) - 1.0;
	destinationRect.origin.y = floorf(destinationRect.origin.y) - 1.0;
	
	destinationRect.size.height = floorf(destinationRect.size.height) + 2.0;
	destinationRect.size.width = floorf(destinationRect.size.width) + 2.0;
	
	return destinationRect;
}

- (void)drawFrame
{		
	if (![self lockFocusIfCanDraw])
		return;
	
	
	[[self openGLContext] makeCurrentContext];
	
	
    NSRect bounds = NSIntegralRect([self bounds]);
	
	glViewport(0, 0, bounds.size.width , bounds.size.height);		
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    
    
	glOrtho(NSMinX(bounds), NSMaxX(bounds), NSMinY(bounds), NSMaxY(bounds), -1.0, 1.0);
	
    
    CGFloat brightness = 60./255.;
	
	glClearColor(brightness, brightness, brightness, 1.);
	glClear(GL_COLOR_BUFFER_BIT);
	
	[ciCtx_ drawImage:image_ inRect:NSRectToCGRect([self frameDestinationRect]) fromRect:[image_ extent]];

    CGFloat w = bounds.size.width;
    CGFloat h = bounds.size.height;

    CGFloat texHeight = 5;
    
    glBindTexture(GL_TEXTURE_2D, shadowTexture_);
    glColor4f(1., 1., 1., 1.);
    
    glBegin(GL_QUADS);
    
    
    /*glTexCoord2f(0, 0);
    glVertex2f(0, 0);
    glTexCoord2f(0, 1);
    glVertex2f(0, texHeight); 
    glTexCoord2f(1, 1);
    glVertex2f(w, texHeight); 
    glTexCoord2f(1, 0);
    glVertex2f(w, 0);*/
    
    
    glTexCoord2f(0, 0);
    glVertex2f(0, h);
    glTexCoord2f(0, 1);
    glVertex2f(0, h-texHeight); 
    glTexCoord2f(1, 1);
    glVertex2f(w, h-texHeight); 
    glTexCoord2f(1, 0);
    glVertex2f(w, h);


    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, 0);

    
	
	glFlush();
	
	glSwapAPPLE();
	
	[self unlockFocus];
	
	
}


- (void)update
{
	[super update];
		
	[self drawFrame];
}


- (void)mouseUp:(NSEvent *)theEvent
{
	[super mouseUp:theEvent];
	NSPoint loc = [theEvent locationInWindow];
	loc = [self convertPoint:loc fromView:nil];
	
	NSRect frameRect = [self frameDestinationRect];
	
	if (!NSPointInRect(loc, frameRect))
		return;
	
	NSPoint relativeLocation;
	relativeLocation.x = (loc.x - frameRect.origin.x)/frameRect.size.width;
	relativeLocation.y = (loc.y - frameRect.origin.y)/frameRect.size.height;
	
	if ([self.delegate respondsToSelector:@selector(view:wasPickedAtPoint:)])
		[self.delegate view:self wasPickedAtPoint:relativeLocation];
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}



- (void)setImage:(unsigned char *)image width:(NSInteger)w height:(NSInteger)h;
{	
	if (!image) return;
	
	if (frameSize_.width != w || frameSize_.height != h) {
		frameSize_.width = w;
		frameSize_.height = h;
		
		[self updateFrameTextureSizeWithImage:image width:w height:h];
	} else {
		[self updateFrameTextureWithImage:image width:w height:h];
	}
	
	[self drawFrame];
}



- (void)updateFrameTextureSizeWithImage:(unsigned char *)image width:(NSInteger)w height:(NSInteger)h;
{
	
	[[self openGLContext] makeCurrentContext];
	
	glBindTexture(GL_TEXTURE_2D, frameTexture_);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	
	int err;
	if ((err = glGetError()))
		NSLog(@"create texture: gl error %i", err);
	
	
	
}


- (void)updateFrameTextureWithImage:(unsigned char *)image width:(NSInteger)w height:(NSInteger)h;
{
	[[self openGLContext] makeCurrentContext];
	
	glBindTexture(GL_TEXTURE_2D, frameTexture_);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	
	int err;
	if ((err = glGetError()))
		NSLog(@"update: gl error %i", err);
	
}




@end
