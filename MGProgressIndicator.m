//
//  MGProgressIndicator.m
//  Filament
//
//  Created by Dennis Lorson on 18/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGProgressIndicator.h"


@implementation MGProgressIndicator

@synthesize progress;

- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {
		progress = 0;
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect 
{
	NSRect outerRect = NSInsetRect([self bounds], 3, 3);
    NSBezierPath *shell = [NSBezierPath bezierPathWithRoundedRect:outerRect xRadius:9 yRadius:9];
	
	[[NSColor colorWithCalibratedWhite:0.8 alpha:1.0] set];
	[shell setLineWidth:2.0];
	[shell stroke];
	
	NSRect innerRect = NSInsetRect(outerRect, 3, 3);
	NSBezierPath *fill = [NSBezierPath bezierPathWithRoundedRect:innerRect xRadius:5 yRadius:5];
	
	NSRect clipRect = innerRect;
	clipRect.size.width = progress * clipRect.size.width;
	NSBezierPath *clipArea = [NSBezierPath bezierPathWithRect:clipRect];
	[clipArea addClip];
	
	[fill fill];
	
}

- (void)setProgress:(CGFloat)val
{
	if (progress != val) {
		progress = val;
		[self setNeedsDisplay:YES];
	}
}

@end
