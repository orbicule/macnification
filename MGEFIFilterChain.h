//
//  MGEFIFilterChain.h
//  StackTable
//
//  Created by Dennis Lorson on 10/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/CoreImage.h>


@interface MGEFIFilterChain : NSObject {

}

+ (NSDictionary *)applyOnFirst:(NSDictionary *)firstSource second:(NSDictionary *)secondSource;
+ (CIImage *)applyOnImages:(NSArray *)images;


@end


@interface MGEFIFilterChain (Private)

+ (NSArray *)rec_applyOnImages:(NSArray *)images;

@end
