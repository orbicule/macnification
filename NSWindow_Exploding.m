//
//  PSExplodingWindow.m
//  Filament
//
//  Created by Peter Schols on 22/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "NSWindow_Exploding.h"


@implementation NSWindow (PSExplodingWindow)


- (void)explodeToFrame:(NSRect)frame;
{
	NSPoint origin = frame.origin;
	float finalWidth = frame.size.width;
	float finalHeight = frame.size.height;
	[NSAnimationContext beginGrouping];
	[[NSAnimationContext currentContext] setDuration:0.20]; 
	[[self animator] setFrame:NSMakeRect(origin.x, origin.y, finalWidth * 1.10, finalHeight * 1.10) display:YES];
	[NSAnimationContext endGrouping];
	
	[self performSelector:@selector(animateCommentWindow:) withObject:[NSValue valueWithRect:frame] afterDelay:0.20];
}


- (void)animateCommentWindow:(NSValue *)value;
{
	NSRect frame = [value rectValue];
	[NSAnimationContext beginGrouping];
	[[NSAnimationContext currentContext] setDuration:0.18]; 
	[[self animator] setFrame:frame display:YES];
	[NSAnimationContext endGrouping];
	[self performSelector:@selector(makeKeyWindow) withObject:nil afterDelay:0.30];
}


- (void)implode;
{
	[NSAnimationContext beginGrouping];
	[[NSAnimationContext currentContext] setDuration:0.10]; 
	[[self animator] setFrame:NSMakeRect([self frame].origin.x, [self frame].origin.y - [self frame].size.height/2, 120, 145) display:NO];
	[NSAnimationContext endGrouping];
}




@end
