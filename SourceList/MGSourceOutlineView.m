#import "MGSourceOutlineView.h"





@implementation MGSourceOutlineView


@synthesize eventHandlingDelegate;

- (void) awakeFromNib
{
	[self setIndentationPerLevel:10];
	
}


// Show the context menu
- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	[super menuForEvent:theEvent];
	return [self menu];	
}





- (void)highlightSelectionInClipRect:(NSRect)rect
{
	NSGradient *grad;
	
	if ([[self window] firstResponder] == self) {
		
		grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.90 alpha:1.0]
											  endingColor:[NSColor colorWithCalibratedWhite:1.0 alpha:1.0]] autorelease];
		
	} else {
	
		grad = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.87 alpha:1.0]
											  endingColor:[NSColor colorWithCalibratedWhite:0.91 alpha:1.0]] autorelease];
		
	}
	
	[grad drawInRect:[self rectOfRow:[self selectedRow]] angle:-90];
	
}

// Needed to avoid a bug when dragging and dropping collections
- (void)draggedImage:(NSImage *)anImage endedAt:(NSPoint)aPoint operation:(NSDragOperation)operation;
{
	
}

- (IBAction)changeBG:(id)sender
{
	[self setBackgroundColor:[sender color]];
}




# pragma mark -
# pragma mark Event handling (predicate window)

- (void)mouseDown:(NSEvent *)theEvent
{
	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	NSInteger row = [self rowAtPoint:location];
	
	if ([self.eventHandlingDelegate respondsToSelector:@selector(sourceOutlineView:wasClickedAtPoint:forRow:)])
		if ([self.eventHandlingDelegate sourceOutlineView:self wasClickedAtPoint:location forRow:row])
			[super mouseDown:theEvent];
	
}



@end
