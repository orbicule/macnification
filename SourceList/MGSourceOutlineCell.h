//
//  LNSSourceListSmallCell.h
//  SourceList
//
//  Created by Mark Alldritt on 07/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGSourceOutlineCell : NSTextFieldCell 
{

	NSImage *image;
	NSImage *badgeImage;
	
}

@property (readwrite, retain) NSImage *badgeImage;
@property (readwrite, retain) NSImage *image;

@end
