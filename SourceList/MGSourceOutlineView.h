/* LNSSourceListView */

#import <Cocoa/Cocoa.h>

@class MGSourceOutlineView;

@protocol MGSourceOutlineViewDelegate <NSObject>

// returns YES if normal event handling should continue
- (BOOL)sourceOutlineView:(MGSourceOutlineView *)cell wasClickedAtPoint:(NSPoint)point forRow:(NSInteger)row;

@end


@interface MGSourceOutlineView : NSOutlineView
{
	id <MGSourceOutlineViewDelegate> eventHandlingDelegate;
}

@property (readwrite, assign) id <MGSourceOutlineViewDelegate> eventHandlingDelegate;

- (IBAction)changeBG:(id)sender;


@end
