//
//  LNSSourceListCell.m
//  SourceList
//
//  Created by Mark Alldritt on 07/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "MGSourceOutlineCell.h"
#import "MGSourceOutlineView.h"
#import "NSAttributedString_MGExtensions.h"


@interface MGSourceOutlineCell ()

- (CGFloat)imageWidth;
- (CGFloat)badgeImageWidth;

@end



@implementation MGSourceOutlineCell


@synthesize badgeImage, image;

// Used to prevent the browser cell from drawing its branch arrow
+ (NSImage*)branchImage { return nil; }
+ (NSImage*)highlightedBranchImage { return nil; }



- (void) dealloc
{
	// this causes a crash...
	//self.image = nil;
	//self.badgeImage = nil;
	
	[super dealloc];
}


#pragma mark -
#pragma mark Event handling



#pragma mark -
#pragma mark Drawing

- (NSColor*) highlightColorWithFrame:(NSRect) cellFrame inView:(NSView*) controlView
{
	//	The table view does the highlighting.  Returning nil seems to stop the cell from
	//	attempting th highlight the row.
	return nil;
}

- (NSColor *)highlightColorInView:(NSView *)controlView
{
	// This NSBrowserCell's equivalent to highlighColorWithFrame 
	return nil;
}


- (void)setPlaceholderString:(NSString *)string;
{
	// To avoid runtime errors
}


- (void)editWithFrame:(NSRect)aRect inView:(NSView *)controlView editor:(NSText *)textObj delegate:(id)anObject event:(NSEvent *)theEvent;
{
	[self setFont:[NSFont boldSystemFontOfSize:11]];

	aRect.origin.y += 4;
	aRect.size.height -= 6;
	aRect.origin.x += 25;
	
	if (self.badgeImage)
		aRect.size.width -= 22;
	
	[textObj setFocusRingType:NSFocusRingTypeExterior];
	[super editWithFrame:aRect inView:controlView editor:textObj delegate:anObject event:theEvent];
	
	[textObj selectAll:self];

}

- (void)selectWithFrame:(NSRect)aRect inView:(NSView *)controlView editor:(NSText *)textObj delegate:(id)anObject start:(NSInteger)selStart length:(NSInteger)selLength;
{
	[self setFont:[NSFont boldSystemFontOfSize:11]];

	aRect.origin.y += 4;
	aRect.size.height -= 8;
	aRect.origin.x += 25;
	aRect.size.width -= 22;
	
	if (self.badgeImage)
		aRect.size.width -= 22;
	
	[textObj setFocusRingType:NSFocusRingTypeExterior];

	[super selectWithFrame:aRect inView:controlView editor:textObj delegate:anObject start:selStart length:selLength];
}

- (void)drawInteriorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	NSParameterAssert([controlView isKindOfClass:[MGSourceOutlineView class]]);
	
	NSFontManager* fontManager = [NSFontManager sharedFontManager];
	NSString* title = [self stringValue];
    
	NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    if ([[self attributedStringValue] length])
        attrs = [NSMutableDictionary dictionaryWithDictionary:[[self attributedStringValue] attributesAtIndex:0 effectiveRange:NULL]];
	[attrs setValue:[NSFont fontWithName:@"Lucida Grande" size:11] forKey:NSFontAttributeName];
	
		
	if ([self isHighlighted]) {
		NSFont* font = [attrs objectForKey:NSFontAttributeName];
		[attrs setValue:[fontManager convertFont:font toHaveTrait:NSBoldFontMask] forKey:NSFontAttributeName];
		[attrs setValue:[NSColor blackColor] forKey:NSForegroundColorAttributeName];
	} else {
		[attrs setValue:[NSColor blackColor] forKey:NSForegroundColorAttributeName];
	}
    
    [attrs removeObjectForKey:NSShadowAttributeName];
	
	NSSize titleSize = [title sizeWithAttributes:attrs];
	
	NSRect inset = cellFrame;
	
	
	inset.size.height = titleSize.height;
	inset.origin.y = NSMinY(cellFrame) + (NSHeight(cellFrame) - titleSize.height) / 2.0;
	inset.origin.x += [self imageWidth];
	inset.size.width -= ([self imageWidth] + [self badgeImageWidth]);	
	inset.origin.y += 1;
	
	[title drawInRect:inset withAttributes:attrs];
	
	
	// icon image
	if (self.image)
		[self.image compositeToPoint:NSMakePoint(floor(inset.origin.x-23), floor(inset.origin.y+14)) operation:NSCompositeSourceOver];
	
	// badge image
	if (self.badgeImage) {
		NSPoint origin = NSMakePoint(NSMaxX(inset) + 4, floor(inset.origin.y+11));
		[self.badgeImage compositeToPoint:origin operation:NSCompositeSourceOver];
	}
	
	
}

- (CGFloat)imageWidth
{
	return 27.0;
}

- (CGFloat)badgeImageWidth
{
	return (self.badgeImage ? 15 : 0);
}

@end
