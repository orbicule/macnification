//
//  PSHUDTableCell.m
//  Filament
//
//  Created by Peter Schols on 23/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "PSHUDTableCell.h"


@implementation PSHUDTableCell


- (NSColor*) highlightColorWithFrame:(NSRect) cellFrame inView:(NSView*) controlView
{
	//	The table view does the highlighting.  Returning nil seems to stop the cell from
	//	attempting th highlight the row.
	return nil;
}

- (NSColor *)highlightColorInView:(NSView *)controlView
{
	// This NSBrowserCell's equivalent to highlighColorWithFrame 
	return nil;
}



- (void)selectWithFrame:(NSRect)aRect inView:(NSView *)controlView editor:(NSText *)textObj delegate:(id)anObject start:(NSInteger)selStart length:(NSInteger)selLength;
{
	[self setTextColor:[NSColor whiteColor]];
	[self setBackgroundColor:[NSColor colorWithCalibratedRed:0.1 green:0.1 blue:0.1 alpha:0.75]];
	[super selectWithFrame:aRect inView:controlView editor:textObj delegate:anObject start:selStart length:selLength];
}




@end
