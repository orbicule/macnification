//
//  Keyword.h
//  Macnification
//
//  Created by Peter Schols on 14/03/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KSExtensibleManagedObject.h"


@interface Keyword : KSExtensibleManagedObject {

}


- (NSString *)entireAncestry;
- (NSArray *)entireAncestryArray;
- (NSArray *)entireAncestryObjectArray;



@end
