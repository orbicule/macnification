//
//  MGMDRatingView.h
//  MetaDataView
//
//  Created by Dennis on 10/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MGMDView.h"


@interface MGMDRatingView : NSView <MGMDManualObserver>{
	
	NSInteger rating;
	BOOL isAverage;
	
	NSImage *ratingImage, *ratingHalfImage;
	
	id observableController;
	NSString *observableKeyPath;
	
	BOOL enabled;

}

@property(nonatomic) BOOL enabled;

- (NSInteger)ratingForPoint:(NSPoint)thePoint;

- (void)setRating:(NSInteger)theRating;
- (NSInteger)rating;


@end
