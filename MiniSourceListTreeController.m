//
//  MiniSourceListTreeController.m
//  Filament
//
//  Created by Dennis Lorson on 08/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MiniSourceListTreeController.h"
#import "MGSourceOutlineCell.h"
#import "Album.h"
#import "MGAppDelegate.h"
#import "ImageArrayController.h"
#import "MGLibraryController.h"

@implementation MiniSourceListTreeController



- (void)awakeFromNib;
{
	[self addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial context:NULL];
	
	[self bind:@"managedObjectContext" toObject:MGLibraryControllerInstance withKeyPath:@"managedObjectContext" options:nil];
	
	// Sort the source list
	[self setSortDescriptors:sortDescriptors];
	
	// do an initial load of the items in the outline view, and expand the standard items
	[self expandStandardGroups];
	
	
	[[[sourceOutlineView enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[sourceOutlineView enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
	
	// Register as an observer for our selected objects
	//[self addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew) context:NULL];

}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
{	
	
	//NSLog(@"%@: content refresh", [self description]);

    ;

	
	if ([keyPath isEqualTo:@"arrangedObjects"]) {
		
		
		// check the amount of children in the projects collection
		// if there are projects, make that group available
		[mainSourceListItems removeObject:projectsGroupItem];
		id projects = [[MGLibraryControllerInstance library] projectsCollection];
		if ([[projects valueForKey:@"children"] count] > 0)
			[mainSourceListItems addObject:projectsGroupItem];
		
		[self expandStandardGroups];
		
		// reload the source list
		[sourceOutlineView reloadData];
		
		if (!initialSourceListExpansionDone) {
			
			// select the library collection
			[self selectItem:[[MGLibraryControllerInstance library] libraryGroup]];
			
			initialSourceListExpansionDone = YES;
			
			[sourceOutlineView setAutosaveName:@"sourceListItems"];
			[sourceOutlineView setAutosaveExpandedItems:YES];			
			
		}
		
	}
	
	else {
		
		// SUPER OBSERVEVALUE CHANGED HERE
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
		
	}
}


// do not display an action icon next to smart folders

- (NSCell *)outlineView:(NSOutlineView *)outlineView dataCellForTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	if (!tableColumn)
		return nil;
	
	if (![[tableColumn identifier] isEqualToString:@"name"])
		return nil;
	
	if ([self outlineView:outlineView isGroupItem:item]) {
		
		id cell = [[[NSTextFieldCell alloc] init]autorelease];
		[cell setFont:[NSFont systemFontOfSize:11]];
		return cell;	
		
	}
	
	else {
		
		MGSourceOutlineCell* smallCell = [[[MGSourceOutlineCell alloc] init] autorelease];
		[smallCell setImage:[(Album *)item iconImage]];
		[smallCell setFont:[[tableColumn dataCell] font]];
		[smallCell setLineBreakMode:[[tableColumn dataCell] lineBreakMode]];
		[smallCell setFocusRingType:NSFocusRingTypeExterior];
		[smallCell setEditable:YES];

		return smallCell;	
	
        
        
	}
	
	
	return nil;
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{
    ;

	id item = [self selectedCollection];
	
	SourceListTreeController *largeController = [MGLibraryControllerInstance sourcelistTreeController];
	
	[largeController selectItem:item];

	
}


@end
