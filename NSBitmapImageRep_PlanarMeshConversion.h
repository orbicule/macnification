//
//  NSBitmapImageRep_PlanarMeshConversion.h
//  Filament
//
//  Created by Dennis Lorson on 22/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSBitmapImageRep (PlanarMeshConversion)


+ (NSBitmapImageRep *)imageRepWithMergedChannels:(NSArray *)channels;
- (NSArray *)splitChannels;





@end
