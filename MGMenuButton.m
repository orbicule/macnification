//
//  MGMenuButton.m
//  MetaDataView
//
//  Created by Dennis on 28/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGMenuButton.h"


@implementation MGMenuButton




- (void)openMenu:(id)sender
{
	NSEvent *currentEvent = [NSApp currentEvent];
	
	NSInteger windowNb = [[self window] windowNumber];
	
	NSPoint newLoc = [self convertPoint:NSMakePoint(NSMinX([self bounds]), NSMaxY([self bounds])) toView:nil];
	
	NSEvent *adjustedEvent = [NSEvent mouseEventWithType:[currentEvent type]
												location:newLoc
										   modifierFlags:[currentEvent modifierFlags] 
											   timestamp:[currentEvent timestamp] 
											windowNumber:windowNb 
												 context:[currentEvent context] 
											 eventNumber:[currentEvent eventNumber] 
											  clickCount:[currentEvent clickCount]
												pressure:[currentEvent pressure]];
	
	
	[NSMenu popUpContextMenu:buttonMenu withEvent:adjustedEvent forView:self];	
	
}

- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		
		[self setTarget:self];
		[self setAction:@selector(openMenu:)];
		
	}
	
	return self;
	
}

@end
