/* StatisticsController */

#import <Cocoa/Cocoa.h>

@interface StatisticsController : NSObject {

	NSArray *roiArray;
	
	IBOutlet NSArrayController *roiController;
	IBOutlet NSWindow *statisticsWindow;
	
	NSString *pasteBoardString;
    IBOutlet NSPopUpButton *popup;
	
	IBOutlet NSTextField *countField;
	IBOutlet NSTextField *minimumField;
	IBOutlet NSTextField *maximumField;
	IBOutlet NSTextField *averageField;
	IBOutlet NSTextField *deviationField;
	IBOutlet NSTextField *errorField;
}


// Action invoked by the popup
- (IBAction)popUpDidChangeSelection:(id)sender;
- (IBAction)copyStatisticsToClipboard:(id)sender;
- (void)updateStatistics;



// Calculations
- (void)updateStatistics;
- (NSNumber *)calculateCountForKey:(NSString *)key;
- (NSNumber *)calculateMinimumForKey:(NSString *)key;
- (NSNumber *)calculateMaximumForKey:(NSString *)key;
- (NSNumber *)calculateAverageForKey:(NSString *)key;
- (NSNumber *)calculateDeviationForKey:(NSString *)key;
- (NSNumber *)calculateErrorForKey:(NSString *)key;


// Accessors
- (NSString *)pasteBoardString;
- (void)setPasteBoardString:(NSString *)value;




@end
