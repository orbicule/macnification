//
//  ExpandableImageView.m
//  LightTable
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "ExpandableImageView.h"
#import "NSImage_MGAdditions.h"

#import "QuartzCore/QuartzCore.h"

@implementation ExpandableImageView




- (void)setAlphaValue:(CGFloat)alpha
{
	[super setAlphaValue:alpha];
	[self setNeedsDisplay:YES];
}

- (id)initWithFrame:(NSRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		_scaling = NSScaleProportionally;
	}
	
	return self;
}

- (void)dealloc
{
	[_image release];
	[super dealloc];
}

- (void) setImage:(NSImage*)image
{
	if (_image) {
		[_image autorelease];
		_image = nil;
	}
	
	[image setCacheMode: NSImageCacheNever];
	
	_image = [image retain];
	[_image setScalesWhenResized:YES];
	[self setNeedsDisplay:YES];
}

- (NSImage*)image
{
	return _image;
}

- (void)setImageScaling:(NSImageScaling)newScaling
{
	_scaling = newScaling;
	[self setNeedsDisplay:YES];
}

- (NSImageScaling)imageScaling
{
	return _scaling;
}

- (void) drawRect:(NSRect)rects
{
	//[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationLow];

	NSRect bounds = [self bounds];

	if (_image) {
		//NSSize imageRepSize = [imageRep size];		
		NSSize size = [self bounds].size;
		NSPoint pt;
		pt.x = (bounds.size.width - size.width) / 2;
		pt.y = (bounds.size.height - size.height) / 2;
		
		NSRect imageRect;
		imageRect.origin = NSZeroPoint;
		imageRect.size = size;
		
		//[copy drawInRect:NSMakeRect(pt.x,pt.y,size.width,size.height)];
		[_image drawInRect:NSMakeRect(pt.x,pt.y,size.width,size.height) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:[self alphaValue]];

	}
}



@end