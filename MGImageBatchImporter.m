//
//  ImageImporter.m
//  Macnification
//
//  Created by Peter Schols on 30/10/06.
//  Copyright 2006 Biovolution. All rights reserved.
//

#import <dispatch/dispatch.h>

#import "MGAppDelegate.h"
#import "MGLibraryController.h"
#import "MGImageBatchImporter.h"
#import "StringExtensions.h"
#import "Stack.h"
#import "Image.h"
#import "NSBitmapImageRep_Conversion.h"
#import "MGImageFileImporter.h"
#import "MGMetadataSet.h"
#import "MGLibraryController.h"


@interface MGImageBatchImporter ()


- (NSArray *)_threaded_importImageFileWithImporter:(MGImageFileImporter *)importer intoAlbums:(NSSet *)albums recursive:(BOOL)recursive;


// Copy image files
- (NSString *)_importFolderName;
- (NSString *)_copyFileFromImporter:(MGImageFileImporter *)importer toFilamentLibraryInFolder:(NSString *)folderName;
- (void)_writeImageRep:(NSBitmapImageRep *)rep toFile:(NSString *)filePath compressed:(BOOL)compressed;


// utilities
- (NSString *)_relativePathFromAbsolutePath:(NSString *)absolute root:(NSString *)root;
- (NSString *)_folderStringFromRelativeString:(NSString *)relative;
- (NSDictionary *)_singleFolderLevelDictionaryFromLeafFilePaths:(NSArray *)leafFiles;
- (NSString *)_commonAncestorForFilePaths:(NSArray *)filePaths;

- (NSArray *)_extractImagePathsFromFileList:(NSArray *)filesAndDirectories;
- (NSArray *)_sizesOfFilesAtPaths:(NSArray *)paths totalSize:(unsigned long long *)total;
- (NSArray *)_removeDuplicateFiles:(NSArray *)original;

- (NSManagedObjectContext *)_managedObjectContext;
- (Image *)_createEmptyImage;


- (void)_setProgressForCurrentImage:(float)progress;
- (void)_setOverallProgress:(float)progress;

@end



@implementation MGImageBatchImporter


@synthesize delegate = delegate_;

@synthesize splitChannels;


@synthesize showsProgressSheet = showsProgressSheet_;
@synthesize album = album_;
@synthesize libraryAlbum = libraryAlbum_;
@synthesize lastImportAlbum = lastImportAlbum_;
@synthesize projectsAlbum = projectsAlbum_;


# pragma mark -
# pragma mark Init/dealloc

- (id) init
{
	self = [super init];
	if (self != nil) {
		self.showsProgressSheet = YES;
        


        
	}
	return self;
}


- (void) dealloc
{
	self.album = nil;
	self.projectsAlbum = nil;
	self.lastImportAlbum = nil;
	self.libraryAlbum = nil;
	
	[cumulativeImageFileSizes_ release];
	[imageFileSizes_ release];

	[super dealloc];
}


- (NSManagedObjectContext *)_managedObjectContext
{
	return [MGLibraryControllerInstance managedObjectContext];
}


+ (NSArray *)supportedFileTypes;
{
	return [MGImageFileImporter supportedPathExtensions];
}




# pragma mark -
# pragma mark Import



// Parses an array of strings (paths to image files), create images and add them to the Library and to the currently selected album
- (void)importImageFilesAndDirectories:(NSArray *)files
{

    [self retain];
    [self performSelectorInBackground:@selector(_threaded_importImageFilesAndDirectories_:) withObject:files];
    
}

- (void)_threaded_importImageFilesAndDirectories_:(NSArray *)files
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
 
    
    NSAssert(self.libraryAlbum, @"Should provide a Library!");
	NSAssert(self.lastImportAlbum, @"Should provide a Last Import Group!");
    
    
    // -------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------

    
    // reset the "last import" folder
	[self.lastImportAlbum setValue:nil forKey:@"images"];
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	NSArray *imageFilesToImport = [self _removeDuplicateFiles:[self _extractImagePathsFromFileList:files]];
    
    // if it's a group (project) or the projects area, create folders
	NSDictionary *singleFolderLevelFiles = nil;
    if ([self.album isGroup] || self.album == self.projectsAlbum) {
            singleFolderLevelFiles = [self _singleFolderLevelDictionaryFromLeafFilePaths:imageFilesToImport];        
    }
    
    
	// Set the image's album and add the "Library" and "Last Import" albums by default
	NSMutableSet *albums = [NSMutableSet set];
	[albums addObject:self.libraryAlbum];
	[albums addObject:self.lastImportAlbum];
	if (self.album != nil && ![self.album isSmartAlbum] && ![self.album isLightTable] && ![self.album isGroup])
		[albums addObject:self.album];
    
	
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
	// Get file sizes and integrate them
	NSArray *fileSizes = [[self _sizesOfFilesAtPaths:imageFilesToImport totalSize:&totalImageFileSize_] retain];
	NSMutableArray *integratedSizes = [NSMutableArray array];
	unsigned long long cumulSize = 0;
	for (NSNumber *fileSize in fileSizes) {
		[integratedSizes addObject:[NSNumber numberWithUnsignedLongLong:cumulSize]];
		cumulSize += [fileSize unsignedLongLongValue];
	}
	
	[cumulativeImageFileSizes_ release];
	cumulativeImageFileSizes_ = [integratedSizes retain];
	[imageFileSizes_ release];
	imageFileSizes_ = [fileSizes retain];
	
	
	currentImageFileIndex_ = 0;
	numberOfImageFilesToImport_ = [imageFilesToImport count];
	
	// Show the progress sheet
	if (self.showsProgressSheet) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [MGLibraryControllerInstance showProgressSheet];
            [MGLibraryControllerInstance setProgressTitle:@"Importing images"];
            [MGLibraryControllerInstance setProgressMessage:@"Analyzing..."];
            [self _setOverallProgress:-1];
        });
	}
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
	
    NSMutableArray *allImportedImageObjects = [NSMutableArray array];
    

    
    if (singleFolderLevelFiles) {
        
        for (NSString *key in [singleFolderLevelFiles allKeys]) {
            
            __block Album *newFolder = nil;
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                newFolder = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]];
                [newFolder setValue:key forKey:@"name"];
                [newFolder setValue:self.album forKey:@"parent"];
                [self.album addChildToEnd:newFolder];
                
                // avoid processing at the end of the runloop (this would otherwise not execute in this block so it could conflict with the import thread)
                [[MGLibraryControllerInstance managedObjectContext] processPendingChanges];
                
            });
            

            
            NSSet *adjustedAlbums = [albums setByAddingObject:newFolder];
            
            
            for (NSString *filePath in [singleFolderLevelFiles objectForKey:key]) {
                
                
                // Create an autorelease pool for this loop
                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                
                MGImageFileImporter *importer = [MGImageFileImporter fileImporterWithPath:filePath];
                
                NSArray *importedImagesFromFile = [self _threaded_importImageFileWithImporter:importer intoAlbums:adjustedAlbums recursive:NO];
                
                  [allImportedImageObjects addObjectsFromArray:importedImagesFromFile];
                
                
                [pool release];
                currentImageFileIndex_++;
                
            }
            
        }
        
        
    }
    
    
    else {
        
        for (NSString *file in imageFilesToImport) {
            
            // Create an autorelease pool for this loop
            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
            
            MGImageFileImporter *importer = [MGImageFileImporter fileImporterWithPath:file];
            
            NSArray *importedImagesFromFile = [self _threaded_importImageFileWithImporter:importer intoAlbums:albums recursive:NO];
            [allImportedImageObjects addObjectsFromArray:importedImagesFromFile];
            
            
            [pool release];
            currentImageFileIndex_++;
        }
    }
	
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [self _setOverallProgress:-1];
        [MGLibraryControllerInstance setProgressTitle:@"Finishing import"];
        [MGLibraryControllerInstance setProgressMessage:@"Saving Library..."];
        
        NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/Macnification"] stringByExpandingTildeInPath];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:path])
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
        
        [MGLibraryControllerInstance save];
        
        // Hide the Progress sheet
        if (self.showsProgressSheet)
            [MGLibraryControllerInstance hideProgressSheet];
        
    });
	

	
        
    [allImportedImageObjects retain];
    
    [pool drain];


    
    [self performSelectorOnMainThread:@selector(_finishImport:) withObject:allImportedImageObjects waitUntilDone:NO];

}

- (void)_finishImport:(NSArray *)importedImages
{
    [importedImages autorelease];

    [self autorelease];
    
    if ([self.delegate respondsToSelector:@selector(imageImporter:didImportImages:)])
        [self.delegate imageImporter:self didImportImages:importedImages];
}



// returns the imported Image objects
- (NSArray *)_threaded_importImageFileWithImporter:(MGImageFileImporter *)importer intoAlbums:(NSSet *)albums recursive:(BOOL)recursive;
{

	float preRepresentationProgressFraction = [importer fractionOfImportTimeForNonRepresentationWork];

	
	[MGLibraryControllerInstance setProgressTitle:[NSString stringWithFormat:@"Importing %@ (%d of %d)", 
																[[importer originalFilePath] lastPathComponent], currentImageFileIndex_ + 1, numberOfImageFilesToImport_]];
	
	
	if (preRepresentationProgressFraction > 0.0 && !recursive)
		[MGLibraryControllerInstance setProgressMessage:@"Reading original file format..."];
	else
		[MGLibraryControllerInstance setProgressMessage:@"Copying image data..."];

	
	MGMetadataSet *metadata = [importer metadata];
		
	if (!recursive)
		[self _setProgressForCurrentImage:preRepresentationProgressFraction];
	
	
	
	
	// reset the rep counter, but not if we're in recursive mode
	if (!recursive) {
		currentImageIndex_ = 0;
		numberOfImagesToImport_ = [importer numberOfImageRepresentations];
	}
	
	
	
	// sub-importers: recursive
	
	if ([importer subImporters]) {
		
		NSMutableArray *imported = [NSMutableArray array];
		for (MGImageFileImporter *subImporter in [importer subImporters]) 
			[imported addObjectsFromArray:[self _threaded_importImageFileWithImporter:subImporter intoAlbums:albums recursive:YES]];
		return imported;
	}
	
	

	
	
	NSMutableArray *importedImages = [NSMutableArray array];

	
	int nRepresentations = [importer numberOfImageRepresentations];
	int nChannels = [metadata numberOfChannels];
	
	NSMutableArray *channels = [NSMutableArray array];
	for (int i = 0; i < nChannels; i++) {
      [channels addObject:[NSMutableArray array]];
  }
	
	NSMutableArray *leftOvers = [NSMutableArray array];
	[MGLibraryControllerInstance setProgressMessage:@"Copying image data..."];
    	
	for (int i = 0; i < nRepresentations; i++) {
		
        
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		
        NSBitmapImageRep *rep = [importer imageRepresentationAtIndex:i];
		int channelIndex = [metadata channelForRepresentationAtIndex:i];
        
		
		if (!rep)
			continue;
		
		__block Image *image = nil;
        
        // This block should be executed on main (both fast and updating)
        dispatch_sync(dispatch_get_main_queue(), ^{
            image = [self _createEmptyImage];
            
            [image applyOriginalImagePathWithFileName:[importer destinationName] extension:[importer compressedDestinationPathExtension]];
            
            // avoid processing at the end of the runloop (this would otherwise not execute in this block so it could conflict with the import thread)
            [[MGLibraryControllerInstance managedObjectContext] processPendingChanges];

        });
        
        
        [importedImages addObject:image];
        
        [self _writeImageRep:rep toFile:[image originalImagePathExpanded:YES] compressed:[importer destinationNeedsCompression]];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [image updateThumbnail];
            
            // avoid processing at the end of the runloop (this would otherwise not execute in this block so it could conflict with the import thread)
            [[MGLibraryControllerInstance managedObjectContext] processPendingChanges];
            
        });
		
		if (channelIndex != -1 && channelIndex < nChannels)
			[(NSMutableArray *)[channels objectAtIndex:channelIndex] addObject:image];
		else
			[leftOvers addObject:image];
        
		float progress = preRepresentationProgressFraction + (1.0 - preRepresentationProgressFraction) * (float)(currentImageIndex_ + 1)/(float)numberOfImagesToImport_;
		
		[self _setProgressForCurrentImage:progress];
        
		currentImageIndex_++;
        
        
        [pool drain];
        
		
	}
	
    
    // This entire block should be executed on main (both fast and updating)
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        
        
        [metadata applyToImages:importedImages];
        
        
        if (!recursive)
            [self _setProgressForCurrentImage:1.0];
        
        
        // Do we need to stack?
        BOOL needsStacking = YES;
        
        if (nRepresentations <= nChannels)
            needsStacking = NO;
        
        for (NSMutableArray *channel in channels)
            if ([channel count] != [[channels objectAtIndex:0] count])
                needsStacking = NO;
        
        
        if (needsStacking) {
            
            NSMutableArray *stacks = [NSMutableArray array];
            
            int i = 0;
            
            for (NSArray *channel in channels) {
                
                if ([channel count] == 0)
                    continue;
                
                Stack *stack = [NSEntityDescription insertNewObjectForEntityForName:@"Stack" inManagedObjectContext:[self _managedObjectContext]];
                [stack addImages:channel];
                [stacks addObject:stack];
                
                NSString *stackName = [NSString stringWithFormat:@"%@%@", [metadata valueForKey:MGMetadataImageNameKey], (nChannels > 1 ? [NSString stringWithFormat:@" - ch%i", i] : @"")];
                [stack setValue:stackName forKey:@"name"];
                
                i++;
                
            }
            
            for (Album *album in albums) {
                
                
                [album addImages:stacks];
                // add the leftover images directly
                [album addImages:leftOvers];
            }
            
        }
        
        else {
            for (Album *album in albums)
                [album addImages:importedImages];
        }
        
        
        // avoid processing at the end of the runloop (this would otherwise not execute in this block so it could conflict with the import thread)
        [[MGLibraryControllerInstance managedObjectContext] processPendingChanges];
        
        
    });
    
	


	
	return importedImages;
}




# pragma mark -
# pragma mark File system tools


- (NSString *)_relativePathFromAbsolutePath:(NSString *)absolute root:(NSString *)root
{
    absolute = [[absolute stringByStandardizingPath] stringByAbbreviatingWithTildeInPath];
    root = [[root stringByStandardizingPath] stringByAbbreviatingWithTildeInPath];
    
    NSString *relative = [absolute stringByReplacingOccurrencesOfString:root withString:@"" options:NSAnchoredSearch range:NSMakeRange(0, [absolute length])];
    
    return relative;
}

- (NSString *)_folderStringFromRelativeString:(NSString *)relative
{
    NSString *result = [relative stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];
    if ([result length] == 0)
        result = @"untitled folder";
    
    return result;
}

// Used to create a single-level folder list; i.e. the entire folder structure is flattened
// Each file is put into a folder with name relative/path/to/file (relative to top folder)

- (NSDictionary *)_singleFolderLevelDictionaryFromLeafFilePaths:(NSArray *)leafFiles
{
    if ([leafFiles count] == 0)
        return [NSDictionary dictionary];
    
    // the top (root) folder is the longest one that all files have in common
    NSString *rootPath = [self _commonAncestorForFilePaths:leafFiles];
    
        
    NSMutableDictionary *folderNamesAndFiles = [NSMutableDictionary dictionary];
    
    for (NSString *file in leafFiles) {
        
        NSString *path = [file stringByDeletingLastPathComponent];
        
        NSString *relativePath = [self _relativePathFromAbsolutePath:path root:rootPath];
        NSString *displayPath = [self _folderStringFromRelativeString:relativePath];
        
        NSMutableArray *otherFiles = [folderNamesAndFiles objectForKey:displayPath];
        
        if (!otherFiles) {
            otherFiles = [NSMutableArray array];
            [folderNamesAndFiles setObject:otherFiles forKey:displayPath];
        }
        
        //NSLog(@"adding file \"%@\" to folder \"%@\"", file, displayPath);
        
        [otherFiles addObject:file];
    }
    
    return folderNamesAndFiles;
}

- (NSString *)_commonAncestorForFilePaths:(NSArray *)filePaths
{
    if ([filePaths count] == 0)
        return nil;
    
    // start with the first path -- any path contains the common ancestor component, so this is OK
    NSString *firstFileRoot = [filePaths objectAtIndex:0];
    BOOL isDir;
    [[NSFileManager defaultManager] fileExistsAtPath:firstFileRoot isDirectory:&isDir];
    if (!isDir)
        firstFileRoot = [firstFileRoot stringByDeletingLastPathComponent];
        
    NSString *commonPath = firstFileRoot;
    
    BOOL isCommonToAllFiles = NO;
    int nIterations = 0;    // just for safety
    
    while (nIterations < 1000) {
        
        isCommonToAllFiles = YES;
        nIterations++;
        
        for (NSString *leafPath in filePaths)
            if ([leafPath rangeOfString:commonPath options:NSAnchoredSearch].location == NSNotFound)
                isCommonToAllFiles = NO;

        if (isCommonToAllFiles)
            break;
        
        commonPath = [commonPath stringByDeletingLastPathComponent];
    }
    
    // all files now have "commonPath" in their path.
    // in other words, the path is the container of the shortest file (highest up in the file system).
    // the root for determining relative paths must be one level higher, so that the top level folder is included in the relative names.
    commonPath = [commonPath stringByDeletingLastPathComponent];
    
    return commonPath;
}


// Used when the file list needs to be flattened; i.e. we only keep the leaf files

- (NSArray *)_extractImagePathsFromFileList:(NSArray *)filesAndDirectories
{
	// Get all filenames in the given array, traversing the entire subtree if the file is a directory
	
	NSArray *validImageFileTypes = [MGImageFileImporter supportedPathExtensions];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSMutableArray *imageFilesToImport = [NSMutableArray array];
	
	for (NSString *file in filesAndDirectories) {

		BOOL isDir;
		
		// Directory
		if ([manager fileExistsAtPath:file isDirectory:&isDir] && isDir) {
			// If it's a directory, add all children to the array
			NSArray *subPaths = [manager subpathsAtPath:file];
			
			for (id subPath in subPaths)
				if ([validImageFileTypes containsObject:[[subPath pathExtension] lowercaseString]])
					[imageFilesToImport addObject:[[file stringByAppendingPathComponent:subPath] stringByExpandingTildeInPath]];
				
		}
		
		// File
		else {
			if ([validImageFileTypes containsObject:[[file pathExtension] lowercaseString]])
				[imageFilesToImport addObject:[file stringByExpandingTildeInPath]];
		}
	}

	return imageFilesToImport;
}


- (NSArray *)_sizesOfFilesAtPaths:(NSArray *)paths totalSize:(unsigned long long *)total;
{
	NSMutableArray *sizes = [NSMutableArray array];
	
	unsigned long long totalSize = 0;
	
	for (NSString *path in paths) {
		
		NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
		
		NSNumber *fileSize = [attrs objectForKey:NSFileSize];
		
		if (!fileSize)
			fileSize = [NSNumber numberWithUnsignedLongLong:1000000];
		
		[sizes addObject:fileSize];
		
		totalSize += [fileSize unsignedLongLongValue];
		
	}
	
	if (total)
		*total = totalSize;
	
	return sizes;
}

- (NSArray *)_removeDuplicateFiles:(NSArray *)original
{
	// in case of ICS/IDS, remove the ICS one (which is the smaller index file) to avoid importing them twice
	NSMutableArray *remainingPaths = [NSMutableArray arrayWithArray:original];
	
	for (NSString *file in original)
		if ([[file pathExtension] isEqualTo:@"ids"])
			[remainingPaths removeObject:[[file stringByDeletingPathExtension] stringByAppendingPathExtension:@"ics"]];
	
	return remainingPaths;
	
}


# pragma mark -
# pragma mark Copying image files


// This method creates an import folder inside the library image data folder and returns its name
// This folder has the following naming scheme: dd-mm-yyyy
- (NSString *)_importFolderName;
{
	NSFileManager *fm = [NSFileManager defaultManager];
	NSString *currentDateString = [[NSCalendarDate calendarDate] descriptionWithCalendarFormat:@"%d-%m-%Y"];
    
    NSString *basePath = [[MGLibraryControllerInstance library] imageDataPath];
    
    
	NSString *destinationFolder = [basePath stringByAppendingPathComponent:currentDateString];
	
	// If the folder for the current date does not already exist, let's create it
	if (![fm fileExistsAtPath:destinationFolder]) {
		[fm createDirectoryAtPath:destinationFolder withIntermediateDirectories:YES attributes:nil error:nil];
	}
	return destinationFolder;
}


// Copy the image file to the Filament Library image data folder
// This method returns the complete path to the image in the Filament Library folder structure
- (NSString *)_copyFileFromImporter:(MGImageFileImporter *)importer toFilamentLibraryInFolder:(NSString *)folderName;
{	
	NSString *originalFilePath = [importer originalFilePath];
	
	NSFileManager *fm = [NSFileManager defaultManager];
	NSString *dPath = [folderName stringByAppendingPathComponent:[originalFilePath lastPathComponent]];
	
	// Get a unique path for this image
	NSString *fullImagePath = [dPath uniquePath];
	
	// Copy the imported image to this new path, copy its attributes and return it
	if ([fm copyItemAtPath:originalFilePath toPath:fullImagePath error:nil])
		[fm setAttributes:[fm attributesOfItemAtPath:originalFilePath error:nil] ofItemAtPath:fullImagePath error:nil];

	return fullImagePath;
}



- (void)_writeImageRep:(NSBitmapImageRep *)rep toFile:(NSString *)filePath compressed:(BOOL)compressed;
{	
	NSSize pixelSize = NSMakeSize([rep pixelsWide], [rep pixelsHigh]);
	[rep setSize:pixelSize];
	
	NSString *ext = [filePath pathExtension];
	
	
	if (compressed) {
	
		if ([ext isEqual:@"jpeg"] || [ext isEqual:@"jpg"])
			[[rep representationUsingType:NSJPEGFileType properties:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:0.65] forKey:NSImageCompressionFactor]] writeToFile:filePath atomically:YES];
		
		else if ([ext isEqual:@"png"])
			[[rep representationUsingType:NSPNGFileType properties:nil] writeToFile:filePath atomically:YES];
		
		else
			[[rep TIFFRepresentation] writeToFile:filePath atomically:YES];

	}
	
	
	else
		[[rep TIFFRepresentation] writeToFile:filePath atomically:YES];
	
	
	// since this write occurs with the same file system attributes as it would in the cache, we need not set those (when using a cache, the cache file attrs are copied)
	
}



# pragma mark -
# pragma mark Utilities


- (Image *)_createEmptyImage;
{
	return [NSEntityDescription insertNewObjectForEntityForName:@"Image" inManagedObjectContext:[self _managedObjectContext]];
}
						
- (void)_setProgressForCurrentImage:(float)progress
{
	// take file size into account
	float offset = [[cumulativeImageFileSizes_ objectAtIndex:currentImageFileIndex_] floatValue] / (float)totalImageFileSize_;
	float scale = [[imageFileSizes_ objectAtIndex:currentImageFileIndex_] floatValue] / (float)totalImageFileSize_;
	
	//float offset = (float)currentImageFileIndex_/(float)numberOfImageFilesToImport_;
	//float scale = 1./(float)numberOfImageFilesToImport_;
	
	[self _setOverallProgress:offset + progress * scale];
}


- (void)_setOverallProgress:(float)progress
{
	if (self.showsProgressSheet)
		[MGLibraryControllerInstance setProgress:progress * 100.00];
}

@end
