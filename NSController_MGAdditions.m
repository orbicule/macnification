//
//  NSController_MGAdditions.m
//  Filament
//
//  Created by Dennis Lorson on 01/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "NSController_MGAdditions.h"


@implementation NSController (MGAdditions)




- (void)batchFaultObjects:(NSArray *)objects ofEntity:(NSString *)entityName
{
	if (!objects || [objects count] ==0) return;

	if (![objects isKindOfClass:[NSArray class]]) {
		
		NSLog(@"batchFaultObjects: requires an NSArray argument");
		return;
	}
	
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	
	NSMutableArray *faultObjects = [NSMutableArray array];
	for (id obj in objects) {
		if ([obj isFault])
			[faultObjects addObject:obj];
		
	}
	
	NSEntityDescription *entity = entityName ? [NSEntityDescription entityForName:entityName inManagedObjectContext:[(NSArrayController *)self managedObjectContext]] : [[objects objectAtIndex:0] entity];
	
	[req setEntity:entity];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@", faultObjects];
	[req setPredicate:predicate];
	[req setReturnsObjectsAsFaults:NO];
	//NSArray *fetched = [[(NSArrayController *)self managedObjectContext] executeFetchRequest:req error:nil];
	//NSLog(@"%@ batch faulted %i objects of entity %@", [self description], [fetched count], [entity name]);
}


@end
