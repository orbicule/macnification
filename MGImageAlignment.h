//
//  ImageAlignment.h
//  ImageAlignment
//
//  Created by Dennis Lorson on 18/10/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>
#import <QuartzCore/QuartzCore.h>
#import <vImage/vImage.h>
#import "MGImageAlignmentTypes.h"


CGFloat IAParametersNorm(IAParameters params);
IAParameters IAParametersScale(IAParameters one, CGFloat factor);
IAParameters IAParametersAdd(IAParameters one, IAParameters two);
CGAffineTransform IACGTransformFromNSTransform(NSAffineTransformStruct tr);
IAParameters IAParametersMake(CGFloat tx, CGFloat ty, CGFloat sxy);


@interface MGImageAlignment : NSObject {
	
	CIContext *ciCtx;
	
	
	// cached filters and CIImages
	CIFilter *distanceFilter;
	CIFilter *averageFilter;
}

- (void)setup;


// ALIGNMENT
- (NSAffineTransform *)alignImage:(CIImage *)image toImage:(CIImage *)reference;
- (IAParameters)alignImage:(CIImage *)first withImage:(CIImage *)second centerParameters:(IAParameters)centerParams;
- (CGFloat)distanceBetweenImage:(CIImage *)first andImage:(CIImage *)second withTrafo:(NSAffineTransform *)trafo andScale:(CGFloat)scale display:(BOOL)display;


// UTILITY
- (NSAffineTransform *)trafoWithParameters:(IAParameters)params forImage:(CIImage *)image;

@end
