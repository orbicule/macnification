//
//  PSFullScreenImageBrowserView.h
//  Filament
//
//  Created by Peter Schols on 19/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>

@class PSImageView;

@interface PSFullScreenImageBrowserView : IKImageBrowserView {

	IBOutlet PSImageView *imageView;
	
}

@end
