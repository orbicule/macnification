//
//  LineROI.m
//  Macnification
//
//  Created by Peter Schols on 04/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "LineROI.h"
#import "Image.h"


@implementation LineROI


+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	NSMutableSet *kps = [[[super keyPathsForValuesAffectingValueForKey:key] mutableCopy] autorelease];
	
	
	if ([key isEqualToString:@"summary"]) {
		[kps addObject:@"length"];
		[kps addObject:@"pixelLength"];
	}
	
	return kps;
}



+ (void)initialize 
{
	// Any change in length should trigger a change in summary
	//[self setKeys:[NSArray arrayWithObjects:@"length", @"pixelLength", nil] triggerChangeNotificationsForDependentKey:@"summary"];
}



- (void)setStartPoint:(NSPoint)startPoint endPoint:(NSPoint)endPoint;
{
	[self setAllPoints:[NSArray arrayWithObjects:[NSValue valueWithPoint:startPoint], [NSValue valueWithPoint:endPoint], nil]];
	// Update our measurements
	[self updateMeasurements];
}



// We resize the LineROI by moving one of the handles
- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
{
	[self setStartPoint:point endPoint:oppositePoint];
}



- (void)moveWithDelta:(NSPoint)deltaPoint;
{
	NSPoint newStartPoint = NSMakePoint([self startPoint].x + deltaPoint.x, [self startPoint].y + deltaPoint.y);
	NSPoint newEndPoint = NSMakePoint([self endPoint].x + deltaPoint.x, [self endPoint].y + deltaPoint.y);
	[self setStartPoint:newStartPoint endPoint:newEndPoint];
}



- (NSPoint)oppositePointForHandle:(int)handle;
{
	if (handle == 1) {
		return [self endPoint];	
	}
	else if (handle == 2) {
		return [self startPoint];	
	}
	else {
		return NSZeroPoint;	
	}
}


- (void)updateMeasurements;
{
	// Calculate our length attribute and set it
	[self calculateLength];
}


// We calculate the length once, so we don't have to repeat this calculation every time
- (void)calculateLength;
{
	// Calculate the length in a real world unit
	float xSize = [self startPoint].x - [self endPoint].x;
	float ySize = [self startPoint].y - [self endPoint].y;
	float pixelLength = sqrt(pow(xSize, 2) + pow(ySize, 2));
	float realLength = [[self valueForKey:@"image"] realLengthForPixelLength:pixelLength];
	
	// Set the length attributes
	[self setValue:[NSNumber numberWithFloat:roundf(pixelLength)] forKey:@"pixelLength"];
	[self setValue:[NSNumber numberWithFloat:realLength] forKey:@"length"];
}



- (NSPoint)startPoint;
{
	return [[[self allPoints]objectAtIndex:0]pointValue];
}


- (NSPoint)endPoint;
{
	return [[[self allPoints]objectAtIndex:1]pointValue];
}



- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
	NSPoint scaledStartPoint = NSMakePoint([self startPoint].x * scale, [self startPoint].y * scale);
	NSPoint scaledEndPoint = NSMakePoint([self endPoint].x * scale, [self endPoint].y * scale);
	NSBezierPath *path = [self pathWithScale:scale];
	// Draw the line
	[path setLineWidth:stroke*scale];
	[path stroke];
	
	if (isSelected) {
		// Draw the handles if selected
		[self drawHandleAtPoint:scaledStartPoint withScale:stroke transform:trafo view:view];
		[self drawHandleAtPoint:scaledEndPoint withScale:stroke transform:trafo view:view];
	}
	
	// Let our ROI superclass do the drawing of the note icon
	[super drawNoteIconWithScale:stroke transform:trafo view:view];
}



- (NSBezierPath *)pathWithScale:(float)scale;
{
	NSPoint scaledStartPoint = NSMakePoint([self startPoint].x * scale, [self startPoint].y * scale);
	NSPoint scaledEndPoint = NSMakePoint([self endPoint].x * scale, [self endPoint].y * scale);
	
	// workaround: lineROIs aren't drawn when start.x == end.x or start.y == end.y
	if (scaledStartPoint.x == scaledEndPoint.y)
		scaledStartPoint.x += 0.0001;
	if (scaledStartPoint.y == scaledEndPoint.y)
		scaledStartPoint.y += 0.0001;
	
	NSBezierPath *path = [[NSBezierPath alloc] init];
	[path moveToPoint:scaledStartPoint];
	[path lineToPoint:scaledEndPoint];
	
	
	return [path autorelease];
}



- (NSRect)noteIconRect;
{
	return NSMakeRect((([self startPoint].x + [self endPoint].x) / 2) + 5, ([self startPoint].y + [self endPoint].y) / 2, 16, 16);
}


- (int)handleUnderPoint:(NSPoint)point;
{
	NSRect rectForStartHandle = [self hitRectForHandleAtPoint:[self startPoint] withScale:1.0];
	NSRect rectForEndHandle = [self hitRectForHandleAtPoint:[self endPoint] withScale:1.0];
	if (NSPointInRect(point, rectForStartHandle)) {
		return 1;
	}
	else if (NSPointInRect(point, rectForEndHandle)) {
		return 2;
	}
	return 0;
}



- (BOOL)containsPoint:(NSPoint)point;
{
	BOOL isHit;
	NSPoint point1, point2, point3, point4;
	
	CGFloat margin = 15;
	
	// Check the inclination of the line
	float xDistance = fabs([self startPoint].x - [self endPoint].x);
	float yDistance = fabs([self startPoint].y - [self endPoint].y);
	
	if (yDistance > xDistance) {
		// The line is (more) vertical 	
		point1 = NSMakePoint([self startPoint].x - margin, [self startPoint].y);
		point2 = NSMakePoint([self startPoint].x + margin, [self startPoint].y);
		point3 = NSMakePoint([self endPoint].x + margin, [self endPoint].y);
		point4 = NSMakePoint([self endPoint].x - margin, [self endPoint].y);
	} else {
		// The line is (more) horizontal
		point1 = NSMakePoint([self startPoint].x, [self startPoint].y - margin);
		point2 = NSMakePoint([self startPoint].x, [self startPoint].y + margin);
		point3 = NSMakePoint([self endPoint].x, [self endPoint].y + margin);
		point4 = NSMakePoint([self endPoint].x, [self endPoint].y - margin);
	}
	
	NSBezierPath *path = [[NSBezierPath alloc] init];
	[path moveToPoint:point1];
	[path lineToPoint:point2];
	[path lineToPoint:point3];
	[path lineToPoint:point4];
	[path closePath];
	
	[path setLineWidth:2];
	if ([path containsPoint:point]) {
		isHit = YES;
	} else {
		isHit = NO;
	}
	
	// Check if the use has clicked the noteicon
	if (NSPointInRect(point, [self noteIconRect])) {
		isHit = YES;	
	}
	
	[path release];
	path = nil;	
	return isHit;
}




- (NSAttributedString *)pixelSummary;
{
	NSString *theString = [NSString stringWithFormat:@"%C %d px", (unsigned short)0x2571, [[self valueForKey:@"pixelLength"]intValue]];
	NSMutableAttributedString *aString = [[[NSMutableAttributedString alloc] initWithString:theString] autorelease];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:12], NSFontAttributeName, [self summaryColor], NSForegroundColorAttributeName, nil];
	// Add the attributes to the first character and to the first character after the line break
	[aString addAttributes:dict range:NSMakeRange(0, 1)];
	return aString;
}



- (NSString *)summary;
{
	NSString *theString;
	float length = [[self valueForKey:@"length"]floatValue];
	if (length < 100000000000000.0 && length > 0.000000001) {
		theString = [NSString stringWithFormat:@"%2.2f %@", length, [self valueForKeyPath:@"image.calibration.unit"]];
	}
	else {
		theString = @"Not calibrated";
	}
	return theString;
}



- (NSString *)colorContentsSummary;
{
	return @"N/A";
}




# pragma mark -
# pragma mark Color contents

- (void)calculateColorContents;
{
	// Do nothing here, so we don't invoke super
}


@end
