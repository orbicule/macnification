//
//  BrowserEmbeddingView.m
//  Filament
//
//  Created by Dennis Lorson on 07/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "BrowserEmbeddingView.h"


@implementation BrowserEmbeddingView



- (void)drawRect:(NSRect)rect
{
	NSRect bounds = [self bounds];
	
	// HEADER (H 8px)
	
	NSImage *headerLeft = [NSImage imageNamed:@"browser_header_left"];
	NSImage *headerMid = [NSImage imageNamed:@"browser_header_middle"];
	NSImage *headerRight = [NSImage imageNamed:@"browser_header_right"];
	
	
	NSImage *footerLeft = [NSImage imageNamed:@"source_footer_left"];
	NSImage *footerMid = [NSImage imageNamed:@"source_footer_middle"];
	NSImage *footerRight = [NSImage imageNamed:@"source_footer_right"];
	
	
	CGFloat headerHeight = [headerLeft size].height;
	CGFloat headerCornerWidth = [headerLeft size].width;
	
	NSRect headerLeftRect = NSMakeRect(NSMinX(bounds), NSMaxY(bounds) - headerHeight, headerCornerWidth, headerHeight);
	NSRect headerMidRect = NSMakeRect(NSMinX(bounds) + headerCornerWidth, NSMaxY(bounds) - headerHeight, NSWidth(bounds) - 2 * headerCornerWidth, headerHeight);
	NSRect headerRightRect = NSMakeRect(NSMaxX(bounds) - headerCornerWidth, NSMaxY(bounds) - headerHeight, headerCornerWidth, headerHeight);
	
	[headerLeft drawInRect:headerLeftRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[headerMid drawInRect:headerMidRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[headerRight drawInRect:headerRightRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	
	// FOOTER (H 22px)
	
	CGFloat footerHeight = [footerLeft size].height;
	CGFloat footerCornerWidth = [footerLeft size].width;
	
	NSRect footerLeftRect = NSMakeRect(NSMinX(bounds), NSMinY(bounds), footerCornerWidth, footerHeight);
	NSRect footerMidRect = NSMakeRect(NSMinX(bounds) + footerCornerWidth, NSMinY(bounds), NSWidth(bounds) - 2 * footerCornerWidth, footerHeight);
	NSRect footerRightRect = NSMakeRect(NSMaxX(bounds) - footerCornerWidth, NSMinY(bounds), footerCornerWidth, footerHeight);
	
	[footerLeft drawInRect:footerLeftRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[footerMid drawInRect:footerMidRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	[footerRight drawInRect:footerRightRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	
	
	// SIDES
	
	NSBezierPath *sidesDark = [NSBezierPath bezierPath];
	
	// dark left
	[sidesDark moveToPoint:NSMakePoint(NSMinX([self bounds]), NSMinY([self bounds]) + footerHeight)];
	[sidesDark lineToPoint:NSMakePoint(NSMinX([self bounds]), NSMaxY([self bounds]) - headerHeight - 1)];
	
	// dark right
	[sidesDark moveToPoint:NSMakePoint(NSMaxX([self bounds]) - 1, NSMinY([self bounds]) + footerHeight)];
	[sidesDark lineToPoint:NSMakePoint(NSMaxX([self bounds]) - 1, NSMaxY([self bounds]) - headerHeight - 1)];
	
	NSBezierPath *sidesLight = [NSBezierPath bezierPath];
	
	// light left
	[sidesLight moveToPoint:NSMakePoint(NSMinX([self bounds]) + 1, NSMinY([self bounds]) + footerHeight)];
	[sidesLight lineToPoint:NSMakePoint(NSMinX([self bounds]) + 1, NSMaxY([self bounds]) - headerHeight)];
	
	// light right
	[sidesLight moveToPoint:NSMakePoint(NSMaxX([self bounds]) - 2, NSMinY([self bounds]) + footerHeight)];
	[sidesLight lineToPoint:NSMakePoint(NSMaxX([self bounds]) - 2, NSMaxY([self bounds]) - headerHeight)];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	[[NSColor colorWithDeviceWhite:1.0 alpha:45.0/255.0] set];
	[sidesDark stroke];
	[[NSColor colorWithDeviceWhite:102.0/255.0 alpha:1.0] set];
	[sidesLight stroke];
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
	
}


@end
