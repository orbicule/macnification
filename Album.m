//
//  Album.m
//  Macnification
//
//  Created by Peter Schols on 22/11/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import "Album.h"
#import "MGLibrary.h"


@implementation Album



- (id)stackImages { return nil;	}
- (id)lightTableImages { return nil; }
- (void)prepareForDeletion {}


- (void)addImages:(NSArray *)images;
{
	[[self mutableSetValueForKey:@"images"] addObjectsFromArray:images];	
}

- (void)addKeywords:(NSArray *)keywords;
{
	for (id image in [self valueForKey:@"images"])
		[image addKeywords:keywords];
}

- (NSImage *)iconImage;
{
	NSImage *iconImage;
	
	int rank = [self valueForKey:@"rank"] ? [[self valueForKey:@"rank"] intValue] : -1;
	
	switch (rank) {
		case LIBRARY_COLLECTION_RANK:
			iconImage = [NSImage imageNamed:@"librarySmall"];
			break;
		case LAST_IMPORT_COLLECTION_RANK:
			iconImage = [NSImage imageNamed:@"importSmall"];
			break;
		default:
			iconImage = [NSImage imageNamed:@"folderSmall"];
			break;
	}
	
	[iconImage setSize:NSMakeSize(16, 16)];
	return iconImage;
}

- (NSImage *)largeIconImage;
{
	return [NSImage imageNamed:@"folderLarge"];
}

- (Album *)parentGroup;
{
	return [self valueForKey:@"parent"];
}

- (BOOL)canEdit;
{
	int rank = [self valueForKey:@"rank"] ? [[self valueForKey:@"rank"] intValue] : -1;

	if (rank == LIBRARY_COLLECTION_RANK || rank == LAST_IMPORT_COLLECTION_RANK || rank == PROJECTS_COLLECTION_RANK || rank == LAST_CAPTURE_COLLECTION_RANK)
		return NO;

	return YES;
}


- (NSData *)actionImageData { return nil; }
- (BOOL)needsZoomSlider { return YES; }

- (int)countOfImages
{
	return [[self valueForKey:@"images"] count];
}


# pragma mark -
# pragma mark Drag and drop


- (BOOL)acceptsDroppedImages;
{
	int rank = [self valueForKey:@"rank"] ? [[self valueForKey:@"rank"] intValue] : -1;

	if (rank == LIBRARY_COLLECTION_RANK || rank == LAST_IMPORT_COLLECTION_RANK || rank == LAST_CAPTURE_COLLECTION_RANK || rank == PROJECTS_COLLECTION_RANK)
		return NO;
	
	return YES;
}


- (BOOL)acceptsDroppedKeywords;
{
	int rank = [self valueForKey:@"rank"] ? [[self valueForKey:@"rank"] intValue] : -1;
	
	if (rank == LIBRARY_COLLECTION_RANK || rank == LAST_IMPORT_COLLECTION_RANK || rank == LAST_CAPTURE_COLLECTION_RANK || rank == PROJECTS_COLLECTION_RANK)
		return NO;
	
	return YES;
}


- (BOOL)acceptsDroppedImagesFromFinder;
{
	int rank = [self valueForKey:@"rank"] ? [[self valueForKey:@"rank"] intValue] : -1;
	
	if (rank == LAST_IMPORT_COLLECTION_RANK || rank == LAST_CAPTURE_COLLECTION_RANK /*|| rank == PROJECTS_COLLECTION_RANK*/)
		return NO;
	
	return YES;
}


- (BOOL)canBeDragged;
{
	int rank = [self valueForKey:@"rank"] ? [[self valueForKey:@"rank"] intValue] : -1;
	
	if (rank == LIBRARY_COLLECTION_RANK || rank == LAST_IMPORT_COLLECTION_RANK || rank == LAST_CAPTURE_COLLECTION_RANK || rank == PROJECTS_COLLECTION_RANK)
		return NO;
	
	return YES;
}

- (BOOL)canHaveAlbumAsChild:(Album *)proposedChild;
{
	return NO;
}

- (BOOL)canHaveAlbumsAsChildren:(NSArray *)proposedChildren;
{
	for (Album *album in proposedChildren)
		if (![self canHaveAlbumAsChild:album])
			return NO;
	
	return YES;
}


- (NSString *)description
{
	return [self valueForKey:@"name"];
}


#pragma mark -
#pragma mark Type info


- (BOOL)isLightTable;
{
	return [[[self entity] name] isEqualTo:@"LightTable"];

}

- (BOOL)isSmartAlbum;
{
	return [[[self entity] name] isEqualTo:@"SmartAlbum"];

}

- (BOOL)isFolder;
{
	return [[[self entity] name] isEqualTo:@"Album"];
}

- (BOOL)isGroup;
{
	return [[[self entity] name] isEqualTo:@"Group"];
}


#pragma mark -
#pragma mark Ranking

- (NSArray *)sortedChildren;
{
	NSArray *children = [[self valueForKey:@"children"] allObjects];
	
	NSArray *sorted = [children sortedArrayUsingDescriptors:[NSArray arrayWithObjects:
															 [NSSortDescriptor sortDescriptorWithKey:@"rank" ascending:YES],
															 [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],
															 nil]];
	
	return sorted;
}

- (Album *)childAtIndex:(int)index;
{
	NSArray *children = [self sortedChildren];
	if (index >= [children count])
		return nil;
	
	return [children objectAtIndex:index];
}

- (int)indexOfChild:(Album *)child;
{
	NSUInteger index = [[self sortedChildren] indexOfObject:child];
	
	if (index == NSNotFound)
		return -1;
	else
		return index;
	
}

- (void)_reassignRanks
{
	NSArray *sorted = [self sortedChildren];
	
	// leave 1 index open for new items
	int i = 1;
	for (Album *child in sorted)
		[child setValue:[NSNumber numberWithInt:(i++ * 2)] forKey:@"rank"];
}

- (void)addChildToEnd:(Album *)child;
{
	int maxRank = [[[[self sortedChildren] lastObject] valueForKey:@"rank"] intValue];
	
	[child setValue:[NSNumber numberWithInt:maxRank + 1] forKey:@"rank"];
	
	[[self mutableSetValueForKey:@"children"] addObject:child];
}

- (void)addChild:(Album *)child atIndex:(int)index;
{
	[self _reassignRanks];
	
	NSArray *sorted = [self sortedChildren];
	
	index = MAX(0, MIN([sorted count], index));
	
	int rank = -1;
	
	if (index == 0)
		rank = 0;
	else
		rank = [[[sorted objectAtIndex:index - 1] valueForKey:@"rank"] intValue] + 1;
	
	[child setValue:[NSNumber numberWithInt:rank] forKey:@"rank"];
	
	[[self mutableSetValueForKey:@"children"] addObject:child];
}


@end
