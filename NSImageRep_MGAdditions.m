//
//  NSImageRep_MGAdditions.m
//  LightTable
//
//  Created by Dennis Lorson on 29/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "NSImageRep_MGAdditions.h"


@implementation NSImageRep (MGAdditions)


- (CGFloat)dpiX  
{
	return 72.0*[self pixelsWide]/[self size].width; 
}

- (CGFloat)dpiY
{ 
	return 72.0*[self pixelsHigh]/[self size].height; 
}

- (NSSize)dpi
{
	return NSMakeSize([self dpiX], [self dpiY]);
}


- (void)setDpiX:(CGFloat)dpi
{
    NSSize size = [self size];
    size.width = 72.0*[self pixelsWide] / dpi;
    [self setSize:size];
}

- (void)setDpiY:(CGFloat)dpi
{
    NSSize size = [self size];
    size.height = 72.0*[self pixelsHigh] / dpi;
    [self setSize:size];
}


- (void)setDpi:(NSSize)dpi
{
	[self setDpiX:dpi.width];
	[self setDpiY:dpi.height];
}



@end
