//
//  CaptureDC1394Device.h
//  Filament
//
//  Created by Dennis Lorson on 18/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <dc1394/dc1394.h>

#import "CaptureDevice.h"

@class CaptureOpenGLPreview;


@interface CaptureDC1394Device : CaptureDevice 
{
	dc1394_t *controller_;
	
	NSString *name_;

	dc1394_device_id guid_;
	dc1394camera_t *camera_;
	
	BOOL isInLiveMode_;
	BOOL sessionIsStarted_;
	
	CaptureOpenGLPreview *preview_;
	
	CGFloat exposure_;
	
	BOOL isInFormat7Mode_;
}

@property (readwrite) dc1394_device_id guid;
@property (readwrite) dc1394_t *controller;

- (id)initWithDeviceID:(dc1394_device_id)guid controller:(dc1394_t *)ctl;


@end
