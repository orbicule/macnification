//
//  MGMDHeaderView.h
//  MetaDataView
//
//  Created by Dennis on 11/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGMDHeaderView : NSView {
	
	IBOutlet NSImageView *imageView;
	IBOutlet NSTextField *titleField;
	IBOutlet NSTextField *creationDateField, *importDateField, *modificationDateField;
	

}

@end
