//
//  WorkflowInfoWindow.m
//  LightTable
//
//  Created by Dennis Lorson on 20/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "WorkflowInfoPanel.h"
#import <QuartzCore/QuartzCore.h>
#import "WorkflowInfoView.h"


@implementation WorkflowInfoPanel

@synthesize hasCloseButton;

- (id)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)windowStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)deferCreation
{
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:bufferingType defer:deferCreation];
	return self;
}

- (void)awakeFromNib
{
	[self setOpaque:NO];
	[self setBackgroundColor:[NSColor clearColor]];
	[self setLevel:NSStatusWindowLevel];
	
}


- (void)orderFront:(id)sender
{
	for (CALayer *sublayer in [[infoView layer] sublayers]) {
		
		sublayer.opacity = 1.0;
	}
	
	[super orderFront:sender];

}

- (void)orderOut:(id)sender
{
	for (CALayer *sublayer in [[infoView layer] sublayers]) {
		
		sublayer.opacity = 0.0;
	}
	[super orderOut:sender];

}

- (void)setHasCloseButton:(BOOL)flag
{
	hasCloseButton = flag;
	
	[infoView relayout];
}

- (void)setMessage:(NSString *)string
{
	
	NSRect newContentsFrame = [infoView setMessage:string];
	NSPoint oldOrigin = [self frame].origin;
	NSRect newWindowFrame = [self frameRectForContentRect:newContentsFrame];
	newWindowFrame.origin = oldOrigin;
	[self setFrame:newWindowFrame display:YES];
	
	[infoView setFrameOrigin:NSMakePoint(0,0)];
}



@end
