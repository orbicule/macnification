//
//  MGFluoViewTIFFImporter.h
//  Filament
//
//  Created by Dennis Lorson on 18/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGOMECompatibleFileImporter.h"


@interface MGFluoViewTIFFFileImporter : MGOMECompatibleFileImporter 
{

}

@end
