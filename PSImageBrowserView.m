//
//  PSImageBrowserView.m
//  Filament
//
//  Created by Peter Schols on 07/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "PSImageBrowserView.h"
#import "PSGradientView.h"
#import "MGAppDelegate.h"
#import "Stack.h"
#import "MGStackImageBrowserCell.h"
#import "MGLibraryController.h"


@implementation PSImageBrowserView


- (void)awakeFromNib
{
	searchView.startColor = [NSColor colorWithCalibratedWhite:0.3 alpha:1.0];
	searchView.endColor = [NSColor colorWithCalibratedWhite:0.1 alpha:1.0];
}


- (void)showSearchBarWithMessage:(NSString *)message;
{

	[searchMessage setAlphaValue:0.0];
	[searchMessage setStringValue:[message stringByReplacingOccurrencesOfString:@"Search" withString:@"Searched"]];
	[doneButton setAlphaValue:0.0];
	[doneButton setHidden:NO];
	[[searchMessage animator] setAlphaValue:1.0];
	[[doneButton animator] setAlphaValue:1.0];
	[self reloadData];
	[[[self superview]superview] display];
	[self performSelector:@selector(display) withObject:nil afterDelay:0.20];
}



- (void)hideSearchBar;
{
	[[searchMessage animator] setAlphaValue:0.0];
	[[doneButton animator] setAlphaValue:0.0];
	[searchMessage setStringValue:@""];
	[doneButton setHidden:YES];
}



- (void)startAnimation:(NSString *)animation withSymbol:(NSString *)symbol forImagesAtIndexes:(NSIndexSet *)indices;
{
	NSMutableArray *rectsForSelectedImages = [NSMutableArray arrayWithCapacity:5];
	NSRect previousRect;
	NSUInteger currentIndex = [indices firstIndex];
    while (currentIndex != NSNotFound)
    {
		NSRect frameForSelectedImage = [self itemFrameAtIndex:currentIndex];
		NSRect scrollViewRect = [self convertRect:frameForSelectedImage toView:[[self superview]superview]];
		if (! NSEqualRects(scrollViewRect, previousRect)) {
			// Only add this rect if it's different from the previous one (in case of collapsed stacks, we only want to add one rect for the entire stack)
			[rectsForSelectedImages addObject:[NSValue valueWithRect:scrollViewRect]];   
		}
		previousRect = scrollViewRect;
		currentIndex = [indices indexGreaterThanIndex: currentIndex];
    }
	
	// Do not animate if we are animating more than 150 different images
	if ([rectsForSelectedImages count] < 150) {
		// Calculate the screen frame for the image browser, so we can exactly overlay our keywordAnimationWindow
		NSPoint imageBrowserOriginInWindow = [[[[self superview]superview]superview] convertPoint:[[[self superview]superview] frame].origin toView:nil];
		NSPoint imageBrowserOriginOnScreen = [[self window] convertBaseToScreen:imageBrowserOriginInWindow];
		NSRect screenRectForImageBrowser = NSMakeRect(imageBrowserOriginOnScreen.x, imageBrowserOriginOnScreen.y, 
													  [[self superview] frame].size.width, [[self superview]  frame].size.height);
		
		// Instantiate the keywordAnimationController and start the animation
		if (!keywordAnimationController) {
			keywordAnimationController = [[KeywordAnimationController alloc]init];
		}
		[keywordAnimationController showWindow:self];
		[keywordAnimationController setRectsArray:[[rectsForSelectedImages copy]autorelease] browserFrame:screenRectForImageBrowser];
		[keywordAnimationController startAnimation:animation withSymbol:symbol];
	}
}

- (void)magnifyWithEvent:(NSEvent *)theEvent;
{
	CGFloat magnification = (float)[theEvent magnification];
	CGFloat newVal = [self zoomValue] + magnification*1.;
	if (newVal > 0.99 || newVal < 0.01) return;
	
	[self setZoomValue:newVal];
	// update the array controller
	[[MGLibraryControllerInstance sourcelistTreeController] setValue:[NSNumber numberWithFloat:[self zoomValue]] forKeyPath:@"selection.browserZoom"];

}


#pragma mark -
#pragma mark Cell Appearance

- (IKImageBrowserCell *) newCellForRepresentedItem:(id)anItem;
{
	if ([anItem isKindOfClass:[Stack class]]) {
		// DO NOT release
		MGStackImageBrowserCell *cell = [[MGStackImageBrowserCell alloc] init];
		cell.numberOfImages = [[anItem valueForKey:@"stackImages"] count];
		cell.imageWidth = [(Stack *)anItem imageWidth];
		
		return cell;
	}
	
	return [super newCellForRepresentedItem:anItem];
}




@end
