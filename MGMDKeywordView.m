//
//  MGMDKeywordsView.m
//  Filament
//
//  Created by Dennis Lorson on 04/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDKeywordView.h"
#import "ImageArrayController.h"
#import "MGMDTokenField.h"
#import "MGMDConstants.h"
#import "MGMDFieldView.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"

@implementation MGMDKeywordView


- (BOOL)acceptsFirstResponder
{
	return NO;
}

- (BOOL)becomeFirstResponder
{
	return NO;
}


- (void) dealloc
{
	[draggedKeywords release];
	
	[super dealloc];
}


- (void)setEnabled:(BOOL)flag
{		
	[addKeywordsButton setEnabled:flag];
	
}

- (void)awakeFromNib
{
	draggedKeywords = nil;
	
	[[tokenField cell] setWraps:YES];		
	[tokenField setDelegate:self];
	
	[tokenField setFocusRingType:NSFocusRingTypeNone];
	[tokenField setAutoresizingMask:(NSViewWidthSizable | NSViewHeightSizable)];
	[tokenField setFont:[NSFont systemFontOfSize:11]];
	[tokenField setBordered:NO];
	[tokenField setBezeled:NO];
	
	//[tokenField setSelectable:NO];
	
	[[tokenField cell] setDrawsBackground:NO];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenFieldEditorDidEndEditing:) name:@"MGTokenFieldResignFirstResponderNotification" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenFieldEditorDidBeginEditing:) name:@"MGTokenFieldBecomeFirstResponderNotification" object:nil];
	
	[emptyView setFrame:[tokenField frame]];
	[emptyView setHidden:YES];
	[self addSubview:emptyView];
	
	[addKeywordsButton setTarget:MGLibraryControllerInstance];
	[addKeywordsButton setAction:@selector(toggleKeywordPanel:)];

	
}



- (void)bind:(NSString *)binding toObject:(id)ctl withKeyPath:(NSString *)aPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		observableController = ctl;
		observableKeyPath = [aPath retain];
		
		[observableController addObserver:self forKeyPath:observableKeyPath options:0 context:nil];
		[(ImageArrayController *)observableController addManualObserver:self forKeyPath:observableKeyPath];
		
		[(ImageArrayController *)observableController addObserver:self forKeyPath:@"selection" options:0 context:nil];
		
	} else {
		
		[super bind:binding toObject:observableController withKeyPath:observableKeyPath options:options];
		
	}
}

- (void)manualObserveValueForKeyPath:(NSString *)aPath ofObject:(id)object;
{
	// the experiments value of the selection has changed.
	// just forward this to our real observeValue method.
	
	[self observeValueForKeyPath:aPath ofObject:object change:0 context:nil];
	
	
}


- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		[observableController removeObserver:self forKeyPath:observableKeyPath];
		[observableKeyPath release];
		observableKeyPath = nil;
		
		[(ImageArrayController *)observableController removeManualObserver:self];
		
	} else {
		
		[super unbind:binding];
	}
}


- (void)observeValueForKeyPath:(NSString *)aPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	// this observe is triggered for both selection change and content change within the selection.
    if (object == observableController) {
		
		// "value" did change so set our object value manually and accordingly
		id value = [observableController valueForKeyPath:observableKeyPath];
		
		if (value == NSNoSelectionMarker || value == NSNotApplicableMarker) {
			
			value = nil;
			[[tokenField cell] setPlaceholderString:MGMDNoSelectionString];
			
		}
		
		else if (value == NSMultipleValuesMarker) {
			
			value = nil;
			[[tokenField cell] setPlaceholderString:MGMDMultipleValuesString];
			
		}
		
		else if (value && [value count] == 0) {
			
			[[tokenField cell] setPlaceholderString:@"None Specified"];
			
		}
		
		[tokenField setObjectValue:[value allObjects]];
		
		if (!value) {
			
			[emptyView setHidden:YES];
			[tokenField setEnabled:NO];
			[tokenField setSelectable:NO];
			
		} else if ([value count] == 0) {
			
			[emptyView setFrame:[tokenField frame]];
			[emptyView setHidden:NO];
			[tokenField setEnabled:NO];
			[tokenField setSelectable:NO];
			
		} else {
			
			[emptyView setHidden:YES];
			[tokenField setEnabled:YES];
			[tokenField setSelectable:YES];

		}
		
		[(MGMDFieldView *)[self superview] updateContentsSize];
		
		
	} else {
		[super observeValueForKeyPath:aPath ofObject:object change:change context:context];
	}
}



- (NSString *)contentDescription
{
	return [tokenField stringValue];
	
}


- (NSString *)contentValue
{
	return [self contentDescription];
	
	
}

- (CGFloat)contentsHeight
{	
	CGFloat editorHeight = [[(MGMDTokenField *)tokenField cell] cellSizeForBounds: NSMakeRect(0.0, 0.0, NSWidth([tokenField frame]), 100000.0)].height;
	
	return editorHeight + 3.0;
	
	
}



# pragma mark -
# pragma mark Token Field Delegate and bindings update
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Token Field Delegate and bindings update
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSString *)tokenField:(NSTokenField *)tokenField displayStringForRepresentedObject:(id)representedObject
{
	return [self stringForKeyword:representedObject];
}


- (NSString *)stringForKeyword:(NSManagedObject *)keyword
{
	NSString *divider = @" → ";
	
	NSManagedObject *currentAncestor = keyword;
	NSMutableString *string = [[[NSMutableString alloc] initWithString:[keyword valueForKey:@"name"]] autorelease];
	
	if (![currentAncestor valueForKey:@"parent"]) return string;
	
	while ((currentAncestor = [currentAncestor valueForKey:@"parent"])) {
		
		NSString *addition = [NSString stringWithFormat:@"%@%@", divider, [currentAncestor valueForKey:@"name"]];
		[string insertString:addition atIndex:[string length]];
		
	}
	
	return string;
}


- (BOOL)tokenField:(NSTokenField *)tokenField hasMenuForRepresentedObject:(id)representedObject
{
	return YES;
	
}

- (NSMenu *)tokenField:(NSTokenField *)tokenField menuForRepresentedObject:(id)representedObject
{
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11], NSFontAttributeName, nil];
	
	NSString *keywordName = [self tokenField:nil displayStringForRepresentedObject:representedObject];
	
	NSMenuItem *removeItem = [[[NSMenuItem alloc] init] autorelease];
	[removeItem setAttributedTitle:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Remove \"%@\"", keywordName] attributes:attrs] autorelease]];
	[removeItem setRepresentedObject:representedObject];
	[removeItem setTarget:self];
	[removeItem setAction:@selector(removeKeyword:)];
	
	[menu addItem:removeItem];
	
	return menu;
}

- (void)removeKeyword:(id)sender
{
	id keyword = [sender representedObject];
	NSMutableSet *selectionKeywords = [observableController mutableSetValueForKeyPath:@"selection.keywords"];
	
	[selectionKeywords removeObject:keyword];
	
	[observableController setValue:selectionKeywords forKeyPath:@"selection.keywords"];
	
}




- (BOOL)tokenField:(NSTokenField *)tokenField writeRepresentedObjects:(NSArray *)objects toPasteboard:(NSPasteboard *)pboard
{
	[pboard declareTypes:[NSArray arrayWithObject:@"keywordsPboardType"] owner:self];
	
	[draggedKeywords release];
	draggedKeywords = [objects copy];
	return YES;
	
}


- (NSSet *)draggedKeywordsForTokenField:(NSTokenField *)field
{
	return [NSSet setWithArray:draggedKeywords];
}







@end
