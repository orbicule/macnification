//
//  MGMainWindow.m
//  Filament
//
//  Created by Dennis Lorson on 08/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGMainWindow.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"
#import "SourceListTreeController.h"


@implementation MGMainWindow


- (void)sendEvent:(NSEvent *)theEvent
{
	NSWindow *predicateWindow = [[MGLibraryControllerInstance sourcelistTreeController] predicateWindow];
	
	
	if (([theEvent type] == NSLeftMouseDown || [theEvent type] == NSRightMouseDown) && [predicateWindow isVisible])
		[[MGLibraryControllerInstance sourcelistTreeController] closePredicateWindow:self];
	
	if ([theEvent type] == NSLeftMouseDown || [theEvent type] == NSRightMouseDown) {
		//[[[MGAppDelegateInstance sourcelistTreeController] captureController] closeAdjustmentsHUD];
	}
	

	if ([self isKeyWindow] && [theEvent type] == NSKeyDown && [theEvent keyCode] == 53) {
		//NSLog(@"ESC!");
		if ([[MGLibraryControllerInstance sourcelistTreeController] isShowingStack]) {
			[[MGLibraryControllerInstance sourcelistTreeController] hideStack:self];
			return;
		}

	}

	[super sendEvent:theEvent];
}

@end
