//
//  LightTable.m
//  LightTable
//
//  Created by Dennis Lorson on 13/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "Stack.h"

#import "Image.h"
#import "StackImage.h"
#import "MGAppDelegate.h"
#import "NSManagedObjectExtensions.h"
#import "SourceListTreeController.h"
#import "MGLibraryController.h"

@interface Stack ()

- (void)drawImage:(NSImage *)image withIndex:(NSInteger)index ofTotal:(NSInteger)nImages inRect:(NSRect)rect;

- (NSArray *)sortedImages;
- (void)invalidateSortedImageArray;

@end



@implementation Stack


- (void)awakeFromInsert
{
	[super awakeFromInsert];
	
	// to invalidate the fetched properties
	[self addObserver:self forKeyPath:@"stackImages" options:0 context:nil];
	
	
	//[self setValue:[NSNumber numberWithInt:30] forKey:@"rank"];
	
	NSString *defaultSortKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"defaultStackSortKey"];
	if ([defaultSortKey isEqualToString:@"Date"]) 
		[self setValue:@"CreationDate" forKey:@"sortKey"];
	else
		[self setValue:@"Name" forKey:@"sortKey"];
}


- (void) dealloc
{
	[self invalidateSortedImageArray];
	[super dealloc];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"stackImages"] && object == self)
		[self invalidateSortedImageArray];
	else
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	
}

- (void)addImages:(NSArray *)images;
{
	NSMutableSet *stackImages = [NSMutableSet set];
	
	for (Image *image in images) {
		
		if ([image isKindOfClass:[Stack class]])
			continue;
		
		[[image mutableSetValueForKey:@"albums"] removeAllObjects];
		
		// Create a new StackTableImage and set image as its image
		StackImage *newStackImage = [NSEntityDescription insertNewObjectForEntityForName:@"StackImage" inManagedObjectContext:[self managedObjectContext]];
		[newStackImage setValue:image forKey:@"image"];
		
		
		[stackImages addObject:newStackImage];

	}
	
	[[self mutableSetValueForKey:@"stackImages"] unionSet:stackImages];

}

- (BOOL)needsZoomSlider;
{
	return NO;	
}

- (void)removeImages
{
	NSSet *images = [self valueForKeyPath:@"stackImages.@distinctUnionOfObjects.image"];
	
	// batch fault all images
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@", images];
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	[req setEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:[self managedObjectContext]]];
	[req setPredicate:predicate];
	[req setRelationshipKeyPathsForPrefetching:[NSArray arrayWithObjects:@"lightTableImages", @"keywords", @"filters", @"experiments", @"instrument", @"rois", @"calibration", nil]];
	[req setReturnsObjectsAsFaults:NO];
	
	[[self managedObjectContext] executeFetchRequest:req error:nil];
	
	
	NSLog(@"removing %i stack images", (int)[images count]);
	
	for (id image in [[images copy] autorelease]) {
		[[self managedObjectContext] deleteObject:image];
		
	}
}

 
#pragma mark -
#pragma mark IKIB

- (NSString *)imageRepresentationType
{
	return IKImageBrowserNSImageRepresentationType;
}

- (id)imageRepresentation
{
	return [self image];
}

#pragma mark -
#pragma mark Custom image

- (NSArray *)ciFilters;
{
	return [NSArray array];
}

- (BOOL)hasFilters;
{
	return NO;
}

- (NSString *)activeImagePath
{
	return [[NSBundle mainBundle] pathForResource:@"AppIcon" ofType:@"icns"];
}


- (CGImageRef)imageRef
{
	NSImage *image = [self image];
	NSBitmapImageRep *rep = [[image representations] objectAtIndex:0];
	
	return [rep CGImage];
}


- (CIImage *)ciImage
{
	NSImage *img = [self image];
	NSBitmapImageRep *rep = [[img representations] objectAtIndex:0];
	
	return [[[CIImage alloc] initWithBitmapImageRep:rep] autorelease];
}

- (NSImage *)image
{
	NSArray *images = [self sortedImages];
	
	NSSize imageSize = NSMakeSize(400, 400);
	
		
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																	 pixelsWide:(int)imageSize.width
																	 pixelsHigh:(int)imageSize.height
																  bitsPerSample:8
																samplesPerPixel:4
																	   hasAlpha:YES
																	   isPlanar:NO
																 colorSpaceName:NSDeviceRGBColorSpace
																   bitmapFormat:0
																	bytesPerRow:4 * (int)imageSize.width
																   bitsPerPixel:4 * 8] autorelease];
	
	
	NSGraphicsContext *ctx = [NSGraphicsContext currentContext];
	
	[NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithBitmapImageRep:rep]];
	
	int maxNStackImages = 3;
	
	
	
	NSImage *singleImage = nil;
	
	if ([images count] > 0)
		singleImage = [[images objectAtIndex:0] valueForKeyPath:@"filteredImage"];

	
	for (NSInteger i = MIN(maxNStackImages, [images count]) - 1; i > -1; i--) {
		[self drawImage:singleImage withIndex:i ofTotal:MIN(maxNStackImages, [images count]) inRect:NSMakeRect(0, 0, imageSize.width, imageSize.height)];
	}
	
	[NSGraphicsContext setCurrentContext:ctx];
	
	NSImage *image = [[[NSImage alloc] initWithSize:imageSize] autorelease];
	[image addRepresentation:rep];

	return image;
}

- (void)drawImage:(NSImage *)img withIndex:(NSInteger)index ofTotal:(NSInteger)nImages inRect:(NSRect)rect
{
	//NSImage *img = [image valueForKeyPath:@"filteredImage"];
	
	NSSize imageSize = [img size];
	
	CGFloat ratio = imageSize.height/imageSize.width;
	
	CGFloat scaleFactor = 1.0 - index * 0.15;
	CGFloat baselineOffset = rect.size.height * 0.21 * pow(0.97, index) * index;
	
	NSRect destRect;

	destRect.size.height = rect.size.height * 0.8;
	destRect.size.width = MIN(rect.size.width, destRect.size.height / ratio);
	//destRect.size.height = destRect.size.width * ratio;

	destRect.size.width *= scaleFactor;
	destRect.size.height *= scaleFactor;
	
	
	destRect.origin.x = NSMidX(rect) - destRect.size.width * 0.5;
	destRect.origin.y = baselineOffset;
	
	if (index == 0) {
		[img drawInRect:destRect fromRect:NSMakeRect(0, 0, imageSize.width, imageSize.height) operation:NSCompositeCopy fraction:1.0];
	}
	
	else {
		
		NSRect destRectAdjusted = destRect;
		
		CGFloat fraction = 0.13;
		// draw only the top 20%
		destRectAdjusted.origin.y += (1.0 - fraction) * destRectAdjusted.size.height;
		destRectAdjusted.size.height *= fraction;
		
		NSRect srcRect = NSMakeRect(0, 0, imageSize.width, imageSize.height);
		srcRect.origin.y += (1.0 - fraction) * srcRect.size.height;
		srcRect.size.height *= fraction;
		
		[img drawInRect:destRectAdjusted fromRect:srcRect operation:NSCompositeCopy fraction:1.0];
		
	}
	
	// draw a border
	NSRect inset = NSInsetRect(destRect, rect.size.width * 0.01, rect.size.width * 0.01);
	
	[[NSGraphicsContext currentContext] saveGraphicsState];
	
	NSBezierPath *path = [NSBezierPath bezierPath];
	[path setWindingRule:NSEvenOddWindingRule];
	[path appendBezierPathWithRect:destRect];
	[path appendBezierPathWithRect:inset];
	[path addClip];
	
	[[NSColor colorWithCalibratedWhite:0.0 alpha:0.35] set];
	
	[[NSBezierPath bezierPathWithRect:destRect] fill];
	//NSRectFill(destRect);
	
	[[NSGraphicsContext currentContext] restoreGraphicsState];
	 
}

- (CGFloat)imageWidth
{
	if (imageWidth_ > 0)
		return imageWidth_;

	NSArray *images = [self sortedImages];
	
	
	if ([images count] == 0) 
		return 0;
	
	NSImage *singleImage = [[images objectAtIndex:0] valueForKeyPath:@"filteredImage"];
	
	// as a fraction of the total width of the stack preview image
	NSSize imageSize = [singleImage size];
	
	CGFloat ratio = imageSize.height/imageSize.width;
	// protect against NaN
    if (imageSize.width == 0 || ratio == 0)
        ratio = 1;
    
	NSRect rect = NSMakeRect(0, 0, 400, 400);
		
	NSRect destRect;
	
	destRect.size.height = rect.size.height * 0.8;
	destRect.size.width = MIN(rect.size.width, destRect.size.height / ratio);

	imageWidth_ = destRect.size.width/rect.size.width;
	
	return imageWidth_;
}


#pragma mark -
#pragma mark Derived properties/metadata

- (void)invalidateSortedImageArray;
{
	//NSLog(@"invalidating sorted stack images");
	[sortedImageArray_ release];
	sortedImageArray_ = nil;
}

- (NSArray *)images
{
	return [[self valueForKeyPath:@"stackImages.image"] allObjects];
}

- (NSArray *)sortedImages
{
	// cached image array, sorted by creation date
	if (sortedImageArray_)
		return sortedImageArray_;
	
	NSSet *images = [self valueForKeyPath:@"stackImages.@distinctUnionOfObjects.image"];
	NSSortDescriptor *descDate = [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES];
	NSSortDescriptor *descName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];

	sortedImageArray_ = [[images sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descDate, descName, nil]] copy];
	
	return sortedImageArray_;
}

- (NSArray *)keysDerivedFromFirstImage
{
	return [NSArray arrayWithObjects:
			@"creationDate",
			@"importDate",
			@"pixelSizeDescription",
			@"calibration",
			nil];
}


- (NSArray *)keysDerivedFromAllImages
{
	NSMutableArray *standardKeys = [NSMutableArray arrayWithObjects:
									@"roiCount",
									@"isCalibrated",
									@"hasFilters",
									@"hasAdjustments",
									@"instrument.instrumentName",
									@"instrument.numericAperture",
									@"instrument.objectiveName",
									@"instrument.spotSize",
									@"instrument.voltage",
									@"instrument.workingDistance",
									@"instrument.magnification",
									@"instrument.immersion",
									@"experiment",
									@"experimenter.name",
									@"channels",
									@"comment",
																		
									nil];
	
	NSArray *customKeys = [[MGCustomMetadataController sharedController] customAttributes];

	for (NSDictionary *key in customKeys)
		[standardKeys addObject:[key objectForKey:@"path"]];
	
	return standardKeys;
}

- (NSArray *)averageKeysDerivedFromAllImages
{
	return [NSArray arrayWithObjects:
			//@"rating",
			nil];
}




- (id)valueForKeyPath:(NSString *)keyPath
{
	if ([[self keysDerivedFromAllImages] containsObject:keyPath]) {
		
		NSSet *set = [self valueForKeyPath:[NSString stringWithFormat:@"stackImages.image.%@", keyPath]];
		
		// > 1 different values (excluding NULL)
		if ([set count] > 1)
			return NSMultipleValuesMarker;

		// no values, only NULL
		if ([set count] == 0)
			return nil;
		
		NSArray *nullObjects = [[[self valueForKeyPath:@"stackImages.image"] allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == nil", keyPath]];
		
		// one or more values, but also NULL
		if ([nullObjects count] > 0)
			return NSMultipleValuesMarker;
		
		// one value, no NULL
		return [set anyObject];
		
	}
	
	else if ([[self keysDerivedFromFirstImage] containsObject:keyPath]) {
		
		if ([[self sortedImages] count] == 0)
			return nil;
		
		return [[[self sortedImages] objectAtIndex:0] valueForKeyPath:keyPath];
	}
	
	
	else
		return [super valueForKeyPath:keyPath];
}


- (id)valueForKey:(NSString *)key
{
	if ([[self keysDerivedFromAllImages] containsObject:key]) {
		
		NSSet *set = [self valueForKeyPath:[NSString stringWithFormat:@"stackImages.image.%@", key]];
		
		// > 1 different values (excluding NULL)
		if ([set count] > 1)
			return NSMultipleValuesMarker;
		
		// no values, only NULL
		if ([set count] == 0)
			return nil;
		
		NSArray *nullObjects = [[[self valueForKeyPath:@"stackImages.image"] allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == nil", key]];
		
		// one or more values, but also NULL
		if ([nullObjects count] > 0)
			return NSMultipleValuesMarker;
		
		// one value, no NULL
		return [set anyObject];
		

	}
	
	else if ([[self keysDerivedFromFirstImage] containsObject:key]) {
		
		if ([[self sortedImages] count] == 0)
			return nil;
		
		return [[[self sortedImages] objectAtIndex:0] valueForKey:key];
	}
	
	else if ([[self averageKeysDerivedFromAllImages] containsObject:key]) {
		
		return [self valueForKeyPath:[NSString stringWithFormat:@"stackImages.@avg.image.%@", key]];

		
	}
	
	
	else
		return [super valueForKey:key];
}


- (void)setValue:(id)value forKey:(NSString *)key
{
	if ([[self keysDerivedFromAllImages] containsObject:key]) {
		[self setValue:value forKeyPath:[NSString stringWithFormat:@"stackImages.image.%@", key]];
	}
	
	else {
		[super setValue:value forKey:key];
	}
}

- (void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
	if ([[self keysDerivedFromAllImages] containsObject:keyPath]) {
		NSLog(@"Stack set value: %@ for keyPath: %@", [value description], keyPath);
		for (Image *image in [self valueForKeyPath:@"stackImages.image"])
			[image setValue:value forKeyPath:keyPath];
	}
	
	else {
		[super setValue:value forKeyPath:keyPath];
	}
}


#pragma mark -
#pragma mark Split/merge

- (NSArray *)splitChannels
{
	NSArray *images = [self sortedImages];
	
	NSMutableArray *r = [NSMutableArray array];
	NSMutableArray *g = [NSMutableArray array];
	NSMutableArray *b = [NSMutableArray array];
	
	for (Image *image in images) {
		
		NSArray *channelImages = [image splitChannels];
		
		if ([channelImages count] > 0)
			[r addObject:[channelImages objectAtIndex:0]];

		if ([channelImages count] > 1)
			[g addObject:[channelImages objectAtIndex:1]];
		
		if ([channelImages count] > 2)
			[b addObject:[channelImages objectAtIndex:2]];
		
	}
	
	
	id libraryCollection = [[MGLibraryControllerInstance library] libraryGroup];

    NSMutableArray *stacks = [NSMutableArray array];
	
	if ([r count]) {
		Stack *rStack = (Stack *)[self clone];
		[rStack setName:[[self name] stringByAppendingFormat:@" - ch0"]];
		[rStack addImages:r];
		
		[rStack setValue:[NSSet setWithObject:libraryCollection] forKey:@"albums"];
        
        [stacks addObject:rStack];
	}
	
	if ([g count]) {
		Stack *gStack = (Stack *)[self clone];
		[gStack setName:[[self name] stringByAppendingFormat:@" - ch1"]];
		[gStack addImages:g];
		
		[gStack setValue:[NSSet setWithObject:libraryCollection] forKey:@"albums"];

        [stacks addObject:gStack];

	}
	
	if ([b count]) {
		Stack *bStack = (Stack *)[self clone];
		[bStack setName:[[self name] stringByAppendingFormat:@" - ch2"]];
		[bStack addImages:b];
		
		[bStack setValue:[NSSet setWithObject:libraryCollection] forKey:@"albums"];

        [stacks addObject:bStack];
	}
	
	return stacks;
}



#pragma mark -
#pragma mark Stack/Image related

- (int)numberOfContainedImages;
{
	return [[self valueForKey:@"stackImages"] count];
}


- (Image *)displayImage;
{
	if ([[self sortedImages] count])
		return [[self sortedImages] objectAtIndex:0];
	
	return nil;
}





#pragma mark -
#pragma mark Calibration/scalebar

- (BOOL)isCalibrated
{
	for (Image *image in [self images])
		if (![image isCalibrated])
			return NO;
	return YES;
}

- (void)addScaleBarWithProperties:(NSDictionary *)properties;
{
	for (Image *image in [self images])
		[image addScaleBarWithProperties:properties];
}


- (void)removeScaleBar;
{
	for (Image *image in [self images])
		[image removeScaleBar];
}

- (void)copyCalibrationFromImage:(Image *)otherImage;
{
	for (Image *image in [self images])
		[image copyCalibrationFromImage:otherImage];
}

- (void)removeCalibration;
{
	for (Image *image in [self images])
		[image removeCalibration];
	
}

#pragma mark -
#pragma mark Keywords

- (void)copyKeywordsFromImage:(Image *)otherImage;
{
	for (Image *image in [self images])
		[image copyKeywordsFromImage:otherImage];
}

- (void)removeAllKeywords;
{
	for (Image *image in [self images])
		[image removeAllKeywords];
}

#pragma mark -
#pragma mark Filters

- (void)copyFiltersFromImage:(Image *)otherImage;
{
	for (Image *image in [self images])
		[image copyFiltersFromImage:otherImage];
}

- (void)removeAllFilters;
{
	for (Image *image in [self images])
		[image removeAllFilters];
}

#pragma mark -
#pragma mark ROI

- (void)copyROIsFromImage:(Image *)existingImage removeExisting:(BOOL)remove;
{
	for (Image *image in [self images])
		[image copyROIsFromImage:existingImage removeExisting:remove];
}

@end
