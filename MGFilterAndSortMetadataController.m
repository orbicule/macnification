//
//  MGFilterAndSortMetadataController.m
//  Filament
//
//  Created by Dennis Lorson on 24/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGFilterAndSortMetadataController.h"

#import "MGCustomMetadataController.h"

@interface MGFilterAndSortMetadataController ()

- (BOOL)_isCustomAttributeKeyPath:(NSString *)keyPath;
- (BOOL)_isCurrentlyAvailableCustomAttributeKeyPath:(NSString *)keyPath;


- (NSArray *)_appendCustomKeyPathsToArray:(NSArray *)array;

@end



@implementation MGFilterAndSortMetadataController

+ (MGFilterAndSortMetadataController *)sharedController;
{
	static MGFilterAndSortMetadataController *sharedController_ = nil;
	
	if (!sharedController_) 
		sharedController_ = [[MGFilterAndSortMetadataController alloc] init];
	
	return sharedController_;
}


- (NSArray *)searchAttributeKeyPaths;
{
	NSArray *defaultKeypaths = [NSArray arrayWithObjects:
								@"name",
								@"comment",
								@"rating.description",
								[NSNull null],
								@"instrument.instrumentName",
								@"instrument.objectiveName",
								@"instrument.spotSize",
								@"instrument.voltage",
								@"instrument.workingDistance",
								@"instrument.magnification",
								@"instrument.immersion",
								@"instrument.numericAperture",
								@"channelMetadata",
								[NSNull null],
								@"allKeywords",
								@"listOfAlbums",
								@"allROIComments",
								@"roiCount",
								[NSNull null],
								@"experimenter.name",
								@"experiment.experimentName",
								@"experiment.comment",
								@"experiment.type",
								@"experiment.id",
								nil];
	
	return [self _appendCustomKeyPathsToArray:defaultKeypaths];
	
}


- (NSArray *)_appendCustomKeyPathsToArray:(NSArray *)array
{
	NSArray *customAttrs = [[MGCustomMetadataController sharedController] customAttributes];
	
	if ([customAttrs count] == 0)
		return array;
	
	NSMutableArray *customKeyPaths = [NSMutableArray array];
	[customKeyPaths addObject:[NSNull null]];
	
	for (NSDictionary *customAttr in customAttrs)
		[customKeyPaths addObject:[customAttr objectForKey:@"path"]];
	
	return [array arrayByAddingObjectsFromArray:customKeyPaths];
	
}

- (NSArray *)sortAttributeKeyPaths;
{
	NSArray *defaultKeypaths = [NSArray arrayWithObjects:
								@"name",
								@"comment",
								@"rating.description",
								[NSNull null],
								@"creationDate",
								@"importDate",
								@"modificationDate",
								[NSNull null],
								@"instrument.instrumentName",
								@"instrument.objectiveName",
								@"instrument.spotSize",
								@"instrument.voltage",
								@"instrument.workingDistance",
								@"instrument.magnification",
								@"instrument.immersion",
								@"instrument.numericAperture",
								[NSNull null],
								@"roiCount",
								[NSNull null],
								@"experimenter.name",
								@"experiment.experimentName",
								@"experiment.comment",
								@"experiment.type",
								@"experiment.id",
								nil];
	
	return [self _appendCustomKeyPathsToArray:defaultKeypaths];

}


- (NSArray *)exportAttributeKeyPaths;
{
	NSArray *defaultKeypaths = [NSArray arrayWithObjects:
								@"name",
								@"comment",
								@"rating.description",

								@"creationDate",
								@"importDate",
								@"modificationDate",

								@"instrument.instrumentName",
								@"instrument.objectiveName",
								@"instrument.spotSize",
								@"instrument.voltage",
								@"instrument.workingDistance",
								@"instrument.magnification",
								@"instrument.immersion",
								@"instrument.numericAperture",

								@"roiCount",

								@"experimenter.name",
								@"experiment.experimentName",
								@"experiment.comment",
								@"experiment.type",
								@"experiment.id",
								nil];
	
	NSMutableArray *keyPaths = [[[self _appendCustomKeyPathsToArray:defaultKeypaths] mutableCopy] autorelease];
	
	[keyPaths removeObject:[NSNull null]];
	
	return keyPaths;
}


- (NSString *)humanReadableNameForAttributeKeyPath:(NSString *)keyPath;
{
	if ([keyPath isEqualTo:@"name"])
		return @"Image Name";
	if ([keyPath isEqualTo:@"comment"])
		return @"Comments";
	if ([keyPath isEqualTo:@"instrument.instrumentName"])
		return @"Instrument Name";
	if ([keyPath isEqualTo:@"instrument.objectiveName"])
		return @"Objective Name";
	if ([keyPath isEqualTo:@"instrument.spotSize"])
		return @"Spot Size";
	if ([keyPath isEqualTo:@"instrument.voltage"])
		return @"Voltage";
	if ([keyPath isEqualTo:@"instrument.workingDistance"])
		return @"Working Distance";
	if ([keyPath isEqualTo:@"instrument.magnification"])
		return @"Magnification";
	if ([keyPath isEqualTo:@"experimenter.name"])
		return @"Experimenter";
	if ([keyPath isEqualTo:@"rating.description"])
		return @"Rating";
	if ([keyPath isEqualTo:@"allKeywords"])
		return @"Keywords";
	if ([keyPath isEqualTo:@"keywords"])
		return @"Keywords";
	if ([keyPath isEqualTo:@"listOfAlbums"])
		return @"Folder/Stack/Light Table Name";
	if ([keyPath isEqualTo:@"experiment.experimentName"])
		return @"Experiment Name";
	if ([keyPath isEqualTo:@"experiment.comment"])
		return @"Experiment Comments";
	if ([keyPath isEqualTo:@"experiment.type"])
		return @"Experiment Type";
	if ([keyPath isEqualTo:@"experiment.id"])
		return @"Experiment ID";
	if ([keyPath isEqualTo:@"allROIComments"])
		return @"ROI Comments";
	if ([keyPath isEqualTo:@"rois"])
		return @"ROI Comments";
	if ([keyPath isEqualTo:@"channelMetadata"])
		return @"Channel Information";
	
	if ([keyPath isEqualTo:@"creationDate"])
		return @"Creation Date";
	if ([keyPath isEqualTo:@"importDate"])
		return @"Import Date";
	if ([keyPath isEqualTo:@"modificationDate"])
		return @"Modification Date";
	
	
	if ([keyPath isEqualTo:@"roiCount"])
		return @"ROI Count";
	if ([keyPath isEqualTo:@"instrument.immersion"])
		return @"Immersion";
	if ([keyPath isEqualTo:@"instrument.numericAperture"])
		return @"Numeric Aperture";
	if ([keyPath isEqualTo:@"modificationDate"])
		return @"Modification Date";
	
	
	// is it a custom attribute?
	
	NSArray *customAttrs = [[MGCustomMetadataController sharedController] customAttributes];
	for (NSDictionary *customAttr in customAttrs)
		if ([[customAttr objectForKey:@"path"] isEqualTo:keyPath])
			return [customAttr objectForKey:@"name"];

	
	
	
	return @"UNKNOWN ATTRIBUTE";
}

- (NSPredicate *)searchPredicateForAttributeKeyPath:(NSString *)keyPath prefix:(NSString *)prefix searchString:(NSString *)searchString;
{
	NSString *prefixedKeyPath = [prefix length] ? [NSString stringWithFormat:@"%@.%@", prefix, keyPath] : keyPath;
	
	
	NSPredicate *containsPredicate = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", prefixedKeyPath, searchString];
	NSPredicate *isEqualToPredicate = [NSPredicate predicateWithFormat:@"%K == %@", prefixedKeyPath, searchString];
	
	
	if ([keyPath isEqualTo:@"name"] ||
		[keyPath isEqualTo:@"comment"] ||
		[keyPath isEqualTo:@"instrument.instrumentName"] ||
		[keyPath isEqualTo:@"instrument.objectiveName"] ||
		[keyPath isEqualTo:@"instrument.immersion"] ||
		[keyPath isEqualTo:@"experimenter.name"] ||
		[keyPath isEqualTo:@"rating.description"] ||
		[keyPath isEqualTo:@"allKeywords"] ||
		[keyPath isEqualTo:@"listOfAlbums"] ||
		[keyPath isEqualTo:@"experiment.experimentName"] ||
		[keyPath isEqualTo:@"experiment.comment"] ||
		[keyPath isEqualTo:@"experiment.type"] ||
		[keyPath isEqualTo:@"experiment.id"] ||
		[keyPath isEqualTo:@"allROIComments"] ||
		[keyPath isEqualTo:@"channelMetadata"])
		return containsPredicate;
	
	if ([keyPath isEqualTo:@"instrument.spotSize"] ||
		[keyPath isEqualTo:@"instrument.numericAperture"] ||
		[keyPath isEqualTo:@"instrument.voltage"] ||
		[keyPath isEqualTo:@"instrument.workingDistance"] ||
		[keyPath isEqualTo:@"roiCount"] ||
		[keyPath isEqualTo:@"instrument.magnification"])
		return isEqualToPredicate;
	
	
	if ([self _isCurrentlyAvailableCustomAttributeKeyPath:keyPath])
		return containsPredicate;
	
	
	return nil;
}



- (NSArray *)predicateEditorRowTemplatesForFilterAttributes;
{
	NSMutableArray *templates = [NSMutableArray array];
	
	NSMutableArray *leftExpressions;
	NSArray *operators;
	NSPredicateEditorRowTemplate *template;
	NSArray *keyPaths;
	

	
	
	// String (to-one)
	keyPaths = [NSArray arrayWithObjects:
				@"name",
				@"comment",
				@"keywords",
				nil];
	
	
	
	keyPaths = [[[self _appendCustomKeyPathsToArray:keyPaths] mutableCopy] autorelease];
	[(NSMutableArray *)keyPaths removeObject:[NSNull null]];
	
	
	
	leftExpressions = [NSMutableArray array];
	for (NSString *kp in keyPaths)
		[leftExpressions addObject:[NSExpression expressionForKeyPath:kp]];
	
	
	operators = [NSArray arrayWithObjects:
				 [NSNumber numberWithInt:NSContainsPredicateOperatorType],
				 [NSNumber numberWithInt:NSMatchesPredicateOperatorType],
				 [NSNumber numberWithInt:NSBeginsWithPredicateOperatorType],
				 [NSNumber numberWithInt:NSEndsWithPredicateOperatorType],
				 nil];
	
	template = [[[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:leftExpressions
												 rightExpressionAttributeType:NSStringAttributeType
																	 modifier:NSDirectPredicateModifier
																	operators:operators
																	  options:NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption] 
				autorelease];
	
	[templates addObject:template];
	
	
	
	
	
	// String (to-many)
	
	keyPaths = [NSArray arrayWithObjects:
				@"instrument.instrumentName",
				@"instrument.immersion",
				@"instrument.objectiveName",
				@"experimenter.name",
				@"experiment.experimentName",
				@"experiment.comment",
				@"experiment.type",
				@"experiment.id",
				@"rois",
				nil];
	
	
	leftExpressions = [NSMutableArray array];
	for (NSString *kp in keyPaths)
		[leftExpressions addObject:[NSExpression expressionForKeyPath:kp]];
	
	
	operators = [NSArray arrayWithObjects:
				 [NSNumber numberWithInt:NSContainsPredicateOperatorType],
				 [NSNumber numberWithInt:NSMatchesPredicateOperatorType],
				 [NSNumber numberWithInt:NSBeginsWithPredicateOperatorType],
				 [NSNumber numberWithInt:NSEndsWithPredicateOperatorType],
				 nil];
	
	template = [[[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:leftExpressions
												 rightExpressionAttributeType:NSStringAttributeType
																	 modifier:NSAnyPredicateModifier
																	operators:operators
																	  options:NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption] 
				autorelease];
	
	[templates addObject:template];
	
	
	// Numeric (float)
	
	keyPaths = [NSArray arrayWithObjects:
				@"instrument.spotSize",
				@"instrument.voltage",
				@"instrument.numericAperture",
				@"instrument.workingDistance",
				@"instrument.magnification",
				nil];
	
	
	
	
	
	
	leftExpressions = [NSMutableArray array];
	for (NSString *kp in keyPaths)
		[leftExpressions addObject:[NSExpression expressionForKeyPath:kp]];
	
	
	operators = [NSArray arrayWithObjects:
				 [NSNumber numberWithInt:NSLessThanPredicateOperatorType],
				 [NSNumber numberWithInt:NSLessThanOrEqualToPredicateOperatorType],
				 [NSNumber numberWithInt:NSGreaterThanPredicateOperatorType],
				 [NSNumber numberWithInt:NSGreaterThanOrEqualToPredicateOperatorType],
				 [NSNumber numberWithInt:NSEqualToPredicateOperatorType],
				 [NSNumber numberWithInt:NSNotEqualToPredicateOperatorType],

				 nil];
	
	template = [[[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:leftExpressions
												 rightExpressionAttributeType:NSDoubleAttributeType
																	 modifier:NSAnyPredicateModifier
																	operators:operators
																	  options:0] 
				autorelease];
	
	[templates addObject:template];
	
	
	
	
	
	// Adjust names
	
	for (NSPredicateEditorRowTemplate *template in templates) {
		
		NSArray *views = [template templateViews];
		
		if (![views count])
			continue;
		
		NSPopUpButton *leftExprView = (NSPopUpButton *)[views objectAtIndex:0];
		
		NSArray *menuItems = [[leftExprView menu] itemArray];
		
		for (NSMenuItem *item in menuItems) {
			NSString *humanReadableString = [self humanReadableNameForAttributeKeyPath:[item title]];
			if (humanReadableString)
				[item setTitle:humanReadableString];
		}
		
		
	}
	
	// Compound
	
	template = [[[NSPredicateEditorRowTemplate alloc] initWithCompoundTypes:[NSArray arrayWithObjects:[NSNumber numberWithInt:NSAndPredicateType], [NSNumber numberWithInt:NSOrPredicateType], nil]] autorelease];
	[templates addObject:template];
	
	return templates;
	
}

- (void)adjustSmartAlbumPredicate:(NSPredicate *)predicate fetchPredicate:(NSPredicate **)fetchPredicate inMemoryPredicate:(NSPredicate **)inMemoryPredicate compoundType:(NSCompoundPredicateType *)compoundType
{
	*fetchPredicate = nil;
	*inMemoryPredicate = nil;
	*compoundType = NSOrPredicateType;
	
	
	//NSLog(@"------------------------------------------------------------------------------------------------");
	//NSLog(@"Splitting predicate:\n\n														%@\n\n", [predicate predicateFormat]);
	
	
	if (![predicate isKindOfClass:[NSCompoundPredicate class]])
		return;
	
	
	NSCompoundPredicate *compoundPredicate = (NSCompoundPredicate *)predicate;
	NSArray *subpredicates = [compoundPredicate subpredicates];
	
	NSMutableArray *storeSubpredicates = [NSMutableArray array];
	NSMutableArray *inMemorySubpredicates = [NSMutableArray array];

	
	for (NSComparisonPredicate *predicate in subpredicates) {
		
		
		// must be comparison predicate
		if (![predicate isKindOfClass:[NSComparisonPredicate class]]) {
			[storeSubpredicates addObject:predicate];
			continue;
		}
		
		// must be a keypath-based expression
		if ([[predicate leftExpression] expressionType] != NSKeyPathExpressionType) {
			[storeSubpredicates addObject:predicate];
			continue;
		}
		

		NSPredicate *modifiedPredicate = predicate;
		
		NSString *keyPath = [[predicate leftExpression] keyPath];
		NSString *value = [[predicate rightExpression] description];

		if ([keyPath isEqualTo:@"keywords"]) {
			NSString *modifiedPredicateFormat = [NSString stringWithFormat: 
												 @"ANY keywords.name CONTAINS[c] %@ "
												 "OR ANY keywords.parent.name CONTAINS[c] %@ "
												 "OR ANY keywords.parent.parent.name CONTAINS[c] %@", value, value, value];
			modifiedPredicate = [NSPredicate predicateWithFormat:modifiedPredicateFormat];
		}
		
		else if ([keyPath isEqualTo:@"rois"]) {
			NSString *modifiedPredicateFormat = [NSString stringWithFormat: 
												 @"ANY rois.comment CONTAINS[c] %@", value];
			modifiedPredicate = [NSPredicate predicateWithFormat:modifiedPredicateFormat];
		}
		
		else if ([keyPath isEqualTo:@"instrument.channels"]) {
			NSString *modifiedPredicateFormat = [NSString stringWithFormat: 
												 @"ANY instrument.channels.channelName CONTAINS[c] %@ "
												 "|| ANY instrument.channels.contrastMethod CONTAINS[c] %@ "
												 "|| ANY instrument.channels.lightSourceName CONTAINS[c] %@ "
												 "|| ANY instrument.channels.lightSourceType CONTAINS[c] %@ "
												 "|| ANY instrument.channels.medium CONTAINS[c] %@ "
												 "|| ANY instrument.channels.mode CONTAINS[c] %@", 
												 value, value, value, value, value, value];
			
			modifiedPredicate = [NSPredicate predicateWithFormat:modifiedPredicateFormat];
		}
		
		
		
		if ([self _isCurrentlyAvailableCustomAttributeKeyPath:keyPath])
			[inMemorySubpredicates addObject:predicate];
		else if ([self _isCustomAttributeKeyPath:keyPath])
			; // an old custom field (that is now removed) -- silently ignore, will be removed when the user edits the predicate in the UI
		else
			[storeSubpredicates addObject:modifiedPredicate];
		
	}
		
	
	// Create compound predicates
	
	NSCompoundPredicate *storeCompoundPredicate = nil;
	if ([storeSubpredicates count] > 0)
		storeCompoundPredicate = [[[NSCompoundPredicate alloc] initWithType:[compoundPredicate compoundPredicateType] subpredicates:storeSubpredicates] autorelease];
	
	NSCompoundPredicate *inMemoryCompoundPredicate = nil;
	if ([inMemorySubpredicates count] > 0)
		inMemoryCompoundPredicate = [[[NSCompoundPredicate alloc] initWithType:[compoundPredicate compoundPredicateType] subpredicates:inMemorySubpredicates] autorelease];
	
	
	
	// Add extra condition to predicate: no stack images allowed
	
	// if the compound type is AND, we should only check the store
	// since [(STORE AND INMEM) AND NONSTACK] == [(STORE AND NONSTACK) AND INMEM]
	// if the compound type is OR, check both
	// since [(STORE OR INMEM) AND NONSTACK] == [(STORE AND NONSTACK) OR (INMEM AND NONSTACK)]
	
	if ([compoundPredicate compoundPredicateType] == NSOrPredicateType && inMemoryCompoundPredicate) {
		
		inMemorySubpredicates = [[NSArray arrayWithObjects:inMemoryCompoundPredicate, [NSPredicate predicateWithFormat:@"stackImage == nil"], nil] mutableCopy];
		inMemoryCompoundPredicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:inMemorySubpredicates] autorelease];	
		
	}
	
	if (storeCompoundPredicate) {
		
		storeSubpredicates = [[NSArray arrayWithObjects:storeCompoundPredicate, [NSPredicate predicateWithFormat:@"stackImage == nil"], nil] mutableCopy];
		storeCompoundPredicate = [[[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:storeSubpredicates] autorelease];
		
	}
	

	
	
	// set output values
	
	*fetchPredicate = storeCompoundPredicate;
	*inMemoryPredicate = inMemoryCompoundPredicate;
	*compoundType = [compoundPredicate compoundPredicateType];
	
	//NSLog(@"Store predicate:\n\n														%@\n\n", [storeCompoundPredicate predicateFormat]);
	//NSLog(@"In-memory predicate:\n\n														%@\n\n", [inMemoryCompoundPredicate predicateFormat]);
	//NSLog(@"Combination type:\n\n														%@\n\n", *compoundType == NSAndPredicateType ? @"AND" : (*compoundType == NSOrPredicateType ? @"OR" : @"UNKNOWN"));

	
	
	//NSLog(@"------------------------------------------------------------------------------------------------");

	
}


- (BOOL)_isCurrentlyAvailableCustomAttributeKeyPath:(NSString *)keyPath
{
	NSArray *customAttrs = [[MGCustomMetadataController sharedController] customAttributes];
	for (NSDictionary *customAttr in customAttrs)
		if ([[customAttr objectForKey:@"path"] isEqualTo:keyPath])
			return YES;
	
	return NO;
}


- (BOOL)_isCustomAttributeKeyPath:(NSString *)keyPath
{
	return [keyPath hasPrefix:@"customMetadata."];
}


@end
