//
//  MGFEIImporter.m
//  Filament
//
//  Created by Dennis Lorson on 17/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGFEIFileImporter.h"

#import "MGImageFileImporter_Private.h"
#import "MGMetadataSet.h"


@interface MGFEIFileImporter ()

+ (NSString *)_endOfFile:(NSString *)path;
- (NSArray *)_propertyRowsFromString:(NSString *)string;
- (NSDictionary *)_propertyDictionaryFromRows:(NSArray *)rows;
- (void)_interpretAndFilterPropertyValuesFromDictionary:(NSDictionary *)dictionary intoMetadataSet:(MGMetadataSet *)set;


@end




@implementation MGFEIFileImporter



+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	NSString *endOfFile = [self _endOfFile:path];
	
	return [endOfFile rangeOfString:@"[User]"].location != NSNotFound;
}


+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:@"tiff", @"tif", nil];
}

- (float)fractionOfImportTimeForNonRepresentationWork;
{
	return 0.2;
}

- (MGMetadataSet *)_generateMetadata
{

	MGMetadataSet *set = [super _generateMetadata];
	
	NSString *fileAppendix = [MGFEIFileImporter _endOfFile:[self originalFilePath]];
	
	NSArray *propertyRows = [self _propertyRowsFromString:fileAppendix];
	
	NSDictionary *properties = [self _propertyDictionaryFromRows:propertyRows];
	//NSLog(@"rows: %@", [properties description]);

	[self _interpretAndFilterPropertyValuesFromDictionary:properties intoMetadataSet:set];
		
	return set;
	
}


- (void)_interpretAndFilterPropertyValuesFromDictionary:(NSDictionary *)dictionary intoMetadataSet:(MGMetadataSet *)set;
{
	// 1) only allow those properties we predefined to match one of the Macnification metadata items
	// 2) convert these to numbers/dates/... if needed

	id value;
	
	// date
	if ((value = [dictionary objectForKey:@"Date"])) {
		NSString *time = [dictionary objectForKey:@"Time"];
		
		if (time)
			value = [value stringByAppendingFormat:@" %@", time];
		
		NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
		
		[dateFormatter setTimeStyle:time ? NSDateFormatterMediumStyle : NSDateFormatterNoStyle];
		
		NSDate *date = [dateFormatter dateFromString:value];
		
		if (date)
			[set setValue:date forKey:MGMetadataImageCreationDateKey];
	}
	
	

	// comment
	if ((value = [dictionary objectForKey:@"UserText"]))
		[set setValue:value forKey:MGMetadataImageCommentKey];
	
	// experimenter
	if ((value = [dictionary objectForKey:@"User"]))
		[set setValue:value forKey:MGMetadataExperimenterNameKey];
	
	// aperture
	if ((value = [dictionary objectForKey:@"ApertureDiameter"]))
		[set setValue:[NSNumber numberWithDouble:[value doubleValue]] forKey:MGMetadataInstrumentNumericApertureKey];
	
	// spot size
	if ((value = [dictionary objectForKey:@"Spot"]))
		[set setValue:[NSNumber numberWithDouble:[value doubleValue]] forKey:MGMetadataInstrumentSpotSizeKey];	
	
	// workingDistance
	if ((value = [dictionary objectForKey:@"WorkingDistance"])) {
		value = [NSNumber numberWithDouble:[value doubleValue] * 1e6];
		[set setValue:value forKey:MGMetadataInstrumentWorkingDistanceKey];	

	}
	
	// calibration
	// seems to be fixed-unit, in terms of meters
	if ([dictionary objectForKey:@"PixelWidth"] && [dictionary objectForKey:@"PixelHeight"]) {
		value = [dictionary objectForKey:@"PixelWidth"];
		value = [NSNumber numberWithDouble:[value doubleValue] * 1e6];
		[set setValue:value forKey:MGMetadataCalibrationPixelWidthKey];	
		
		value = [dictionary objectForKey:@"PixelHeight"];
		value = [NSNumber numberWithDouble:[value doubleValue] * 1e6];
		[set setValue:value forKey:MGMetadataCalibrationPixelHeightKey];	
		
		[set setValue:@"µm" forKey:MGMetadataCalibrationUnitKey];	

	}	
}

- (NSDictionary *)_propertyDictionaryFromRows:(NSArray *)rows
{
	NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
	
	for (NSString *row in rows) {
		NSArray *propAndValue = [row componentsSeparatedByString:@"="];
		if ([propAndValue count] > 1 && [[propAndValue objectAtIndex:1] length] > 0)
			[dictionary setObject:[propAndValue objectAtIndex:1] forKey:[propAndValue objectAtIndex:0]];
	}
	
	return dictionary;
}

- (NSArray *)_propertyRowsFromString:(NSString *)string
{
	// returns an array of strings, each of the form "Property=SomeValue".
	NSScanner *scanner = [NSScanner scannerWithString:string];
	
	[scanner scanUpToString:@"[User]" intoString:nil];

	NSString *metadataPart = [string substringFromIndex:[scanner scanLocation]];

	NSArray *propertyRowsUnfiltered = [metadataPart componentsSeparatedByString:@"\n"];
	
	
	NSMutableArray *propertyRowsFiltered = [NSMutableArray array];
	
	for (NSString *propertyRow in propertyRowsUnfiltered) {
		if ([propertyRow length] > 1 &&
			[propertyRow rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"[]"]].location == NSNotFound) {
			
			NSString *newLineCharRemoved = [propertyRow stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
			[propertyRowsFiltered addObject:newLineCharRemoved];

		}
	}
	
	return propertyRowsFiltered;
}

+ (NSString *)_endOfFile:(NSString *)path;
{
	NSString *file = [path stringByExpandingTildeInPath];
	
	NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:file];
	
	unsigned long long size = [handle seekToEndOfFile];
	
	unsigned long long startPos = MAX(0, (long long)(size - 1024 * 20));
	
	[handle seekToFileOffset:startPos];
	
	NSMutableData *data = [[[handle readDataToEndOfFile] mutableCopy] autorelease];
	
	char *buff = (char *)[data mutableBytes];
	
	// remove all NULL chars from the data
	NSInteger len = [data length];
	for (int i = 0; i < len; i++) {
		if (buff[i] == '\0')
			buff[i] = '*';
	}
	
	NSString *string = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
	
	return string;
}

@end
