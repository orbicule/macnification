//
//  ChannelMergeBinImageLayer.m
//  Filament
//
//  Created by Dennis Lorson on 25/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ChannelMergeBinImageLayer.h"


@implementation ChannelMergeBinImageLayer

@synthesize image, compositeImage;


- (id)init
{
	if ((self = [super init])) {
		
		
		self.tileSize = CGSizeMake(2000,2000);
		self.levelsOfDetail = 1;
		//self.backgroundColor = CGColorCreateGenericGray(1, 0.02);
		
		self.contentsGravity = kCAGravityResizeAspect;
		self.opaque = NO;
		self.needsDisplayOnBoundsChange = NO;
				
	}
	return self;
}


- (void) dealloc
{
	
	[image release];
	[compositeImage release];
	
	[super dealloc];
}


- (void)setCompositeImage:(NSBitmapImageRep *)img
{
	if (compositeImage != img) {
		
		[compositeImage release];
		compositeImage = [img retain];
		
	}
	
	[self setNeedsDisplay];
}

- (void)setImage:(Image *)img
{
	if (image != img) {
		
		[image release];
		image = [img retain];
		
	}
	
	[self setNeedsDisplay];
}




- (void)drawInContext:(CGContextRef)ctx
{
	NSGraphicsContext *nsGraphicsContext = [NSGraphicsContext graphicsContextWithGraphicsPort:ctx flipped:NO];
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:nsGraphicsContext];
	
	CGRect destRect = CGContextGetClipBoundingBox(ctx);
	
	if (image) {
		
		CIImage *filtered = [image filteredCIImage];
		
		[[nsGraphicsContext CIContext] drawImage:filtered inRect:destRect fromRect:[filtered extent]];
		
	} else if (compositeImage) {
		
		[compositeImage drawInRect:NSRectFromCGRect(destRect)];
		
	}
	
	[NSGraphicsContext restoreGraphicsState];
}

@end
