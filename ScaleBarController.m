//
//  ScaleBarController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 4 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import "ScaleBarController.h"
#import "MGAppDelegate.h"
#import "Image.h"
#import "MGLibraryController.h"


@implementation ScaleBarController


- (id)initWithImageArrayController:(ImageArrayController *)controller;
{
	if ((self = [super initWithWindowNibName:@"ScaleBar"])) {
		[self setImageArrayController:controller];
		return self;
	}
	return nil;
}



- (void)setup;
{
	// Initialise the font button
	NSString *fontName = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"scaleBarFontName"];
	NSNumber *fontSize = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"scaleBarFontSize"];
	NSFont *font = [NSFont fontWithName:fontName size:[fontSize floatValue]];
	[fontNameButton setTitle:[NSString stringWithFormat:@"%@ %@pt", [font displayName], fontSize]];
	
	// Initialise the scaleBarPositionView
	Image *selectedImage = [[[self imageArrayController] selectedObjects]objectAtIndex:0];
	[scaleBarPositionView setImage:[[selectedImage displayImage] filteredImageRef] imageProperties:nil];
	[scaleBarPositionView setScaleBarLayer];

	
	// Warn the user if some images are not calibrated
	int numberOfUncalibratedImages = 0;
	int numberOfSelectedImages = [[[self imageArrayController] selectedObjects] count];
	for (Image *image in [[self imageArrayController] selectedObjects]) {
		if (![image	isCalibrated]) {
			numberOfUncalibratedImages++;
		}
	}
	
	NSString *stringForImages = (numberOfSelectedImages > 1)? @"images": @"image";
	if (numberOfUncalibratedImages > 0) {
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"OK"];
		[alert setMessageText:[NSString stringWithFormat:@"Macnification cannot add a scale bar to %d of %d %@", numberOfUncalibratedImages, numberOfSelectedImages, stringForImages]];
		[alert setInformativeText:
		 @"Some of the selected images are not calibrated. Only calibrated images can have a scale bar.\n\n"
		 "In order to calibrate an image:\n"
		 " - Choose Image -> Analyze to go full screen\n"
		 " - Select the Line Tool\n"
		 " - Draw a line ROI over an object of known length\n"
		 " - Click the Calibrate button"];
		[alert setAlertStyle:NSWarningAlertStyle];
		[alert runModal];
		[alert release];
	}
	// Only show the scale bar panel if at least one image is calibrated
	if (numberOfSelectedImages - numberOfUncalibratedImages > 0) {
		[NSApp beginSheet:[self window] modalForWindow:[NSApp mainWindow] modalDelegate:self didEndSelector:NULL contextInfo:nil];
	}
	
}




- (IBAction)addScaleBar:(id)sender;
{
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	NSNumber *xOrigin = [userDefaults valueForKey:@"scaleBarXProportion"];
	NSNumber *yOrigin = [userDefaults valueForKey:@"scaleBarYProportion"];
	NSNumber *stroke = [userDefaults valueForKey:@"scaleBarStroke"];
	NSColor *color = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"scaleBarColor"]];
	NSString *style = [userDefaults valueForKey:@"scaleBarStyle"];
	NSNumber *drawsLabel = [userDefaults valueForKey:@"showsScaleBarLabel"];
	NSString *fontName = [userDefaults valueForKey:@"scaleBarFontName"];
	NSNumber *fontSize = [userDefaults valueForKey:@"scaleBarFontSize"];
	NSNumber *restrictToStandardSizes = [userDefaults valueForKey:@"scaleBarRestrictedToStandardSizes"];
	
	NSColor *backgroundBoxColor = [userDefaults valueForKey:@"scaleBarBackgroundBoxColor"];
	NSNumber *useBackgroundBox = [userDefaults valueForKey:@"scaleBarUseBackgroundBox"];
	
	
	NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:xOrigin, @"xOrigin", yOrigin, @"yOrigin", stroke,
								@"stroke", color, @"color", style, @"style", drawsLabel, @"showsLabel", fontName, @"fontName", fontSize, 
								@"fontSize", restrictToStandardSizes, @"restrictToStandardSizes",
								backgroundBoxColor, @"backgroundBoxColor", useBackgroundBox, @"useBackgroundBox", NULL];
	for (Image *image in [[self imageArrayController] selectedObjects]) {
		[image addScaleBarWithProperties:properties];
	}
	[self closeScaleBarPanel:self];
	[imageArrayController startScaleBarAnimation];
}


- (IBAction)closeScaleBarPanel:(id)sender;
{
	[[self window] orderOut:nil];
	[NSApp endSheet:[self window]];   
}



- (IBAction)showFontPanel:(id)sender;
{
	NSFontPanel *fontpanel = [NSFontPanel sharedFontPanel];
	[fontpanel orderFront: self];
	// Set window as firstResponder so we get changeFont: messages
	[[self window] makeFirstResponder:[self window]];
}


- (void)changeFont:(id)sender;
{
    /*
	 This is the message the font panel sends when a new font is selected
	 */
	// Get selected font
	NSFontManager *fontManager = [NSFontManager sharedFontManager];
	NSFont *selectedFont = [fontManager selectedFont];
	if (selectedFont == nil)
	{
		selectedFont = [NSFont systemFontOfSize:[NSFont systemFontSize]];
	}
	NSFont *panelFont = [fontManager convertFont:selectedFont];
	
	// Get and store details of selected font
	// Note: use fontName, not displayName.  The font name identifies the font to
	// the system, we use a value transformer to show the user the display name
	NSNumber *fontSize = [NSNumber numberWithFloat:[panelFont pointSize]];	
	
	//NSLog(@"Panel: %@", [panelFont displayName]);
	
	// Update the user defaults
	[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:[panelFont fontName] forKey:@"scaleBarFontName"];
	[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:fontSize forKey:@"scaleBarFontSize"];
	
	[fontNameButton setTitle:[NSString stringWithFormat:@"%@ %@pt", [panelFont displayName], fontSize]];
	
	// Update the preview
	[scaleBarPositionView reDraw:self];
}




- (ImageArrayController *)imageArrayController {
    return [[imageArrayController retain] autorelease];
}

- (void)setImageArrayController:(ImageArrayController *)value {
    if (imageArrayController != value) {
        [imageArrayController release];
        imageArrayController = [value retain];
    }
}





@end
