//
//  LightTableImageToolView.h
//  LightTable
//
//	This class displays the custom drawing on top of a light table image (bounding box, control points).
//	It handles no events.
//	Its superview is a LightTableImageView.
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface LightTableImageToolView : NSView 
{

	CGFloat imageSizeInfoBoxOpacity;
	CGFloat controlPointsOpacity;
	CGFloat selectionRectangleOpacity;
	
	NSBezierPath *commentIndicator;
		
	NSTrackingArea *viewTrackingArea;
	
	BOOL showsScalebarIfAvailable;
	

}

@property (nonatomic) CGFloat imageSizeInfoBoxOpacity, controlPointsOpacity, selectionRectangleOpacity;
@property (nonatomic) BOOL showsScalebarIfAvailable;

enum 
{
	UpperLeftControlPoint = 1,
	MiddleLeftControlPoint = 2,
	LowerLeftControlPoint = 3,
	UpperMiddleControlPoint = 4,
	LowerMiddleControlPoint = 5,
	UpperRightControlPoint = 6,
	MiddleRightControlPoint = 7,
	LowerRightControlPoint = 8,
	NoControlPoint = 9
};

typedef NSInteger ControlPoint;


- (NSBezierPath *)pathForControlPoint:(ControlPoint)thePoint;
- (ControlPoint)controlPointAtPoint:(NSPoint)point;

- (void)drawSelectionRectangle;

- (void)drawScalebar;



@end
