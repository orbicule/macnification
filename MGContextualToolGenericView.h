//
//  MGContextualToolSeparator.h
//  StackTable
//
//  Created by Dennis Lorson on 21/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


typedef enum _MGContextualToolType
{
	
	MGWhitespaceToolType = 0,
	MGSeparatorToolType = 1
	
} MGContextualToolType;


@interface MGContextualToolGenericView : NSView {
	
	
	MGContextualToolType toolType;

}


@property(readwrite) MGContextualToolType toolType;


@end
