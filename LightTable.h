//
//  LightTable.h
//  LightTable
//
//	NSManagedObject subclass that handles lighttable model-related responsabilities.
//
//  Created by Dennis on 13/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Album.h"
 
@class LightTableImage;
@class LightTableContentView;

@interface LightTable : Album 
{

}

- (void)insertImageObjects:(NSArray *)images atLocation:(NSPoint)location inView:(LightTableContentView *)theView;

// temporary
- (NSArray *)insertImages:(NSArray *)fileNames atLocation:(NSPoint)location inView:(LightTableContentView *)view;
- (LightTableImage *)insertImage:(NSString *)filePath;

@end
