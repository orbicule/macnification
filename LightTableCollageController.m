//
//  LightTableCollageController.m
//  Filament
//
//  Created by Dennis Lorson on 22/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "LightTableCollageController.h"
#import "LightTableCollagePreview.h"
#import "StringExtensions.h"
#import "MGAppDelegate.h"
#import "SourceListTreeController.h"
#import "MGLibraryController.h"

@implementation LightTableCollageController



- (void)showCollagePreviewWithImage:(NSImage *)image;
{
	[self changeBackgroundColor:self];
	
	[collageView setImage:image];
	
	[NSApp beginSheet:collageWindow modalForWindow:[NSApp mainWindow] modalDelegate:self didEndSelector:nil contextInfo:nil];
	
}

- (IBAction)changeBackgroundColor:(id)sender;
{
	if (sender == backgroundColorWell)
		[useBackgroundColorCheckbox setState:NSOnState];
	
	[collageView setBackgroundColor:([useBackgroundColorCheckbox state] == NSOnState) ? [backgroundColorWell color] : [NSColor clearColor]];
}


- (IBAction)cancelCollage:(id)sender
{
	[collageView releaseResources];

	
	[NSApp endSheet:collageWindow];
	[collageWindow orderOut:self];
}

- (IBAction)importCollage:(id)sender
{
	collage = [collageView selectedImage];
	[collageView releaseResources];
	
	[NSApp endSheet:collageWindow];
	[collageWindow orderOut:self];
	
	
	
	NSString *imageName = [collageNameTextField stringValue];
	if ([imageName length] == 0)
		imageName = @"untitled image";
	
    [tmpImportPath release];
    tmpImportPath = [[[NSString stringWithFormat:@"/tmp/%@.tiff", imageName] uniquePath] retain];
	
	[[collage TIFFRepresentationUsingCompression:NSTIFFCompressionLZW factor:1.0] writeToFile:tmpImportPath atomically:YES];
	
    
    // Send the array of selected files to the ImageImporter
    MGImageBatchImporter *imp = [[[MGImageBatchImporter alloc] init] autorelease];
    imp.splitChannels = NO;
    imp.libraryAlbum = [[MGLibraryControllerInstance library] libraryGroup];
    imp.lastImportAlbum = [[MGLibraryControllerInstance library] lastImportGroup];
    imp.projectsAlbum = [[MGLibraryControllerInstance library] projectsCollection];
    imp.delegate = self;
    
    [imp importImageFilesAndDirectories:[NSArray arrayWithObject:tmpImportPath]];
		
	collage = nil;
}

- (void)imageImporter:(MGImageBatchImporter *)importer didImportImages:(NSArray *)images;
{
    [[NSFileManager defaultManager] removeItemAtPath:tmpImportPath error:nil];
    [tmpImportPath release];
    tmpImportPath = nil;
}

- (IBAction)exportCollage:(id)sender
{
	collage = [[collageView selectedImage] retain];
	[collageView releaseResources];

	
	[NSApp endSheet:collageWindow];
	[collageWindow orderOut:self];
	
	NSString *imageName = [collageNameTextField stringValue];
	if ([imageName length] == 0)
		imageName = @"untitled image";
	
	
	NSSavePanel *panel = [NSSavePanel savePanel];

	
	if (!saveOptions)
		saveOptions = [[IKSaveOptions alloc] initWithImageProperties:nil imageUTType:nil];
	
	[saveOptions addSaveOptionsAccessoryViewToSavePanel:panel];
	
	[panel beginSheetForDirectory:nil 
							 file:@"untitled image" 
				   modalForWindow:[NSApp mainWindow]
					modalDelegate:self 
				   didEndSelector:@selector(exportPanelDidEnd:returnCode:contextInfo:) 
					  contextInfo:nil];
	
}

- (void)exportPanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo;
{
	
	[sheet close];
	
	if (returnCode == NSOKButton) {
		
		NSDictionary *fileTypesAndExtensions = [NSDictionary dictionaryWithObjectsAndKeys:
												@"jpg", kUTTypeJPEG,
												@"jpg", kUTTypeJPEG2000,
												@"gif", kUTTypeGIF,
												@"bmp", kUTTypeBMP,
												@"exr", @"com.ilm.openexr-image",
												@"pdf", kUTTypePDF,
												@"psd", @"com.adobe.photoshop-​image",
												@"pic", kUTTypePICT,
												@"png", kUTTypePNG,
												@"sgi", @"com.sgi.sgi-image",
												@"tga", @"com.truevision.tga-image",
												@"tif", kUTTypeTIFF,
												nil];
		
		NSString *extension = [fileTypesAndExtensions objectForKey:[saveOptions imageUTType]];
		if (!extension) extension = @"tif";

		
		NSString *finalPath = [[[sheet filename] stringByDeletingPathExtension] stringByAppendingPathExtension:extension];
		
		NSBitmapImageRep *imageRepToWrite = [[[NSBitmapImageRep alloc] initWithData:[collage TIFFRepresentation]] autorelease];
		
		NSString *imageType = [saveOptions imageUTType];
		CGImageRef image = [imageRepToWrite CGImage];
		
		NSURL *url = [NSURL fileURLWithPath:finalPath];
		CGImageDestinationRef dest = CGImageDestinationCreateWithURL((CFURLRef)url, (CFStringRef)imageType, 1, NULL);
		CGImageDestinationAddImage(dest, image, (CFDictionaryRef)[saveOptions imageProperties]);
		CGImageDestinationFinalize(dest);
		CFRelease(dest);
		
		
		
		
	}	
	
	
	[collage release];
	collage = nil;
}


- (void)dealloc
{
	[saveOptions release];
	
	[super dealloc];
}


@end
