//
//  FormatterController.m
//  Filament
//
//  Created by Dennis Lorson on 21/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "FormatterController.h"

static FormatterController *sharedController = nil;


@implementation FormatterController


+ (FormatterController *)sharedFormatterController
{
	
	if (!sharedController) {
		
		sharedController = [[FormatterController alloc] init];
	
	}
	
	return sharedController;
}


- (id)init
{
	self = [super init];
	
	if (self != nil) {
		
		[NSBundle loadNibNamed:@"Formatters.nib" owner:self];
		
	}
	
	return self;
}


- (NSString *)formattedStringForValue:(id)value ofKeyPath:(NSString *)keyPath
{
	if ([keyPath isEqualToString:@"name"]) {
		
		return value;
		
	}
	
	if ([keyPath isEqualToString:@"instrument.instrumentName"]) {
		
		return value;
		
	}
	
	if ([keyPath isEqualToString:@"comment"]) {
		
		return value;
		
	}
	
	if ([keyPath isEqualToString:@"instrument.voltage"]) {
		
		return [voltageFormatter stringFromNumber:(NSNumber *)value];
		
	}
	
	if ([keyPath isEqualToString:@"instrument.workingDistance"]) {
		
		return [workingDistanceFormatter stringFromNumber:(NSNumber *)value];
		
	}
	
	if ([keyPath isEqualToString:@"instrument.magnification"]) {
		
		return [magnificationFormatter stringFromNumber:(NSNumber *)value];
		
	}
	
	if ([keyPath isEqualToString:@"instrument.spotSize"]) {
		
		return [spotSizeFormatter stringFromNumber:(NSNumber *)value];
		
	}
	
	if ([keyPath isEqualToString:@"creationDate"] || [keyPath isEqualToString:@"importDate"] || [keyPath isEqualToString:@"modificationDate"]) {
		
		return [standardDateFormatter stringFromDate:(NSDate *)value];
		
	}
	
	if ([keyPath isEqualToString:@"suggestedScaleBarLength"]) {
		
		return [suggestedScalebarLengthFormatter stringFromNumber:(NSNumber *)value];
		
	}
	
	
	return value;
}


@end
