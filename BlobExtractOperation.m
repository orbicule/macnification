//
//  BlobExtractOperation.m
//  BlobExtraction
//
//  Created by Dennis Lorson on 02/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "BlobExtractOperation.h"
#import "NSBezierPath_MGAdditions.h"
#import "MGCoreImageMutex.h"

// inline function to save on calls
// only compare the first pixel since it is much cheaper
#define REPixelIsBackground(A)  ((A)[1] < 128)// && (A)[2] < 128 && (A)[3] < 128)





// -----------------------------------------------------------------------------------------------------------------------------------------------------



struct RECluster_
{
	
	// the region this cluster belongs to
	RERegion *region;
	
	// linked list support: next cluster in a list
	RECluster *next;
	
};


struct RERegion_
{
	
	// the clusters belonging to this region, as a linked list.  Used in the region growing step.
	RECluster *clusters;
	
	// the gathered pixels after assigning them to the region, and before sorting.
	CFMutableArrayRef pixels;
	
	// array containing all the pixel groups (== pixels connected through 4-connectivity) gathered when sorting the pixels
	// one region can contain several groups; one is the outer blob, all the others are inner blobs
	CFMutableArrayRef sortedPixelGroups[100];
	
};

struct REData_
{
	
	// the REPixel structures for each pixel
	REPixel *pixels;
	REPixel *regionOrderedPixels;
	
	// image info
	NSInteger w, h, spp, bpr;
	unsigned char *imageData;
	
	CFMutableSetRef grownRegions;
	RERegion **grownRegionsArray;	// to be able to process them efficiently (by index)
	
	// keep a separate ref to all allocated structures, so they can be freed later.
	CFMutableSetRef allocatedRegions, allocatedClusters;
	
	
};


struct REPixel_ 
{
	
	// the cluster this pixel belongs to
	RECluster *cluster;
	
	// coordinates, to easily reference the position in the spatial memory block from the region/cluster based ordering
	short x, y;
	
	char isFiltered:1;
	
};

struct REInterpolationSegment_
{
	
	// nb values to interpolate (including the static endpoints)
	NSInteger count;
	
	// contains the values to be approximated (including the endpoints)
	NSInteger *measurementX, *measurementY;
	
	// contains the approximation (including the endpoints), which is updated until a certain criterion is satisfied
	// the indexes correspond to the measurement indexes, e.g. approximation[2] is the approximation for measurement[2].
	// each value is either fixed (that means an interpolation point has been set) and has a "real value",
	// or it is not set (INT_MAX).  If it is not set, the algorithm interpolates between the two nearest fixed points.
	NSInteger *approximationX, *approximationY;
	
	
};




// -----------------------------------------------------------------------------------------------------------------------------------------------------



@implementation BlobExtractOperation


NSString *BlobExtractionOperationDidEndNotification = @"BlobExtractionDidEndNotification";


@synthesize image, allowsImageEdgeBlobs, searchesInnerBlobs, minimumBlobArea, maxInterpolationError;



- (id)initWithBinaryImage:(NSBitmapImageRep *)theImage
{
	if ((self = [super init])) {
	
		self.image = theImage;
		self.minimumBlobArea = 20;
		self.allowsImageEdgeBlobs = YES;
		self.searchesInnerBlobs = YES;
		self.maxInterpolationError = 1.0;
		
	}
	return self;
}



- (void)main
{
	// algorithm method.
	//NSDate *date = [NSDate date];
	[self extractBlobs];
	//NSLog(@"time elapsed with: %f -- sizeof == %ld", [[NSDate date] timeIntervalSinceDate:date], sizeof(REPixel));
}


- (void)dealloc
{
	if (data)
		REReclaimResources(data);
	data = NULL;
		
	self.image = nil;
		
	[super dealloc];
}


- (void)extractBlobs
{
	NSBitmapImageRep *inputRep = self.image;

	NSDate *date = [NSDate date];
	
	unsigned char *imageData = NULL;
	
	MGCoreImageMutex *mutex = [MGCoreImageMutex sharedMutex];
	
	@synchronized(mutex) {
		imageData = [inputRep bitmapData];
	}
		
	//NSLog(@"image data %fs", -[date timeIntervalSinceNow]);
	date = [NSDate date];
	
	NSInteger w = [inputRep pixelsWide];
	NSInteger h = [inputRep pixelsHigh];
	NSInteger spp = [inputRep samplesPerPixel];
	NSInteger bpr = [inputRep bytesPerRow];
	
	data = REDataCreate(w, h, bpr, spp, imageData);
	
	if ([self isCancelled]) {
		NSLog(@"Stopping region extractor %@", [self description]);
		return;
	}
	REApplyMorphOperators(data);
	
	//NSLog(@"morph %fs", -[date timeIntervalSinceNow]);
	date = [NSDate date];
	
	if ([self isCancelled]) {
		NSLog(@"Stopping region extractor %@", [self description]);
		return;
	}
	REGrowRegions(data);
	
	//NSLog(@"grow %fs", -[date timeIntervalSinceNow]);
	date = [NSDate date];
	
	if ([self isCancelled]) {
		NSLog(@"Stopping region extractor %@", [self description]);
		return;
	}
	REPruneRegions(data);
	
	//NSLog(@"prune %fs", -[date timeIntervalSinceNow]);
	date = [NSDate date];
	
	if ([self isCancelled]) {
		NSLog(@"Stopping region extractor %@", [self description]);
		return;
	}
	RERestructurePixels(data, 1);
	
	//NSLog(@"restructure %fs", -[date timeIntervalSinceNow]);
	date = [NSDate date];
	
	if ([self isCancelled]) {
		NSLog(@"Stopping region extractor %@", [self description]);
		return;
	}
	
	RESortRegions(data, searchesInnerBlobs, maxInterpolationError);
	
	//NSLog(@"sort %fs", -[date timeIntervalSinceNow]);
	date = [NSDate date];
	
	if ([self isCancelled]) {
		NSLog(@"Stopping region extractor %@", [self description]);
		return;
	}
	NSArray *blobs = [self convertRegionData:data minimumArea:minimumBlobArea innerBlobs:searchesInnerBlobs];
		
	// send the results through a notification.
	// the receiver will have to call -performSelectorOnMainThread with the given userInfo.
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:blobs, @"rois", nil];
	NSNotification *note = [NSNotification notificationWithName:BlobExtractionOperationDidEndNotification object:self userInfo:userInfo];
	[[NSNotificationCenter defaultCenter] postNotification:note];

}


- (NSArray *)convertRegionData:(REData *)processData minimumArea:(NSInteger)minArea innerBlobs:(BOOL)retainInnerBlobs;
{
	// return:  NSArray consisting of NSArrays consisting of NSValues encoding the points.
	// The returned blobs satisfy:
	//	- they exceed the minimum area
	//	- they are outer blobs if this constraint is active
	
	NSMutableArray *blobs = [NSMutableArray array];
	
	CFMutableSetRef foundRegions = processData->grownRegions;
	
	RERegion **allRegions = processData->grownRegionsArray;
	
	
	int i, k;
	
	// for each region
	for (i = 0; i < CFSetGetCount(foundRegions); i++) {
		
		RERegion *currentRegion = allRegions[i];
		
		NSInteger groupIndex = 0;
		CFMutableArrayRef currentPixelGroup = currentRegion->sortedPixelGroups[0];
		
		// for each region pixel group
		while (currentPixelGroup) {
			
			if (groupIndex > 0 && !retainInnerBlobs) break;
			
			if ([self pixelAreaOfRegion:currentPixelGroup] < minArea) break; 
			
			NSMutableArray *blobPoints = [NSMutableArray array];
			
			// for each pixel
			for (k = 0; k < CFArrayGetCount(currentPixelGroup); k++) {
				
				REPixel *pix = (REPixel *)CFArrayGetValueAtIndex(currentPixelGroup, k);
				
				NSPoint pixelLocation = NSMakePoint(pix->x, pix->y);
				
				// flip the points in the y direction to correspond to the non-flipped image
				pixelLocation.y = processData->h - 1 - pixelLocation.y;
				
				if (!(pix->isFiltered))
					[blobPoints addObject:[NSValue valueWithPoint:pixelLocation]];
				
			}
			
			if ([blobPoints count] > 2)
				[blobs addObject:blobPoints];
			
			groupIndex++;
			currentPixelGroup = currentRegion->sortedPixelGroups[groupIndex];
			
		}
	}
	
	return blobs;
}


- (NSInteger)pixelAreaOfRegion:(CFArrayRef)pixels
{
	NSBezierPath *path = [self bezierWithPoints:pixels];
	
	if (!path) return 0;
	
	return [path area];
	
	NSRect bounds = NSIntegralRect([path bounds]);
	
	NSInteger pixelsInBezier = 0;
	
	NSInteger xRes, yRes, areaRes;
	
	xRes = ceilf(bounds.size.width/50.);
	yRes = ceilf(bounds.size.height/50.);

	areaRes = xRes * yRes;
	
	/*if (bounds.size.width > 50) xRes = 2;
	if (bounds.size.width > 100) xRes = 3;
	if (bounds.size.width > 500) xRes = 10;
	if (bounds.size.width > 50) xRes = 2;*/
	
	
	int x, y;
	
	for (x = NSMinX(bounds); x < NSMaxX(bounds) + 1; x += xRes) {
		
		for (y = NSMinY(bounds); y < NSMaxY(bounds) + 1; y += yRes) {
			
			if ([path containsPoint:NSMakePoint(x, y)]) pixelsInBezier += areaRes;
			
		}
		
	}
	
	return pixelsInBezier;
}

- (NSBezierPath *)bezierWithAppKitPoints:(NSArray *)points
{
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	NSInteger pointCount = [points count];
	
	if (pointCount == 0) return NULL;
	
	int i;
	for (i = 0; i < pointCount; i++) {
		
		NSPoint point = [[points objectAtIndex:i] pointValue];
		
		if (i == 0) [path moveToPoint:point]; else [path lineToPoint:point];
		
	}
	
	[path closePath];
	return path;
	
	
}

- (NSBezierPath *)bezierWithPoints:(CFArrayRef)points
{
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	NSInteger pointCount = CFArrayGetCount(points);
	
	if (pointCount == 0) return NULL;
	
	int i;
	for (i = 0; i < pointCount; i++) {
		
		REPixel *pixel = (REPixel *)CFArrayGetValueAtIndex(points, i);
		
		if (!(pixel->isFiltered) || i == 0) {
			
			NSPoint point = NSMakePoint(pixel->x, pixel->y);
			
			if (i == 0) [path moveToPoint:point]; else [path lineToPoint:point];
			
		}
		
	}
	
	[path closePath];
	return path;
}

#pragma mark Algorithm functions

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pass 0 -- Apply various morphological operators to the image in order to condition it
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void REApplyMorphOperators(REData *data)
{
	
	// close
	REDilate(data);
	REErode(data);
	
	// open
	REErode(data);
	REDilate(data);
	
}


void REDilate(REData *processData) 
{
	// dilate the given image (that is, every black pixel that is adjacent to a white pixel turns white)
	unsigned char *data = processData->imageData;
	NSInteger w = processData->w;
	NSInteger h = processData->h;
	NSInteger bpr = processData->bpr;
	NSInteger spp = processData->spp;
		
	
	NSInteger hMinOne = h - 1;
	NSInteger wMinOne = w - 1;
	
	int x, y;
	
	for (y = 0; y < h; y++) {
		
		NSInteger currRowStart = y * bpr;
		NSInteger prevRowStart = currRowStart - bpr;
		NSInteger nextRowStart = currRowStart + bpr;
		
		BOOL yAboveMinEdge = (y > 0);
		BOOL yBelowMaxEdge = (y < hMinOne);
		
		for (x = 0; x < w; x++) {
			
			// to mark a pixel for transition to white, we set the last component to 1 (to distinguish them from pixels that have always been white)
			unsigned char *pixelAddr = &(data[currRowStart + x * spp]);
			
			if (REPixelIsBackground(pixelAddr)) {
				
				BOOL hasForegroundPixel = NO;
				
				BOOL xAboveMinEdge = (x > 0);
				BOOL xBelowMaxEdge = (x < wMinOne);
				
				// close and distant neighbours ordered to minimize access time (per row -->constant yval)
				hasForegroundPixel = hasForegroundPixel || (xAboveMinEdge && !REPixelIsBackground( &(data[currRowStart + (x - 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (xBelowMaxEdge && !REPixelIsBackground( &(data[currRowStart + (x + 1) * spp]) ));
				
				hasForegroundPixel = hasForegroundPixel || (yAboveMinEdge && !REPixelIsBackground( &(data[prevRowStart + x * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (xAboveMinEdge && yAboveMinEdge && !REPixelIsBackground( &(data[prevRowStart + (x - 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (xBelowMaxEdge && yAboveMinEdge && !REPixelIsBackground( &(data[prevRowStart + (x + 1) * spp]) ));
				
				hasForegroundPixel = hasForegroundPixel || (xBelowMaxEdge && yBelowMaxEdge && !REPixelIsBackground( &(data[nextRowStart + (x + 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (xAboveMinEdge && yBelowMaxEdge && !REPixelIsBackground( &(data[nextRowStart + (x - 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (yBelowMaxEdge && !REPixelIsBackground( &(data[nextRowStart + x * spp]) ));

				
				if (hasForegroundPixel) {
					
					pixelAddr[3] = 1;
					
				}
				
			}
		}
	}
	
	// now pass through the image a second time to check these values as white (1 -> 255 on RGBA) for the first two rows
	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			
			unsigned char *pixelAddr = &(data[y * bpr + x * spp]);
			
			if (pixelAddr[3] == 1) {
				
				pixelAddr[1] = 255;
				pixelAddr[2] = 255;
				pixelAddr[3] = 255;
				
			}
		}
	}
	
	
}


void REErode(REData *processData) 
{
	// erode the given image (that is, every white pixel that is adjacent to a black pixel turns black)
	unsigned char *data = processData->imageData;
	NSInteger w = processData->w;
	NSInteger h = processData->h;
	NSInteger bpr = processData->bpr;
	NSInteger spp = processData->spp;
	
	int x, y;
	
	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			
			// to mark a pixel for transition to black, we set the last component to 1 (to distinguish them from pixels that have always been black)
			unsigned char *pixelAddr = &(data[y * bpr + x * spp]);
			
			if (!REPixelIsBackground(pixelAddr)) {
				
				BOOL hasForegroundPixel = NO;
				
				// close and distant neighbours ordered to minimize access time (per row -->constant yval)
				hasForegroundPixel = hasForegroundPixel || (x > 0 && REPixelIsBackground( &(data[y * bpr + (x - 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (x < w - 1 && REPixelIsBackground( &(data[y * bpr + (x + 1) * spp]) ));

				hasForegroundPixel = hasForegroundPixel || (y > 0 && REPixelIsBackground( &(data[(y - 1) * bpr + x * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (x > 0 && y > 0 && REPixelIsBackground( &(data[(y - 1) * bpr + (x - 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (x < w - 1 && y > 0 && REPixelIsBackground( &(data[(y - 1) * bpr + (x + 1) * spp]) ));
				
				hasForegroundPixel = hasForegroundPixel || (y < h - 1 && REPixelIsBackground( &(data[(y + 1) * bpr + x * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (x < w - 1 && y < h - 1 && REPixelIsBackground( &(data[(y + 1) * bpr + (x + 1) * spp]) ));
				hasForegroundPixel = hasForegroundPixel || (x > 0 && y < h - 1 && REPixelIsBackground( &(data[(y + 1) * bpr + (x - 1) * spp]) ));
				
				
				if (hasForegroundPixel) {
					
					pixelAddr[3] = 1;
					
				}
				
			}
			
			
		}
		
	}
	
	// now pass through the image a second time to check these values as black (1 -> 255 on RGBA)
	
	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			
			unsigned char *pixelAddr = &(data[y * bpr + x * spp]);
			
			if (pixelAddr[3] == 1) {
				
				pixelAddr[1] = 0;
				pixelAddr[2] = 0;
				pixelAddr[3] = 0;
				
			}
		}
	}
	
	
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pass 1 -- Region growing algorithm
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void REGrowRegions(REData *processData)
{
	
	unsigned char *data = processData->imageData;
	NSInteger w = processData->w;
	NSInteger h = processData->h;
	NSInteger bpr = processData->bpr;
	NSInteger spp = processData->spp;
	
	REPixel *pixels = processData->pixels;
	
	int x, y;
	for (y = 0; y < h; y++) {
		
		// loop over one line
		
		// keep track of the last region that was started, in order to know when to merge regions.  
		// This started region is void after each line.
		RERegion *lastStartedRegion = NULL;
		
		for (x = 0; x < w; x++) {
			
			unsigned char *pixelAddr = &(data[y * bpr + x * spp]);
			REPixel *currentPixelInfo = &(pixels[y * w + x]);
			
			BOOL isBgPixel = REPixelIsBackground(pixelAddr);
			
			if (!isBgPixel) {
				
				// detect the cluster (row-blob) that this pixel belongs to -- if there is none, create one (without region!)
				
				RECluster *clusterLeft = NULL;
				
				if (x > 0) {
					clusterLeft = pixels[y * w + (x - 1)].cluster;
				}
				
				if (!clusterLeft) {
					
					// the bg pixel has no cluster left of it.  create a new cluster and region and assign.
					RECluster *newCluster = calloc(1, sizeof(RECluster));
					RERegion *newRegion = calloc(1, sizeof(RERegion));
					newRegion->pixels = NULL;
					
					// add these clusters/regions to their arrays to keep track of them
					CFSetAddValue(processData->allocatedRegions, newRegion);
					CFSetAddValue(processData->allocatedClusters, newCluster);
					
					// assign
					REClusterSetRegion(newCluster, newRegion);
					currentPixelInfo->cluster = newCluster;
					
					lastStartedRegion = newRegion;
					
				} else {
					
					// just assign the preexisting cluster
					
					currentPixelInfo->cluster = clusterLeft;
				}
				
				// the pixel has a cluster now, see if the pixel above it has a cluster.  
				// if so, this cluster region will be the region of the cluster above.
				
				// if this cluster already has a region, the region that exists will be merged with the newly found region. 
				
				if (y > 0 && pixels[(y - 1) * w + x].cluster && pixels[(y - 1) * w + x].cluster->region) {
					// there is a valid region above
					// check if the cluster already inherited a region earlier on.
					// this can be easily verified by comparing the regions
					
					if (lastStartedRegion && lastStartedRegion != currentPixelInfo->cluster->region && 
						currentPixelInfo->cluster->region != pixels[(y - 1) * w + x].cluster->region) {
						
						// the cluster has inherited a region; we need to merge that region with the newly found one.
						REMergeRegions(currentPixelInfo->cluster->region, pixels[(y - 1) * w + x].cluster->region);
						
					} else {
						
						// just let the cluster inherit the region from above, no need to merge (yet)
						REClusterSetRegion(currentPixelInfo->cluster, pixels[(y - 1) * w + x].cluster->region);
						
					}
				}
				
			} else {
				
				// set the info of the pixel to no region
				currentPixelInfo->cluster = NULL;
				
			}
		}
	}
	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pass 2 -- Edge pruning and per-region data ordering
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void REPruneRegions(REData *processData)
{
	
	// only retain the edge points.
	// loop through all the pixels; if a pixel is an edge pixel (4-connectivity) retain it and add it to the data structure for the corresponding region.
	
	CFMutableSetRef foundRegions = processData->grownRegions;
	
	unsigned char *data = processData->imageData;
	NSInteger w = processData->w;
	NSInteger h = processData->h;
	NSInteger bpr = processData->bpr;
	NSInteger spp = processData->spp;
	
	REPixel *pixels = processData->pixels;
	
	int x, y;
	
	for (y = 0; y < h; y++) {
		
		for (x = 0; x < w; x++) {
			
			REPixel *currentPixelInfo = &(pixels[y * w + x]);
			
			unsigned char *pixelAbove = &(data[(y - 1) * bpr + x * spp]);
			unsigned char *pixelBelow = &(data[(y + 1) * bpr + x * spp]);
			unsigned char *pixelLeft = &(data[y * bpr + (x - 1) * spp]);
			unsigned char *pixelRight = &(data[y * bpr + (x + 1) * spp]);
			
			BOOL hasPixelAbove = (y > 0 && REPixelIsBackground(pixelAbove));
			BOOL hasPixelBelow = (y < h - 1 && REPixelIsBackground(pixelBelow));
			BOOL hasPixelLeft = (x > 0 && REPixelIsBackground(pixelLeft));
			BOOL hasPixelRight = (x < w - 1 && REPixelIsBackground(pixelRight));
			
			BOOL isEdgePixel = ( hasPixelAbove || hasPixelBelow || hasPixelLeft || hasPixelRight || x == 0 || y == 0 || x == w - 1 || y == h - 1);
			
			if (!isEdgePixel) {
				
				currentPixelInfo->cluster = NULL;
				
			}
			
			
			if (currentPixelInfo->cluster && currentPixelInfo->cluster->region) {
				
				currentPixelInfo->x = x;
				currentPixelInfo->y = y;
				
				if (currentPixelInfo->cluster->region->pixels == NULL)
					currentPixelInfo->cluster->region->pixels = CFArrayCreateMutable(NULL, 0, NULL);
				
				// add the pixel info to the region
				CFArrayAppendValue(currentPixelInfo->cluster->region->pixels, currentPixelInfo);
				
				// add the region to the set (which will provide uniquing for us)
				CFSetAddValue(foundRegions, currentPixelInfo->cluster->region);
			}
			
		}
		
	}
	
	// we now have a set of regions, each containing a set of pixels.
	//NSLog(@"region count: %ld", CFSetGetCount(foundRegions));	
	
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pass 2b -- PixelInfo memory restructuring
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// here, we create a second copy of the pixel info, this time ordered by region and without any non-edge points.
// The purpose is to dramatically decrease cache misses, thus improving the time of ~O(n^2) memory accesses n the sort procedure.

void RERestructurePixels(REData *processData, int restructure) 
{
	
	// use the second scratchpad for the region pixels, this time ordered by region.
	// the second buffer has the size of the original image too, which is obviously much too large, but play it safe here...
	
	REPixel *newPixLocation = processData->regionOrderedPixels;
	
	CFMutableSetRef foundRegions = processData->grownRegions;
	
	processData->grownRegionsArray = (RERegion **) calloc(CFSetGetCount(foundRegions), sizeof(RERegion *));
	
	RERegion **allRegions = processData->grownRegionsArray;
	
	CFSetGetValues(foundRegions, (const void **)allRegions);
	
	if (!restructure) return;
	
	// the location to which to copy the REPixel struct
	NSInteger currentPixelAddress = 0;
	
	int i; // region, point and closest neighbour loop
	for (i = 0; i < CFSetGetCount(foundRegions); i++) {
		
		RERegion *currentRegion = allRegions[i];
		CFMutableArrayRef pixels = currentRegion->pixels;
		
		// this array will contain references to the copied objects
		CFMutableArrayRef orderedPixelRefs = CFArrayCreateMutable(NULL, CFArrayGetCount(pixels), NULL);
		int j;
		for (j = 0; j != CFArrayGetCount(pixels); j++) {
			
			// get the old pixel address
			REPixel *oldPix = (REPixel *)CFArrayGetValueAtIndex(pixels, j);
			
			// copy the pixel to the new address
			REPixel *newPix = (REPixel *) memcpy(&(newPixLocation[currentPixelAddress]), oldPix, sizeof(REPixel));
			
			// add the new address to the new array
			CFArrayAppendValue(orderedPixelRefs, newPix);
			
			// increase the address
			currentPixelAddress++;
		}
		
		// the region array is complete, replace the old one by the new one
		CFRelease(pixels);
		currentRegion->pixels = orderedPixelRefs;
		
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pass 3 -- Per-region sorting
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RESortRegions(REData *processData, int retainInnerBlobs, CGFloat maxInterpolationError)
{
		
	CFMutableSetRef foundRegions = processData->grownRegions;
	
	// edit: we already did this in 2b.
	//processData->grownRegionsArray = (RERegion **) calloc(CFSetGetCount(foundRegions), sizeof(RERegion *));
	
	RERegion **allRegions = processData->grownRegionsArray;
	
	//CFSetGetValues(foundRegions, (const void **)allRegions);
	
	int i; // region, point and closest neighbour loop
	for (i = 0; i < CFSetGetCount(foundRegions); i++) {
		
		RERegion *currentRegion = allRegions[i];
		//NSLog(@"sorting region %ld with %ld points", i, CFArrayGetCount(currentRegion->pixels));
		
		NSInteger currentPixelGroup = 0;
		NSInteger remainingPixels = CFArrayGetCount(currentRegion->pixels);
		
		
		currentRegion->sortedPixelGroups[0] = currentRegion->pixels;
		
		while (remainingPixels > 20 && currentPixelGroup < 99) {
			
			REPixelSort(currentRegion->sortedPixelGroups[currentPixelGroup], &(currentRegion->sortedPixelGroups[currentPixelGroup + 1]), processData);
			
			if (CFArrayGetCount(currentRegion->sortedPixelGroups[currentPixelGroup]) == 0) {
				
				// we have an impossible sort --> abort for this region
				break;
				
			}
			if(currentRegion->sortedPixelGroups[currentPixelGroup + 1])
				remainingPixels = CFArrayGetCount(currentRegion->sortedPixelGroups[currentPixelGroup + 1]);
			else
				remainingPixels = 0;
			
			//NSLog(@"pixels left in this region: %ld", remainingPixels);
			
			currentPixelGroup ++;
			
		}
		
		currentRegion->sortedPixelGroups[99] = NULL;
		
		// determine the pixel group with the max nb pixels; this is the outer blob.
		NSInteger maxNbPixels = 0;
		NSInteger currentGroupIndex = 0;
		NSInteger indexOfMax = -1;
		CFMutableArrayRef pixelGroup = currentRegion->sortedPixelGroups[0];
		while (pixelGroup) {
			if (CFArrayGetCount(pixelGroup) > maxNbPixels) {
				maxNbPixels = CFArrayGetCount(pixelGroup);
				indexOfMax = currentGroupIndex;				
			}
			
			currentGroupIndex++;
			pixelGroup = currentRegion->sortedPixelGroups[currentGroupIndex];
			
		}
		
		if (indexOfMax > 0) {
			// set the outer blob to the first position in the array
			CFMutableArrayRef temp = currentRegion->sortedPixelGroups[0];
			currentRegion->sortedPixelGroups[0] = currentRegion->sortedPixelGroups[indexOfMax];
			currentRegion->sortedPixelGroups[indexOfMax] = temp;
			
		}
		
		
		// interpolate the points
		currentGroupIndex = 0;
		pixelGroup = currentRegion->sortedPixelGroups[0];
		while (pixelGroup) {
			
			REInterpolatePoints(pixelGroup, maxInterpolationError);
			
			currentGroupIndex++;
			pixelGroup = currentRegion->sortedPixelGroups[currentGroupIndex];
			
		}
		
	}
	
	
}




// pixels is the array to sort and will contain the sortable pixels (<= original amt) sorted upon return.
// leftovers will contain an allocated array with the remaining pixels. if no leftover pixels are present, leftovers == NULL
// if this method cannot sort the pixels in any way, the pixels array is emptied and leftovers == NULL
void REPixelSort(CFMutableArrayRef pixels, CFMutableArrayRef *leftovers, REData *processData)
{
	
	unsigned char *imageData = processData->imageData;
	int bpr = processData->bpr;
	int spp = processData->spp;
	int w = processData->w;
	int h = processData->h;
		
	int pixelCount = CFArrayGetCount(pixels);
	
	//NSLog(@"-----------------------");
	//NSLog(@"nb points %i", pixelCount);
	
	int j,k;
	
	int nbIterationsAfterLastHit = 0;	
	
	// sort by iterating through the array.  For each point, find its neighbour by choosing the pixel with at most sqrt(2) distance
	for (j = 0; j < pixelCount - 1; j++) {
		
		// init
		
		REPixel *from = (REPixel *)CFArrayGetValueAtIndex(pixels, j);
		int indexOfClosestPixel = 0;
		float minDistance = 100000.;
		int minDistanceInt = 100000;	// avoid FP ops by introducing an integer value that contains the value of minDistance
		
		// find the minimum distance
		for(k = j + 1; k < pixelCount; k++) {
			
			// again, we want to minimize the FP ops (in the distance computation)
			// so do an integer check to filter out 99% of the cases where we CAN make the comparison simpler.
			// if one of the distances (in x or y direction) is larger than minDistance, the (x+y) distance will be too.
			
			REPixel *to = (REPixel *)CFArrayGetValueAtIndex(pixels, k);
			
			int dx = from->x - to->x;
			
			if ((dx * dx < minDistanceInt)) {
				
				int dy = from->y - to->y;
				
				if (dy * dy < minDistanceInt) {
					
					float currentDist = dx * dx + dy * dy;	// this distance is squared
					if (currentDist < minDistance) {
						minDistance = currentDist;
						minDistanceInt = ceilf(minDistance);
						indexOfClosestPixel = k;
						
						// if the distance is <= 1, we have found an optimum (4-conn) so break from the inner for loop
						if (minDistance <= 1) {
							break;
						}
						
						// if our closest distance is <= 2, we can't afford to stop, 
						// except for when the pixel does not have 4-conn with edge pixels, then dist 2 is the optimum (8-conn)
						else if (minDistance <= 2) {
							
							int x = from->x;
							int y = from->y;
							
							// the pixel may not have any direct neighbours for it to be accepted
							
							if ((x < 1 || REPixelIsBackground( &(imageData[y * bpr + (x - 1) * spp]) )) && 
								(y < 1 || REPixelIsBackground( &(imageData[(y - 1) * bpr + x * spp]) )) &&
								(x > w - 2 || REPixelIsBackground( &(imageData[y * bpr + (x + 1) * spp]) )) &&
								(y > h - 2 || REPixelIsBackground( &(imageData[(y + 1) * bpr + x * spp]) ))) {
								
								// break from the inner for loop (closest neighbour found)
								break;
								
							}
						}
					}
				}				
			}
		}
		
		//printf("%ld iterations were needed\n", k - (j+1));
		
		// if we encountered a min distance, move the pixel to the position of the "from" pixel (+1)
		// if not, move the "from" pixel to the back of the queue.
		if (minDistance <= 2) {
			
			nbIterationsAfterLastHit = 0;
			
			// move the value to the front
			//CFArrayExchangeValuesAtIndices(pixels, indexOfClosestPixel, j + 1);
			REPixel *closestPixel = (REPixel *)CFArrayGetValueAtIndex(pixels, indexOfClosestPixel);
			CFArrayRemoveValueAtIndex(pixels, indexOfClosestPixel);
			CFArrayInsertValueAtIndex(pixels, j + 1, closestPixel);
			
			// did we complete a circle?
			REPixel *first = (REPixel *)CFArrayGetValueAtIndex(pixels, 0);
			REPixel *new = (REPixel *)CFArrayGetValueAtIndex(pixels, j + 1);
			
			// the distance check is very heavy (FP) so avoid calling it too much.
			// instead, look if one of the distances is already larger than the threshold, then automatically the other will be too...
			if (ABS(first->x - new->x) <= 2 && ABS(first->y - new->y) <= 2) {
				
				// the heavier call...
				if (j > 2 && pow((first->x - new->x), 2)+ pow((first->y - new->y), 2) <= 2) {
					
					//NSLog(@"pixel loop found containing %ld pixels", j + 2);
					
					// we have a circle and will not be able to sort the remaining pixels (from j + 2 on).  Copy those into another array and return...
					CFMutableArrayRef remainingPixels = CFArrayCreateMutableCopy(NULL, 0, pixels);
					CFArrayReplaceValues(remainingPixels, CFRangeMake(0, j + 2), NULL, 0); // remove 0 -> j+1
					CFArrayReplaceValues(pixels, CFRangeMake(j + 1, pixelCount - (j + 1)), NULL, 0);
					
					*leftovers = remainingPixels;
					return;
				}
				
			}
			

			
			
		} else {
			
			REPixel *pixelToMoveBack = (REPixel *)CFArrayGetValueAtIndex(pixels, j);
			CFArrayRemoveValueAtIndex(pixels, j);
			CFArrayAppendValue(pixels, pixelToMoveBack);
			
			// since a new pixel is now at the position of the pixel with no neighbour, decrease j in order to compensate for the next loop iteration.
			j--;
			//NSLog(@"moving pixel back");
			nbIterationsAfterLastHit++;
			
			if (nbIterationsAfterLastHit > 500) {
				
				//NSLog(@"SORTING OF PIXELS FAILED, DISCARDING GROUP");
				CFArrayRemoveAllValues(pixels);
				*leftovers = NULL;
				return;
				
				
			}
		}
	}
	
	*leftovers = NULL;
	
}


void REReclaimResources(REData *processData)
{
	free(processData->pixels);
	free(processData->regionOrderedPixels);

	free(processData->grownRegionsArray);
	
	CFRelease(processData->grownRegions);
	
	// free the allocated regions and clusters
	RECluster **clusterRefs = (RECluster **)calloc(CFSetGetCount(processData->allocatedClusters), sizeof(RERegion *));
	RERegion **regionRefs = (RERegion **)calloc(CFSetGetCount(processData->allocatedRegions), sizeof(RERegion *));
	
	CFSetGetValues(processData->allocatedClusters, (const void **)clusterRefs);
	CFSetGetValues(processData->allocatedRegions, (const void **)regionRefs);
	
	int i;	
	
	for(i = 0; i < CFSetGetCount(processData->allocatedRegions); i++) {
		
		if (regionRefs[i] && regionRefs[i]->pixels)
			CFRelease(regionRefs[i]->pixels);
		
		
		// the first sorted pixel array is equal to the "pixels" array so leave it alone (has been deallocated already)
		int j = 1;
		while (regionRefs[i]->sortedPixelGroups[j]) {
			//CFRelease(regionRefs[i]->sortedPixelGroups[j]);
			j++;
		}
		
		free(regionRefs[i]);
		
	}
	
	for(i = 0; i < CFSetGetCount(processData->allocatedClusters); i++) {
		free(clusterRefs[i]);
	}
	
	free(clusterRefs);
	free(regionRefs);
	
	CFRelease(processData->allocatedRegions);
	CFRelease(processData->allocatedClusters);
	
	free(processData);
}


#pragma mark Interpolator

// upon return, all non-retained REPixels have isFiltered set to 1.
void REInterpolatePoints(CFArrayRef points, CGFloat maxInterpolationError)
{
	// divide the pixels into batches of a set amount
	// restructure the data into interpolation segments
	// run the segment interpolation for each of them
	// see which pixels are retained (those who don't have INT_MAX as value anymore)
	
	NSInteger pointCount = CFArrayGetCount(points);
	
	NSInteger segmentPixelCount = 100;
	if (pointCount < 100) segmentPixelCount = 10;
	if (pointCount < 30) segmentPixelCount = 5;
	
	
	NSInteger currentSegmentStartIndex = 0;
	
	while (currentSegmentStartIndex < pointCount) {
		
		NSInteger pointsToUseInSegment = MIN(segmentPixelCount, pointCount - currentSegmentStartIndex);
		
		
		// pack
		REInterpolationSegment *segment = (REInterpolationSegment *) calloc(1, sizeof(REInterpolationSegment));
		segment->count = pointsToUseInSegment;
		segment->measurementX = (NSInteger *) calloc(pointsToUseInSegment, sizeof(NSInteger));
		segment->measurementY = (NSInteger *) calloc(pointsToUseInSegment, sizeof(NSInteger));
		segment->approximationX = (NSInteger *) calloc(pointsToUseInSegment, sizeof(NSInteger));
		segment->approximationY = (NSInteger *) calloc(pointsToUseInSegment, sizeof(NSInteger));
		
		NSInteger i;
		for (i = 0; i < pointsToUseInSegment; i++) {
			
			// fill the measurement data and the endpoints of the approximation data
			
			REPixel *copyPixel = (REPixel *) CFArrayGetValueAtIndex(points, currentSegmentStartIndex + i);
			
			segment->measurementX[i] = copyPixel->x;
			segment->measurementY[i] = copyPixel->y;
			
			if (i == 0 || i == pointsToUseInSegment - 1) {
				
				segment->approximationX[i] = copyPixel->x;
				segment->approximationY[i] = copyPixel->y;
				
			} else {
				
				segment->approximationX[i] = INT_MAX;
				segment->approximationY[i] = INT_MAX;
				
			}
			
		}
		
		// the packing is complete, run the interpolation algorithm
		REInterpolatePointSegment(segment, maxInterpolationError);
		
		
		// unpack the data; this simply means mark the REPixels which correspond to values that are still set to INT_MAX 
		// (there is a separate ref to the REPixels somewhere else to free them later)
		
		for (i = 0; i < pointsToUseInSegment; i++) {
			
			// fill the measurement data and the endpoints of the approximation data
			
			REPixel *copyPixel = (REPixel *) CFArrayGetValueAtIndex(points, currentSegmentStartIndex + i);
			
			if (segment->approximationX[i] == INT_MAX)
				copyPixel->isFiltered = 1;
			
		}
		
		// free resources
		free(segment->measurementX);
		free(segment->measurementY);
		free(segment->approximationX);
		free(segment->approximationY);
		free(segment);
		
		
		// increase segment start point by pointsToUse - 1 to create overlap of one pixel between shapes
		currentSegmentStartIndex += MAX((pointsToUseInSegment - 1), 1);
		
	}
	
}


void REInterpolatePointSegment(REInterpolationSegment *segment, CGFloat errorThreshold)
{
	if (segment->count < 3) return;		// this is a perfect approximation so don't bother
	
	CGFloat maxError = 10000.;
	NSInteger maxErrorIndex;
	
	REFindMaxError(segment, &maxError, &maxErrorIndex);
	
	while (maxError > errorThreshold) {
		
		segment->approximationX[maxErrorIndex] = segment->measurementX[maxErrorIndex];
		segment->approximationY[maxErrorIndex] = segment->measurementY[maxErrorIndex];
		
		REFindMaxError(segment, &maxError, &maxErrorIndex);
		
	}
}

void REFindMaxError(REInterpolationSegment *segment, CGFloat *maxError, NSInteger *maxErrorIndex)
{
	NSInteger *x = segment->measurementX;
	NSInteger *y = segment->measurementY;
	NSInteger *ax = segment->approximationX;
	NSInteger *ay = segment->approximationY;
	
	NSInteger count = segment->count;
	
	NSInteger currentAX, currentAY, currentX, currentY;
	
	CGFloat currentError, interpolatedValueX, interpolatedValueY;
	NSInteger prevFixed, nextFixed, indexDiff;
	
	*maxError = 0.;
	*maxErrorIndex = -1; 
	
	// loop through all points (except endpoints which are fixed) and compute the error (if they are not fixed)
	int i;
	for (i = 1; i < count - 1; i++) {
		
		currentX = x[i];
		currentY = y[i];
		currentAX = ax[i];
		currentAY = ay[i];
		
		// only measure if no fixed point (fixed points are optimal)
		if (currentAX == INT_MAX) {
			
			// find the closest fixed values
			// do not check for array bounds as there need to be at least fixed endpoints
			prevFixed = i;
			while (ax[prevFixed] == INT_MAX)
				prevFixed--;
			
			nextFixed = i;
			while (ax[nextFixed] == INT_MAX)
				nextFixed++;
			
			indexDiff = nextFixed - prevFixed;
			
			interpolatedValueX = (CGFloat)(i - prevFixed)/(CGFloat)indexDiff * ax[nextFixed] + (CGFloat)(nextFixed - i)/(CGFloat)indexDiff * ax[prevFixed];
			interpolatedValueY = (CGFloat)(i - prevFixed)/(CGFloat)indexDiff * ay[nextFixed] + (CGFloat)(nextFixed - i)/(CGFloat)indexDiff * ay[prevFixed];
			
			currentError = powf((x[i] - interpolatedValueX), 2) + powf((y[i] - interpolatedValueY), 2);
			
			if (currentError > *maxError) {
				
				*maxError = currentError;
				*maxErrorIndex = i;
				
			}
		}
	}
}




#pragma mark Auxiliary functions

void REClusterSetRegion(RECluster *cluster, RERegion *region)
{
	if (cluster->region == region) return;
	
	// set the relationship in both ways
	cluster->region = region;
	
	RECluster *temp = region->clusters;
	region->clusters = cluster;
	cluster->next = temp;
	
}

void REPrintClustersFromRegion(RERegion *region)
{
	RECluster *cluster = region->clusters;
	
	while (cluster) {
		
		//NSLog(@"%x", (unsigned int)cluster);
		cluster = cluster->next;
		
	}
	
	
}
/*
int REPixelIsBackground(unsigned char *firstComponent) 
{
	return (firstComponent[1] < 128 && firstComponent[2] < 128 && firstComponent[3] < 128);
	
}*/

void REMergeRegions(RERegion *toMergeFrom, RERegion *toMergeInto)
{
	// 1. iterate through all the clusters of the region to merge from
	// 2. for each cluster, set the region to the "merge to" region
	// 3. for each cluster, add it to the clusters of the new region
	
	RECluster *currentCluster = toMergeFrom->clusters;
	while(currentCluster) {
		RECluster *nextCluster = currentCluster->next;
		
		currentCluster->region = toMergeInto;
		
		// insert the cluster at the start of the cluster linked list of the region
		RECluster *temp = toMergeInto->clusters;
		toMergeInto->clusters = currentCluster;
		currentCluster->next = temp;
		// end insert
		
		currentCluster = nextCluster;
	}
	
}

REData * REDataCreate(NSInteger w, NSInteger h, NSInteger bpr, NSInteger spp, unsigned char *imageData)
{
	REData *data = (REData *)calloc(1, sizeof(REData));
	
	data->w = w;
	data->h = h;
	data->bpr = bpr;
	data->spp = spp;
	
	data->imageData = imageData;
	
	data->pixels = (REPixel *) calloc(w * h, sizeof(REPixel));
	data->regionOrderedPixels = (REPixel *) calloc(w * h, sizeof(REPixel));
	
	data->grownRegions = CFSetCreateMutable(NULL, 0, NULL);
	
	data->allocatedRegions = CFSetCreateMutable(NULL, 0, NULL);
	data->allocatedClusters = CFSetCreateMutable(NULL, 0, NULL);
	
	return data;
}


@end
