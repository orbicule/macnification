//
//  MigrationPolicy_v10_to_v18_Stack.m
//  Filament
//
//  Created by Dennis Lorson on 20/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MigrationPolicies_v10_to_v18.h"


@implementation MigrationPolicy_v10_to_v18_Stack

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{	
	return [super createDestinationInstancesForSourceInstance:sInstance entityMapping:mapping manager:manager error:error];
}

- (BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{	
	return [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];
}


@end


#pragma mark -
// -----------------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------------


@implementation MigrationPolicy_v10_to_v18_StackImage

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{	
	return [super createDestinationInstancesForSourceInstance:sInstance entityMapping:mapping manager:manager error:error];
}

- (BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
	BOOL ok = [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];
	
	// create the stack relationship: only the first one is kept, the other ones are discarded.  this shouldn't cause any problems: no stackimage has ever belonged to more than one stack
	
	// get the old stackImage
	NSArray *array = [manager sourceInstancesForEntityMappingNamed:[mapping name] destinationInstances:[NSArray arrayWithObject:dInstance]];
	if ([array count] != 1)
		return NO;
	NSManagedObject *oldStackImage = [array objectAtIndex:0];
	
	// get its stacks
	NSArray *oldStacks = [[oldStackImage valueForKey:@"stacks"] allObjects];
	if ([oldStacks count] == 0)
		return ok;
	
	// separate the stack to be retained from the rest
	// don't care about the other ones (which shouldn't even exist); they will only lose some images
	NSArray *oldRetained = [oldStacks subarrayWithRange:NSMakeRange(0, 1)];

	// get the corresponding objects from the new store
	NSArray *newRetained = [manager destinationInstancesForEntityMappingNamed:@"StackToStack" sourceInstances:oldRetained];
	
	if ([newRetained count] == 0)
		return NO;
	
	[dInstance setValue:[newRetained objectAtIndex:0] forKey:@"stack"];
	
	return ok;

}


@end


#pragma mark -
// -----------------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------------


@implementation MigrationPolicy_v10_to_v18_Image

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{	
	return [super createDestinationInstancesForSourceInstance:sInstance entityMapping:mapping manager:manager error:error];
}

- (BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
	BOOL ok = [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];

	// create the stackImage relationship: only the first one is kept, the other ones are discarded.  In almost all cases, this shouldn't cause any problems
	// The worst that could happen is for a couple of images to be removed from a stack, but this stack is easily recreated.
	

	// get the image
	NSArray *array = [manager sourceInstancesForEntityMappingNamed:[mapping name] destinationInstances:[NSArray arrayWithObject:dInstance]];
	if ([array count] != 1)
		return NO;
	NSManagedObject *oldImage = [array objectAtIndex:0];
	
	// get its stackImages
	NSArray *oldStackImages = [[oldImage valueForKey:@"stackImages"] allObjects];
	if ([oldStackImages count] == 0)
		return ok;
	
	//NSLog(@"Image has old stackImages: %i", [oldStackImages count]);

	// separate the stackImage to be retained from the rest
	NSArray *oldRetained = [oldStackImages subarrayWithRange:NSMakeRange(0, 1)];
	NSArray *oldNonRetained = [NSArray array];
	if ([oldStackImages count] > 1) {
		NSRange range = NSMakeRange(1, [oldStackImages count] - 1);
		oldNonRetained = [oldStackImages subarrayWithRange:range];
	}
	
	//NSLog(@"Image has old retained stackImages: %i", [oldRetained count]);

	// get the corresponding objects from the new store
	NSArray *newRetained = [manager destinationInstancesForEntityMappingNamed:@"StackImageToStackImage" sourceInstances:oldRetained];
	NSArray *newNonRetained = [manager destinationInstancesForEntityMappingNamed:@"StackImageToStackImage" sourceInstances:oldNonRetained];
	
	//NSLog(@"Image has new retained stackImages: %i", [newRetained count]);

	if ([newRetained count] == 0)
		return NO;
	
	//NSLog(@"Retaining 1 image, discarding %i", [newNonRetained count]);
	
	// reestablish the relationship with the retained object
	[dInstance setValue:[newRetained objectAtIndex:0] forKey:@"stackImage"];
	
	// remove all others.
	for (NSManagedObject *obj in newNonRetained)
		[[obj managedObjectContext] deleteObject:obj];
	
	
	return ok;
}


@end
