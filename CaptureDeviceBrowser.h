//
//  CaptureDeviceBrowser.h
//  Filament
//
//  Created by Dennis Lorson on 02/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "CaptureDeviceBrowserModule.h"

@class CaptureDeviceBrowser;
@class CaptureDevice;

@protocol CaptureDeviceBrowserDelegate <NSObject>

- (void)captureDeviceBrowser:(CaptureDeviceBrowser *)browser didAddDevice:(CaptureDevice *)device;
- (void)captureDeviceBrowser:(CaptureDeviceBrowser *)browser didRemoveDevice:(CaptureDevice *)device;

@end


@interface CaptureDeviceBrowser : NSObject <CaptureDeviceBrowserModuleDelegate>
{
	id <CaptureDeviceBrowserDelegate> delegate_;

@private
	
	NSMutableArray *devices_;
	NSMutableArray *modules_;
	
	NSMutableArray *duplicateDevices_;
	NSMutableArray *delayedDevices_;			// to keep the delayed devices.  Otherwise, we can't track if they've been removed in the mean time.

}

@property (readwrite, retain) id <CaptureDeviceBrowserDelegate> delegate;

+ (CaptureDeviceBrowser *)sharedBrowser;

- (void)stopAllSessions;

- (void)stopSearchingForDevices;
- (void)startSearchingForDevices;

- (NSArray *)devices;

@end
