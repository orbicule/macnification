//
//  AdjustmentsController.m
//  Filament
//
//  Created by Peter Schols on 27/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "AdjustmentsController.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"


@implementation AdjustmentsController

#pragma mark Housekeeping

- (id)init 
{
    self = [super initWithWindowNibName:@"Filters"];
	delayedThumbnailUpdateTimer = nil;
    return self;
}


- (void)dealloc;
{
	[imageArrayController removeObserver:self forKeyPath:@"selection"];
	[imageFilterSet release];
	[changedFiltersSet release];
	[availableLUTs release];
	
	[super dealloc];
}



- (void)awakeFromNib;
{
	// load the available luts
	availableLUTs = [[self availableLUTs] retain];
	[self setLUTPopUpContent:availableLUTs];
	
	// Prepare the array of CIFilters and modified CIFilters
	imageFilterSet = [[NSMutableSet setWithCapacity:5] retain];
	changedFiltersSet = [[NSMutableSet setWithCapacity:5] retain];
	
	// Initialise our local imageArrayController variable
	imageArrayController = [MGLibraryControllerInstance imageArrayController];
	

}


- (void)delayedUpdateThumbnailForImage:(Image *)anImg
{
	
	if (delayedThumbnailUpdateTimer) {
		
		[delayedThumbnailUpdateTimer invalidate];
		[delayedThumbnailUpdateTimer release];
		delayedThumbnailUpdateTimer = nil;
		
	}
	
	
	delayedThumbnailUpdateTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1
																	target:self selector:@selector(delayedUpdateThumbnailFire:) 
																  userInfo:anImg 
																   repeats:NO] retain];
	

	
}
									

- (void)delayedUpdateThumbnailFire:(NSTimer*)theTimer
{
	[imageArrayController updateThumbnailForImage:[theTimer userInfo]];
	[delayedThumbnailUpdateTimer release];
	delayedThumbnailUpdateTimer = nil;
	
}

									   

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
{
    
	// The user is changing the filter settings
	if ([keyPath isEqualTo:@"outputImage"]) {
		// Mark our image as dirty since the user has changed filter settings and refresh our imageView so we see the updates
		imageIsDirty = YES;
		// Add the filter to the changedFiltersArray
		[changedFiltersSet addObject:object];
		[self saveFilters];
		[imageArrayController filtersDidChange];
		
		[self delayedUpdateThumbnailForImage:image];

	}
    
    
	
	// The user is changing the selected image 
	else {
        
        
		// do we have a single selection?
		if ([[object selectedObjects] count] == 1) {
			
			[applyToStackButton setHidden:![[[imageArrayController selectedObjects] objectAtIndex:0] isPartOfStack]];
			
			// If the filters for this image have been changed, save the image
			if (imageIsDirty) { 
				[self saveFilters];
				[imageArrayController updateThumbnailForImage:image];
			}

			imageIsDirty = NO;

			[self loadFiltersFromImage];
			
		}
	}
}


#pragma mark -
#pragma mark Working Filterset modification


- (void)loadFiltersFromImage;
{
	// Remove all existing filters
	[self removeAllFilters];
	
	// Get the selected image
	image = [[imageArrayController selectedObjects] objectAtIndex:0];
	
	// Get the new filters for the selected image and store them in the filtersArray
	[imageFilterSet addObjectsFromArray:[image ciFilters]];
		
	CIFilter *currentFilter;
    
	
	// Iterate all fixed filters and check if the are present in the filtersArray
	for (NSString *filterName in [self fixedFilters]) {
		if ((currentFilter = [self filterInWorkingSetWithName:filterName])) {
			// Add the filter to the changedFiltersArray
			[changedFiltersSet addObject:currentFilter];
		}
		else {
			// If it's not already there, create a new filter
			currentFilter = [CIFilter filterWithName:filterName];
			
			// Initialise the filter with the default values
			[self setIdentitiesForFilter:currentFilter];
			
			// Add it to the filtersArray
			[imageFilterSet addObject:currentFilter];
		}
		// We supply the filter with a dummy image
		[currentFilter setValue:[CIImage emptyImage] forKey:@"inputImage"];
		
		// We bind the UI to the CIFilter
		[self bindFilter:currentFilter toUIWithName:filterName];

		[currentFilter addObserver:self forKeyPath:@"outputImage" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	}

	
}


- (void)setIdentitiesForFilter:(CIFilter *)filter
{
	// first, set the defaults for all properties in case some of the properties have no identity.
	[filter setDefaults];
	
	NSDictionary *attrs = [filter attributes];
	
	for (NSString *attr in [attrs allKeys]) {
		
		if ([attr rangeOfString:@"input"].location != NSNotFound) {
			
			NSDictionary *attributeProps = [attrs objectForKey:attr];
			
			if ([attributeProps objectForKey:kCIAttributeIdentity]) {
				[filter setValue:[attributeProps objectForKey:kCIAttributeIdentity] forKey:attr];
				//NSLog(@"set identity for prop %@ from filter %@ to %@", attr, [filter description], [[attributeProps objectForKey:kCIAttributeIdentity] description]);
			}
			
		}
		
	}
}




- (void)removeAllFilters;
{
    ;

	
	for (CIFilter *filter in imageFilterSet) {
		@try {
			[filter removeObserver:self forKeyPath:@"outputImage"];
		} @catch (NSException *e) {
			NSLog(@"Could not remove observer for filter:%@\n      (Exception: %@)", [filter description], [e description]);
		}
		[self unbindFilter:filter];
	}
	
	// Remove all filters from the both the static and changed filter set
	[imageFilterSet removeAllObjects];
	[changedFiltersSet removeAllObjects];
}



- (void)saveFilters;
{
	//NSLog(@"Saving filters:\n%@", changedFiltersSet);
	[image setCIFilters:[changedFiltersSet allObjects]];
}


#pragma mark -


- (void)bindFilter:(CIFilter *)filter toUIWithName:(NSString *)filterName;
{
	if ([filterName isEqualTo:@"CIExposureAdjust"]) {
		[exposureSlider bind:@"value" toObject:filter withKeyPath:@"inputEV" options:nil];
	}
	else if ([filterName isEqualTo:@"CIColorControls"]) {
		[brightnessSlider bind:@"value" toObject:filter withKeyPath:@"inputBrightness" options:nil];
		[contrastSlider bind:@"value" toObject:filter withKeyPath:@"inputContrast" options:nil];
		[saturationSlider bind:@"value" toObject:filter withKeyPath:@"inputSaturation" options:nil];
	}
	else if ([filterName isEqualTo:@"CISharpenLuminance"]) {
		[sharpnessSlider bind:@"value" toObject:filter withKeyPath:@"inputSharpness" options:nil];
	}	
	else if ([filterName isEqualTo:@"DODGaussianBlurFilter"]) {
		//NSLog(@"bind filter  %@ to blurslider %@", [filter description], [blurSlider description]);
		//[filter setValue:[NSNumber numberWithInt:0] forKey:@"inputRadius"];
		[blurSlider bind:@"value" toObject:filter withKeyPath:@"inputRadius" options:nil];
	}	
	else if ([filterName isEqualTo:@"WhiteBalanceCorrectionFilter"]) {
		[whiteBalanceColorWell setColor:[NSColor colorWithCIColor:[filter valueForKey:@"inputColor"]]];
	}
	else if ([filterName isEqualTo:@"InvertFilter"]) {
		[invertCheckbox bind:@"value" toObject:filter withKeyPath:@"inputEnabled" options:nil];
	}
	else if ([filterName isEqualTo:@"LUTColormapFilter"]) {
		
		[self updateLUTSelectionForPath:[filter valueForKey:@"inputLUTPath"]];
		
	}
    
    ;

}

- (void)unbindFilter:(CIFilter *)filter
{
	NSString *filterName = [filter className];
	
	if ([filterName isEqualTo:@"CIExposureAdjust"]) {
		[exposureSlider unbind:@"value"];
	}
	else if ([filterName isEqualTo:@"CIColorControls"]) {
		[brightnessSlider unbind:@"value"];
		[contrastSlider unbind:@"value"];
		[saturationSlider unbind:@"value"];
	}
	else if ([filterName isEqualTo:@"CISharpenLuminance"]) {
		[sharpnessSlider unbind:@"value"];
	}	
	else if ([filterName isEqualTo:@"DODGaussianBlurFilter"]) {

		[blurSlider unbind:@"value"];
	}	
	else if ([filterName isEqualTo:@"WhiteBalanceCorrectionFilter"]) {

	}
	else if ([filterName isEqualTo:@"InvertFilter"]) {
		[invertCheckbox unbind:@"value"];
	}
	else if ([filterName isEqualTo:@"LUTColormapFilter"]) {
		
		[lutPopupButton selectItemAtIndex:0];
		
	}
	
	
	
}



- (CIFilter *)filterInWorkingSetWithName:(NSString *)filterName;
{
	for (CIFilter *filter in imageFilterSet)
		if ([[filter className] isEqualTo:filterName])
			return filter;
			
	return nil;
}



- (NSArray *)fixedFilters;
{
	return [NSArray arrayWithObjects:@"CIExposureAdjust", @"CIColorControls", @"CISharpenLuminance", @"DODGaussianBlurFilter", @"WhiteBalanceCorrectionFilter", /*@"InvertFilter",*/ @"LUTColormapFilter", nil];
}	




#pragma mark -
#pragma mark Filter batch ops



- (IBAction)resetAllFilters:(id)sender;
{
	// Unregister as an observer for the current filters
	[self removeAllFilters];
	
	// Remove all filters from the data model
	[imageArrayController removeAllFilters:self];
    
    ;

	
	// Adjust our interface
	[self loadFiltersFromImage];
	
	// Let the imageArrayController know about the change so it can update the PSImageView
	[imageArrayController filtersDidChange];	
}


- (IBAction)copyAllFilters:(id)sender;
{
	[imageArrayController copyFilters:self];	
}

- (IBAction)applyFiltersToStack:(id)sender;
{
	[imageArrayController applyFiltersToEntireStack:sender];
}


- (IBAction)pasteAllFilters:(id)sender;
{
	// Unregister as an observer for the current filters
	[self removeAllFilters];
	
	// Duplicate the filters in the CD model
	[imageArrayController pasteFilters:self];	
	
	// Adjust our interface
	[self loadFiltersFromImage];
    
    ;

	
	// Let the imageArrayController know about the change so it can update the PSImageView
	[imageArrayController filtersDidChange];	
}


- (IBAction)applyFiltersToEntireStack:(id)sender;
{
	[imageArrayController applyFiltersToEntireStack:sender];
}



#pragma mark -
#pragma mark Color picker



- (IBAction)colorWellChanged:(id)sender;
{
    // Get the NSColor from the color well and convert it to a CIColor
    CIColor *color = [[CIColor alloc] initWithColor:[whiteBalanceColorWell color]];
	
	// Set this CIColor for the filter
	CIFilter *whiteBalanceFilter = [self filterInWorkingSetWithName:@"WhiteBalanceCorrectionFilter"];
	[whiteBalanceFilter setValue:color forKey:@"inputColor"];
	[color release];
	
	// Add the WhiteBalanceCorrectionFilter to the changedFiltersArray
	[changedFiltersSet addObject:whiteBalanceFilter];
	
	// Update the filters and the view
	[self saveFilters];
	[imageArrayController filtersDidChange];
}



- (void)userPickedNewWhiteBalanceCorrectionColor:(NSNotification *)note;
{
	[self setColorForWhiteBalanceCorrectionFilter:[note object]];
	
	// Set the colorPicker button to unselected
	[colorPickerButton setState:NSOffState];
	
	// Reset the cursor
	[[NSCursor arrowCursor] set];
}



- (IBAction)pickColorForWhiteBalanceCorrection:(id)sender;
{
	// Set the current white balance correction filter to use pure white
	[self setColorForWhiteBalanceCorrectionFilter:[NSColor whiteColor]];
	
	// Notify the PSImageView that the user is about to pick a new color
	[(NSCursor *)[[NSCursor alloc] initWithImage:[NSImage imageNamed:@"targetCursor"] hotSpot:NSMakePoint(11, 11)] set];	
	NSNotification *enableColorPickerMode = [NSNotification notificationWithName:@"enableColorPickerMode" object:self userInfo:NULL];
	[[NSNotificationCenter defaultCenter] performSelector:@selector(postNotification:) withObject:enableColorPickerMode afterDelay:0.2];
	//[[NSNotificationCenter defaultCenter] postNotification:enableColorPickerMode];
}



- (void)setColorForWhiteBalanceCorrectionFilter:(NSColor *)aColor;
{
	// Get the NSColor from the color well and convert it to a CIColor
	
    CIColor *color = [[[CIColor alloc] initWithColor:aColor] autorelease];
		
	// Set this CIColor for the filter
	CIFilter *whiteBalanceFilter = [self filterInWorkingSetWithName:@"WhiteBalanceCorrectionFilter"];
	[whiteBalanceFilter setValue:color forKey:@"inputColor"];
	
	// Add the WhiteBalanceCorrectionFilter to the changedFiltersArray
	[changedFiltersSet addObject:whiteBalanceFilter];
	
	// Update the filters and the view
	[self saveFilters];
	[imageArrayController filtersDidChange];	
}




#pragma mark -
#pragma mark Window management



// Update the browsers if the user exits full screen mode after applying filters to an image
// This method is called by the FullScreenController notification that we have registered for
- (void)willExitFullScreenMode;
{
	if (imageIsDirty) {
		[imageArrayController performSelector:@selector(updateThumbnailForImage:) withObject:image afterDelay:0.3];
	}
}




- (void)windowWillClose:(NSNotification *)not
{
	if (imageIsDirty) { 
		[self saveFilters];
		[imageArrayController updateThumbnailForImage:image];
	}
	
	imageIsDirty = NO;
	
	
	[self removeAllFilters];
	
	if (isObservingController) {
		//NSLog(@"remove adjustmentsController observer");
		
		// remove ourselves as observer
		[imageArrayController removeObserver:self forKeyPath:@"selection"];
		//[[NSNotificationCenter defaultCenter] removeObserver:self];
		
		isObservingController = NO;
	}
	
	
	
}


- (void)close
{
	if (imageIsDirty) { 
		[self saveFilters];
		[imageArrayController updateThumbnailForImage:image];
	}
	
	imageIsDirty = NO;
	
	
	[self removeAllFilters];
	
	if (isObservingController) {
		//NSLog(@"remove adjustmentsController observer (close)");
		
		// remove ourselves as observer
		[imageArrayController removeObserver:self forKeyPath:@"selection"];
		
		//[[NSNotificationCenter defaultCenter] removeObserver:self];
		
		isObservingController = NO;
	}
	
	[super close];
}


- (IBAction)showWindow:(id)sender
{
	
	// Register as an observer of the selected image
	if (!isObservingController) {
		[imageArrayController addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial) context:NULL];
		isObservingController = YES;
	}
	
	// Read the filters for the initial image -- removed: is done by KVOInitial
	//[self loadFiltersFromImage];
	
	// Watch for new colors coming in from the PSImageView (when using the White Balance Color Picker)
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userPickedNewWhiteBalanceCorrectionColor:) name:@"pickedNewWhiteBalanceCorrectionColor" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willExitFullScreenMode) name:@"willExitFullScreenMode" object:nil];
	
	
	[super showWindow:self];
}




- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem
{

	if ([anItem action] == @selector(applyFiltersToEntireStack:)) {

		return ([[imageArrayController selectedObjects] count] == 1 && [[[imageArrayController selectedObjects] objectAtIndex:0] isPartOfStack]);
		
	} else {
		
		return YES;
	}

}

#pragma mark -
#pragma mark LUT functionality


- (void)updateLUTSelectionForPath:(NSString *)path
{
	if (!path) {
		[lutPopupButton selectItemAtIndex:0];
		return;
	}
	
	NSString *title = [self lutTitleForPath:path];
	
	for (NSMenuItem *item in [lutPopupButton itemArray])
		if ([[item title] isEqualToString:title])
			[lutPopupButton selectItem:item];

}

- (NSString *)lutTitleForPath:(NSString *)path
{
	NSString *title = [path lastPathComponent];
	title = [title stringByDeletingPathExtension];
	title = [title capitalizedString];
	return [title stringByReplacingOccurrencesOfString:@"_" withString:@" "];
}

- (void)setLUTPopUpContent:(NSArray *)array
{	
	[lutPopupButton setAutoenablesItems:NO];
	
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	[menu setAutoenablesItems:NO];
	
	// "None" item
	NSMenuItem *item = [[[NSMenuItem alloc] init] autorelease];
	[item setTitle:@"None"];
	[item setEnabled:YES];
	[item setTag:-1];
	[item setTarget:self];
	[item setAction:@selector(changeLUT:)];
	[menu addItem:item];
	
	
	
	for (NSDictionary *category in array) {
		
		// separator
		NSMenuItem *sep = [NSMenuItem separatorItem];
		[sep setTag:-2];
		[menu addItem:sep];
		
		// title
		item = [[[NSMenuItem alloc] init] autorelease];
		[item setTitle:[category objectForKey:@"name"]];
		[item setEnabled:NO];
		[item setTag:-2];
		[menu addItem:item];
		
		// items
		for (NSString *lutPath in [category objectForKey:@"paths"]) {
			
			NSString *title = [self lutTitleForPath:lutPath];
			
			NSMenuItem *item = [[[NSMenuItem alloc] init] autorelease];
			[item setTag:[array indexOfObject:lutPath]];
			[item setTitle:title];
			[item setTarget:self];
			[item setAction:@selector(changeLUT:)];
			[item setRepresentedObject:lutPath];
			[menu addItem:item];
			
		}
	}
	
	
	[lutPopupButton setMenu:menu];
}

- (void)changeLUT:(id)sender
{
	NSString *lutPath = [[lutPopupButton selectedItem] representedObject];
		
		
	if (lutPath) {
		CIFilter *lutFilter = [self filterInWorkingSetWithName:@"LUTColormapFilter"];
		[lutFilter setValue:lutPath forKey:@"inputLUTPath"];
		[changedFiltersSet addObject:lutFilter];		
	}	
	else {
		[changedFiltersSet removeObject:[self filterInWorkingSetWithName:@"LUTColormapFilter"]];
	}
	
	// Update the filters and the view
	[self saveFilters];
	[imageArrayController filtersDidChange];
	
	
}


- (NSArray *)availableLUTs
{
	NSString *builtInDir = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/LUT/"];
	
	NSArray *subdirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:builtInDir error:nil];
	
	NSMutableArray *categories = [NSMutableArray array];
	
	for (NSString *subdir in subdirs) {
		
		NSString *path = [builtInDir stringByAppendingPathComponent:subdir];
		BOOL isDir = NO;
		[[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
		
		if (!isDir)
			continue;
		
		NSArray *lutNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
		
		if ([lutNames count] == 0)
			continue;
		
		NSMutableArray *lutPaths = [NSMutableArray array];
		
		for (NSString *lutName in lutNames)
			[lutPaths addObject:[path stringByAppendingPathComponent:lutName]];
		
		[categories addObject:[NSDictionary dictionaryWithObjectsAndKeys:subdir, @"name", lutPaths, @"paths", nil]];
		
	}
	
	
	
	NSString *customPath = [NSHomeDirectory() stringByAppendingString:@"/Library/Application Support/Macnification/LUT/"];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:customPath isDirectory:NULL]) {
		
		NSArray *customLUTs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:customPath error:nil];
		
		NSMutableArray *customLUTPaths = [NSMutableArray array];

		for (NSString *file in customLUTs) {
			
			if ([[file pathExtension] isEqualToString:@"lut"])
				[customLUTPaths addObject:[customPath stringByAppendingString:file]];

		}
		
		if ([customLUTPaths count] > 0)
			[categories addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Custom", @"name", customLUTPaths, @"paths", nil]];
		
	}

	
	return categories;
}


@end
