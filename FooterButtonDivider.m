//
//  FooterButtonDivider.m
//  Filament
//
//  Created by Dennis Lorson on 11/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "FooterButtonDivider.h"


@implementation FooterButtonDivider

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    // Drawing code here.
	
	
	
	// SIDES
	
	NSBezierPath *sidesDark = [NSBezierPath bezierPath];
	[sidesDark moveToPoint:NSMakePoint(NSMinX([self bounds]), NSMinY([self bounds]))];
	[sidesDark lineToPoint:NSMakePoint(NSMinX([self bounds]), NSMaxY([self bounds]))];
	
	NSBezierPath *sidesLight = [NSBezierPath bezierPath];
	[sidesLight moveToPoint:NSMakePoint(NSMinX([self bounds]) + 1, NSMinY([self bounds]))];
	[sidesLight lineToPoint:NSMakePoint(NSMinX([self bounds]) + 1, NSMaxY([self bounds]))];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	[[NSColor colorWithCalibratedWhite:102.0/255.0 alpha:1.0] set];
	[sidesLight stroke];

	[[NSColor colorWithCalibratedWhite:1.0 alpha:60.0/255.0] set];
	[sidesDark stroke];

	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
	
	
	
}

@end
