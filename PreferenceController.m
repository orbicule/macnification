//
//  PreferenceController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 4 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import "PreferenceController.h"
#import "MGAppDelegate.h"

@interface PreferenceController ()

- (NSArray *)toolbarSelectableItemIdentifiers: (NSToolbar *)aToolbar;
- (void)_updateLibraryPanelUI:(NSOpenPanel *)panel;
- (NSString *)_existingLibraryInFolderAtPath:(NSString *)path;
- (void)_changeLibraryPathContinue:(NSOpenPanel *)panel;

@end


@implementation PreferenceController

- (id)init 
{
    self = [super initWithWindowNibName:@"Preferences"];
    return self;
}



- (void)awakeFromNib;
{
	[self updateExternalEditorButton];
	[self updateExternalAnalyzerButton];
	[self updateSpreadsheetAppButton];
	[self switchToPreferencePane:8];
	[toolbar setSelectedItemIdentifier:[[self toolbarSelectableItemIdentifiers:toolbar]objectAtIndex:0]];
}



- (IBAction)chooseApplication:(id)sender;
{
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	[panel setCanChooseDirectories:NO];
	[panel setAllowsMultipleSelection:NO];
	[panel setPrompt:@"Select Application"];
	[panel beginSheetForDirectory:@"/Applications" file:nil types:nil modalForWindow:[self window] modalDelegate:self didEndSelector:@selector(openPanelDidEnd:returnCode:contextInfo:) contextInfo:sender];
}


- (void)openPanelDidEnd:(NSOpenPanel *)openPanel returnCode:(int)returnCode contextInfo:(id)contextInfo
{
	if (returnCode == NSOKButton) {
		NSString *path = [openPanel filename];
		[openPanel close];
		//NSLog(@"%d", [contextInfo tag]);
		if ([contextInfo tag] == 1) {
			[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:path forKey:@"externalEditorPath"];
			[self updateExternalEditorButton];
		}
		else if ([contextInfo tag] == 2) {
			[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:path forKey:@"externalAnalyzerPath"];
			[self updateExternalAnalyzerButton];
		}
		else if ([contextInfo tag] == 5) {
			[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:path forKey:@"spreadsheetApplicationPath"];
			[self updateSpreadsheetAppButton];
		}
	}
}



- (void)updateExternalEditorButton;
{
	NSString *fullPath = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"externalEditorPath"];
	NSString *applicationName = [[fullPath lastPathComponent]stringByDeletingPathExtension];
	NSImage *iconForExternalEditor = fullPath ? [[NSWorkspace sharedWorkspace] iconForFile:fullPath] : nil;
	[externalEditorField setStringValue:applicationName];
	[externalEditorButton setImage:iconForExternalEditor];
}



- (void)updateExternalAnalyzerButton;
{
	NSString *fullPath = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"externalAnalyzerPath"];
	NSString *applicationName = [[fullPath lastPathComponent]stringByDeletingPathExtension];
	NSImage *iconForExternalAnalyzer =  fullPath ? [[NSWorkspace sharedWorkspace] iconForFile:fullPath] : nil;
	[externalAnalyzerField setStringValue:applicationName];
	[externalAnalyzerButton setImage:iconForExternalAnalyzer];
}



- (void)updateSpreadsheetAppButton;
{
	NSString *fullPath = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"spreadsheetApplicationPath"];
	
	if (fullPath) {
		
		NSString *applicationName = [[fullPath lastPathComponent]stringByDeletingPathExtension];
		NSImage *iconForExternalEditor =  fullPath ? [[NSWorkspace sharedWorkspace] iconForFile:fullPath] : nil;
		[spreadsheetApplicationField setStringValue:applicationName];
		[spreadsheetApplicationButton setImage:iconForExternalEditor];
		
	}
	
	// Let our observers know that we changed our spreadsheet application (the ROI HUD needs this)
	NSNotification *note = [NSNotification notificationWithName:@"spreadsheetApplicationDidChange" object:self userInfo:NULL];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}


- (IBAction)restoreWarningsAndDialogs:(id)sender
{
	NSArray *warningNames = [NSArray arrayWithObjects:
							 @"suppressRemoveCollectionWarning", 
							 @"suppressRemoveImagesWarning", 
							 @"suppressDuplicateStackImageWarning",
							 @"suppressUnstackImagesWarning", 
							 @"suppressStackImagesWarning", 
							 nil];
	
	for (NSString *key in warningNames) {
		
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:key];
		
	}
	
	
}


- (IBAction)changeLibraryPath:(id)sender;
{
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    
    [openPanel setCanChooseDirectories:YES];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setAllowedFileTypes:[NSArray arrayWithObject:@"mnlibrary"]];
    
    NSString *currentPath = [[[MGLibrary prefsLibraryPath] stringByExpandingTildeInPath] stringByDeletingLastPathComponent];
    
    [openPanel setDirectoryURL:[NSURL fileURLWithPath:currentPath isDirectory:NO]];
    [openPanel setDelegate:self];
    
    [openPanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result) {
        
        
        if (result == NSFileHandlingPanelOKButton)
            [self _changeLibraryPathContinue:openPanel];
        
    }];
}

- (void)panel:(id)sender didChangeToDirectoryURL:(NSURL *)url
{
    [self _updateLibraryPanelUI:(NSOpenPanel *)sender];
}

- (void)panelSelectionDidChange:(id)sender
{
    [self _updateLibraryPanelUI:(NSOpenPanel *)sender];

}

- (void)_updateLibraryPanelUI:(NSOpenPanel *)panel
{
    NSString *currentPath = [MGLibrary prefsLibraryPath];

    
    NSString *dir = [[panel directoryURL] path];
    NSString *selectedFile = nil;
    if ([[panel URLs] count])
        selectedFile = [[[panel URLs] objectAtIndex:0] path];
    
    if ([[selectedFile pathExtension] isEqual:@"mnlibrary"]) {
        
        BOOL currentlyInUse = [[selectedFile stringByAbbreviatingWithTildeInPath] isEqual:currentPath];
        
        if (currentlyInUse) {
            [panel setPrompt:@"Select"];
            [panel setMessage:[NSString stringWithFormat:@"Choose \"Select\" to continue using the library named \"%@\"", [[selectedFile lastPathComponent] stringByDeletingPathExtension]]];
        }
        else {
            [panel setPrompt:@"Select And Relaunch"];
            [panel setMessage:[NSString stringWithFormat:@"Choose \"Select And Relaunch\" to use the library named \"%@\"", [[selectedFile lastPathComponent] stringByDeletingPathExtension]]];
        }

    }
    
    else if (selectedFile && [self _existingLibraryInFolderAtPath:selectedFile]) {
        
        NSString *libraryPath = [self _existingLibraryInFolderAtPath:selectedFile];
        
        BOOL currentlyInUse = [[libraryPath stringByAbbreviatingWithTildeInPath] isEqual:currentPath];
        
        if (currentlyInUse) {
            [panel setPrompt:@"Select"];
            [panel setMessage:[NSString stringWithFormat:@"Choose \"Select\" to continue using the library named \"%@\"", [[libraryPath lastPathComponent] stringByDeletingPathExtension]]];
        }
        else {
            [panel setPrompt:@"Select And Relaunch"];
            [panel setMessage:[NSString stringWithFormat:@"Choose \"Select And Relaunch\" to use the library named \"%@\"", [[libraryPath lastPathComponent] stringByDeletingPathExtension]]];
        }
        

    }
    
    else if (selectedFile) {
        
        [panel setPrompt:@"Create And Relaunch"];
        [panel setMessage:[NSString stringWithFormat:@"Choose \"Create And Relaunch\" to create a new library named \"Macnification Library\" in the folder \"%@\"", [selectedFile lastPathComponent]]];
        
    }
    
    else {
        
        [panel setPrompt:@"Create And Relaunch"];
        [panel setMessage:[NSString stringWithFormat:@"Choose \"Create And Relaunch\" to create a new library named \"Macnification Library\" in the folder \"%@\"", [dir lastPathComponent]]];
        
    }
    
 
}

- (void)_changeLibraryPathContinue:(NSOpenPanel *)panel;
{
    NSString *dir = [[panel directoryURL] path];
    NSString *selectedFile = nil;
    if ([[panel URLs] count])
        selectedFile = [[[panel URLs] objectAtIndex:0] path];
    
    NSString *bundlePath = nil;
    
    if ([[selectedFile pathExtension] isEqual:@"mnlibrary"])
        bundlePath = selectedFile;
    else if (selectedFile && [self _existingLibraryInFolderAtPath:selectedFile])
        bundlePath = [self _existingLibraryInFolderAtPath:selectedFile];
    else if (selectedFile)
        bundlePath = [selectedFile stringByAppendingPathComponent:@"Macnification Library.mnlibrary"];
    else
        bundlePath = [dir stringByAppendingPathComponent:@"Macnification Library.mnlibrary"];

      
    bundlePath = [bundlePath stringByAbbreviatingWithTildeInPath];
    
    if ([bundlePath isEqual:[MGLibrary prefsLibraryPath]])
        return;
    
    
    [MGAppDelegateInstance prepareForRestart];
    
    
    [MGLibrary setPrefsLibraryPath:bundlePath];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createNewLibraryOnStartup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [MGAppDelegateInstance restart];
    
}


- (NSString *)_existingLibraryInFolderAtPath:(NSString *)path
{
    NSString *expanded = [path stringByExpandingTildeInPath];
    
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:expanded error:nil];
    
    for (NSString *file in contents)
        if ([[file pathExtension] isEqual:@"mnlibrary"])
            return [expanded stringByAppendingPathComponent:file];
    
    return nil;
}


- (IBAction)switchToPane:(id)sender;
{
	[self switchToPreferencePane:[sender tag]];
}



- (void)switchToPreferencePaneAndSelectPreference:(NSUInteger)paneNumber;
{
	[self switchToPreferencePane:paneNumber - 1];
	//[toolbar setSelectedItemIdentifier:[[self toolbarSelectableItemIdentifiers:toolbar] objectAtIndex:paneNumber]];
	for (NSToolbarItem *item in [toolbar items]) {
		
		if ([item tag] == paneNumber - 1) {
			[toolbar setSelectedItemIdentifier:[item itemIdentifier]];
			return;
		}
	}
}


- (void)switchToPreferencePane:(NSUInteger)paneNumber;
{
	NSView *newView;
	switch (paneNumber) {
		case 0:
		default:
			newView = view1;
			break;
		case 1:
			newView = view2;
			break;
		case 2:
			newView = view3;
			break;
		case 3:
			newView = view4;
			break;
		case 4:
			newView = view5;
			break;
		case 5:
			newView = view6;
			break;
		case 6:
			newView = view7;
			break;	
		case 7:
			newView = view8;
			break;
		case 8:
			newView = view9;
			break;	
	}
	[[self window] setContentView:emptyView];
	NSRect currentWindowRect = [[self window] frame];
	NSRect newWindowRect = [NSWindow frameRectForContentRect:(NSRect)[newView frame] styleMask:NSTitledWindowMask];
	[[self window] setFrame:NSMakeRect(currentWindowRect.origin.x, currentWindowRect.origin.y-(newWindowRect.size.height-currentWindowRect.size.height+56), 534, newWindowRect.size.height+56) display:YES animate:YES];
	[[self window] setContentView:newView];
}



// Delegate method for NSToolbar
- (NSArray *)toolbarSelectableItemIdentifiers: (NSToolbar *)aToolbar;
{	
	NSMutableArray *allItemIdentifiers = [NSMutableArray arrayWithCapacity:5];
	for (NSToolbarItem *item in [aToolbar items]) {
		[allItemIdentifiers addObject:[item itemIdentifier]];
	}
	return allItemIdentifiers;
}


@end
