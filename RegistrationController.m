//
//  RegistrationController.m
//  Filament
//
//  Created by Peter Schols on 31/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "RegistrationController.h"
#import <SystemConfiguration/SCNetwork.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@implementation RegistrationController


- (void)awakeFromNib;
{
	if ([self isRegistered]) {
		[registeredTextField setStringValue:@"Registered Copy"];
		[tabPanel selectLastTabViewItem:self];
	}
	else {
		[tabPanel selectFirstTabViewItem:self];
	}
}



- (IBAction)authorize:(id)sender;
{
	if ([self networkReachableWithoutAnythingSpecialHappening]) {
		NSString *urlString = [NSString stringWithFormat:@"http://www.macnification.com/registration/authorize.php?licensekey=%@", [licenseField stringValue]];
		NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSISOLatin1StringEncoding error:nil];
		if ([result hasPrefix:@"Successfully registered"]) {
			[self createRegistration];
			[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:[licenseField stringValue] forKey:@"licensekey"];
			[registeredTextField setStringValue:@"Registered Copy"];
			[tabPanel selectLastTabViewItem:self];
			// Should write the license key to the user defaults, so we can use it when deauthorizing
		}
		[unRegisteredTextField setStringValue:result];
	}
	else {
		[unRegisteredTextField setStringValue:@"Not connected to the Internet"];
	}
}



- (IBAction)deAuthorize:(id)sender;
{
	if ([self isRegistered]) {
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"Unregister This Mac"];
		[alert addButtonWithTitle:@"Cancel"];
		[alert setMessageText:@"Are you sure you want to unregister this Mac?"];
		[alert setInformativeText:@"This will reset registration information for this Mac and Macnification will revert to its unregistered state.\n\nSelect Preferences - Registration to re-register this Mac in the future."];
		[alert setAlertStyle:NSWarningAlertStyle];
		if ([alert runModal] == NSAlertFirstButtonReturn) {
			NSString *urlString = [NSString stringWithFormat:@"http://www.macnification.com/registration/deauthorize.php?licensekey=%@", 	[[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"licensekey"]];
			[self removeRegistration];
			[tabPanel selectFirstTabViewItem:self];
			[tabPanel display];
			[unRegisteredTextField setStringValue:@""];
            
			
			NSAlert *alert2 = [[NSAlert alloc] init];
			[alert2 addButtonWithTitle:@"OK"];
			[alert2 setMessageText:[[NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSISOLatin1StringEncoding error:nil]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			[alert2 setInformativeText:@"You can always re-register this Mac later by entering your license key."];
			[alert2 setAlertStyle:NSWarningAlertStyle];
			if ([alert2 runModal] == NSAlertFirstButtonReturn) {

			}
			[alert2 release];
		}
		[alert release];
	}
}



- (IBAction)lostLicenseKey:(id)sender;
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.macnification.com/support"]];
}



- (void)createRegistration;
{
	[[self processedEtid] writeToFile:[NSString stringWithFormat:@"/Users/Shared/.%1.0f", roundf(sqrt(64))] atomically:NO encoding:NSUTF8StringEncoding error:nil];
}



- (void)removeRegistration;
{
	NSFileManager *fm = [NSFileManager defaultManager];
	[fm removeItemAtPath:[NSString stringWithFormat:@"/Users/Shared/.%1.0f", roundf(sqrt(64))] error:nil];
}



- (BOOL)isRegistered;
{
	NSString *regInfo = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"/Users/Shared/.%1.0f", roundf(sqrt(64))] encoding:NSUTF8StringEncoding error:nil];
	if ([regInfo isEqualTo:[self processedEtid]]) {
		return YES;	
	}
	return NO;
}


- (BOOL)isRegisteredOrIsWithinTrialPeriod;
{
	if ([self isRegistered] || [self daysLeft] < 32) {
		return YES;	
	}
	return NO;
}



- (BOOL)networkReachableWithoutAnythingSpecialHappening;
{
	SCNetworkReachabilityRef target;
	SCNetworkConnectionFlags reachabilityStatus = 0;
	Boolean success = false;
	target = SCNetworkReachabilityCreateWithName(NULL, "www.macnification.com");
	Boolean ok = SCNetworkReachabilityGetFlags(target, &reachabilityStatus);
	CFRelease(target);
	
	if (ok)
		success = !(reachabilityStatus & kSCNetworkFlagsConnectionRequired) 
		&&  (reachabilityStatus & kSCNetworkFlagsReachable);
	
    
	return success;
}



- (NSString *)processedEtid;
{
	return [NSString stringWithFormat:@"8364712589%@97412563", [self etid]];	
}



- (NSString *)etid;
{
	NSString *interfacesPrefs =	@"/Library/Preferences/SystemConfiguration/NetworkInterfaces.plist";
	NSDictionary *interfaces = [NSDictionary dictionaryWithContentsOfFile:interfacesPrefs];
	NSString *macAddress = [[[[interfaces objectForKey:@"Interfaces"]objectAtIndex:0]objectForKey:@"IOMACAddress"] description];
	macAddress = [macAddress stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	NSMutableArray *array = (NSMutableArray *)[macAddress componentsSeparatedByString:@" "];
    macAddress = [array componentsJoinedByString:@""];
	return macAddress;
}



- (NSUInteger)daysLeft;
{
	NSFileManager *fm = [NSFileManager defaultManager];
		
	NSDate *libraryCreationDate = [[fm attributesOfItemAtPath:[@"~/Library/Application Support/Macnification/Macnification.db" stringByExpandingTildeInPath] error:nil] objectForKey:NSFileCreationDate];
	
	double timeIntervalInSeconds = [[NSDate date] timeIntervalSinceDate:libraryCreationDate];
	NSUInteger timeIntervalInDays = (NSUInteger)roundf(timeIntervalInSeconds / 86400.00);
	//return 31;
	return (30 - timeIntervalInDays);

}




@end
