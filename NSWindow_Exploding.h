//
//  PSExplodingWindow.h
//  Filament
//
//  Created by Peter Schols on 22/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSWindow (PSExplodingWindow)



- (void)explodeToFrame:(NSRect)frame;
- (void)implode;

@end
