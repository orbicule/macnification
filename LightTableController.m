//
//  LightTableController.m
//  Filament
//
//  Created by Peter Schols on 25/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "LightTableController.h"

#import "LightTableCenteringClipView.h"
#import "MGFilterAndSortMetadataController.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"

@interface LightTableController ()

- (void)_updateSearchFieldCategories;

@end



@implementation LightTableController



- (id)initWithSuperview:(NSView *)superview;
{
	self = [super init];
	if(self) {
		
		if([NSBundle loadNibNamed:@"LightTable" owner:self]) {
			
			[m_view setFrame:[superview frame]];
			[superview addSubview:m_view];
		
			
			[[MGCustomMetadataController sharedController] addObserver:self];
			[self _updateSearchFieldCategories];
		}
		else {

			[self autorelease];
			self = nil;
			[NSException raise:@"MyViewController-init-exception" format:@" - could not load nib from bundle"];
		}
	}
	return self;
}



-(void)awakeFromNib
{
	
	// ltScrollView is an instance variable wired in IB to the NSScrollView
	
	id docView = [[ltScrollView documentView] retain];
	id newClipView = [[LightTableCenteringClipView alloc] initWithFrame:[[ltScrollView contentView] frame]];
	[newClipView setBackgroundColor:[NSColor windowBackgroundColor]];
	[ltScrollView setContentView:(NSClipView *)newClipView];
	[ltScrollView setAutohidesScrollers:YES];
	[newClipView release];
	[ltScrollView setDocumentView:docView];
	[docView release];
	

	[toggleMDButton setTarget:MGLibraryControllerInstance];
	[toggleMDButton setAction:@selector(toggleMdSplitView:)];
	
	
	[ltImageArrayController addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionInitial context:nil];
}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == ltImageArrayController) {
		if ([[ltImageArrayController selectedObjects] count] > 0) {
			[[opacitySliderView superview] addSubview:opacitySliderView positioned:NSWindowAbove relativeTo:nil];
			[opacitySliderView setHidden:NO];
			[noSelectionView setHidden:YES];
		} else {
			[opacitySliderView setHidden:YES];
			[[noSelectionView superview] addSubview:noSelectionView positioned:NSWindowAbove relativeTo:nil];
			[noSelectionView setHidden:NO];
		}
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}



- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)controller;
{
	// refresh the search menu
	[self searchCurrentFolderWithPredicate:nil naturalLanguageString:@""];
	[self _updateSearchFieldCategories];
}


- (NSArrayController *)contentController;
{
	return ltImageArrayController;
	
	
}

#pragma mark -
#pragma mark Search


- (void)_updateSearchFieldCategories
{	
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] searchAttributeKeyPaths]) {
		
		NSMenuItem *item = nil;
		
		if ((id)keyPath != [NSNull null]) {
			
			item  = [[[NSMenuItem alloc] initWithTitle:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath]
												action:@selector(changeSearchCategory:) 
										 keyEquivalent:@""] autorelease];
			[item setTarget:self];
			[item setRepresentedObject:keyPath];
			
		}
		else {
			
			item = [NSMenuItem separatorItem];
			
		}
		
		[menu addItem:item];
		
	}
	
	[[ltSearchField cell] setSearchMenuTemplate:menu];
	
	[self changeSearchCategory:[menu itemAtIndex:0]];
	
}



- (void)changeSearchCategory:(id)sender
{
	NSMenuItem *item = (NSMenuItem *)sender;
	
	NSMenu *menuCopy = [[[item menu] copy] autorelease];
	
	for (NSMenuItem *otherItem in [menuCopy itemArray])
		[otherItem setState:NSOffState];		
	
	
	[[menuCopy itemAtIndex:[[item menu] indexOfItem:item]] setState:NSOnState];
	
	[[ltSearchField cell] setSearchMenuTemplate:menuCopy];
	
	[[ltSearchField cell] setPlaceholderString:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:[item representedObject]]];
	
}

- (IBAction)changeSearchString:(id)sender;
{
	NSString *string = [(NSSearchField *)sender stringValue];
	
	NSMenuItem *selectedItem = nil;
	for (NSMenuItem *item in [[[(NSSearchField *)sender cell] searchMenuTemplate] itemArray])
		if ([item state] == NSOnState)
			selectedItem = item;
	
	if (![string length] || !selectedItem)
		[ltImageArrayController setFilterPredicate:nil];
	
	else 
		[ltImageArrayController setFilterPredicate:[[MGFilterAndSortMetadataController sharedController] searchPredicateForAttributeKeyPath:(NSString *)[selectedItem representedObject] 
																																		prefix:@"image"
																																  searchString:string]];
	
	
}


- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;
{
	
	if (filterPredicate)	
		[self showSearchBarWithMessage:description];

	else {
				
		[self hideSearchBar];
		[ltSearchField setStringValue:@""];
		[ltSearchField resignFirstResponder];
	}
	
	
	[ltImageArrayController setFilterPredicate:filterPredicate];
	
}


- (void)showSearchBarWithMessage:(NSString *)message;
{
	
	[searchMessage setAlphaValue:0.0];
	[searchMessage setStringValue:[message stringByReplacingOccurrencesOfString:@"Search" withString:@"Searched"]];
	[doneButton setAlphaValue:0.0];
	[doneButton setHidden:NO];
	[[searchMessage animator] setAlphaValue:1.0];
	[[doneButton animator] setAlphaValue:1.0];

}



- (void)hideSearchBar;
{
	[[searchMessage animator] setAlphaValue:0.0];
	[[doneButton animator] setAlphaValue:0.0];
	[searchMessage setStringValue:@""];
	[doneButton setHidden:YES];
}

- (IBAction)clearFilterPredicate:(id)sender
{
	[self removeFilterPredicate];
	
}

- (void)removeFilterPredicate
{
	[ltImageArrayController setFilterPredicate:nil];
	[self hideSearchBar];
	
}



@end
