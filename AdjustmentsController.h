//
//  AdjustmentsController.h
//  Filament
//
//  Created by Peter Schols on 27/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>
#import "Image.h"
#import "ImageArrayController.h"



@interface AdjustmentsController : NSWindowController {

	BOOL imageIsDirty;
	BOOL isObservingController;
	
	NSMutableSet *imageFilterSet;
	NSMutableSet *changedFiltersSet;
	Image *image;
	ImageArrayController *imageArrayController;
	NSArray *availableLUTs;
	
	NSTimer *delayedThumbnailUpdateTimer;

	// Outlets
	IBOutlet NSSlider *exposureSlider;
	IBOutlet NSSlider *brightnessSlider;
	IBOutlet NSSlider *contrastSlider;
	IBOutlet NSSlider *saturationSlider;
	IBOutlet NSSlider *sharpnessSlider;
	IBOutlet NSSlider *blurSlider;
	IBOutlet NSColorWell *whiteBalanceColorWell;
	IBOutlet NSButton *invertCheckbox;
	IBOutlet NSSlider *thresholdSlider;
	IBOutlet NSButton *colorPickerButton;
	
	IBOutlet NSPopUpButton *lutPopupButton;
	
	IBOutlet NSButton *applyToStackButton;
}

- (IBAction)copyAllFilters:(id)sender;
- (IBAction)pasteAllFilters:(id)sender;
- (IBAction)resetAllFilters:(id)sender;

- (IBAction)colorWellChanged:(id)sender;
- (IBAction)pickColorForWhiteBalanceCorrection:(id)sender;
- (void)setColorForWhiteBalanceCorrectionFilter:(NSColor *)aColor;

- (CIFilter *)filterInWorkingSetWithName:(NSString *)filterName;
- (NSArray *)fixedFilters;
- (void)loadFiltersFromImage;
- (void)bindFilter:(CIFilter *)filter toUIWithName:(NSString *)filterName;
- (void)unbindFilter:(CIFilter *)filter;
- (void)setIdentitiesForFilter:(CIFilter *)filter;

- (void)removeAllFilters;
- (void)saveFilters;
- (IBAction)applyFiltersToEntireStack:(id)sender;

- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem;

- (void)setLUTPopUpContent:(NSArray *)array;
- (void)changeLUT:(id)sender;
- (NSArray *)availableLUTs;
- (NSString *)lutTitleForPath:(NSString *)path;
- (void)updateLUTSelectionForPath:(NSString *)path;


@end
