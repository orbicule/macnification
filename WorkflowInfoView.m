//
//  WorkflowInfoView.m
//  LightTable
//
//  Created by Dennis Lorson on 20/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "WorkflowInfoView.h"
#import "WorkflowInfoPanel.h"

@implementation WorkflowInfoView

- (id)initWithFrame:(NSRect)frame 
{
	self = [super initWithFrame:frame];
    
	if (self) {


    }
	
    return self;
}

- (void)awakeFromNib
{
	[self setWantsLayer:YES];
	backgroundLayer = [CATextLayer layer];
	
	backgroundLayer.backgroundColor = CGColorCreateGenericGray(0, 0.5);
	backgroundLayer.borderColor = CGColorCreateGenericGray(1, 1);
	
	backgroundLayer.borderWidth = 2.0;
	backgroundLayer.cornerRadius = 15.0;
	
	backgroundLayer.frame = [self layer].bounds;
	backgroundLayer.autoresizingMask = kCALayerWidthSizable | kCALayerHeightSizable;
	
	backgroundLayer.opacity = 0.0;

	
	textLayer = [CATextLayer layer];
	
	textLayer.font = [NSFont systemFontOfSize:12];
	textLayer.fontSize = 12;
	textLayer.foregroundColor = CGColorCreateGenericGray(1, 1);
	textLayer.frame = backgroundLayer.bounds;
		

	closeButtonLayer = [CALayer layer];
	closeButtonLayer.backgroundColor = CGColorCreateGenericGray(0, 1);
	closeButtonLayer.borderColor = CGColorCreateGenericGray(1, 1);
	
	closeButtonLayer.borderWidth = 2.0;
	closeButtonLayer.cornerRadius = 11.5;
	closeButtonLayer.delegate = self;
	
	closeButtonLayer.frame = CGRectMake(0,0,23,23);

	[backgroundLayer addSublayer:textLayer];
	[[self layer] addSublayer:backgroundLayer];
	[[self layer] addSublayer:closeButtonLayer];

}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
	NSGraphicsContext *nsGraphicsContext;
	nsGraphicsContext = [NSGraphicsContext graphicsContextWithGraphicsPort:ctx
																   flipped:NO];
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:nsGraphicsContext];
	
	NSRect bounds = NSRectFromCGRect(CGContextGetClipBoundingBox(ctx));
	
	NSRect crossBox = NSInsetRect(bounds, 8, 8);
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	[path moveToPoint:NSMakePoint(NSMinX(crossBox), NSMinY(crossBox))];
	[path lineToPoint:NSMakePoint(NSMaxX(crossBox), NSMaxY(crossBox))];

	[path moveToPoint:NSMakePoint(NSMinX(crossBox), NSMaxY(crossBox))];
	[path lineToPoint:NSMakePoint(NSMaxX(crossBox), NSMinY(crossBox))];

	[path setLineWidth:2.5];
	[path setLineCapStyle:NSRoundLineCapStyle];
	
	[[NSColor whiteColor] set];
	
	[path stroke];
	
	[NSGraphicsContext restoreGraphicsState];
}

- (void)relayout
{
	// trigger the setMessage method
	[self setMessage:textLayer.string];
	
}


- (NSRect)setMessage:(NSString *)string
{
	// if we want a button and there is no closeButton yet, create it.
	BOOL wantsCloseButton = ((WorkflowInfoPanel *)[self window]).hasCloseButton && [[[self window] delegate] respondsToSelector:@selector(workflowInfoPanelShouldOrderOut:)];
	
	closeButtonLayer.hidden = !wantsCloseButton;
	
	textLayer.string = string;

	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont systemFontOfSize:12], NSFontAttributeName, nil];
	
	NSSize textSize = [textLayer.string sizeWithAttributes:attributes];
	NSSize margin = NSMakeSize(12,8);

	
	CGRect frame = textLayer.frame;
	frame.size.width = textSize.width;
	frame.size.height = textSize.height;
	frame.origin.x = margin.width;
	frame.origin.y = margin.height;
	textLayer.frame = frame;
	
	
	CGRect backgroundFrame = backgroundLayer.frame;
	backgroundFrame.size.width = textLayer.frame.size.width + 2 * margin.width;
	backgroundFrame.size.height = textLayer.frame.size.height + 2 * margin.height;
	
	backgroundLayer.frame = backgroundFrame;
	
	// now, position the close button in the top right corner of the text layer
	CGPoint topRightPoint = CGPointMake(CGRectGetMaxX(backgroundFrame), CGRectGetMaxY(backgroundFrame));
	CGPoint closeButtonOrigin = CGPointMake(topRightPoint.x - closeButtonLayer.frame.size.width/2 - 1, topRightPoint.y - closeButtonLayer.frame.size.height/2 - 2);
	
	CGRect newCloseFrame;
	newCloseFrame.origin = closeButtonOrigin;
	newCloseFrame.size = closeButtonLayer.frame.size;
	closeButtonLayer.frame = newCloseFrame;
	
	[closeButtonLayer setNeedsDisplay];
	
	backgroundFrame = CGRectUnion(backgroundFrame, newCloseFrame);
	
	
	return NSRectFromCGRect(backgroundFrame);
}


- (void)mouseDown:(NSEvent *)theEvent
{
	[super mouseDown:theEvent];
	//NSLog(@"CLOSE");

	NSPoint loc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	if(!((WorkflowInfoPanel *)[self window]).hasCloseButton) return;
	
	if (NSPointInRect(loc, NSRectFromCGRect(closeButtonLayer.frame))) {

		
		if ([[[self window] delegate] respondsToSelector:@selector(workflowInfoPanelShouldOrderOut:)]) {
			
			
			// TODO: disabled for now
			if (YES/*[[[self window] delegate] workflowInfoPanelShouldOrderOut:[self window]]*/) {
				
				[[self window] orderOut:self];
				[[self window] setDelegate:nil];
				
			}
			
		}
		
		
	}

	
}



@end
