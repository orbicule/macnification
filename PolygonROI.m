//
//  PolygonROI.m
//  Macnification
//
//  Created by Peter Schols on 12/04/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "PolygonROI.h"
#import "Image.h"



@implementation PolygonROI


+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	NSMutableSet *kps = [[[super keyPathsForValuesAffectingValueForKey:key] mutableCopy] autorelease];
	
	
	if ([key isEqualToString:@"summary"]) {
		[kps addObject:@"area"];
		[kps addObject:@"perimeter"];
	}
	
	return kps;
}


+ (void)initialize 
{
	// Any change in length should trigger a change in summary
	//[self setKeys:[NSArray arrayWithObjects:@"area", @"perimeter", nil] triggerChangeNotificationsForDependentKey:@"summary"];
}



- (void)setPolygonPoints:(NSArray *)pointsArray;
{
	[self setAllPoints:pointsArray];	
	// Update our measurements
	[self updateMeasurements];
}



- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
{
	// Get the existing points
	NSMutableArray *existingPoints = [NSMutableArray arrayWithCapacity:20];
	[existingPoints addObjectsFromArray:[self allPoints]];
	// Change the point we are dragging
	[existingPoints replaceObjectAtIndex:handle-1 withObject:[NSValue valueWithPoint:point]];
	[self setAllPoints:existingPoints];
	// Update our measurements
	[self updateMeasurements];
}



- (void)moveWithDelta:(NSPoint)deltaPoint;
{
	NSMutableArray *existingPoints = [NSMutableArray arrayWithCapacity:20];
	for (NSValue *value in [self allPoints]) {
		NSPoint currentPoint = [value pointValue];
		NSPoint newPoint = NSMakePoint(currentPoint.x + deltaPoint.x, currentPoint.y + deltaPoint.y);
		[existingPoints addObject:[NSValue valueWithPoint:newPoint]];
	}
	[self setAllPoints:existingPoints];
}




- (void)updateMeasurements;
{
	// Calculate our area and perimeter attributes and set it
	[self calculateArea];
	[self calculatePerimeter];
}



// We calculate the area once, so we don't have to repeat this calculation every time
- (void)calculateArea;
{
	NSInteger area = 0;
	
	NSArray *allPoints = [self allPoints];
	NSInteger n = [allPoints count];
	
	NSInteger i, j;
	for (i = 0; i < n; i++) {
		j = (i + 1) % n;
		
		NSPoint first = [[allPoints objectAtIndex:i] pointValue];
		NSPoint second = [[allPoints objectAtIndex:j] pointValue];

		area += first.x * second.y;
		area -= first.y * second.x;
	}
	
	area /= 2;
	
	area = ABS(area);
	
	NSInteger pixelArea = area;
    
	float realArea = [[self valueForKey:@"image"] realAreaForPixelArea:area];
	
	// Set the area attributes
	[self setValue:[NSNumber numberWithDouble:pixelArea] forKey:@"pixelArea"];
	[self setValue:[NSNumber numberWithFloat:realArea] forKey:@"area"];
		
}



// Calculate the perimeter in a real world unit
- (void)calculatePerimeter;
{
	int i;
	float pixelPerimeter = 0.0;
	// Iterate all points and calculate the sum of their distance
	NSArray *allPoints = [self allPoints];
	for (i = 1; i < [allPoints count]; i++) {
		NSPoint thePreviousPoint = [[[self allPoints] objectAtIndex:i-1] pointValue];
		NSPoint thePoint = [[[self allPoints] objectAtIndex:i] pointValue];
		pixelPerimeter = pixelPerimeter + [self pixelLengthBetweenPoint:thePreviousPoint andPoint:thePoint];
	}
	
	// Add the length from the last point to the first to close the polygon
	NSPoint firstPoint = [[[self allPoints] objectAtIndex:0] pointValue];
	NSPoint lastPoint = [[[self allPoints]objectAtIndex:[[self allPoints]count]-1] pointValue];
	pixelPerimeter = pixelPerimeter + [self pixelLengthBetweenPoint:firstPoint andPoint:lastPoint];
	
	
	// Convert the pixelPerimeter to the real perimeter
	float realPerimeter = [[self valueForKey:@"image"] realLengthForPixelLength:pixelPerimeter];
	
	// Set the area attributes
	[self setValue:[NSNumber numberWithInt:roundf(pixelPerimeter)] forKey:@"pixelPerimeter"];
	[self setValue:[NSNumber numberWithFloat:realPerimeter] forKey:@"perimeter"];
}



- (float)pixelLengthBetweenPoint:(NSPoint)point1 andPoint:(NSPoint)point2;
{
	float xSize = point1.x - point2.x;
	float ySize = point1.y - point2.y;
	float pixelLength = sqrt(pow(xSize, 2) + pow(ySize, 2));	
	return pixelLength;
}



- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
	NSBezierPath *path = [self pathWithScale:scale];
	[path setLineWidth:stroke*scale];
	[path stroke];
	
	if (isSelected) {
		for (NSValue *value in [self allPoints]) {
			NSPoint thePoint = [value pointValue];
			[self drawHandleAtPoint:thePoint withScale:stroke transform:trafo view:view];
		}
	}
	// Let our ROI superclass do the drawing of the note icon
	[super drawNoteIconWithScale:stroke transform:trafo view:view];
}



- (NSBezierPath *)pathWithScale:(float)scale;
{
	int i;
	NSBezierPath *path = [[NSBezierPath alloc] init];
	//NSLog(@"pathWithScale in %@ with [self allPoints]: %@", [[self objectID]description], [self allPoints]);
	[path moveToPoint:[[[self allPoints] objectAtIndex:0]pointValue]];
	for (i = 1; i < [[self allPoints] count]; i++) {
		NSPoint thePoint = [[[self allPoints] objectAtIndex:i]pointValue];
		[path lineToPoint:thePoint];
	}
	if (isCurrentlyBeingDrawn && [[self allPoints] count] > 0) {
		[path lineToPoint:NSMakePoint(movedPoint.x * scale, movedPoint.y * scale)];
	}
	else {
		[path closePath];
	}
	return [path autorelease];
}



- (int)handleUnderPoint:(NSPoint)point;
{
	for (NSValue *value in [self allPoints]) {
		NSPoint polygonPoint = [value pointValue];
		NSRect handleRect = [self hitRectForHandleAtPoint:polygonPoint withScale:1.0];
		
		if (NSPointInRect(point, handleRect)) {
			return [[self allPoints] indexOfObject:value] + 1;
		}
	}
	return 0;
}





- (BOOL)containsPoint:(NSPoint)point;
{
	NSBezierPath *path = [self pathWithScale:1.0];
	if ([path containsPoint:point]) {
		return YES;
	}
	// Check if the use has clicked the noteicon
	if (NSPointInRect(point, [self noteIconRect])) {
		return NO;	
	}
	return NO;
}



- (NSRect)noteIconRect;
{
	NSArray *allPoints = [self allPoints];
	if ([allPoints count] == 0)
		return NSZeroRect;
	
	NSPoint startPoint = [[allPoints objectAtIndex:0] pointValue];

	NSPoint endPoint;
	if ([allPoints count] == 1)
		endPoint = startPoint;
	else
		endPoint = [[allPoints objectAtIndex:1] pointValue];
	return NSMakeRect(((startPoint.x + endPoint.x) / 2) + 3, (startPoint.y + endPoint.y) / 2, 16, 16);
}



- (NSAttributedString *)pixelSummary;
{
	NSString *theString = [NSString stringWithFormat:@"%C %lld px\n%C %d px", (unsigned short)0x2617, [[self valueForKey:@"pixelArea"] longLongValue], (unsigned short)0x2616, [[self valueForKey:@"pixelPerimeter"]intValue]];
	NSMutableAttributedString *aString = [[[NSMutableAttributedString alloc] initWithString:theString] autorelease];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:12], NSFontAttributeName, [self summaryColor], NSForegroundColorAttributeName, nil];
	// Add the attributes to the first character and to the first character after the line break
	[aString addAttributes:dict range:NSMakeRange(0, 1)];
	NSRange range = [theString rangeOfString:@"\n"];
	[aString addAttributes:dict range:NSMakeRange(range.location+1, 1)];
	return aString;
}



- (NSString *)summary;
{
	NSString *theString;
	float area = [[self valueForKey:@"area"]floatValue];
	if (area < 100000000000000.0 && area > 0.000000001) {
		theString = [NSString stringWithFormat:@"%2.2f %@%C\n%2.2f %@", area, [self valueForKeyPath:@"image.calibration.unit"], (unsigned short)0x00B2, [[self valueForKey:@"perimeter"]floatValue], [self valueForKeyPath:@"image.calibration.unit"]];
	}
	else {
		theString = @"Not calibrated";
	}
	return theString;
}





// Accessors
- (BOOL)isCurrentlyBeingDrawn {
    return isCurrentlyBeingDrawn;
}

- (void)setIsCurrentlyBeingDrawn:(BOOL)value {
    if (isCurrentlyBeingDrawn != value) {
        isCurrentlyBeingDrawn = value;
    }
}

- (NSPoint)movedPoint {
    return movedPoint;
}

- (void)setMovedPoint:(NSPoint)value {
	movedPoint = value;
}


@end
