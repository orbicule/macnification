//
//  WorkflowInfoWindow.h
//  LightTable
//
//  Created by Dennis Lorson on 20/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class WorkflowInfoView;


@interface WorkflowInfoPanel : NSWindow {
	
	IBOutlet WorkflowInfoView *infoView; 
	
	BOOL hasCloseButton;						// if set to YES and the window delegate implements workflowInfoPanelShouldOrderOut: then a close button is displayed.
	
}

@property(nonatomic) BOOL hasCloseButton;

- (void)setMessage:(NSString *)string;



@end
