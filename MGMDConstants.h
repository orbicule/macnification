//
//  MGMDConstants.h
//  MetaData
//
//  Created by Dennis Lorson on 27/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSString *MGMDNoSelectionString;
extern NSString *MGMDNullValueString;
extern NSString *MGMDMultipleValuesString;
extern NSString *MGMDNotApplicableString;

@interface MGMDConstants : NSObject {

}

@end
