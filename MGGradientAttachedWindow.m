//
//  MGGradientAttachedWindow.m
//  Filament
//
//  Created by Dennis Lorson on 13/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGGradientAttachedWindow.h"



@implementation MGGradientAttachedWindow



- (NSColor *)_backgroundColorPatternImage
{
    NSImage *bg = [[NSImage alloc] initWithSize:[self frame].size];
    NSRect bgRect = NSZeroRect;
    bgRect.size = [bg size];
    
    [bg lockFocus];
    NSBezierPath *bgPath = [self _backgroundPath];
    [NSGraphicsContext saveGraphicsState];
    [bgPath addClip];
    
    // Draw background.
    
	NSGradient *bgGrad = [[[NSGradient alloc] initWithColorsAndLocations:
						   [NSColor colorWithCalibratedWhite:0.2 alpha:0.7], 0.01, 
						   [NSColor colorWithCalibratedWhite:0.0 alpha:0.7], 0.35, nil] autorelease];

	
	[bgGrad drawInBezierPath:bgPath angle:-90];
	
	//[bgGloss drawInBezierPath:bgPath angle:-90];
	
	//[bgPath fill];
    
    // Draw border if appropriate.
    if (borderWidth > 0) {
        // Double the borderWidth since we're drawing inside the path.
        [bgPath setLineWidth:(borderWidth * 2.0) * MAATTACHEDWINDOW_SCALE_FACTOR];
        [borderColor set];
        [bgPath stroke];
    }
    
    [NSGraphicsContext restoreGraphicsState];
    [bg unlockFocus];
    
    return [NSColor colorWithPatternImage:[bg autorelease]];
}



- (void)_appendArrowToPath:(NSBezierPath *)path
{
    if (!hasArrow) {
        return;
    }
    
    float scaleFactor = MAATTACHEDWINDOW_SCALE_FACTOR;
    float scaledArrowWidth = arrowBaseWidth * 0.75 * scaleFactor;
    float halfArrowWidth = scaledArrowWidth / 2.0;
    float scaledArrowHeight = arrowHeight * 0.6 * scaleFactor;
    NSPoint currPt = [path currentPoint];
    NSPoint tipPt = currPt;
    NSPoint endPt = currPt;
    
	
	// Arrow points towards bottom. We're starting from the right.
	tipPt.y -= scaledArrowHeight;
	tipPt.x -= halfArrowWidth;
	endPt.x -= scaledArrowWidth;
	
    [path lineToPoint:tipPt];
    [path lineToPoint:endPt];
}




- (NSBezierPath *)_backgroundPath
{
    /*
     Construct path for window background, taking account of:
     1. hasArrow
     2. _side
     3. drawsRoundCornerBesideArrow
     4. arrowBaseWidth
     5. arrowHeight
     6. cornerRadius
     */
	cornerRadius = 10.0;
    float scaleFactor = MAATTACHEDWINDOW_SCALE_FACTOR;
    float scaledRadius = cornerRadius * scaleFactor;
    float scaledArrowWidth = arrowBaseWidth * 0.75 * scaleFactor;
    float halfArrowWidth = scaledArrowWidth / 2.0;
    NSRect contentArea = NSInsetRect(_viewFrame,
                                     -viewMargin * scaleFactor,
                                     -viewMargin * scaleFactor);
    float minX = NSMinX(contentArea) * scaleFactor;
    float midX = NSMidX(contentArea) * scaleFactor;
    float maxX = NSMaxX(contentArea) * scaleFactor;
    float minY = NSMinY(contentArea) * scaleFactor;
    float midY = NSMidY(contentArea) * scaleFactor;
    float maxY = NSMaxY(contentArea) * scaleFactor;
    
    NSBezierPath *path = [NSBezierPath bezierPath];
    [path setLineJoinStyle:NSRoundLineJoinStyle];
    
    // Begin at top-left. This will be either after the rounded corner, or 
    // at the top-left point if cornerRadius is zero and/or we're drawing 
    // the arrow at the top-left or left-top without a rounded corner.
    NSPoint currPt = NSMakePoint(minX, maxY);
    if (scaledRadius > 0 &&
        (drawsRoundCornerBesideArrow || 
		 (_side != MAPositionBottomRight && _side != MAPositionRightBottom)) 
        ) {
        currPt.x += scaledRadius;
    }
    
    NSPoint endOfLine = NSMakePoint(maxX, maxY);
    BOOL shouldDrawNextCorner = NO;
    if (scaledRadius > 0 &&
        (drawsRoundCornerBesideArrow || 
         (_side != MAPositionBottomLeft && _side != MAPositionLeftBottom)) 
        ) {
        endOfLine.x -= scaledRadius;
        shouldDrawNextCorner = YES;
    }
    
    [path moveToPoint:currPt];
    
    // If arrow should be drawn at top-left point, draw it.
    if (_side == MAPositionBottomRight) {
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionBottom) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(midX - halfArrowWidth, maxY)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionBottomLeft) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(endOfLine.x - scaledArrowWidth, maxY)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    }
    
    // Line to end of this side.
    [path lineToPoint:endOfLine];
    
    // Rounded corner on top-right.
    if (shouldDrawNextCorner) {
        [path appendBezierPathWithArcFromPoint:NSMakePoint(maxX, maxY) 
                                       toPoint:NSMakePoint(maxX, maxY - scaledRadius) 
                                        radius:scaledRadius];
    }
    
    
    // Draw the right side, beginning at the top-right.
    endOfLine = NSMakePoint(maxX, minY);
    shouldDrawNextCorner = NO;
    if (scaledRadius > 0 &&
        (drawsRoundCornerBesideArrow || 
         (_side != MAPositionTopLeft && _side != MAPositionLeftTop)) 
        ) {
        endOfLine.y += scaledRadius;
        shouldDrawNextCorner = YES;
    }
    
    // If arrow should be drawn at right-top point, draw it.
    if (_side == MAPositionLeftBottom) {
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionLeft) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(maxX, midY + halfArrowWidth)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionLeftTop) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(maxX, endOfLine.y + scaledArrowWidth)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    }
    
    // Line to end of this side.
    [path lineToPoint:endOfLine];
    
    // Rounded corner on bottom-right.
    if (shouldDrawNextCorner) {
        [path appendBezierPathWithArcFromPoint:NSMakePoint(maxX, minY) 
                                       toPoint:NSMakePoint(maxX - scaledRadius, minY) 
                                        radius:scaledRadius];
    }
    
    
    // Draw the bottom side, beginning at the bottom-right.
    endOfLine = NSMakePoint(minX, minY);
    shouldDrawNextCorner = NO;
    if (scaledRadius > 0 &&
        (drawsRoundCornerBesideArrow || 
         (_side != MAPositionTopRight && _side != MAPositionRightTop)) 
        ) {
        endOfLine.x += scaledRadius;
        shouldDrawNextCorner = YES;
    }
    
    // If arrow should be drawn at bottom-right point, draw it.
    if (_side == MAPositionTopLeft) {
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionTop) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(midX + halfArrowWidth, minY)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionTopRight) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(endOfLine.x + scaledArrowWidth, minY)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    }
    
    // Line to end of this side.
    [path lineToPoint:endOfLine];
    
    // Rounded corner on bottom-left.
    if (shouldDrawNextCorner) {
        [path appendBezierPathWithArcFromPoint:NSMakePoint(minX, minY) 
                                       toPoint:NSMakePoint(minX, minY + scaledRadius) 
                                        radius:scaledRadius];
    }
    
    
    // Draw the left side, beginning at the bottom-left.
    endOfLine = NSMakePoint(minX, maxY);
    shouldDrawNextCorner = NO;
    if (scaledRadius > 0 &&
        (drawsRoundCornerBesideArrow || 
         (_side != MAPositionRightBottom && _side != MAPositionBottomRight)) 
        ) {
        endOfLine.y -= scaledRadius;
        shouldDrawNextCorner = YES;
    }
    
    // If arrow should be drawn at left-bottom point, draw it.
    if (_side == MAPositionRightTop) {
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionRight) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(minX, midY - halfArrowWidth)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    } else if (_side == MAPositionRightBottom) {
        // Line to relevant point before arrow.
        [path lineToPoint:NSMakePoint(minX, endOfLine.y - scaledArrowWidth)];
        // Draw arrow.
        [self _appendArrowToPath:path];
    }
    
    // Line to end of this side.
    [path lineToPoint:endOfLine];
    
    // Rounded corner on top-left.
    if (shouldDrawNextCorner) {
        [path appendBezierPathWithArcFromPoint:NSMakePoint(minX, maxY) 
                                       toPoint:NSMakePoint(minX + scaledRadius, maxY) 
                                        radius:scaledRadius];
    }
    
    [path closePath];
    return path;
}



- (BOOL)canBecomeKeyWindow;
{
	return NO;	
}




@end
