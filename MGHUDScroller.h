//
//  MGHUDScroller.h
//  Filament
//
//  Created by Dennis Lorson on 27/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGHUDScroller : NSScroller {

}

+ (CGFloat)scrollerWidth;
+ (CGFloat)scrollerWidthForControlSize: (NSControlSize)controlSize;

@end


@interface NSScroller (Additions)

+ (void)_setArrowsConfig:(int)config;

@end
