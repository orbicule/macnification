//
//  AnalysisPlugin.h
//  Filament
//
//  Created by Peter Schols on 13/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Image.h"


@interface AnalysisPlugin : NSObject {
	
	IBOutlet NSWindow *pluginWindow;
}


+ (AnalysisPlugin *)analysisPlugin;
- (NSWindow *)analysisWindowForImages:(NSArray *)images;

- (IBAction)endSheet:(id)sender;


@end



// We are declaring this method here to avoid compiler warnings
@interface NSObject (NSObjectExtensions) 

- (void)endPluginSheet:(NSWindow *)sheet;

@end



// Declared here to avoid warnings
@interface Instrument : NSObject {
	
}

- (NSNumber *)magnification;

@end