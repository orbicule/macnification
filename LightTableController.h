//
//  LightTableController.h
//  Filament
//
//  Created by Peter Schols on 25/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGCustomMetadataController.h"

@interface LightTableController : NSObject <MGCustomMetadataObserver>
{
	IBOutlet NSView * m_view;
	
	IBOutlet NSScrollView *ltScrollView;
	
	IBOutlet NSButton *toggleMDButton;
	
	IBOutlet id ltImageArrayController;
	IBOutlet NSSearchField *ltSearchField;
	
	IBOutlet NSView *opacitySliderView, *noSelectionView; 
	
	
	IBOutlet NSTextField *searchMessage;
	IBOutlet NSButton *doneButton;
	
}

- (id)initWithSuperview:(NSView *)superview;

- (void)removeFilterPredicate;
- (NSArrayController *)contentController;
- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;

- (IBAction)clearFilterPredicate:(id)sender;

- (void)showSearchBarWithMessage:(NSString *)message;
- (void)hideSearchBar;

- (void)changeSearchCategory:(id)sender;
- (IBAction)changeSearchString:(id)sender;
@end
