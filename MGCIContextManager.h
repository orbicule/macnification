//
//  MGCIContextManager.h
//  Filament
//
//  Created by Dennis Lorson on 04/09/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@interface MGCIContextManager : NSObject {
	
	CIContext *currentContext;
	
	CGContextRef backingContext;
	CGRect currentContextExtent;
	
	void *backingContextData;
}

+ (MGCIContextManager *)sharedContextManager;

- (CIContext *)contextWithExtent:(CGRect)extent;
- (void)clearContext;

- (void)renderImage:(CIImage *)image inRect:(CGRect)rect;

- (CGImageRef)imageRefFromCIImage_10A432_WORKAROUND:(CIImage *)ciImg;
- (CGImageRef)imageRefFromCIImage:(CIImage *)ciImg;

- (CGImageRef)copyOfImage:(CGImageRef)original;


@end
