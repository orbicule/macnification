//
//  MGOMEFileImporter_Private.h
//  Filament
//
//  Created by Dennis Lorson on 28/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGOMEFileImporter (Private)

- (void)_addMetadataFromRoot:(NSXMLElement *)root toSet:(MGMetadataSet *)set;

- (NSXMLElement *)_elementForName:(NSString *)name inElement:(NSXMLElement *)parent;
- (NSXMLElement *)_elementForXPath:(NSString *)path inElement:(NSXMLElement *)parent;

@end
