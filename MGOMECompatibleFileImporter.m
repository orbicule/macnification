//
//  MGOMECompatibleFileImporter.m
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGOMECompatibleFileImporter.h"

#import "MGImageFileImporter_Private.h"
#import "MGMetadataSet.h"
#import "StringExtensions.h"


@interface MGOMECompatibleFileImporter ()


- (NSString *)_omeTIFFFilePath;
- (void)_convertToOMETIFFFileIfNeeded;
- (void)_removeOMETIFFFile;
- (NSString *)_lociToolsNameForFile:(NSString *)file;


@end




@implementation MGOMECompatibleFileImporter

+ (BOOL)_canOpenFilePath:(NSString *)path;
{

	if ([[self supportedPathExtensions] containsObject:[[path compositePathExtension] lowercaseString]])
        return YES;
    
    return [[self supportedPathExtensions] containsObject:[[path pathExtension] lowercaseString]];
}


+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:
			@"lsm", @"seq", @"ics", @"ids", @"visbio", @"ipw", @"stk", 
			@"stk.tiff", @"stk.tif", @"zvi", @"oif", @"svs", @"lif", @"ipm", @"dv", nil];
}


- (float)fractionOfImportTimeForNonRepresentationWork;
{
	return 0.5;
}


- (void) dealloc
{
	[self _removeOMETIFFFile];
	
	[super dealloc];
}



- (NSString *)filePath;
{
	return filePath_;
}

- (NSString *)representationFilePath;
{
	return [self _omeTIFFFilePath];
}





- (MGMetadataSet *)_generateMetadata
{
	[self _convertToOMETIFFFileIfNeeded];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:[self _omeTIFFFilePath]])
		return nil;
	
	MGMetadataSet *set = [super _generateMetadata];
	
	// Bio-Formats will auto-correct the endianness but seems to leave the original endianness in the header.  So ignore it.
	[set removeValueForKey:MGMetadataImageLittleEndianKey];
	
	return set;
}



- (int)numberOfImageRepresentations
{
	[self _convertToOMETIFFFileIfNeeded];
	
	return [super numberOfImageRepresentations];
}


- (NSBitmapImageRep *)imageRepresentationAtIndex:(int)index
{
	[self _convertToOMETIFFFileIfNeeded];

	return [super imageRepresentationAtIndex:index];
}


#pragma mark -
#pragma mark Conversion to OME

- (NSString *)_omeTIFFFilePath
{
	NSString *fileName = [NSString stringWithFormat:@"%@_%i.ome.tiff", [[filePath_ lastPathComponent] stringByDeletingPathExtension], (int)[self hash]];
	
	NSString *fullPath = [NSString stringWithFormat:@"/var/tmp/%@", fileName];
	
	return fullPath;
}


- (void)_convertToOMETIFFFileIfNeeded
{
	if (didConvertToOME_)
		return;
	
	didConvertToOME_ = YES;
    

	
	NSString *omeFilePath = [self _omeTIFFFilePath];
	
	NSString *pathToOMEFolder = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Contents/Resources/ome/"];
	NSPipe *pipe = [NSPipe pipe];
	
	NSTask *javaTask = [[NSTask alloc] init];
	[javaTask setStandardOutput:pipe];
	[javaTask setLaunchPath:@"/usr/bin/java"];
	[javaTask setArguments:[NSArray arrayWithObjects:
							@"-mx512m", 
							@"-Djava.awt.headless=true", 
							@"-classpath", [pathToOMEFolder stringByAppendingPathComponent:[self _lociToolsNameForFile:filePath_]], 
							@"loci.formats.tools.ImageConverter",
							//@"-merge",		// causes invalid CGimagesources (e.g. 2 channels)
							filePath_, 
							omeFilePath, nil]];
	[javaTask launch];
	[javaTask waitUntilExit];
	
	while ([javaTask isRunning]) {}
    
    

	
	[javaTask release];
	
	// check if the file really exists to avoid false image conversion error triggering
	// NOTE: should always provide a tilde-expanded path!
	if (![[NSFileManager defaultManager] fileExistsAtPath:omeFilePath])
		NSLog(@"%@ -- Conversion to TIFF failed (File at %@ does not exist)", [self description], omeFilePath);
	
}



- (void)_removeOMETIFFFile
{
	NSString *path = [self _omeTIFFFilePath];
	
	//NSLog(@"Removing file at path %@", path);
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}


// Decide which loci_tools version we are going to use based on the file type (extension)
// We use several versions to maximize format compatibility
- (NSString *)_lociToolsNameForFile:(NSString *)file;
{	
	NSString *ext = [[file pathExtension] lowercaseString];
	
	if ([ext isEqualTo:@"stk"]) {
        return @"loci_tools_2010_12_06.jar";	
    }
    
    if ([ext isEqualTo:@"lif"])
        return @"loci_tools_2011_06_16.jar";


	//if ([ext isEqualTo:@"ics"] || [ext isEqualTo:@"ids"] || [ext isEqualTo:@"lif"] || [ext isEqualTo:@"lsm"] || [ext isEqualTo:@"ipm"] || [ext isEqualTo:@"dv"] || [ext isEqualTo:@"zvi"]) {
		return @"loci_tools_svn.jar";
	//}
	
	//return @"loci_tools.jar";
}


@end
