#import "StatisticsController.h"


@implementation StatisticsController



- (void)awakeFromNib;
{
	// Register as an observer for the selected objects in the ROIArrayController
	[roiController addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	[roiController addObserver:self forKeyPath:@"arrangedObjects" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
}



// Sent when the ROI selection changes
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	//NSLog(@"ROI selection has changed");
	if ([statisticsWindow isVisible]) {
		[self updateStatistics];
	}
}



// Sent by the popup when the selection changes
- (IBAction)popUpDidChangeSelection:(id)sender;
{
    [self updateStatistics];
}


- (IBAction)copyStatisticsToClipboard:(id)sender;
{
	NSPasteboard *pb = [NSPasteboard generalPasteboard];
	[pb declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:nil];
    [pb setString:[self pasteBoardString] forType:NSStringPboardType];	
}




// Calculate everything
- (void)updateStatistics;
{
	if ([[roiController arrangedObjects] count] == 0) {
		[countField setStringValue:@"0"];
		[minimumField setStringValue:@""];
		[maximumField setStringValue:@""];
		[averageField setStringValue:@""];
		[deviationField setStringValue:@""];
		[errorField setStringValue:@""];		
	}
	
	else {
		BOOL isCalibrated = NO;
		BOOL isPixelStatistic = NO;
		if ([[[roiController arrangedObjects] objectAtIndex:0] valueForKeyPath:@"image.calibration"]) {
			isCalibrated = YES;
		}
		
		NSString *unit = [[[roiController arrangedObjects] objectAtIndex:0] valueForKeyPath:@"image.calibration.unit"];
		

		NSString *key = [[popup titleOfSelectedItem] lowercaseString];
		if ([key isEqualTo:@"pixel length"]) {
			key = @"pixelLength";
			unit = @"px";
			isPixelStatistic = YES;
		}
		else if ([key isEqualTo:@"pixel area"]) {
			key = @"pixelArea";
			unit = @"px";
			isPixelStatistic = YES;
		}
		else if ([key isEqualTo:@"pixel perimeter"]) {
			key = @"pixelPerimeter";
			unit = @"px";
			isPixelStatistic = YES;
		}
		
		// If there is only one ROI selected (or none), calculate the statistics for every ROI
		if ([[roiController selectedObjects] count] < 2) {
			roiArray = [roiController arrangedObjects];
		}
		else {
			roiArray = [roiController selectedObjects];
		}
		
		
		if ([key isEqualTo:@"area"]) {
			unit = [unit stringByAppendingString:[NSString stringWithFormat:@"%C", (unsigned short)0x00B2]];
		}
		
		NSNumber *count = [self calculateCountForKey:key];
		NSNumber *minimum = ([self calculateMinimumForKey:key]) ? [self calculateMinimumForKey:key]: [NSNumber numberWithInt:0];
		NSNumber *maximum = [self calculateMaximumForKey:key];
		NSNumber *average = [self calculateAverageForKey:key];
		NSNumber *deviation = [self calculateDeviationForKey:key];
		NSNumber *error = [self calculateErrorForKey:key];
	
		
		// Update the text fields
		if ((!isCalibrated && !isPixelStatistic) || ([count intValue] == 0)) {
			
			
			[countField setStringValue:@"0"];
			[minimumField setStringValue:@""];
			[maximumField setStringValue:@""];
			[averageField setStringValue:@""];
			[deviationField setStringValue:@""];
			[errorField setStringValue:@""];	
			[self setPasteBoardString:@"No measurements available"];
		}
		else {
			[countField setStringValue:[NSString stringWithFormat:@"%@", count]];
			[minimumField setStringValue:[NSString stringWithFormat:@"%@ %@", minimum, unit]];
			[maximumField setStringValue:[NSString stringWithFormat:@"%@ %@", maximum, unit]];
			[averageField setStringValue:[NSString stringWithFormat:@"%@ %@", average, unit]];
			[deviationField setStringValue:[NSString stringWithFormat:@"%@ %@", deviation, unit]];
			[errorField setStringValue:[NSString stringWithFormat:@"%@ %@", error, unit]];
			[self setPasteBoardString:[NSString stringWithFormat:@"Count: %@\nMinimum: %@ %@\nAverage: %@ %@\nMaximum: %@ %@\nStandard Deviation: %@ %@\nStandard Error: %@ %@", count, minimum, unit, average, unit, maximum, unit, deviation, unit, error, unit]];
		}
		
	}
}




- (NSNumber *)calculateCountForKey:(NSString *)key;
{
	int count = 0;
    for (id roi in roiArray) {
		// Only include this ROI if it has a property that corresponds to the key
		if ([[[roi entity] attributesByName] objectForKey:key]) {
			count++;
		}
	}
	return [NSNumber numberWithInt:count];
}



- (NSNumber *)calculateMinimumForKey:(NSString *)key;
{
    float minimum = 10000000000000000000.0;
    for (id roi in roiArray) {
		// Only include this ROI if it has a property that corresponds to the key
		if ([[[roi entity] attributesByName] objectForKey:key]) {
			float value = [[roi valueForKey:key]floatValue];
			if (value < minimum) {
				minimum = value;
			}
		}
	}
	return [NSNumber numberWithFloat:minimum];
}



- (NSNumber *)calculateMaximumForKey:(NSString *)key;
{
    float maximum = -10000000000000000000.0;
    for (id roi in roiArray) {
		// Only include this ROI if it has a property that corresponds to the key
		if ([[[roi entity] attributesByName] objectForKey:key]) {
			float value = [[roi valueForKey:key]floatValue];
			if (value > maximum) {
				maximum = value;
			}
		}
	}
	return [NSNumber numberWithFloat:maximum];
}



- (NSNumber *)calculateAverageForKey:(NSString *)key;
{
    float average = 0.0;
	int count = [[self calculateCountForKey:key]intValue];
	for (id roi in roiArray) {
		// Only include this ROI if it has a property that corresponds to the key
		if ([[[roi entity] attributesByName] objectForKey:key]) {
			float value = [[roi valueForKey:key]floatValue];
			average = average + value;
		}
	}
	return [NSNumber numberWithFloat:(average/count)];
}


// Officieel: StdDev = sqrt(differenceWithAverage^2/(count-1))
- (NSNumber *)calculateDeviationForKey:(NSString *)key;
{
    float deviation = 0.00;
	float sum = 0.00;
	float average = [[self calculateAverageForKey:key] floatValue];
	int count = [[self calculateCountForKey:key]intValue];
	for (id roi in roiArray) {
		// Only include this ROI if it has a property that corresponds to the key
		if ([[[roi entity] attributesByName] objectForKey:key]) {
			float value = [[roi valueForKey:key]floatValue];
			deviation = pow((value - average), 2);
			sum = sum + deviation;
		}
	}
	return [NSNumber numberWithFloat:sqrt(sum/(count-1))];
}


// StdErr = StdDev/sqrt(count)
- (NSNumber *)calculateErrorForKey:(NSString *)key;
{
	float stdev = [[self calculateDeviationForKey:key] floatValue];
	int count = [[self calculateCountForKey:key]intValue];
	return [NSNumber numberWithFloat:stdev/sqrt(count)];
}




- (NSString *)pasteBoardString {
    return [[pasteBoardString retain] autorelease];
}

- (void)setPasteBoardString:(NSString *)value {
    if (pasteBoardString != value) {
        [pasteBoardString release];
        pasteBoardString = [value copy];
    }
}




- (void)dealloc
{    
	[pasteBoardString release];
	[roiController release];
	[super dealloc];   
}
@end
