//
//  ScaleBar.h
//  Filament
//
//  Created by Peter Schols on 04/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KSExtensibleManagedObject.h"


@interface ScaleBar : KSExtensibleManagedObject {

}

- (void)setupWithProperties:(NSDictionary *)properties;
- (NSNumber *)calculatedScaleBarLength;
- (NSUInteger)increaseInteger:(NSUInteger)number toNearestX:(NSUInteger)x;

- (void)recalculateLength;

- (void)draw;
- (BOOL)containsPoint:(NSPoint)point;
- (void)setPosition:(NSPoint)point;

// Accessors
- (NSColor *)color;
- (void)setColor:(NSColor *)aColor;


@end
