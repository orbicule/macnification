//
//  CircleROI.m
//  Macnification
//
//  Created by Peter Schols on 11/04/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "CircleROI.h"
#import "Image.h"


@implementation CircleROI



+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	NSMutableSet *kps = [[[super keyPathsForValuesAffectingValueForKey:key] mutableCopy] autorelease];
	
	
	if ([key isEqualToString:@"summary"]) {
		[kps addObject:@"area"];
		[kps addObject:@"perimeter"];
	}
	
	return kps;
}



+ (void)initialize 
{
	// Any change in length should trigger a change in summary
	//[self setKeys:[NSArray arrayWithObjects:@"area", @"perimeter", nil] triggerChangeNotificationsForDependentKey:@"summary"];
}



- (void)setStartPoint:(NSPoint)startPoint endPoint:(NSPoint)endPoint;
{
	[self setAllPoints:[NSArray arrayWithObjects:[NSValue valueWithPoint:startPoint], [NSValue valueWithPoint:endPoint], nil]];
	// Update our measurements
	[self updateMeasurements];
}



// We resize the CircleROI by moving one of the handles
- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
{
	[self setStartPoint:oppositePoint endPoint:point];
}



- (NSPoint)oppositePointForHandle:(int)handle;
{
	NSRect bounds = [self boundsWithScale:1.0];
	
	NSPoint lowerLeftPoint = NSMakePoint(NSMinX(bounds), NSMinY(bounds));
	NSPoint upperLeftPoint = NSMakePoint(NSMinX(bounds), NSMaxY(bounds));
	NSPoint lowerRightPoint = NSMakePoint(NSMaxX(bounds), NSMinY(bounds));
	NSPoint upperRightPoint = NSMakePoint(NSMaxX(bounds), NSMaxY(bounds));
	
	if (handle == 1) {
		return upperRightPoint;
	}
	else if (handle == 2) {
		return lowerRightPoint;
	}
	else if (handle == 3) {
		return upperLeftPoint;
	}
	else {
		return lowerLeftPoint;
	}
}


- (void)updateMeasurements;
{
	// Calculate our area and perimeter attributes and set it
	[self calculateArea];
	[self calculatePerimeter];
}


// We calculate the area once, so we don't have to repeat this calculation every time
- (void)calculateArea;
{
	// Calculate the area in a real world unit
	float xSize = fabs([self startPoint].x - [self endPoint].x);
	float ySize = fabs([self startPoint].y - [self endPoint].y);
	float pixelArea = 3.1416 * (xSize / 2) * (ySize / 2);
	float realArea = [[self valueForKey:@"image"] realAreaForPixelArea:pixelArea];

	// Set the area attributes
	[self setValue:[NSNumber numberWithInt:roundf(pixelArea)] forKey:@"pixelArea"];
	[self setValue:[NSNumber numberWithFloat:realArea] forKey:@"area"];
	
}


- (void)calculatePerimeter;
{
	// Calculate the perimeter in a real world unit
	float xSize = fabs([self startPoint].x - [self endPoint].x);
	float ySize = fabs([self startPoint].y - [self endPoint].y);
	float pixelPerimeter = 3.1416 * sqrt(2 * (pow((xSize/2), 2) + pow((ySize/2), 2)));
	float realPerimeter = [[self valueForKey:@"image"] realLengthForPixelLength:pixelPerimeter];
	
	// Set the area attributes
	[self setValue:[NSNumber numberWithInt:roundf(pixelPerimeter)] forKey:@"pixelPerimeter"];
	[self setValue:[NSNumber numberWithFloat:realPerimeter] forKey:@"perimeter"];
}



- (NSPoint)startPoint;
{
	return [[[self allPoints]objectAtIndex:0]pointValue];
}


- (NSPoint)endPoint;
{
	return [[[self allPoints]objectAtIndex:1]pointValue];
}



- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
	
	
	
	NSRect bounds = [self boundsWithScale:scale];
	NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:bounds];
	[path setLineWidth:stroke*scale];
	[path stroke];
	
	if (isSelected) {
		// Draw the handles
		[self drawHandleAtPoint:NSMakePoint(NSMinX(bounds), NSMinY(bounds)) withScale:stroke transform:trafo view:view];
		[self drawHandleAtPoint:NSMakePoint(NSMinX(bounds), NSMaxY(bounds)) withScale:stroke transform:trafo view:view];
		[self drawHandleAtPoint:NSMakePoint(NSMaxX(bounds), NSMinY(bounds)) withScale:stroke transform:trafo view:view];
		[self drawHandleAtPoint:NSMakePoint(NSMaxX(bounds), NSMaxY(bounds)) withScale:stroke transform:trafo view:view];
	}
	// Let our ROI superclass do the drawing of the note icon
	[super drawNoteIconWithScale:stroke transform:trafo view:view];
}


- (NSBezierPath *)pathWithScale:(float)scale;
{
	NSRect bounds = [self boundsWithScale:scale];
	NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:bounds];
	return path;
}



- (NSRect)boundsWithScale:(float)scale;
{
	float minX = MIN([self startPoint].x, [self endPoint].x) * scale;
	float minY = MIN([self startPoint].y, [self endPoint].y) * scale;
	float xSize = fabs([self startPoint].x - [self endPoint].x) * scale;
	float ySize = fabs([self startPoint].y - [self endPoint].y) * scale;
	return NSMakeRect(minX, minY, xSize, ySize);
}



- (int)handleUnderPoint:(NSPoint)point;
{
	NSRect bounds = [self boundsWithScale:1.0];
	
	NSPoint lowerLeftPoint = NSMakePoint(NSMinX(bounds), NSMinY(bounds));
	NSPoint upperLeftPoint = NSMakePoint(NSMinX(bounds), NSMaxY(bounds));
	NSPoint lowerRightPoint = NSMakePoint(NSMaxX(bounds), NSMinY(bounds));
	NSPoint upperRightPoint = NSMakePoint(NSMaxX(bounds), NSMaxY(bounds));
	
	NSRect rectForLowerLeftPoint = [self hitRectForHandleAtPoint:lowerLeftPoint withScale:1.0];
	NSRect rectForUpperLeftPoint = [self hitRectForHandleAtPoint:upperLeftPoint withScale:1.0];
	NSRect rectForLowerRightPoint = [self hitRectForHandleAtPoint:lowerRightPoint withScale:1.0];
	NSRect rectForUpperRightPoint = [self hitRectForHandleAtPoint:upperRightPoint withScale:1.0];
	
	if (NSPointInRect(point, rectForLowerLeftPoint)) {
		return 1;
	}
	else if (NSPointInRect(point, rectForUpperLeftPoint)) {
		return 2;
	}
	else if (NSPointInRect(point, rectForLowerRightPoint)) {
		return 3;
	}
	else if (NSPointInRect(point, rectForUpperRightPoint)) {
		return 4;
	}
	return 0;
}


- (void)moveWithDelta:(NSPoint)deltaPoint;
{
	NSPoint newStartPoint = NSMakePoint([self startPoint].x + deltaPoint.x, [self startPoint].y + deltaPoint.y);
	NSPoint newEndPoint = NSMakePoint([self endPoint].x + deltaPoint.x, [self endPoint].y + deltaPoint.y);
	[self setStartPoint:newStartPoint endPoint:newEndPoint];
}


- (BOOL)containsPoint:(NSPoint)point;
{
	float minX = MIN([self startPoint].x, [self endPoint].x);
	float minY = MIN([self startPoint].y, [self endPoint].y);
	float xSize = fabs([self startPoint].x - [self endPoint].x);
	float ySize = fabs([self startPoint].y - [self endPoint].y);
	NSRect rect = NSMakeRect(minX, minY, xSize, ySize);
	
	return NSPointInRect(point, rect);
}


- (NSRect)noteIconRect;
{
	NSRect rect = [self boundsWithScale:1.0];
	NSPoint lowerLeftPoint = NSMakePoint(NSMinX(rect), NSMinY(rect));
	return NSMakeRect(lowerLeftPoint.x + 5, lowerLeftPoint.y + 5, 16, 16);
}


- (NSAttributedString *)pixelSummary;
{
	NSString *theString = [NSString stringWithFormat:@"%C %d px\n%C %d px", (unsigned short)0x25CF, [[self valueForKey:@"pixelArea"]intValue], (unsigned short)0x25EF, [[self valueForKey:@"pixelPerimeter"]intValue]];
	NSMutableAttributedString *aString = [[[NSMutableAttributedString alloc] initWithString:theString] autorelease];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:12], NSFontAttributeName, [self summaryColor], NSForegroundColorAttributeName, nil];
	// Add the attributes to the first character and to the first character after the line break
	[aString addAttributes:dict range:NSMakeRange(0, 1)];
	NSRange range = [theString rangeOfString:@"\n"];
	[aString addAttributes:dict range:NSMakeRange(range.location+1, 1)];
	return aString;
}



- (NSString *)summary;
{
	NSString *theString;
	float area = [[self valueForKey:@"area"]floatValue];
	if (area < 100000000000000.0 && area > 0.000000001) {
		theString = [NSString stringWithFormat:@"%2.2f %@%C\n%2.2f %@", area, [self valueForKeyPath:@"image.calibration.unit"], (unsigned short)0x00B2, [[self valueForKey:@"perimeter"]floatValue], [self valueForKeyPath:@"image.calibration.unit"]];
	}
	else {
		theString = @"Not calibrated";
	}
	return theString;
}





@end
