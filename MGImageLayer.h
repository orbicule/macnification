//
//  MGImageLayer.h
//  Filament
//
//  Created by Dennis Lorson on 06/09/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@interface MGImageLayer : CAOpenGLLayer
{
	
	CIImage *_image;
	CIContext *_ciContext;

}

@property (nonatomic, retain) CIImage *image;

- (NSInteger)maximumTextureSize;

@end
