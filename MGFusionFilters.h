//
//  MGFusionFilters.h
//  Filament
//
//  Created by Dennis Lorson on 09/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <QuartzCore/QuartzCore.h>

@interface SharedFilterMethods : NSObject { }

+ (void)loadKernelsIfNeeded;
+ (NSArray *)extentOptionWithRect:(CGRect)rect;

@end


@interface MGFusionFilter : CIFilter
{
	CIImage *inputImage;
}

@end


@interface MGFusionPOTFilter : MGFusionFilter 
{
}

- (CGSize)paddedSizeForImage:(CIImage *)img;

@end


@interface MGFusionPadFilter : MGFusionFilter 
{
	NSNumber *inputPadAmount;
}
@end

@interface MGFusionKRGBFilter : MGFusionFilter {}
@end

@interface MGFusionNormalizeAlphaFilter : MGFusionFilter {}
@end

@interface MGFusionRedToGrayscaleFilter : MGFusionFilter {}
@end

@interface MGFusionConvolveUpFilter : MGFusionFilter {}
@end

@interface MGFusionConvolveDownFilter : MGFusionFilter {}
@end

@interface MGFusionSMLFilter : MGFusionFilter {}
@end

@interface MGFusionSmoothFilter : MGFusionFilter {}
@end

@interface MGFusionDecimateFilter : MGFusionFilter {}
@end

@interface MGFusionAddFilter : MGFusionFilter 
{
	CIImage *inputImage2;
}
@end

@interface MGFusionSubtractFilter : MGFusionFilter 
{
	CIImage *inputImage2;
}
@end

@interface MGFusionMergeFilter : MGFusionFilter 
{
	CIImage *inputImage2;
}
@end

@interface MGFusionSubsampleFilter : MGFusionFilter {}
@end

@interface MGFusionSupersampleFilter : MGFusionFilter {}
@end



