//
//  MGHUDRangeSlider.h
//  Filament
//
//  Created by Dennis Lorson on 06/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@class MGWidgetAttachedWindow;

typedef enum _MGSliderKnob {
	
	MGUpperBoundSliderKnob = 1,
	MGLowerBoundSliderKnob = 2,
	MGNoSliderKnob = 3,
	
} MGSliderKnob;


@interface MGHUDRangeSlider : NSControl {
	
	CGFloat lowerBound, upperBound;
	CGFloat _minValue, _maxValue;
	
	id target;
	SEL selector;
	SEL mouseOverSelector;
	SEL mouseUpSelector;
	
	MGSliderKnob activeSliderKnob;
	
	NSImage *trackImageL, *trackImageR, *trackImageM;
	NSImage *knob;
	
	BOOL hasMouseOver;
	
	BOOL isDragging;
	BOOL suppressedExitEvent;
	
	CGFloat rangeOverlayOpacity;
	
	NSTextField *tooltipTextField;
	
	MGWidgetAttachedWindow *tooltipWindow;
	
	NSGradient *colorOverlay;
}

@property(readwrite) BOOL hasMouseOver;


- (void)setMouseOverAction:(SEL)action;
- (void)setMouseUpAction:(SEL)action;

- (void)drawTrack;
- (void)drawLowerBoundSlider;
- (void)drawUpperBoundSlider;
- (NSRect)trackRect;

- (void)setColorOverlay:(NSGradient *)overlay;

- (CGFloat)rangeOverlayOpacity;
- (void)setRangeOverlayOpacity:(CGFloat)opacity;
- (void)setRangeOverlayActive:(BOOL)active;

- (CGFloat)lowerBound;
- (CGFloat)upperBound;
- (void)setLowerBound:(CGFloat)bound;
- (void)setUpperBound:(CGFloat)bound;

- (CGFloat)maxValue;
- (CGFloat)minValue;
- (void)setMaxValue:(CGFloat)value;
- (void)setMinValue:(CGFloat)value;

- (CGFloat)percentageForLocationInBounds:(NSPoint)location;
- (CGFloat)valueForLocationInBounds:(NSPoint)location;

- (CGFloat)locationInBoundsForPercentage:(CGFloat)percentage;
- (CGFloat)locationInBoundsForValue:(CGFloat)value;

- (void)showTooltipWindowForKnob:(MGSliderKnob)aKnob animate:(BOOL)fadeIn;
- (void)closeToolTip;


@end
