//
//  StackImageLayer.m
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "StackImageLayer.h"
#import "Image.h"
#import "MGFunctionAdditions.h"

@implementation StackImageLayer


@synthesize stackImage, contentsVisible;





- (NSSize)dimensionsForImageAtPath:(NSString *)path
{
	if (!path) return NSZeroSize;
	
	return MGGetDimensionsForImage(path);
}


- (CGImageRef)image;
{
	CGImageRef orig = [(Image *)[self.stackImage valueForKeyPath:@"image"] filteredImageRef];
	
	CGColorSpaceRef colorspace = nil;
	
	if (CGColorSpaceGetNumberOfComponents(CGImageGetColorSpace(orig)) == 3)
		colorspace = CGColorSpaceCreateDeviceRGB();
	else
		colorspace = CGColorSpaceCreateDeviceGray();
		
	CGImageRef matched = CGImageCreateCopyWithColorSpace(orig, colorspace);
		
	[(id)matched autorelease];
	
	return matched;
}


- (CGImageRef)reflectionWithImage:(CGImageRef)original heightFraction:(CGFloat)frac
{	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	
	NSInteger width = CGImageGetWidth(original);
	NSInteger height = roundf(CGImageGetHeight(original) * frac);
	
	size_t bytesPerRow = 4 * width;
	
	unsigned char *bitmapData = (unsigned char *)malloc(bytesPerRow * height);
	
	CGContextRef context = CGBitmapContextCreate(bitmapData, width, height, 8, bytesPerRow, colorspace, kCGImageAlphaPremultipliedFirst);
	
	CGImageRef partOfOriginal = CGImageCreateWithImageInRect(original, CGRectMake(0, CGImageGetHeight(original) * (1.0 - frac), width, height));
	
	// flip the image
	CGContextScaleCTM(context, 1.0, -1.0);
	CGContextTranslateCTM (context, 0, -height);
	
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), partOfOriginal);
	
	CGImageRelease(partOfOriginal);
	
	// gradient
	const CGFloat components[] = {255, 255, 255, 1.0,
	255, 255, 255, 0.6};
	const CGFloat locations[] = {0, 1};
	
	CGGradientRef grad = CGGradientCreateWithColorComponents(colorspace, components, locations, 2);
	
	// blending mode == dest out -> 100% alpha mask will make completely transparent, 0% will retain the masked image alpha.
	CGContextSetBlendMode(context, kCGBlendModeDestinationOut);
	CGContextDrawLinearGradient(context, grad, CGPointMake(0, height), CGPointMake(0, 0), (CGGradientDrawingOptions)0);
	
	CGImageRef result = CGBitmapContextCreateImage(context);
	
	CGContextRelease(context);
	CGColorSpaceRelease(colorspace);
	CGGradientRelease(grad);
	free(bitmapData);
	
	return (CGImageRef)[(id)result autorelease];
	
	
}


- (void) dealloc
{
	
	[super dealloc];
}


@end
