//
//  MGImportMetadata.m
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGMetadataSet.h"
#import "MGAppDelegate.h"
#import "Image.h"


@interface MGMetadataSet ()

- (NSSet *)_createChannels;
- (NSManagedObject *)_createInstrument;
- (NSManagedObject *)_createCalibration;
- (NSManagedObject *)_createExperimenter;

- (NSManagedObjectContext *)_managedObjectContext;

- (NSArray *)_sortedPlanes;



@end



@implementation MGMetadataSet

- (id) init
{
	self = [super init];
	if (self != nil) {
		metadata_ = [[NSMutableDictionary dictionary] retain];
		
	}
	return self;
}

- (void) dealloc
{
	[metadata_ release];
	[sortedPlanes_ release];
	
	[super dealloc];
}

- (MGMetadataSet *)clone
{
	MGMetadataSet *copy = [[[MGMetadataSet alloc] init] autorelease];
	for (NSString *key in [metadata_ allKeys])
		[copy setValue:[metadata_ objectForKey:key] forKey:key];
	
	return copy;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if (value)
        [metadata_ setObject:value forKey:key];
    
    else
        NSLog(@"MGMetadataSet error: tried to insert nil value for key %@", key);
}

- (id)valueForKey:(NSString *)key
{
	return [metadata_ objectForKey:key];
}

- (void)removeValueForKey:(NSString *)key;
{
	[metadata_ removeObjectForKey:key];
}


#pragma mark -
#pragma mark Determining channels for representations

- (int)numberOfChannels
{
	id obj = [self valueForKey:MGMetadataNumberOfChannelsKey];
	if (obj)
		return [obj intValue];
	else
		return 1;
}

- (int)channelForRepresentationAtIndex:(int)index
{
	NSArray *planes = [metadata_ objectForKey:MGMetadataImagePlanesKey];
	
	if (index > [planes count] - 1)
		return -1;
	
	if (![[planes objectAtIndex:index] objectForKey:MGMetadataImagePlaneCKey])
		return -1;
	
	return [[[planes objectAtIndex:index] objectForKey:MGMetadataImagePlaneCKey] intValue];
					   
}

- (NSArray *)_sortedPlanes
{
	if (sortedPlanes_)
		return sortedPlanes_;
	
	NSArray *planes = [metadata_ objectForKey:MGMetadataImagePlanesKey];
	
	NSArray *sortDescriptors = [NSArray arrayWithObjects:
								[NSSortDescriptor sortDescriptorWithKey:MGMetadataImagePlaneCKey ascending:YES],
								[NSSortDescriptor sortDescriptorWithKey:MGMetadataImagePlaneZKey ascending:YES],
								[NSSortDescriptor sortDescriptorWithKey:MGMetadataImagePlaneTKey ascending:YES],
								nil];
	
	sortedPlanes_ = [[planes sortedArrayUsingDescriptors:sortDescriptors] retain];
	
	return sortedPlanes_;
}

#pragma mark -
#pragma mark Application

- (void)applyToImage:(Image *)image;
{	
	// INSTRUMENT
	
	id oldInstrument = [image valueForKey:@"instrument"];
	if (oldInstrument && [[oldInstrument valueForKey:@"images"] count] == 1)
		[image setValue:nil forKey:@"instrument"];
	[[oldInstrument managedObjectContext] deleteObject:oldInstrument];
	
	NSManagedObject *instrument = [self _createInstrument];
	
	[image setValue:instrument forKey:@"instrument"];
	
	
	// EXPERIMENTER
	
	id experimenter = [self _createExperimenter];
	
	if (experimenter)
		[image setValue:experimenter forKey:@"experimenter"];
	
	
	// CALIBRATION
	id calibration = [self _createCalibration];
	
	if (calibration)
		[image setValue:calibration forKey:@"calibration"];
	
	
	// PROPERTIES
	if ([metadata_ objectForKey:MGMetadataImageImportDateKey])
		[image setValue:[metadata_ objectForKey:MGMetadataImageImportDateKey] forKey:@"importDate"];
	
	if ([metadata_ objectForKey:MGMetadataImageCreationDateKey])
		[image setValue:[metadata_ objectForKey:MGMetadataImageCreationDateKey] forKey:@"creationDate"];
	
	if ([metadata_ objectForKey:MGMetadataImageCommentKey])
		[image setValue:[metadata_ objectForKey:MGMetadataImageCommentKey] forKey:@"comment"];
	
	if ([metadata_ objectForKey:MGMetadataImageNameKey])
		[image setValue:[metadata_ objectForKey:MGMetadataImageNameKey] forKey:@"name"];
	
}

- (void)applyToImages:(NSArray *)images;
{
	for (Image *image in images)
		[self applyToImage:image];

	
	int count = [images count];

	// override: image name (sequenced)
	NSNumberFormatter *formatter = [[[NSNumberFormatter alloc] init] autorelease];
	
	if (count <= 10)
		[formatter setMinimumIntegerDigits:1];
	else if (count <= 100)
		[formatter setMinimumIntegerDigits:2];
	else if (count <= 1000)
		[formatter setMinimumIntegerDigits:3];
	else if (count <= 10000)
		[formatter setMinimumIntegerDigits:4];
	else
		[formatter setMinimumIntegerDigits:5];
    	
	NSString *baseName = [metadata_ objectForKey:MGMetadataImageNameKey];
	
	for (int i = 0; i < count; i++) {
		
		NSString *indexString = [formatter stringFromNumber:[NSNumber numberWithInt:i]];
		NSString *imageName = (count > 1) ? [NSString stringWithFormat:@"%@ - %@", baseName, indexString] : baseName;
		[[images objectAtIndex:i] setValue:imageName forKey:@"name"];

		
	}
}


- (NSSet *)_createChannels
{
	NSMutableSet *channels = [NSMutableSet set];

	NSArray *channelMetadata = [metadata_ objectForKey:MGMetadataChannelsKey];
	
	// make sure the channel names are unique
	NSMutableArray *processedChannelNames = [NSMutableArray array];
	
	for (NSDictionary *channelDict in channelMetadata) {
		
		id channel = [NSEntityDescription insertNewObjectForEntityForName:@"Channel" inManagedObjectContext:[self _managedObjectContext]];
		
		if ([channelDict objectForKey:MGMetadataChannelNameKey]) {
			NSString *name = [channelDict objectForKey:MGMetadataChannelNameKey];
			
			NSString *adjustedName = name;
			
			int i = 2;
			while ([processedChannelNames containsObject:adjustedName]) {
				adjustedName = [NSString stringWithFormat:@"%@ (%i)", name, i];
				i++;
			}
			
			[channel setValue:adjustedName forKey:MGMetadataChannelNameKey];
			[processedChannelNames addObject:adjustedName];
			
		}
		
		if ([channelDict objectForKey:MGMetadataChannelWavelengthKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelWavelengthKey] forKey:MGMetadataChannelWavelengthKey];
		if ([channelDict objectForKey:MGMetadataChannelPinholeSizeKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelPinholeSizeKey] forKey:MGMetadataChannelPinholeSizeKey];
		if ([channelDict objectForKey:MGMetadataChannelContrastMethodKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelContrastMethodKey] forKey:MGMetadataChannelContrastMethodKey];
		if ([channelDict objectForKey:MGMetadataChannelModeKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelModeKey] forKey:MGMetadataChannelModeKey];
		if ([channelDict objectForKey:MGMetadataChannelLightSourceNameKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelLightSourceNameKey] forKey:MGMetadataChannelLightSourceNameKey];
		if ([channelDict objectForKey:MGMetadataChannelLightSourceTypeKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelLightSourceTypeKey] forKey:MGMetadataChannelLightSourceTypeKey];
		if ([channelDict objectForKey:MGMetadataChannelMediumKey])
			[channel setValue:[channelDict objectForKey:MGMetadataChannelMediumKey] forKey:MGMetadataChannelMediumKey];
		
		[channels addObject:channel];
		
	}

	
	return channels;
	
}


- (NSManagedObject *)_createInstrument
{
	// always create an instrument!
	
	id instrument = [NSEntityDescription insertNewObjectForEntityForName:@"Instrument" inManagedObjectContext:[self _managedObjectContext]];
	
	if ([metadata_ objectForKey:MGMetadataInstrumentNameKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentNameKey] forKey:MGMetadataInstrumentNameKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentObjectiveNameKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentObjectiveNameKey] forKey:MGMetadataInstrumentObjectiveNameKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentWorkingDistanceKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentWorkingDistanceKey] forKey:MGMetadataInstrumentWorkingDistanceKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentImmersionKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentImmersionKey] forKey:MGMetadataInstrumentImmersionKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentNumericApertureKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentNumericApertureKey] forKey:MGMetadataInstrumentNumericApertureKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentMagnificationKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentMagnificationKey] forKey:MGMetadataInstrumentMagnificationKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentVoltageKey])
		[instrument setValue:[NSNumber numberWithFloat:[[metadata_ objectForKey:MGMetadataInstrumentVoltageKey] floatValue] * 0.001] forKey:MGMetadataInstrumentVoltageKey];
	if ([metadata_ objectForKey:MGMetadataInstrumentSpotSizeKey])
		[instrument setValue:[metadata_ objectForKey:MGMetadataInstrumentSpotSizeKey] forKey:MGMetadataInstrumentSpotSizeKey];
	
	
	NSSet *channels = [self _createChannels];
	
	
	NSSet *oldChannels = [instrument valueForKey:@"channels"];
	if ([oldChannels count])
		[instrument setValue:[NSSet set] forKey:@"channels"];
	
	for (id channel in oldChannels)
		[[instrument managedObjectContext] deleteObject:channel];
	
	[instrument setValue:channels forKey:@"channels"];
	
	
	return instrument;

}


- (NSManagedObject *)_createExperimenter
{
	id experimenter = nil;
	
	NSString *experimenterName = nil;
	
	if ((experimenterName = [metadata_ objectForKey:MGMetadataExperimenterNameKey])) {
		experimenter = [NSEntityDescription insertNewObjectForEntityForName:@"Experimenter" inManagedObjectContext:[self _managedObjectContext]];
		[experimenter setValue:experimenterName forKey:@"name"];
	}
	
	return experimenter;
}

- (NSManagedObject *)_createCalibration
{
	NSManagedObject *calibration = nil;
	
	if ([metadata_ objectForKey:MGMetadataImagePixelSizeXKey]) {
		calibration = [NSEntityDescription insertNewObjectForEntityForName:@"Calibration" inManagedObjectContext:[self _managedObjectContext]];
		double pixelSize = [[metadata_ objectForKey:MGMetadataImagePixelSizeXKey] doubleValue];
		double pixelsPerUnit = 1.00 / pixelSize;
		[calibration setValue:[NSNumber numberWithDouble:pixelsPerUnit] forKey:MGMetadataCalibrationHorizontalPixelsForUnitKey];
		[calibration setValue:@"µm" forKey:MGMetadataCalibrationUnitKey];
	}
	
	
	else if ([metadata_ objectForKey:MGMetadataImagePhysicalSizeXKey] && [metadata_ objectForKey:MGMetadataCalibrationUnitKey]) {
		
		// physicalSizeX: size of the X dimension (in um?)
		// sizeX: # pixels of the X dimension
		
		float physicalSizeX = [[metadata_ objectForKey:MGMetadataImagePhysicalSizeXKey] floatValue];
		
		double pixelsPerUnit = (float)1.0 / physicalSizeX;
		
		calibration = [NSEntityDescription insertNewObjectForEntityForName:@"Calibration" inManagedObjectContext:[self _managedObjectContext]];
		[calibration setValue:[NSNumber numberWithDouble:pixelsPerUnit] forKey:MGMetadataCalibrationHorizontalPixelsForUnitKey];
		[calibration setValue:[metadata_ objectForKey:MGMetadataCalibrationUnitKey] forKey:MGMetadataCalibrationUnitKey];
		
	}
	
	else if ([metadata_ objectForKey:MGMetadataCalibrationHorizontalPixelsForUnitKey] && [metadata_ objectForKey:MGMetadataCalibrationUnitKey]) {
		calibration = [NSEntityDescription insertNewObjectForEntityForName:@"Calibration" inManagedObjectContext:[self _managedObjectContext]];
		[calibration setValue:[metadata_ objectForKey:MGMetadataCalibrationUnitKey] forKey:MGMetadataCalibrationUnitKey];
		[calibration setValue:[metadata_ objectForKey:MGMetadataCalibrationHorizontalPixelsForUnitKey] forKey:MGMetadataCalibrationHorizontalPixelsForUnitKey];

	}
	
	
	
	else if ([metadata_ objectForKey:MGMetadataCalibrationPixelWidthKey] || [metadata_ objectForKey:MGMetadataCalibrationPixelHeightKey]) {
		calibration = [NSEntityDescription insertNewObjectForEntityForName:@"Calibration" inManagedObjectContext:[self _managedObjectContext]];
		double pixelWidth = [[metadata_ objectForKey:MGMetadataCalibrationPixelWidthKey] doubleValue];
		double pixelHeight = [[metadata_ objectForKey:MGMetadataCalibrationPixelHeightKey] doubleValue];
		
		[calibration setValue:[NSNumber numberWithDouble:1.00/pixelWidth] forKey:@"horizontalPixelsForUnit"];
		[calibration setValue:[NSNumber numberWithDouble:1.00/pixelHeight] forKey:@"verticalPixelsForUnit"];
		
		NSString *unit;
		if ((unit = [metadata_ objectForKey:MGMetadataCalibrationUnitKey])) {
			
			if ([unit isEqualToString:@"um"] || [unit isEqualToString:@"UM"])
				unit = @"µm";
			
			[calibration setValue:unit forKey:MGMetadataCalibrationUnitKey];
			
		} else {
			[calibration setValue:@"µm" forKey:MGMetadataCalibrationUnitKey];
		}
				
		
	}
	
	
	return calibration;
}

- (NSManagedObjectContext *)_managedObjectContext
{
	return [MGLibraryControllerInstance managedObjectContext];
}

@end




// --------------------------------------------------------------------------------------------------------------------------------


// Image

NSString * const MGMetadataImageNameKey = @"name";
NSString * const MGMetadataImageImportDateKey = @"importDate";
NSString * const MGMetadataImageCreationDateKey = @"creationDate";
NSString * const MGMetadataImagePixelSizeXKey = @"pixelSizeX";
NSString * const MGMetadataImagePhysicalSizeXKey = @"physicalSizeX";
NSString * const MGMetadataImageSizeXKey = @"sizeX";
NSString * const MGMetadataImageCommentKey = @"comment";
NSString * const MGMetadataImageSuggestedScaleBarLengthKey = @"suggestedScaleBarLength";

NSString * const MGMetadataImageIntensityRangeMinKey = @"intensityRangeMin";
NSString * const MGMetadataImageIntensityRangeMaxKey = @"intensityRangeMax";

NSString * const MGMetadataImageLittleEndianKey = @"littleEndian";
NSString * const MGMetadataImageTIFFLittleEndianKey = @"tiffLittleEndian";

// Planes

NSString * const MGMetadataImagePlanesKey = @"planes";
NSString * const MGMetadataImagePlaneCKey = @"C";					
NSString * const MGMetadataImagePlaneTKey = @"T";
NSString * const MGMetadataImagePlaneZKey = @"Z";


// Instrument

NSString * const MGMetadataInstrumentNameKey = @"instrumentName";
NSString * const MGMetadataInstrumentObjectiveNameKey = @"objectiveName";
NSString * const MGMetadataInstrumentWorkingDistanceKey = @"workingDistance";
NSString * const MGMetadataInstrumentImmersionKey = @"immersion";
NSString * const MGMetadataInstrumentNumericApertureKey = @"numericAperture";
NSString * const MGMetadataInstrumentMagnificationKey = @"magnification";
NSString * const MGMetadataInstrumentVoltageKey = @"voltage";
NSString * const MGMetadataInstrumentSpotSizeKey = @"spotSize";


// Channel

NSString * const MGMetadataChannelsKey = @"channels";
NSString * const MGMetadataNumberOfChannelsKey = @"numberOfChannels";

NSString * const MGMetadataChannelNameKey = @"channelName";
NSString * const MGMetadataChannelWavelengthKey = @"waveLength";
NSString * const MGMetadataChannelPinholeSizeKey = @"pinholeSize";
NSString * const MGMetadataChannelContrastMethodKey = @"contrastMethod";
NSString * const MGMetadataChannelModeKey = @"mode";
NSString * const MGMetadataChannelLightSourceNameKey = @"lightSourceName";
NSString * const MGMetadataChannelLightSourceTypeKey = @"lightSourceType";
NSString * const MGMetadataChannelMediumKey = @"medium";


// Calibration

NSString * const MGMetadataCalibrationHorizontalPixelsForUnitKey = @"horizontalPixelsForUnit";
NSString * const MGMetadataCalibrationUnitKey = @"unit";
NSString * const MGMetadataCalibrationPixelWidthKey = @"calibrationPixelWidth";
NSString * const MGMetadataCalibrationPixelHeightKey = @"calibrationPixelHeight";


// Experimenter

NSString * const MGMetadataExperimenterNameKey = @"experimenterName";



