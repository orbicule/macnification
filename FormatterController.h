//
//  FormatterController.h
//  Filament
//
//  Created by Dennis Lorson on 21/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FormatterController : NSObject {
	
	IBOutlet NSNumberFormatter	*workingDistanceFormatter,
								*voltageFormatter,
								*magnificationFormatter,
								*spotSizeFormatter,
								*suggestedScalebarLengthFormatter;
	
	
	IBOutlet NSDateFormatter *standardDateFormatter;
	

}


+ (FormatterController *)sharedFormatterController;

- (NSString *)formattedStringForValue:(id)value ofKeyPath:(NSString *)keyPath;


@end
