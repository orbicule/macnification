//
//  ScaleBar.m
//  Filament
//
//  Created by Peter Schols on 04/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "ScaleBar.h"
#import "Image.h"


@implementation ScaleBar


# pragma mark -
# pragma mark Setup


// Setup a new scale bar based on an NSDictionary
- (void)setupWithProperties:(NSDictionary *)properties;
{
	// Set our properties as defined in the properties NSDictionary
	[self setValue:[properties objectForKey:@"color"] forKey:@"color"];
	[self setValue:[properties objectForKey:@"fontSize"] forKey:@"fontSize"];
	[self setValue:[properties objectForKey:@"fontName"] forKey:@"fontName"];
	[self setValue:[properties objectForKey:@"stroke"] forKey:@"stroke"];
	[self setValue:[properties objectForKey:@"style"] forKey:@"style"];
	[self setValue:[properties objectForKey:@"showsLabel"] forKey:@"showsLabel"];
	[self setValue:[NSNumber numberWithBool:YES] forKey:@"isVisible"];
	[self setValue:[properties objectForKey:@"restrictToStandardSizes"] forKey:@"restrictToStandardSizes"];
	[self setValue:[properties objectForKey:@"useBackgroundBox"] forKey:@"useBackgroundBox"];
	[self setValue:[properties objectForKey:@"backgroundBoxColor"] forKey:@"backgroundBoxColor"];

	// Set the x and y proportion, keeping in mind the image's size
	[self setValue:[properties objectForKey:@"xOrigin"] forKey:@"xProportion"];
	[self setValue:[properties objectForKey:@"yOrigin"] forKey:@"yProportion"];
	
	[self recalculateLength];
}


- (void)recalculateLength
{
	// Set the length. It's either the suggested scale bar length, or we come up with a reasonable size ourselves
	float suggestedScaleBarLength = [[self valueForKeyPath:@"image.suggestedScaleBarLength"] floatValue];
	if (suggestedScaleBarLength > 0.00 && ![[self valueForKey:@"restrictToStandardSizes"] boolValue]) {
		[self setValue:[NSNumber numberWithFloat:suggestedScaleBarLength] forKey:@"length"];
	}
	else {
		[self setValue:[self calculatedScaleBarLength] forKey:@"length"];
	}
}


- (NSNumber *)calculatedScaleBarLength;
{
	int nicelyRoundedLength = 1;
	float horizontalPixelsPerUnit = [[self valueForKeyPath:@"image.calibration.horizontalPixelsForUnit"]floatValue];
	NSSize imageSize = [(Image *)[self valueForKey:@"image"] pixelSize];
	
	float horizontalUnits = imageSize.width / horizontalPixelsPerUnit;
	float proposedLength = horizontalUnits / 6.0;
	proposedLength = roundf(proposedLength);
	if (proposedLength == 0.0)
		proposedLength = 1.0;
	
	if ([[self valueForKey:@"restrictToStandardSizes"] boolValue]) {
		
		if (proposedLength < 1.5)
			proposedLength = 1;
		else if (proposedLength < 3.5)
			proposedLength = 2;
		else if (proposedLength < 7.5)
			proposedLength = 5;
		else if (proposedLength < 15)
			proposedLength = 10;
		else if (proposedLength < 35)
			proposedLength = 20;
		else if (proposedLength < 75)
			proposedLength = 50;
		else if (proposedLength < 150)
			proposedLength = 100;
		else if (proposedLength < 350)
			proposedLength = 200;
		else if (proposedLength < 750)
			proposedLength = 500;
		else if (proposedLength < 1500)
			proposedLength = 1000;
		else if (proposedLength < 3500)
			proposedLength = 2000;
		else if (proposedLength < 7500)
			proposedLength = 5000;
		else if (proposedLength < 15000)
			proposedLength = 10000;
		else
			;
		
	}
	
	if (proposedLength > 10 && proposedLength < 150) 
		nicelyRoundedLength = [self increaseInteger:(int)proposedLength toNearestX:10];

	else if (proposedLength > 150 && proposedLength < 1000)
		nicelyRoundedLength = [self increaseInteger:(int)proposedLength toNearestX:100];

	else if (proposedLength >= 1000)
		nicelyRoundedLength = [self increaseInteger:(int)proposedLength toNearestX:1000];

	else
		nicelyRoundedLength = proposedLength;
	
		
	return [NSNumber numberWithInt:nicelyRoundedLength];
}



- (NSUInteger)increaseInteger:(NSUInteger)number toNearestX:(NSUInteger)x;
{
	while (number % x)
		number++;
	
	return number;
}



# pragma mark -
# pragma mark Drawing


- (void)draw;
{
	// Get the image size
	NSSize imageSize = [[self valueForKey:@"image"] pixelSize];
	
	// Convert our properties to primitive types
	float xOriginOnImage = [[self valueForKey:@"xProportion"]floatValue] * imageSize.width;
	float yOriginOnImage = [[self valueForKey:@"yProportion"]floatValue] * imageSize.height;
	float strokeOnImage = [[self valueForKey:@"stroke"] floatValue] / 100.0 * imageSize.height;
	float lengthOnImage = [[self valueForKey:@"length"]floatValue] * [[self valueForKeyPath:@"image.calibration.horizontalPixelsForUnit"]floatValue];
	NSString *scaleBarStyle = [self valueForKey:@"style"];
	BOOL drawsLabel = [[self valueForKey:@"showsLabel"]boolValue];
	NSString *fontName = [self valueForKey:@"fontName"];
	float fontSize = [[self valueForKey:@"fontSize"] floatValue] * 0.0015625 * imageSize.width;
	NSColor *calibratedColor = [[self color] colorUsingColorSpaceName:NSDeviceRGBColorSpace];
		
	/*	
	if ([[self valueForKeyPath:@"image.calibration.unit"] hasSuffix:@"mm"]) {
		lengthOnImage = lengthOnImage * 1000.00;	
	}
	*/
	
	
	NSRect scaleBarRect = NSMakeRect(xOriginOnImage - (lengthOnImage / 2.0), yOriginOnImage, lengthOnImage, strokeOnImage/2);
	    
    
    NSRect scaleBarBackgroundRect = scaleBarRect;
	
	
	if (drawsLabel) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSFont fontWithName:fontName size:fontSize], NSFontAttributeName, [self color], NSForegroundColorAttributeName, nil];
		NSString *string = [NSString stringWithFormat:@"%@ %@", [self valueForKey:@"length"], [self valueForKeyPath:@"image.calibration.unit"]];
		NSSize stringSize = [string sizeWithAttributes:dict];
		NSRect labelRect = NSMakeRect(xOriginOnImage - (stringSize.width/2), yOriginOnImage - stringSize.height, stringSize.width, stringSize.height);
		scaleBarBackgroundRect = NSUnionRect(scaleBarBackgroundRect, labelRect);
	}
	
	// draw the background
	if ([[self valueForKey:@"useBackgroundBox"] boolValue]) {
		
		scaleBarBackgroundRect = NSInsetRect(scaleBarBackgroundRect, -5, -5);
		
		NSColor *bgColor = [NSUnarchiver unarchiveObjectWithData:[self valueForKey:@"backgroundBoxColor"]];
		[bgColor set];
		[NSBezierPath fillRect:scaleBarBackgroundRect];
		
	}
	
	
	// Draw the scale bar
	[calibratedColor set];
	[NSBezierPath fillRect:scaleBarRect];
	
	//NSLog(@"%@", NSStringFromRect(scaleBarRect));
	
	if ([scaleBarStyle isEqualTo:@"Zebra"]) {
		if ([calibratedColor redComponent] < 0.2 && [calibratedColor greenComponent] < 0.2 && [calibratedColor blueComponent] < 0.2) {
			[[NSColor whiteColor] set];
		}
		else {
			[[NSColor blackColor] set];
		}
		NSRect zebraRect = NSOffsetRect(scaleBarRect, 0.0, strokeOnImage/2.0);
		[NSBezierPath fillRect:zebraRect];
	}
	
	// Draw the label
	if (drawsLabel) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSFont fontWithName:fontName size:fontSize], NSFontAttributeName, [self color], NSForegroundColorAttributeName, nil];
		NSString *string = [NSString stringWithFormat:@"%@ %@", [self valueForKey:@"length"], [self valueForKeyPath:@"image.calibration.unit"]];
		NSSize stringSize = [string sizeWithAttributes:dict];
		[string drawAtPoint:NSMakePoint(xOriginOnImage - (stringSize.width/2), yOriginOnImage - stringSize.height) withAttributes:dict];
	}	
}




# pragma mark -
# pragma mark Hit Testing


- (BOOL)containsPoint:(NSPoint)point;
{
	// Get the image size
	NSSize imageSize = [[self valueForKey:@"image"] pixelSize];
	
	float xOriginOnImage = [[self valueForKey:@"xProportion"]floatValue] * imageSize.width;
	float yOriginOnImage = [[self valueForKey:@"yProportion"]floatValue] * imageSize.height;
	float strokeOnImage = [[self valueForKey:@"stroke"] floatValue] / 100.0 * imageSize.height;
	float lengthOnImage = [[self valueForKey:@"length"]floatValue] * [[self valueForKeyPath:@"image.calibration.horizontalPixelsForUnit"]floatValue];
	BOOL drawsLabel = [[self valueForKey:@"showsLabel"]boolValue];
	NSString *fontName = [self valueForKey:@"fontName"];
	float fontSize = [[self valueForKey:@"fontSize"] floatValue] * 0.0015625 * imageSize.width;
	
	
	NSRect scaleBarRect = NSMakeRect(xOriginOnImage - (lengthOnImage / 2.0), yOriginOnImage, lengthOnImage, strokeOnImage);
	NSRect scaleBarBackgroundRect = scaleBarRect;
	NSRect labelRect = NSZeroRect;

	if (drawsLabel) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSFont fontWithName:fontName size:fontSize], NSFontAttributeName, [self color], NSForegroundColorAttributeName, nil];
		NSString *string = [NSString stringWithFormat:@"%@ %@", [self valueForKey:@"length"], [self valueForKeyPath:@"image.calibration.unit"]];
		NSSize stringSize = [string sizeWithAttributes:dict];
		labelRect = NSMakeRect(xOriginOnImage - (stringSize.width/2), yOriginOnImage - stringSize.height, stringSize.width, stringSize.height);
		scaleBarBackgroundRect = NSUnionRect(scaleBarBackgroundRect, labelRect);
	}
	
	scaleBarBackgroundRect = NSInsetRect(scaleBarBackgroundRect, -5, -5);
	
	if ([[self valueForKey:@"useBackgroundBox"] boolValue])
		return NSPointInRect(point, scaleBarBackgroundRect);
	else
		return NSPointInRect(point, scaleBarRect) || NSPointInRect(point, labelRect);
}



- (void)setPosition:(NSPoint)point;
{
	// Get the image size
	NSSize imageSize = [[self valueForKey:@"image"] pixelSize];
	
	float newXProportion = point.x / imageSize.width;
	float newYProportion = point.y / imageSize.height;
	
	[self setValue:[NSNumber numberWithFloat:newXProportion] forKey:@"xProportion"];
	[self setValue:[NSNumber numberWithFloat:newYProportion] forKey:@"yProportion"];
}




# pragma mark -
# pragma mark Accessors


// On-demand get accessor for the color
- (NSColor *)color;
{
    [self willAccessValueForKey:@"color"];
    NSColor *color = [self primitiveValueForKey:@"color"];
    [self didAccessValueForKey:@"color"];
    if (color == nil) {
        NSData *colorData = [self valueForKey:@"colorData"];
        if (colorData != nil) {
            color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
			[self willChangeValueForKey:@"color"];
            [self setPrimitiveValue:color forKey:@"color"];
			[self willChangeValueForKey:@"color"];
        }
    }
    return color;
}



// Immediate-Update Set Accessor
- (void)setColor:(NSColor *)aColor;
{
    [self willChangeValueForKey:@"color"];
    [self setPrimitiveValue:aColor forKey:@"color"];
    [self didChangeValueForKey:@"color"];
    [self setValue:[NSKeyedArchiver archivedDataWithRootObject:aColor] forKey:@"colorData"];
}



@end
