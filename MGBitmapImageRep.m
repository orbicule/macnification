//
//  NSBitmapImageRep_MGAdditions.m
//  StackTable
//
//  Created by Dennis Lorson on 29/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGBitmapImageRep.h"


@implementation MGBitmapImageRep


unsigned char *MGBitmapConvertToUchar(NSBitmapImageRep *rep)
{
    int bps = [rep bitsPerSample];
    
    if (bps == 8)
        return nil;
    
    unsigned char *src = [rep bitmapData];
    
    if (!src)
        return nil;
    
    
    int bpr = [rep bytesPerRow];
    int spp = [rep samplesPerPixel];
    int bpp = [rep bitsPerPixel];
    int w = [rep pixelsWide];
    int h = [rep pixelsHigh];

    unsigned char *dst = malloc(sizeof(unsigned char) * w * h * spp);


    for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			for (int i = 0; i < spp; i++) {
				
				unsigned char *oldPixAddr = &src[y * bpr + x * (bpp >> 3) + i * (bps >> 3)];
				unsigned char *newPixAddr = &dst[(y * w + x) * spp + i];
                
                long oldVal = ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
                    
                unsigned char newVal = (float)oldVal * 255.0f/(float)(1<<16);
                
                *newPixAddr = newVal;
                
            }
        }
    }
    
    return dst;
}


void MGBitmapInterpolateBilinear(unsigned char *dst, NSPoint p, unsigned char *src00, unsigned char *src01, unsigned char *src10, unsigned char *src11)
{
    NSPoint unit_p = p;
    unit_p.x -= floorf(unit_p.x);
    unit_p.y -= floorf(unit_p.y);
    
    for (int i = 0; i < 4; i++) {
        float src0y = (float)src00[i] * unit_p.y + (float)src01[i] * (1.0f - unit_p.y);
        float src1y = (float)src10[i] * unit_p.y + (float)src11[i] * (1.0f - unit_p.y);
        
        float srcxy = (float)src0y * unit_p.x + (float)src1y * (1.0f - unit_p.x);
        
        dst[i] = (unsigned char)MIN(MAX(srcxy, 0), 255);
    }
    
}


// Creates a 4-component (RGBA) pixel from the specified (x, y), given a bitmap format and buffer start address
void MGBitmapCopyPixelValues(unsigned char *dst, unsigned char *src, int w, int h, int x, int y, BOOL alphaFirst, BOOL hasAlpha, int spp, int bpr, int bpp)
{
    unsigned char *pix = &src[bpr * y + (bpp >> 3) * x];
    
    int colorOffset = (hasAlpha && alphaFirst) ? 1 : 0;
    //int alphaOffset = alphaFirst ? 0 : spp - 1;
        
    if (spp == 1) {
        dst[0] = dst[1] = dst[2] = pix[colorOffset];
    }
    else {
        dst[0] = pix[colorOffset];
        dst[1] = pix[colorOffset + 1];
        dst[2] = pix[colorOffset + 2];
        
    }
    
    dst[3] = 255;    
}


- (void)pixelData:(unsigned char **)data count:(NSInteger *)pixelCount fromPoint:(NSPoint)first toPoint:(NSPoint)second
{
    if (!ucharBitmapData_ && [self bitsPerSample] == 16)
        ucharBitmapData_ = MGBitmapConvertToUchar(self);
    
	unsigned char *bitmapData = nil;
    
    if (ucharBitmapData_)
        bitmapData = ucharBitmapData_;
    else
        bitmapData = [self bitmapData];
    
    int w = [self pixelsWide];
	int h = [self pixelsHigh];
	
    NSPoint start = first, end = second;
    
    // invert y
    start.y = h - start.y;
    end.y = h - end.y;
    
    // make sure start has the lowest x coord
    if (start.x > end.x) {
        NSPoint tmp = start;
        start = end;
        end = tmp;
    }
    
    
    float dist = sqrtf((start.y - end.y) * (start.y - end.y) + (start.x - end.x) * (start.x - end.x));
    
    int n = roundf(dist);
    
	unsigned char *sliceData = calloc(4 * n, sizeof(unsigned char));
    
    
    
    int spp = [self samplesPerPixel];
    int bpp = [self bitsPerPixel];
	int bpr = [self bytesPerRow];
    BOOL hasAlpha = [self hasAlpha];
    BOOL alphaFirst = [self hasAlpha] && ([self bitmapFormat] & NSAlphaFirstBitmapFormat);
    
    int convertedBPR = (ucharBitmapData_ == bitmapData) ? w * spp : bpr;
    
    // step n times along the line (== n pixels in slice)
    
    for (int i = 0; i < n; i++) {
        
        // Compute point
        NSPoint current;
        current.x = start.x + (end.x - start.x) * (float)i/(float)n;
        current.y = start.y + (end.y - start.y) * (float)i/(float)n;
        
        // Sanity checks
        current.x = MIN(MAX(0., current.x), w - 1);
        current.y = MIN(MAX(0., current.y), h - 1);
        
        // To unit
        NSPoint norm = current;
        norm.x -= floorf(norm.x);
        norm.y -= floorf(norm.y);
        
        // Determine 4 surrounding pixels
        unsigned char src00[4], src01[4], src10[4], src11[4];
        
        int x00 = MIN(MAX(0, (int)floorf(current.x)), w - 2);
        int y00 = MIN(MAX(0, (int)floorf(current.y)), h - 2);
        
        MGBitmapCopyPixelValues(src00, bitmapData, w, h, x00, y00, alphaFirst, hasAlpha, spp, convertedBPR, bpp);
        MGBitmapCopyPixelValues(src01, bitmapData, w, h, x00, y00 + 1, alphaFirst, hasAlpha, spp,  convertedBPR, bpp);
        MGBitmapCopyPixelValues(src10, bitmapData, w, h, x00 + 1, y00, alphaFirst, hasAlpha, spp,  convertedBPR, bpp);
        MGBitmapCopyPixelValues(src11, bitmapData, w, h, x00 + 1, y00 + 1, alphaFirst, hasAlpha, spp,  convertedBPR, bpp);
        
        // interpolate
        MGBitmapInterpolateBilinear(&sliceData[i * 4], norm, src00, src01, src10, src11);
        
    }
    
	*data = sliceData;
	*pixelCount = n;

}


- (void)dealloc 
{
    if (ucharBitmapData_)
        free(ucharBitmapData_);

    [super dealloc];
}

@end
