//
//  CurvedLineROI.m
//  Filament
//
//  Created by Peter Schols on 14/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "CurvedLineROI.h"
#import "Image.h"


@implementation CurvedLineROI

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	NSMutableSet *kps = [[[super keyPathsForValuesAffectingValueForKey:key] mutableCopy] autorelease];
	
	
	if ([key isEqualToString:@"summary"]) {
		[kps addObject:@"length"];
	}
	
	return kps;
}


+ (void)initialize 
{
	// Any change in length should trigger a change in summary
	//[self setKeys:[NSArray arrayWithObject:@"length"] triggerChangeNotificationsForDependentKey:@"summary"];
}

 
 
- (void)setPolygonPoints:(NSArray *)pointsArray;
{
	[self setAllPoints:pointsArray];	
	// Update our measurements
	[self updateMeasurements];
}

 

- (void)moveWithDelta:(NSPoint)deltaPoint;
{
	NSMutableArray *existingPoints = [NSMutableArray arrayWithCapacity:20];
	for (NSValue *value in [self allPoints]) {
		NSPoint currentPoint = [value pointValue];
		NSPoint newPoint = NSMakePoint(currentPoint.x + deltaPoint.x, currentPoint.y + deltaPoint.y);
		[existingPoints addObject:[NSValue valueWithPoint:newPoint]];
	}
	[self setAllPoints:existingPoints];
}

 

- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
{
	// Get the existing points
	NSMutableArray *existingPoints = [NSMutableArray arrayWithCapacity:20];
	[existingPoints addObjectsFromArray:[self allPoints]];
	// Change the point we are dragging
	[existingPoints replaceObjectAtIndex:handle-1 withObject:[NSValue valueWithPoint:point]];
	[self setAllPoints:existingPoints];
	// Update our measurements
	[self updateMeasurements];
}



- (void)updateMeasurements;
{
	// Calculate our area and perimeter attributes and set it
	[self calculateLength];
}



// Calculate the length in a real world unit
- (void)calculateLength;
{
	int i;
	float pixelPerimeter = 0.0;
	// Iterate all points and calculate the sum of their distance
	NSArray *allPoints = [self allPoints];
	for (i = 1; i < [allPoints count]; i++) {
		NSPoint thePreviousPoint = [[[self allPoints] objectAtIndex:i-1] pointValue];
		NSPoint thePoint = [[[self allPoints] objectAtIndex:i] pointValue];
		pixelPerimeter = pixelPerimeter + [self pixelLengthBetweenPoint:thePreviousPoint andPoint:thePoint];
	}
		
	// Convert the pixelPerimeter to the real perimeter
	float realPerimeter = [[self valueForKey:@"image"] realLengthForPixelLength:pixelPerimeter];
	
	// Set the length attributes
	[self setValue:[NSNumber numberWithInt:roundf(pixelPerimeter)] forKey:@"pixelLength"];
	[self setValue:[NSNumber numberWithFloat:realPerimeter] forKey:@"length"];
}




- (float)pixelLengthBetweenPoint:(NSPoint)point1 andPoint:(NSPoint)point2;
{
	float xSize = point1.x - point2.x;
	float ySize = point1.y - point2.y;
	float pixelLength = sqrt(pow(xSize, 2) + pow(ySize, 2));	
	return pixelLength;
}



- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
	
	
	NSBezierPath *path = [self pathWithScale:scale];
	[path setLineWidth:stroke*scale];
	[path stroke];
	
	if (isSelected) {
		for (NSValue *value in [self allPoints]) {
			NSPoint thePoint = [value pointValue];
			[self drawHandleAtPoint:thePoint withScale:stroke transform:trafo view:view];
		}
	}
	// Let our ROI superclass do the drawing of the note icon
	[super drawNoteIconWithScale:stroke transform:trafo view:view];
}



- (NSBezierPath *)pathWithScale:(float)scale;
{
	int i;
	NSBezierPath *path = [[NSBezierPath alloc] init];
	[path moveToPoint:[[[self allPoints] objectAtIndex:0] pointValue]];
	for (i = 1; i < [[self allPoints] count]; i++) {
		NSPoint thePoint = [[[self allPoints] objectAtIndex:i] pointValue];
		[path lineToPoint:thePoint];
	}
	if (isCurrentlyBeingDrawn && [[self allPoints] count] > 0) {
		[path lineToPoint:NSMakePoint(movedPoint.x * scale, movedPoint.y * scale)];
	}
	
	return [path autorelease];
}



- (int)handleUnderPoint:(NSPoint)point;
{
	for (NSValue *value in [self allPoints]) {
		NSPoint polygonPoint = [value pointValue];
		NSRect handleRect = [self hitRectForHandleAtPoint:polygonPoint withScale:1.0];
		
		if (NSPointInRect(point, handleRect)) {
			return [[self allPoints] indexOfObject:value] + 1;
		}
	}
	return 0;
}




- (BOOL)containsPoint:(NSPoint)point;
{
	int i;
	BOOL isHit = NO;
	NSPoint point1, point2, point3, point4;
	NSArray *allPoints = [self allPoints];
	for (i=0; i<[allPoints count]-1; i++) {
		NSPoint startPoint = [[allPoints objectAtIndex:i] pointValue];
 		NSPoint endPoint = [[allPoints objectAtIndex:i+1] pointValue];
		// Check the inclination of the line
		float xDistance = fabs(startPoint.x - endPoint.x);
		float yDistance = fabs(startPoint.y - endPoint.y);
		
		if (yDistance > xDistance) {
			// The line is (more) vertical 	
			point1 = NSMakePoint(startPoint.x - 2, startPoint.y);
			point2 = NSMakePoint(startPoint.x + 2, startPoint.y);
			point3 = NSMakePoint(endPoint.x + 2, endPoint.y);
			point4 = NSMakePoint(endPoint.x - 2, endPoint.y);
		} else {
			// The line is (more) horizontal
			point1 = NSMakePoint(startPoint.x, startPoint.y - 2);
			point2 = NSMakePoint(startPoint.x, startPoint.y + 2);
			point3 = NSMakePoint(endPoint.x, endPoint.y + 2);
			point4 = NSMakePoint(endPoint.x, endPoint.y - 2);
		}
		
		NSBezierPath *path = [[NSBezierPath alloc] init];
		[path moveToPoint:point1];
		[path lineToPoint:point2];
		[path lineToPoint:point3];
		[path lineToPoint:point4];
		[path closePath];
		
		[path setLineWidth:2];
		if ([path containsPoint:point]) {
			isHit = YES;
		}
		[path release];
		path = nil;	
	}
	// Check if the use has clicked the noteicon
	if (NSPointInRect(point, [self noteIconRect])) {
		isHit = YES;	
	}	
	return isHit;
}





- (NSRect)noteIconRect;
{
	NSPoint startPoint = [[[self allPoints] objectAtIndex:0] pointValue];
	NSPoint endPoint = [[[self allPoints] objectAtIndex:1] pointValue];
	return NSMakeRect(((startPoint.x + endPoint.x) / 2) + 5, (startPoint.y + endPoint.y) / 2, 16, 16);
}


//2621
- (NSAttributedString *)pixelSummary;
{
	NSString *theString = [NSString stringWithFormat:@"%C %d px", (unsigned short)0x2307, [[self valueForKey:@"pixelLength"]intValue]];
	NSMutableAttributedString *aString = [[[NSMutableAttributedString alloc] initWithString:theString] autorelease];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:12], NSFontAttributeName, [self summaryColor], NSForegroundColorAttributeName, nil];
	// Add the attributes to the first character and to the first character after the line break
	[aString addAttributes:dict range:NSMakeRange(0, 1)];
	return aString;
}



- (NSString *)summary;
{
	NSString *theString;
	float length = [[self valueForKey:@"length"]floatValue];
	if (length < 100000000000000.0 && length > 0.000000001) {
		theString = [NSString stringWithFormat:@"%2.2f %@", length, [self valueForKeyPath:@"image.calibration.unit"]];
	}
	else {
		theString = @"Not calibrated";
	}
	return theString;
}



- (NSString *)colorContentsSummary;
{
	return @"N/A";	
}


/*
- (NSString *)tabText;
{
	return [NSString stringWithFormat:@"%2.2f %@\t\t", [[self valueForKey:@"length"]floatValue], [self valueForKeyPath:@"image.calibration.unit"]];
}
*/



# pragma mark -
# pragma mark Color contents

- (void)calculateColorContents;
{
	// Do nothing here, so we don't invoke super
}




# pragma mark -
# pragma mark Accessors

- (BOOL)isCurrentlyBeingDrawn {
    return isCurrentlyBeingDrawn;
}

- (void)setIsCurrentlyBeingDrawn:(BOOL)value {
    if (isCurrentlyBeingDrawn != value) {
        isCurrentlyBeingDrawn = value;
    }
}

- (NSPoint)movedPoint {
    return movedPoint;
}

- (void)setMovedPoint:(NSPoint)value {
	movedPoint = value;
}




@end
