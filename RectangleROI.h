//
//  RectangleROI.h
//  Macnification
//
//  Created by Peter Schols on 10/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ROI.h"
#import "AreaROI.h"


@interface RectangleROI : AreaROI {

}


- (void)setStartPoint:(NSPoint)startPoint endPoint:(NSPoint)endPoint;


- (void)calculateArea;
- (void)calculatePerimeter;


- (NSPoint)startPoint;
- (NSPoint)endPoint;


- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
- (NSRect)boundsWithScale:(float)scale;


- (BOOL)containsPoint:(NSPoint)point;



@end
