//
//  MGOMESubFileImporter.h
//  Filament
//
//  Created by Dennis Lorson on 28/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGOMEFileImporter.h"

@interface MGOMESubFileImporter : MGOMEFileImporter
{
	MGOMEFileImporter *superImporter_;
	
	NSString *representationFilePath_;
	
	NSXMLDocument *doc_;
	
	MGMetadataSet *commonMetadata_;
	
	NSRange representationRange_;
	BOOL didFetchRepresentationRange_;
}

- (MGOMESubFileImporter *)initWithCommonMetadata:(MGMetadataSet *)metadata xmlDocument:(NSXMLDocument *)doc 
										filePath:(NSString *)filePath representationFilePath:(NSString *)repFilePath superImporter:(MGOMEFileImporter *)superImporter;
@end
