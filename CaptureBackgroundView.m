//
//  CaptureBackgroundView.m
//  Filament
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureBackgroundView.h"


@implementation CaptureBackgroundView

- (void)drawRect:(NSRect)rect
{
	[super drawRect:rect];
	
	[[NSColor colorWithCalibratedRed:60./255. green:60./255. blue:60./255. alpha:1.] set];
	
	NSRectFill([self bounds]);
}

@end
