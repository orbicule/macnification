//
//  MGMDKeywordsView.h
//  Filament
//
//  Created by Dennis Lorson on 04/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MGMDView.h"

@class MGMDTokenField;



																						#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface MGMDKeywordView : NSView <MGMDManualObserver, NSTokenFieldDelegate>
																						#else
@interface MGMDKeywordView : NSView <MGMDManualObserver>
																						#endif
{
	
	
	NSString *observableKeyPath;
	id observableController;
	
	IBOutlet MGMDTokenField *tokenField;

	IBOutlet NSView *emptyView;
	
	IBOutlet NSButton *addKeywordsButton;
	
	NSArray *draggedKeywords;

}

- (NSString *)stringForKeyword:(NSManagedObject *)keyword;
- (NSSet *)draggedKeywordsForTokenField:(NSTokenField *)field;


@end
