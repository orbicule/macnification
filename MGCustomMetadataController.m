//
//  CustomMetadataController.m
//  Filament
//
//  Created by Dennis Lorson on 24/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGCustomMetadataController.h"

#import "MGAppDelegate.h"
#import "SourceListTreeController.h"
#import "Image.h"
#import "MGLibraryController.h"

static NSString *MGCustomMetadataKey = @"customMetadata";

@interface MGCustomMetadataController ()

- (id)_libraryCollection;
- (void)_notifyObserversOfFieldsChange;



@end




@implementation MGCustomMetadataController

+ (MGCustomMetadataController *)sharedController;
{
	static MGCustomMetadataController *sharedController_ = nil;
	
	if (!sharedController_)
		sharedController_ = [[MGCustomMetadataController alloc] init];
	
	return sharedController_;
	
}


- (id)init
{
	self = [super init];
	if (self != nil) {
		observers_ = [[NSMutableArray array] retain];
	}
	return self;
}

- (void)dealloc
{
	[observers_ release];
	
	[super dealloc];
}


- (void)addObserver:(id <MGCustomMetadataObserver>)observer;
{
	[observers_ addObject:observer];
}

- (void)removeObserver:(id <MGCustomMetadataObserver>)observer;
{
	[observers_ removeObject:observer];
}

- (void)_notifyObserversOfFieldsChange;
{
	for (id <MGCustomMetadataObserver> observer in observers_) 
		if ([observer respondsToSelector:@selector(customMetadataControllerDidChangeFields:)])
			[observer customMetadataControllerDidChangeFields:self];
}

- (NSArray *)customAttributes;
{
	return [[self _libraryCollection] valueForKey:MGCustomMetadataKey];

}


- (void)changeAttributeWithName:(NSString *)old to:(NSString *)new
{
	if (!old || !new) return;
	
	if ([old isEqualToString:new]) return;
	
	if (![self isUniqueAttribute:new]) {
		
		NSLog(@"not unique...");
		return;
		
	}
	
	// construct the dictionary
	NSString *path = [self attributePathWithName:new];
	NSString *oldPath = [self attributePathWithName:old];
	
	
	NSDictionary *newDict = [NSDictionary dictionaryWithObjectsAndKeys:path, @"path", new, @"name", nil];
	
	
	NSMutableArray *attrs = [[[[self _libraryCollection] valueForKey:MGCustomMetadataKey] mutableCopy] autorelease];
	
	// remove the old value
	for (NSDictionary *otherAttr in [[attrs copy] autorelease]) {
		
		if ([[otherAttr objectForKey:@"name"] isEqualToString:old])
			[attrs removeObject:otherAttr];
		
	}
	
	// add the new value
	[attrs addObject:newDict];
	
	[[self _libraryCollection] setValue:attrs forKey:MGCustomMetadataKey];
	
	// replace the value in each image
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	[req setEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:[[self _libraryCollection] managedObjectContext]]];
	
	NSArray *images = [[[self _libraryCollection] managedObjectContext] executeFetchRequest:req error:nil];
	
	
	int nbImagesChanged = 0;
	id value;
	for (id image in images) {
		
		// check if there is a value for the old key
		if ((value = [image valueForKeyPath:oldPath])) {
			
			[image setValue:value forKeyPath:path];
			[image setValue:nil forKeyPath:oldPath];
			nbImagesChanged++;
		}
		
	}
	
	NSLog(@"%@ changed value from %@ to %@ in %i out of %i images", [self description], oldPath, path, nbImagesChanged, (int)[images count]);
	
	[self _notifyObserversOfFieldsChange];
		
}



- (void)addAttributeWithName:(NSString *)name
{
	if (![self isUniqueAttribute:name]) {
		
		NSLog(@"not unique...");
		return;
		
	}
	
	// construct the dictionary
	NSString *path = [self attributePathWithName:name];
	
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:path, @"path", name, @"name", nil];
	
	// get the existing array
	NSMutableArray *attrs = [[[[self _libraryCollection] valueForKey:MGCustomMetadataKey] mutableCopy] autorelease];
	if (!attrs)
		attrs = [NSMutableArray array];
	[attrs addObject:dict];
	
	[[self _libraryCollection] setValue:[[attrs copy] autorelease] forKey:MGCustomMetadataKey];
	
	// let the image array controller refresh...
	[[MGLibraryControllerInstance imageArrayController] setSelectedObjects:nil];
	
	[self _notifyObserversOfFieldsChange];	
}

- (BOOL)isUniqueAttribute:(NSString *)name
{
	NSString *path = [self attributePathWithName:name];
	
	NSArray *attrs = [self customAttributes];
	
	for (NSDictionary *attrDictionary in attrs) {
		
		NSString *existingPath = [attrDictionary objectForKey:@"path"];
		
		if ([existingPath isEqualToString:path]) return NO;
		
	}
	
	return YES;
	
}

- (void)removeAttributeWithPath:(NSString *)path;
{
	
	// get the existing array
	NSMutableArray *existingAttrs = [[[self customAttributes] mutableCopy] autorelease];
	
	for (NSDictionary *existingAttr in [[existingAttrs copy] autorelease]) {
		
		if ([[existingAttr objectForKey:@"path"] isEqualToString:path])
			[existingAttrs removeObject:existingAttr];
		
	}
	
	// set the new attr array
	[[self _libraryCollection] setValue:[[existingAttrs copy] autorelease] forKey:MGCustomMetadataKey];
	
	// delete all the data from all images
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	[req setEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:[[MGLibraryControllerInstance imageArrayController] managedObjectContext]]];
	
	NSArray *images = [[[MGLibraryControllerInstance imageArrayController] managedObjectContext] executeFetchRequest:req error:nil];
	
	int i = 0;
		
	for (Image *image in images) {
		
		// only set the value if the image has data for it!  otherwise the spotlight indexer goes crazy
		if ([image valueForKeyPath:path]) {
			
			i++;
			
			[image setValue:nil forKeyPath:path];
		}
		
		
	}

	NSLog(@"deleted custom info %@ from %i images", path, i);
	
	[self _notifyObserversOfFieldsChange];

	
	
}

- (NSString *)attributePathWithName:(NSString *)name
{
	NSString *result = name;
	result = [result stringByReplacingOccurrencesOfString:@" " withString:@""];
	
	result = [@"customMetadata." stringByAppendingString:result];
	
	return result;
}

- (NSString *)attributeNameWithPath:(NSString *)path
{
	for (NSDictionary *attr in [self customAttributes])
		if ([[attr objectForKey:@"path"] isEqualTo:path])
			return [attr objectForKey:@"name"];
	
	return nil;
}

- (id)_libraryCollection
{
	// return the library collection, which is used to keep track of the custom metadata attributes.
	return [[MGLibraryControllerInstance library] libraryGroup];
}




@end
