//
//  MGOMECompatibleFileImporter.h
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGOMEFileImporter.h"

@interface MGOMECompatibleFileImporter : MGOMEFileImporter 
{
	BOOL didConvertToOME_;
}

@end
