//
//  MGOverlaySettingsWindow.m
//  Filament
//
//  Created by Dennis Lorson on 19/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGOverlaySettingsWindow.h"


@implementation MGOverlaySettingsWindow

- (NSColor *)_backgroundColorPatternImage
{
    NSImage *bg = [[NSImage alloc] initWithSize:[self frame].size];
    NSRect bgRect = NSZeroRect;
    bgRect.size = [bg size];
    
    [bg lockFocus];
    NSBezierPath *bgPath = [self _backgroundPath];
    [NSGraphicsContext saveGraphicsState];
    [bgPath addClip];
    
    // Draw background.
    
	NSGradient *bgGrad = [[[NSGradient alloc] initWithColorsAndLocations:
						   [NSColor colorWithCalibratedWhite:37./255. alpha:0.95], 0.01, 
						   [NSColor colorWithCalibratedWhite:37./255. alpha:0.95], 0.35, nil] autorelease];
	
	
	[bgGrad drawInBezierPath:bgPath angle:-90];
	
        // Double the borderWidth since we're drawing inside the path.
        [bgPath setLineWidth:(2.0) * MAATTACHEDWINDOW_SCALE_FACTOR];
        [[NSColor colorWithCalibratedWhite:83.0/255.0 alpha:1.0] set];
        [bgPath stroke];
    
    [NSGraphicsContext restoreGraphicsState];
    [bg unlockFocus];
    
    return [NSColor colorWithPatternImage:[bg autorelease]];
}


- (BOOL)canBecomeKeyWindow
{
	return YES;
}

@end
