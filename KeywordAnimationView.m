//
//  KeywordAnimationView.m
//  Filament
//
//  Created by Peter Schols on 16/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "KeywordAnimationView.h"


@implementation KeywordAnimationView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}


- (void)setBrowserFrame:(NSRect)frame;
{
	[self setFrame:frame];
}


- (void)drawRect:(NSRect)rect {
	
	// DEBUG CODE
	//NSColor *controlColor = [[NSColor selectedControlColor] colorUsingColorSpaceName:NSDeviceRGBColorSpace];
	//[[NSColor colorWithCalibratedRed:[controlColor redComponent] green:[controlColor greenComponent] blue:[controlColor blueComponent] alpha:0.5]set];
	//[[NSColor redColor] set];
	//[NSBezierPath fillRect:rect];
		
	if (animationType == 0) {
		
		/*
		 // We have a keyword / rating / scale bar animation
		 // We let set the highlight color to the system control color
		 NSColor *controlColor = [[NSColor selectedControlColor] colorUsingColorSpaceName:NSDeviceRGBColorSpace];
		 [[NSColor colorWithCalibratedRed:[controlColor redComponent] green:[controlColor greenComponent] blue:[controlColor blueComponent] alpha:iteration/5.0]set];
		 */
		
		NSShadow *shadow = [[NSShadow alloc] init];
		[shadow setShadowColor:[NSColor darkGrayColor]];
		[shadow setShadowOffset:NSMakeSize(-2, -3)];
		[shadow setShadowBlurRadius:3];
		[shadow set];
		
		for (NSValue *value in self.rectsArray) {
			NSRect highlightRect = [value rectValue];
			NSRect largerRect = NSInsetRect(highlightRect, -3, -3);
			largerRect.size.height -= 40;
			largerRect.origin.y += 40;
			//NSBezierPath *roundedRectPath = [NSBezierPath bezierPathWithRoundedRect:largerRect xRadius:5.0 yRadius:5.0];
			//[roundedRectPath stroke];
			NSFont *font = [NSFont fontWithName:@"Lucida Grande" size:500.0/iteration];
			NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, [NSColor colorWithCalibratedRed:1.0 green:1.0 blue:1.0 alpha:iteration/20.0], NSForegroundColorAttributeName, nil];
			
			NSSize stringSize = [animationSymbol sizeWithAttributes:attributes];
			NSPoint drawPoint = NSMakePoint(largerRect.origin.x + (largerRect.size.width/2 - stringSize.width/2), largerRect.origin.y + (largerRect.size.height/2 - stringSize.height/2));
			
			[animationSymbol drawAtPoint:drawPoint withAttributes:attributes];
		}
		[shadow release];
	}
	
	
	else {
		// We have a spotlight animation
		
		NSBezierPath *boundsPath = [NSBezierPath bezierPathWithRect:[self bounds]];
		[boundsPath setWindingRule:NSEvenOddWindingRule];
		
		[NSGraphicsContext saveGraphicsState];

		for (NSValue *value in self.rectsArray) {
			NSRect highlightRect = [value rectValue];
			NSRect largerRect = NSInsetRect(highlightRect, -3, -3);
			largerRect.size.height -= 40;
			largerRect.origin.y += 40;
			
			
			// Create the clipping path for the black overlay: the browser view minus the selected image(s)
			NSBezierPath *visibleRectPath = [NSBezierPath bezierPathWithRect:largerRect];
			[boundsPath appendBezierPath:visibleRectPath];
			[boundsPath addClip];
		}
		
		if (iteration > 30) {
			[[NSColor colorWithCalibratedRed:0.1 green:0.1 blue:0.1 alpha:0.8-((iteration-30.0)/10.0)] set];
		} else {
			[[NSColor colorWithCalibratedRed:0.1 green:0.1 blue:0.1 alpha:0.8] set];
		}
		[boundsPath fill];
		
		[NSGraphicsContext restoreGraphicsState];
	}
}



- (void)startAnimation:(NSString *)animation withSymbol:(NSString *)symbol;
{
	if (!animationTimer) {
		if ([animation isEqualTo:@"Standard"]) {
			animationTimer = [[NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(iterate) userInfo:nil repeats:YES]retain];
			iteration = 1;
			numberOfIterations = 0;
			animationType = 0;
			[self setAnimationSymbol:symbol];
		}
		else if ([animation isEqualTo:@"Spotlight"]) {
			animationTimer = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(iterate) userInfo:nil repeats:YES]retain];
			iteration = 1;
			numberOfIterations = 0;
			animationType = 1;
		}
	}
}


- (void)iterate;
{
	if (iteration > 40) {
		[animationTimer invalidate];
		[animationTimer release];
		animationTimer = nil;
		[self setRectsArray:nil];
		[self setNeedsDisplayInRect:[self bounds]];
		[[self window] orderOut:self];
		return;
	}
	iteration++;
	[self setNeedsDisplayInRect:[self bounds]];	
}




/*
 - (void)iterate;
 {
 if (numberOfIterations > 2) {
 [animationTimer invalidate];
 [animationTimer release];
 animationTimer = nil;
 [[self window] orderOut:self];
 return;
 }
 else {
 if (iteration == 0) {
 goingUp = YES;	
 numberOfIterations++;	
 }
 if (iteration == 6) {
 goingUp = NO;
 }
 
 if (goingUp) {
 iteration++;	
 }
 else {
 iteration--;	
 }
 
 [self setNeedsDisplayInRect:[self bounds]];	
 }
 }
 */



@synthesize rectsArray, animationSymbol;

@end
