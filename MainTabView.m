//
//  MainTabView.m
//  Filament
//
//  Created by Dennis Lorson on 04/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MainTabView.h"


@implementation MainTabView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect 
{
	[[NSColor colorWithCalibratedWhite:0.18 alpha:1.0] set];
	NSRectFill([self bounds]);
    // Drawing code here.
}

@end
