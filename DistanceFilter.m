//
//  SquareFilter.m
//  ImageAlignment
//
//  Created by Dennis Lorson on 19/10/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "DistanceFilter.h"


@implementation DistanceFilter


static CIKernel *distanceKernel = nil;


+ (void)initialize
{
    [CIFilter registerFilterName: @"DistanceFilter"
					 constructor: (id<CIFilterConstructor>)self
				 classAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
								   @"DistanceFilter", kCIAttributeFilterDisplayName,
								   [NSArray arrayWithObjects:
									kCICategoryColorAdjustment, nil], kCIAttributeFilterCategories,
								   nil]
	 ];
}


- (id)init
{
    if(distanceKernel == nil)
    {
        NSBundle    *bundle = [NSBundle bundleForClass: [self class]];
        NSString    *code = [NSString stringWithContentsOfFile:[bundle pathForResource: @"image_distance" ofType: @"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
        NSArray     *kernels = [CIKernel kernelsWithString:code];
		
        distanceKernel = [[kernels objectAtIndex:0] retain];
    }
	
    return [super init];
}


- (CIImage *)outputImage
{
    CISampler *src = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
    CISampler *ref = [CISampler samplerWithImage:inputBackgroundImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
    return [self apply: distanceKernel, src, ref, kCIApplyOptionDefinition, [[src definition] intersectWith:[ref definition]], nil];
}

@end
