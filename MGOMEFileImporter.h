//
//  MGOMEFileImporter.h
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MGGenericImageFileImporter.h"

																					#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface MGOMEFileImporter : MGGenericImageFileImporter <NSXMLParserDelegate>
																					#else
@interface MGOMEFileImporter : MGGenericImageFileImporter
																					#endif
{		
	NSMutableArray *subImporters_;
}

@end
