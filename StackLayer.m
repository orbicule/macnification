//
//  StackLayer.m
//  StackTable
//
//  Created by Dennis Lorson on 19/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "StackLayer.h"
#import "Stack.h"
#import "StackView_Layout.h"

@implementation StackLayer

@synthesize selectedLayer;




- (id)init
{
	if ((self = [super init])) {
		
		self.opaque = NO;
	}
	
	return self;
	
}


- (NSArray *)sortedSublayers
{
	return [((StackView *)self.layoutManager) arrangedStackImagelayers];
}

@end
