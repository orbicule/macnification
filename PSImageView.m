//
//  PSImageView.m
//  Macnification
//
//  Created by Peter Schols on 16/11/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import "PSImageView.h"
#import "Image.h"
#import "ScaleBar.h"
#import "ROI.h"
#import "LineROI.h"
#import "RectangleROI.h"
#import "PolygonROI.h"
#import "CurvedLineROI.h"
#import "ImageKitExtensions.h"
#import "MGNavigatorView.h"


#pragma mark Custom Tiled Layer

@implementation PSImageView

@synthesize temporaryROIs;



- (void)setMeasurementsLayer;
{
	// Having this layer results in the Hand mode tool not working. Commenting this method out restores the Hand tool functionality
	isHidingROIs = NO;
	[self closeInfoPanel];
	
}

- (IBAction)toggleROIs:(id)sender;
{
	if ([self isOverlayHidden])
		[self showOverlay];
	else
		[self hideOverlay];
}




# pragma mark -
# pragma mark Drawing

- (void)drawOverlayRectInBounds:(NSRect)bounds withTransform:(NSAffineTransform *)trafo inView:(NSView *)view;
{
    if (NSEqualRects(bounds, NSZeroRect))
        return;
    
	[super drawOverlayRectInBounds:bounds withTransform:trafo inView:view];
		
	// Do the drawing
	if (isUsingDCCalibration)
		[self drawCalibrationLine];
	
	// Draw the existing ROIs
	[self drawROIsInBounds:bounds withTransform:trafo inView:view];
	
	// Draw the scale bar
	ScaleBar *scaleBar = [image valueForKey:@"scaleBar"];
	[scaleBar draw];
	
}


- (void)drawROIsInBounds:(NSRect)bounds withTransform:(NSAffineTransform *)trafo inView:(NSView *)view;
{
	// Get the ROI stroke and color from the user defaults
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	NSColor *selectedROIColor = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"selectedROIColor"]];
	NSColor *deselectedROIColor = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"deselectedROIColor"]];
	int stroke = [[userDefaults valueForKey:@"ROIStroke"] intValue];
		
	if (!temporaryROIs) {
		
		
		NSArray *rois = [roiArrayController arrangedObjects];
		
		for (ROI *roi in rois) {
			if (NSIntersectsRect([roi boundingRect], bounds)) {
				
				if ([self ROIIsSelected:roi]) {
					[selectedROIColor set];
					[roi drawWithScale:1.0 stroke:(float)stroke/self.zoomFactor isSelected:YES transform:trafo view:view];
				}
				else {
					[deselectedROIColor set];
					[roi drawWithScale:1.0 stroke:(float)stroke/self.zoomFactor isSelected:NO transform:trafo view:view];
				}
				
			}

		}
		
	} else {
		
		for (NSArray *roiPoints in temporaryROIs) {
			
			NSBezierPath *roi = [self roiWithPoints:roiPoints];
			[roi setLineWidth:2.0/self.zoomFactor];
			const CGFloat dash[2] = {5.0/self.zoomFactor, 5.0/self.zoomFactor};
			
			[selectedROIColor set];
			[roi setLineDash:dash count:2 phase:0];
			[roi stroke];
			[[selectedROIColor colorWithAlphaComponent:0.3] set];
			[roi fill];
			
		}
	}
}


- (NSBezierPath *)roiWithPoints:(NSArray *)roiPoints
{
    
    
	NSBezierPath *path = [NSBezierPath bezierPath];
	BOOL isFirstPoint = YES;
	
	for (NSValue *ptVal in roiPoints) {
		
		if (isFirstPoint) {
			isFirstPoint = NO;
			[path moveToPoint:[ptVal pointValue]];
		} else {
			
			[path lineToPoint:[ptVal pointValue]];
		}
		
	}
	
	[path closePath];
	
	return path;
	
}


- (void)setTemporaryROIs:(NSArray *)rois
{
	if (temporaryROIs != rois) {
		
		[temporaryROIs release];
		temporaryROIs = [rois retain];
		
		[self setOverlayNeedsDisplay];
		
	}
}


- (BOOL)ROIIsSelected:(ROI *)roi;
{
	return [[roiArrayController selectedObjects] containsObject:roi];
}


- (ROI *)selectedROI;
{
	if ([[roiArrayController selectedObjects]count] == 0)
		return nil;	
	
	return [[roiArrayController selectedObjects] objectAtIndex:0];
}


- (int)ROICount;
{
	return [[roiArrayController arrangedObjects] count];
}



# pragma mark -
# pragma mark Event handling


- (NSView *)hitTest:(NSPoint)aPoint
{
	if (NSPointInRect(aPoint, self.frame))
		return self;
	else
		return [super hitTest:aPoint];
}

- (BOOL)acceptsFirstResponder;
{
	return YES;	
}


// We should listen for keyboard events in our view
- (void)keyDown:(NSEvent *)theEvent;
{	
	unichar key = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
	
	if (key == NSDeleteCharacter)
		[roiArrayController remove:self];
	else if ((key == '+' || key == '=') && ([theEvent modifierFlags] & NSCommandKeyMask))
		[self zoomIn:self];	
	else if (key == '-' && ([theEvent modifierFlags] & NSCommandKeyMask))
		[self zoomOut:self];	
	
	[super keyDown:theEvent];
}


// Invoked by the NSSegmentedControl

- (IBAction)setCursorMode:(id)sender;
{	
	[rectangleButton setEnabled:YES];
	[rectangleButton setImage:[NSImage imageNamed:@"rectangle"]];
	
	[self deselectAllToolButtonsExceptNumber:[sender tag]];
	switch ([sender tag]) {
		case 0:
			cursorMode = pointerCursorMode;
			[self setToolMode:MGToolModeNone];
			break;
		case 1:
			cursorMode = lineCursorMode;
			[self setToolMode:MGToolModeSelect];
			break;
		case 2:
			cursorMode = rectangleCursorMode;
			[self setToolMode:MGToolModeSelect];
			break;
		case 3:
			cursorMode = circleCursorMode;
			[self setToolMode:MGToolModeSelect];
			break;
		case 4:
			cursorMode = polygonCursorMode;
			[self setToolMode:MGToolModeSelect];
			break;
		case 5:
			cursorMode = curvedLineCursorMode;
			[self setToolMode:MGToolModeSelect];
			break;
	}
	
	// To make sure the tools HUD is no longer key, we make our window key so that we immediately receive all mouse events
	[[self window] makeKeyAndOrderFront:self];
    


}

- (void)deselectAllToolButtonsExceptNumber:(NSUInteger)selectedButtonTag;
{
	NSArray *allButtons = [NSArray arrayWithObjects:arrowButton, lineButton, curvedLineButton, rectangleButton, circleButton, polygonButton, nil];
	for (NSButton *button in allButtons)
		if ([button	tag] != selectedButtonTag)
			[button setState:NSOffState];	
		else
			[button setState:NSOnState];
}




- (void)mouseDown:(NSEvent *)event;
{
	[[self window] makeFirstResponder:self];
	
	// Convert the clicked point's coordinates to the Image's coordinate system
	NSPoint clickedPoint = [self convertPoint:[event locationInWindow] fromView:nil];
	NSPoint normalizedPoint = [self convertViewPointToImagePoint:clickedPoint];
		
	if (isInColorPickerMode) {		
		[self readColorForColorPickerAtPoint:normalizedPoint];
		return;
	}
	
	if (cursorMode == pointerCursorMode) {
		// We are clicking with the arrow cursor
		// Remove a Comment HUD if present
		[self closeROICommentHUD:self];
		NSArray *rois = [roiArrayController arrangedObjects];
		
		// Reset the last dragged point (for moving ROIs by dragging)
		lastDraggedPoint = normalizedPoint;
		
		// Set the selection to nil if we are not extending the selection
		if (!([event modifierFlags] & NSCommandKeyMask)) {
			[roiArrayController setSelectedObjects:nil];
		}
		
		// Iterate all ROIs
		handleClicked = NO;
		roiHasBeenClicked = NO;
		
		
		for (ROI *roi in rois) {
            
            // Check whether a ROI handle contains the normalized point
			if ([roi isHandleAtPoint:normalizedPoint]) {
				// Get the handle we are clicking
				handle = [roi handleUnderPoint:normalizedPoint];
				oppositePoint = [roi oppositePointForHandle:handle];
				[roiArrayController setSelectedObjects:[NSArray arrayWithObject:roi]];
				handleClicked = YES;	
			}
            
			// Check whether the ROI contains the normalized point
			else if 	([roi containsPoint:normalizedPoint]) {
				roiHasBeenClicked = YES;
				if (([event modifierFlags] & NSCommandKeyMask)) {
					// If the command key is pressed, add the ROI to the selected ROIs
					[roiArrayController addSelectedObjects:[NSArray arrayWithObject:roi]];
				} else {
					// Else set the selection to contain only this new ROI
					[roiArrayController setSelectedObjects:[NSArray arrayWithObject:roi]];
				}
				if (([event modifierFlags] & NSAlternateKeyMask)) {
					// Copy the ROI					
					[[self selectedROI] cloneAndAssignToSameImage];
				}
				if ([event clickCount] == 2) {
					// If we double-clicked on a ROI, show the comment HUD
					[self showROICommentHUDAtPoint:[event locationInWindow]];	
				}
			}

		}
        
		
		// Check if the scale bar was clicked
		scaleBarClicked = [(ScaleBar *)[image valueForKey:@"scaleBar"] containsPoint:normalizedPoint];
		
	}
	else {
		downPoint = [self convertPoint:[event locationInWindow] fromView:nil];
		id roi = [self addROIWithClickCount:[event clickCount]];
		// Select the newly added ROI
		if (roi)
			[roiArrayController setSelectedObjects:[NSArray arrayWithObject:roi]];
	}
	
	if (!roiHasBeenClicked && !handleClicked && cursorMode == pointerCursorMode && [event clickCount] == 2) {
		// Image is not calibrated, try to use 1CC
		downPoint = [self convertPoint:[event locationInWindow] fromView:nil];
		isUsingDCCalibration = YES;
		[self oneClickCalibrationWithPoint:[self unScaledPoint:downPoint]];
	}

	[self setOverlayNeedsDisplay];
}


// Called by mouseDown to add a new ROI (or to add a point to an existing ROI in case or PolygonROI)

- (ROI *)addROIWithClickCount:(int)clickCount;
{
	// We are adding a new ROI, so show the ROI HUD (unless this is turned off in the preferences)
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	BOOL showROIHUD = [[userDefaults valueForKey:@"showROIPanelWhenMeasuring"] boolValue];
	if (showROIHUD)
		[roiArrayController showROIHUD];
	
	// Add the ROI to the image
	id roi;
	if (cursorMode == lineCursorMode)
		roi = [image addROI:@"LineROI" startPoint:[self unScaledPoint:downPoint] endPoint:[self unScaledPoint:downPoint]];
	
	else if (cursorMode == rectangleCursorMode)
		roi = [image addROI:@"RectangleROI" startPoint:[self unScaledPoint:downPoint] endPoint:[self unScaledPoint:downPoint]];

	else if (cursorMode == circleCursorMode)
		roi = [image addROI:@"CircleROI" startPoint:[self unScaledPoint:downPoint] endPoint:[self unScaledPoint:downPoint]];

	else if (cursorMode == polygonCursorMode) {
		// Add a point to the polygonPoints NSMutableArray
		[polygonPoints addObject:[NSValue valueWithPoint:[self unScaledPoint:downPoint]]];
		
		if (isDrawingPolygon) {
			if (clickCount == 2) {
				// Remove the duplicate last point when double-clicking
				[polygonPoints removeLastObject];
				
				// show the current measurement HUD and update the current measurement
				[roiArrayController willChangeValueForKey:@"selection.summary"];
				[roiArrayController didChangeValueForKey:@"selection.summary"];
				// Calculate the PolygonROI's color contents
				[[self selectedROI] calculateColorContents];
			}
			// We are adding points to the polygon
			[(PolygonROI *)[self selectedROI] setPolygonPoints:polygonPoints];
			roi = nil;
		} 
		else {
			// We are adding the first polygon point, so we first add the PolygonROI to the image
			roi = [image addROI:@"PolygonROI" withPoints:polygonPoints];
			// Indicate we are in the middle of creating a PolygonROI
			[roi setIsCurrentlyBeingDrawn:YES];
			// Select our newly created ROI
			[roiArrayController setSelectedObjects:[NSArray arrayWithObject:roi]];
			// We set the moved point to the first point to avoid the zeropoint being drawn
			[(PolygonROI *)[self selectedROI] setMovedPoint:[self unScaledPoint:downPoint]];
		}
		
		// We are drawing a polygon
		isDrawingPolygon = YES;
		
		// Make ourselves first responder, so we get the mouseMoved events
		[theWindow makeFirstResponder:self];
	}
	else if (cursorMode == curvedLineCursorMode) {
		// Add a point to the polygonPoints NSMutableArray
		[polygonPoints addObject:[NSValue valueWithPoint:[self unScaledPoint:downPoint]]];
		
		if (isDrawingPolygon) {
			if (clickCount == 2) {
				// Remove the duplicate last point when double-clicking
				[polygonPoints removeLastObject];
			}
			
			// We are adding points to the polygon
			[(PolygonROI *)[self selectedROI] setPolygonPoints:polygonPoints];
			roi = nil;
		} 
		else {
			// We are adding the first polygon point, so we first add the PolygonROI to the image
			roi = [image addROI:@"CurvedLineROI" withPoints:polygonPoints];
			// Indicate we are in the middle of creating a PolygonROI
			[roi setIsCurrentlyBeingDrawn:YES];
			// Select our newly created ROI
			[roiArrayController setSelectedObjects:[NSArray arrayWithObject:roi]];
			// We set the moved point to the first point to avoid the zeropoint being drawn
			[(CurvedLineROI *)[self selectedROI] setMovedPoint:[self unScaledPoint:downPoint]];
		}
		
		// We are drawing a polygon
		isDrawingPolygon = YES;
		
		// Make ourselves first responder, so we get the mouseMoved events
		[theWindow makeFirstResponder:self];
	}
	
	return roi;
}



- (void)mouseDragged:(NSEvent *)event;
{
	if (self.toolMode == MGToolModeMove) {
		[super mouseDragged:event];
		return;
	}
	
	NSPoint p = [event locationInWindow];
	currentPoint = [self convertPoint:p fromView:nil];
	
	NSPoint normalizedPoint = [self convertViewPointToImagePoint:currentPoint];
	
	// The opposite point of the point we are dragging, in scroll view coordinates.
	// We need this to position the current measurement HUD
	//NSPoint oppositePointInWindow;		
	
	if (cursorMode == pointerCursorMode) {

		
		if (handleClicked) {
			if ([event modifierFlags] & NSShiftKeyMask) {
				NSPoint constrainedPoint = [self constrainedPointForNormalizedPoint:normalizedPoint oppositePoint:oppositePoint];
				[[self selectedROI] resizeByMovingHandle:handle toPoint:constrainedPoint withOppositePoint:oppositePoint];
			}
			else {
				// Resize the selected ROI
				[[self selectedROI] resizeByMovingHandle:handle toPoint:normalizedPoint withOppositePoint:oppositePoint];
			}
			
			[self setOverlayNeedsDisplay];
		}
		// If the scale bar has been clicked, move it
		if (scaleBarClicked) {
			
			normalizedPoint.x = MAX(0, MIN([image pixelSize].width, normalizedPoint.x));
			normalizedPoint.y = MAX(0, MIN([image pixelSize].height, normalizedPoint.y));
			
			[(ScaleBar *)[image valueForKey:@"scaleBar"] setPosition:normalizedPoint];		

			[self setOverlayNeedsDisplay];
		}
		
		// If a ROI is selected, move it
		if ([self selectedROI] && !handleClicked) {
			NSPoint delta = NSMakePoint(normalizedPoint.x-lastDraggedPoint.x, normalizedPoint.y-lastDraggedPoint.y);
			[[self selectedROI] moveWithDelta:delta];
			lastDraggedPoint = normalizedPoint;
		}
	}
	
	
	else {
		if (!isDrawingPolygon) {
			
			if (([event modifierFlags] & NSShiftKeyMask) && (cursorMode == rectangleCursorMode || cursorMode == circleCursorMode)) {
				NSPoint constrainedPoint = [self constrainedPointForNormalizedPoint:[self unScaledPoint:currentPoint] oppositePoint:[self unScaledPoint:downPoint]];
				[[self selectedROI] resizeByMovingHandle:1 toPoint:constrainedPoint  withOppositePoint:[self unScaledPoint:downPoint]];
			}
			else {
				// Resize the selected ROI while creating it
				[[self selectedROI] resizeByMovingHandle:1 toPoint:[self unScaledPoint:currentPoint]  withOppositePoint:[self unScaledPoint:downPoint]];
			}
			
			[self setOverlayNeedsDisplay];
			
		}
		
	}
	
	// Update the statisticsController by letting the roiArrayController know that its arrangedObjects and the selection.summary have been updated
	[roiArrayController willChangeValueForKey:@"arrangedObjects"];
	[roiArrayController didChangeValueForKey:@"arrangedObjects"];
	[roiArrayController willChangeValueForKey:@"selection.summary"];
	[roiArrayController didChangeValueForKey:@"selection.summary"];
	
}



// Returns a constrained point when supplying it with a point and its oppositePoint
- (NSPoint)constrainedPointForNormalizedPoint:(NSPoint)nPoint oppositePoint:(NSPoint)oPoint;
{
	float minimumDistanceBetweenPoints = MIN(fabs(oPoint.x - nPoint.x), fabs(oPoint.y - nPoint.y));
	float constrainedX, constrainedY;
	if (oPoint.x - nPoint.x < 0)
		constrainedX = oPoint.x + minimumDistanceBetweenPoints;
	else
		constrainedX = oPoint.x - minimumDistanceBetweenPoints;
	
	if (oPoint.y - nPoint.y < 0)
		constrainedY = oPoint.y + minimumDistanceBetweenPoints;
	else
		constrainedY = oPoint.y - minimumDistanceBetweenPoints;
	
	return NSMakePoint(constrainedX, constrainedY);
}




- (void)mouseUp:(NSEvent *)event
{
	[super mouseUp:event];
	
	if (isHidingROIs)
		[self showInfoPanelWithMessage:@"ROIs are currently hidden. Click Show ROIs to start measuring."];	
	
	if (!isDrawingPolygon && [event clickCount] != 2)
		[[self selectedROI] calculateColorContents];
	
	
	if (cursorMode != pointerCursorMode) {
		NSPoint p = [event locationInWindow];
		upPoint = [self convertPoint:p fromView:nil];
		
		// If the user has double-clicked
		if ([event clickCount] == 2) {
			// Remove all points from the polygonPoints array
			[polygonPoints removeAllObjects];
			// We are no longer drawing a polygon
			isDrawingPolygon = NO;
			[(PolygonROI *)[self selectedROI] setIsCurrentlyBeingDrawn:NO];
		}
        
       // if ([self selectedROI] && ([NSEvent modifierFlags] & NSAlternateKeyMask))
        //    [self updateOverlayForChangesInROIs:[NSArray arrayWithObject:[self selectedROI]]];
       // else
            [self setOverlayNeedsDisplay];
	}
	
	// Update the statisticsController by letting the roiArrayController know that its arrangedObjects have been updated
	[roiArrayController willChangeValueForKey:@"arrangedObjects"];
	[roiArrayController didChangeValueForKey:@"arrangedObjects"];
}


- (void)mouseMoved:(NSEvent *)event
{
	// We are only interested in mouseMoved events when drawing polygons
	if (cursorMode == polygonCursorMode || cursorMode == curvedLineCursorMode) {
		NSPoint p = [event locationInWindow];
		movedPoint = [self convertPoint:p fromView:nil];
		[(PolygonROI *)[self selectedROI] setMovedPoint:[self unScaledPoint:movedPoint]];

       // if ([self selectedROI] && ([NSEvent modifierFlags] & NSAlternateKeyMask))
       //     [self updateOverlayForChangesInROIs:[NSArray arrayWithObject:[self selectedROI]]];
       // else
            [self setOverlayNeedsDisplay];
	}
}	


- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}



- (void)updateOverlayForChangesInROIs:(NSArray *)rois
{
    if (![rois count])
        return;
    
    NSRect changedRect = [(ROI *)[rois objectAtIndex:0] invalidatedRect];
    
    // this should allow for zero rects (just ignores them)
    for (ROI *roi in rois)
        changedRect = NSUnionRect(changedRect, [roi invalidatedRect]);
    
    // transform to the overlay coords
    
    // in self(.layer) coords
    changedRect = [self convertImageRectToViewRect:changedRect];
    
    // in overlayview coords
    changedRect = [overlayView_ convertRect:changedRect fromView:self];
        
    [self setOverlayNeedsDisplayInRect:changedRect];
}


# pragma mark -
# pragma mark White balance correction 


- (void)enableColorPickerMode;
{
	// Set ourselves in colorPickerMode
	isInColorPickerMode = YES;	
	
	// Show the info panel
	[self showInfoPanelWithMessage:@"Click a point on the image that should be white"];
}



- (void)readColorForColorPickerAtPoint:(NSPoint)point;
{
	// Get the color of the point clicked
	NSImage *currentImage = [image filteredImage];
	[currentImage lockFocus];  // NSReadPixel pulls data out of the current focused graphics context, so -lockFocus is necessary here.
	NSColor *colorOfClickedPoint = NSReadPixel(point);
	[currentImage unlockFocus]; 
	isInColorPickerMode = NO;
	
	// Let our observers (the AdjustmentsController) know about our nice new color
	NSNotification *newColorNotification = [NSNotification notificationWithName:@"pickedNewWhiteBalanceCorrectionColor" object:colorOfClickedPoint userInfo:NULL];
	[[NSNotificationCenter defaultCenter] postNotification:newColorNotification];
	
	// Close the info panel
	[self closeInfoPanel];
    

}



# pragma mark -
# pragma mark Calibration

- (void)oneClickCalibrationWithPoint:(NSPoint)point;
{

    
	NSBitmapImageRep *rep = [image filteredImageRep];

	int w = [rep pixelsWide];
	int h = [rep pixelsHigh];
	
	// Convert first
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	CGContextRef ctx = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorspace, kCGImageAlphaNoneSkipLast);
	CGContextDrawImage(ctx, CGRectMake(0, 0, w, h), [rep CGImage]);
	
	
	unsigned char *data = (unsigned char *)CGBitmapContextGetData(ctx);
	
	int clickedX = (int)point.x;
	int clickedY = (int)point.y;
	clickedY = h - clickedY;					// invert y
	clickedX = MIN(MAX(clickedX, 0), w - 1);
	clickedY = MIN(MAX(clickedY, 0), h - 1);
	
	
	int rightLength = 0;
	int leftLength = 0;
	
	unsigned char clickedColor[3];
	for (int i = 0; i < 3; i++)
		clickedColor[i] = data[(clickedY * w + clickedX) * 4 + i];
	
	// Initialise the colors we will use for measuring
	unsigned char currentColor[3];
	for (int i = 0; i < 3; i++)
		currentColor[i] = clickedColor[i];	

	int currentX = clickedX;
	
	// Count the number of pixels between the point clicked and the right end of the scale bar
	while (currentColor[0] == clickedColor[0] && currentColor[1] == clickedColor[1] && currentColor[2] == clickedColor[2] && currentX < w) {
		currentX++;
		for (int i = 0; i < 3; i++)
			currentColor[i] = data[(clickedY * w + currentX) * 4 + i];
	}
	rightLength = currentX - clickedX - 1 ; // substract one because we counted one pixel too far right
	
	
	// Re-Initialise the colors we will use for measuring
	for (int i = 0; i < 3; i++)
		currentColor[i] = clickedColor[i];	
	
	// Count the number of pixels between the point clicked and the left end of the scale bar
	while (currentColor[0] == clickedColor[0] && currentColor[1] == clickedColor[1] && currentColor[2] == clickedColor[2] && currentX > -1) {
		currentX--;
		for (int i = 0; i < 3; i++)
			currentColor[i] = data[(clickedY * w + currentX) * 4 + i];
	}
	leftLength = clickedX - currentX - 1 ; // substract one because we counted one pixel too far right
	
	
	// Calculate the scale bar's total length
	calibrationLength = 0.00 + rightLength + leftLength;
	NSLog(@"calibrationLength: %f", calibrationLength);
	
	// Calculate the leftmost and rightmost point of the scale bar
	calibrationPoint1 = NSMakePoint(point.x - leftLength, point.y);
	calibrationPoint2 = NSMakePoint(point.x + rightLength, point.y);
	
	NSPoint centerPointOfCalibration = NSMakePoint((calibrationPoint1.x + calibrationPoint2.x) / 2, calibrationPoint1.y);	
	[self setOverlayNeedsDisplay];
	
	
	[self showCalibrationPanelAtImagePoint:centerPointOfCalibration];
}



- (void)showCalibrationPanelAtImagePoint:(NSPoint)point;
{

	
	// Get the point in the view based on the imagePoint
	NSPoint scaledPoint = [self scaledPoint:point];
	
	// Conert the view point to the Window point
	NSPoint windowPosition = [self convertPoint:scaledPoint toView:nil];
	
	// Convert the window point to the screen point
	NSPoint screenPosition = [[self window] convertBaseToScreen:windowPosition];
	
	// Show the calibration panel at the calculated location
	[calibrationPanel setFrameTopLeftPoint: NSMakePoint(screenPosition.x - ([calibrationPanel frame].size.width)/2,
														screenPosition.y + [calibrationPanel frame].size.height + 20)];
	[calibrationPanel makeKeyAndOrderFront:self];
	[self setOverlayNeedsDisplay];
}



- (void)drawCalibrationLine;
{
	// Draw a line over the scale bar
	[[NSColor orangeColor] set];
	NSBezierPath *line = [NSBezierPath bezierPath];
	[line moveToPoint:calibrationPoint1];
	[line lineToPoint:calibrationPoint2];
	[line setLineWidth:3*scale];
	[line stroke];
}


// Invoked when dismissing the calibrationPanel
- (IBAction)calibrate:(id)sender;
{
	
	float lineLength;
	NSPoint clickedPoint;
	if (isUsingDCCalibration) {
		// We are calibrating using 1CC, get the clickedPoint to store it in the calibration
		lineLength = calibrationLength;
		clickedPoint = [self unScaledPoint:downPoint];
		
	}
	else {
		// We are calibrating using a LineROI
		ROI *roi = [self selectedROI];
		lineLength = [[roi valueForKey:@"pixelLength"]floatValue]; 
		clickedPoint = NSZeroPoint;		
	}
	
	if (lineLength != 0 && [calibrationScaleBarLengthField doubleValue] != 0) {
				
		// Calibrate the image
		[image calibratePoint:clickedPoint withPixels:(double)(lineLength / [calibrationScaleBarLengthField doubleValue]) perUnit:[calibrationUnitPopup titleOfSelectedItem] calibrationName:[calibrationNameField stringValue]];

		// Hide the calibration panel
		[calibrationPanel orderOut:self];
		[self closeInfoPanel];
		
		[self setOverlayNeedsDisplay];
	}
	else {
		NSBeep();
	}

	isUsingDCCalibration = NO;
}






// Invoked by the user when calibrating using a LineROI
- (IBAction)showCalibrationPanel:(id)sender;
{
	if (sender == self) {
		
		LineROI *roi = (LineROI*)[self selectedROI];
		
		
		
		// Calculate the center point of the LineROI and show the calibration panel at that point
		NSPoint startPoint = [roi startPoint];
		NSPoint endPoint = [roi endPoint];
		NSPoint centerOfLine = NSMakePoint((startPoint.x + endPoint.x)/2, (startPoint.y + endPoint.y)/2);	
		[self showCalibrationPanelAtImagePoint:centerOfLine];
	}
	else {
		NSSize imageSize = [image pixelSize];
		[self showCalibrationPanelAtImagePoint:NSMakePoint(imageSize.width/2, imageSize.height/2)];
	}
	
	// Show the info message
	[self showInfoPanelWithMessage:@"Drag a line over an object of known length and enter its length in the calibration panel to calibrate this image"];
}



// Invoked when the calibration panel closes
- (void)windowWillClose:(NSNotification *)notification;
{
	[self closeInfoPanel];
}





# pragma mark -
# pragma mark Housekeeping


- (void)awakeFromNib;
{	
	// Register as an observer for the selected image
	[fullScreenImageArrayController addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	
	// Register as an observer for the (selected) ROIs
	[roiArrayController addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	[roiArrayController addObserver:self forKeyPath:@"arrangedObjects" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
	
	// Create an NSMutableArray to hold the polygon points
	polygonPoints = [[NSMutableArray alloc] init];
	
	// Watch for notifications regarding the colorPickerMode for White balance correction
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableColorPickerMode) name:@"enableColorPickerMode" object:nil];
	
	// Make sure the buttons don't flicker when changing state
	[[arrowButton cell] setHighlightsBy:0];
	[[lineButton cell] setHighlightsBy:0];
	[[curvedLineButton cell] setHighlightsBy:0];
	[[rectangleButton cell] setHighlightsBy:0];
	[[circleButton cell] setHighlightsBy:0];
	[[polygonButton cell] setHighlightsBy:0];
	
	
	// Select the arrow button
	[arrowButton setState:NSOnState];


}


// Sent when the selected image changes or when ROIs change
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (shouldObserveSelectedImage) {
		// The image selection has changed
		if ([object isEqualTo:fullScreenImageArrayController])
		{
			if ([[fullScreenImageArrayController selectedObjects] count] == 0) {
				
				[self closeNavigatorWindow:self];
				self.image = nil;
				
			}
			
			else {
								
				
				NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
				BOOL alwaysZoomToFit = [[userDefaults valueForKey:@"alwaysZoomToFit"]boolValue];
				
				image = [[object selectedObjects] objectAtIndex:0];
				
				// Store our zoomFactor so it remains the same between every image
				CGFloat zoomFactor = [self zoomFactor];
				
				CIImage *img = [image ciImage];
				self.image = img;
				//[self setImageRep:[image imageRep]];
				[self setNavigatorImage:[image filteredThumbnail]];
				
				// Update our filters
				[self filtersDidChange];
								
				
				// Show the Navigator if needed
				// If we zoom to fit, don't take any action, only close it if necessary: 
				//		the image might be larger than the screen but will be zoomed to fit in a moment.
				//		so an unneccessary Navigator order in would take a lot of CPU hit.
				if (alwaysZoomToFit)
					[self closeNavigatorWindow:self];
				else
					[self showNavigatorIfNeeded];
				
				
				// Reset the global event points
				downPoint = upPoint = currentPoint = calibrationPoint1 = calibrationPoint2 = NSZeroPoint;
				
				// Make sure we add a layer that shows the measurements
				[self setMeasurementsLayer];
				
				// Close any ROI Comment HUD if open
				[self closeROICommentHUD:self];
				
				// Zoom to fit if set in the preferences
				if (alwaysZoomToFit || zoomFactor < 0.1) {		
					[self zoomImageToFitWithAnimation:NO];
				}
				
				[self setOverlayNeedsDisplay];

			}
			
			
		}
		

		else {
			[self setOverlayNeedsDisplay];

		}
			

	}

	
}



- (void)dealloc;
{
	[fullScreenImageArrayController removeObserver:self forKeyPath:@"selection"];
	[roiArrayController removeObserver:self forKeyPath:@"selection"];
	[roiArrayController removeObserver:self forKeyPath:@"arrangedObjects"];
	
	[polygonPoints release];
	[super dealloc];
}


# pragma mark -
# pragma mark Filters


- (void)filtersDidChange;
{
	if ([[fullScreenImageArrayController selectedObjects] count] == 0)
		return;
	
	image = [[fullScreenImageArrayController selectedObjects] objectAtIndex:0];
	
	[self setFilters:[image ciFiltersWithThreshold]];
	
	[self setNavigatorImage:[image filteredThumbnail]];

}


# pragma mark -
# pragma mark ROI comments HUD


- (void)showROICommentHUDAtPoint:(NSPoint)point;
{
	[self closeROICommentHUD:self];
	if (!attachedWindow) {
		attachedWindow = [[MAAttachedWindow alloc] initWithView:attachedWindowView attachedToPoint:point inWindow:[self window] onSide:MAPositionRight atDistance:0.0];
		[attachedWindow setBackgroundColor:[NSColor colorWithCalibratedRed:0.1 green:0.1 blue:0.1 alpha:0.75]];
		[attachedWindow setHasArrow:YES];
		[attachedWindow setBorderWidth:0.75];
		[attachedWindow setMovableByWindowBackground:YES];
		[attachedWindow setHidesOnDeactivate:YES];
	}
	[[self window] addChildWindow:attachedWindow ordered:NSWindowAbove];
	[attachedWindow explodeToFrame:NSMakeRect(point.x, point.y - [attachedWindow frame].size.height/2, 240, 290)];
}



- (IBAction)closeROICommentHUD:(id)sender;
{
	if (attachedWindow) {
		[attachedWindow implode];
		[[self window] removeChildWindow:attachedWindow];
		[attachedWindow release];
		attachedWindow = nil;
		[self setOverlayNeedsDisplay];
	}	
}






# pragma mark -
# pragma mark Info Panel


- (void)showInfoPanelWithMessage:(NSString *)message;
{
	// Calculate the point we display the infopanel
	NSPoint originPoint = [[self enclosingScrollView] documentVisibleRect].origin;
	NSPoint upperLeftPoint = NSMakePoint(originPoint.x + 100, [[self enclosingScrollView] documentVisibleRect].size.height - 50);
	NSPoint pointInWindowCoordinates = [self convertPoint:upperLeftPoint toView:nil];
	NSPoint pointInScreenCoords = [[self window] convertBaseToScreen:pointInWindowCoordinates];
	
	// Show the info panel
	[infoPanel setMessage:message];
	[infoPanel setFrameOrigin:pointInScreenCoords];
	[infoPanel orderFront:self];	
}


- (void)closeInfoPanel;
{
	[infoPanel orderOut:self];	
}


# pragma mark -
# pragma mark Navigator

- (id)initWithFrame:(NSRect)frame;
{	
	if ((self = [super initWithFrame:frame]))
		[NSBundle loadNibNamed:@"Navigator.nib" owner:self];
	return self;
}


- (IBAction)closeNavigatorWindow:(id)sender;
{
	[navigatorWindow orderOut:self];
}


- (void)setNavigatorImage:(CIImage *)newImage;
{ 
	CIImage *filtered = newImage;
	if (filtered)
		[navigatorView setImage:filtered forView:self];
	
	// set the correct window size to contain this image
	CGSize viewSize = navigatorView.thumbnailSize;
	
	CGSize windowSize = CGSizeMake(viewSize.width + 4, viewSize.height + 4);
	
	NSRect windowRect = [navigatorWindow frameRectForContentRect:NSMakeRect(0, 0, windowSize.width, windowSize.height)];
	NSRect currentWindowRect = [navigatorWindow frame];
	windowRect.origin.x = currentWindowRect.origin.x;
	windowRect.origin.y = currentWindowRect.origin.y;
	
	[navigatorWindow setFrame:windowRect display:NO];
}



- (IBAction)zoomIn:(id)sender;
{
	// this is private API from the IKIV.  Don't use it: forward to our own method, which uses the API methods.
	[self zoomInWithAnimation:YES];
	[self showNavigatorIfNeeded];
}



- (IBAction)zoomOut:(id)sender;
{
	[self zoomOutWithAnimation:YES];
	[self showNavigatorIfNeeded];
}

- (IBAction)zoomImageToFit:(id)sender;
{
	[self zoomImageToFitWithAnimation:YES];
	[self showNavigatorIfNeeded];
}


- (void)scrollToPoint:(NSPoint)point;
{
	[super scrollToPoint:point];
	[self updateNavigatorVisibleRect];
}

- (void)scrollToRect:(NSRect)rect;
{
	[super scrollToRect:rect];
	[self updateNavigatorVisibleRect];
}

- (void)scrollByX:(CGFloat)x y:(CGFloat)y
{
    [super scrollByX:x y:y];
    
    [self updateNavigatorVisibleRect];
}


- (void)showNavigatorIfNeeded;
{
	if (![self imageIsCompletelyVisible] && [[self window] isVisible]) {
		
		[navigatorWindow setFloatingPanel:YES];
		[navigatorWindow setMovableByWindowBackground:NO];
		[navigatorWindow orderFront:self];
		
	} else {
		[navigatorWindow orderOut:self];
	}
	// very important: need to let the scrollview adjust itself
	[self updateNavigatorVisibleRect];
}



- (CGRect)viewBounds;
{
	return NSRectToCGRect([self bounds]);
}

- (BOOL)rect:(CGRect)rect1 containsRectInSize:(CGRect)rect2;
{
	if (rect1.size.width >= rect2.size.width - 0.1 && rect1.size.height >= rect2.size.height - 0.1) return YES;
	
	return NO;
	
}


- (void)updateNavigatorVisibleRect
{
	CGRect imageFrame = CGRectZero;
    imageFrame.size = originalImageSize_;
	NSRect docVisibleRect = [self visibleImageRect];
	
	CGFloat x = docVisibleRect.origin.x/imageFrame.size.width;
	CGFloat y = docVisibleRect.origin.y/imageFrame.size.height;
	CGFloat w = docVisibleRect.size.width/imageFrame.size.width;
	CGFloat h = docVisibleRect.size.height/imageFrame.size.height;

	navigatorView.visibleRect = NSMakeRect(x, y, w, h);
}



- (void)scrollToNormalizedRect:(NSRect)normalizedRect;
{
	CGRect imageFrame = CGRectZero;
    imageFrame.size = originalImageSize_;
	NSRect scrollRect = normalizedRect;
	
	scrollRect.origin.x *= imageFrame.size.width;
	scrollRect.origin.y *= imageFrame.size.height;
	scrollRect.size.width *= imageFrame.size.width;
	scrollRect.size.height *= imageFrame.size.height;
	
	[self scrollToPoint:NSMakePoint(NSMidX(scrollRect), NSMidY(scrollRect))];
}

- (NSWindow *)navigatorWindow;
{
	return navigatorWindow;
}

# pragma mark -
# pragma mark Scaling

- (NSPoint)scaledPoint:(NSPoint)point;
{
	return [self convertImagePointToViewPoint:point];	
}


- (NSPoint)unScaledPoint:(NSPoint)point;
{
	return [self convertViewPointToImagePoint:point];
}




# pragma mark -
# pragma mark Accessors


- (CGFloat)scale {
    return scale;
}

- (void)setScale:(CGFloat)value {
    scale = value;
}

@synthesize shouldObserveSelectedImage;


@end
