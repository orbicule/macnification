//
//  LightTableAlignConstraint.m
//  LightTable
//
//  Created by Dennis on 19/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableAlignConstraint.h"
#import "MGFunctionAdditions.h"


@implementation LightTableAlignConstraint


@synthesize direction, coordinate, threshold, type, isActive, zoomLevel;
@dynamic currentCursorPosition;

- (id)init
{
	if ((self = [super init])) {
		
		originalCursorPosition = NSMakePoint(-20000,-20000);
		isActive = YES;
		
	}
	return self;
}

- (void)setCurrentCursorPosition:(NSPoint)position
{
	currentCursorPosition = position;
	
	// if the original position han't been initialized yet, use the given point as orig. position.
	if (originalCursorPosition.x == -20000) originalCursorPosition = position;	
}

- (NSRect)constrainedFrameWithFrame:(NSRect)originalFrame
{
	// return a constrained frame, based on the given frame and properties of self.
	// For move constraints, the new frame differs in origin (e.g. the origin x/y imposed by this constraint is enforced)
	// For resize constraints, the new frame differs in either width or height, but does not contain a modified origin.  This is done by the imageview which knows the necessary current drag control point.
	
	// inactive elements should not hinder the frame change
	if (!self.isActive) return originalFrame;
	
	// do an extra check here to see if we have snapped out....the algorithm we use isn't guaranteed to reach the same check further on.
	if ((direction == HorizontalConstraintDirection && fabs(currentCursorPosition.y - originalCursorPosition.y) > snapOutThreshold / self.zoomLevel)
		 || (direction == VerticalConstraintDirection && fabs(currentCursorPosition.x - originalCursorPosition.x) > snapOutThreshold / self.zoomLevel)) {
		isActive = NO;
	}

	
	NSRect newFrame = originalFrame;

	
	// check the type of constraint.
	if (self.type == MoveAlignConstraint) {
		
		
		// move constraint: adjust ONLY the origin of the image if the image should be snapped.
		// if the type is MoveAlignConstraint, the coordinate value is the desired coordinate value for the ORIGIN, in the direction of this constraint.
				
		NSPoint adjustedOrigin = originalFrame.origin;
		
		if (direction == HorizontalConstraintDirection) {
			
			// if y-value is within threshold value of the coordinate, clip to the coordinate value or snap out.
			if (fabs(originalFrame.origin.y - coordinate) <= threshold / self.zoomLevel) {
				
				// check if the vertical cursor delta (diff current pos - original pos) is above threshold, if so, snap out.
				if (fabs(currentCursorPosition.y - originalCursorPosition.y) > snapOutThreshold / self.zoomLevel) {
					
					adjustedOrigin.y = (currentCursorPosition.y - originalCursorPosition.y > 0) ? coordinate + (threshold + snapOutMargin) / self.zoomLevel : coordinate + (- threshold - snapOutMargin) / self.zoomLevel;
					isActive = NO;
					
				} else {
					adjustedOrigin.y = coordinate;
					
				}
			}
		}
		
		else if (direction == VerticalConstraintDirection) {
			
			if (fabs(originalFrame.origin.x - coordinate) <= threshold / self.zoomLevel) {
				
				// check if the horizontal cursor delta (diff current pos - original pos) is above threshold, if so, snap out.
				if (fabs(currentCursorPosition.x - originalCursorPosition.x) > snapOutThreshold / self.zoomLevel) {
					
					adjustedOrigin.x = (currentCursorPosition.x - originalCursorPosition.x > 0) ? coordinate + (threshold + snapOutMargin) / self.zoomLevel : coordinate + (-threshold - snapOutMargin) / self.zoomLevel;
					isActive = NO;
					
				} else {
					adjustedOrigin.x = coordinate;
					
				}
			}
		}
		
		// apply the new frame
		newFrame.origin = adjustedOrigin;
		
		
	} 
	
	else if (self.type == ResizeAlignConstraint) {
		
		// resize constraint: adjust one of the dimensions of the frame, leave the origin unchanged.
		
		if (direction == HorizontalConstraintDirection) {
			
			// horizontal means: horizontally drawn layout guide.  So change NSMaxY/NSMinY whichever is closer to the threshold value.  
			// This is done by changing the height by the diff between (NSMaxY or NSMinY) and coordinate, whichever is smaller in abs value.
			
			// only do this either of the differences is smaller than the threshold value.
			if (fabs(NSMinY(newFrame) - coordinate) <= threshold / self.zoomLevel || fabs(NSMaxY(newFrame) - coordinate) <= threshold / self.zoomLevel) {
				
				if (fabs(currentCursorPosition.y - originalCursorPosition.y) > snapOutThreshold) {
					
					// the cursor has moved enough, don't do the above but snap out (done in the same manner)
					CGFloat snapOutDiff = (currentCursorPosition.y - originalCursorPosition.y > 0) ? (threshold + snapOutMargin) / self.zoomLevel : (- threshold - snapOutMargin) / self.zoomLevel;
					CGFloat diff = (fabs(NSMinY(newFrame) - coordinate) < fabs(NSMaxY(newFrame) - coordinate)) ? NSMinY(newFrame) - coordinate - snapOutDiff : coordinate + snapOutDiff - NSMaxY(newFrame);
					newFrame.size.height += diff;
					isActive = NO;
					
				} else {
					
					// no snap out, just constrain.
					CGFloat diff = (fabs(NSMinY(newFrame) - coordinate) < fabs(NSMaxY(newFrame) - coordinate)) ? NSMinY(newFrame) - coordinate : coordinate - NSMaxY(newFrame);
					newFrame.size.height += diff;
				}
				
			}
			
		} else if (direction == VerticalConstraintDirection) {
			
			// vertical means: vertically drawn layout guide.  So change NSMaxX/NSMinX whichever is closer to the threshold value.  
			// This is done by changing the width by the diff between (NSMaxX or NSMinX) and coordinate, whichever is smaller in abs value.
			
			// only do this either of the differences is smaller than the threshold value.
			if (fabs(NSMinX(newFrame) - coordinate) < threshold / self.zoomLevel || fabs(NSMaxX(newFrame) - coordinate) < threshold / self.zoomLevel) {
				
				if (fabs(currentCursorPosition.x - originalCursorPosition.x) > snapOutThreshold / self.zoomLevel) {
					
					// the cursor has moved enough, don't do the above but snap out (done in the same manner)
					CGFloat snapOutDiff = (currentCursorPosition.x - originalCursorPosition.x > 0) ? (threshold + snapOutMargin) / self.zoomLevel : (- threshold - snapOutMargin) / self.zoomLevel;
					CGFloat diff = (fabs(NSMinX(newFrame) - coordinate) < fabs(NSMaxX(newFrame) - coordinate)) ? NSMinX(newFrame) - coordinate - snapOutDiff : coordinate + snapOutDiff - NSMaxX(newFrame);
					newFrame.size.width += diff;
					isActive = NO;
					
				} else {
					
					CGFloat diff = (fabs(NSMinX(newFrame) - coordinate) < fabs(NSMaxX(newFrame) - coordinate)) ? NSMinX(newFrame) - coordinate : coordinate - NSMaxX(newFrame);
					newFrame.size.width += diff;
					
				}
				
			}
			
		}
		
	}
	
	return newFrame;
}


- (NSString *)description
{
	// For debug purposes.
	return [NSString stringWithFormat:@"AlignConstraint at %f %@; type:%@, %@ has curr cursor position:%@, init position:%@",self.coordinate, direction == VerticalConstraintDirection ? @"x" : @"y",
			(type == ResizeAlignConstraint) ? @"resize" : @"move",self.isActive ? @"ACTIVE" : @"INACTIVE", NSStringFromPoint(currentCursorPosition), NSStringFromPoint(originalCursorPosition)];
}

@end
