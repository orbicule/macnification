//
//  WorkflowInfoView.h
//  LightTable
//
//  Created by Dennis Lorson on 20/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>


@interface WorkflowInfoView : NSView {

	CATextLayer *backgroundLayer, *textLayer, *closeButtonLayer; 
	
	
}

- (NSRect)setMessage:(NSString *)string;

- (void)relayout;

@end
