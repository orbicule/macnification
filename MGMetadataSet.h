//
//  MGImportMetadata.h
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class Image;


@interface MGMetadataSet : NSObject 
{
	NSMutableDictionary *metadata_;
	NSArray *sortedPlanes_;
}

- (MGMetadataSet *)clone;


- (id)valueForKey:(NSString *)key;
- (void)setValue:(id)value forKey:(NSString *)key;
- (void)removeValueForKey:(NSString *)key;

- (int)numberOfChannels;
- (int)channelForRepresentationAtIndex:(int)index;


// the image given here should be "empty", in that all properties and relationships (except for albums) 
// will be destroyed and replaced by the metadata in this object

- (void)applyToImage:(Image *)image;
- (void)applyToImages:(NSArray *)images;


@end


// --------------------------------------------------------------------------------------------

// METADATA KEYS

// Image

extern NSString * const MGMetadataImageNameKey;
extern NSString * const MGMetadataImageImportDateKey;
extern NSString * const MGMetadataImageCreationDateKey;
extern NSString * const MGMetadataImagePixelSizeXKey;
extern NSString * const MGMetadataImagePhysicalSizeXKey;
extern NSString * const MGMetadataImageSizeXKey;
extern NSString * const MGMetadataImageCommentKey;
extern NSString * const MGMetadataImageSuggestedScaleBarLengthKey;

extern NSString * const MGMetadataImageIntensityRangeMinKey;
extern NSString * const MGMetadataImageIntensityRangeMaxKey;


extern NSString * const MGMetadataImageLittleEndianKey;
extern NSString * const MGMetadataImageTIFFLittleEndianKey;				// these two keys mean the same, but the second one is obtained from the TIFF file (as opposed to metadata)


// Planes

extern NSString * const MGMetadataImagePlanesKey;					// array of dictionaries
extern NSString * const MGMetadataImagePlaneCKey;					
extern NSString * const MGMetadataImagePlaneTKey;
extern NSString * const MGMetadataImagePlaneZKey;
extern NSString * const MGMetadataImagePlaneRepresentationKey;		// index of the corresponding representation in the image


// Instrument

extern NSString * const MGMetadataInstrumentNameKey;
extern NSString * const MGMetadataInstrumentObjectiveNameKey;
extern NSString * const MGMetadataInstrumentWorkingDistanceKey;
extern NSString * const MGMetadataInstrumentImmersionKey;
extern NSString * const MGMetadataInstrumentNumericApertureKey;
extern NSString * const MGMetadataInstrumentMagnificationKey;
extern NSString * const MGMetadataInstrumentVoltageKey;
extern NSString * const MGMetadataInstrumentSpotSizeKey;


// Channel

extern NSString * const MGMetadataNumberOfChannelsKey;
extern NSString * const MGMetadataChannelsKey;

extern NSString * const MGMetadataChannelNameKey;
extern NSString * const MGMetadataChannelWavelengthKey;
extern NSString * const MGMetadataChannelPinholeSizeKey;
extern NSString * const MGMetadataChannelContrastMethodKey;
extern NSString * const MGMetadataChannelModeKey;
extern NSString * const MGMetadataChannelLightSourceNameKey;
extern NSString * const MGMetadataChannelLightSourceTypeKey;
extern NSString * const MGMetadataChannelMediumKey;


// Calibration

extern NSString * const MGMetadataCalibrationHorizontalPixelsForUnitKey;
extern NSString * const MGMetadataCalibrationUnitKey;
extern NSString * const MGMetadataCalibrationPixelWidthKey;
extern NSString * const MGMetadataCalibrationPixelHeightKey;

// Experimenter

extern NSString * const MGMetadataExperimenterNameKey;
