//
//  ROIArrayController.m
//  Macnification
//
//  Created by Peter Schols on 19/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ROIArrayController.h"
#import "NSController_MGAdditions.h"


@implementation ROIArrayController


- (void)awakeFromNib;
{	
	// Sort the ROIs by rank
	//NSSortDescriptor *sortD = [[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES];	
	//[self setSortDescriptors:[NSArray arrayWithObject:sortD]];
	
	// Update the spreasheet menu item in the action menu
	[self updateSpreadsheetMenu];
	
	// Register as an observer for changes in the default spreadsheet application
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSpreadsheetMenu) name:@"spreadsheetApplicationDidChange" object:nil];

	// Set the cursor color in the ROI comments HUD
	[roiCommentTextView setInsertionPointColor:[NSColor whiteColor]];
    
}



// Sent when the ROI selection changes
/*- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	//NSLog(@"ROI selection has changed");
}*/


- (NSArray *)arrangeObjects:(NSArray *)objects
{	
	NSArray *arrangedObjects = [super arrangeObjects:objects];
	
	// batch fault the rois.  This is apparently what the super implementation would do too.
	[self batchFaultObjects:arrangedObjects ofEntity:nil];
	//[self batchFaultObjects:[[arrangedObjects valueForKey:@"rois"] allObjects]];
	
	if ([objects count] < 1000) {
		
	
		NSSortDescriptor *sortD = [[[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES] autorelease];

		arrangedObjects = [arrangedObjects sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortD]];
		
	}
	
	return arrangedObjects;
}

- (void)didChangeValueForKey:(NSString *)key
{
	if ([key isEqualToString:@"arrangedObjects"]) {
		
		// bath fault the rois: we have data that is linked to each other (through summary) so avoid a recursive fetch
		
		if ([self managedObjectContext]) {
			
			[self batchFaultObjects:[self arrangedObjects] ofEntity:@"ROI"];

		}
        
		
	}
	
	[super didChangeValueForKey:key];
	
}

- (void)updateSpreadsheetMenu;
{
	NSString *fullSpreadSheetPath = [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"spreadsheetApplicationPath"];
	
	if (fullSpreadSheetPath) {
		
		NSString *applicationName = [[fullSpreadSheetPath lastPathComponent]stringByDeletingPathExtension];
		NSImage *iconForExternalEditor =  fullSpreadSheetPath ? [[NSWorkspace sharedWorkspace] iconForFile:fullSpreadSheetPath] : nil;
		[iconForExternalEditor setSize:NSMakeSize(16, 16)];
		
		NSMenuItem *menuItem = [actionMenu itemWithTag:5];
		[menuItem setTitle:[NSString stringWithFormat:@"Open ROI List in %@", applicationName]];
		[menuItem setImage:iconForExternalEditor];
		
	} else {
		
		NSMenuItem *menuItem = [actionMenu itemWithTag:5];
		[menuItem setTitle:[NSString stringWithFormat:@"No spreadsheet application set"]];
		[menuItem setEnabled:NO];
		
	}
    
	

}


- (void)showROIHUD;
{
	[roiHud orderFront:self];
	
	
}

- (void)windowWillClose:(NSNotification *)notification
{
	if ([notification object] == roiHud) {
		
		
		
		
	}
	
}

@end
