//
//  MGHUDSlider.m
//  Filament
//
//  Created by Dennis Lorson on 14/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDSlider.h"

#import "MGWidgetAttachedWindow.h"


@interface MGHUDSlider ()

- (void)showTooltipWindowWithAnimation:(BOOL)fadeIn;
- (void)closeToolTip;

@end



@implementation MGHUDSlider

@synthesize delegate;


- (void)setMouseUpAction:(SEL)action
{
	mouseUpSelector = action;
	
}

- (void)setTarget:(id)anObj
{
	[super setTarget:anObj];
	
	target = anObj;
}


- (void)mouseDown:(NSEvent *)theEvent
{	
	[self showTooltipWindowWithAnimation:YES];
        
    if (draggingTimer_) {
        [draggingTimer_ invalidate];
        [draggingTimer_ release];
    }
    
    draggingTimer_ = [[NSTimer timerWithTimeInterval:0.01 target:self selector:@selector(_updateDragging:) userInfo:nil repeats:YES] retain];
    [[NSRunLoop mainRunLoop] addTimer:draggingTimer_ forMode:NSEventTrackingRunLoopMode];
	
    // this starts a dragging event loop...
	[super mouseDown:theEvent];
    
    // mouse up at this point
    [draggingTimer_ invalidate];
    [draggingTimer_ release];
    draggingTimer_ = nil;
    
	[self closeToolTip];
    
    
    if (mouseUpSelector)
		[target performSelector:mouseUpSelector withObject:self];	
}

- (void)_updateDragging:(NSTimer *)timer
{       
    [self showTooltipWindowWithAnimation:NO];    
}

- (void)showTooltipWindowWithAnimation:(BOOL)fadeIn
{
    NSString *title = nil;
    if ([self.delegate respondsToSelector:@selector(valueToolTipForHUDSlider:)])
        title = [self.delegate valueToolTipForHUDSlider:self];
    
    if (!title)
        return;
    
    
    NSRect knobRect = [[self cell] knobRectFlipped:NO];
    
	NSPoint attachPoint;
	attachPoint.x = NSMidX(knobRect);//(aKnob == MGUpperBoundSliderKnob) ? [self locationInBoundsForValue:[self upperBound]] + 30 : [self locationInBoundsForValue:[self lowerBound]] - 30;
	attachPoint.y = NSMaxY(knobRect);
	
	NSPoint pointToAttach = [self convertPoint:attachPoint toView:nil];
		
	// Create the text field we will use to draw the tooltip label
	if (!tooltipTextField) {
		
		tooltipTextField = [[NSTextField alloc] init];
		[tooltipTextField setEditable:NO];
		[tooltipTextField setSelectable:NO];
		[tooltipTextField setDrawsBackground:NO];
		[tooltipTextField setBordered:NO];
		[tooltipTextField setBezeled:NO];
		[tooltipTextField setAlignment:NSCenterTextAlignment];
		[tooltipTextField setFont:[NSFont systemFontOfSize:9]];
		[tooltipTextField setTextColor:[NSColor blackColor]];
		
	}
    
	[tooltipTextField setStringValue:title];
    [tooltipTextField sizeToFit];
    [tooltipTextField setFrameSize:NSMakeSize([tooltipTextField frame].size.width + 5, 12)];
    
	
	// Create the MAAttachedWindow
	if (!tooltipWindow) {
		
		
		NSView *enclosingView = [[[NSView alloc] initWithFrame:NSInsetRect([tooltipTextField frame], -1, 1)] autorelease];
		
		[tooltipTextField setFrameOrigin:NSMakePoint(1, -1)];
		[enclosingView addSubview:tooltipTextField];
		
		
		tooltipWindow = [[MGWidgetAttachedWindow alloc] initWithView:enclosingView attachedToPoint:pointToAttach 
                                                            inWindow:[self window] onSide:MAPositionTop atDistance:3.0];
		[tooltipWindow setReleasedWhenClosed:NO];
		[tooltipWindow setBorderWidth:0.0];
		
	}
	
	if (fadeIn) {
		[tooltipWindow setAlphaValue:0];
		[[tooltipWindow animator] setAlphaValue:1.0];
	} else {
		[tooltipWindow setAlphaValue:1.0];
	}
	
	[tooltipWindow setPoint:pointToAttach];
	
	// Show the window
	[tooltipWindow orderFront:self];
}

- (void)closeToolTip;
{
	if (tooltipWindow) {
		[[tooltipWindow animator] setAlphaValue:0.0];
		[tooltipWindow performSelector:@selector(close) withObject:nil afterDelay:.25];
	}
}



@end
