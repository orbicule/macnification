//
//  MGSlider.m
//  Filament
//
//  Created by Dennis Lorson on 11/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGSlider.h"
#import "MGSliderCell.h"


@implementation MGSlider

+ (Class)cellClass
{
	return [MGSliderCell class];
	
}

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
	BOOL shouldUseSetClass = NO;
	BOOL shouldUseDecodeClassName = NO;
	if ([decoder respondsToSelector:@selector(setClass:forClassName:)]) {
		shouldUseSetClass = YES;
		[(NSKeyedUnarchiver *)decoder setClass:[MGSliderCell class] forClassName:@"NSSliderCell"];
		
	} else if ([decoder respondsToSelector:@selector(decodeClassName:asClassName:)]) {
		shouldUseDecodeClassName = YES;
		[(NSUnarchiver *)decoder decodeClassName:@"NSSliderCell" asClassName:@"progressSliderCell"];
	}
	
	self = [super initWithCoder:decoder];
	
	if (shouldUseSetClass) {
		[(NSKeyedUnarchiver *)decoder setClass:[NSSliderCell class] forClassName:@"NSSliderCell"];
	} else if (shouldUseDecodeClassName) {
		[(NSUnarchiver *)decoder decodeClassName:@"NSSliderCell" asClassName:@"NSSliderCell"];
	}
	
	
	return self;
}

- (BOOL)isFlipped
{
	
	return NO;
	
}


- (void)awakeFromNib
{
	
	[[self cell] setKnobThickness:[[NSImage imageNamed:@"slider_knob"] size].width/2];

}

@end
