//
//  MGMDRatingView.m
//  MetaDataView
//
//  Created by Dennis on 10/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGMDRatingView.h"
#import "ImageArrayController.h"

static CGFloat starMargin = 2.0;
static CGFloat MGMDSideMargin = 0.0;


@implementation MGMDRatingView

@synthesize enabled;


- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
	
    if (self) {
		
		ratingImage = [[NSImage imageNamed:@"ratingStar.tiff"] retain];
		ratingHalfImage = [[NSImage imageNamed:@"ratingStarHalf.tiff"] retain];
		self.enabled = YES;
		
    }
	
    return self;
}


- (void)dealloc
{
	[ratingImage release];
	[ratingHalfImage release];
	
	[super dealloc];
}

- (void)bind:(NSString *)binding toObject:(id)observable withKeyPath:(NSString *)aPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		observableController = observable;
		observableKeyPath = [aPath retain];
		
		[(ImageArrayController *)observableController addManualObserver:self forKeyPath:aPath];
		[observableController addObserver:self forKeyPath:@"selection" options:0 context:nil];

	}
	
	
}

- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		[observableController removeObserver:self forKeyPath:@"selection"];
		[(ImageArrayController *)observableController removeManualObserver:self];

		[observableKeyPath release];
		observableKeyPath = nil;
	}
	else
		[super unbind:binding];
}


- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object
{
	[self observeValueForKeyPath:keyPath ofObject:object change:0 context:nil];
	
	
}

- (void)setEnabled:(BOOL)flag
{
	enabled = flag;
}

- (void) observeValueForKeyPath:(NSString *)aPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (observableController == object) {
		
		id value = [observableController valueForKeyPath:observableKeyPath];
		
		if (value == NSNoSelectionMarker || value == NSNotApplicableMarker) {
			rating = -1;
			isAverage = NO;
			self.enabled = NO;

		} else if (value == NSMultipleValuesMarker) {
			
			// get the average of the keypath set
			NSMutableArray *components = [[[observableKeyPath componentsSeparatedByString:@"."] mutableCopy] autorelease];
			[components insertObject:@"@avg" atIndex:[components count] - 1];
			NSString *newKeyPath = [components componentsJoinedByString:@"."];
			
			NSNumber *avgValue = [observableController valueForKeyPath:newKeyPath];
			
			rating = [avgValue intValue];
			isAverage = YES;
			self.enabled = YES;
			
		} else {
			
			rating = [value intValue];
			isAverage = NO;
			self.enabled = YES;

		}
		[self setNeedsDisplay:YES];
		
	}
	else {
		[super observeValueForKeyPath:aPath ofObject:object change:change context:context];
	}
}


- (NSView *)hitTest:(NSPoint)thePoint
{
	if (self.enabled || ![self superview])
		return [super hitTest:thePoint];
	
	return [self superview];
	
}

- (void)mouseUp:(NSEvent *)theEvent
{
	[super mouseUp:theEvent];
	
}

- (void)mouseDown:(NSEvent *)theEvent
{
	
	if (!self.enabled) {
		
		[super mouseUp:theEvent];
		return;
		
	}
	
	[[self window] makeFirstResponder:self];

	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	[self setValue:[NSNumber numberWithInt:[self ratingForPoint:location]] forKey:@"rating"];
}


- (void)mouseDragged:(NSEvent *)theEvent
{
	if (!self.enabled) {
		
		[super mouseUp:theEvent];
		return;
		
	}
	
	[[self window] makeFirstResponder:self];

	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	//[self willChangeValueForKey:@"rating"];
	[self setValue:[NSNumber numberWithInt:[self ratingForPoint:location]] forKey:@"rating"];
	//[self didChangeValueForKey:@"rating"];

}

- (NSInteger)ratingForPoint:(NSPoint)thePoint
{
	CGFloat starWidth = [(NSBitmapImageRep *)[[ratingImage representations] objectAtIndex:0] pixelsWide];

	if (thePoint.x <= MGMDSideMargin + 1)
		return 0;
	if (thePoint.x <= MGMDSideMargin + 1 * (starWidth + starMargin))
		return 1;
	if (thePoint.x <= MGMDSideMargin + 2 * (starWidth + starMargin))
		return 2;
	if (thePoint.x <= MGMDSideMargin + 3 * (starWidth + starMargin))
		return 3;
	if (thePoint.x <= MGMDSideMargin + 4 * (starWidth + starMargin))
		return 4;
	if (thePoint.x <= MGMDSideMargin + 5 * (starWidth + starMargin))
		return 5;
	
	return [self rating];
	
}

- (BOOL)mouseDownCanMoveWindow
{
	return NO;
}

- (BOOL)acceptsFirstResponder
{
	return YES;
}

- (void)keyDown:(NSEvent *)theEvent
{
	if ([[theEvent characters] characterAtIndex:0] == NSLeftArrowFunctionKey && [self rating] > 0)
		[self setValue:[NSNumber numberWithInt:rating - 1] forKey:@"rating"];
	else if ([[theEvent characters] characterAtIndex:0] == NSRightArrowFunctionKey && [self rating] < 5)
		[self setValue:[NSNumber numberWithInt:rating + 1] forKey:@"rating"];
	else 
		[super keyDown:theEvent];
	
	[self setNeedsDisplay:YES];
	
}

- (BOOL)becomeFirstResponder
{
	BOOL become = [super becomeFirstResponder];
	[self setNeedsDisplay:YES];
	//NSLog(@"%@ become %i", [self description], become);

	return become;
}

- (BOOL)resignFirstResponder
{
	BOOL resign = [super resignFirstResponder];
	[self setNeedsDisplay:YES];
	//NSLog(@"%@ resign %i", [self description], resign);

	return resign;
}

- (void)drawRect:(NSRect)rect 
{
	if ([[self window] firstResponder] == self) {
		NSShadow *shadow = [[[NSShadow alloc] init] autorelease];
		[shadow setShadowColor:[NSColor colorWithCalibratedRed:116.0/255.0 green:160.0/255.0 blue:216.0/255.0 alpha:1.0]]; //116 160 216
		[shadow setShadowBlurRadius:4.0];
		[shadow setShadowOffset:NSMakeSize(0,0)];
		[shadow set];
	}
	
	NSImage *image = isAverage ? ratingHalfImage : ratingImage;
	
    NSPoint currentPoint = NSMakePoint(MGMDSideMargin + .5, 0);
	CGFloat starWidth = [(NSBitmapImageRep *)[[ratingImage representations] objectAtIndex:0] pixelsWide];
	CGFloat starHeight = [(NSBitmapImageRep *)[[ratingImage representations] objectAtIndex:0] pixelsHigh];

	NSInteger adjustedRating = fmax(fmin(rating, 5), 0);
	
	NSInteger currentStar = 0;
	
	
	CGFloat alpha = self.enabled ? 1.0 : 0.3;

	while (currentStar < adjustedRating) {
		[image compositeToPoint:currentPoint operation:NSCompositeSourceOver fraction:alpha];
		
		currentPoint.x += starMargin + starWidth;
		currentStar++;
	}
	
	CGFloat radius = 1.5;
	
	NSPoint bulletCenter = currentPoint;
	bulletCenter.x += starWidth/2.0;
	bulletCenter.y += starHeight/2.0;
	
	NSRect bulletRect = NSMakeRect(bulletCenter.x - radius, bulletCenter.y - radius, 2*radius, 2*radius);
	
	while (currentStar < 5) {
		
		NSBezierPath *bullet = [NSBezierPath bezierPathWithOvalInRect:bulletRect];
		[[NSColor colorWithCalibratedWhite:0.5 alpha:alpha] set];
		[bullet fill];
		[bullet stroke];
		
		
		bulletRect.origin.x += starMargin + starWidth;
		currentStar++;								   
	} 
	
	
}

- (void)setRating:(NSInteger)theRating
{
	if (rating != theRating) {
		
		rating = theRating;
		[observableController setValue:[NSNumber numberWithInt:rating] forKeyPath:observableKeyPath];
		// update the selection images
		//[observableController updateImagesModifiedByExternalEditor];
		[self setNeedsDisplay:YES];
		
	}

}

- (NSInteger)rating
{
	return rating;
}


- (NSString *)contentDescription
{
	if ([self rating] < 0) return @"";
	NSString *stars = @"";
	int i = 1;
	while (i!=6) {
		
		if ([self rating] >= i)
			stars = [stars stringByAppendingString:@"★"];
		else
			stars = [stars stringByAppendingString:@"∙"];
		
		i++;
	}
	return stars;
	
}

- (NSString *)contentValue
{	
	return [NSString stringWithFormat:@"%i", (int)rating];
}
	

@end
