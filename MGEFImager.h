//
//  MGEFICreator.h
//  Filament
//
//  Created by Dennis Lorson on 11/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <QuartzCore/QuartzCore.h>
#import <vImage/vImage.h>

#import "MGImageAlignmentTypes.h"
#import "MGImageBatchImporter.h"

@class MGProgressIndicator;

@interface MGEFImager : NSObject <MGImageBatchImporterDelegate>
{

	IBOutlet NSWindow *efiWindow;
	IBOutlet NSImageView *imageView;
	//IBOutlet NSProgressIndicator *progressIndicator;
	IBOutlet NSButton *saveButton;
	IBOutlet NSButton *cancelButton;
	IBOutlet NSButton *alignmentCheckbox;
	IBOutlet NSButton *colorMatchCheckbox;
	IBOutlet MGProgressIndicator *progressIndicator;
	IBOutlet NSTextField *progressTextField;
	IBOutlet NSView *progressBackgroundView;
	
	IBOutlet NSSlider *sharpenSlider;

	NSMutableArray *imageURLs;
	
	BOOL efiWasCancelled;
	BOOL efiThreadInProgress;

	NSString *stackName;
	
	BOOL alignImages;
	BOOL colorMatchImages;
	
	
	CIImage *referenceImage;
	CIImage *otherImage;
	
	IAImage *referenceImageData;
	IAImage *otherImageData;
	
	IAImage *viewImageData;
    
    NSString *tmpImagePath;
}

@property (nonatomic, copy) NSString *stackName;
@property (nonatomic) BOOL alignImages;
@property (nonatomic) BOOL colorMatchImages;

// INTERFACE FOR STACKVIEW
- (void)showEFISheetForImages:(NSArray *)images;

// UI
- (IBAction)cancelEFI:(id)sender;
- (IBAction)saveResult:(id)sender;
- (IBAction)toggleAlignment:(id)sender;
- (IBAction)toggleColorMatch:(id)sender;
- (IBAction)changeSharpness:(id)sender;


@end
