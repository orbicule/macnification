//
//  MGMDBooleanView.h
//  Filament
//
//  Created by Dennis Lorson on 1/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MGMDView.h"

extern CGFloat MGMDSideMargin;

@interface MGMDBooleanView : NSView <MGMDManualObserver> {
	
	NSImage *yesImage, *noImage, *multValImage;
	
	NSInteger boolValue;

	BOOL enabled;
	
	id observableController;
	NSString *observableKeyPath;
	
}

@property(nonatomic) BOOL enabled;

@end
