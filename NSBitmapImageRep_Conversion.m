//
//  NSBitmapImageRep_PlanarMeshConversion.m
//  Filament
//
//  Created by Dennis Lorson on 22/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "NSBitmapImageRep_Conversion.h"
#import "StringExtensions.h"


@implementation NSBitmapImageRep (Conversion)



# pragma mark -
# pragma mark Channel merge and split


+ (NSBitmapImageRep *)imageRepWithMergedChannels:(NSArray *)channels;
{

	// check if we have at least one valid channel
	NSInteger nbChannels = 0;
	for (NSBitmapImageRep *rep in channels)
		nbChannels += ((id)rep != [NSNull null] ? 1 : 0);
	
	if (nbChannels == 0) {
		NSLog(@"No valid channels found");
		return nil;
	}
	
	NSInteger firstValidChannel = 0;
	while ([[channels objectAtIndex:firstValidChannel] isKindOfClass:[NSNull class]])
		firstValidChannel++;
	
	
	
	// check if all images have the same size
	NSInteger globalW = -1, globalH = -1;
	for (NSBitmapImageRep *rep in channels) {
		
		if ((id)rep != [NSNull null]) {
			
			if (globalW == -1) {
				globalW = [rep pixelsWide];
				globalH = [rep pixelsHigh];				
			} else {
				if (globalH != [rep pixelsHigh] || globalW != [rep pixelsWide]) {
					NSLog(@"Not all image dimensions agree");
					return nil;
				}
			}
		}
	}
	
	// check if all images have the same bps
	NSInteger globalBPS = -1;
	for (NSBitmapImageRep *rep in channels) {
		
		if ((id)rep != [NSNull null]) {
			
			if (globalBPS == -1) {
				globalBPS = [rep bitsPerSample];
			} else {
				if (globalBPS != [rep bitsPerSample]) {
					NSLog(@"Not all image BPS agree");
					return nil;
				}
			}
		}
	}
	
	int globalBytesPerSample = globalBPS >> 3;
	
	// create the new empty image
	NSInteger mergedBps = globalBPS;
	NSInteger mergedBytesPerSample = globalBytesPerSample;
	NSInteger mergedBpr = globalW * 4 * mergedBytesPerSample;
	NSInteger mergedSpp = 4;
	NSBitmapImageRep *merged = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		pixelsWide:globalW
																		pixelsHigh:globalH 
																	 bitsPerSample:mergedBps
																   samplesPerPixel:mergedSpp
																		  hasAlpha:YES
																		  isPlanar:NO
																	colorSpaceName:NSCalibratedRGBColorSpace 
																	  bitmapFormat:0
																	   bytesPerRow:mergedBpr 
																	  bitsPerPixel:mergedBps * 4] autorelease];
	
	if (!merged)
		return nil;
	
	unsigned char *mergedData = [merged bitmapData];
	
	int x, y, b;

	
	
	// make sure that we have all data per image in one plane, then merge it immediately into the destination image
	for (NSBitmapImageRep *rep in channels) {
		
		if ((id)rep != [NSNull null]) {
			
			NSInteger channelIndex = [channels indexOfObject:rep];
			
			NSInteger w = [rep pixelsWide];
			NSInteger h = [rep pixelsHigh];
			NSInteger bpr = [rep bytesPerRow];
			NSInteger spp = [rep samplesPerPixel];
			
			// channel to get the source from
			// always right to choose the channelIndex:
			// - with color images: the channel with useful data is the channel the user has placed this image in (e.g. the red channel source image looks red)
			// - with grayscale three-component images: every component is OK
			// - with grayscale one-component images: will pick the highest possible component == index 0
			NSInteger sourceChannel = MIN(channelIndex, spp - 1);
			
			BOOL alphaFirst = ([rep bitmapFormat] & NSAlphaFirstBitmapFormat) && [rep hasAlpha];
			unsigned char *bitmapData = [rep bitmapData];
			
			for (y = 0; y < h; y++) {
				for (x = 0; x < w; x++) {
					for (b = 0; b < mergedBytesPerSample; b++) {
						//																																			!! look in the correct source channel
						mergedData[y * mergedBpr + (x * mergedSpp + channelIndex) * mergedBytesPerSample + b] = bitmapData[y * bpr + (x * spp + (alphaFirst ? 1 : 0) + sourceChannel) * globalBytesPerSample + b]; 
						
					}
					

					
				}
			}
		}
	}
	
	
	// do the alpha channel
	// this is important: it's used in the merge view
	if ([[channels objectAtIndex:firstValidChannel] hasAlpha]) {
		
		NSBitmapImageRep *rep = [channels objectAtIndex:firstValidChannel];
		
		NSInteger w = [rep pixelsWide];
		NSInteger h = [rep pixelsHigh];
		NSInteger bpr = [rep bytesPerRow];
		NSInteger spp = [rep samplesPerPixel];
		unsigned char *bitmapData = [rep bitmapData];
		
		int offset = [rep samplesPerPixel] - 1;
		BOOL alphaFirst = ([rep bitmapFormat] & NSAlphaFirstBitmapFormat) && [rep hasAlpha];
		
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				for (b = 0; b < mergedBytesPerSample; b++) {
					mergedData[y * mergedBpr + (x * mergedSpp + 3) * mergedBytesPerSample + b] = bitmapData[y * bpr + (x * spp + (alphaFirst ? 0 :offset)) * globalBytesPerSample + b]; 
				}
				
			}
		}
	}
	else {
		
		NSInteger w = globalW;
		NSInteger h = globalH;
		
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				for (b = 0; b < mergedBytesPerSample; b++) {
					mergedData[y * mergedBpr + (x * mergedSpp + 3) * mergedBytesPerSample + b] = 255;
				}
			}
		}
	}
	
	
	
	
	return merged;
}


- (NSArray *)splitChannels;
{
	BOOL splitIntoColorImages = ([[[NSUserDefaults standardUserDefaults] objectForKey:@"splitChannelMethod"] intValue] == 1);
	
	unsigned char *planarData[5];
	for (int i = 0; i < 5; i++)
		planarData[i] = NULL;
	
	[self bitmapData];
	
	NSInteger x, y, i;
	
	//NSInteger nonZeroPixelsInChannel[5] = {0, 0, 0, 0, 0};
	
	NSInteger spp = [self samplesPerPixel];
	NSInteger bpr = [self bytesPerRow];
	NSInteger w = [self pixelsWide];
	NSInteger h = [self pixelsHigh];
	NSInteger bps = [self bitsPerSample];
	
	int bytespersample = bps >> 3;
	int bytesperpixel = [self bitsPerPixel] >> 3;	// may be different from spp * bytespersample


	if ([self isPlanar]) {
		NSLog(@"isPlanar");
		// fill the planar data array with the channels that are given to us by the getDataPlanes method
		[self getBitmapDataPlanes:planarData];
		
	} else {
		
		unsigned char *bitmapData = [self bitmapData];
				

		
		// create the new channels
		for (i = 0; i < spp; i++) {
			planarData[i] = malloc(w * h * sizeof(unsigned char) * bytespersample);
		}
		
		// bugfix 29/04/2010: support for 16-bit and 32-bit spaces
		
		// bugfix 17/09/2010: support for nonstandard bytesperpixel
				
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				for (i = 0; i < spp; i++) {
					
					//if (bitmapData[y * bpr + x * bytesperpixel + i * bytespersample] > 0 && nonZeroPixelsInChannel[i] < 10)
					//	nonZeroPixelsInChannel[i]++;
					
					for (int b = 0; b < bytespersample; b++) {
					
						(planarData[i])[(y * w + x) * bytespersample + b] = bitmapData[y * bpr + x * bytesperpixel + i * bytespersample + b];
					}
				}
			}
		}
	}
	
	
	NSMutableArray *channelReps = [NSMutableArray array];
	
	int numberOfNonEmptyPlanes = 0;
	for (i = 0; i < 5; i++)
		if (planarData[i] != NULL) 	
			numberOfNonEmptyPlanes++;
	
	int dstSPP = splitIntoColorImages ? 3 : 1;
	int dstBiPP = bps * dstSPP;
	int dstByPP = bytespersample * dstSPP;
	
	
	for (i = 0; i < numberOfNonEmptyPlanes; i++) {
		
		// disabled this check; at least now we can be sure there will always be three outputs
		if (NO/*nonZeroPixelsInChannel[i] == 0*/)
			continue;
		
		NSBitmapFormat format = (bps == 32 ? NSFloatingPointSamplesBitmapFormat : 0);
		
		// bugfix 16/12/08: now correctly generates the bpr for 16-bit images
		
		NSBitmapImageRep *imageRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																			  pixelsWide:w
																			  pixelsHigh:h
																		   bitsPerSample:bps
																		 samplesPerPixel:dstSPP
																				hasAlpha:NO
																				isPlanar:splitIntoColorImages ? NO : YES
																		  colorSpaceName:splitIntoColorImages ? NSCalibratedRGBColorSpace : NSCalibratedWhiteColorSpace 
																			bitmapFormat:format
																			 bytesPerRow:w * dstByPP
																			bitsPerPixel:dstBiPP] autorelease];
		
		
		// bugfix 29/04/2010: correct support for 16-bit and 32-bit spaces
		
		unsigned char *newData = [imageRep bitmapData];
		
		int channelIndex = splitIntoColorImages ? i : 0;
		
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				
				newData[(y * w + x) * dstByPP + channelIndex * bytespersample] = (planarData[i])[(y * w + x) * bytespersample];
				
				if (bps >= 16)
					newData[(y * w + x) * dstByPP + channelIndex * bytespersample + 1] = (planarData[i])[(y * w + x) * bytespersample + 1];
				if (bps >= 24)
					newData[(y * w + x) * dstByPP + channelIndex * bytespersample + 2] = (planarData[i])[(y * w + x) * bytespersample + 2];
				if (bps >= 32)
					newData[(y * w + x) * dstByPP + channelIndex * bytespersample + 3] = (planarData[i])[(y * w + x) * bytespersample + 3];
				
			}
		}
		
		[channelReps addObject:imageRep];
		
		
		if (![self isPlanar])
			free(planarData[i]);
		
		
	}
	
	
	return channelReps;
	
}




# pragma mark -
# pragma mark Byte order conversion


- (NSBitmapImageRep *)imageRepWithHostEndiannessFromSourceEndianness:(long)origEndianness;
{
	if (NSHostByteOrder() == origEndianness)
		return self;
	
	unsigned short *data = (unsigned short *)[self bitmapData];
	
	NSInteger spp = [self samplesPerPixel];
	NSInteger bpr = [self bytesPerRow];
	
	NSInteger bps = [self bitsPerSample];
		
	if (bps != 16)
		return self;
		
	int x,y,i;
	
	for (y = 0; y < [self pixelsHigh]; y++) {
		
		for (x = 0; x < [self pixelsWide]; x++) {
			
			unsigned short *addr = &data[y * bpr/2 + x * spp];
			
			for (i = 0; i < spp; i++) {
				
				if (origEndianness == NS_LittleEndian)
					addr[i] = NSSwapLittleShortToHost(addr[i]);
				else
					addr[i] = NSSwapBigShortToHost(addr[i]);
			}
			
		}
		
	}
	return self;
}

# pragma mark -
# pragma mark BPS (depth) conversion


- (NSBitmapImageRep *)imageRepWithAdjustedDepth
{
	unsigned char *oldData = [self bitmapData];

	
	NSInteger oldBps = [self bitsPerSample];
	NSInteger channels = [self samplesPerPixel];
	NSInteger oldSpp = [self samplesPerPixel];
	NSInteger oldBpr = [self bytesPerRow];
	NSInteger w = [self pixelsWide];
	NSInteger h = [self pixelsHigh];

	if (oldBps <= 16 || channels > 1) return self;
	
	//NSLog(@"%@ -- Converting %i bps image", [self description], oldBps);

	NSBitmapImageRep *newRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		pixelsWide:w
																		pixelsHigh:h
																	 bitsPerSample:16
																   samplesPerPixel:1
																		  hasAlpha:NO
																		  isPlanar:NO
																	colorSpaceName:NSCalibratedWhiteColorSpace
																	   bytesPerRow:0
																	  bitsPerPixel:16] autorelease];
	
	
	unsigned char *newData = [newRep bitmapData];

	NSInteger newSpp = [newRep samplesPerPixel];
	NSInteger newBpr = [newRep bytesPerRow];
	
		
	int x,y,i;
	for (y = 0; y < [self pixelsHigh]; y++) {
		
		for (x = 0; x < [self pixelsWide]; x++) {
			
			unsigned char *oldPix = &oldData[y * oldBpr + x * oldSpp * MAX(1, (oldBps >> 3))];
			unsigned char *newPix = &newData[y * newBpr + x * newSpp * 2];
						
			for (i = 0; i < newSpp; i++) {
				
				unsigned char comp = *(oldPix + i * 2 + 1);
				newPix[i] = comp;
				
			}
			
		}
		
	}
	
	return newRep;
	
}

# pragma mark -
# pragma mark Range conversion

- (NSBitmapImageRep *)imageRepWithScaledRange
{
	// update the pixel values so that the extremal values map to [0, 2^depth]
	// endianness should be taken care of here (we assume big endian)
	
	unsigned char *oldData = [self bitmapData];

	
	int bps = [self bitsPerSample];
	int spp = [self samplesPerPixel];
	int bpp = [self bitsPerPixel];
	int bpr = [self bytesPerRow];
	int w = [self pixelsWide];
	int h = [self pixelsHigh];
	
	NSBitmapFormat format = [self bitmapFormat];
	BOOL hasFloatComps = (format & NSFloatingPointSamplesBitmapFormat);
	
	
	// don't want to do this with RGB images...
	if (spp > 1 && bps <= 8) return self;
	
	
	long min = LONG_MAX, max = 0;
	
	
	// find the extremal values
	int x, y, i;
	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			for (i = 0; i < spp; i++) {
				
				unsigned char *oldPixAddr = &oldData[y * bpr + x * (bpp >> 3) + i * MAX(1,(bps >> 3))];
				
				if (bps == 8) {
					
					min = MIN(min, (long)(oldPixAddr[0]));
					max = MAX(max, (long)(oldPixAddr[0]));

					
				} else if (bps == 16) {
					
					min = MIN(min, ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0]);
					max = MAX(max, ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0]);
					
				} else if (bps == 24) {
					
					min = MIN(min, ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0]);
					max = MAX(max, ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0]);
					
				} else if (bps == 32 && !hasFloatComps) {	// int32
					
					min = MIN(min, ((long)oldPixAddr[3] << 24) + ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0]);
					max = MAX(max, ((long)oldPixAddr[3] << 24) + ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0]);
					
				} else {	// float
										
					min = MIN(min, (float)((float *)oldPixAddr)[0]);
					max = MAX(max, (float)((float *)oldPixAddr)[0]);
										
				}
			}
		}
	}
	
	
	double diffinv;
	long maxScaledVal;
	
	if (bps == 32) {
		
		// range in float: [0::1]
		diffinv = 1.0/(double)(max - min);
		maxScaledVal = 1.0;
	
	} else {
		
		diffinv = ((long)(1 << bps) - 1)/(double)(max - min);
		maxScaledVal = ((long)(1 << bps) - 1);
	}

	
	// if range is optimal:
	if (min < 5 && labs(max - (1 << bps)) < 5)  {
		//NSLog(@"Range mapping not needed"); 
		return self;
	}
	
	// if the range is invalid:
	if (labs(max - min) < 5 || min < 0) return self;
	
	//NSLog(@"Mapping range of imported image from %ld to %ld", min, max);
	
	// create the new rep
	// there are colorspace issues so make sure we choose a correct one
	NSBitmapImageRep *newRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		pixelsWide:w
																		pixelsHigh:h
																	 bitsPerSample:bps
																   samplesPerPixel:spp
																		  hasAlpha:[self hasAlpha]
																		  isPlanar:[self isPlanar]
																	colorSpaceName:(spp == 1 ? NSCalibratedWhiteColorSpace : NSCalibratedRGBColorSpace)
																	  bitmapFormat:[self bitmapFormat]
																	   bytesPerRow:bpr
																	  bitsPerPixel:bpp] autorelease];
	
	
	// safety net -- if the new rep could not be created
	if (!newRep) return self;
	
	
	// convert every value
	unsigned char *newData = [newRep bitmapData];

	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			for (i = 0; i < spp; i++) {
				
				unsigned char *oldPixAddr = &oldData[y * bpr + x * (bpp >> 3) + i * MAX(1,(bps >> 3))];
				unsigned char *newPixAddr = &newData[y * bpr + x * (bpp >> 3) + i * MAX(1,(bps >> 3))];
				
				if (bps == 8) {
					
					long oldVal = (long)(oldPixAddr[0]);
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[0] = newVal;
					
				} else if (bps == 16) {
					
					long oldVal = ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[1] = (unsigned char)(newVal>>8);
					newPixAddr[0] = (unsigned char)(newVal & 0x000000ff);
					
					//if (x % 5000 == 0)
					//	NSLog(@"oldVal = %ld, newval = %ld, newpix[1] = %i, newpix[0] = %i", oldVal, newVal, newPixAddr[1], newPixAddr[0]);

					
				} else if (bps == 24) {
					
					long oldVal = ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[2] = (unsigned char)(newVal>>16);
					newPixAddr[1] = (unsigned char)((newVal>>8) & 0x000000ff);
					newPixAddr[0] = (unsigned char)(newVal & 0x000000ff);

					
				} else if (bps == 32 && !hasFloatComps) {	// int32
					
					long oldVal = ((long)oldPixAddr[3] << 24) + ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[3] = (unsigned char)(newVal>>24);
					newPixAddr[2] = (unsigned char)((newVal>>16) & 0x000000ff);
					newPixAddr[1] = (unsigned char)((newVal>>8) & 0x000000ff);
					newPixAddr[0] = (unsigned char)(newVal & 0x000000ff);
					
				} else {			// float
					
					float oldVal = ((float *)oldPixAddr)[0];
					float newVal = MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					((float *)newPixAddr)[0] = newVal;
					
				}
			}
		}
	}
	
	
	return newRep;
}



- (NSBitmapImageRep *)imageRepWithScaledRangeWithMin:(long)min max:(long)max
{
	// update the pixel values so that the extremal values map to [0, 2^depth]
	// endianness should be taken care of here (we assume big endian)
	
	unsigned char *oldData = [self bitmapData];
	
	
	int bps = [self bitsPerSample];
	int spp = [self samplesPerPixel];
	int bpp = [self bitsPerPixel];
	int bpr = [self bytesPerRow];
	int w = [self pixelsWide];
	int h = [self pixelsHigh];
	
	NSBitmapFormat format = [self bitmapFormat];
	BOOL hasFloatComps = (format & NSFloatingPointSamplesBitmapFormat);
	
	
	// don't want to do this with RGB images...
	if (spp > 1 && bps <= 8) return self;
	
	double diffinv;
	long maxScaledVal;
	
	if (bps == 32) {
		
		// range in float: [0::1]
		diffinv = 1.0/(double)(max - min);
		maxScaledVal = 1.0;
		
	} else {
		
		diffinv = ((long)(1 << bps) - 1)/(double)(max - min);
		maxScaledVal = ((long)(1 << bps) - 1);
	}
	
	
	// if range is optimal:
	if (min < 5 && labs(max - (1 << bps)) < 5)  {
		//NSLog(@"Range mapping not needed"); 
		return self;
	}
	
	// if the range is invalid:
	if (labs(max - min) < 5 || min < 0) return self;
	
	//NSLog(@"Mapping range of imported image from %ld to %ld", min, max);
	
	// create the new rep
	// there are colorspace issues so make sure we choose a correct one
	NSBitmapImageRep *newRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		pixelsWide:w
																		pixelsHigh:h
																	 bitsPerSample:bps
																   samplesPerPixel:spp
																		  hasAlpha:[self hasAlpha]
																		  isPlanar:[self isPlanar]
																	colorSpaceName:(spp == 1 ? NSCalibratedWhiteColorSpace : NSCalibratedRGBColorSpace)
																	  bitmapFormat:[self bitmapFormat]
																	   bytesPerRow:bpr
																	  bitsPerPixel:bpp] autorelease];
	
	
	// safety net -- if the new rep could not be created
	if (!newRep) return self;
	
	
	// convert every value
	unsigned char *newData = [newRep bitmapData];
	
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			for (int i = 0; i < spp; i++) {
				
				unsigned char *oldPixAddr = &oldData[y * bpr + x * (bpp >> 3) + i * MAX(1,(bps >> 3))];
				unsigned char *newPixAddr = &newData[y * bpr + x * (bpp >> 3) + i * MAX(1,(bps >> 3))];
				
				if (bps == 8) {
					
					long oldVal = (long)(oldPixAddr[0]);
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[0] = newVal;
					
				} else if (bps == 16) {
					
					long oldVal = ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[1] = (unsigned char)(newVal>>8);
					newPixAddr[0] = (unsigned char)(newVal & 0x000000ff);
					
					//if (x % 5000 == 0)
					//	NSLog(@"oldVal = %ld, newval = %ld, newpix[1] = %i, newpix[0] = %i", oldVal, newVal, newPixAddr[1], newPixAddr[0]);
					
					
				} else if (bps == 24) {
					
					long oldVal = ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[2] = (unsigned char)(newVal>>16);
					newPixAddr[1] = (unsigned char)((newVal>>8) & 0x000000ff);
					newPixAddr[0] = (unsigned char)(newVal & 0x000000ff);
					
					
				} else if (bps == 32 && !hasFloatComps) {	// int32
					
					long oldVal = ((long)oldPixAddr[3] << 24) + ((long)oldPixAddr[2] << 16) + ((long)oldPixAddr[1] << 8) + (long)oldPixAddr[0];
					long newVal = (double)MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					newPixAddr[3] = (unsigned char)(newVal>>24);
					newPixAddr[2] = (unsigned char)((newVal>>16) & 0x000000ff);
					newPixAddr[1] = (unsigned char)((newVal>>8) & 0x000000ff);
					newPixAddr[0] = (unsigned char)(newVal & 0x000000ff);
					
				} else {			// float
					
					float oldVal = ((float *)oldPixAddr)[0];
					float newVal = MIN(MAX(0, (oldVal - min)*diffinv), maxScaledVal);
					((float *)newPixAddr)[0] = newVal;
					
				}
			}
		}
	}
	
	
	return newRep;
	
	
}



@end
