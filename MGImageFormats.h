/*
 *  MGImageFormats.h
 *  Filament
 *
 *  Created by Dennis Lorson on 11/03/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

/** 8-bit unsigned integer (0-255). */
#define MG_IMAGE_GRAY8 0

/**	16-bit signed integer (-32768-32767). Imported signed images
 are converted to unsigned by adding 32768. */
#define MG_IMAGE_GRAY16_SIGNED 1

/** 16-bit unsigned integer (0-65535). */
#define MG_IMAGE_GRAY16_UNSIGNED 2

/**	32-bit signed integer. Imported 32-bit integer images are
 converted to floating-point. */
#define MG_IMAGE_GRAY32_INT 3

/** 32-bit floating-point. */
#define MG_IMAGE_GRAY32_FLOAT 4

/** 8-bit unsigned integer with color lookup table. */
#define MG_IMAGE_COLOR8 5

/** 24-bit interleaved RGB. Import/export only. */
#define MG_IMAGE_RGB 6

/** 24-bit planer RGB. Import only. */
#define MG_IMAGE_RGB_PLANAR 7

/** 1-bit black and white. Import only. */
#define MG_IMAGE_BITMAP 8

/** 32-bit interleaved ARGB. Import only. */
#define MG_IMAGE_ARGB 9

/** 24-bit interleaved BGR. Import only. */
#define MG_IMAGE_BGR 10

/**	32-bit unsigned integer. Imported 32-bit integer images are
 converted to floating-point. */
#define MG_IMAGE_GRAY32_UNSIGNED 11

/** 48-bit interleaved RGB. */
//#define MG_IMAGE_RGB48 12

/** 12-bit unsigned integer (0-4095). Import only. */
//#define MG_IMAGE_GRAY12_UNSIGNED 13

/** 24-bit unsigned integer. Import only. */
//#define MG_IMAGE_GRAY24_UNSIGNED 14

/** 32-bit interleaved BARG (MCID). Import only. */
#define MG_IMAGE_BARG  15

/** 64-bit floating-point. Import only.*/
//#define MG_IMAGE_GRAY64_FLOAT  16

/** 48-bit planar RGB. Import only. */
//#define MG_IMAGE_RGB48_PLANAR 17

/*
// File formats
#define MG_IMAGE_UNKNOWN 0;
#define MG_IMAGE_RAW 1;
#define MG_IMAGE_TIFF 2;
#define MG_IMAGE_GIF_OR_JPG 3;
#define MG_IMAGE_FITS 4;
#define MG_IMAGE_BMP 5;
#define MG_IMAGE_DICOM 6;
#define MG_IMAGE_ZIP_ARCHIVE 7;
#define MG_IMAGE_PGM 8;
#define MG_IMAGE_IMAGEIO 9;

// Compression modes
#define MG_IMAGE_COMPRESSION_UNKNOWN 0;
#define MG_IMAGE_COMPRESSION_NONE= 1;
#define MG_IMAGE_LZW 2;
#define MG_IMAGE_LZW_WITH_DIFFERENCING 3;
#define MG_IMAGE_JPEG 4;
*/
