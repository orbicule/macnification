//
//  LightTableView.h
//  LightTable
//
//	This class is the main view of the light table.
//	It handles mostly coordination tasks between its subviews, and part of the layout guide coordination.
//  It has a tool view (drag rectangle, layout guides) and content view (contains the imageviews and handles most mouse events) as subviews.
//
//  Created by Dennis on 18/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class LightTableContentView;
@class LightTableToolView;
@class LightTableImageView;
@class LightTableCommentView;
@class MGContextualToolStrip;
@class WorkflowInfoPanel;
@class LightTable;
@class LightTableCollageController;


@interface LightTableView : NSView 
{

	IBOutlet NSArrayController *lightTableArrayController, *lightTableImageArrayController;		// the arraycontrollers
	IBOutlet NSArrayController *imageArrayController;											// the main app image controller
	
	
	IBOutlet NSMenu *contextualMenu, *tableContextualMenu;										// the contextual menu for this view and its subviews.
	IBOutlet LightTableCommentView *commentView;
	
	IBOutlet LightTableCollageController *collageController;
			
	IBOutlet NSView *opacitySliderContainer;
	IBOutlet NSSlider *opacitySlider;
	
	IBOutlet MGContextualToolStrip *toolStrip;

	LightTableContentView *contentView;															// the content view subview of this view.  Contains the actual images.
	LightTableToolView *toolView;																// the tool view subview of this view, responsible for drawing layout guides and selection rectangles.
	
	NSMutableArray *layoutElements;																// a copy of the layout elements for each LightTableImageView.
	
	// layout timeout vars
	NSTimer *guideTimeoutTimer;																	// responsible for deactivating drawing of the layout guides when the keyboard has been used.
	
	CGFloat zoomLevel;																			// the zoom level of this view (current bounds relative to LightTable bounds)
	
	BOOL snapsToLayoutGuides;																	// YES if image views should snap to layout guides.
	BOOL printingRectMode;																		// YES when in the mode to select a rectangle destined to be printed.
	LightTable *previouslySelectedLightTable;
	
	
	NSRect contentViewBounds;
	
	NSButton *saveTableButton, *saveSelectionButton, *showAnnotationsButton, *resizeAndRearrangeButton, *rearrangeButton, *removeButton, *opacityHudButton;
	
	BOOL showsScalebarIfAvailable;
	
	IBOutlet NSWindow *opacityHUD;


}

@property(readonly) LightTableContentView *contentView;
@property(nonatomic) BOOL snapsToLayoutGuides, printingRectMode;
@property(readonly) CGFloat zoomLevel;										//easy access for subviews.
@property(nonatomic) BOOL showsScalebarIfAvailable;



- (void)initializeToolStrip;
- (void)updateImageContent;



// Drawing
- (void)setShowsScalebarIfAvailable:(BOOL)flag;



// View Layout
- (void)applyContentViewBoundsFromModel;

- (void)setContentViewBounds:(NSRect)bounds;
- (void)updateContentViewBoundsForImages;
- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize;
- (IBAction)minimizeFrameSize:(id)sender;

- (void)saveFrame:(NSRect)frame toTable:(LightTable *)table;
- (NSRect)loadFrame;

- (NSPoint)loadCenterPoint;
- (void)saveCenterPoint:(NSPoint)centerPoint toTable:(LightTable *)table;
- (NSPoint)contentViewCenterPoint;

- (void)superviewDidResize:(NSNotification *)notification;

- (NSPoint)currentScrollCenterPoint;
- (void)scrollContentViewCenterPoint:(NSPoint)center;
- (void)scrollCenterPoint:(NSPoint)center;



// Layout guidelines

- (void)addAlignLayoutElementForView:(LightTableImageView *)theView;
- (void)removeAlignLayoutElementForView:(LightTableImageView *)theView;

- (void)removeAllLayoutGuides;

- (void)detectLayoutGuidesForFrameChange:(BOOL)move ofView:(LightTableImageView *)theView precision:(NSInteger)precision keepAlive:(BOOL)keepAlive;

- (void)setGuideTimeout:(NSTimeInterval)time;
- (void)disableGuideTimeout;
- (void)guideTimerDidEnd:(NSTimer *)timer;


// Selection rectangle

- (void)updateSelectionRectangle:(NSRect)newRect;



// Zooming + scrolling

- (void)setZoomLevel:(CGFloat)level;
- (CGFloat)zoomLevel;



// View printing

- (NSImage *)compositeImageWithUsedRect;


// Image layout and reordering (programmatically)

- (IBAction)resizeToMatchSelected:(id)sender;

- (IBAction)resizeToRealSize:(id)sender;
- (IBAction)resizeToMatchSelectedUsingMagnification:(id)sender;
- (IBAction)arrangeAllInGrid:(id)sender;

- (IBAction)resizeAndArrange:(id)sender;

- (IBAction)distributeHorizontally:(id)sender;
- (IBAction)distributeVertically:(id)sender;


- (IBAction)alignTopHorizontally:(id)sender;
- (IBAction)alignTopVertically:(id)sender;
- (IBAction)alignBottomHorizontally:(id)sender;
- (IBAction)alignBottomVertically:(id)sender;



// Comment view
- (IBAction)cancelCommentView:(id)sender;

- (IBAction)commitCommentView:(id)sender;
- (IBAction)showCommentViewForSelectedView:(id)sender;

- (IBAction)toggleOpacityHUD:(id)sender;





@end
