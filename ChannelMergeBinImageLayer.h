//
//  ChannelMergeBinImageLayer.h
//  Filament
//
//  Created by Dennis Lorson on 25/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>
#import "Image.h"
#import "ChannelMergeBinView.h"


@interface ChannelMergeBinImageLayer : CATiledLayer {
	
	MergeChannel channel;
	Image *image;
	NSBitmapImageRep *compositeImage;

}


@property (nonatomic, retain) Image *image;
@property (nonatomic, retain) NSBitmapImageRep *compositeImage;
@end
