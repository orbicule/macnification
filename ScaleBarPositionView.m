//
//  ScaleBarPositionView.m
//  Filament
//
//  Created by Peter Schols on 09/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ScaleBarPositionView.h"


@implementation ScaleBarPositionView


- (IBAction)filterArray:(id)sender;
{
	//NSLog(@"%@", [self filterArray]);
}


- (IBAction)reDraw:(id)sender;
{
	[[self overlayForType:IKOverlayTypeImage] setNeedsDisplay];
}



- (void)setScaleBarLayer;
{
	CALayer *theLayer = [CALayer layer];
	[theLayer setDelegate:self];
	[self setOverlay:theLayer forType:IKOverlayTypeImage];
	[theLayer setNeedsDisplay];
}



- (void)drawLayer:(CALayer *)theLayer inContext:(CGContextRef)theContext;
{
	// Get the default settings from the user defaults
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	float xOriginOnImage = [[userDefaults valueForKey:@"scaleBarXProportion"]floatValue] * [self imageSize].width;
	float yOriginOnImage = [[userDefaults valueForKey:@"scaleBarYProportion"]floatValue] * [self imageSize].height;
	float strokeOnImage = [[userDefaults valueForKey:@"scaleBarStroke"] floatValue] / 100.0 * [self imageSize].height;
	float lengthOnImage = 0.2 * [self imageSize].width;
	NSColor *scaleBarColor = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"scaleBarColor"]];
	NSString *scaleBarStyle = [userDefaults valueForKey:@"scaleBarStyle"];
	BOOL drawsLabel = [[userDefaults valueForKey:@"showsScaleBarLabel"]boolValue];
	NSString *fontName = [userDefaults valueForKey:@"scaleBarFontName"];
	float fontSize = [[userDefaults valueForKey:@"scaleBarFontSize"] floatValue] * 0.0015625 * [self imageSize].width;
	
	// Set the current graphics context
	NSGraphicsContext *nsGraphicsContext = [NSGraphicsContext graphicsContextWithGraphicsPort:theContext flipped:NO];
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:nsGraphicsContext];	
	
	NSRect scaleBarRect = NSMakeRect(xOriginOnImage - (lengthOnImage / 2.0), yOriginOnImage, lengthOnImage, strokeOnImage);
	NSRect scaleBarBackgroundRect = scaleBarRect;
	
	if (drawsLabel) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSFont fontWithName:fontName size:fontSize], NSFontAttributeName, scaleBarColor, NSForegroundColorAttributeName, nil];
		NSString *string = @"5 µm";
		NSSize stringSize = [string sizeWithAttributes:dict];
		NSRect labelRect = NSMakeRect(xOriginOnImage - (stringSize.width/2), yOriginOnImage - stringSize.height, stringSize.width, stringSize.height);
		scaleBarBackgroundRect = NSUnionRect(scaleBarBackgroundRect, labelRect);
	}
	
	
	// draw the background
	if ([[userDefaults valueForKey:@"scaleBarUseBackgroundBox"] boolValue]) {
		
		scaleBarBackgroundRect = NSInsetRect(scaleBarBackgroundRect, -5, -5);
		
		NSColor *bgColor = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"scaleBarBackgroundBoxColor"]];
		[bgColor set];
		[NSBezierPath fillRect:scaleBarBackgroundRect];
		
	}
	
	
	// Draw the scale bar
	[scaleBarColor set];
	[NSBezierPath fillRect:scaleBarRect];
	
	
	
	
	
	if ([scaleBarStyle isEqualTo:@"Zebra"]) {
		if ([scaleBarColor isEqualTo:[NSColor blackColor]]) {
			[[NSColor whiteColor] set];
		}
		else {
			[[NSColor blackColor] set];
		}
		NSRect zebraRect = NSOffsetRect(scaleBarRect, 0.0, strokeOnImage/2.0);
		[NSBezierPath fillRect:zebraRect];
	}
	
	// Draw the label
	if (drawsLabel) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSFont fontWithName:fontName size:fontSize], NSFontAttributeName, scaleBarColor, NSForegroundColorAttributeName, nil];
		NSString *string = @"5 µm";
		NSSize stringSize = [string sizeWithAttributes:dict];
		[string drawAtPoint:NSMakePoint(xOriginOnImage - (stringSize.width/2), yOriginOnImage - stringSize.height) withAttributes:dict];
	}
    
	// Restore the graphics state
	[NSGraphicsContext restoreGraphicsState];
}



- (void)mouseDragged:(NSEvent *)event
{
	NSPoint viewPoint = [self convertPoint:[event locationInWindow] fromView:nil];
	NSPoint imagePoint = [self convertViewPointToImagePoint:viewPoint];
	
	// Calculate the proportions and set them in the user defaults
	float xProportion = imagePoint.x / [self imageSize].width;
	float yProportion = imagePoint.y / [self imageSize].height;
	[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:[NSNumber numberWithFloat:xProportion] forKey:@"scaleBarXProportion"];
	[[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:[NSNumber numberWithFloat:yProportion] forKey:@"scaleBarYProportion"];
	
	// Redraw ourselves
	[[self overlayForType:IKOverlayTypeImage] setNeedsDisplay];
}



@end
