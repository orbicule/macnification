//
//  main.m
//  restart
//
//  Created by Dennis Lorson on 04/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    // insert code here...
    NSLog(@"Hello, World!");

    [pool drain];
    return 0;
}

