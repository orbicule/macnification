//
//  MGHUDCheckboxButtonCell.m
//  Filament
//
//  Created by Dennis Lorson on 28/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDCheckboxButtonCell.h"


@interface NSImage (PrivateExtensions)

+ (NSImage *)frameworkImageNamed:(NSString *)string;

@end


@implementation MGHUDCheckboxButtonCell


- (void)drawImage:(NSImage*)image withFrame:(NSRect)frame inView:(NSView*)controlView
{
	NSString *state = [self isHighlighted] ? @"highlighted" : @"normal";
	NSString *position = [self intValue] ? @"on" : @"off";
	NSImage *checkImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_checkbox_%@_%@.tiff", position, state]];
	
	NSSize size = [checkImage size];
	float addX = 2;
	float y = NSMaxY(frame) - (frame.size.height-size.height)/2.0;
	float x = frame.origin.x+addX;
	
	[checkImage compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver];
}




@end
