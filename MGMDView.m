//
//  MGMDView.m
//  MetaDataView
//
//  Created by Dennis on 8/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGMDView.h"
#import "MGMDFieldView.h"
#import "NSView_MGAdditions.h"
#import "MGSetTransformer.h"
#import "MGMDTokenField.h"
#import "MGMDConstants.h"

#import "ImageArrayController.h"
#import "SourceListTreeController.h"
#import "MGAppDelegate.h"

static CGFloat MGMDVerticalInterViewMargin = 5.0;
static CGFloat MGMDSideMargin = 0.0;

static CGFloat MGMDVerticalHeaderMargin = 98;



@interface MGMDView (Private)

- (void)bindFieldToController:(MGMDFieldView *)field;
- (void)unbindFieldFromController:(MGMDFieldView *)field;
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object;

- (void)reloadInsertedFieldsFromPrefs;
- (void)loadBuiltInAttributeMenuItems;
- (void)updateMenuItemStates;

- (NSMenuItem *)menuItemForIdentifier:(NSString *)identifier;
- (MGMDFieldView *)fieldForIdentifier:(NSString *)identifier;


- (void)subviewDidUpdateFrame:(MGMDFieldView *)subview;
- (void)updateFrameSize;
- (void)updateFieldPositionsWithAnimation:(BOOL)useAnimation;

- (void)startDraggingField:(MGMDFieldView *)field withEvent:(NSEvent *)theEvent;

- (NSInteger)field:(NSView *)field positionForPoint:(NSPoint)point;



- (NSString *)tokenField:(NSTokenField *)tokenField displayStringForRepresentedObject:(id)representedObject;
- (NSArray *)tokenField:(NSTokenField *)tokenField completionsForSubstring:(NSString *)substring indexOfToken:(NSInteger)tokenIndex indexOfSelectedItem:(NSInteger *)selectedIndex;
- (id)tokenField:(NSTokenField *)tokenField representedObjectForEditingString:(NSString *)editingString;

- (NSString *)stringForKeyword:(NSManagedObject *)keyword;
- (NSManagedObject *)keywordForString:(NSString *)string;
- (NSString *)childComponentOfString:(NSString *)string;


@end



@implementation MGMDView

@synthesize editModeOn, delegate;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self) {
		
		customMenuItems = [[NSMutableArray array] retain];
		
		id transformer = [[[MGSetTransformer alloc] init] autorelease];
		[NSValueTransformer setValueTransformer:transformer forName:@"MGSetTransformer"];
		
		fields = [[NSMutableArray array] retain];
		draggedField = nil;
		
		[self registerForDraggedTypes:[NSArray arrayWithObjects:@"MGMDFieldType", nil]];
		
		// make separate dicts in order to discern the different attribute groups
		instrumentAttributes = [[NSDictionary dictionaryWithObjectsAndKeys:
								 
								 @"Instrument", @"instrument.instrumentName",
								 @"Magnification", @"instrument.magnification",
								 @"Voltage", @"instrument.voltage",
								 @"Working Distance", @"instrument.workingDistance",
								 @"Spot Size", @"instrument.spotSize",
								 @"Immersion", @"instrument.immersion",
								 @"Objective", @"instrument.objectiveName",
								 @"Numeric Aperture", @"instrument.numericAperture",
								 @"Channels", @"instrument.channels",
								 
								 nil] retain];
		
		imageAttributes = [[NSDictionary dictionaryWithObjectsAndKeys:
							
							@"Comments", @"comment",
							@"Keywords", @"keywords",
							@"Calibrated", @"isCalibrated",
							@"Pixel Size", @"pixelSizeDescription",
							@"Adjustments", @"hasFilters",
							@"ROI Count", @"roiCount",
							@"Rating", @"rating",

							nil] retain];
		
		
		otherAttributes = [[NSDictionary dictionaryWithObjectsAndKeys:
							

							@"Experiment", @"experiment",
							@"Experimenter", @"experimenter.name",
							
							
							nil] retain];
		
		// add these groups into one large dictionary.
		NSMutableDictionary *composedAttributes = [NSMutableDictionary dictionary];
		[composedAttributes addEntriesFromDictionary:instrumentAttributes];
		[composedAttributes addEntriesFromDictionary:imageAttributes];
		[composedAttributes addEntriesFromDictionary:otherAttributes];
		
		builtInAttributes = [composedAttributes copy];
		
		// fields that are inserted on the first app start (without prefs)
		defaultFields = [[NSArray arrayWithObjects:
						  
						  @"comment",
						  @"keywords",
						  @"isCalibrated",
						  @"pixelSizeDescription",
						  @"hasFilters",
						  @"roiCount",
						  @"rating",
						  
						  @"MGSeparator",
						  
						  @"instrument.instrumentName",
						  @"instrument.magnification",
						  @"instrument.voltage",
						  @"instrument.workingDistance",
						  @"instrument.immersion",
						  @"instrument.objectiveName",
						  @"instrument.numericAperture",
						  
						  @"MGSeparator",

						  @"experiment",
						  @"experimenter.name",
						  
						  nil] retain];
		
		insertedFieldIdentifiers = [[NSMutableArray array] retain];
		
		controller = nil;
		
    }
	
    return self;
}

- (void)dealloc
{
	[customMenuItems release];
	
	[fields release];
	[insertedFieldIdentifiers release];
	[builtInAttributes release];
	[defaultFields release];
	[customAttributes release];
	[fieldsRegisteredAsSharedObserver release];
	
	[super dealloc];
}


- (void)awakeFromNib
{
	[[MGCustomMetadataController sharedController] addObserver:self];
	
	
	[self loadBuiltInAttributeMenuItems];
		
	
	NSRect headerFrame = [headerView frame];
	headerFrame.size.width = [self bounds].size.width;
	[headerView setFrame:headerFrame];


	NSRect footerFrame = [footerView frame];
	footerFrame.size.width = [self bounds].size.width;
	[footerView setFrame:headerFrame];
	
	
	[self addSubview:headerView];
	[self addSubview:footerView];
	
	[self refreshCustomAttributesList];
	
	[self reloadInsertedFieldsFromPrefs];
	
	
	[[addFieldButton cell] setUsesItemFromMenu:NO];
	[addFieldButton setImage:[NSImage imageNamed:NSImageNameAddTemplate]];
	[addFieldButton setMenu:addFieldMenu];
		
	
	[[self window] setDelegate:self];
    
    [[self enclosingScrollView] setDrawsBackground:YES];
    [[self enclosingScrollView] setBackgroundColor:[NSColor colorWithDeviceWhite:235.0/255.0 alpha:1.0]];
	
}


- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		controller = observableController;
		
		// this is to change the field values when the selection changes.  Refresh due to model property changes is handled purely manually.
		[controller addObserver:self forKeyPath:@"selection" options:0 context:nil];
				
		[headerView bind:@"value" toObject:observableController withKeyPath:keyPath options:options];
		for (MGMDFieldView *view in fields) {
			
			[self bindFieldToController:view];
			
		}		
		
	} else {
		
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
		
	}
	
	
}


- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		for (MGMDFieldView *view in fields) {
			[self unbindFieldFromController:view];
		}
	
		[controller removeObserver:self forKeyPath:@"selection"];
				
	} else {
		
		[super unbind:binding];
		
	}
	
	
}

- (void)reloadInsertedFieldsFromPrefs
{
	NSArray *identifiers = nil;
	
	if (!(identifiers = [[NSUserDefaults standardUserDefaults] arrayForKey:@"MDActiveFields"]))
		
		identifiers = defaultFields;
	
	
	// load all fields from the preferences.  If no prefs are available, load the default set.
	for (NSString *identifier in [[insertedFieldIdentifiers copy] autorelease]) {
		[self removeFieldWithIdentifier:identifier];
	}

	
	for (NSString *identifier in identifiers) {
		
		// will not be added if the identifier is not a key in customAttributes.
		[self addFieldWithIdentifier:identifier];
		
	}
	
}


- (void)saveFieldsToPrefs
{	
	NSMutableArray *identifiers = [NSMutableArray array];
	
	for (MGMDFieldView *field in fields) {
		
		[identifiers addObject:field.fieldIdentifier];
		
	}
	
	[[NSUserDefaults standardUserDefaults] setObject:[identifiers copy] forKey:@"MDActiveFields"];
	
}


- (void)bindFieldToController:(MGMDFieldView *)field
{
	[field bind:@"value" toObject:controller withKeyPath:[@"selection." stringByAppendingString:field.fieldIdentifier] options:nil];
}



- (void)unbindFieldFromController:(MGMDFieldView *)field
{				
	[field unbind:@"value"];		
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object == controller && [keyPath isEqualToString:@"selection"]) {
			
	} 
	else if (object == controller) {
		
		
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object
{
	
	
}




#pragma mark -
#pragma mark Accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (MGMDChannelView *)channelView
{
	return channelView;
	
}

#pragma mark -
#pragma mark Field menus
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Field menus
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)nonNumericEqualStringWithQuantity:(NSString *)quantityDesc property:(NSString *)propertyDesc
{
	return [NSString stringWithFormat:@"Search for images with %@ as %@", quantityDesc, propertyDesc];
	
}

- (NSString *)numericEqualStringWithQuantity:(NSString *)quantityDesc property:(NSString *)propertyDesc
{
	return [NSString stringWithFormat:@"Search for images with %@ equal to %@", propertyDesc, quantityDesc];
	
}

- (NSString *)numericSmallerStringWithQuantity:(NSString *)quantityDesc property:(NSString *)propertyDesc
{
	return [NSString stringWithFormat:@"Search for images with %@ less than %@", propertyDesc, quantityDesc];
	
}

- (NSString *)numericLargerStringWithQuantity:(NSString *)quantityDesc property:(NSString *)propertyDesc
{
	return [NSString stringWithFormat:@"Search for images with %@ more than %@", propertyDesc, quantityDesc];
	
}


- (void)searchNonNumericEqual:(id)sender
{
	MGMDFieldView *field = [sender representedObject];
	NSString *value = [field contentValue];
	NSString *identifier = field.fieldIdentifier;
	
	// manual change for some "difficult" fields...
	if ([identifier isEqualToString:@"experiment"])
		identifier = @"experiment.experimentName";
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like %@", identifier, value];
	[MGLibraryControllerInstance searchCurrentFolderWithPredicate:predicate naturalLanguageString:[sender title]];
	//NSLog(@"Executed predicate: %@ like %@", identifier, value);
	
}

- (void)searchNumericEqual:(id)sender
{
	// need to adjust the content description string: format into a numeric!
	// as we're doing comparisons here, a float value should always suffice.
	MGMDFieldView *field = [sender representedObject];
	NSString *value = [field contentValue];
	NSString *identifier = field.fieldIdentifier;

	CGFloat floatValue = [value floatValue];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %f", identifier, floatValue];
	[MGLibraryControllerInstance searchCurrentFolderWithPredicate:predicate naturalLanguageString:[sender title]];
	//NSLog(@"Executed predicate: %@ = %f", identifier, floatValue);
	
	
	
}

- (void)searchNumericSmaller:(id)sender
{
	// need to adjust the content description string: format into a numeric!
	// as we're doing comparisons here, a float value should always suffice.
	MGMDFieldView *field = [sender representedObject];
	NSString *value = [field contentValue];
	NSString *identifier = field.fieldIdentifier;
	
	CGFloat floatValue = [value floatValue];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K < %f", identifier, floatValue];
	[MGLibraryControllerInstance searchCurrentFolderWithPredicate:predicate naturalLanguageString:[sender title]];
	//NSLog(@"Executed predicate: %@ > %f", identifier, floatValue);
	
	
}

- (void)searchNumericLarger:(id)sender
{
	// need to adjust the content description string: format into a numeric!
	// as we're doing comparisons here, a float value should always suffice.
	MGMDFieldView *field = [sender representedObject];
	NSString *value = [field contentValue];
	NSString *identifier = field.fieldIdentifier;
	
	CGFloat floatValue = [value floatValue];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K > %f", identifier, floatValue];
	[MGLibraryControllerInstance searchCurrentFolderWithPredicate:predicate naturalLanguageString:[sender title]];
	//NSLog(@"Executed predicate: %@ < %f", identifier, floatValue);
	
	
}

- (NSMenu *)menuForContentsOfField:(MGMDFieldView *)field withIdentifier:(NSString *)identifier
{
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11], NSFontAttributeName, nil];
	
	[menu setAutoenablesItems:NO];

	
	
	// from here on, a content description will be neccessary...
	
	BOOL validContents = ([[field contentDescription] length] > 0);
	
	NSString *contentDescription = validContents ? [field contentDescription] : @"...";
	
	id filterController = controller;
	
	// these are the identifiers that do not rely on content descriptions
	
	
	if ([identifier isEqualToString:@"roiCount"]) {
		
		NSMenuItem *copy = [[[NSMenuItem alloc] init] autorelease];
		[copy setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Copy All ROIs" attributes:attrs] autorelease]];
		[copy setTarget:filterController];
		[copy setAction:@selector(copyROIs:)];
		
		[copy setEnabled:[[field contentDescription] floatValue] > 0];
		
		[menu addItem:copy];
		
		
		NSMenuItem *paste = [[[NSMenuItem alloc] init] autorelease];
		[paste setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Paste ROIs" attributes:attrs] autorelease]];
		[paste setTarget:filterController];
		[paste setAction:@selector(pasteROIs:)];
		
		[paste setEnabled:[[controller selectedObjects] count] > 0];

		
		[menu addItem:paste];
		
		NSMenuItem *remove = [[[NSMenuItem alloc] init] autorelease];
		[remove setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Remove All ROIs" attributes:attrs] autorelease]];
		[remove setTarget:filterController];
		[remove setAction:@selector(removeAllROIs:)];
		
		[remove setEnabled:[[controller selectedObjects] count] > 0 && [[field contentDescription] floatValue] > 0];

		
		[menu addItem:remove];
		
		
		return menu;
	} 	
	
	if ([identifier isEqualToString:@"isCalibrated"]) {
		
		NSMenuItem *copy = [[[NSMenuItem alloc] init] autorelease];
		[copy setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Copy Calibration" attributes:attrs] autorelease]];
		[copy setTarget:filterController];
		[copy setAction:@selector(copyCalibration:)];
		
		[copy setEnabled:[[field contentDescription] isEqualToString:@"YES"]];
		
		[menu addItem:copy];
		
		
		NSMenuItem *paste = [[[NSMenuItem alloc] init] autorelease];
		[paste setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Paste Calibration" attributes:attrs] autorelease]];
		[paste setTarget:filterController];
		[paste setAction:@selector(pasteCalibration:)];
		
		[paste setEnabled:[[controller selectedObjects] count] > 0];

		
		[menu addItem:paste];
		
		NSMenuItem *remove = [[[NSMenuItem alloc] init] autorelease];
		[remove setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Remove Calibration" attributes:attrs] autorelease]];
		[remove setTarget:filterController];
		[remove setAction:@selector(removeCalibration:)];
		
		[remove setEnabled:[[field contentDescription] isEqualToString:@"YES"]];

		
		[menu addItem:remove];
		
		
		// add existing named calibrations
		NSManagedObjectContext *ctx = [MGLibraryControllerInstance managedObjectContext];
		NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
		[req setEntity:[NSEntityDescription entityForName:@"Calibration" inManagedObjectContext:ctx]];
		[req setPredicate:[NSPredicate predicateWithFormat:@"name != '' AND name != nil"]];
		[req setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
		
		NSArray *calibrations = [ctx executeFetchRequest:req error:nil];
		
		if ([calibrations count] == 0)
			return menu;
		
		
		NSMenuItem *sep = [NSMenuItem separatorItem];
		[menu addItem:sep];
		
		
		for (id calibration in calibrations) {
			NSMenuItem *item = [[[NSMenuItem alloc] init] autorelease];
			[item setAttributedTitle:[[[NSAttributedString alloc] initWithString:[calibration valueForKey:@"name"] attributes:attrs] autorelease]];
			[item setRepresentedObject:calibration];
			[item setTarget:filterController];
			[item setAction:@selector(applyCalibration:)];
			
			[menu addItem:item];
		}
		
		
		return menu;
	}
	
	
	if ([identifier isEqualToString:@"hasFilters"]) {
		
		NSMenuItem *copy = [[[NSMenuItem alloc] init] autorelease];
		[copy setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Copy All Adjustments" attributes:attrs] autorelease]];
		[copy setTarget:filterController];
		[copy setAction:@selector(copyFilters:)];
		
		[copy setEnabled:[[field contentDescription] isEqualToString:@"YES"]];
		
		[menu addItem:copy];
		
		
		NSMenuItem *paste = [[[NSMenuItem alloc] init] autorelease];
		[paste setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Paste Adjustments" attributes:attrs] autorelease]];
		[paste setTarget:filterController];
		[paste setAction:@selector(pasteFilters:)];

		[paste setEnabled:[[controller selectedObjects] count] > 0];

		
		[menu addItem:paste];
		
		NSMenuItem *remove = [[[NSMenuItem alloc] init] autorelease];
		[remove setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Remove All Adjustments" attributes:attrs] autorelease]];
		[remove setTarget:filterController];
		[remove setAction:@selector(removeAllFilters:)];
		
		
		[remove setEnabled:[[controller selectedObjects] count] > 0];
		
		[menu addItem:remove];
		
		return menu;
	} 
	
	if ([identifier isEqualToString:@"keywords"]) {
		
		NSMenuItem *copy = [[[NSMenuItem alloc] init] autorelease];
		[copy setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Copy All Keywords" attributes:attrs] autorelease]];
		[copy setTarget:filterController];
		[copy setAction:@selector(copyKeywords:)];
		
		[copy setEnabled:[[controller selectedObjects] count] > 0 && [[field contentDescription] length] > 0];
		
		[menu addItem:copy];
		
		
		NSMenuItem *paste = [[[NSMenuItem alloc] init] autorelease];
		[paste setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Paste Keywords" attributes:attrs] autorelease]];
		[paste setTarget:filterController];
		[paste setAction:@selector(pasteKeywords:)];
		
		[paste setEnabled:[[controller selectedObjects] count] > 0];

		[menu addItem:paste];
		
		
		NSMenuItem *remove = [[[NSMenuItem alloc] init] autorelease];
		[remove setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Remove All Keywords" attributes:attrs] autorelease]];
		[remove setTarget:filterController];
		[remove setAction:@selector(removeAllKeywords:)];
		
		[remove setEnabled:[[controller selectedObjects] count] > 0 && [[field contentDescription] length] > 0];
		
		[menu addItem:remove];
		
		
		NSMenuItem *separator = [NSMenuItem separatorItem];
		[menu addItem:separator];
		

		NSMenuItem *manage = [[[NSMenuItem alloc] init] autorelease];
		[manage setAttributedTitle:[[[NSAttributedString alloc] initWithString:@"Manage Keywords" attributes:attrs] autorelease]];
		[manage setTarget:MGLibraryControllerInstance];
		[manage setAction:@selector(toggleKeywordPanel:)];
		
		[menu addItem:manage];
		
		
		return menu;
	} 
	
	
	else if ([identifier isEqualToString:@"experiment"]) {
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"experiment"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[searchEqual setEnabled:validContents];
		
		[menu addItem:searchEqual];
		
		
	} 
	
	
	else if ([identifier rangeOfString:@"customMetadata"].location != NSNotFound) {
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"value"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[searchEqual setEnabled:validContents];
		
		[menu addItem:searchEqual];
		
		
	} 

	else if ([identifier isEqualToString:@"comment"]) {
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"comments"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[searchEqual setEnabled:validContents];

		
		[menu addItem:searchEqual];
		
		NSMenuItem *separator = [NSMenuItem separatorItem];
		[menu addItem:separator];
		
		NSString *batchTitle = @"Append Comment...";

		
		NSMenuItem *batchComment = [[[NSMenuItem alloc] init] autorelease];
		[batchComment setAttributedTitle:[[[NSAttributedString alloc] initWithString:batchTitle attributes:attrs] autorelease]];
		
		[batchComment setRepresentedObject:field];
		[batchComment setAction:@selector(appendBatchCommentToSelectedImages:)];
		[batchComment setTarget:controller];
		
		[batchComment setEnabled:[[controller selectedObjects] count] > 0];

		[menu addItem:batchComment];
	} 
	
	
	else if ([identifier isEqualToString:@"rating"]) {
		
		NSString *propertyDescription = @"rating";

		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		

		
		[menu addItem:searchEqual];
		
		

		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		


		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];
	
	}
	

	else if ([identifier isEqualToString:@"roiCount"]) {
		
		NSString *propertyDescription = @"# of ROIs";
		
		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];
		
		
	}

	
	
	else if ([identifier isEqualToString:@"experimenter.name"]) {
		
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"experimenter"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		[searchEqual setEnabled:validContents];
	
	}
	
	
	else if ([identifier isEqualToString:@"instrument.immersion"]) {
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"immersion"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		[searchEqual setEnabled:validContents];

	}
	
	
	
	else if ([identifier isEqualToString:@"instrument.instrumentName"]) {
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"instrument"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		[searchEqual setEnabled:validContents];

	
	}
	
	
	else if ([identifier isEqualToString:@"instrument.objectiveName"]) {
		
		NSString *equalTitle = [self nonNumericEqualStringWithQuantity:contentDescription property:@"objective"];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNonNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		[searchEqual setEnabled:validContents];
	}
	
	
	else if ([identifier isEqualToString:@"instrument.magnification"]) {
		
		NSString *propertyDescription = @"magnification";
		
		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];

	}
	
	
	else if ([identifier isEqualToString:@"instrument.numericAperture"]) {
		
		
		NSString *propertyDescription = @"numeric aperture";
		
		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];
		

	}
	
	else if ([identifier isEqualToString:@"instrument.voltage"]) {
		
		
		NSString *propertyDescription = @"voltage";
		
		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];

	}
	
	else if ([identifier isEqualToString:@"instrument.spotSize"]) {
		

		NSString *propertyDescription = @"spot size";
		
		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];
	}
	
	else if ([identifier isEqualToString:@"instrument.workingDistance"]) {
		
		NSString *propertyDescription = @"working distance";
		
		
		NSString *equalTitle = [self numericEqualStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchEqual = [[[NSMenuItem alloc] init] autorelease];
		[searchEqual setAttributedTitle:[[[NSAttributedString alloc] initWithString:equalTitle attributes:attrs] autorelease]];
		
		[searchEqual setRepresentedObject:field];
		[searchEqual setAction:@selector(searchNumericEqual:)];
		[searchEqual setTarget:self];
		
		[menu addItem:searchEqual];
		
		NSString *largerTitle = [self numericLargerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchLarger = [[[NSMenuItem alloc] init] autorelease];
		[searchLarger setAttributedTitle:[[[NSAttributedString alloc] initWithString:largerTitle attributes:attrs] autorelease]];
		
		[searchLarger setRepresentedObject:field];
		[searchLarger setAction:@selector(searchNumericLarger:)];
		[searchLarger setTarget:self];
		
		[menu addItem:searchLarger];
		
		NSString *smallerTitle = [self numericSmallerStringWithQuantity:contentDescription property:propertyDescription];
		
		NSMenuItem *searchSmaller = [[[NSMenuItem alloc] init] autorelease];
		[searchSmaller setAttributedTitle:[[[NSAttributedString alloc] initWithString:smallerTitle attributes:attrs] autorelease]];
		
		[searchSmaller setRepresentedObject:field];
		[searchSmaller setAction:@selector(searchNumericSmaller:)];
		[searchSmaller setTarget:self];
		
		[menu addItem:searchSmaller];
		
		[searchEqual setEnabled:validContents];
		[searchSmaller setEnabled:validContents];
		[searchLarger setEnabled:validContents];
	}
	
	else {
		
		
	}

	return menu;
	

}



#pragma mark -
#pragma mark Modification
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Modification
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)removeFieldWithIdentifier:(NSString *)identifier
{
	MGMDFieldView *field = [self fieldForIdentifier:identifier];
		
	[field unbind:@"value"];
	[fields removeObject:field];
	
	[field removeFromSuperview];
	
	[self updateMenuItemStates];
	
	[self updateFieldPositionsWithAnimation:YES];
	
	[self updateFrameSize];

	
}


- (MGMDFieldView *)addFieldWithIdentifier:(NSString *)identifier
{
	if ([identifier isEqualToString:@"MGSeparator"]) {
		
		NSView *newView = [self addSeparator];
		[newView bind:@"value" toObject:controller withKeyPath:[@"selection." stringByAppendingString:identifier] options:nil];
		return (MGMDFieldView *)newView;
	}
	
	// if the custom attribute hasn't been loaded yet but exists in the prefs, ignore it.  We'll update from prefs later again, when the custom attrs are set.
	if (![builtInAttributes valueForKey:identifier] && ![customAttributes valueForKey:identifier]) return nil;
	
	// use the value from the customAttrs dictionary as the title for this identifier. Will be nil if a builtIn attr., is then handled (hardcoded) by the FieldView.
	MGMDFieldView *newView = [[[MGMDFieldView alloc] initWithIdentifier:identifier title:[customAttributes valueForKey:identifier]] autorelease];
	
	newView.titleMenu = [newView fieldMenu];
	
	[newView setFrameSize:NSMakeSize([self bounds].size.width - 2 * MGMDSideMargin, [newView frame].size.height)];
	[newView setAutoresizingMask:(NSViewWidthSizable | NSViewMaxYMargin)];
	
	// important: when controller is still nil, items will be bound after the controller is set.
	if (controller) {
		
		[self bindFieldToController:newView];
		
	}
	
	[insertedFieldIdentifiers addObject:newView];
	
	
	[fields addObject:newView];
	
	[self updateFieldPositionsWithAnimation:NO];
	
	[self addSubview:newView];
	
	for (MGMDFieldView *view in fields) {
		[view updateLabelEdge];
		[view updateContentsSize];
	}
	
	newView.editModeOn = self.editModeOn;
	
	[self updateFrameSize];
	
	[self updateMenuItemStates];
	
	return newView;
}


- (void)addField:(id)sender
{
	NSString *key = [(NSMenuItem *)sender representedObject];
	
	if ([insertedFieldIdentifiers containsObject:key]) {
		
		// the item is active in the view, so delete
		[self removeFieldWithIdentifier:key];
		
	} else {
		
		[self addFieldWithIdentifier:key];
		
	}
	
	[self saveFieldsToPrefs];
	
	[self updateFrameSize];
}

- (IBAction)addSeparator:(id)sender
{
	[self addSeparator];
	[self saveFieldsToPrefs];
	
}


- (MGMDFieldView *)addSeparator
{
	
	MGMDFieldView *newView = [[[MGMDFieldView alloc] initAsSeparator] autorelease];
	[newView setFrameSize:NSMakeSize([self bounds].size.width - 2 * MGMDSideMargin, [newView frame].size.height)];
	
	[fields addObject:newView];
	
	[self updateFieldPositionsWithAnimation:NO];
	
	[self addSubview:newView];
	
	[self updateFrameSize];
	
	
	newView.editModeOn = self.editModeOn;
		
	return newView;
	
}

- (void)removeLastField
{
	MGMDFieldView *newView = [fields lastObject];
	
	[fields removeObject:newView];
	
	[newView removeFromSuperview];
	
	[self updateFieldPositionsWithAnimation:NO];
	
	for (MGMDFieldView *view in fields) {
		[view updateLabelEdge];
		[view updateContentsSize];
	}
	
	[self updateFrameSize];

}


- (IBAction)toggleEditMode:(id)sender
{
	self.editModeOn = !self.editModeOn;
}

- (IBAction)endEditingMode:(id)sender
{
	if (self.editModeOn) {
		self.editModeOn = NO;
		[toggleEditModeButton setState:NSOffState];
		
	}
	

}



- (void)setEditModeOn:(BOOL)flag
{
	editModeOn = flag;
	
	[self setWantsLayer:flag];
	[CATransaction setValue:[NSNumber numberWithBool:YES] forKey:kCATransactionDisableActions];
	[CATransaction setValue:[NSNumber numberWithFloat:0.0] forKey:kCATransactionAnimationDuration];
	[self layer].backgroundColor = CGColorCreateGenericGray(237.0/255.0, 1.0);
	
	//[self setWantsLayer:flag];
	
	for (MGMDFieldView *view in fields) {
		
		view.editModeOn = flag;
		
	}
	
}



- (void)loadBuiltInAttributeMenuItems
{
	// load the placeholder item indexes before every menu item is inserted so we know where exactly to insert the menu items.
	
	NSArray *desc = [NSArray arrayWithObject:[[[NSSortDescriptor alloc] initWithKey:@"" ascending:YES] autorelease]];
	
	NSInteger startIndex;
	
	// the "other" items
	startIndex = [[otherPlaceholderMenuItem menu] indexOfItem:otherPlaceholderMenuItem] + 1;
	
	for (NSString *key in [[otherAttributes allKeys] sortedArrayUsingDescriptors:desc]) {
		
		NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:[otherAttributes valueForKey:key] action:@selector(addField:) keyEquivalent:@""] autorelease];
		[item setTarget:self];
		[item setRepresentedObject:key];
		[addFieldMenu insertItem:item atIndex:startIndex];
		
		startIndex++;
	}
			
	NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:@"Separator" action:@selector(addSeparator:) keyEquivalent:@""] autorelease];
	[item setTarget:self];
	[addFieldMenu insertItem:item atIndex:startIndex];
	
	
	
	// the "image" items
	
	startIndex = [[imagePlaceholderMenuItem menu] indexOfItem:imagePlaceholderMenuItem] + 1;
	
	for (NSString *key in [[imageAttributes allKeys] sortedArrayUsingDescriptors:desc]) {
		
		NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:[imageAttributes valueForKey:key] action:@selector(addField:) keyEquivalent:@""] autorelease];
		[item setTarget:self];
		[item setRepresentedObject:key];
		[addFieldMenu insertItem:item atIndex:startIndex];
		
		startIndex++;
	}
	
	
	// the "instrument" items
	
	startIndex = [[instrumentPlaceholderMenuItem menu] indexOfItem:instrumentPlaceholderMenuItem] + 1;
	
	for (NSString *key in [[instrumentAttributes allKeys] sortedArrayUsingDescriptors:desc]) {
		
		NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:[instrumentAttributes valueForKey:key] action:@selector(addField:) keyEquivalent:@""] autorelease];
		[item setTarget:self];
		[item setRepresentedObject:key];
		[addFieldMenu insertItem:item atIndex:startIndex];
		
		startIndex++;
	}
	
	
	
	
}

- (NSMenuItem *)menuItemForIdentifier:(NSString *)identifier
{
	for (NSMenuItem *item in [addFieldMenu itemArray]) {
		if ([[item representedObject] isEqualToString:identifier])
			return item;
		
	}
	return nil;
	
}

- (MGMDFieldView *)fieldForIdentifier:(NSString *)identifier
{
	for (MGMDFieldView *view in fields) {
		
		if ([view.fieldIdentifier isEqualToString:identifier])
			return view;
		
	}
	return nil;
	
}

- (void)updateMenuItemStates
{
	[insertedFieldIdentifiers removeAllObjects];
	
	for (NSMenuItem *item in [addFieldMenu itemArray]) {
		
		[item setState:NSOffState];
		
	}
	
	for (MGMDFieldView *field in fields) {
		
		[insertedFieldIdentifiers addObject:field.fieldIdentifier];
		NSMenuItem *associatedItem = [self menuItemForIdentifier:field.fieldIdentifier];
		[associatedItem setState:NSOnState];
		
	}
	
}


#pragma mark -
#pragma mark Custom metadata
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Custom metadata
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)ctl;
{
	[self refreshCustomAttributesList];

	for (NSDictionary *customAttr in [ctl customAttributes]) {
		
		if ([self fieldForIdentifier:[customAttr objectForKey:@"path"]]) {
			
			// exists; do nothing
			
		}
		
		else {
			
			// create a new field and add it
			
			[self addFieldWithIdentifier:[customAttr objectForKey:@"path"]];
			
			[self saveFieldsToPrefs];
			
		}
	}
	
	
	// there may have been a delete	
	[self saveFieldsToPrefs];
	
}



- (void)refreshCustomAttributesList
{	
	[self setCustomAttributes:[[MGCustomMetadataController sharedController] customAttributes]];	
}

- (void)setCustomAttributes:(NSArray *)attrs
{
	// add a given dictionary of items to the POSSIBLE addable items in the MDView menu (they are not added as a field until the user does so)
	
	// convert the array of dictionaries to a single dictionary
	NSMutableDictionary *customAttrs = [NSMutableDictionary dictionary];
	for (NSDictionary *dictionary in attrs)
		[customAttrs setObject:[dictionary objectForKey:@"name"] forKey:[dictionary objectForKey:@"path"]];
	
	// first, remove the old items from the menu and view (if any)
	NSMutableArray *itemsToRemove = [NSMutableArray array];
	
	for (NSMenuItem *item in customMenuItems) {
		
		BOOL alsoInNewAttributes = NO;
		
		for (NSString *key in [customAttrs allKeys]) {
			
			if ([key isEqualToString:[item representedObject]]) alsoInNewAttributes = YES;
			
		}
		
		if (!alsoInNewAttributes) [itemsToRemove addObject:item];
	}
	
	for (NSMenuItem *item in itemsToRemove) {
		
		[customMenuItems removeObject:item];
		[addFieldMenu removeItem:item];
		[self removeFieldWithIdentifier:[item representedObject]];
		
	}
	
	
	
	
	// now, determine the list of new items (that weren't in the menu before)
	
	NSMutableArray *keysToAdd = [NSMutableArray array];
	
	for (NSString *key in [customAttrs allKeys]) {
		
		BOOL notInMenuYet = YES;
		
		for (NSMenuItem *item in customMenuItems) {
			
			if ([[item representedObject] isEqualToString:key]) notInMenuYet = NO;
			
		}
		
		if (notInMenuYet) [keysToAdd addObject:key];
		
	}
	
	
	
	
	if (customAttrs != customAttributes) {
		
		[customAttributes release];
		customAttributes = [customAttrs retain];
		
	}
	
	
	for (NSString *key in keysToAdd) {
		// add the menu items belonging to these keys
		NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:[customAttrs valueForKey:key] action:@selector(addField:) keyEquivalent:@""] autorelease];
		[item setTarget:self];
		[item setRepresentedObject:key];
		[customMenuItems addObject:item];
		[addFieldMenu addItem:item];
		
	}
	
	[self reloadInsertedFieldsFromPrefs];
	
}


#pragma mark -
#pragma mark Mouse events + drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse events + drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (BOOL)mouseDownCanMoveWindow
{
	return NO;
}

- (BOOL)resignFirstResponder
{
	BOOL ok = [super resignFirstResponder];
	if (ok) self.editModeOn = NO;
	
	return ok;
	
}




- (void)drawRect:(NSRect)rect 
{
	[super drawRect:rect];
	
	[[NSColor colorWithDeviceWhite:235.0/255.0 alpha:1.0] set];
	NSRectFill(NSInsetRect([self bounds], -1, -1));
}




#pragma mark -
#pragma mark Layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (BOOL)isFlipped
{
	// flipped coordinate system to make coordinate handling easier.
	
	return YES;
}


- (void)updateFrameSize
{
	CGFloat currentYPosition = MGMDVerticalInterViewMargin + MGMDVerticalHeaderMargin;
	
	for (MGMDFieldView *view in fields) {
				
		NSRect frame = [view frame];
		CGFloat height = frame.size.height;
		
		currentYPosition += MGMDVerticalInterViewMargin + height;
		
		//NSLog(@"added %@ with height %f to a total of %f", view, height, currentYPosition);

	}
	
	
	//currentYPosition += (/*[footerView frame].size.height*/ - 2 * MGMDVerticalInterViewMargin);
	
	// here is the end of our view...
	NSRect frame = [self frame];
	frame.size.height = fmax(currentYPosition, [[self enclosingScrollView] frame].size.height);
	
	//NSLog(@"total %f", currentYPosition);
	//NSLog(@"enclosingscrollview %f", [[self enclosingScrollView] frame].size.height - 3);
	//NSLog(@"---------------");
	
	[self setFrame:frame];
}


- (void)subviewDidUpdateFrame:(MGMDFieldView *)subview
{
	[self updateFieldPositionsWithAnimation:NO];
	[self updateFrameSize];
}




- (void)updateFieldPositionsWithAnimation:(BOOL)useAnimation
{
	CGFloat currentYPosition = MGMDVerticalInterViewMargin + MGMDVerticalHeaderMargin;
	
	for (MGMDFieldView *view in fields) {
		
		if (useAnimation) {
			[[NSAnimationContext currentContext] setDuration:0.2];
			//[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
			[[view animator] setFrameOrigin:NSMakePoint(MGMDSideMargin, currentYPosition)];
		}
			
		else {
			[view setFrameOrigin:NSMakePoint(MGMDSideMargin, currentYPosition)];
		}
		
		NSRect frame = [view frame];
		CGFloat height = frame.size.height;
		
		currentYPosition += MGMDVerticalInterViewMargin + height;
		
	}
	
	[footerView setFrameOrigin:NSMakePoint(NSMinX([self bounds]), currentYPosition - MGMDVerticalInterViewMargin)];
	
	
	
	
	// set the next key views
	// exclude separators, so create temp array
	NSMutableArray *contentFields = [NSMutableArray array];
	
	for (MGMDFieldView *view in fields) {
		
		if (![view.fieldIdentifier isEqualToString:@"MGSeparator"] && view.fieldContents)
			[contentFields addObject:view.fieldContents];
		
	}
	
	for (NSView *view in contentFields) {
		
		if ([contentFields indexOfObject:view] == [contentFields count] - 1) {
			
			[view setNextKeyView:[contentFields objectAtIndex:0]];
			
		} else {
			
			[view setNextKeyView:[contentFields objectAtIndex:[contentFields indexOfObject:view] + 1]];
			
		}
		//NSLog(@"view %@ has nextkeyview %@", [view description], [[view nextKeyView] description]);
		
		
	}
}

- (CGFloat)globalMinimumLabelWidth
{
	CGFloat minWidth = 0;
	for (MGMDFieldView *view in fields)
		minWidth = fmax(minWidth, view.minimumLabelWidth);
	
	return minWidth;
}


- (void)resizeSubviewsWithOldSize:(NSSize)oldSize
{
	[super resizeSubviewsWithOldSize:oldSize];
	
	for (MGMDFieldView *view in fields) {
		
		[view updateContentsSize];
		
	}
	
	[self updateFieldPositionsWithAnimation:NO];
	
	[self updateFrameSize];
	
	
}


#pragma mark -
#pragma mark Dragging source 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Dragging source
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




- (void)startDraggingField:(MGMDFieldView *)field withEvent:(NSEvent *)theEvent
{
	
	//NSLog(@"start");
	
	draggedField = field;
	originalFieldPosition = [fields indexOfObject:field];
	
	// remove this view from the fields list and replace it by an empty placeholder
	draggedFieldPlaceholder = [[MGMDFieldView alloc] initAsPlaceholderForField:field];
	
	[fields removeObject:field];
	[self updateFieldPositionsWithAnimation:YES];
	[self setNeedsDisplay:YES];
	
	NSPasteboard *pboard = [NSPasteboard pasteboardWithName:NSDragPboard];
    [pboard declareTypes:[NSArray arrayWithObject:@"MGMDFieldType"]  owner:self];
		
	NSImage *fieldImage = [field viewImage];
	
	NSPoint dragPointInFieldCoords = [field convertPoint:[theEvent locationInWindow] fromView:nil];
	NSPoint dragPointInMainViewCoords = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	NSPoint imageOrigin = dragPointInMainViewCoords;
	imageOrigin.x -= dragPointInFieldCoords.x;
	imageOrigin.y += dragPointInFieldCoords.y;

	[[field retain] removeFromSuperview];
	[self setNeedsDisplay:YES];
	
	
	dragMouseImageOffset = dragPointInFieldCoords;
	
	[self dragImage:fieldImage
				 at:imageOrigin
			 offset:NSMakeSize(0,0) 
			  event:theEvent 
		 pasteboard:pboard
			 source:self 
		  slideBack:NO];
	

	
}


- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	
	return NSDragOperationCopy;
	
}

- (void)draggedImage:(NSImage *)anImage endedAt:(NSPoint)aPoint operation:(NSDragOperation)operation
{
	if (operation== NSDragOperationNone) {

		NSPoint cursorLocation = aPoint;
		
		cursorLocation.x += dragMouseImageOffset.x;
		cursorLocation.y += dragMouseImageOffset.y;
		
		NSShowAnimationEffect(NSAnimationEffectPoof, cursorLocation, NSMakeSize(42,52), nil, nil, nil);
	}
}

#pragma mark -
#pragma mark Dragging destination 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Dragging destination
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSDragOperation)draggingEntered:(id < NSDraggingInfo >)sender
{
	NSPoint locationInView = [self convertPoint:[sender draggingLocation] fromView:nil];
	NSInteger placeholderIndex = [self field:draggedField positionForPoint:locationInView];

	if ([[self subviews] count] > 0)
		[self addSubview:draggedFieldPlaceholder positioned:NSWindowBelow relativeTo:nil];
	else
		[self addSubview:draggedFieldPlaceholder];
	

	
	if ([fields count] == 0)
		[fields addObject:draggedFieldPlaceholder];
	else
		[fields insertObject:draggedFieldPlaceholder atIndex:fmin(placeholderIndex, [fields count]-1)];
	
	
	
	NSString *identifier = draggedField.fieldIdentifier;
	if (![insertedFieldIdentifiers containsObject:identifier])
		[insertedFieldIdentifiers addObject:identifier];
	
		
	[self updateFieldPositionsWithAnimation:NO];
	
	[self updateFrameSize];

		
	return NSDragOperationMove;
}


- (void)draggingExited:(id < NSDraggingInfo >)sender
{
	[fields removeObject:draggedFieldPlaceholder];
		
	[draggedFieldPlaceholder removeFromSuperview];
	
	[self updateMenuItemStates];
	
	[self updateFieldPositionsWithAnimation:YES];
	
	[self updateFrameSize];
	
	[self saveFieldsToPrefs];
	
}

- (NSDragOperation)draggingUpdated:(id < NSDraggingInfo >)sender
{
	NSPoint locationInView = [self convertPoint:[sender draggingLocation] fromView:nil];
	NSInteger placeholderIndex = [self field:draggedField positionForPoint:locationInView];
		
	[fields removeObject:draggedFieldPlaceholder];
	if ([fields count] == 0)
		[fields addObject:draggedFieldPlaceholder];
	else
		[fields insertObject:draggedFieldPlaceholder atIndex:fmin(placeholderIndex, [fields count])];
	
	[self updateFieldPositionsWithAnimation:YES];
	
	return NSDragOperationMove;
}


- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender
{
	NSInteger placeholderIndex = [fields indexOfObject:draggedFieldPlaceholder];
	[fields replaceObjectAtIndex:placeholderIndex withObject:[draggedField autorelease]];
	
	[draggedFieldPlaceholder removeFromSuperview];
	[draggedFieldPlaceholder release];
	draggedFieldPlaceholder = nil;
	
	[self addSubview:draggedField];
	[self updateFieldPositionsWithAnimation:NO];
	
	[self saveFieldsToPrefs];
	 
	return YES;
}

- (BOOL)prepareForDragOperation:(id < NSDraggingInfo >)sender
{	
	return YES;
}


- (void)concludeDragOperation:(id < NSDraggingInfo >)sender
{
	
	
}




- (NSInteger)field:(NSView *)field positionForPoint:(NSPoint)point
{
	// retrieve the current position of the placeholder
	// given a point in MDView coords, returns the position in the fields array corresponding to this view.
	NSInteger currentIndex = 0;
	
	if ([fields count] == 0) {
		
		return 0;
		//return  MGMDVerticalInterViewMargin + MGMDVerticalHeaderMargin;
		
	}
		
	/*while (currentIndex < [fields count]) {
		
		NSRect currentFrame = currentIndex > 0 ? [[fields objectAtIndex:currentIndex - 1] frame] : NSZeroRect;
		NSRect nextFrame = [[fields objectAtIndex:currentIndex] frame];
		
		BOOL lowerLimitOK = (currentIndex == 0 || NSMidY(currentFrame) < point.y);
		BOOL upperLimitOK = NSMidY(nextFrame) >= point.y;
		
		if (lowerLimitOK && upperLimitOK) {
			
			//NSLog(@"returned index %i",currentIndex);
			return currentIndex;
			
		}
		
		currentIndex++;
	}
	*/
	
	// New algorithm:
	
	while (currentIndex < [fields count]) {
		
		
		NSRect currentFrame = currentIndex > 0 ? [(NSView *)[fields objectAtIndex:currentIndex - 1] frame] : NSMakeRect(0,MGMDVerticalHeaderMargin ,0,0);
		
		BOOL lowerLimitOK = (currentIndex == 0 || NSMaxY(currentFrame) + MGMDVerticalInterViewMargin < point.y);
		BOOL upperLimitOK =  NSMaxY(currentFrame) + MGMDVerticalInterViewMargin + [field frame].size.height >= point.y;
		
		if (lowerLimitOK && upperLimitOK) {
			
			return currentIndex;

			
		}
		
		currentIndex++;
	}
	
	// no index found, this means that the position belongs to the last index of the field array.
	return [fields count];
}



@end
