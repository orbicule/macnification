//
//  MGMDHeaderView.m
//  MetaDataView
//
//  Created by Dennis on 11/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGMDHeaderView.h"


@implementation MGMDHeaderView


- (void)drawRect:(NSRect)rect 
{
	//[super drawRect:rect];
	
	[[NSColor colorWithDeviceWhite:200.0/255.0 alpha:1.0] set];
	
	//NSRectFill([self bounds]);
	
	
	
}

- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		// observablecontroller will be the imageArrayController, keyPath the selection
		
		[imageView bind:@"value" toObject:observableController withKeyPath:[keyPath stringByAppendingString:@".filteredThumbnail"] options:options];
		[titleField bind:@"value" toObject:observableController withKeyPath:[keyPath stringByAppendingString:@".name"] options:options];
		[creationDateField bind:@"value" toObject:observableController withKeyPath:[keyPath stringByAppendingString:@".creationDate"] options:options];
		[importDateField bind:@"value" toObject:observableController withKeyPath:[keyPath stringByAppendingString:@".importDate"] options:options];
		[modificationDateField bind:@"value" toObject:observableController withKeyPath:[keyPath stringByAppendingString:@".modificationDate"] options:options];
		
	} else {
		
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
		
	}
	
}


@end
