//
//  KeywordController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 16 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "ImageArrayController.h"


@interface KeywordController : NSWindowController {
	
	ImageArrayController *imageArrayController;
	IBOutlet NSTreeController *keywordsTreeController;
	
	IBOutlet NSTabView *editTabView;
	IBOutlet NSButton *editButton;
	IBOutlet NSOutlineView *keywordView;
	
	
	IBOutlet NSButton *addButton, *addChildButton, *removeButton, *applyButton;
	
	BOOL editModeOn;
	
}

- (id)initWithImageArrayController:(ImageArrayController *)controller;


- (IBAction)toggleEditMode:(id)sender;
- (IBAction)applyKeywordsToSelectedImages:(id)sender;




// Accessors
- (ImageArrayController *)imageArrayController;
- (void)setImageArrayController:(ImageArrayController *)value;



@end
