//
//  MontageUnitView.h
//  Stack
//
//	This view class encapsulates the contents of one laout unit, meaning image + text attributes.  It is used as a subview of the MontageView.
//
//  Created by Dennis Lorson on 2/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MontageUnitView : NSView 
{
	
	NSArray *attrs;
	NSDictionary *attrStyle;

}

// attributes are the text attributes, supplied with the correct metadata key.
// attrStyle is an expandable dictionary containing several style elements for the attrs (currently: text color)
- (id)initWithFrame:(NSRect)frame image:(NSImage *)image attributes:(NSArray *)attributes attrStyle:(NSDictionary *)style;

+ (CGFloat)heightForAttributes:(NSInteger)nbAttributes;


@end
