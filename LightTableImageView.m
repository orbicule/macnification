//
//  LightTableImageView.m
//  LightTable
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableImageView.h"

#import "NSImage_MGAdditions.h"
#import "LightTableContentView.h"
#import "LightTableImageContentView.h"
#import "LightTableImageToolView.h"
#import "LightTableAlignConstraint.h"
#import "LightTableView.h"
#import "NSView_MGAdditions.h"
#import "LightTableAlignLayoutElement.h"
#import "Image.h"


CGFloat LightTableMinimumDimension = 38;


@interface LightTableImageView (Private)


- (CGFloat)frameWidthForFrameHeight:(CGFloat)theHeight;
- (CGFloat)frameHeightForFrameWidth:(CGFloat)theWidth;

- (void)offsetCursorPositionByDelta:(NSPoint)delta;

- (void)setKeyboardTimeout:(NSTimeInterval)time;
- (void)disableKeyboardTimeout;
- (void)keyboardTimerDidEnd:(NSTimer *)timer;

- (NSArray *)applicableConstraintsFromArray:(NSArray *)originals forResizeAtHandle:(ControlPoint)draggedPoint;
		

@end

@implementation LightTableImageView

@synthesize lightTableImage, previousSize, selected, toolView;
@dynamic frame, displaysControlPoints;




- (NSString *)stringWithRect:(NSRect)rect
{
	return [NSString stringWithFormat:@"-- %1.3f, %1.3f, %1.3f, %1.3f", rect.origin.x,rect.origin.y,rect.size.width,rect.size.height];
}
			
			

- (id)initWithFrame:(NSRect)frame
{
	if ((self = [super initWithFrame:frame])) {
		toolView = [[LightTableImageToolView alloc] initWithFrame:NSMakeRect(0,0,frame.size.width,frame.size.height)];
		[self addSubview:toolView];
		hasDragged = NO;
		layoutAlignConstraints = [[NSMutableArray array] retain];
		self.previousSize = NSMakeSize(0,0);
		imageWidthToHeightRatio = -1;
	}
	return self;
}

- (void)dealloc
{
	[imageView release];
	[toolView release];
	[layoutAlignConstraints release];
	
	[super dealloc];
}

- (id)copyWithZone:(NSZone *)zone
{
	// NSCopying protocol implemented in order to easily copy class objects for the print view.
	LightTableImageView *copy = [[LightTableImageView alloc] initWithFrame:[self frame]];	
	
	[copy setImage:[self image]];
	// important: need to override frame because -setImage changes it
	[copy setFrame:[self frame]];
	[copy->imageView setOpacity:[self->imageView opacity]];

	return copy;
}

- (void)initSubviews
{
	if (!imageView) {
		imageView = [[LightTableImageContentView alloc] initWithFrame:[self bounds]];
		
		// change our own frame to keep the aspect ratio of the new image view correct, retaining our width and changing our height when needed.
		[self setFrameSize:NSMakeSize([self frameWidthForFrameHeight:[self frame].size.height], [self frame].size.height)];
		[self addSubview:imageView];
		// put the tool view on top
		[self addSubview:toolView];
	}	
}


- (void) bind:(NSString *)binding toObject:(id)observable withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"image"]) {
		[observable addObserver:self forKeyPath:keyPath options:NSKeyValueObservingOptionInitial context:nil];
		[imageView bind:@"opacity" toObject:observable withKeyPath:@"opacity" options:nil];
	}
	else {
		[super bind:binding toObject:observable withKeyPath:keyPath options:options];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"image.filteredImageRepWithScaleBar"]) {
		
		NSBitmapImageRep *rep = [(Image *)[object valueForKey:@"image"] filteredImageRepWithScaleBar];
		NSImage *img = [[[NSImage alloc] initWithSize:NSMakeSize([rep pixelsWide], [rep pixelsHigh])] autorelease];
		[img addRepresentation:rep];
		
		[self setImage:img];
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"image"]) {
		[self.lightTableImage removeObserver:self forKeyPath:@"image.filteredImageRepWithScaleBar"];
	}
	else {
		[super unbind:binding];
	}
}


#pragma mark Mouse events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSView *)hitTest:(NSPoint)aPoint
{
	// all event handling done in this class, so return self IF in our frame (because aPoint is in coord of superview)
	return NSPointInRect(aPoint, [self frame]) ? self : nil;
}

- (void)mouseEntered:(NSEvent *)theEvent
{
	// break if the mouse is dragging
	if (hasDragged) return;
	
	// we need to let the lighttable view reassign the control points view.
	[(LightTableContentView *)[self superview] reassignControlPointsRectIfNeededWithEvent:theEvent];
}


- (void)mouseExited:(NSEvent *)theEvent
{
	// break if the mouse is dragging
	if (hasDragged) return;
	
	// we need to let the lighttable view reassign the control points view.
	[(LightTableContentView *)[self superview] reassignControlPointsRectIfNeededWithEvent:theEvent];
}

- (void)mouseDown:(NSEvent *)theEvent
{
	((LightTableContentView *)[self superview]).lastClickedView = self;
	
	NSPoint locationInToolView = [toolView convertPoint:[theEvent locationInWindow] fromView:nil];
	currentDragControlPoint = [toolView controlPointAtPoint:locationInToolView];
	if (currentDragControlPoint != NoControlPoint) {
		[[toolView animator] setImageSizeInfoBoxOpacity:0.6];
	}	

	[[self window] makeFirstResponder:self];
	
	// select the image view (show bounding box)
	// inform the superview that we have been selected.
	// can't do this ourselves, because the selection is a global setting (so other items may need to be deselected)
	if (!self.selected) {
		if ([self superview] && [[self superview] respondsToSelector:@selector(subview:shouldBeSelectedWithEvent:)]) {
			if ([(LightTableContentView *)[self superview] subview:self shouldBeSelectedWithEvent:theEvent]) {
				self.selected = YES;
			}
		}
	}
	
	if ([theEvent clickCount] == 2) {
		
		// double-click: if the view is the only one selected, resize to 100%
		if ([(LightTableContentView *)[self superview] numberOfSelectedViews] == 1) {
			
			if (!isRealSize) {
				
				self.previousSize = [self bounds].size;
				[(LightTableContentView *)[self superview] resizeToRealSize:self];
				isRealSize = YES;
				
			} else {
				[(LightTableContentView *)[self superview] resizeToPreviousSize:self];
				isRealSize = NO;
			}

		}
	}
	hasMouseDown = YES;
	
	isMoving = currentDragControlPoint == NoControlPoint;
	
	//mouseDownTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(performAutoscroll:) userInfo:nil repeats:YES];
	
	
	// check if the frame overlaps with any of the other images.
	if ([(LightTableContentView *)[self superview] viewOverlapsWithOtherImageView:self]) {
		
		[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
		
		[[imageView animator] setAlphaValue:0.55];
		[toolView setSelectionRectangleOpacity:0.55];
		
	} else {
		
		[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
		
		[[imageView animator] setAlphaValue:1.0];
		[toolView setSelectionRectangleOpacity:1.0];
		
	}
	

}

- (void)rightMouseDown:(NSEvent *)theEvent
{
	((LightTableContentView *)[self superview]).lastClickedView = self;
	
	
	// select the image view (show bounding box)
	// inform the superview that we have been selected.
	// can't do this ourselves, because the selection is a global setting (so other items may need to be deselected)
	if (!self.selected) {
		if ([self superview] && [[self superview] respondsToSelector:@selector(subview:shouldBeSelectedWithEvent:)]) {
			if ([(LightTableContentView *)[self superview] subview:self shouldBeSelectedWithEvent:theEvent]) {
				self.selected = YES;
			}
		}
	}
	
	// set the selection rect ourselves and manually, otherwise the animation is blocked while the menu is active, and the user has no feedback
	[toolView setSelectionRectangleOpacity:1.0];

	
	[super rightMouseDown:theEvent];
}



- (void)mouseUp:(NSEvent *)theEvent
{
	if (currentDragControlPoint != NoControlPoint)
		[[toolView animator] setImageSizeInfoBoxOpacity:0];
	
	if ([theEvent clickCount] == 2) {
	
	}

	[toolView setSelectionRectangleOpacity:1.0];
	
	currentDragControlPoint = NoControlPoint;
	
	// extra check for control points view.
	[(LightTableContentView *)[self superview] reassignControlPointsRectIfNeededWithEvent:theEvent];
	
	// remove guides and own constraints.
	[((LightTableView *)[[self superview] superview]) removeAllLayoutGuides];
	[layoutAlignConstraints removeAllObjects];
	
	hasDragged = NO;
	hasMouseDown = NO;
	
	[mouseDownTimer invalidate];
	mouseDownTimer = nil;
	
	
	
	// the image transparency is not active when not dragging...so reset.
	[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
		
	[[imageView animator] setAlphaValue:1.0];
	[toolView setSelectionRectangleOpacity:1.0];
	
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	
	// check if the frame overlaps with any of the other images.
	if ([(LightTableContentView *)[self superview] viewOverlapsWithOtherImageView:self]) {
		
		[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
		
		[[imageView animator] setAlphaValue:0.55];
		[[toolView animator] setSelectionRectangleOpacity:0.55];
		
	} else {
		
		[CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];

		[[imageView animator] setAlphaValue:1.0];
		[[toolView animator] setSelectionRectangleOpacity:1.0];
		
	}
	
	
	CGFloat oldX = [self frame].origin.x;
	CGFloat oldY = [self frame].origin.y;
	CGFloat oldWidth = [self frame].size.width;
	CGFloat oldHeight = [self frame].size.height;
	
	// adjust deltas for zoom level
	CGFloat deltaX = [theEvent deltaX] / ((LightTableContentView *)[self superview]).zoomLevel;
	CGFloat deltaY = [theEvent deltaY] / ((LightTableContentView *)[self superview]).zoomLevel;
	
	CGFloat newHeight = [self frameHeightForFrameWidth:oldWidth + deltaX];
	CGFloat newWidth = [self frameWidthForFrameHeight:oldHeight - deltaY];

	NSRect newFrame;
	
	switch (currentDragControlPoint) {
		
		case NoControlPoint:
			// regular shape move
			// need to forward this frame change to other views.
			newFrame = NSMakeRect(oldX + deltaX, oldY - deltaY, oldWidth, oldHeight);
			[(LightTableContentView *)[self superview] offsetImageOriginsWithPoint:NSMakePoint(deltaX,deltaY) eventView:self];
			//NSLog(@"old %@   new %@", [self stringWithRect:[self frame]], [self stringWithRect:newFrame]);

			break;
		case UpperLeftControlPoint:
			newFrame = NSMakeRect(oldX + deltaX, oldY, oldWidth - deltaX, [self frameHeightForFrameWidth:oldWidth - deltaX]);
			break;
		case UpperMiddleControlPoint:
			newFrame = NSMakeRect(oldX + (oldWidth - newWidth)/2, oldY, newWidth, oldHeight - deltaY);
			break;
		case UpperRightControlPoint:
			newFrame = NSMakeRect(oldX, oldY, oldWidth + deltaX, newHeight);
			break;
		case MiddleRightControlPoint:
			newFrame = NSMakeRect(oldX, oldY + (oldHeight - newHeight)/2, oldWidth + deltaX, newHeight);
			break;
		case LowerRightControlPoint:
			newFrame = NSMakeRect(oldX, oldY + oldHeight - newHeight, oldWidth + deltaX, newHeight);
			break;
		case LowerMiddleControlPoint:
			newFrame = NSMakeRect(oldX + (oldWidth - [self frameWidthForFrameHeight:oldHeight + deltaY])/2, oldY - deltaY, [self frameWidthForFrameHeight:oldHeight + deltaY], oldHeight + deltaY);
			break;
		case LowerLeftControlPoint:
			newFrame = NSMakeRect(oldX + oldWidth - [self frameWidthForFrameHeight:oldHeight + deltaY], oldY - deltaY, [self frameWidthForFrameHeight:oldHeight + deltaY], oldHeight + deltaY);
			break;
		case MiddleLeftControlPoint:
			newFrame = NSMakeRect(oldX + deltaX, oldY + (oldHeight - [self frameHeightForFrameWidth:oldWidth - deltaX])/2, oldWidth - deltaX, [self frameHeightForFrameWidth:oldWidth - deltaX]);
			break;
			
	}
	
	if (currentDragControlPoint != NoControlPoint) isRealSize = NO;
	
	
	// we need to alter the frame AFTER checking the constraints, because snap in and snap out work correctly this way (not so with keyboard actions where snaps are not enabled)
	
	// only add constraints if single selection.
	// add a high precision constraint if resizing, as it can only be appropriate for one dimension!
	if ([(LightTableContentView *)[self superview] numberOfSelectedViews] == 1) {
		LayoutPrecision precision = (currentDragControlPoint == NoControlPoint) ? StandardPrecision : StandardResizePrecision;
		[((LightTableView *)[[self superview] superview]) detectLayoutGuidesForFrameChange:(currentDragControlPoint == NoControlPoint) ofView:self precision:precision keepAlive:NO];
	}
	

	[self setFrame:newFrame withEvent:theEvent constrainIfNeeded:([(LightTableContentView *)[self superview] numberOfSelectedViews] == 1) move:(currentDragControlPoint == NoControlPoint)];
	

	hasDragged = YES;
	//[self performAutoscroll:theEvent];
	[self autoscroll:theEvent];
}

- (void)keyDown:(NSEvent *)theEvent
{
	BOOL moved = NO;
	[self setKeyboardTimeout:1.0];
	
	// keyboard positioning of this imageview.
	unichar key = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
	CGFloat deltaX = 0;
	CGFloat deltaY = 0;
	
	// fast positioning when shift held down
	NSInteger nbPixels = ([theEvent modifierFlags] & NSShiftKeyMask) ? 8 : 1;
	
	if (key == NSUpArrowFunctionKey) {
		deltaY = nbPixels;
		moved = YES;
	}
	if (key == NSDownArrowFunctionKey) {
		deltaY = - nbPixels;
		moved = YES;
	}
	if (key == NSLeftArrowFunctionKey) {
		deltaX = - nbPixels;
		moved = YES;
	}
	if (key == NSRightArrowFunctionKey) {
		deltaX = nbPixels;
		moved = YES;
	}
	
	if (key == NSDeleteCharacter) {
		[(LightTableContentView *)[self superview] shouldRemoveSelectedObjects];
	}
	
	if (key == NSTabCharacter) {
		([theEvent modifierFlags] & NSShiftKeyMask) ? [(LightTableContentView *)[self superview] selectPreviousImageView:self] : [(LightTableContentView *)[self superview] selectNextImageView:self];
	}
	
	if (key == 'a' && ([theEvent modifierFlags] & NSCommandKeyMask)) {
		[(LightTableContentView *)[self superview] selectAll];
	}
	
	if (key == ' ') {
		// double-click: if the view is the only one selected, resize to 100%
		if ([(LightTableContentView *)[self superview] numberOfSelectedViews] == 1) {
			
			if (self.previousSize.width == 0 && self.previousSize.height == 0) {
				
				self.previousSize = [self bounds].size;
				[(LightTableContentView *)[self superview] resizeToRealSize:self];
				
			} else {
				
				[(LightTableContentView *)[self superview] resizeToPreviousSize:self];
				self.previousSize = NSMakeSize(0,0);
				
			}
			
		}
		
	}

	if (moved) {
		
		[[toolView animator] setSelectionRectangleOpacity:0.2];
		
		NSRect newFrame = [self frame];
		newFrame.origin.x += deltaX;
		newFrame.origin.y += deltaY;
		
		// need to invoke setframe BEFORE detecting layout guides, because these are based on the NEW frame.
		[self setFrame:newFrame withEvent:theEvent constrainIfNeeded:NO move:YES];
		// only show guides if 1 view selected?
		if ([(LightTableContentView *)[self superview] numberOfSelectedViews] == 1)
			[((LightTableView *)[[self superview] superview]) detectLayoutGuidesForFrameChange:YES ofView:self precision:HighPrecision keepAlive:YES];
		[(LightTableContentView *)[self superview] offsetImageOriginsWithPoint:NSMakePoint(deltaX,-deltaY) eventView:self];
	}
}


- (void)keyDownOnSuperview:(NSEvent *)theEvent
{
	// invoked by our superview (LTContentView) on relevant events.
	// this event is forwarded to all applicable image views, so we don't need to call the superview again, just take care of our own props.
	BOOL onlyViewSelected = ([(LightTableContentView *)[self superview] numberOfSelectedViews] == 1);
	
	// move our frame
	BOOL moved = NO;
	if (onlyViewSelected) [self setKeyboardTimeout:1.0];
	
	// keyboard positioning of this imageview.
	unichar key = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
	CGFloat deltaX = 0;
	CGFloat deltaY = 0;
	
	// fast positioning when shift held down
	NSInteger nbPixels = ([theEvent modifierFlags] & NSShiftKeyMask) ? 8 : 1;
	
	if (key == NSUpArrowFunctionKey) {
		deltaY = nbPixels;
		moved = YES;
	}
	if (key == NSDownArrowFunctionKey) {
		deltaY = - nbPixels;
		moved = YES;
	}
	if (key == NSLeftArrowFunctionKey) {
		deltaX = - nbPixels;
		moved = YES;
	}
	if (key == NSRightArrowFunctionKey) {
		deltaX = nbPixels;
		moved = YES;
	}
	
	if (key == NSDeleteCharacter) {
		// should be handled by the superview already.
	}

	if (moved) {
		
		if (onlyViewSelected) [[toolView animator] setSelectionRectangleOpacity:0.2];
		
		NSRect newFrame = [self frame];
		newFrame.origin.x += deltaX;
		newFrame.origin.y += deltaY;
		
		// need to invoke setframe BEFORE detecting layout guides, because these are based on the NEW frame.
		[self setFrame:newFrame withEvent:theEvent constrainIfNeeded:NO move:YES];
		// only show guides if 1 view selected?
		if (onlyViewSelected) [((LightTableView *)[[self superview] superview]) detectLayoutGuidesForFrameChange:YES ofView:self precision:StandardPrecision keepAlive:YES];
	}
	
}

#pragma mark Keyboard move timer
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard move timer
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// set of methods that control the time-out of keyoard-initiated actions.
// e.g. the time-out of the selection box and info box fade.

- (void)setKeyboardTimeout:(NSTimeInterval)time
{
	// set the guides to timeout within a given time.
	// resets the internal NSTimer as needed
	if (keyboardTimer) {
		// if it is not nil, it is guaranteed to be not released (by our code), so invalidate
		[keyboardTimer invalidate];
	}
	keyboardTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(keyboardTimerDidEnd:) userInfo:nil repeats:NO];
	
}

- (void)disableKeyboardTimeout
{ 
	if (keyboardTimer) {
		[keyboardTimer invalidate];
		keyboardTimer = nil;
	}
}

- (void)keyboardTimerDidEnd:(NSTimer *)timer
{
	// first and foremost, set our timer var to nil because the timer has been released
	keyboardTimer = nil;
	
	// check if the mouse is down; if so, don't handle timeout because the mouseUp will restore the correct state of the visual elements.
	if (!hasMouseDown) {
		if (self.selected)
			[[toolView animator] setSelectionRectangleOpacity:1.0];
	}
}



#pragma mark Frame origin/resizing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Frame origin/resizing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)setFrame:(NSRect)frame withEvent:(NSEvent *)theEvent constrainIfNeeded:(BOOL)constrain move:(BOOL)move
{
	// any user initiated resize or frameset method goes through here.  It allows to set minimum heights, widths, etc.
	// theEventis given in order to track the cursor position changes.
	
	// move == YES if the frame change is invoked by a move action, YES in case of resize action.
	
	
	if (frame.size.width < LightTableMinimumDimension) return;
	if (frame.size.height < LightTableMinimumDimension) return;	
	
	// we need to change the frame origin if any layout constraints apply.
	NSRect adjustedFrame = frame;
		
	// filter out unneeded constraints if we're resizing.
	NSArray *constraintsToFilter = [[layoutAlignConstraints copy] autorelease];
	[layoutAlignConstraints removeAllObjects];
	[layoutAlignConstraints addObjectsFromArray:[self applicableConstraintsFromArray:constraintsToFilter forResizeAtHandle:currentDragControlPoint]];
	
	if (constrain && ((LightTableView *)[[self superview] superview]).snapsToLayoutGuides) {
		for (LightTableAlignConstraint *constraint in layoutAlignConstraints) {
			
			// BUGFIX 31/7 self -> superview (need to convert to LightTableContentView!)
			constraint.currentCursorPosition = [[self superview] convertPoint:[theEvent locationInWindow] fromView:nil];
			
			// this check should be superfluous, but do it to be safe.
			// only the frame origin will be changed then by this call.
			if (constraint.type == (move ? MoveAlignConstraint : ResizeAlignConstraint))
				adjustedFrame = [constraint constrainedFrameWithFrame:adjustedFrame];
		}
	}
	
	
	// adjust the frame position if the user is resizing
	if (!move)
		adjustedFrame = [self resizedFrameWithOldFrame:frame newSize:adjustedFrame.size controlPoint:currentDragControlPoint];
		
	
	// remove unneeded constraints (that have exceeded their number of snaps)
	NSMutableArray *ctToRemove = [NSMutableArray array];
	for (LightTableAlignConstraint *constraint in layoutAlignConstraints) {
		if (!constraint.isActive) {
			[ctToRemove addObject:constraint];
		}
	}
	
	[layoutAlignConstraints removeObjectsInArray:ctToRemove];
	
	[self setFrame:adjustedFrame];

	
	// if any constraints were removed, we need to update the tool view so that the corresponding layout guides are not drawn anymore.
	LayoutPrecision precision = move ? StandardPrecision : StandardResizePrecision;

	if ([ctToRemove count] > 0)
		[((LightTableView *)[[self superview] superview]) detectLayoutGuidesForFrameChange:move ofView:self precision:precision keepAlive:NO];
	
}

- (NSRect)resizedFrameWithOldFrame:(NSRect)oldFrame newSize:(NSSize)newSize controlPoint:(ControlPoint)controlPoint
{
	// this method will resize AND reposition the frame so that it has the given new size, and as if it were resized by the user through the given control point.
	// it is invoked when a constraint imposes a new size for the frame; this constraint does not know in which direction resizing took place, this is a responsability for self; hence this method.
	
	if (controlPoint == NoControlPoint) return oldFrame;
	
	NSRect newFrame = oldFrame;
	
	NSSize adjustedNewSize = newSize;
	
	// adjust the new size for correct aspect ratio
	if (newSize.width - oldFrame.size.width == 0) {
		// the height has changed, so adjust the width for a correct aspect ratio
		newSize.width = [self frameWidthForFrameHeight:newSize.height];
	} else {
		newSize.height = [self frameHeightForFrameWidth:newSize.width];
	}
	
	// resize the frame
	newFrame.size = adjustedNewSize;
	
	NSSize sizeDiff = NSMakeSize(newFrame.size.width - oldFrame.size.width, newFrame.size.height - oldFrame.size.height);
	
	// reposition the frame as needed based on the dragged control point.
	switch (controlPoint) {
			
		case UpperLeftControlPoint:
			newFrame.origin.x -= sizeDiff.width;
			break;
			
		case MiddleLeftControlPoint:
			newFrame.origin.x -= sizeDiff.width;
			newFrame.origin.y -= sizeDiff.height/2;
			break;
			
		case LowerLeftControlPoint:
			newFrame.origin.x -= sizeDiff.width;
			newFrame.origin.y -= sizeDiff.height;
			break;
			
		case LowerMiddleControlPoint:
			newFrame.origin.x -= sizeDiff.width/2;
			newFrame.origin.y -= sizeDiff.height;
			break;
			
		case LowerRightControlPoint:
			newFrame.origin.y -= sizeDiff.height;
			break;
			
		case MiddleRightControlPoint:
			newFrame.origin.y -= sizeDiff.height/2;
			break;
			
		case UpperRightControlPoint:
			break;
			
		case UpperMiddleControlPoint:
			newFrame.origin.x -= sizeDiff.width/2;
			break;
			
	}
	
	return newFrame;
}

- (NSArray *)applicableConstraintsFromArray:(NSArray *)originals forResizeAtHandle:(ControlPoint)draggedPoint
{
	// Returns the constraints that are to be actually adhered to when resizing, out of the array of given constraints.
	// We generally only want to snap on the side that is dragged by the user (or two if in a corner).
	// Since the control point (handle) is the imgviews responsability, we need to filter the constraints after they are imposed.
		
	if (draggedPoint == NoControlPoint) return originals;
	
	NSMutableArray *filteredConstraints = [NSMutableArray array];
	
	// the maximal difference between the coordinate values of one of our frame edges and the constraint, in order for this edge to be considered as snapping to the constraint.
	// as long as it's smaller than half our width or height, this value should be fine.
	CGFloat constraintDistance = 5.0;
	NSRect frame = [self frame];
	
	
	for (LightTableAlignConstraint *constraint in originals) {
		
		BOOL add = NO;
		
		switch (draggedPoint) {
				
			case LowerLeftControlPoint:
				add = ((fabs(NSMinX(frame) - constraint.coordinate) < constraintDistance && constraint.direction == VerticalConstraintDirection) 
					   || (fabs(NSMinY(frame) - constraint.coordinate) < constraintDistance && constraint.direction == HorizontalConstraintDirection));
				break;
			case MiddleLeftControlPoint:
				add = (fabs(NSMinX(frame) - constraint.coordinate) < constraintDistance && constraint.direction == VerticalConstraintDirection);
				break;
			case UpperLeftControlPoint:
				add = ((fabs(NSMinX(frame) - constraint.coordinate) < constraintDistance && constraint.direction == VerticalConstraintDirection) 
					   || (fabs(NSMaxY(frame) - constraint.coordinate) < constraintDistance && constraint.direction == HorizontalConstraintDirection));
				break;
			case UpperMiddleControlPoint:
				add = (fabs(NSMaxY(frame) - constraint.coordinate) < constraintDistance && constraint.direction == HorizontalConstraintDirection);
				break;
			case UpperRightControlPoint:
				add = ((fabs(NSMaxX(frame) - constraint.coordinate) < constraintDistance && constraint.direction == VerticalConstraintDirection) 
					   || (fabs(NSMaxY(frame) - constraint.coordinate) < constraintDistance && constraint.direction == HorizontalConstraintDirection));
				break;
			case MiddleRightControlPoint:
				add = (fabs(NSMaxX(frame) - constraint.coordinate) < constraintDistance && constraint.direction == VerticalConstraintDirection);
				break;
			case LowerRightControlPoint:
				add = ((fabs(NSMaxX(frame) - constraint.coordinate) < constraintDistance && constraint.direction == VerticalConstraintDirection) 
					   || (fabs(NSMinY(frame) - constraint.coordinate) < constraintDistance && constraint.direction == HorizontalConstraintDirection));
				break;
			case LowerMiddleControlPoint:
				add = (fabs(NSMinY(frame) - constraint.coordinate) < constraintDistance && constraint.direction == HorizontalConstraintDirection);
				break;
		}
		
		if (add) [filteredConstraints addObject:constraint];
		
	}
	return filteredConstraints;
}
	

- (void)offsetCursorPositionByDelta:(NSPoint)delta
{
	// not used anymore, to remove.
	NSPoint currentPosition = [NSEvent mouseLocation];
	NSPoint currentPositionInWindow = [[self window] convertScreenToBase:currentPosition];
	NSPoint currentPositionInView = [[self superview] convertPoint:currentPositionInWindow fromView:nil];
	NSPoint newPosition = NSMakePoint(currentPositionInView.x + delta.x, currentPositionInView.y + delta.y);
	NSPoint newPositionGlobal = [self convertPointToGlobal:newPosition];
	CGWarpMouseCursorPosition(CGPointMake(newPositionGlobal.x, newPositionGlobal.y));
}

- (NSRect)imageViewFrame
{	
	return [self bounds];
}

- (CGFloat)frameWidthForFrameHeight:(CGFloat)theHeight
{
	// returns the width of the frame of THIS view, when given a desired height, making sure the image view aspect ratio stays correct.
	CGFloat desiredImageViewHeight = theHeight;
	CGFloat ratio = [self imageWidthToHeightRatio];
	CGFloat imageViewWidth = (ratio * desiredImageViewHeight);
	CGFloat ownViewWidth = imageViewWidth;
	
	return ownViewWidth;
}

- (CGFloat)frameHeightForFrameWidth:(CGFloat)theWidth
{
	// returns the width of the frame of THIS view, when given a desired height, making sure the image view aspect ratio stays correct.
	CGFloat desiredImageViewWidth = theWidth;
	CGFloat ratio = [self imageWidthToHeightRatio];
	CGFloat imageViewHeight = (1.0/ratio * desiredImageViewWidth);
	CGFloat ownViewHeight = imageViewHeight;
	
	return ownViewHeight;
}


- (CGFloat)imageWidthToHeightRatio
{
	// returns the width to height - ratio of the current image.
	//if (![self image]) return 1.0;
	if (imageWidthToHeightRatio < 0) {
		NSSize imageSize = [(Image *)[self.lightTableImage valueForKeyPath:@"image"] pixelSize];
		imageWidthToHeightRatio = imageSize.width / imageSize.height;
	}

	return imageWidthToHeightRatio;
}


- (void)setImage:(NSImage *)theImage
{
	if (!imageView) {
		imageView = [[LightTableImageContentView alloc] initWithFrame:[self bounds]];
		[self addSubview:imageView];

	}	
	
	[imageView setImage:theImage];
}

- (NSImage *)image
{
	return [imageView image];
}

- (void)setFrame:(NSRect)frame
{	
	// we need to align on integral values (thanks CA) but be sure NOT to change sizes!!
	NSRect integralFrame = frame;
	integralFrame.size.width = frame.size.width;
	integralFrame.size.height = frame.size.height;

	NSRect oldFrame = [self frame];
	[[self superview] setNeedsDisplayInRect:NSInsetRect(oldFrame, -5,-5	)];
	
	[super setFrame:integralFrame];
	[toolView setFrameSize:[self frame].size];
	[imageView setFrame:[self imageViewFrame]];
	[self setNeedsDisplay:YES];
	
	// update the frame of the main view in the scroll view
	[(LightTableView *)[[self superview] superview] updateContentViewBoundsForImages];
}

- (void)setFrameSize:(NSSize)newSize
{
	[super setFrameSize:newSize];
	[toolView setFrameSize:[self frame].size];
	[imageView setFrame:[self imageViewFrame]];
}


- (void)addAlignConstraint:(LightTableAlignConstraint *)constraint
{
	// avoid replacing constraints that already exist, so they can count their nb of snaps and deactivate themselves in time.
	for (LightTableAlignConstraint *ownConstraint in layoutAlignConstraints) {
		if (constraint.direction == ownConstraint.direction && constraint.coordinate == ownConstraint.coordinate && constraint.threshold == ownConstraint.threshold)
			return;
	}
	[layoutAlignConstraints addObject:constraint];
}

- (void)removeLayoutConstraints
{
	[layoutAlignConstraints removeAllObjects];
}




#pragma mark Properties/accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Properties/accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (BOOL)displaysControlPoints
{
	return toolView.controlPointsOpacity > 0;
}

- (void)setDisplaysControlPoints:(BOOL)flag
{
	//if ((flag && toolView.controlPointsOpacity == 1.0) || (!flag && toolView.controlPointsOpacity == 0.0)) return;
	[[toolView animator] setControlPointsOpacity: (flag ? 1.0 : 0.0)];
}


- (void)setSelected:(BOOL)flag
{
	selected = flag;
	
	//[NSAnimationContext beginGrouping];
	//[[NSAnimationContext currentContext] setDuration:0.0];
	
	[toolView setSelectionRectangleOpacity:(selected ? 1.0 : 0.0)];
	
	//[NSAnimationContext endGrouping];
	
}

- (void)setAlphaValue:(CGFloat)alpha
{
	//if ([imageView alphaValue] == alpha) return;
	[imageView setAlphaValue:alpha];
}


- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	return [(LightTableContentView *)[self superview] menuForEvent:theEvent];
}

@end
