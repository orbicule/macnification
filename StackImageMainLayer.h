//
//  ImageLayer.h
//  StackTable
//
//	CALayer subclass representing the graphical form of a stack table image.  Contains a weak ref to its corresponding (StackTableImage) model object.
//
//  Created by Dennis Lorson on 25/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "StackImageLayer.h"

@class StackImage;
@class MGBitmapImageRep;
@class StackImageReflectionLayer;

@interface StackImageMainLayer : StackImageLayer {
		

	StackImageReflectionLayer *reflectionLayer;
	CALayer *toolLayer;
	
	MGBitmapImageRep *thumbnailRep;
	MGBitmapImageRep *fullResRep;
	NSSize fullImageSize;
	
	BOOL selected;	
	BOOL showReflections;
	
}


@property(nonatomic) BOOL selected;

@property(nonatomic, retain) MGBitmapImageRep *thumbnailRep;
@property(nonatomic, retain) MGBitmapImageRep *fullResRep;
@property(nonatomic, assign) StackImageReflectionLayer *reflectionLayer;
@property(nonatomic, retain) CALayer *toolLayer;

@property(readonly) NSPoint layerOffset;
@property(readonly) NSPoint contentsOffset;
@property(readonly) CGRect globalBounds;
@property(nonatomic) BOOL showReflections;



- (void)setHierarchyNeedsDisplay;

// image dimensions
- (NSSize)fullImageSize;
- (CGFloat)imageDataSize;

// slice tools
- (void)slicePixels:(void **)pixels count:(NSInteger *)count fromPoint:(NSPoint)first toPoint:(NSPoint)second;
- (void)fullResSlicePixels:(void **)pixels count:(NSInteger *)count fromPoint:(NSPoint)first toPoint:(NSPoint)second;
- (void)clearSliceResources;
- (void)clearFullResSliceResources;


// reflection
//- (CGImageRef)reflectionWithImage:(CGImageRef)original heightFraction:(CGFloat)frac;
- (void)createReflectionLayer;
- (void)removeReflectionlayer;


@end
