//
//  FullScreenImageArrayController.m
//  Filament
//
//  Created by Dennis Lorson on 24/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "FullScreenImageArrayController.h"

#import "Stack.h"

@implementation FullScreenImageArrayController

- (void)awakeFromNib
{
	[imageArrayController_ addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionInitial context:nil];
	
}

- (NSArray *)arrangeObjects:(NSArray *)objects
{
	NSMutableArray *array = [NSMutableArray arrayWithCapacity:[objects count]];
	
	for (id object in objects) {
		if (![object isKindOfClass:[Stack class]])
			[array addObject:object];
	}
	
	return array;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object == imageArrayController_ && [keyPath isEqualToString:@"selection"]) {
		
		[self setSelectedObjects:[imageArrayController_ selectedObjects]];
        ;

	}
	
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}



#pragma mark -
#pragma mark IKIB delegate

- (void) imageBrowser:(IKImageBrowserView *) aBrowser cellWasDoubleClickedAtIndex:(NSUInteger) index;
{
	// do nothing
}

- (void) imageBrowser:(IKImageBrowserView *) aBrowser cellWasRightClickedAtIndex:(NSUInteger) index withEvent:(NSEvent *) event;
{
	// do nothing
}

- (void) imageBrowserSelectionDidChange:(IKImageBrowserView *) aBrowser;
{
	[self setSelectionIndexes:[aBrowser selectionIndexes]];
	
    ;

	// ignore the case where we have no selection -- this is not possible by user intervention.
	// so it had to originate from the regular browser, and does not have to be looped back
	if ([[aBrowser selectionIndexes] count])
		[imageArrayController_ setSelectedObjects:[self selectedObjects]];
}

@end
