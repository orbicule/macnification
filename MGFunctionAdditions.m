//
//  MGFunctionAdditions.m
//  LightTable
//
//  Created by Dennis Lorson on 30/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGFunctionAdditions.h"

#import <QuickTime/QuickTime.h>


BOOL MGEqualRects(NSRect first, NSRect second)
{
	// don't use ultra-precise comparison...
	if (fabs(first.origin.x - second.origin.x) > 0.005) return NO;
	if (fabs(first.origin.y - second.origin.y) > 0.005) return NO;
	if (fabs(first.size.width - second.size.width) > 0.005) return NO;
	if (fabs(first.size.height - second.size.height) > 0.005) return NO;
	
	return YES;
}

CGFloat sign(CGFloat value)
{
	return (value < 0) ? -1 : 1;
}

NSSize MGScaleSize(NSSize size, CGFloat scale) 
{
	NSSize newSize;
	newSize.width = size.width * scale;
	newSize.height = size.height * scale;
	
	return newSize;
}

NSPoint MGRectCenter(NSRect rect)
{
	NSPoint center;
	center.x = rect.origin.x + rect.size.width / 2;
	center.y = rect.origin.y + rect.size.height / 2;
	
	return center;
}

NSPoint MGPointLinearCombination(NSPoint first, NSPoint second, CGFloat weight)
{
	// returns a point that is a linear combination of the given points, weighted by the given float.
	NSPoint newPoint;
	newPoint.x = first.x * weight + second.x * (1 - weight);
	newPoint.y = first.y * weight + second.y * (1 - weight);
	
	return newPoint;
}


NSPoint MGDeviceIntegralPoint(NSPoint point)
{
	CGFloat scaleFactor = [[NSScreen mainScreen] userSpaceScaleFactor];		// main screen == key window screen
		
	NSPoint integralPoint = point;
	
	integralPoint.x *= scaleFactor;
	integralPoint.y *= scaleFactor;	
	
	integralPoint.x = (int)integralPoint.x;
	integralPoint.y = (int)integralPoint.y;
	
	integralPoint.x += 0.5;
	integralPoint.y += 0.5;
	
	integralPoint.x /= scaleFactor;
	integralPoint.y /= scaleFactor;	
	
	return integralPoint;
	
}

CGFloat MGDeviceIntegralValue(CGFloat value)
{
	CGFloat scaleFactor = [[NSScreen mainScreen] userSpaceScaleFactor];		// main screen == key window screen
	
	CGFloat integralValue = value;
	
	integralValue *= scaleFactor;
	
	integralValue = (int)integralValue;
	integralValue += 0.5;
	
	integralValue /= scaleFactor;
	
	return integralValue;
}
	

NSRect MGDeviceIntegralRect(NSRect rect)
{
	// http://developer.apple.com/documentation/Cocoa/Conceptual/CocoaDrawingGuide/Transforms/chapter_4_section_2.html
	
	CGFloat scaleFactor = [[NSScreen mainScreen] userSpaceScaleFactor];		// main screen == key window screen
		
	NSRect integralRect = rect;
	
	// Convert coordinates to device space units.
	integralRect.origin.x *= scaleFactor;
	integralRect.origin.y *= scaleFactor;
	integralRect.size.width *= scaleFactor;
	integralRect.size.height *= scaleFactor;
	
	// Normalize the rectangle to integer pixel boundaries and then shift the origin by 0.5
	// to produce crisper lines.
	integralRect = NSIntegralRect(integralRect);
	integralRect.origin.x += 0.5;
	integralRect.origin.y += 0.5;
	
	// Convert back to user space.
	integralRect.origin.x /= scaleFactor;
	integralRect.origin.y /= scaleFactor;
	integralRect.size.width /= scaleFactor;
	integralRect.size.height /= scaleFactor;
	
	return integralRect;
	
}


void MGImageGetSlicePixels(CGImageRef image, unsigned char **pixels, NSInteger *pixelCount, NSPoint fromPoint, NSPoint toPoint)
{
	// Get the pixel data along the line between the two specified points, starting with the point that has the highest xVal.
	// Used by the image layers to get slice pixel data.
	
	NSInteger width = CGImageGetWidth(image);
	NSInteger height = CGImageGetHeight(image);
	
	// we need to invert the y coordinate of the image (px 0 is located in the upper left corner)
	NSPoint firstRounded = NSMakePoint(roundf(fromPoint.x), roundf(height - fromPoint.y));
	NSPoint secondRounded = NSMakePoint(roundf(toPoint.x), roundf(width - toPoint.y));
	
	// bugfix: "<=", otherwise lowest may be ==highest
	NSPoint lowestXPoint = firstRounded.x <= secondRounded.x ? secondRounded : firstRounded;
	NSPoint highestXPoint = firstRounded.x > secondRounded.x ? secondRounded : firstRounded;
	
	NSInteger numberOfRows = fabs(firstRounded.x - secondRounded.x);
	NSInteger numberOfColumns = fabs(firstRounded.y - secondRounded.y);
	
	// the weight varies from 0 (the point with lowest xVal) to 1 (the other point).
	// the number of weighted vectors we need is equal to the distance between the two points in pixels, which is approximated by sqrt(pixelsWide^2+pixelsHigh^2).
	CGFloat weightStep = 1.0 / sqrtf((float)(numberOfRows * numberOfRows + numberOfColumns * numberOfColumns));
	
	NSInteger nbSteps = floorf(1.0 / weightStep);
	
	NSInteger currentStep = 0;
	
	// allocate space for the pixel data.
	// sliceData will be released by the object requesting it (i.c. a stack image layer)	
	unsigned char *sliceData = malloc(sizeof(unsigned char) * 4 * nbSteps);
	
	// create a context to draw the image into
	size_t bytesPerRow = 4 * width;
	void *contextData = malloc(bytesPerRow * height);
	CGContextRef context = CGBitmapContextCreate(contextData, width, height, 8, bytesPerRow, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
		
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
	
	char *bitmapData = CGBitmapContextGetData(context);
	
	while (currentStep < nbSteps) {
		
		CGFloat weight = currentStep * weightStep;
		
		NSInteger x = roundf(weight * lowestXPoint.x + (1 - weight) * highestXPoint.x);
		NSInteger y = roundf(weight * lowestXPoint.y + (1 - weight) * highestXPoint.y);
				
		// check if we're within bounds, otherwise we get garbage.
		if (x < width && x >= 0 && y < height && y >= 0) {
			
			char *pixelData = bitmapData + 4 * (x * width + y);
			
			
			sliceData[currentStep * 4] = pixelData[0];
			sliceData[currentStep * 4 + 1] = pixelData[1];
			sliceData[currentStep * 4 + 2] = pixelData[2];
			
		} else {
			
			// fill with a black color
			sliceData[currentStep * 4] = 0x00;
			sliceData[currentStep * 4 + 1] = 0x00;
			sliceData[currentStep * 4 + 2] = 0x00;
			
		}
		
		
		currentStep++;
	}
	
	
	*pixels = sliceData;
	*pixelCount = nbSteps;
	
	
	CGContextRelease(context);
	free(contextData);
	
	
}

CGImageRef MGImageResize(CGImageRef original, CGSize newSize)
{	
	// to be released by caller
	NSInteger width = newSize.width;
	NSInteger height = newSize.height;
	size_t bytesPerRow = 4 * width;
	
	void *memData = malloc(bytesPerRow * height);
	
	CGContextRef context = CGBitmapContextCreate(memData, width, height, 8, bytesPerRow, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	
	CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
	
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), original);
	
	CGImageRef result = CGBitmapContextCreateImage(context);
	
	CGContextRelease(context);
	free(memData);
	
	return result;
}



NSSize MGGetDimensionsForImage(NSString *path)
{
	NSString *fullImgPath = [path stringByExpandingTildeInPath];
	
	CGImageSourceRef src = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:fullImgPath], NULL);
	
	if (!src) return NSZeroSize;
	
	NSDictionary *props = (NSDictionary *)CGImageSourceCopyPropertiesAtIndex(src, 0, NULL);
	[props autorelease];
	
	NSNumber *pixelsWide = (NSNumber *)[props objectForKey:(NSString *)kCGImagePropertyPixelWidth];
	NSNumber *pixelsHigh = (NSNumber *)[props objectForKey:(NSString *)kCGImagePropertyPixelHeight];
		
	return NSMakeSize([pixelsWide intValue], [pixelsHigh intValue]);
}



NSPoint MGRectLineIntersection(NSPoint lineStartPoint, NSPoint lineEndPoint, NSRect rect)
{
	NSPoint outerPoint = NSPointInRect(lineStartPoint, rect) ? lineEndPoint : lineStartPoint;
	NSPoint innerPoint = NSPointInRect(lineStartPoint, rect) ? lineStartPoint : lineEndPoint;
	
	// determine which side of the rectangle will contain the intersection
	CGFloat slope = (outerPoint.y - innerPoint.y) / (outerPoint.x - innerPoint.x);
	
	CGFloat leftUpperCornerSlope = (NSMaxY(rect) - innerPoint.y) / (NSMinX(rect) - innerPoint.x);
	CGFloat rightUpperCornerSlope = (NSMaxY(rect) - innerPoint.y) / (NSMaxX(rect) - innerPoint.x);
	CGFloat leftLowerCornerSlope = (NSMinY(rect) - innerPoint.y) / (NSMinX(rect) - innerPoint.x);
	CGFloat rightLowerCornerSlope = (NSMinY(rect) - innerPoint.y) / (NSMaxX(rect) - innerPoint.x);
	
	NSPoint intersection;
	CGFloat s;
	
	if (slope > rightUpperCornerSlope && slope < leftUpperCornerSlope) {
		
		// top bounds
		intersection.y = NSMaxY(rect);
		s = (intersection.y - innerPoint.y) / (outerPoint.y - innerPoint.y);
		intersection.x = s * outerPoint.x + (1 - s) * innerPoint.x;
		
	} else if (slope < rightLowerCornerSlope && slope > leftLowerCornerSlope) {
		
		// bottom bounds
		intersection.y = NSMinY(rect);
		s = (intersection.y - innerPoint.y) / (outerPoint.y - innerPoint.y);
		intersection.x = s * outerPoint.x + (1 - s) * innerPoint.x;
		
	} else if (slope > rightLowerCornerSlope && slope < rightUpperCornerSlope) {
		
		// right bounds
		intersection.x = NSMaxX(rect);
		s = (intersection.x - innerPoint.x) / (outerPoint.x - innerPoint.x);
		intersection.y = s * outerPoint.y + (1 - s) * innerPoint.y;
		
		
	} else {
		
		// left bounds
		intersection.x = NSMinX(rect);
		s = (intersection.x - innerPoint.x) / (outerPoint.x - innerPoint.x);
		intersection.y = s * outerPoint.y + (1 - s) * innerPoint.y;
		
	}
	
	return intersection;
	
}






@implementation MGFunctionAdditions

@end
