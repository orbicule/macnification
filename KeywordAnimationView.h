//
//  KeywordAnimationView.h
//  Filament
//
//  Created by Peter Schols on 16/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface KeywordAnimationView : NSView {

	int iteration;
	int numberOfIterations;
	BOOL goingUp;
	NSRect browserFrame;	
	NSArray *rectsArray;
	NSTimer *animationTimer;
	NSString *animationSymbol;
	
	int animationType;
	
}

- (void)startAnimation:(NSString *)animation withSymbol:(NSString *)symbol;

- (void)setBrowserFrame:(NSRect)frame;



@property(retain) NSArray *rectsArray;
@property(readwrite, retain) NSString *animationSymbol;


@end
