//
//  ProjectArrayController.m
//  Filament
//
//  Created by Peter Schols on 20/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ProjectArrayController.h"
#import "SourceListTreeController.h"

@implementation ProjectArrayController

- (void)awakeFromNib;
{
	// Sort the subcollections
	NSSortDescriptor *sortRank = [[[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES] autorelease];
	NSSortDescriptor *sortName = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];
	[self setSortDescriptors:[NSArray arrayWithObjects:sortRank, sortName, nil]];
	
}



// Change selection
- (IBAction)switchToClickedCollection:(id)sender;
{
	id clickedCollection = [[[sender infoForBinding:@"title"] objectForKey:@"NSObservedObject"]representedObject];
	[sourceListTreeController selectItem:clickedCollection];
}


@end
