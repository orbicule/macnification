//
//  MGCIImageFusion.h
//  Filament
//
//  Created by Dennis Lorson on 09/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "MGImageAlignmentTypes.h"
#import "MGImageFusion.h"

@interface MGCIImageFusion : NSObject 
{
	CIImage *image_;
	CIImage *refImage_;
}


- (id)initWithImage:(CIImage *)img referenceImage:(CIImage *)refImg;
- (CIImage *)fusionImage;

@end
