//
//  NSAttributedString_MGExtensions.h
//  Filament
//
//  Created by Dennis Lorson on 07/07/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSAttributedString (MGExtensions)

- (NSAttributedString *)attributedStringByAddingOrReplacingAttributes:(NSDictionary *)attrs;

@end
