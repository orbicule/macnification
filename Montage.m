//
//  Montage.m
//  Stack
//
//  Created by Dennis Lorson on 2/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import "Montage.h"

#import "MontageView.h"
#import "StringExtensions.h"
#import "Image.h"
#import "MGFilterAndSortMetadataController.h"
#import "Stack.h"


#define MONTAGE_DRAG_TYPE @"MONTAGE_DRAG_TYPE"


@interface Montage (Private)

- (CGImageRef)imageWithOriginal:(CGImageRef)original size:(CGSize)newSize;

- (void)loadAndApplyPreferences;
- (void)savePreferences;

- (void)updateMontagePreview;


- (NSArray *)_attributeArraySortedWithPreferencesWithOriginal:(NSArray *)original;
- (NSArray *)_preferenceAttributes;
- (NSArray *)_preferenceSelectedAttributes;
- (void)_saveAttrsToPrefs;
- (NSArray *)_defaultAttributeFieldOrder;
- (void)_reloadAttributes;
- (NSDictionary *)_metadataFields;

- (NSArray *)_sortedImages;
- (NSArray *)_sortedSelectedAttributes;
- (NSArray *)_expandStacks:(NSArray *)original;

- (NSString *)_stringForAttributeValue:(id)value;
- (NSImage *)_previewForImage:(Image *)image;


@end



@implementation Montage

@synthesize images, imagesPerRow, backgroundColor, title, displaysDate, displaysTitle;

- (id)init
{
	
	if ((self = [super init])) {
		
		[self retain];
		
		sortedAttributes_ = [[NSMutableArray array] retain];
		selectedAttributes_ = [[NSMutableArray array] retain];
		
		images = [[NSMutableArray array] retain];
		self.backgroundColor = [NSColor colorWithCalibratedWhite:1 alpha:1.0];
		
		self.imagesPerRow = 5;
	}
	
	return self;
}

- (void)awakeFromNib
{
	[includedAttributesTableView_ setDataSource:self];
	[includedAttributesTableView_ setDelegate:self];
	
	[includedAttributesTableView_ registerForDraggedTypes:[NSArray arrayWithObject:MONTAGE_DRAG_TYPE]];
	
	// trigger first load from prefs
	[self _reloadAttributes];
	
	[self loadAndApplyPreferences];

}

- (void)dealloc
{
	[sortedAttributes_ release];
	[selectedAttributes_ release];
	[humanReadableAttributeNames_ release];
	[images release];
	
	[super dealloc];
	
}


- (void)updateMontagePreview
{
	if (!montagePreview)
		montagePreview = [[MontageView alloc] init];
	
	montagePreview.dataSource = self;
	montagePreview.rendersPreview = YES;
	
	[montagePreview updateLayout];
	
	PDFDocument *doc = [montagePreview montagePDF];
	
	[montagePDFPreview setDocument:doc];
	
	
}

- (IBAction)printMontage:(id)sender
{
	wantsPrint = YES;
	
	[progressField setHidden:NO];
	[progressField display];		// stupid appkit...needs this to unhide the field
	[progressIndicator setHidden:NO];
	[progressIndicator setUsesThreadedAnimation:YES];
	[progressIndicator startAnimation:self];
	
	// now close the sheet....
	[NSApp endSheet:montageWindow];
	[montageWindow orderOut:self];
	
	// start the print job when the sheet has been closed
}

- (IBAction)createMontage:(id)sender
{
	[progressField setHidden:NO];
	[progressField display];		// stupid appkit...needs this to unhide the field
	[progressIndicator setHidden:NO];
	[progressIndicator setUsesThreadedAnimation:YES];
	[progressIndicator startAnimation:self];
	
	// create the highres montage view
	MontageView *finalMontageView = [[[MontageView alloc] init] autorelease];
	finalMontageView.dataSource = self;
	finalMontageView.rendersPreview = NO;
	
	[finalMontageView updateLayout];
	
	finalMontage = [[finalMontageView montagePDF] retain];
	
	// now close the sheet....
	[NSApp endSheet:montageWindow];
	[montageWindow orderOut:self];
}

- (IBAction)cancelMontage:(id)sender
{
	// now close the sheet....
	didCancel = YES;
	[NSApp endSheet:montageWindow];
	[montageWindow orderOut:self];
	
}

- (IBAction)selectAllAttributes:(id)sender;
{
	[selectedAttributes_ removeAllObjects];
	[selectedAttributes_ addObjectsFromArray:sortedAttributes_];
	[includedAttributesTableView_ reloadData];
	[self updateMontagePreview];
}

- (IBAction)selectNoAttributes:(id)sender;
{
	[selectedAttributes_ removeAllObjects];
	[includedAttributesTableView_ reloadData];
	[self updateMontagePreview];
}

#pragma mark -
#pragma mark Sheets

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo;
{
	[self savePreferences];
	
	[sheet orderOut:self];
	
	// first, release the preview.
	[montagePreview release];
	montagePreview = nil;
	
	if (didCancel) {
		
		[montageWindow release];
		
		[finalMontage autorelease];
		
		[self autorelease];
		
		return;
	}

	if (!wantsPrint) {
		
		[montageWindow release];
		
				
		// call the delegate
		if (callsDelegate) {
			
			// release the top level nib objects
			[finalMontage autorelease];
			
			[self autorelease];
			
			NSInvocation *returnCall = [NSInvocation invocationWithMethodSignature:[delegate methodSignatureForSelector:didEndSelector]];
			[returnCall setSelector:didEndSelector];
			[returnCall setArgument:&self atIndex:2];
			[returnCall setArgument:&finalMontage atIndex:3];
			
			[returnCall invokeWithTarget:delegate];
			
		} else {
			
			// do not release montage object yet.
			NSSavePanel *panel = [NSSavePanel savePanel];
			[panel setExtensionHidden:YES];
			NSString *saveString = [NSString stringWithFormat:@"%@.pdf", self.title];
			[panel beginSheetForDirectory:nil 
									 file:saveString 
						   modalForWindow:[NSApp mainWindow] 
							modalDelegate:self 
						   didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:) contextInfo:nil];
			
		}

		
	} else {
		
		// create the highres montage view
		MontageView *finalMontageView = [[[MontageView alloc] init] autorelease];
		finalMontageView.dataSource = self;
		finalMontageView.rendersPreview = NO;
		
		[finalMontageView updateLayout];
		
		CGFloat margin = 15.0;
		
		NSPrintInfo *customInfo = [NSPrintInfo sharedPrintInfo];
		[customInfo setTopMargin:margin];
		[customInfo setBottomMargin:margin];
		[customInfo setRightMargin:margin];
		[customInfo setLeftMargin:margin];
		[customInfo setHorizontalPagination:NSFitPagination];
		//[customInfo setVerticalPagination:NSFitPagination];
		
		NSPrintOperation *op = [NSPrintOperation printOperationWithView:finalMontageView];
		[op setPrintInfo:customInfo];
		
		[op runOperationModalForWindow:[NSApp mainWindow] delegate:self didRunSelector:@selector(printOperationDidRun:success:contextInfo:) contextInfo:nil];
	}
}
			 
			 
- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo;
{
	[finalMontage autorelease];
	
	[[finalMontage dataRepresentation] writeToFile:[sheet filename] atomically:YES];
	
	[self autorelease];
	// no need to inform delegate.
	
}


- (void)printOperationDidRun:(NSPrintOperation *)printOperation  success:(BOOL)success  contextInfo:(void *)contextInfo
{
	// still need to clean up and notify the delegate (without any given file)
	// release the top level nib objects
	[montageWindow autorelease];
	
	[self autorelease];
	
	// call the delegate
	if (callsDelegate) {
		
		NSInvocation *returnCall = [NSInvocation invocationWithMethodSignature:[delegate methodSignatureForSelector:didEndSelector]];
		[returnCall setSelector:didEndSelector];
		[returnCall setArgument:&self atIndex:2];
		//[returnCall setArgument:nil atIndex:3];
		
		[returnCall invokeWithTarget:delegate];
		
	}
	
	
}

#pragma mark -
#pragma mark Public


- (void)showPreviewForWindow:(NSWindow *)window delegate:(id)aDelegate didEndSelector:(SEL)aSel;
{
	// the action to do after the montage has been created:
	delegate = aDelegate;
	didEndSelector = aSel;
	
	if (delegate && [delegate respondsToSelector:aSel])
		callsDelegate = YES;
	
	if (!montageWindow)
		[NSBundle loadNibNamed:@"MontagePreview.nib" owner:self];
	
	
	[NSApp beginSheet:montageWindow modalForWindow:window modalDelegate:self didEndSelector:@selector(sheetDidEnd:returnCode:contextInfo:) contextInfo:nil];
		
	[self updateMontagePreview];

}

- (void)addImage:(Image *)image;
{
	// expand stacks
	if ([image isKindOfClass:[Stack class]]) {
		for (Image *stackImage in [(Stack *)image images])
			[self addImage:stackImage];
		
		return;
	}
	
	NSImage *preview = [self _previewForImage:image];
	
	// add the collected data to the data array
	NSDictionary *imageAndData = [NSDictionary dictionaryWithObjectsAndKeys:image, @"image", preview, @"previewImage", nil];
	[images addObject:imageAndData];
}


- (NSImage *)_previewForImage:(Image *)image
{
	NSBitmapImageRep *rep = displaysScaleBars ? [image filteredImageRepWithScaleBar] : [image filteredImageRep];
	
	CGFloat width = 200;
	
	// create the preview images out of the layer contents as they need not be large.
	CGSize smallSize = CGSizeMake(width, (CGFloat)[rep pixelsHigh] / (CGFloat)[rep pixelsWide] * width );
	
	CGImageRef original = [rep CGImage];
	CGImageRef thumbnail = [self imageWithOriginal:original size:smallSize];
	NSBitmapImageRep *thumbRep = [[[NSBitmapImageRep alloc] initWithCGImage:thumbnail] autorelease];
	
	CGImageRelease(thumbnail);
	
	NSImage *previewImage = [[[NSImage alloc] init] autorelease];
	[previewImage addRepresentation:thumbRep];
	
	return previewImage;
}

- (void)_regeneratePreviews
{
	NSMutableArray *newImages = [NSMutableArray array];
	for (NSDictionary *dict in images) {
		
		Image *image = [dict objectForKey:@"image"];
		NSImage *preview = [image valueForKey:@"scaleBar"] ? [self _previewForImage:image] : [dict objectForKey:@"previewImage"];

		NSDictionary *imageAndData = [NSDictionary dictionaryWithObjectsAndKeys:image, @"image", preview, @"previewImage", nil];

		[newImages addObject:imageAndData];
	}
	
	[images removeAllObjects];
	[images addObjectsFromArray:newImages];
}

#pragma mark -
#pragma mark Bindings update

- (void)setImagesPerRow:(NSInteger)img
{
	imagesPerRow = img;
	
	[self updateMontagePreview];
	
}

- (void)setDisplaysDate:(BOOL)flag
{
	displaysDate = flag;
	
	[self updateMontagePreview];
	
}

- (void)setDisplaysTitle:(BOOL)flag
{
	displaysTitle = flag;
	
	[self updateMontagePreview];
	
}

- (void)setTitle:(NSString *)aTitle
{
	if (title != aTitle) {
		
		[title release];
		title = [aTitle retain];
		
		[self updateMontagePreview];
		
	}
}


- (void)setBackgroundColor:(NSColor *)color
{
	if (color != backgroundColor) {
		[backgroundColor release];
		NSColor *rgbColor = [color colorUsingColorSpaceName:NSDeviceRGBColorSpace];
		backgroundColor = [rgbColor retain];
		[self updateMontagePreview];		
	}
}

- (IBAction)changeDisplaysScalebars:(id)sender;
{
	displaysScaleBars = ([(NSButton *)sender state] == NSOnState);
	
	[self _regeneratePreviews];
	[self updateMontagePreview];
}

#pragma mark -
#pragma mark Table data source


- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
	return [sortedAttributes_ count];
}

- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	
	if ([anObject intValue] == NSOnState)
		[selectedAttributes_ addObject:[sortedAttributes_ objectAtIndex:rowIndex]];
	else
		[selectedAttributes_ removeObject:[sortedAttributes_ objectAtIndex:rowIndex]];
	
	[self _saveAttrsToPrefs];
	[includedAttributesTableView_ reloadData];
	
	[self updateMontagePreview];
	
}


- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	if ([[aTableColumn identifier] isEqualToString:@"key"]) {
		
		NSString *identifier = [humanReadableAttributeNames_ objectForKey:[sortedAttributes_ objectAtIndex:rowIndex]];
		
		if ([[sortedAttributes_ objectAtIndex:rowIndex] isEqualToString:@"name"] ||  [[sortedAttributes_ objectAtIndex:rowIndex] isEqualToString:@"image.name"])
			return [[[NSAttributedString alloc] initWithString:@"Image/Stack Name" 
													attributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12], NSFontAttributeName, nil]] 
					autorelease];
		
		else
			return identifier;
		
	}
	
	else
		return ([selectedAttributes_ containsObject:[sortedAttributes_ objectAtIndex:rowIndex]]) ? [NSNumber numberWithInt:NSOnState] : [NSNumber numberWithInt:NSOffState];
	
}

- (BOOL)tableView:(NSTableView *)aTableView shouldSelectRow:(NSInteger)rowIndex
{
	return YES;
}



#pragma mark -
#pragma mark Montage view datasource

- (int)montageViewNumberOfImages:(MontageView *)view;
{
	return [[self _expandStacks:images] count];
}

- (NSImage *)montageView:(MontageView *)view imageAtIndex:(int)index;
{
	NSDictionary *dict = [[self _sortedImages] objectAtIndex:index];
	
	if (view.rendersPreview)
		return [dict objectForKey:@"previewImage"];
	
	NSImage *image = [[[NSImage alloc] init] autorelease];
	
	if (displaysScaleBars)
		[image addRepresentation:[(Image *)[dict objectForKey:@"image"] filteredImageRepWithScaleBar]];
	else
		[image addRepresentation:[(Image *)[dict objectForKey:@"image"] filteredImageRep]];

	return image;
}

- (int)montageViewNumberOfMetadataFields:(MontageView *)view;
{
	return [selectedAttributes_ count];
}

- (NSString *)montageView:(MontageView *)view valueForMetadataFieldAtIndex:(int)metadataIndex imageIndex:(int)imageIndex;
{
	NSArray *sortedImages = [self _sortedImages];
	NSArray *sortedAttrs = [self _sortedSelectedAttributes];
	
	return [self _stringForAttributeValue:[[[sortedImages objectAtIndex:imageIndex] objectForKey:@"image"] valueForKey:[sortedAttrs objectAtIndex:metadataIndex]]];
}

- (int)montageViewImagesPerRow:(MontageView *)view;
{
	return imagesPerRow;
}

- (NSColor *)backgroundColor;
{
	return backgroundColor;
}

- (BOOL)displaysTitle;
{
	return displaysTitle;
}
- (NSString *)title;
{
	return title;
}

- (BOOL)displaysDate;
{
	return displaysDate;
}




#pragma mark -
#pragma mark Dragging

- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard
{
	draggedTableViewIndex_ = [rowIndexes firstIndex];
	
	[pboard setData:[NSData data] forType:MONTAGE_DRAG_TYPE];
	
	return YES;
}

- (NSDragOperation)tableView:(NSTableView *)aTableView validateDrop:(id < NSDraggingInfo >)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation
{
	if ([info draggingSource] != aTableView)
		return NSDragOperationNone;
	
	if (operation == NSTableViewDropOn) {
		int newRow = MAX(0, MIN([aTableView numberOfRows], row));
		[aTableView setDropRow:newRow dropOperation:NSTableViewDropAbove];
	}
	
	return NSDragOperationMove;
}

- (BOOL)tableView:(NSTableView *)aTableView acceptDrop:(id < NSDraggingInfo >)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation
{
	if (operation == NSTableViewDropOn)
		return NO;
	
	if (draggedTableViewIndex_ == -1 || draggedTableViewIndex_ > [sortedAttributes_ count] - 1)
		return NO;
	
	// identity operation
	if (row == draggedTableViewIndex_)
		return YES;
	
	// move the item to the correct array index
	NSString *item = [sortedAttributes_ objectAtIndex:draggedTableViewIndex_];
	
	[sortedAttributes_ insertObject:item atIndex:row];
	
	// if the dst index < the src index, we need to add one to the src index to find the index to remove
	if (draggedTableViewIndex_ > row)
		[sortedAttributes_ removeObjectAtIndex:draggedTableViewIndex_ + 1];
	else
		[sortedAttributes_ removeObjectAtIndex:draggedTableViewIndex_];
	
	[includedAttributesTableView_ reloadData];
	[self _saveAttrsToPrefs];
	[self updateMontagePreview];
	
	
	return YES;
}





#pragma mark -
#pragma mark Preferences


- (void)loadAndApplyPreferences
{
	// Load any preferences the user might have
	NSDictionary *preferences = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"MontagePrefs"];
	
	if ([preferences objectForKey:@"backgroundColor"]) {
		
		NSColor *bgColor = [NSUnarchiver unarchiveObjectWithData:[preferences objectForKey:@"backgroundColor"]];
		self.backgroundColor = bgColor;
		
	}
	
	if ([preferences objectForKey:@"imagesPerRow"]) {
		
		self.imagesPerRow = [[preferences objectForKey:@"imagesPerRow"] intValue];
		
	}
	
	if ([preferences objectForKey:@"displaysTitle"]) {
		
		self.displaysTitle = [[preferences objectForKey:@"displaysTitle"] boolValue];
		
	}
	
	if ([preferences objectForKey:@"displaysDate"]) {
		
		self.displaysDate = [[preferences objectForKey:@"displaysDate"] boolValue];
		
	}
	
}

- (void)savePreferences
{
	NSMutableDictionary *montagePrefs = [NSMutableDictionary dictionary];

	NSData *bgColorData = [NSArchiver archivedDataWithRootObject:self.backgroundColor];
	[montagePrefs setValue:bgColorData forKey:@"backgroundColor"];
	
	[montagePrefs setValue:[NSNumber numberWithInt:self.imagesPerRow] forKey:@"imagesPerRow"];
		
	[montagePrefs setValue:[NSNumber numberWithBool:self.displaysTitle] forKey:@"displaysTitle"];
	
	[montagePrefs setValue:[NSNumber numberWithBool:self.displaysDate] forKey:@"displaysDate"];

	
	[[NSUserDefaults standardUserDefaults] setObject:montagePrefs forKey:@"MontagePrefs"];
}


#pragma mark -
#pragma mark Prefs (attributes)

- (NSArray *)_attributeArraySortedWithPreferencesWithOriginal:(NSArray *)original
{
	NSArray *prefsArray = [self _preferenceAttributes];
	
	if (!prefsArray)
		return original;
	
	NSMutableArray *sorted = [NSMutableArray array];
	
	// filter the prefs: only retain what is in the original array.  This way we retain the sort order from the prefs array.
	for (NSString *attr in prefsArray)
		if ([original containsObject:attr])
			[sorted addObject:attr];
	
	// add any attrs that are in original, but not in the prefs
	NSMutableArray *newAttrs = [[[original mutableCopy] mutableCopy] autorelease]; 
	[newAttrs removeObjectsInArray:sorted];
	
	[sorted addObjectsFromArray:newAttrs];
	
	return sorted;
	
}

- (NSArray *)_preferenceAttributes
{
	NSString *prefKey = @"montageAttributes";
	
	if ([[NSUserDefaults standardUserDefaults] objectForKey:prefKey])
		return[[NSUserDefaults standardUserDefaults] arrayForKey:prefKey];
	
	return [self _defaultAttributeFieldOrder];
}


- (NSArray *)_preferenceSelectedAttributes
{	
	return [[NSUserDefaults standardUserDefaults] arrayForKey:@"montageAttributesSelected"];
}

- (void)_saveAttrsToPrefs
{
	[[NSUserDefaults standardUserDefaults] setObject:sortedAttributes_ forKey:@"montageAttributes"];
	[[NSUserDefaults standardUserDefaults] setObject:selectedAttributes_ forKey:@"montageAttributesSelected"];
}

- (NSArray *)_defaultAttributeFieldOrder
{
	return [[MGFilterAndSortMetadataController sharedController] exportAttributeKeyPaths];
}


- (void)_reloadAttributes
{
	[sortedAttributes_ removeAllObjects];
	
	NSDictionary *newAttributesAndNames = [self _metadataFields];
	
	// sort them according to the prefs
	NSArray *newAttributes = [newAttributesAndNames allKeys];
	newAttributes = [self _attributeArraySortedWithPreferencesWithOriginal:newAttributes];
	
	
	[sortedAttributes_ addObjectsFromArray:newAttributes];
	
	
	[selectedAttributes_ removeAllObjects];
	// select some (get from prefs, or all if not available in prefs)
	NSArray *prefsSelectedAttrs = [self _preferenceSelectedAttributes];
	if (!prefsSelectedAttrs)
		;//[selectedAttributes_ addObjectsFromArray:sortedAttributes_];
	else {
		
		// first filter the selected attrs with those actually available
		NSMutableArray *prefsSelectedAttrsFiltered = [[prefsSelectedAttrs mutableCopy] autorelease];
		for (NSString *attr in prefsSelectedAttrs)
			if (![sortedAttributes_ containsObject:attr])
				[prefsSelectedAttrsFiltered removeObject:attr];
		
		[selectedAttributes_ addObjectsFromArray:prefsSelectedAttrsFiltered];
	}
	
	
	[humanReadableAttributeNames_ release];
	humanReadableAttributeNames_ = [newAttributesAndNames retain];
	
	
	
	[includedAttributesTableView_ reloadData];
	[includedAttributesTableView_ deselectAll:self];
	
}


- (NSDictionary *)_metadataFields;
{
	NSMutableDictionary *fields = [NSMutableDictionary dictionary];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] exportAttributeKeyPaths])
		[fields setObject:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath] forKey:keyPath];
	
	return fields;
}

#pragma mark -
#pragma mark Sorting etc


- (NSArray *)_sortedImages
{
	NSMutableArray *sortDescriptors = [NSMutableArray array];
	for (NSString *keyPath in [self _sortedSelectedAttributes]) {
		
		if ([sortDescriptors count] >= 5)
			continue;
		
		// relative to dictionary
		[sortDescriptors addObject:[NSSortDescriptor sortDescriptorWithKey:[@"image." stringByAppendingString:keyPath] ascending:YES]];
	}
	
	
	return [images sortedArrayUsingDescriptors:sortDescriptors];
		
}

- (NSArray *)_sortedSelectedAttributes
{
	NSMutableArray *sortedSelected = [NSMutableArray array];
	
	for (NSString *keyPath in sortedAttributes_)
		if ([selectedAttributes_ containsObject:keyPath])
			[sortedSelected addObject:keyPath];
	
	return sortedSelected;
	
}


- (NSArray *)_expandStacks:(NSArray *)original
{	
	NSMutableArray *stacksExpanded = [NSMutableArray array];
	for (Image *image in original) {
		if ([image isKindOfClass:[Stack class]])
			[stacksExpanded addObjectsFromArray:[(Stack *)image images]];
		else
			[stacksExpanded addObject:image];
	}
	
	return stacksExpanded;
}

#pragma mark -
#pragma mark Utility methods

- (NSString *)_stringForAttributeValue:(id)value
{
	if (value == NULL)
		return @"-";
	
	if (value == NSMultipleValuesMarker)
		return @"[Multiple]";
	
	
	if ([value isKindOfClass:[NSString class]]) {
		if ([value length] == 0)
			return @"-";
		else
			return value;		
	}
	
	if ([value isKindOfClass:[NSDate class]])
		return [value description];

	
	return [value descriptionWithLocale:[NSLocale currentLocale]];
}


- (CGImageRef)imageWithOriginal:(CGImageRef)original size:(CGSize)newSize
{
	// to be released by caller
	NSInteger width = newSize.width;
	NSInteger height = newSize.height;
	size_t bytesPerRow = 4 * width;
	
	void *memData = malloc(bytesPerRow * height);
	
	CGContextRef context = CGBitmapContextCreate(memData, width, height, 8, bytesPerRow, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), original);
	
	CGImageRef result = CGBitmapContextCreateImage(context);
	
	CGContextRelease(context);
	free(memData);
	
	return result;
}

@end
