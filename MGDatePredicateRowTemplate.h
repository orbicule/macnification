//
//  MGDatePredicateRowTemplate.h
//  Filament
//
//  Created by Dennis Lorson on 17/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGDatePredicateRowTemplate : NSPredicateEditorRowTemplate
{
	NSDatePicker *datePicker_;
}

@end
