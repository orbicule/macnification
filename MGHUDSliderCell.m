//
//  MGHUDSliderCell.m
//  Filament
//
//  Created by Dennis Lorson on 26/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDSliderCell.h"


@implementation MGHUDSliderCell



- (void)drawBarInside:(NSRect)cellFrame flipped:(BOOL)flipped
{
	cellFrame.origin.x -= 0.5;
	NSImage *leftImage = [NSImage imageNamed:@"hud_slider_left.tiff"];
	NSImage *fillImage = [NSImage imageNamed:@"hud_slider_middle.tiff"];
	NSImage *rightImage = [NSImage imageNamed:@"hud_slider_right.tiff"];
	
	NSSize size = [leftImage size];
	float addX = size.width / 2.0;
	float y = NSMaxY(cellFrame) - (cellFrame.size.height-size.height)/2.0 - 1;
	float x = cellFrame.origin.x+addX;
	float fillX = x + size.width;
	float fillWidth = cellFrame.size.width - size.width - addX;
	
	CGFloat alpha = [self isEnabled] ? 0.6 : 0.3;
	
	[leftImage compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:alpha];
	
	size = [rightImage size];
	addX = size.width / 2.0;
	x = NSMaxX(cellFrame) - size.width - addX;
	fillWidth -= size.width+addX;
	
	[rightImage compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:alpha];
	
	[fillImage setScalesWhenResized:YES];
	[fillImage setSize:NSMakeSize(fillWidth, [fillImage size].height)];
	[fillImage compositeToPoint:NSMakePoint(fillX, y) operation:NSCompositeSourceOver fraction:alpha];
}

- (void)drawKnob:(NSRect)rect
{
	CGFloat alpha = [self isEnabled] ? 1.0 : 0.35;

	NSImage *knob = [NSImage imageNamed:@"hud_slider_knob.tiff"];
		
	float x = rect.origin.x + (rect.size.width - [knob size].width) / 2;
	float y = NSMaxY(rect) - (rect.size.height - [knob size].height) / 2 - 1;
	
	[knob compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:alpha];
}

-(NSRect)knobRectFlipped:(BOOL)flipped
{
	NSRect rect = [super knobRectFlipped:flipped];
	if([self numberOfTickMarks] > 0){
		rect.size.height+=2;
		return NSOffsetRect(rect, 0, flipped ? 2 : -2);
	}
	return rect;
}

- (BOOL)_usesCustomTrackImage
{
	return YES;
}




@end
