//
//  MGOMEFileImporter.m
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGOMEFileImporter.h"

#import "MGMetadataSet.h"
#import "MGImageFileImporter_Private.h"
#import "MGOMESubFileImporter.h"
#import "MGOMEFileImporter_Private.h"
#import "StringExtensions.h"

@interface MGOMEFileImporter ()

- (NSString *)_lociToolsMetadataNameForFile:(NSString *)file;

- (BOOL)_hasSingleImageTag:(NSXMLElement *)root;


// for creating our own subimporters
- (void )_generateInfoForSubImportersFromRoot:(NSXMLElement *)root;

// easily add channel metadata
- (void)_addMetadata:(NSObject *)value forKey:(NSString *)key channelIndex:(int)index toChannelArray:(NSMutableArray *)array;
- (void)_addMetadata:(NSObject *)value forKey:(NSString *)key withIndexInString:(NSString *)string toChannelArray:(NSMutableArray *)array;

@end



@implementation MGOMEFileImporter



+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	NSArray *supportedTypes = [NSArray arrayWithObjects:@"ome.tiff", @"ome.tif", nil];
	
	for (NSString *type in supportedTypes)
		if ([[[path lastPathComponent] lowercaseString] rangeOfString:type].location != NSNotFound)
			return YES;
	
	return NO;
}

+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:@"tiff", @"tif", nil];
}

- (float)fractionOfImportTimeForNonRepresentationWork;
{
	return 0.3;
}


- (void) dealloc
{	
	[super dealloc];
}



- (MGMetadataSet *)_generateMetadata
{
	// Does 1 of 2 things:
	// * Generates and returns the metadata, if there's only one Image tag in the OME-XML
	// * returns nil and creates a subimporter array
	
	NSString *pathToOMEFolder = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Contents/Resources/ome/"];
		
    ;

    
	NSPipe *pipe = [NSPipe pipe];
	NSTask *javaTask = [[NSTask alloc] init];
	[javaTask setStandardOutput:pipe];
	[javaTask setLaunchPath:@"/usr/bin/java"];
	[javaTask setArguments:[NSArray arrayWithObjects:@"-mx512m", 
							@"-classpath", 
							[pathToOMEFolder stringByAppendingPathComponent:[self _lociToolsMetadataNameForFile:[self originalFilePath]]], 
							@"loci.formats.tools.TiffComment", 
							[self representationFilePath],
							nil]];
	[javaTask launch];
	
	NSData *outputData = [[pipe fileHandleForReading]readDataToEndOfFile];
	
	[javaTask waitUntilExit];
	[javaTask release];
	
    ;

    

    
	MGMetadataSet *metadata = [super _generateMetadata];
	[metadata setValue:[NSMutableArray array] forKey:MGMetadataChannelsKey];
	
	//NSLog(@"%@",[[[NSString alloc] initWithData:outputData encoding:NSISOLatin1StringEncoding] autorelease]);
	
	NSXMLDocument *doc = [[[NSXMLDocument alloc] initWithData:outputData options:0 error:nil] autorelease];
	NSXMLElement *ome = [doc rootElement];

	if ([self _hasSingleImageTag:ome])
		[self _addMetadataFromRoot:ome toSet:metadata];
	else
		[self _generateInfoForSubImportersFromRoot:ome];
	
	return metadata;
	
}


- (BOOL)_hasSingleImageTag:(NSXMLElement *)root;
{
	NSArray *imageTags = [root nodesForXPath:@".//Image" error:nil];
	
	return [imageTags count] <= 1;
}


- (void )_generateInfoForSubImportersFromRoot:(NSXMLElement *)root
{
	// one array entry:
	// 0:: xml
	// 1:: representation range
	
	NSArray *images = [root nodesForXPath:@".//Image" error:nil];
	NSArray *instruments = [root nodesForXPath:@".//Instrument" error:nil];
	
	
	////////////////////////////////////////////////////////////////
	// Match instruments and ranges to images
	NSMutableArray *imageInfo = [NSMutableArray array];
	
	for (NSXMLElement *image in images) {
		
		NSString *instrumentRefID = [[self _elementForXPath:@"//InstrumentRef[1]/@ID" inElement:image] stringValue];
		
		NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObject:image forKey:@"image"];
		[imageInfo addObject:dict];
				
		for (NSXMLElement *instrument in instruments) {
			NSString *instrumentID = [[self _elementForXPath:@"@ID" inElement:instrument] stringValue];
			
			if ([instrumentID isEqualTo:instrumentRefID])
				[dict setObject:instrument forKey:@"instrument"];
		}
		

	}
	
	////////////////////////////////////////////////////////////////
	// For each image, generate a new root element
	
	
	
	[subImporters_ release];
	subImporters_ = [[NSMutableArray array] retain];
	
	for (NSDictionary *imageDict in imageInfo) {
		
		NSXMLElement *newRoot = [[[NSXMLElement alloc] initWithName:@"OME"] autorelease];
		
		[newRoot addChild:[[[imageDict objectForKey:@"image"] copy] autorelease]];
		
		if ([imageDict objectForKey:@"instrument"])
			[newRoot addChild:[[[imageDict objectForKey:@"instrument"] copy] autorelease]];
		
		
		NSString *xml = [newRoot XMLString];
		
		NSLog(@"\n\n\n\n%@\n\n\n\n", xml);
		
		NSXMLDocument *doc = [[[NSXMLDocument alloc] initWithXMLString:xml options:0 error:nil] autorelease];
		
		
		[subImporters_ addObject:[[[MGOMESubFileImporter alloc] initWithCommonMetadata:[super _generateMetadata] 
																		   xmlDocument:doc 
																			  filePath:[self originalFilePath]
																representationFilePath:[self representationFilePath]
																		 superImporter:self] autorelease]];
		
	}

		
}




- (void)_addMetadataFromRoot:(NSXMLElement *)root toSet:(MGMetadataSet *)set
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// INSTRUMENT
	
	NSXMLElement *instrument = [self _elementForName:@"Instrument" inElement:root];
	NSXMLElement *instrumentModel = [self _elementForXPath:@"//Microscope/@Model" inElement:instrument];

	
	NSXMLElement *objective = [self _elementForName:@"Objective" inElement:instrument];
	
	NSXMLElement *immersion = [self _elementForName:@"Immersion" inElement:objective];
	NSXMLElement *objectiveModel = [self _elementForXPath:@"@Model" inElement:objective];
	NSXMLElement *workingDistance = [self _elementForName:@"WorkingDistance" inElement:objective];
	NSXMLElement *lensNA = [self _elementForName:@"LensNA" inElement:objective];
	NSXMLElement *nominalMagnification = [self _elementForName:@"NominalMagnification" inElement:objective];
	
    
	if ([instrumentModel stringValue])
		[set setValue:[instrumentModel stringValue] forKey:MGMetadataInstrumentNameKey];
	
	if ([immersion stringValue])
		[set setValue:[immersion stringValue] forKey:MGMetadataInstrumentImmersionKey];
	
	if ([lensNA stringValue])
		[set setValue:[NSNumber numberWithFloat:[[lensNA stringValue] floatValue]] forKey:MGMetadataInstrumentNumericApertureKey];
	
	if ([nominalMagnification stringValue])
		[set setValue:[NSNumber numberWithFloat:[[nominalMagnification stringValue] floatValue]] forKey:MGMetadataInstrumentMagnificationKey];
	
	if ([objectiveModel stringValue])
		[set setValue:[objectiveModel stringValue] forKey:MGMetadataInstrumentObjectiveNameKey];
	
	if ([workingDistance stringValue])
		[set setValue:[NSNumber numberWithFloat:[[workingDistance stringValue] floatValue]] forKey:MGMetadataInstrumentWorkingDistanceKey];

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// CHANNELS
	
	NSXMLElement *image = [self _elementForName:@"Image" inElement:root];
	
	NSXMLNode *imageName = [self _elementForXPath:@"@Name" inElement:image];
	
	
	// for some stupid stupid reason, this can be the current file path (it's what Bio-Formats does to ZVI files, which have no name attr)
	if ([[imageName stringValue] length]) {

		// remove the path if necessary
		NSString *modifiedName = [[[imageName stringValue] lastPathComponent] stringByDeletingPathExtension];
		
		[set setValue:modifiedName forKey:MGMetadataImageNameKey];

	}
	
	
	NSArray *channels = [image elementsForName:@"LogicalChannel"];
	NSArray *lightSources = [instrument elementsForName:@"LightSource"];
	
	// create channels
	NSMutableArray *channelMD = [NSMutableArray array];
	
	
	int i = 0;
	
	for (NSXMLElement *channel in channels) {
		
		NSMutableDictionary *channelDict = [NSMutableDictionary dictionary];
		[channelMD addObject:channelDict];
		
		NSXMLNode *name = [channel attributeForName:@"Name"];
		NSXMLNode *pinholeSize = [channel attributeForName:@"PinholeSize"];
		NSXMLNode *waveLength = [channel attributeForName:@"EmWave"];
		NSXMLNode *contrastMethod = [channel attributeForName:@"ContrastMethod"];
		NSXMLNode *mode = [channel attributeForName:@"Mode"];
		

        
		if ([name stringValue])
			[channelDict setValue:[name stringValue] forKey:MGMetadataChannelNameKey];
		
		if ([pinholeSize stringValue])
			[channelDict setValue:[NSNumber numberWithFloat:[[pinholeSize stringValue] floatValue]] forKey:MGMetadataChannelPinholeSizeKey];
		
		if ([waveLength stringValue])
			[channelDict setValue:[NSNumber numberWithFloat:[[waveLength stringValue] floatValue]] forKey:MGMetadataChannelWavelengthKey];
		
		if ([contrastMethod stringValue])
			[channelDict setValue:[contrastMethod stringValue] forKey:MGMetadataChannelContrastMethodKey];
		
		if ([mode stringValue])
			[channelDict setValue:[mode stringValue] forKey:MGMetadataChannelModeKey];

		
		// add light source info if the light sources correspond
		NSXMLElement *lightSource = nil;
		
		if ([lightSources count] == [channels count])
			lightSource = [lightSources objectAtIndex:i];
		else if ([lightSources count] == 1)
			lightSource = [lightSources objectAtIndex:0];
		
		NSString *lightSourceName = nil;
		NSXMLElement *lightSourceElement = nil;
		
		if ((lightSourceElement = [self _elementForName:@"Laser" inElement:lightSource]))
			lightSourceName = @"Laser";
		else if ((lightSourceElement = [self _elementForName:@"Filament" inElement:lightSource]))
			lightSourceName = @"Filament";
		else if ((lightSourceElement = [self _elementForName:@"Arc" inElement:lightSource]))
			lightSourceName = @"Arc";
		
		
		if (lightSourceElement) {
			
			NSXMLNode *lightSourceType = [lightSourceElement attributeForName:@"Type"];
			NSXMLNode *medium = [lightSourceElement attributeForName:@"LaserMedium"];
			
			
			[channelDict setValue:lightSourceName forKey:MGMetadataChannelLightSourceNameKey];
			
			if ([lightSourceType stringValue])
				[channelDict setValue:[lightSourceType stringValue] forKey:MGMetadataChannelLightSourceTypeKey];
			
			if ([medium stringValue])
				[channelDict setValue:[medium stringValue] forKey:MGMetadataChannelMediumKey];
			
		}
		
		i++;
	}
	
    ;
	
	[set setValue:channelMD forKey:MGMetadataChannelsKey];
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// EXPERIMENTER
	
	NSXMLElement *experimenter = [self _elementForName:@"Experimenter" inElement:root];
	NSXMLElement *firstName = [self _elementForName:@"FirstName" inElement:experimenter];
	NSXMLElement *lastName = [self _elementForName:@"LastName" inElement:experimenter];
	
	NSString *compoundName = nil;
	
	if ([firstName stringValue] && [lastName stringValue])
		compoundName = [[firstName stringValue] stringByAppendingFormat:@" %@", [lastName stringValue]];
	else if ([firstName stringValue])
		compoundName = [firstName stringValue];
	else if ([lastName stringValue])
		compoundName = [lastName stringValue];
	
	if (compoundName)
		[set setValue:compoundName forKey:MGMetadataExperimenterNameKey];
	
    ;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// CHANNELS (PLANES)
    

	
	NSXMLElement *pixels = [self _elementForName:@"Pixels" inElement:image];
	
	NSArray *planes = [pixels elementsForName:@"Plane"];
	
	NSMutableArray *planesMD = [NSMutableArray array];
	
	for (NSXMLElement *plane in planes) {
		
		NSMutableDictionary *planeMD = [NSMutableDictionary dictionary];
		
		if ([[plane attributeForName:@"TheC"] stringValue])
			[planeMD setObject:[NSNumber numberWithInt:[[[plane attributeForName:@"TheC"] stringValue] intValue]] forKey:MGMetadataImagePlaneCKey];
		
		if ([[plane attributeForName:@"TheZ"] stringValue])
			[planeMD setObject:[NSNumber numberWithInt:[[[plane attributeForName:@"TheZ"] stringValue] intValue]] forKey:MGMetadataImagePlaneZKey];
		
		if ([[plane attributeForName:@"TheT"] stringValue])
			[planeMD setObject:[NSNumber numberWithInt:[[[plane attributeForName:@"TheT"] stringValue] intValue]] forKey:MGMetadataImagePlaneTKey];
		
		[planesMD addObject:planeMD];
		
	}
	
	if ([planesMD count])
		[set setValue:planesMD forKey:MGMetadataImagePlanesKey];
	
	;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// OTHER
	
	
	// Pixel Info (Size of the batch in Time (T) / Channels (C) / Space (X, Y, Z))
	NSXMLNode *bigEndian = [pixels attributeForName:@"BigEndian"];
	NSXMLNode *sizeC = [pixels attributeForName:@"SizeC"];
	NSXMLNode *pixelSize = [pixels attributeForName:@"PixelSizeX"];
	if (!pixelSize)
		 pixelSize = [image attributeForName:@"PixelSizeX"];

	NSXMLNode *physicalSize = [pixels attributeForName:@"PhysicalSizeX"];
	NSXMLNode *sizeX = [pixels attributeForName:@"SizeX"];
	
	if ([[bigEndian stringValue] isEqualTo:@"true"])
		[set setValue:[NSNumber numberWithBool:NO] forKey:MGMetadataImageLittleEndianKey];
	else if ([[bigEndian stringValue] isEqualTo:@"false"])
		[set setValue:[NSNumber numberWithBool:YES] forKey:MGMetadataImageLittleEndianKey];
	
	
	if ([sizeC stringValue])
		[set setValue:[NSNumber numberWithInt:[[sizeC stringValue] intValue]] forKey:MGMetadataNumberOfChannelsKey];
	
	if ([pixelSize stringValue])
		[set setValue:[NSNumber numberWithFloat:[[pixelSize stringValue] floatValue]] forKey:MGMetadataImagePixelSizeXKey];
	
	if ([physicalSize stringValue]) {
		// PhysicalSizeX: size of a pixel, in um
		[set setValue:@"µm" forKey:MGMetadataCalibrationUnitKey];
		[set setValue:[NSNumber numberWithFloat:[[physicalSize stringValue] floatValue]] forKey:MGMetadataImagePhysicalSizeXKey];
	}
	
	if ([sizeX stringValue])
		[set setValue:[NSNumber numberWithInt:[[sizeX stringValue] intValue]] forKey:MGMetadataImageSizeXKey];
	
    ;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// CUSTOM
	
	
	// Search for possible headers with Custom Attributes
	
	NSMutableArray *customAttrsPossibleParentElements = [NSMutableArray array];

	NSXMLElement *customAttrParentElement = nil;	
    
    

  
  if ((customAttrParentElement = [self _elementForName:@"CA:CustomAttributes" inElement:image]))
      [customAttrsPossibleParentElements addObject:customAttrParentElement];
  if ((customAttrParentElement = [self _elementForName:@"CustomAttributes" inElement:image]))
      [customAttrsPossibleParentElements addObject:customAttrParentElement];

	
	// For each header, retrieve the set of attributes and search those
	for (NSXMLElement *customAttrElement in customAttrsPossibleParentElements) {
		
		NSArray *customAttrs = [customAttrElement elementsForName:@"OriginalMetadata"];
		
		NSString *date = nil, *time = nil;
				
		for (NSXMLElement *customAttr in customAttrs) {
			
			NSString *name = [[customAttr attributeForName:@"Name"] stringValue];
			NSString *value = [[customAttr attributeForName:@"Value"] stringValue];
			
			if ([name isEqualTo:@"Microscope"])
				[set setValue:value forKey:MGMetadataInstrumentNameKey];
			
			else if ([name isEqualTo:@"Scale Factor for X"])
				[set setValue:value forKey:MGMetadataImagePhysicalSizeXKey];
            
            else if ([name isEqualTo:@"Scale Unit for X"]) {
                
                int enumValue = [value intValue];
                
                if (enumValue != 0) {
                    if (enumValue == 76)
                        [set setValue:@"µm" forKey:MGMetadataCalibrationUnitKey];
                }
                
            }
            

            
            else if ([name isEqualTo:@"User Name"])
				[set setValue:value forKey:MGMetadataExperimenterNameKey];
			
			else if ([name isEqualTo:@"Magnification"])
				[set setValue:[NSNumber numberWithInt:[value intValue]] forKey:MGMetadataInstrumentMagnificationKey];

			else if ([name isEqualTo:@"Objective Lens"])
				[set setValue:value forKey:MGMetadataInstrumentObjectiveNameKey];
			
			else if ([name isEqualTo:@"Map min"])
				[set setValue:[NSNumber numberWithFloat:[value floatValue]] forKey:MGMetadataImageIntensityRangeMinKey];
			
			else if ([name isEqualTo:@"Map max"])
				[set setValue:[NSNumber numberWithFloat:[value floatValue]] forKey:MGMetadataImageIntensityRangeMaxKey];
			
			else if ([name isEqualTo:@"Date"])
				date = value;
			
			else if ([name isEqualTo:@"Time"])
				time = value;	
            
            
            
            // Channel custom metadata
            
            else if ([name beginsWith:@"Channel Name"])
                [self _addMetadata:value forKey:MGMetadataChannelNameKey withIndexInString:name toChannelArray:channelMD];

            else if ([name beginsWith:@"Emission Wavelength"])
                [self _addMetadata:value forKey:MGMetadataChannelWavelengthKey withIndexInString:name toChannelArray:channelMD];
            
            ;

		}

		
		if (date && time) {
			
			NSString *datetime = [NSString stringWithFormat:@"%@ %@", date, time];
			
			NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
			[f setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
			
			NSDate *date = [f dateFromString:datetime];
						
			if (date)
				[set setValue:date forKey:MGMetadataImageCreationDateKey];
			
		}
		
	}
	

	
}

- (void)_addMetadata:(NSObject *)value forKey:(NSString *)key withIndexInString:(NSString *)string toChannelArray:(NSMutableArray *)array;
{
    // string is "Property Name X", with X an integer
    NSMutableCharacterSet *set = [[[NSCharacterSet punctuationCharacterSet] mutableCopy] autorelease];
    [set formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *tmp = [string stringByTrimmingCharactersInSet:set];
    
    ;

    NSRange indexRange = [tmp rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet] options:NSBackwardsSearch];
    
    if (indexRange.location == NSNotFound)
        return;
    
    NSString *indexString = [tmp substringWithRange:indexRange];
    
    int index = [indexString intValue];
    
    [self _addMetadata:value forKey:key channelIndex:index toChannelArray:array];
    
}

- (void)_addMetadata:(NSObject *)value forKey:(NSString *)key channelIndex:(int)index toChannelArray:(NSMutableArray *)array;
{
    while (index > (int)[array count] - 1)
        [array addObject:[NSMutableDictionary dictionary]];
    
    NSMutableDictionary *channelDict = [array objectAtIndex:index];
    
    [channelDict setObject:value forKey:key];
        
}

#pragma mark -
#pragma mark Tools

- (NSXMLElement *)_elementForXPath:(NSString *)path inElement:(NSXMLElement *)parent
{
	if (!parent)
		return nil;
	
	NSArray *elements = [parent nodesForXPath:path error:nil];
	
	if ([elements count])
		return [elements objectAtIndex:0];
	
	return nil;
}

- (NSXMLElement *)_elementForName:(NSString *)name inElement:(NSXMLElement *)parent
{
	if (!parent)
		return nil;
	
	NSArray *elements = [parent elementsForName:name];
	
	if ([elements count] > 0)
		return [elements objectAtIndex:0];
	else
		return nil;

}


- (NSString *)_lociToolsMetadataNameForFile:(NSString *)file
{
    
	NSString *ext = [[file pathExtension] lowercaseString];
	
	if ([ext isEqualTo:@"dm3"] || [ext isEqualTo:@"lif"]) {
		return @"loci_tools.jar";	
	}
	
	return @"loci_tools.jar";
}


#pragma mark -
#pragma mark Sub-importers

- (NSArray *)subImporters;
{
	// load the metadata; will also create the sub importers if not available yet
	[self metadata];
	
	return subImporters_;
}

- (int)numberOfImageRepresentations
{
	// based on the isrc
	int nSuperReps = [super numberOfImageRepresentations];
	
	if (![self subImporters])
		return nSuperReps;
	
	// our own
	int nReps = 0;
	for (MGOMESubFileImporter *subImporter in subImporters_)
		nReps += [subImporter numberOfImageRepresentations];
	
	return MIN(nReps, nSuperReps);
	
}
 
@end
