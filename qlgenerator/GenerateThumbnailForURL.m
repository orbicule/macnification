#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>
#include <Cocoa/Cocoa.h>

/* -----------------------------------------------------------------------------
    Generate a thumbnail for file

   This function's job is to create thumbnail for designated file as fast as possible
   ----------------------------------------------------------------------------- */

OSStatus GenerateThumbnailForURL(void *thisInterface, QLThumbnailRequestRef thumbnail, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options, CGSize maxSize)
{
	NSURL *fileURL = (NSURL *)QLThumbnailRequestCopyURL(thumbnail);
	
	NSDictionary *dict = [[[NSDictionary alloc] initWithContentsOfURL:fileURL] autorelease];
	
	NSURL *imageURL = [NSURL fileURLWithPath:[[dict objectForKey:@"imagePath"] stringByExpandingTildeInPath]];
	
	NSLog([imageURL description]);
	//NSLog(@"------------------------------------------------------------------------------------------");
	CGImageSourceRef src = CGImageSourceCreateWithURL((CFURLRef)imageURL, NULL);
	
	CGImageRef image = CGImageSourceCreateImageAtIndex(src, 0, NULL);
	
	CGSize imageSize = CGSizeMake(CGImageGetWidth(image), CGImageGetHeight(image));
	
	//CGContextRef ctx = QLThumbnailRequestCreateContext(thumbnail, imageSize, YES, NULL);
	
	//CGRect destRect;
	//destRect.origin = CGPointZero;
	//destRect.size = imageSize;
	
	//CGContextDrawImage(ctx, destRect, image);
	
	//QLThumbnailRequestFlushContext(thumbnail, ctx);
	
	QLThumbnailRequestSetImage(thumbnail, image, NULL);
	
	//CGContextRelease(ctx);
	CGImageRelease(image);
	//CFRelease(src);
	
	return noErr;
}

void CancelThumbnailGeneration(void* thisInterface, QLThumbnailRequestRef thumbnail)
{
    // implement only if supported
}
