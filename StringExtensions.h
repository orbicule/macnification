//
//  StringExtensions.h
//  Macnification
//
//  Created by Peter Schols on 17/04/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSString (StringExtensions)


- (NSString *)uniquePath;
- (NSString *)uniquePathByUsingBasePath:(NSString *)base;

- (NSString *)stringByAppendingFilenameSuffix:(NSString *)suffix;

- (NSString *)compositePathExtension;

- (BOOL)beginsWith:(NSString *)startString;


@end
