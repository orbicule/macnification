//
//  MetadataController.m
//  Filament
//
//  Created by Dennis Lorson on 28/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MetadataController.h"

#import "MGMDView.h"
#import "MGMDFieldView.h"


@implementation MetadataController

- (void)awakeFromNib
{
	// if the metadata view exists, it means that this method is called when the Metadata.nib has been loaded.  We do not have to invoke the [self awake...] again.
	if (!metadataView)
		[self awakeWithSuperview:metadataViewContainer];
	
}

- (void)awakeWithSuperview:(NSView *)superview;
{
	// Load nib, using this object as the file's owner...
	
	if([NSBundle loadNibNamed:@"Metadata" owner:self])
	{
		// By the time the nib is loaded, view's awakeFromNib will have been called and m_view outlet set in this object
		// Add m_view to superview
		[m_view setAutoresizingMask:(NSViewHeightSizable | NSViewWidthSizable)];
		[m_view setFrame:[superview bounds]];
		[superview addSubview:m_view];
		
		[self setupMetadataView];
		// Do whatever other initialization you need here
	}
	else
	{
		// Nib loading failed - this object will be invalid, autorelease & raise exception
		[self autorelease];
		self = nil;
		[NSException raise:@"MyViewController-init-exception" format:@" - could not load nib from bundle"];
	}
	
}

- (void)bindToKeyPath:(NSString *)keyPath ofObject:(id)obj
{
	[metadataView bind:@"value" toObject:obj withKeyPath:keyPath options:nil];
}

- (void)setupMetadataView
{
	[self bindToKeyPath:@"selection" ofObject:imageController];
	metadataView.delegate = self;
	
	[[[metadataView enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[metadataView enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
}


- (MGMDView *)metadataView;
{
	return metadataView;
}


- (NSMenu *)metadataView:(MGMDView *)view menuForField:(MGMDFieldView *)field withIdentifier:(NSString *)identifier
{
	//NSLog(@"metadataview asks for menu");
	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	
	NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:10], NSFontAttributeName, nil];
	
	if ([field respondsToSelector:@selector(contentDescription)] && [[field contentDescription] length] > 0) {
		
		
		if ([field.fieldIdentifier isEqualToString:@"instrument.immersion"] ||
			[field.fieldIdentifier isEqualToString:@"instrument.instrumentName"] ||
			[field.fieldIdentifier isEqualToString:@"instrument.magnification"] ||
			[field.fieldIdentifier isEqualToString:@"instrument.numericAperture"] ||
			[field.fieldIdentifier isEqualToString:@"instrument.objectiveName"] ||
			[field.fieldIdentifier isEqualToString:@"instrument.spotSize"] ||
			[field.fieldIdentifier isEqualToString:@"instrument.voltage"]) {
			
			
			
			NSAttributedString *search = [[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Search Library for images with %@",[field contentDescription]] attributes:attrs] autorelease];
			NSMenuItem *searchItem = [[[NSMenuItem alloc] init] autorelease];
			[searchItem setAttributedTitle:search];
			[searchItem setAction:@selector(someAction:)];
			[searchItem setTarget:self];
			
			[menu addItem:searchItem];
			
			
		}
		

		
	}
	
	
	
	return menu;
}




@end
