//
//  LightTableMontagePreview.m
//  Filament
//
//  Created by Dennis Lorson on 21/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "LightTableCollagePreview.h"



@implementation LightTableCollagePreview

@synthesize image;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}


- (void)awakeFromNib
{
	[self resetSelectionRect];
	
	currentControlPoint = NoControlPoint;
}


- (void)setImage:(NSImage *)img
{
	if (image != img) {
		
		[image release];
		image = [img retain];
		
		[self resetSelectionRect];
		
		[self setNeedsDisplay:YES];
	}
	
}


- (void)setBackgroundColor:(NSColor *)color
{
	if (backgroundColor != color) {
		
		[backgroundColor release];
		backgroundColor = [color retain];
		
		[self setNeedsDisplay:YES];
	}
	
}

- (void)drawRect:(NSRect)rect 
{
    
	[[NSColor colorWithCalibratedWhite:0.5 alpha:1.0] set];
	NSRectFill([self bounds]);
	
	if (self.image) {
		
		NSBitmapImageRep *rep = [[image representations] objectAtIndex:0];
		CGFloat w = [rep pixelsWide];
		CGFloat h = [rep pixelsHigh];
		
		CGFloat availableWidth = [self bounds].size.width - 60;
		CGFloat availableHeight = [self bounds].size.height - 60;
		
		NSInteger wToUse, hToUse;
		
		if (w/availableWidth > h/availableHeight) {
			
			wToUse = availableWidth;
			hToUse = wToUse * h / w;
			
		} else {
			
			hToUse = availableHeight;
			wToUse = hToUse * w / h;
		}
		
		NSRect destRect;
		destRect.size.width = wToUse;
		destRect.size.height = hToUse;
		destRect.origin.x = NSMidX([self bounds]) - wToUse / 2;
		destRect.origin.y = NSMidY([self bounds]) - hToUse / 2;
		
		
		destRect = NSIntegralRect(destRect);
		
		imageRect = destRect;
		
		
		
		[NSGraphicsContext saveGraphicsState];
		
		NSShadow *shadow = [[[NSShadow alloc] init] autorelease];
		[shadow setShadowBlurRadius:8.0];
		[shadow setShadowColor:[[NSColor blackColor] colorWithAlphaComponent:0.6]];
		[shadow setShadowOffset:NSMakeSize(4, -4)];
		[shadow set];
		
		[[NSColor whiteColor] set];
		[[NSBezierPath bezierPathWithRect:destRect] fill];		
		
		[NSGraphicsContext restoreGraphicsState];

		[self drawBackgroundPatternInRect:destRect];
		
		[backgroundColor set];
		[[NSBezierPath bezierPathWithRect:destRect] fill];
		[self.image drawInRect:destRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
		
		
		// draw shade outside selection
		NSRect inner = [self convertRect:selectionRect toRect:destRect];
		
		inner = NSIntegralRect(inner);
		inner.origin.x += .5;
		inner.origin.y += .5;
		
		[self drawShadeInOuterRect:destRect innerRect:inner];

		[self drawHandlesInRect:inner];
		
	}
	
}


- (NSRect)convertRect:(NSRect)normalized toRect:(NSRect)dest
{
	NSRect newRect;
	newRect.origin.x = NSMinX(dest) + NSMinX(normalized) * NSWidth(dest);
	newRect.origin.y = NSMinY(dest) + NSMinY(normalized) * NSHeight(dest);
	newRect.size.width = NSWidth(normalized) * NSWidth(dest);
	newRect.size.height = NSHeight(normalized) * NSHeight(dest);
	
	
	return newRect;
	
}


- (NSRect)convertRect:(NSRect)original fromRect:(NSRect)src
{
	NSRect newRect;
	newRect.origin.x = (NSMinX(original) - NSMinX(src)) / NSWidth(src);
	newRect.origin.y = (NSMinY(original) - NSMinY(src)) / NSHeight(src);
	newRect.size.width = NSWidth(original) / NSWidth(src);
	newRect.size.height = NSHeight(original) / NSHeight(src);
	
	
	return newRect;
	
}

- (void)drawBackgroundPatternInRect:(NSRect)rect
{
	
	NSSize rectangleSize = NSMakeSize(5, 5);
	
	[NSGraphicsContext saveGraphicsState];
	
	[[NSBezierPath bezierPathWithRect:rect] addClip];
	[[NSColor lightGrayColor] set];
	int x, y;
	BOOL startRowWithFill = NO;
	BOOL fill = NO;
	for (y = NSMinY(rect); y < NSMaxY(rect); y += rectangleSize.width) {
		
		fill = startRowWithFill;
		startRowWithFill = !startRowWithFill;
		
		for (x = NSMinX(rect); x < NSMaxX(rect); x += rectangleSize.height) {
			if (fill) {
				
				NSBezierPath *path = [NSBezierPath bezierPathWithRect:NSMakeRect(x, y, rectangleSize.width, rectangleSize.height)];
				[path fill];
			}

			fill = !fill;
		}
		
	}
	[NSGraphicsContext restoreGraphicsState];

	
}

- (void)drawShadeInOuterRect:(NSRect)outer innerRect:(NSRect)inner
{
	
	[NSGraphicsContext saveGraphicsState];
	
	NSRect innerAdj = NSIntersectionRect(outer, inner);
	
	NSBezierPath *path = [NSBezierPath bezierPath];

	[path setWindingRule:NSEvenOddWindingRule];
	[path appendBezierPathWithRect:innerAdj];
	[path appendBezierPathWithRect:outer];	
	[path addClip];
	
	[[NSColor colorWithCalibratedWhite:0.0 alpha:0.7] set];
	
	[path fill];
	
	[NSGraphicsContext restoreGraphicsState];

	
	
}


- (void)drawHandlesInRect:(NSRect)rect
{
	[NSGraphicsContext saveGraphicsState];
	NSBezierPath *clip = [NSBezierPath bezierPathWithRect:NSInsetRect(rect, -20, -20)];
	[clip appendBezierPath:[self pathForControlPoint:LowerLeftControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:LowerRightControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:LowerMiddleControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:MiddleLeftControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:MiddleRightControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:UpperLeftControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:UpperMiddleControlPoint inRect:rect]];
	[clip appendBezierPath:[self pathForControlPoint:UpperRightControlPoint inRect:rect]];
	[clip setWindingRule:NSEvenOddWindingRule];
	
	[clip addClip];
	
	[[NSColor colorWithCalibratedWhite:1.0 alpha:1.0] set];
	
	NSBezierPath *path = [NSBezierPath bezierPathWithRect:rect];
	[path setLineWidth:1.0];
	[path stroke];
	
	const CGFloat dash[2] = {3, 3};
	
	NSBezierPath *verticalCenter = [NSBezierPath bezierPath];
	[verticalCenter moveToPoint:NSMakePoint(floorf(NSMidX(rect)) + .5, floorf(NSMinY(rect)) + .5)];
	[verticalCenter lineToPoint:NSMakePoint(floorf(NSMidX(rect)) + .5, floorf(NSMaxY(rect)) + .5)];
	[verticalCenter setLineWidth:1.0];
	[verticalCenter setLineDash:dash count:2 phase:0];
	[[NSColor colorWithCalibratedWhite:1.0 alpha:1.0] set];
	[verticalCenter stroke];
	
	[verticalCenter setLineDash:dash count:2 phase:dash[0]];
	[[NSColor colorWithCalibratedWhite:0.5 alpha:1.0] set];
	//[verticalCenter stroke];

	
	NSBezierPath *horizontalCenter = [NSBezierPath bezierPath];
	[horizontalCenter moveToPoint:NSMakePoint(floorf(NSMinX(rect)) + .5, floorf(NSMidY(rect)) + .5)];
	[horizontalCenter lineToPoint:NSMakePoint(floorf(NSMaxX(rect)) + .5, floorf(NSMidY(rect)) + .5)];
	[horizontalCenter setLineWidth:1.0];
	[[NSColor colorWithCalibratedWhite:1.0 alpha:1.0] set];
	[horizontalCenter setLineDash:dash count:2 phase:0];
	[horizontalCenter stroke];
	
	[[NSColor colorWithCalibratedWhite:0.5 alpha:1.0] set];
	[horizontalCenter setLineDash:dash count:2 phase:dash[0]];
	//[horizontalCenter stroke];
	
	[NSGraphicsContext restoreGraphicsState];

	
	[[NSColor whiteColor] set];
	[[self pathForControlPoint:LowerLeftControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:LowerRightControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:LowerMiddleControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:MiddleLeftControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:MiddleRightControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:UpperLeftControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:UpperMiddleControlPoint inRect:rect] stroke];
	[[self pathForControlPoint:UpperRightControlPoint inRect:rect] stroke];
	
	[[NSColor colorWithCalibratedWhite:1.0 alpha:0.5] set];
	[[self pathForControlPoint:LowerLeftControlPoint inRect:rect] fill];
	[[self pathForControlPoint:LowerRightControlPoint inRect:rect] fill];
	[[self pathForControlPoint:LowerMiddleControlPoint inRect:rect] fill];
	[[self pathForControlPoint:MiddleLeftControlPoint inRect:rect] fill];
	[[self pathForControlPoint:MiddleRightControlPoint inRect:rect] fill];
	[[self pathForControlPoint:UpperLeftControlPoint inRect:rect] fill];
	[[self pathForControlPoint:UpperMiddleControlPoint inRect:rect] fill];
	[[self pathForControlPoint:UpperRightControlPoint inRect:rect] fill];
}


- (void)resetSelectionRect
{
	if (!self.image) {
		
		selectionRect = NSZeroRect;
	
	} else {
		
		selectionRect = NSMakeRect(.3, .3, .4, .4);
		
	}
	
	
}


- (void)mouseDown:(NSEvent *)theEvent
{
	NSPoint loc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	currentControlPoint = [self controlPointAtPoint:loc inRect:[self convertRect:selectionRect toRect:imageRect]];
	if (NSPointInRect(loc, [self convertRect:selectionRect toRect:imageRect]))
		dragStartPoint = loc;
	else
		dragStartPoint = NSZeroPoint;
		
}


- (void)mouseDragged:(NSEvent *)theEvent
{	
	NSRect oldSelectionRect = [self convertRect:selectionRect toRect:imageRect];
	
	CGFloat oldX = oldSelectionRect.origin.x;
	CGFloat oldY = oldSelectionRect.origin.y;
	CGFloat oldWidth = oldSelectionRect.size.width;
	CGFloat oldHeight = oldSelectionRect.size.height;
	
	CGFloat deltaX = [theEvent deltaX];
	CGFloat deltaY = [theEvent deltaY];
	
	NSRect newFrame;
	
	switch (currentControlPoint) {
			
		case NoControlPoint:
			if (!NSEqualPoints(NSZeroPoint, dragStartPoint)) {
				
				newFrame = oldSelectionRect;
				newFrame.origin.x += deltaX;
				newFrame.origin.y -= deltaY;
				
				newFrame.origin.x = MIN(newFrame.origin.x, NSMaxX(imageRect) - NSWidth(newFrame));
				newFrame.origin.x = MAX(newFrame.origin.x, NSMinX(imageRect));
				
				newFrame.origin.y = MIN(newFrame.origin.y, NSMaxY(imageRect) - NSHeight(newFrame));
				newFrame.origin.y = MAX(newFrame.origin.y, NSMinY(imageRect));
				
				
			} else {
				
				newFrame = oldSelectionRect;
			}
			break;
		case UpperLeftControlPoint:
			newFrame = NSMakeRect(oldX + deltaX, oldY, oldWidth - deltaX, oldHeight - deltaY);
			break;
		case UpperMiddleControlPoint:
			newFrame = NSMakeRect(oldX, oldY, oldWidth, oldHeight - deltaY);
			break;
		case UpperRightControlPoint:
			newFrame = NSMakeRect(oldX, oldY, oldWidth + deltaX, oldHeight - deltaY);
			break;
		case MiddleRightControlPoint:
			newFrame = NSMakeRect(oldX, oldY, oldWidth + deltaX, oldHeight);
			break;
		case LowerRightControlPoint:
			newFrame = NSMakeRect(oldX, oldY - deltaY, oldWidth + deltaX, oldHeight + deltaY);
			break;
		case LowerMiddleControlPoint:
			newFrame = NSMakeRect(oldX, oldY - deltaY, oldWidth, oldHeight + deltaY);
			break;
		case LowerLeftControlPoint:
			newFrame = NSMakeRect(oldX + deltaX, oldY - deltaY, oldWidth - deltaX, oldHeight + deltaY);
			break;
		case MiddleLeftControlPoint:
			newFrame = NSMakeRect(oldX + deltaX, oldY, oldWidth - deltaX, oldHeight);
			break;
			
	}
	
	if (currentControlPoint != NoControlPoint)
		newFrame = NSIntersectionRect(newFrame, imageRect);
	
	if (NSWidth(newFrame) < 30) {
		
		newFrame.size.width = 30;
		newFrame.origin.x = MIN(newFrame.origin.x, NSMaxX(imageRect) - NSWidth(newFrame));
		
	}
	
	
	if (NSHeight(newFrame) < 30) {
		
		newFrame.origin.y = MIN(newFrame.origin.y, NSMaxY(imageRect) - 30);
		newFrame.size.height = 30;
		
	}
	
	selectionRect = [self convertRect:newFrame fromRect:imageRect];
	[self setNeedsDisplayInRect:NSInsetRect(newFrame, -10, -10)];
	[self setNeedsDisplayInRect:NSInsetRect(oldSelectionRect, -10, -10)];
	//[self setNeedsDisplay:YES];
}


- (void)mouseUp:(NSEvent *)theEvent
{
	currentControlPoint = NoControlPoint;
	
	
	
}


- (NSBezierPath *)pathForControlPoint:(ControlPoint)thePoint inRect:(NSRect)rect
{
	NSSize size = NSMakeSize(8.0, 8.0);
	NSBezierPath *path = [NSBezierPath bezierPath];
	NSPoint origin;
	switch (thePoint) {
			
		case LowerLeftControlPoint:
			origin = NSMakePoint(NSMinX(rect) - size.width / 2., NSMinY(rect) - size.height / 2.);
			break;
			
		case MiddleLeftControlPoint:
			origin = NSMakePoint(NSMinX(rect) - size.width / 2., NSMidY(rect) - size.height / 2.);
			break;
			
		case UpperLeftControlPoint:
			origin = NSMakePoint(NSMinX(rect) - size.width / 2., NSMaxY(rect) - size.height / 2.);
			break;
			
		case UpperMiddleControlPoint:
			origin = NSMakePoint(NSMidX(rect) - size.width / 2., NSMaxY(rect) - size.height / 2.);
			break;
			
		case UpperRightControlPoint:
			origin = NSMakePoint(NSMaxX(rect) - size.width / 2., NSMaxY(rect) - size.height / 2.);
			break;
			
		case MiddleRightControlPoint:
			origin = NSMakePoint(NSMaxX(rect) - size.width / 2., NSMidY(rect) - size.height / 2.);
			break;
			
		case LowerRightControlPoint:
			origin = NSMakePoint(NSMaxX(rect) - size.width / 2., NSMinY(rect) - size.height / 2.);
			break;
			
		case LowerMiddleControlPoint:
			origin = NSMakePoint(NSMidX(rect) - size.width / 2., NSMinY(rect) - size.height / 2.);
			break;
			
		case NoControlPoint:
			return path;
			
	}
	origin.x = floorf(origin.x) + .5;
	origin.y = floorf(origin.y) + .5;

	[path appendBezierPathWithRect:NSMakeRect(origin.x, origin.y, size.width, size.height)];
	return path;
}

- (ControlPoint)controlPointAtPoint:(NSPoint)point inRect:(NSRect)rect
{
	// used to determine the control point (if there is one) at the cursor position.
	
	if ([[self pathForControlPoint:LowerLeftControlPoint inRect:rect] containsPoint:point]) return LowerLeftControlPoint;
	if ([[self pathForControlPoint:MiddleLeftControlPoint inRect:rect] containsPoint:point]) return MiddleLeftControlPoint;
	if ([[self pathForControlPoint:UpperLeftControlPoint inRect:rect] containsPoint:point]) return UpperLeftControlPoint;
	if ([[self pathForControlPoint:UpperMiddleControlPoint inRect:rect] containsPoint:point]) return UpperMiddleControlPoint;
	if ([[self pathForControlPoint:UpperRightControlPoint inRect:rect] containsPoint:point]) return UpperRightControlPoint;
	if ([[self pathForControlPoint:MiddleRightControlPoint inRect:rect] containsPoint:point]) return MiddleRightControlPoint;
	if ([[self pathForControlPoint:LowerRightControlPoint inRect:rect] containsPoint:point]) return LowerRightControlPoint;
	if ([[self pathForControlPoint:LowerMiddleControlPoint inRect:rect] containsPoint:point]) return LowerMiddleControlPoint;
	
	return NoControlPoint;
}

- (NSImage *)selectedImage
{

	NSRect originalImageRect = NSMakeRect(0, 0, [self.image size].width, [self.image size].height);
	NSRect selectedArea = [self convertRect:selectionRect toRect:originalImageRect];
	selectedArea = NSIntegralRect(selectedArea);
	NSImage *destImg = [[[NSImage alloc] initWithSize:selectedArea.size] autorelease];
	
	[destImg lockFocus];
	
	[self.image drawInRect:NSMakeRect(0, 0, selectedArea.size.width, selectedArea.size.height) fromRect:selectedArea operation:NSCompositeSourceOver fraction:1.0];
	
	[destImg unlockFocus];
	
	return destImg;
}

- (void)releaseResources;
{
	self.image = nil;
}


- (void) dealloc
{
	self.image = nil;
	[self setBackgroundColor:nil];
	
	[super dealloc];
}


@end
