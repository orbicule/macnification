//
//  MGContextualToolSeparator.m
//  StackTable
//
//  Created by Dennis Lorson on 21/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGContextualToolGenericView.h"


@implementation MGContextualToolGenericView

@synthesize toolType;


- (void)drawRect:(NSRect)frame
{
	// just draw either nothing or a vertical separator.
	
	if (toolType == MGSeparatorToolType) {
		
		NSBezierPath *darkLine = [NSBezierPath bezierPath];
		[darkLine moveToPoint:NSMakePoint([self bounds].size.width/2, 0)];
		[darkLine lineToPoint:NSMakePoint([self bounds].size.width/2, [self bounds].size.height)];
		
		NSBezierPath *lightLine = [NSBezierPath bezierPath];
		[lightLine moveToPoint:NSMakePoint([self bounds].size.width/2 + 1, 0)];
		[lightLine lineToPoint:NSMakePoint([self bounds].size.width/2 + 1, [self bounds].size.height)];
		
		[[NSGraphicsContext currentContext] setShouldAntialias:NO];
		
		[[NSColor colorWithCalibratedWhite:105.0/255 alpha:0.8] set];
		[darkLine stroke];
		
		[[NSColor colorWithCalibratedWhite:205.0/255 alpha:0.8] set];
		[lightLine stroke];
		
		[[NSGraphicsContext currentContext] setShouldAntialias:YES];
		
	}
}
	

@end
