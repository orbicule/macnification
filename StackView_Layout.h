//
//  StackView (Layout)
//
//	Layout methods responsible for determining positions of the image layers within the stack layer.
//
//  Created by Dennis Lorson on 19/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

#import "StackView.h"

@class StackImageLayer;

@interface StackView (Layout)

- (void)setAnimatesLayout:(BOOL)flag;
- (void)setNeedsLayout;

- (NSArray *)arrangedStackImagelayers;
- (void)rearrangeStackImageLayers;

- (CGSize)maximumImageLayerSizeInStackLayer:(StackLayer *)layer;

- (CGSize)sizeOfImageLayer:(StackImageMainLayer *)imageLayer inLayer:(CALayer *)layer;
- (CGSize)placeholderLayerSizeInLayer:(CALayer *)layer;


@end
