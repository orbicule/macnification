//
//  MGLibraryController.h
//  Filament
//
//  Created by Dennis Lorson on 05/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MGLibrary, SourceListTreeController, FullScreenController, ImageArrayController, 
ScaleBarController, MiniBrowserController, KeywordController, ExperimentsController,
MGLibraryController, RBSplitView;


@interface MGLibraryController : NSObject <NSWindowDelegate>
{
    MGLibrary *library_;
    
	
	ScaleBarController *scaleBarController;
	MiniBrowserController *miniBrowserController;
	KeywordController *keywordController;
	ExperimentsController *experimentsController;

	IBOutlet FullScreenController *fullScreenController;
	
	IBOutlet NSWindow *window;
	IBOutlet NSArrayController *roiArrayController;
	IBOutlet SourceListTreeController *sourceListTreeController;
	IBOutlet ImageArrayController *imageArrayController;
	IBOutlet NSCollectionView *gridView;
	IBOutlet NSPanel *keywordsPanel;
	IBOutlet NSWindow *roiPanel;
    
    RBSplitView *splitView;
    
	IBOutlet NSOutlineView *sourceList;
    IBOutlet NSView *mdContainerView;
	IBOutlet NSTabView *tabView;
	
	// embedding views (for LT, Stack, MD: they are embedded in their own nib hierarchy, so don't use the embedded indirection here)
	IBOutlet NSView *projectEmbeddedView;
	IBOutlet NSView *browserEmbeddedView;
	IBOutlet NSView *sourceListEmbeddedView;
	
	// Alerts
	IBOutlet NSProgressIndicator *progressBar;
	IBOutlet NSWindow *progressSheet;
	IBOutlet NSTextField *progressTextField, *progressTitleTextField;
	IBOutlet NSButton *cancelImportButton;
	
	// Plugins
	NSMutableDictionary *analysisPlugins;

}

- (id)initWithLibrary:(MGLibrary *)library;
- (MGLibrary *)library;
- (void)loadUI;


- (void)openFiles:(NSArray *)files;

- (void)prepareForTermination;

// get the main window
- (NSWindow *)window;

// Show windows and panels
- (void)showMainWindow;
- (IBAction)showExperimentsWindow:(id)sender;
- (IBAction)toggleKeywordPanel:(id)sender;
- (IBAction)showScaleBarPanel:(id)sender;
- (IBAction)showMiniBrowser:(id)sender;
- (IBAction)toggleROIPanel:(id)sender;

- (IBAction)showRegistrationPanel:(id)sender;

// Toggle the main splitview
- (IBAction)toggleMdSplitView:(id)sender;



// Progress Sheet
- (void)showProgressSheet;
- (void)setProgress:(double)progress;
- (void)setProgressMessage:(NSString *)message;
- (void)setProgressTitle:(NSString *)title;
- (void)setCancelImportButtonHidden:(BOOL)flag;
- (void)startProgressAnimation;
- (void)hideProgressSheet;

// Accessors
- (ImageArrayController *)imageArrayController;
- (SourceListTreeController *)sourcelistTreeController;


- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;

// Plugins
- (IBAction)launchPlugin:(id)sender;
- (void)discoverPlugins;
- (void)setupPluginMenu;


- (void)exportSpotlightMetadata;
- (void)refreshAndReloadImageBrowser;
- (void)save;


- (NSManagedObjectContext *)managedObjectContext;
- (IBAction)saveAction:sender;

- (void)updateImagesModifiedByExternalEditor;


@end
