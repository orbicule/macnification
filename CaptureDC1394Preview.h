//
//  CaptureDC1394Preview.h
//  Filament
//
//  Created by Dennis Lorson on 23/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "CaptureOpenGLPreview.h"
#import <dc1394/dc1394.h>

@interface CaptureDC1394Preview : CaptureOpenGLPreview
{

}


@end
