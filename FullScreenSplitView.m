//
//  FullScreenSplitView.m
//  Filament
//
//  Created by Dennis Lorson on 24/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "FullScreenSplitView.h"


@implementation FullScreenSplitView


- (NSColor *)dividerColor;
{
	return [NSColor colorWithCalibratedWhite:33./255. alpha:1.0];
	
}


@end
