//
//  MGImageFileImporter.h
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MGMetadataSet;


@interface MGImageFileImporter : NSObject 
{
	NSString *filePath_;
	
	MGMetadataSet *metadata_;
	
}


+ (MGImageFileImporter *)fileImporterWithPath:(NSString *)filePath;
+ (NSArray *)supportedPathExtensions;
+ (BOOL)supportsExtension:(NSString *)extension;

// some image files contain multiple image sets; this is handled with the subimporters.
// if an image file has only one image set, this method returns nil.
- (NSArray *)subImporters;
- (MGImageFileImporter *)superImporter;


// the file path that was supplied to the importer
- (NSString *)originalFilePath;
// the file path where to take the representations from (may be the same as "originalFilePath")
- (NSString *)representationFilePath;


- (NSString *)destinationName;
- (NSString *)destinationPathExtension;
- (NSString *)compressedDestinationPathExtension;

- (BOOL)destinationNeedsCompression;

- (MGMetadataSet *)metadata;

- (int)numberOfImageRepresentations;
- (NSBitmapImageRep *)imageRepresentationAtIndex:(int)index;


- (float)fractionOfImportTimeForNonRepresentationWork;



@end
