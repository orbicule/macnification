//
//  MGFusionFilters.m
//  Filament
//
//  Created by Dennis Lorson on 09/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGFusionFilters.h"


static CIKernel *PadKernel = nil;
static CIKernel *KRGBKernel = nil;
static CIKernel *ConvolveDownHKernel = nil;
static CIKernel *ConvolveDownVKernel = nil;
static CIKernel *ConvolveUpHKernel = nil;
static CIKernel *ConvolveUpVKernel = nil;
static CIKernel *SMLHKernel = nil;
static CIKernel *SMLVKernel = nil;
static CIKernel *SmoothHKernel = nil;
static CIKernel *SmoothVKernel = nil;
static CIKernel *SubsampleKernel = nil;
static CIKernel *SupersampleKernel = nil;
static CIKernel *DecimateKernel = nil;
static CIKernel *LaplacianKernel = nil;
static CIKernel *AddKernel = nil;
static CIKernel *AddAlphaKernel = nil;
static CIKernel *SubtractKernel = nil;
static CIKernel *MergeKernel = nil;
static CIKernel *NormalizeAlphaKernel = nil;
static CIKernel *RedToGrayscaleKernel = nil;


#pragma mark Shared


@implementation SharedFilterMethods

+ (void)loadKernelsIfNeeded
{
	if(PadKernel == nil) {
		
		NSBundle    *bundle = [NSBundle bundleForClass:[self class]];
        NSString    *code = [NSString stringWithContentsOfFile:[bundle pathForResource:@"imagefusion" ofType: @"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
        NSArray     *kernels = [CIKernel kernelsWithString:code];
		
		int i = 0;
		
        PadKernel =					[[kernels objectAtIndex:i++] retain];
		KRGBKernel =				[[kernels objectAtIndex:i++] retain];
		ConvolveDownHKernel =		[[kernels objectAtIndex:i++] retain];
		ConvolveDownVKernel =		[[kernels objectAtIndex:i++] retain];
		ConvolveUpHKernel =			[[kernels objectAtIndex:i++] retain];
		ConvolveUpVKernel =			[[kernels objectAtIndex:i++] retain];
		SMLHKernel = 				[[kernels objectAtIndex:i++] retain];
		SMLVKernel =				[[kernels objectAtIndex:i++] retain];
		SmoothHKernel = 			[[kernels objectAtIndex:i++] retain];
		SmoothVKernel = 			[[kernels objectAtIndex:i++] retain];
		SubsampleKernel =			[[kernels objectAtIndex:i++] retain];
		SupersampleKernel =			[[kernels objectAtIndex:i++] retain];
		DecimateKernel =			[[kernels objectAtIndex:i++] retain];
		LaplacianKernel =			[[kernels objectAtIndex:i++] retain];
		AddKernel =					[[kernels objectAtIndex:i++] retain];
		AddAlphaKernel =			[[kernels objectAtIndex:i++] retain];
		SubtractKernel = 			[[kernels objectAtIndex:i++] retain];
		MergeKernel =				[[kernels objectAtIndex:i++] retain];
		NormalizeAlphaKernel =		[[kernels objectAtIndex:i++] retain];
		RedToGrayscaleKernel =		[[kernels objectAtIndex:i++] retain];

	}
}

+ (NSArray *)extentOptionWithRect:(CGRect)rect
{
	return [NSArray arrayWithObjects:
			[NSNumber numberWithFloat:rect.origin.x],
			[NSNumber numberWithFloat:rect.origin.y],
			[NSNumber numberWithFloat:rect.size.width],
			[NSNumber numberWithFloat:rect.size.height],
			nil];
}

@end


@implementation MGFusionFilter


+ (void)initialize
{
	NSString *filterName = NSStringFromClass([self class]);
	
    [CIFilter registerFilterName:filterName constructor:(id<CIFilterConstructor>)self classAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
																						 filterName, kCIAttributeFilterDisplayName,
																						 [NSArray arrayWithObjects:
																						  kCICategoryColorAdjustment, nil], kCIAttributeFilterCategories,
																						 nil]
	 ];
}

- (id)init
{
    [SharedFilterMethods loadKernelsIfNeeded];
    return [super init];
}


@end


#pragma mark -
#pragma mark -
#pragma mark Padding

@implementation MGFusionPOTFilter


- (CIImage *)outputImage
{
	CGSize paddedSize = [self paddedSizeForImage:inputImage];
	
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	CGRect extent = [inputImage extent];
	extent.size = paddedSize;
	
	CIImage *result = [self apply:PadKernel, sampler, kCIApplyOptionExtent, [NSArray arrayWithObjects:
																					[NSNumber numberWithFloat:extent.origin.x],
																					[NSNumber numberWithFloat:extent.origin.y],
																					[NSNumber numberWithFloat:extent.size.width],
																					[NSNumber numberWithFloat:extent.size.height], nil],
					   nil];
	
	//NSLog(@"input: %fx%f  -- output: %fx%f", [inputImage extent].size.width, [inputImage extent].size.height, paddedSize.width, paddedSize.height);
	
	return result;
}

- (CGSize)paddedSizeForImage:(CIImage *)img
{
	CGSize imageSize = [img extent].size;
	
	int minimumSize = 8;
	int nLevels = 0;
	float currentPixelsX = imageSize.width;
	float currentPixelsY = imageSize.height;
	float minCurrentPixels = MIN(currentPixelsX, currentPixelsY);
	
	while (minCurrentPixels > minimumSize + 1e-5) {
		minCurrentPixels /= 2;
		currentPixelsX /= 2;
		currentPixelsY /= 2;
		nLevels++;
	}
	
	int newPixelsX = (int)ceilf(currentPixelsX) << nLevels;
	int newPixelsY = (int)ceilf(currentPixelsY) << nLevels;
	
	return CGSizeMake(newPixelsX, newPixelsY);
}


@end



@implementation MGFusionPadFilter


- (NSDictionary *)customAttributes
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
			[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:  3.00], kCIAttributeDefault, kCIAttributeTypeScalar, kCIAttributeType, nil], 
			@"inputPadAmount",
			nil];
}

- (CIImage *)outputImage
{
	
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	CGRect extent = CGRectIntegral([inputImage extent]);
	extent.origin.x -= [inputPadAmount integerValue];
	extent.origin.y -= [inputPadAmount integerValue];
	extent.size.width += 2. * [inputPadAmount integerValue];
	extent.size.height += 2. * [inputPadAmount integerValue];
	
	CIImage *result = [self apply:PadKernel, sampler, kCIApplyOptionDefinition, [SharedFilterMethods extentOptionWithRect:extent], nil];
	
	return result;
}


@end


#pragma mark -
#pragma mark -
#pragma mark Conversion

@implementation MGFusionKRGBFilter


- (CIImage *)outputImage
{
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	return [self apply:KRGBKernel, sampler, kCIApplyOptionDefinition, [sampler definition], nil];
}


@end

@implementation MGFusionNormalizeAlphaFilter


- (CIImage *)outputImage
{
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	return [self apply:NormalizeAlphaKernel, sampler, kCIApplyOptionDefinition, [sampler definition], nil];
}


@end

@implementation MGFusionRedToGrayscaleFilter


- (CIImage *)outputImage
{
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	return [self apply:RedToGrayscaleKernel, sampler, kCIApplyOptionDefinition, [sampler definition], nil];
}


@end

#pragma mark -
#pragma mark -
#pragma mark LP/HP


@implementation MGFusionConvolveUpFilter


- (CGRect)regionOf:(int)sampler destRect:(CGRect)r userInfo:(NSNumber *)horizontal
{
	CGRect copy = r;
	if ([horizontal boolValue]) {
		copy.origin.x -= 3;
		copy.size.width += 6;
	}
	else {
		copy.origin.y -= 3;
		copy.size.height += 6;
	}
	
	return copy;
}

- (CIImage *)outputImage
{
	[ConvolveUpHKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	[ConvolveUpVKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	

	CGRect extent = [inputImage extent];
	extent = CGRectInset(extent, 3, 0);
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CIImage *temp = [self apply:ConvolveUpHKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:YES], nil];
	
	extent = [temp extent];
	extent = CGRectInset(extent, 0, 3);
	sampler = [CISampler samplerWithImage:temp options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	temp = [self apply:ConvolveUpVKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:NO], nil];
	
	return temp;
}


@end


@implementation MGFusionConvolveDownFilter


- (CGRect)regionOf:(int)sampler destRect:(CGRect)r userInfo:(NSNumber *)horizontal
{
	CGRect copy = r;
	if ([horizontal boolValue]) {
		copy.origin.x -= 4;
		copy.size.width += 8;
	}
	else {
		copy.origin.y -= 4;
		copy.size.height += 8 ;
	}
	
	return copy;
}

- (CIImage *)outputImage
{
	[ConvolveDownHKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	[ConvolveDownVKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	
	
	CGRect extent = [inputImage extent];
	extent = CGRectInset(extent, 4, 0);
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CIImage *temp = [self apply:ConvolveDownHKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:YES], nil];
	
	extent = [temp extent];
	extent = CGRectInset(extent, 0, 4);
	sampler = [CISampler samplerWithImage:temp options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	temp = [self apply:ConvolveDownVKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:NO], nil];
	
	return temp;
}


@end

#pragma mark -
#pragma mark -
#pragma mark SML/Smooth


@implementation MGFusionSMLFilter

- (CGRect)regionOf:(int)sampler destRect:(CGRect)r userInfo:(NSNumber *)horizontal
{
	CGRect copy = r;
	if ([horizontal boolValue]) {
		copy.origin.x -= 1;
		copy.size.width += 2;
	}
	else {
		copy.origin.y -= 1;
		copy.size.height += 2 ;
	}
	
	return copy;
}

- (CIImage *)outputImage
{
	[SMLHKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	[SMLVKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
		
	CGRect extent = [inputImage extent];
	extent = CGRectInset(extent, 1, 1);
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CIImage *h = [self apply:SMLHKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:YES], nil];
	
	extent = [inputImage extent];
	extent = CGRectInset(extent, 1, 1);
	sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CIImage *v = [self apply:SMLVKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:NO], nil];
	
	// add h and v (absolute value is already done by the kernel)
	CISampler *sampler1 = [CISampler samplerWithImage:h options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];	
	CISampler *sampler2 = [CISampler samplerWithImage:v options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	extent = [h extent];
	CIImage *result = [self apply:AddAlphaKernel, sampler1, sampler2, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:NO], nil];
	
	return result;
}


@end




@implementation MGFusionSmoothFilter

- (CGRect)regionOf:(int)sampler destRect:(CGRect)r userInfo:(NSNumber *)horizontal
{
	CGRect copy = r;
	if ([horizontal boolValue]) {
		copy.origin.x -= 1;
		copy.size.width += 2;
	}
	else {
		copy.origin.y -= 1;
		copy.size.height += 2 ;
	}
	
	return copy;
}

- (CIImage *)outputImage
{
	[SmoothHKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	[SmoothVKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	
	
	CGRect extent = [inputImage extent];
	extent = CGRectInset(extent, 1, 0);
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CIImage *temp = [self apply:SmoothHKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:YES], nil];
	
	extent = [temp extent];
	extent = CGRectInset(extent, 0, 1);
	sampler = [CISampler samplerWithImage:temp options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	temp = [self apply:SmoothVKernel, sampler, kCIApplyOptionExtent, [SharedFilterMethods extentOptionWithRect:extent], kCIApplyOptionUserInfo, [NSNumber numberWithBool:NO], nil];
	
	return temp;
}


@end


#pragma mark -
#pragma mark -
#pragma mark Simple ops


@implementation MGFusionDecimateFilter

- (CIImage *)outputImage
{
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	return [self apply:DecimateKernel, sampler, kCIApplyOptionDefinition, [sampler definition], nil];
}

@end


@implementation MGFusionAddFilter

- (CIImage *)outputImage
{
	CISampler *sampler1 = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CISampler *sampler2 = [CISampler samplerWithImage:inputImage2 options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];

	return [self apply:AddKernel, sampler1, sampler2, kCIApplyOptionDefinition, [sampler1 definition], nil];
}

@end



@implementation MGFusionSubtractFilter

- (CIImage *)outputImage
{
	CISampler *sampler1 = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CISampler *sampler2 = [CISampler samplerWithImage:inputImage2 options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	return [self apply:SubtractKernel, sampler1, sampler2, kCIApplyOptionDefinition, [sampler1 definition], nil];
}

@end


@implementation MGFusionMergeFilter

- (CIImage *)outputImage
{
	CISampler *sampler1 = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	CISampler *sampler2 = [CISampler samplerWithImage:inputImage2 options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	return [self apply:MergeKernel, sampler1, sampler2, kCIApplyOptionDefinition, [sampler1 definition], nil];
}

@end


@implementation MGFusionSubsampleFilter

- (CGRect)regionOf:(int)sampler destRect:(CGRect)r userInfo:userInfo
{
	CGRect d = r;
	d.origin.x *= 2.0;
	d.origin.y *= 2.0;
	d.size.width *= 2.0;
	d.size.height *= 2.0;

	return d;
}

- (CIImage *)outputImage
{
	[SubsampleKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	CGAffineTransform trafo = CGAffineTransformMakeScale(0.5, 0.5);
	
	return [self apply:SubsampleKernel, sampler, kCIApplyOptionDefinition, [[sampler definition] transformBy:trafo interior:YES], nil];
}

@end


@implementation MGFusionSupersampleFilter

- (CGRect)regionOf:(int)sampler destRect:(CGRect)r userInfo:userInfo
{
	CGRect d = r;
	d.origin.x *= 0.5;
	d.origin.y *= 0.5;
	d.size.width *= 0.5;
	d.size.height *= 0.5;
	
	return d;
}

- (CIImage *)outputImage
{
	[SupersampleKernel setROISelector:@selector(regionOf:destRect:userInfo:)];
	
	CISampler *sampler = [CISampler samplerWithImage:inputImage options:[NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil]];
	
	CGAffineTransform trafo = CGAffineTransformMakeScale(2.0, 2.0);
	
	return [self apply:SupersampleKernel, sampler, kCIApplyOptionDefinition, [[sampler definition] transformBy:trafo interior:YES], nil];
}

@end

