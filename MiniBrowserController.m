//
//  MiniBrowserController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 12 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import "MiniBrowserController.h"
#import "MGAppDelegate.h"
#import "SourceListTreeController.h"
#import "MGSourceOutlineView.h"
#import "MiniSourceListTreeController.h"
#import "ImageArrayController.h"
#import "MGLibraryController.h"

@implementation MiniBrowserController



- (id)init;
{
	if ((self = [super initWithWindowNibName:@"MiniBrowser"])) {
		return self;
	}
	return nil;
}


- (void)awakeFromNib;
{

	
	[imageBrowser setValue:[NSColor colorWithCalibratedRed:0.18 green:0.18 blue:0.18 alpha:1] forKey:IKImageBrowserBackgroundColorKey];
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Lucida Grande" size:9], NSFontAttributeName, [NSColor whiteColor], @"NSColor", nil] forKey:IKImageBrowserCellsTitleAttributesKey];
	[imageBrowser setValue:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Lucida Grande" size:9], NSFontAttributeName, [NSColor whiteColor], @"NSColor", nil] forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];
	
	[imageBrowser setAnimates:NO];
	
	ImageArrayController *iactl = [MGLibraryControllerInstance imageArrayController];
	[imageBrowser setDataSource:iactl];
	[imageBrowser setDelegate:iactl];
	
	[iactl addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld context:nil];
	
	[imageBrowser reloadData];
	
	[[[imageBrowser enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[imageBrowser enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];

	[[[sourceList enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[sourceList enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
	
	[splitView setDividerStyle:NSSplitViewDividerStyleThin];
	
	sourceListWidth = [(NSView *)[[splitView subviews] objectAtIndex:0] frame].size.width;
	
	
	[imageBrowser setCellsStyleMask:IKCellsStyleTitled];
	
	[self performSelector:@selector(expandAll) withObject:nil afterDelay:0.2];
	
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"arrangedObjects"]) {
		
		[imageBrowser reloadData];
		
	} else {
		
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (void)expandAll
{
	[sourceList expandItem:nil expandChildren:YES];
}


- (void)windowWillClose:(NSNotification *)notif;
{
	[MGLibraryControllerInstance showMainWindow];
}


- (void)windowDidResize:(NSNotification *)aNotification;
{
		[splitView setPosition:sourceListWidth ofDividerAtIndex:0];
}



- (BOOL)windowShouldZoom:(NSWindow *)window toFrame:(NSRect)proposedFrame
{
	[window close];
	[MGLibraryControllerInstance showMainWindow];
	return NO;
}

//  Delegate method called before resizing the mdSplitView. We use it to save the position of the sourceListSplitView
//	so that we can set that position back in the splitViewDidResizeSubviews method. That way, the source list retains its width.
- (void)splitViewWillResizeSubviews:(NSNotification *)aNotification;
{
	if ([aNotification object] == splitView) {
		sourceListWidth = [(NSView *)[[splitView subviews] objectAtIndex:0] frame].size.width;
	}
}


@end
