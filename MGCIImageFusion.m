//
//  MGCIImageFusion.m
//  Filament
//
//  Created by Dennis Lorson on 09/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGCIImageFusion.h"
#import "MGFusionFilters.h"

@interface MGCIImageFusion ()

- (CIImage *)preprocessImage:(CIImage *)image;

- (NSArray *)constructLPTWithImage:(CIImage *)image nLevels:(size_t)nLevels;
- (void)performLPTConstructionStepOnImage:(CIImage *)image highpassResult:(CIImage **)hp lowpassResult:(CIImage **)lp;

- (CIImage *)collapseLPT:(NSArray *)lpt;
- (CIImage *)performLPTCollapseStepWithHighpass:(CIImage *)hp lowpass:(CIImage *)lp;

- (NSArray *)computeSMLForLPT:(NSArray *)lpt;
- (NSArray *)mergeLPT:(NSArray *)one withLPT:(NSArray *)two;

- (CIImage *)lowpassFilter:(CIImage *)image;
- (CIImage *)highpassFilter:(CIImage *)image;
- (CIImage *)padForHP:(CIImage *)image;
- (CIImage *)padForLP:(CIImage *)image;
- (CIImage *)padFor3x3Kernel:(CIImage *)image;
- (CIImage *)subtract:(CIImage *)toBeSubtracted from:(CIImage *)original;
- (CIImage *)add:(CIImage *)one and:(CIImage *)two;
- (CIImage *)decimate:(CIImage *)image;
- (CIImage *)subsample:(CIImage *)image;
- (CIImage *)supersample:(CIImage *)image;
- (CIImage *)computeSML:(CIImage *)image;
- (CIImage *)smooth:(CIImage *)image;
- (CIImage *)merge:(CIImage *)one with:(CIImage *)two;
- (CIImage *)cropAndTranslate:(CIImage *)image toRect:(CGRect)rect;
- (CIImage *)crop:(CIImage *)image toSize:(CGSize)size;
- (CIImage *)composite:(CIImage *)background behind:(CIImage *)foreground;

- (void)saveImage:(CIImage *)img withName:(NSString *)name;
- (void)saveImageColor:(CIImage *)img withName:(NSString *)name;



@end

#pragma mark -

@implementation MGCIImageFusion



- (id)initWithImage:(CIImage *)img referenceImage:(CIImage *)refImg;
{
	if ((self = [super init])) {
		image_ = [img retain];
		refImage_ = [refImg retain];
	}
	
	return self;

}

- (void) dealloc
{
	[image_ release];
	[refImage_ release];
	[super dealloc];
}


static int LPTNumber = 0;
static BOOL saveImages = NO;

- (CIImage *)fusionImage;
{
	saveImages = NO;
	
	CIImage *img1 = image_;
	CIImage *img2 = refImage_;
	
	// crop to the refImage rect,
	// and translate so the origin is at (0, 0) (necessary because some kernels (decimate!) work relative to the sampler extent origin)
	// then composite the ref image behind the other one to avoid black blocks
	CGRect rect = CGRectIntegral(CGRectInset([img2 extent], 1.0, 1.0));
	img1 = [self cropAndTranslate:img1 toRect:rect];
	img2 = [self cropAndTranslate:img2 toRect:rect];
	
	img1 = [self composite:img2 behind:img1];
	
	
	//NSLog(@"image 1 extent: %@", NSStringFromRect(NSRectFromCGRect([img1 extent])));
	//NSLog(@"image 2 extent: %@", NSStringFromRect(NSRectFromCGRect([img2 extent])));
	
	img1 = [self preprocessImage:img1];
	img2 = [self preprocessImage:img2];
	
	
	NSArray *lpt1 = [self constructLPTWithImage:img1 nLevels:6];
	NSArray *lpt2 = [self constructLPTWithImage:img2 nLevels:6];
	
	LPTNumber = 0;
	lpt1 = [self computeSMLForLPT:lpt1];
	LPTNumber = 1;
	lpt2 = [self computeSMLForLPT:lpt2];
	
	NSArray *lptMerged = [self mergeLPT:lpt1 withLPT:lpt2];
	
	CIImage *result = [self collapseLPT:lptMerged];
	
	result = [self crop:result toSize:rect.size];
	
	CIFilter *normalize = [CIFilter filterWithName:@"MGFusionNormalizeAlphaFilter"];
	[normalize setValue:result forKey:@"inputImage"];
	result = [normalize valueForKey:@"outputImage"];

	//NSLog(@"Fusion: %f secs", [[NSDate date] timeIntervalSinceDate:date]);
	
	return result;
}

- (CIImage *)preprocessImage:(CIImage *)image
{
	CIImage *temp = image;
	
	[self saveImage:temp withName:@"pre_in"];
	
	// set the grayscale channel
	CIFilter *krgbFilter = [CIFilter filterWithName:@"MGFusionKRGBFilter"];
	[krgbFilter setValue:temp forKey:@"inputImage"];
	temp = [krgbFilter valueForKey:@"outputImage"];
	
	[self saveImage:temp withName:@"pre_krgb"];


	// pad up to near-powers of two
	CIFilter *POTFilter = [CIFilter filterWithName:@"MGFusionPOTFilter"];
	[POTFilter setValue:temp forKey:@"inputImage"];
	temp = [POTFilter valueForKey:@"outputImage"];
	
	[self saveImage:temp withName:@"pre_pot"];


	return temp;
	
}


#pragma mark -
#pragma mark LPT construction

static int LPTIndex = 0;

- (NSArray *)constructLPTWithImage:(CIImage *)image nLevels:(size_t)nLevels
{	
	CIImage *currentLevelImage = image;
	
	NSMutableArray *levels = [NSMutableArray array];
	
	for (int j = 0; j < nLevels - 1; j++) {
		
		LPTIndex = j;
		
		CIImage *hp = nil;
		CIImage *lp = nil;
		[self performLPTConstructionStepOnImage:currentLevelImage highpassResult:&hp lowpassResult:&lp];
		
		currentLevelImage = lp;
		
		[levels addObject:hp];
		
	}
	
	[levels addObject:currentLevelImage];
	
	return levels;
}

- (void)performLPTConstructionStepOnImage:(CIImage *)image highpassResult:(CIImage **)hp lowpassResult:(CIImage **)lp
{
	CIImage *temp = image;
	CIImage *lowpass, *highpass;

	[self saveImage:temp withName:[NSString stringWithFormat:@"LPTC-input-%i", LPTIndex]];

	// down
	temp = [self padForLP:temp];
	[self saveImage:temp withName:[NSString stringWithFormat:@"LPTC-padded1-%i", LPTIndex]];
	temp = [self lowpassFilter:temp];
	[self saveImage:temp withName:[NSString stringWithFormat:@"LPTC-downfiltered-%i", LPTIndex]];

	// create lowpass
	lowpass = [self subsample:temp];
	[self saveImage:lowpass withName:[NSString stringWithFormat:@"LPTC-lowpass-%i", LPTIndex]];

	
	// decimate
	temp = [self decimate:temp];
	[self saveImage:temp withName:[NSString stringWithFormat:@"LPTC-decimated-%i", LPTIndex]];

	// up
	temp = [self padForHP:temp];
	[self saveImage:temp withName:[NSString stringWithFormat:@"LPTC-padded2-%i", LPTIndex]];
	temp = [self highpassFilter:temp];
	[self saveImage:temp withName:[NSString stringWithFormat:@"LPTC-upfiltered-%i", LPTIndex]];

	// laplacian
	highpass = [self subtract:temp from:image];
	[self saveImage:highpass withName:[NSString stringWithFormat:@"LPTC-laplacian-%i", LPTIndex]];

	
	*lp = lowpass;
	*hp = highpass;
	
	[self saveImage:highpass withName:[NSString stringWithFormat:@"LPT-H-%i", LPTIndex]];
	[self saveImage:lowpass withName:[NSString stringWithFormat:@"LPT-L-%i", LPTIndex]];

	
}


#pragma mark -
#pragma mark LPT collapse

- (CIImage *)collapseLPT:(NSArray *)lpt
{
	CIImage *lowpass = [lpt lastObject];
	
	for (int i = [lpt count] - 2; i >= 0 ; i--) {
		
		LPTIndex = i;
		
		CIImage *highpass = [lpt objectAtIndex:i];
		CIImage *reconstructed = [self performLPTCollapseStepWithHighpass:highpass lowpass:lowpass];
		
		lowpass = reconstructed;
	}
	
	return lowpass;
}


- (CIImage *)performLPTCollapseStepWithHighpass:(CIImage *)hp lowpass:(CIImage *)lp
{
	[self saveImageColor:lp withName:[NSString stringWithFormat:@"LPTD-inputLP-%i", LPTIndex]];
	
	CIImage *lp_supersampled = [self supersample:lp];

	[self saveImageColor:lp_supersampled withName:[NSString stringWithFormat:@"LPTD-supersampled-%i", LPTIndex]];

	// ------------------------------------	
	
	CIImage *temp = hp;
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-inputHP-%i", LPTIndex]];
	
	temp = [self padForLP:temp];
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-padded1-%i", LPTIndex]];
	
	temp = [self lowpassFilter:temp];
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-lp-%i", LPTIndex]];

	temp = [self subtract:temp from:lp_supersampled];
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-subtracted-%i", LPTIndex]];
	
	temp = [self decimate:temp];
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-decimated-%i", LPTIndex]];
	
	temp = [self padForHP:temp];
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-padded2-%i", LPTIndex]];

	temp = [self highpassFilter:temp];
	
	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-hp-%i", LPTIndex]];

	temp = [self add:temp and:hp];

	[self saveImageColor:temp withName:[NSString stringWithFormat:@"LPTD-added-%i", LPTIndex]];

	
	return temp;
}


#pragma mark -
#pragma mark SML

- (NSArray *)computeSMLForLPT:(NSArray *)lpt
{
	LPTIndex = 0;
	NSMutableArray *sml = [NSMutableArray array];
	for (CIImage *laplacian in lpt) {
		
		[self saveImage:laplacian withName:[NSString stringWithFormat:@"%i-LPTD-original-%i", LPTNumber, LPTIndex]];

		CIImage *result = laplacian;
		
		result = [self padFor3x3Kernel:result];
		result = [self computeSML:result];
		
		[self saveImage:result withName:[NSString stringWithFormat:@"%i-LPTD-sml-%i", LPTNumber, LPTIndex]];
		
		result = [self padFor3x3Kernel:result];
		result = [self smooth:result];
		
		[self saveImage:result withName:[NSString stringWithFormat:@"%i-LPTD-smoothed-%i", LPTNumber, LPTIndex]];

		[sml addObject:result];
		LPTIndex++;
	}
	return sml;
}

- (NSArray *)mergeLPT:(NSArray *)one withLPT:(NSArray *)two
{
	NSMutableArray *merged = [NSMutableArray array];
	for (int i = 0; i < [one count]; i++) {
		CIImage *mergedImg = [self merge:[one objectAtIndex:i] with:[two objectAtIndex:i]];
		
		[self saveImage:mergedImg withName:[NSString stringWithFormat:@"LPTD-mergefactor-%i", i]];
		
		[merged addObject:mergedImg];
	}
	return merged;
}


#pragma mark -
#pragma mark Filter wrapper methods

- (CIImage *)lowpassFilter:(CIImage *)image
{
	CIFilter *lp = [CIFilter filterWithName:@"MGFusionConvolveDownFilter"];
	[lp setValue:image forKey:@"inputImage"];
	return [lp valueForKey:@"outputImage"];
}

- (CIImage *)highpassFilter:(CIImage *)image
{
	CIFilter *hp = [CIFilter filterWithName:@"MGFusionConvolveUpFilter"];
	[hp setValue:image forKey:@"inputImage"];
	return [hp valueForKey:@"outputImage"];
}

- (CIImage *)padForHP:(CIImage *)image
{
	CIFilter *padFilter = [CIFilter filterWithName:@"MGFusionPadFilter"];
	[padFilter setValue:image forKey:@"inputImage"];
	[padFilter setValue:[NSNumber numberWithInt:UPSAMPLE_CONV_PADDING] forKey:@"inputPadAmount"];
	return [padFilter valueForKey:@"outputImage"];
}

- (CIImage *)padForLP:(CIImage *)image
{
	CIFilter *padFilter = [CIFilter filterWithName:@"MGFusionPadFilter"];
	[padFilter setValue:image forKey:@"inputImage"];
	[padFilter setValue:[NSNumber numberWithInt:DOWNSAMPLE_CONV_PADDING] forKey:@"inputPadAmount"];
	return [padFilter valueForKey:@"outputImage"];
}

- (CIImage *)padFor3x3Kernel:(CIImage *)image
{
	CIFilter *padFilter = [CIFilter filterWithName:@"MGFusionPadFilter"];
	[padFilter setValue:image forKey:@"inputImage"];
	[padFilter setValue:[NSNumber numberWithInt:1] forKey:@"inputPadAmount"];
	return [padFilter valueForKey:@"outputImage"];
}

- (CIImage *)decimate:(CIImage *)image
{
	CIFilter *decimate = [CIFilter filterWithName:@"MGFusionDecimateFilter"];
	[decimate setValue:image forKey:@"inputImage"];
	return [decimate valueForKey:@"outputImage"];
}

- (CIImage *)subsample:(CIImage *)image
{
	CIFilter *subsample = [CIFilter filterWithName:@"MGFusionSubsampleFilter"];
	[subsample setValue:image forKey:@"inputImage"];
	return [subsample valueForKey:@"outputImage"];
}

- (CIImage *)supersample:(CIImage *)image
{
	CIFilter *supersample = [CIFilter filterWithName:@"MGFusionSupersampleFilter"];
	[supersample setValue:image forKey:@"inputImage"];
	return [supersample valueForKey:@"outputImage"];
}


- (CIImage *)subtract:(CIImage *)toBeSubtracted from:(CIImage *)original
{
	CIFilter *subtract = [CIFilter filterWithName:@"MGFusionSubtractFilter"];
	[subtract setValue:original forKey:@"inputImage"];
	[subtract setValue:toBeSubtracted forKey:@"inputImage2"];
	return [subtract valueForKey:@"outputImage"];
}

- (CIImage *)add:(CIImage *)one and:(CIImage *)two
{
	CIFilter *add = [CIFilter filterWithName:@"MGFusionAddFilter"];
	[add setValue:one forKey:@"inputImage"];
	[add setValue:two forKey:@"inputImage2"];
	return [add valueForKey:@"outputImage"];
}

- (CIImage *)computeSML:(CIImage *)image
{
	CIFilter *sml = [CIFilter filterWithName:@"MGFusionSMLFilter"];
	[sml setValue:image forKey:@"inputImage"];
	return [sml valueForKey:@"outputImage"];
}

- (CIImage *)smooth:(CIImage *)image
{
	CIFilter *smooth = [CIFilter filterWithName:@"MGFusionSmoothFilter"];
	[smooth setValue:image forKey:@"inputImage"];
	return [smooth valueForKey:@"outputImage"];
}

- (CIImage *)merge:(CIImage *)one with:(CIImage *)two
{
	CIFilter *merge = [CIFilter filterWithName:@"MGFusionMergeFilter"];
	[merge setValue:one forKey:@"inputImage"];
	[merge setValue:two forKey:@"inputImage2"];
	return [merge valueForKey:@"outputImage"];
}

- (CIImage *)cropAndTranslate:(CIImage *)image toRect:(CGRect)rect
{
	CIFilter *crop = [CIFilter filterWithName:@"CICrop"];
	CIVector *cropRect = [CIVector vectorWithX:rect.origin.x Y:rect.origin.y Z:rect.size.width W:rect.size.height];
	[crop setValue:cropRect forKey:@"inputRectangle"];
	[crop setValue:image forKey:@"inputImage"];
	
	CIImage *cropped = [crop valueForKey:@"outputImage"];
	
	NSAffineTransform *t = [NSAffineTransform transform];
	[t translateXBy:-rect.origin.x yBy:-rect.origin.y];
	CIFilter *trafo = [CIFilter filterWithName:@"CIAffineTransform"];
	[trafo setValue:t forKey:@"inputTransform"];
	[trafo setValue:cropped forKey:@"inputImage"];
	
	return [trafo valueForKey:@"outputImage"];
}

- (CIImage *)crop:(CIImage *)image toSize:(CGSize)size
{
	CIFilter *crop = [CIFilter filterWithName:@"CICrop"];
	CIVector *cropRect = [CIVector vectorWithX:0 Y:0 Z:size.width W:size.height];
	[crop setValue:cropRect forKey:@"inputRectangle"];
	[crop setValue:image forKey:@"inputImage"];
	
	return [crop valueForKey:@"outputImage"];
}

- (CIImage *)composite:(CIImage *)background behind:(CIImage *)foreground
{
	CIFilter *composite = [CIFilter filterWithName:@"CISourceOverCompositing"];
	[composite setValue:background forKey:@"inputBackgroundImage"];
	[composite setValue:foreground forKey:@"inputImage"];
	return [composite valueForKey:@"outputImage"];
}


#pragma mark -
#pragma mark Utility


- (IAImage *)imageWithCIImage:(CIImage *)image
{
	
	CGSize refSize = [image extent].size;
	
	NSInteger w = (int)roundf(refSize.width);
	NSInteger h = (int)roundf(refSize.height);
	
	IAImage *result = malloc(sizeof(IAImage));
	result->w = w;
	result->h = h;
	
	result->data = calloc(4 * result->w * result->h, sizeof(unsigned char));
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	
	// ARGB8888; let CG do the conversion for us
	CGContextRef ctx = CGBitmapContextCreate(result->data, 
											 result->w, 
											 result->h, 
											 8 * sizeof(unsigned char),
											 4 * result->w * sizeof(unsigned char),
											 colorspace, 
											 kCGImageAlphaPremultipliedFirst);
	
	CGColorSpaceRelease(colorspace);
	
	NSDictionary *options = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:kCIContextUseSoftwareRenderer];
	
	CIContext *ciCtx = [CIContext contextWithCGContext:ctx options:options];
	
	[ciCtx drawImage:image inRect:CGRectMake(0,0,w,h) fromRect:CGRectMake(0,0,w,h)];
	
	CGContextRelease(ctx);
	
	return result;
	
	
	
}




#pragma mark -
#pragma mark Debug


- (void)saveImage:(CIImage *)img withName:(NSString *)name
{	
	
	if (!saveImages)
		return;

	
	CIFilter *redToGray = [CIFilter filterWithName:@"MGFusionRedToGrayscaleFilter"];
	[redToGray setValue:img forKey:@"inputImage"];
	CIImage *gray = [redToGray valueForKey:@"outputImage"];
	
	size_t w = [img extent].size.width;
	size_t h = [img extent].size.height;
	unsigned char *data = (unsigned char *)calloc(w * h * 4, sizeof(unsigned char));
	CGContextRef ctx = CGBitmapContextCreate(data, w, h, 8, 4 * w, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaNoneSkipFirst);
	CIContext *ciCtx = [CIContext contextWithCGContext:ctx options:NULL];
	[ciCtx drawImage:gray inRect:CGRectMake(0, 0, w, h) fromRect:[img extent]];
	
	NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Desktop/fusion/%@.tiff", name];
	
	CGImageRef result = CGBitmapContextCreateImage(ctx);
	CGImageDestinationRef idst = CGImageDestinationCreateWithURL((CFURLRef)[NSURL fileURLWithPath:path], (CFStringRef)@"public.tiff", 1, NULL);
	CGImageDestinationAddImage(idst, result, NULL);
	
	CGImageDestinationFinalize(idst);
	
	CFRelease(idst);
	CGImageRelease(result);
	CGContextRelease(ctx);
	free(data);
	
}


- (void)saveImageColor:(CIImage *)img withName:(NSString *)name
{	
	
	if (!saveImages)
		return;
	
	CIFilter *normalize = [CIFilter filterWithName:@"MGFusionNormalizeAlphaFilter"];
	[normalize setValue:img forKey:@"inputImage"];
	CIImage *normalized = [normalize valueForKey:@"outputImage"];
	
	size_t w = [img extent].size.width;
	size_t h = [img extent].size.height;
	unsigned char *data = (unsigned char *)calloc(w * h * 4, sizeof(unsigned char));
	CGContextRef ctx = CGBitmapContextCreate(data, w, h, 8, 4 * w, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaNoneSkipFirst);
	CIContext *ciCtx = [CIContext contextWithCGContext:ctx options:NULL];
	[ciCtx drawImage:normalized inRect:CGRectMake(0, 0, w, h) fromRect:[img extent]];
	
	NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Desktop/fusion/%@.tiff", name];
	
	CGImageRef result = CGBitmapContextCreateImage(ctx);
	CGImageDestinationRef idst = CGImageDestinationCreateWithURL((CFURLRef)[NSURL fileURLWithPath:path], (CFStringRef)@"public.tiff", 1, NULL);
	CGImageDestinationAddImage(idst, result, NULL);
	
	CGImageDestinationFinalize(idst);
	
	CFRelease(idst);
	CGImageRelease(result);
	CGContextRelease(ctx);
	free(data);
	
}

@end
