//
//  Macnification_AppDelegate.m
//  Macnification
//
//  Created by Peter Schols on 30/10/06.
//  Copyright Orbicule 2006 . All rights reserved.
//

#import "MGAppDelegate.h"
#import <ImageKit/ImageKit.h>
#import "AnalysisPlugin.h"
#import "RegistrationController.h"
#import "NSMigrationManager_LeopardWorkaround.h"
#import "CaptureDeviceBrowser.h"
#import "TimeOutController.h"
#import "FullScreenController.h"
#import "MGCoreImageMutex.h"
#import "MGLibraryController.h"
#import "PreferenceController.h"
#import "MGImageFilters.h"


@interface MGAppDelegate ()

- (void)_continueLaunchingAfterLicenseUI;

- (MGLibrary *)_library;
- (void)_openLibraryController;


- (void)_changeLibraryPath:(NSString *)path;

- (void)_showWelcomeWindow;
- (void)_showChooseLibraryWindowWithExistingDefaultLibrary:(BOOL)adjustForExistingDefaultLibrary;;
- (void)_closeStartWindows;


+ (void)_initializeUserDefaults;
+ (NSString *)_spreadsheetApplicationPath;
+ (NSString *)_extAnalyzerApplicationPath;
+ (NSString *)_extEditorApplicationPath;
+ (NSString *)_usablePathWithIdentifiers:(NSArray *)identifiers;


@end


@implementation MGAppDelegate


@synthesize sharedOperationQueue, analysisPlugins;




# pragma mark -
# pragma mark Init and lifecycle

- (id) init
{
	self = [super init];
	if (self != nil) {
		[NSMigrationManager addRelationshipMigrationMethodIfMissing];
        
        [self setSharedOperationQueue:[[[NSOperationQueue alloc] init] autorelease]]; 
        [self.sharedOperationQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
        
        // Shows the alpha slider in color panels 
        [NSColor setIgnoresAlpha:NO];
        
        // Force initialization of the mutex
        [MGCoreImageMutex sharedMutex];
        
        MGRegisterImageFilters();
        

	}
	return self;
}


- (void) dealloc 
{
	[sharedOperationQueue release];
	
	[super dealloc];
}


- (void)awakeFromNib
{

}

#pragma mark -
#pragma mark User Defaults

+ (void)initialize
{
	[MGAppDelegate _initializeUserDefaults];
}

+ (void)_initializeUserDefaults;
{
	NSString *fullPath = nil;
	
	for (NSString *identifier in [NSArray arrayWithObjects:@"com.CRS.CoLocalizer.Pro", @"com.CRS.CoLocalizer.Express", @"gov.nih.info.rsb.ImageJ", @"com.apple.Preview", nil]) {
		if (fullPath)
			continue;
		fullPath = [[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier:identifier];
	}
	
	
	
	NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
									   @"Arial", @"scaleBarFontName", 
									   [NSNumber numberWithInt:20], @"scaleBarFontSize", 
									   [NSArchiver archivedDataWithRootObject:[NSColor whiteColor]], @"scaleBarColor",
									   [NSNumber numberWithFloat:1.0], @"scaleBarStroke", 
									   @"Plain", @"scaleBarStyle", 
									   [NSNumber numberWithBool:YES], @"showsScaleBarLabel", 
									   [NSNumber numberWithFloat:0.80], @"scaleBarXProportion", 
									   [NSNumber numberWithFloat:0.07], @"scaleBarYProportion", 
									   [NSArchiver archivedDataWithRootObject:[NSColor colorWithCalibratedRed:0.8 green:0.4 blue:0 alpha:1.0]], @"selectedROIColor", 
									   [NSArchiver archivedDataWithRootObject:[NSColor colorWithCalibratedRed:0.8 green:0.4 blue:0 alpha:0.5]], @"deselectedROIColor",
									   [NSArchiver archivedDataWithRootObject:[NSColor colorWithCalibratedRed:0.8 green:0.4 blue:0 alpha:1.0]], @"layoutGuideColor", 
									   [NSArchiver archivedDataWithRootObject:[NSColor colorWithCalibratedRed:0.8 green:0.4 blue:0 alpha:1.0]], @"sliceToolColor", 
									   [NSNumber numberWithFloat:1.0], @"ROIStroke", 
									   [NSNumber numberWithBool:YES], @"showROIPanelWhenMeasuring", 
									   [NSNumber numberWithBool:YES], @"alwaysZoomToFit", 
									   [NSNumber numberWithBool:YES], @"showNoteIcon",
									   [NSNumber numberWithBool:YES], @"snapToGuides",
									   @"Name", @"defaultStackSortKey",
									   [NSNumber numberWithBool:NO], @"showStackReflections",
									   [NSNumber numberWithBool:YES], @"showWelcomeScreen",
									   [NSNumber numberWithBool:NO], @"retainCompressedFormats",
									   [NSNumber numberWithBool:NO], @"scaleBarRestrictedToStandardSizes",
									   [NSArchiver archivedDataWithRootObject:[NSColor blackColor]], @"scaleBarBackgroundBoxColor",
									   [NSNumber numberWithBool:NO], @"scaleBarUseBackgroundBox",
									   [NSNumber numberWithInt:0], @"splitChannelMethod",
									   [NSNumber numberWithBool:YES], @"exportImagesWithScalebar",
									   
									   [NSNumber numberWithInt:0], @"metadataExportContent",
									   [NSNumber numberWithInt:0], @"metadataExportStyle",
									   [NSNumber numberWithInt:0], @"metadataExportSeparator",
									   [NSNumber numberWithBool:NO], @"metadataExportShowAsOneEntry",
                                       
                                        
									   @"[No Data]", @"metadataExportPlaceholder",
									   
									   nil];
	
	
	
	
	if ([MGAppDelegate _spreadsheetApplicationPath])
		[dictionary setObject:[MGAppDelegate _spreadsheetApplicationPath] forKey:@"spreadsheetApplicationPath"];
	if ([MGAppDelegate _extAnalyzerApplicationPath])
		[dictionary setObject:[MGAppDelegate _extAnalyzerApplicationPath] forKey:@"externalAnalyzerPath"];
	if ([MGAppDelegate _extEditorApplicationPath])
		[dictionary setObject:[MGAppDelegate _extEditorApplicationPath] forKey:@"externalEditorPath"];
	
    
	[[NSUserDefaultsController sharedUserDefaultsController] setInitialValues:dictionary];
	[[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    
	// Let our observers know that we changed our spreadsheet application (the ROI HUD needs this)
	NSNotification *note = [NSNotification notificationWithName:@"spreadsheetApplicationDidChange" object:self userInfo:NULL];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}


// Find out which spreadsheet app we will use by default
+ (NSString *)_spreadsheetApplicationPath;
{
	return [MGAppDelegate _usablePathWithIdentifiers:[NSArray arrayWithObjects:@"com.apple.iWork.Numbers", @"com.microsoft.Excel", nil]];
}

+ (NSString *)_extAnalyzerApplicationPath;
{
	return [MGAppDelegate _usablePathWithIdentifiers:[NSArray arrayWithObjects:@"com.CRS.CoLocalizer.Pro", @"com.CRS.CoLocalizer.Express", @"gov.nih.info.rsb.ImageJ", @"com.apple.Preview", nil]];
}

+ (NSString *)_extEditorApplicationPath;
{
	return [MGAppDelegate _usablePathWithIdentifiers:[NSArray arrayWithObjects:@"gov.nih.info.rsb.ImageJ", @"com.apple.Preview", nil]];
}

+ (NSString *)_usablePathWithIdentifiers:(NSArray *)identifiers
{
	NSString *fullPath = nil;
	
	for (NSString *identifier in identifiers)
		if ((fullPath = [[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier:identifier]))
			return fullPath;
	
	return nil;
}





# pragma mark -
# pragma mark Application delegate


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{    

    [self _continueLaunchingAfterLicenseUI];
}


- (void)_continueLaunchingAfterLicenseUI
{
    if ([MGLibrary prefsLibraryPath]) {
        
        if ([MGLibrary libraryExistsAtPath:[MGLibrary prefsLibraryPath]] || [[NSUserDefaults standardUserDefaults] boolForKey:@"createNewLibraryOnStartup"]) {
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"createNewLibraryOnStartup"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            library_ = [[MGLibrary alloc] initWithPath:[MGLibrary prefsLibraryPath]];
            [library_ setDelegate:self];
            
            [self _openLibraryController];
        }
        
        else if ([MGLibrary legacyLibraryExists])
            [self createOrOpenDefaultLibrary:self];
        
        else
            [self _showChooseLibraryWindowWithExistingDefaultLibrary:[MGLibrary libraryExistsAtPath:[MGLibrary defaultLibraryPath]]];
        
    }
    
    else {
        
        if ([MGLibrary legacyLibraryExists])
            [self createOrOpenDefaultLibrary:self];
        else 
            [self _showWelcomeWindow];
    }
    
    
    // try to open pending files at this point
    // if libraryController still doesn't exist, don't bother (these are edge cases only)
    if (pendingOpenFiles_) {
        [libraryController_ openFiles:pendingOpenFiles_];
        [pendingOpenFiles_ release];
        pendingOpenFiles_ = nil;
    }
    
}


- (void)applicationDidBecomeActive:(NSNotification *)aNotification;
{
	[libraryController_ updateImagesModifiedByExternalEditor];
    
}



- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag
{
    [[libraryController_ window]  performSelector:@selector(makeKeyAndOrderFront:) withObject:self afterDelay:0.02];

	return YES;   
}

- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename
{
    if ([[filename pathExtension] isEqual:@"mnlibrary"])
        [self _changeLibraryPath:filename];
    else {
        if (libraryController_) {
            [libraryController_ openFiles:[NSArray arrayWithObject:filename]];
        }
        else {
            // queue these files
            if (pendingOpenFiles_)
                pendingOpenFiles_ = [[[pendingOpenFiles_ autorelease] arrayByAddingObject:filename] retain];
            else
                pendingOpenFiles_ = [[NSArray arrayWithObject:filename] retain];
        }
    }
    
    return YES;
}


- (void)application:(NSApplication *)theApplication openFiles:(NSArray *)filenames;
{
    if ([filenames count] > 0 && [[[filenames objectAtIndex:0] pathExtension] isEqual:@"mnlibrary"])
        [self _changeLibraryPath:[filenames objectAtIndex:0]];
    
    else {
        if (libraryController_) {
            [libraryController_ openFiles:filenames];
        }
        else {
            // queue these files
            if (pendingOpenFiles_)
                pendingOpenFiles_ = [[[pendingOpenFiles_ autorelease] arrayByAddingObjectsFromArray:filenames] retain];
            else
                pendingOpenFiles_ = [filenames copy];
        }
    }
    


}


#pragma mark -
#pragma mark Panel handling

- (void)_showWelcomeWindow
{
    [self _closeStartWindows];
    

    [welcomeWindow_ center];
    [welcomeWindow_ makeKeyAndOrderFront:self];
}


- (void)_showChooseLibraryWindowWithExistingDefaultLibrary:(BOOL)adjustForExistingDefaultLibrary;
{
    [self _closeStartWindows];
    
    [selectLibraryCurrentLocationField_ setStringValue:[MGLibrary prefsLibraryPath]];

    [selectLibraryNewOrOpenButton_ setTitle:adjustForExistingDefaultLibrary ? @"Open Default Library" : @"Create New Library"];
    [selectLibraryNewOrOpenField_ setStringValue:adjustForExistingDefaultLibrary ? @"Open the default Library in ~/Pictures/" : @"Create an empty Library in ~/Pictures/"];

    [selectLibraryWindow_ center];
    [selectLibraryWindow_ makeKeyAndOrderFront:self];
    
}

- (void)_closeStartWindows
{
    [welcomeWindow_ orderOut:self];
    [selectLibraryWindow_ orderOut:self];
}

#pragma mark -
#pragma mark Library handling


- (void)_openLibraryController
{
    NSAssert(library_, @"Library should exist at this point!");
    
    if (!libraryController_) {
        libraryController_ = [[MGLibraryController alloc] initWithLibrary:library_];
        [libraryController_ loadUI];
    }
    
    [[libraryController_ window] performSelector:@selector(makeKeyAndOrderFront:) withObject:self afterDelay:0.03];
}



- (void)_changeLibraryPath:(NSString *)path;
{
    
    if ([[MGLibrary prefsLibraryPath] isEqual:[path stringByAbbreviatingWithTildeInPath]])
        return;
    
    if (libraryController_) {
        // Library is already open -- need to restart
        NSAlert *alert = [NSAlert alertWithMessageText:@"Do you want to change your Library?" defaultButton:@"Relaunch and Change" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"Macnification needs to relaunch in order to change its Library."];
        if ([alert runModal] != NSAlertDefaultReturn)
            return;
        
    }
    
    
    NSLog(@"Applying new library (%@)", path);
    
    [MGLibrary setPrefsLibraryPath:path];
    
    if (libraryController_) {
        [self prepareForRestart];
        [self restart];
    }
}


#pragma mark -
#pragma mark Restart


- (void)prepareForRestart;
{
    [libraryController_ save];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)restart;
{
    NSString *relauncherPath = [[NSBundle mainBundle] pathForAuxiliaryExecutable:@"restart"];
    
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath:relauncherPath];
    [task setArguments:[NSArray arrayWithObject:[[NSBundle mainBundle] bundlePath]]];
    [task setStandardInput:[NSPipe pipe]];
    [task launch];
    
	[NSApp terminate:self];
}



#pragma mark -
#pragma mark Library controller delegate

- (void)libraryWillStartConversion:(MGLibrary *)ctl;
{
    [refactoringWindow center];
    [refactoringWindow makeKeyAndOrderFront:self];
    [refactoringIndicator setIndeterminate:YES];
    [refactoringIndicator setUsesThreadedAnimation:YES];
    [refactoringIndicator startAnimation:self];
}

    
- (void)libraryDidCompleteConversion:(MGLibrary *)ctl;
{
    
    [refactoringIndicator stopAnimation:self];
    [refactoringWindow close];

}




# pragma mark -
# pragma mark Expire



- (void)showTimeOutScreenIfUnregistered;
{
	RegistrationController *rc = [[RegistrationController alloc] init];
	if ([rc isRegistered]) {
	}
	else {
		if (!timeOutController) {
			timeOutController = [[TimeOutController alloc] init];
		}
		[timeOutController showWindow:self];
	}
	[rc release];	
}

- (IBAction)showRegistrationPanel:(id)sender;
{
}


# pragma mark -
# pragma mark Prefs


- (IBAction)showPreferencePanel:(id)sender;
{
	if (!preferenceController) {
		preferenceController = [[PreferenceController alloc]init];
	}
	[preferenceController showWindow:self];
}



- (IBAction)showRegistrationPreferences:(id)sender;
{
	if (!preferenceController) {
		preferenceController = [[PreferenceController alloc]init];
	}
	[preferenceController showWindow:self];
	[preferenceController switchToPreferencePaneAndSelectPreference:4];
}

#pragma mark -
#pragma mark Panel actions


- (IBAction)showHelp:(id)sender;
{
    
}

- (IBAction)createEmptyLibrary:(id)sender;
{
    [MGLibrary setPrefsLibraryPath:[MGLibrary defaultLibraryPath]];

    
    library_ = [[MGLibrary alloc] initWithDefaultPathUseDemoContents:NO];
    [library_ setDelegate:self];
    
    [self _closeStartWindows];
    [self _openLibraryController];
}

- (IBAction)createDemoLibrary:(id)sender;
{
    [MGLibrary setPrefsLibraryPath:[MGLibrary defaultLibraryPath]];

    library_ = [[MGLibrary alloc] initWithDefaultPathUseDemoContents:YES];
    [library_ setDelegate:self];

    [self _closeStartWindows];
    [self _openLibraryController];
}



- (IBAction)chooseLibrary:(id)sender;
{
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setAllowedFileTypes:[NSArray arrayWithObject:@"mnlibrary"]];
    
    if ([openPanel runModal] == NSCancelButton)
        return;
    
    if ([[openPanel filenames] count] != 1) {
        NSBeep();
        return;
    }
    
    NSString *file = [[openPanel filenames] objectAtIndex:0];
    
    [MGLibrary setPrefsLibraryPath:file];

    library_ = [[MGLibrary alloc] initWithPath:file];
    [library_ setDelegate:self];

    [self _openLibraryController];

}

- (IBAction)createOrOpenDefaultLibrary:(id)sender;
{
    [MGLibrary setPrefsLibraryPath:[MGLibrary defaultLibraryPath]];

    library_ = [[MGLibrary alloc] initWithDefaultPathUseDemoContents:NO];
    [library_ setDelegate:self];

    [self _closeStartWindows];
    [self _openLibraryController];
}



# pragma mark -
# pragma mark Core Data methods


- (MGLibrary *)_library
{
    return library_;
}

- (MGLibraryController *)libraryController;
{
    return libraryController_;
}


- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender 
{
	
	
	NSError *error;
	int reply = NSTerminateNow;
	
	// Export our Spotlight Metadata files before saving the managedObjectContext, so we can export only updated or inserted images.
	[libraryController_ exportSpotlightMetadata];
	
    
    NSManagedObjectContext *ctx = [library_ managedObjectContext];
	
	if (ctx) {
		if ([ctx commitEditing]) {
			if ([ctx hasChanges] && ![ctx save:&error]) {
				
				NSLog(@"%@", [error description]);
				
				BOOL errorResult = [[NSApplication sharedApplication] presentError:error];
				
				if (errorResult == YES) {
					reply = NSTerminateCancel;
				} 
				
				else {
					
					int alertReturn = NSRunAlertPanel(nil, @"Could not save changes while quitting. Quit anyway?" , @"Quit anyway", @"Cancel", nil);
					if (alertReturn == NSAlertAlternateReturn) {
						reply = NSTerminateCancel;	
					}
				}
			}
		} 
		
		else {
			reply = NSTerminateCancel;
		}
	}
    
    [libraryController_ prepareForTermination];
	
	return reply;
}



@end
