//
//  LightTableView.m
//  LightTable
//
//  Created by Dennis on 18/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableView.h"

#import "LightTableContentView.h"
#import "LightTableToolView.h"
#import "LightTableAlignLayoutElement.h"
#import "LightTableImageView.h"
#import "LightTable.h"
#import "LightTableAlignConstraint.h"
#import "LightTableCollageController.h"

#import "MGContextualToolStrip.h"
#import "NSButton_MGAdditions.h"
#import "NSView_MGAdditions.h"
#import "WorkflowInfoPanel.h"
#import "MGAppDelegate.h"

#import "NSView_MGAdditions.h"
#import "MGFunctionAdditions.h"


static CGSize LTMinimumContentSize = {150, 100};

@interface LightTableView (Private)


- (void)setGuideTimeout:(NSTimeInterval)time;
- (void)disableGuideTimeout;
- (void)guideTimerDidEnd:(NSTimer *)timer;

@end


@implementation LightTableView


@synthesize contentView, snapsToLayoutGuides, printingRectMode, zoomLevel, showsScalebarIfAvailable;



#pragma mark Init
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		
		layoutElements = [[NSMutableArray array] retain];
		guideTimeoutTimer = nil;
		zoomLevel = 1.0;
		[self bind:@"snapsToLayoutGuides" toObject:[NSUserDefaultsController sharedUserDefaultsController] withKeyPath:@"values.snapToGuides" options:nil];
		previouslySelectedLightTable = nil;
		
		contentViewBounds = NSZeroRect;
		
    }
    return self;
}


- (void)awakeFromNib
{

	// we can't let this view autosize itself because it would make the frame smaller within the enclosing scroll view when the latter is resized (e.g. window resize)
	// the frame has to remain constant because the area of our light table is invariant to any operation besides image bounding box changes.
	// so we need to set the correct mask, AND 
	//[self setAutoresizesSubviews:YES];
	
	[self setAutoresizingMask:NSViewNotSizable];
	
	// content view: contains the imageview and draws the light table background.
	contentView = [[LightTableContentView alloc] initWithFrame:[self bounds]];
	contentView.imageViewContextualMenu = contextualMenu;
	contentView.tableContextualMenu = tableContextualMenu;

	contentView.commentView = commentView;
	[contentView setAutoresizingMask:(NSViewHeightSizable | NSViewWidthSizable)];
	[self addSubview:contentView];
	
	[contentView setHidden:YES];
	
	// toolview: overlaps the content view.
	toolView = [[LightTableToolView alloc] initWithFrame:[self bounds]];
	[toolView setAutoresizingMask:(NSViewHeightSizable | NSViewWidthSizable)];
	[self addSubview:toolView positioned:NSWindowAbove relativeTo:contentView];
	
	// the bindings that are both forwarded to the content view and used by ourself.
	[self bind:@"imageContent" toObject:lightTableImageArrayController withKeyPath:@"arrangedObjects" options:nil];
	[self bind:@"imageSelection" toObject:lightTableImageArrayController withKeyPath:@"selectedObjects" options:nil];
	[self bind:@"lightTableSelection" toObject:lightTableArrayController withKeyPath:@"selectedObjects" options:nil];
	
	// TODO: add scalebar support
	[self bind:@"zoomLevel" toObject:lightTableArrayController withKeyPath:@"selection.browserZoom" options:nil];
	//[self bind:@"showsScalebarIfAvailable" toObject:lightTableArrayController withKeyPath:@"selection.showsScalebarIfAvailable" options:nil];
	
	// set the default bounds until the first light table is loaded.
	//[self setContentViewBounds:NSMakeRect(0,0,100,100)];
	
	// register for scrollview/clipview bounds updates, because we need to resize too, as this isn't done automatically.  Ask out scrollview to post the relevant notifications.
	[[self enclosingScrollView] setPostsBoundsChangedNotifications:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(superviewDidResize:) name:NSViewFrameDidChangeNotification object:[self enclosingScrollView]];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateImageContent) name:@"willExitFullScreenMode" object:nil];
	
	
	[self initializeToolStrip];
	
	[self setAutoresizesSubviews:NO];
	[toolView setAutoresizesSubviews:YES];
	[contentView setAutoresizesSubviews:YES];
	
	[[[self enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[self enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
		
}



- (void)initializeToolStrip
{
	//NSImage *coll = [NSImage imageNamed:@"lt_table_collage"];
	NSImage *custColl = [NSImage imageNamed:@"lt_custom_collage"];
	NSImage *rearrangeResize = [NSImage imageNamed:@"lt_rearrange_resize"];
	NSImage *rearrange = [NSImage imageNamed:@"lt_rearrange"];
	NSImage *remove = [NSImage imageNamed:@"lt_remove_image"];
	NSImage *opacityHud = [NSImage imageNamed:@"lt_opacity_hud"];
	
	//[coll setSize:NSMakeSize(32, 32)];
	[custColl setSize:NSMakeSize(32, 32)];
	[rearrangeResize setSize:NSMakeSize(32, 32)];
	[rearrange setSize:NSMakeSize(32, 32)];
	[remove setSize:NSMakeSize(32, 32)];
	[opacityHud setSize:NSMakeSize(32, 32)];

	
	saveTableButton = [NSButton toolStripButtonWithImage:custColl title:@"Table Collage"];
	//saveSelectionButton = [NSButton toolStripButtonWithImage:custColl title:@"Custom Collage"];
	resizeAndRearrangeButton = [NSButton toolStripButtonWithImage:rearrangeResize title:@"Rearrange and Match"];
	rearrangeButton = [NSButton toolStripButtonWithImage:rearrange title:@"Rearrange"];

	removeButton = [NSButton toolStripButtonWithImage:remove title:@"Remove"];
	opacityHudButton = [NSButton toolStripButtonWithImage:opacityHud title:@"Opacity"];

	
	[saveTableButton setTarget:self];
	//[saveSelectionButton setTarget:self];
	[showAnnotationsButton setTarget:self];
	[resizeAndRearrangeButton setTarget:self];
	[rearrangeButton setTarget:self];
	[removeButton setTarget:contentView];
	[opacityHudButton setTarget:self];

	
	[saveTableButton setToolTip:@"Create a Collage of the Light Table"];
	//[saveSelectionButton setToolTip:@"Create a Collage from a specific area of the Light Table"];
	[resizeAndRearrangeButton setToolTip:@"Arrange all images in the Light Table, resizing them to the size of the selection"];
	[rearrangeButton setToolTip:@"Arrange all images in the Light Table"];
	[removeButton setToolTip:@"Remove the selected images from the Light Table"];
	[opacityHudButton setToolTip:@"Toggle the image opacity Inspector"];

	
	[saveTableButton setAction:@selector(createCollage:)];
	//[saveSelectionButton setAction:@selector(enterImagePrintRectangleMode:)];
	[showAnnotationsButton setAction:@selector(showCommentViewForSelectedView:)];
	[resizeAndRearrangeButton setAction:@selector(resizeAndArrange:)];
	[rearrangeButton setAction:@selector(arrangeAllInGrid:)];
	[removeButton setAction:@selector(shouldRemoveSelectedObjects)];
	[opacityHudButton setAction:@selector(toggleOpacityHUD:)];
	
		
	[toolStrip addToolView:saveTableButton withAlignment:MGLeftViewAlignment];
	[toolStrip addToolView:opacityHudButton withAlignment:MGLeftViewAlignment];

	//[toolStrip addToolView:saveSelectionButton withAlignment:MGLeftViewAlignment];
	[toolStrip addWhitespaceWithAlignment:MGCenterViewAlignment];

	
	[toolStrip addToolView:removeButton withAlignment:MGRightViewAlignment];

	[toolStrip addSeparatorWithAlignment:MGRightViewAlignment];
	[toolStrip addToolView:resizeAndRearrangeButton withAlignment:MGRightViewAlignment];
	[toolStrip addToolView:rearrangeButton withAlignment:MGRightViewAlignment];
	
}


- (void)dealloc
{
	// unbind the bindings
	// the bindings that are both forwarded to the content view and used by ourself.
	[self unbind:@"imageContent"]; 
	[self unbind:@"imageSelection"];
	[self unbind:@"lightTableSelection"];
	[self unbind:@"zoomLevel"];
	
	[toolView release];
	[contentView release];
	[layoutElements release];
	
	// remove ourself as observer
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}




#pragma mark Bindings
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bindings
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	NSKeyValueObservingOptions opt = (NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial);
	
	if ([binding isEqualToString:@"imageSelection"] || [binding isEqualToString:@"imageContent"] || [binding isEqualToString:@"lightTableSelection"]) {
		
		// forward these binding requests to the content view.
		[contentView bind:binding toObject:observableController withKeyPath:keyPath options:options];		
		
		if ([binding isEqualToString:@"lightTableSelection"]) {
			
			// observe to change bounds/frame size if needed.
			[observableController addObserver:self forKeyPath:@"selectedObjects" options:opt context:nil];
			[lightTableImageArrayController addObserver:self forKeyPath:@"arrangedObjects" options:opt context:nil];
			[lightTableImageArrayController addObserver:self forKeyPath:@"selectedObjects" options:opt context:nil];

		}
		
	} else {
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
	}
}

- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"imageSelection"] || [binding isEqualToString:@"imageContent"] || [binding isEqualToString:@"lightTableSelection"]) {
		
		// forward these binding requests to the content view.
		[contentView unbind:binding];		
		
		if ([binding isEqualToString:@"lightTableSelection"]) {
			
			// remove observer that was used to change bounds/frame size if needed.
			[lightTableArrayController removeObserver:self forKeyPath:@"selectedObjects"];
			[lightTableImageArrayController removeObserver:self forKeyPath:@"arrangedObjects"];
			[lightTableImageArrayController removeObserver:self forKeyPath:@"selectedObjects"];

		}
		
	} else {
		[super unbind:binding];
	}
	
	
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{	
	if (![previouslySelectedLightTable isDeleted])
		[self saveFrame:[contentView bounds] toTable:previouslySelectedLightTable];
	
	if (object == lightTableArrayController && [keyPath isEqualToString:@"selectedObjects"]) {
		if ([[lightTableArrayController selectedObjects] count] == 1) {
			
			// Make sure the selected collection is a LT! (added by Peter)
			LightTable *selectedTable = [[lightTableArrayController selectedObjects] objectAtIndex:0];
			
			if ([selectedTable isKindOfClass:[NSManagedObject class]] && [[[selectedTable entity] name] isEqualTo:@"LightTable"]) {
								
				// change our bounds to conform to the light table size.
				
				[self applyContentViewBoundsFromModel];
				[self performSelector:@selector(applyContentViewBoundsFromModel) withObject:nil afterDelay:0.001];
				[self scrollContentViewCenterPoint:[self loadCenterPoint]];
				
				previouslySelectedLightTable = selectedTable;
				
				[contentView setHidden:NO];
				
			} else {
				
				// close some LT specific widgets
				[opacityHUD orderOut:self];
				
			}
		}
	} else if (object == lightTableImageArrayController && [keyPath isEqualToString:@"selectedObjects"]) {

		[resizeAndRearrangeButton setEnabled:[[lightTableImageArrayController selectedObjects] count] > 0];
		[removeButton setEnabled:[[lightTableImageArrayController selectedObjects] count] > 0];
		
									  
	} else if (object == lightTableImageArrayController && [keyPath isEqualToString:@"arrangedObjects"]) {

		// update UI
		[saveTableButton setEnabled:[[lightTableImageArrayController arrangedObjects] count] > 0];
		[saveSelectionButton setEnabled:[[lightTableImageArrayController arrangedObjects] count] > 0];
		[resizeAndRearrangeButton setEnabled:[[lightTableImageArrayController arrangedObjects] count] > 0];
		[rearrangeButton setEnabled:[[lightTableImageArrayController arrangedObjects] count] > 0];

									  
	} else {
		
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
		
	 }
}


- (void)updateImageContent
{
	// trigger a bindings update for filteredImage
	
	
	
	
}



#pragma mark Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setShowsScalebarIfAvailable:(BOOL)flag
{
	for (LightTableImage *img in [lightTableImageArrayController arrangedObjects]) {
		
		LightTableImageView *view = [contentView imageViewForImage:img];
		view.toolView.showsScalebarIfAvailable = flag;
	}
	
	showsScalebarIfAvailable = flag;
}

- (NSView *)hitTest:(NSPoint)aPoint
{
	// we never need to forward the event to the toolview (as it only serves drawing purposes), always to the content view.
	// aPoint is in the superviews coord system, so need to invoke hitTest with aPoint converted to own system.
	NSView *subview = [contentView hitTest:[self convertPoint:aPoint fromView:[self superview]]];
	return subview;
	
}

- (void)drawRect:(NSRect)rect 
{
	
	[[NSColor colorWithCalibratedWhite:0.25 alpha:1.0] set];
	
	//[[NSColor redColor] set];
	
	NSRectFill([self bounds]);
	
}

- (BOOL)isOpaque
{
	return YES;
}

#pragma mark View Layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// View Layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)applyContentViewBoundsFromModel
{
	NSRect newRect = [self loadFrame];
	
	[self setContentViewBounds:newRect];

}

- (void)setContentViewBounds:(NSRect)bounds;
{
	
	NSRect integralBounds = NSIntegralRect(bounds);
	
	NSRect ctvFrameRect = NSZeroRect;
	ctvFrameRect.size = integralBounds.size;
	
	// set our own frame, by multiplying our bounds with the scale factor
	CGFloat scaleFactor = self.zoomLevel;
	NSRect ownFrameRect = ctvFrameRect;
	ownFrameRect.size.width *= scaleFactor;
	ownFrameRect.size.height *= scaleFactor;
	ownFrameRect = NSIntegralRect(ownFrameRect);
	
	// set our own bounds

	[self setBounds:ctvFrameRect];

	// set the frame AFTER the bounds
	[self setFrame:ownFrameRect];
	
	// set the bounds of the content view
	[contentView setFrame:ctvFrameRect];
	[contentView setBounds:integralBounds];

	// do the same with the tool view
	[toolView setFrame:ctvFrameRect];
	[toolView setBounds:integralBounds];

	// this call, again, is absolutely needed. For some reason beyond my grasp, the contentview frame is not equal to the ltv bounds, 
	// despite our  previous invocation of the same call.
	[self setBounds:ctvFrameRect];

	
	//NSLog(@" ------------- ");
	//NSLog(@"ctv + tv bounds: %@", NSStringFromRect(integralBounds));
	//NSLog(@"ctv + tv frame + ltv bounds: %@", NSStringFromRect(ctvFrameRect));
	//NSLog(@"ltv frame: %@", NSStringFromRect(ownFrameRect));
	//NSLog(@" ------------- ");
	
	[self setNeedsDisplay:YES];
	[contentView setNeedsDisplay:YES];

}


- (void)updateContentViewBoundsForImages
{
	// a frame of the imageviews has changed.
	// update our view hierarchy to fit the imageviews (based on the image bounding box)
	
	// the new bounds that the content view must assume is the union rect of the current ctv bounds and the bounding box of the images.
	NSRect imageBoundingBox = [contentView imageViewsBoundingBox];
	NSRect oldCtvBounds = [contentView bounds];
	
	
	NSRect proposedNewBounds = NSUnionRect(oldCtvBounds, NSInsetRect(imageBoundingBox, -2 * MGTableMargin, -2 * MGTableMargin));
	
	// it's no use updating the same frame
	if (NSEqualRects(proposedNewBounds, oldCtvBounds)) return;

	// in order for the frame to be smaller than the image box, the union rect between frame and box should encompass frame (e.g. their intersection should be equal to frame)
	// use our own comparison function as the dimensions of the rects are not always exact.
	if (!MGEqualRects(NSIntersectionRect(oldCtvBounds, proposedNewBounds), oldCtvBounds)) return;
	//NSLog(@"old bounds %@, boundingbox %@", NSStringFromRect(oldCtvBounds), NSStringFromRect(imageBoundingBox));

	//NSLog(@"setting ctv size with bounding box");
	
	[self setContentViewBounds:proposedNewBounds];
	
	if ([[lightTableArrayController selectedObjects] count] == 1) {
		[self saveFrame:proposedNewBounds toTable:[[lightTableArrayController selectedObjects] objectAtIndex:0]];
	}

}

- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize
{
	[super resizeSubviewsWithOldSize:oldBoundsSize];
	
	
}



- (IBAction)minimizeFrameSize:(id)sender
{
	// constrain the frame to the image bounding box but make sure to keep at least the size of the document visible rect.
	
	NSRect imgBoundingBox = [contentView imageViewsBoundingBox];
	imgBoundingBox = NSInsetRect(imgBoundingBox, -2 * MGTableMargin, -2 * MGTableMargin);
	
	[self setContentViewBounds:imgBoundingBox];
	
	
	if ([[lightTableArrayController selectedObjects] count] == 1) {
		[self saveFrame:imgBoundingBox toTable:[[lightTableArrayController selectedObjects] objectAtIndex:0]];
	}
	
}

- (void)saveFrame:(NSRect)frame toTable:(LightTable *)table
{	
	//NSLog(@"saving LT geometry: %@ toTable: %@", NSStringFromRect(frame), @"");
	
	[table setValue:[NSNumber numberWithFloat:frame.origin.x] forKey:@"boundsX"];
	[table setValue:[NSNumber numberWithFloat:frame.origin.y] forKey:@"boundsY"];
	[table setValue:[NSNumber numberWithFloat:frame.size.height] forKey:@"boundsHeight"];
	[table setValue:[NSNumber numberWithFloat:frame.size.width] forKey:@"boundsWidth"];
	
	
	// now is a good time to save the center position!
	NSPoint centerPoint = [self contentViewCenterPoint];
	
	[self saveCenterPoint:centerPoint toTable:table];
	
	// just because Core Data doesn't think it should save, doesn't mean we would like it to ;)
	// this will cause a managed object model not to be found at certain times (app exit) so catch it.
	@try {
		[[table managedObjectContext] processPendingChanges];
	}
	
	@catch (NSException * e) {
	}

}

- (NSRect)loadFrame
{
	if ([[lightTableArrayController selectedObjects] count] == 0) return NSZeroRect;
	
	id selectedTable = [[lightTableArrayController selectedObjects] objectAtIndex:0];
	
	CGFloat x = [[selectedTable valueForKey:@"boundsX"] floatValue];
	CGFloat y = [[selectedTable valueForKey:@"boundsY"] floatValue];
	CGFloat height = [[selectedTable valueForKey:@"boundsHeight"] floatValue];
	CGFloat width = [[selectedTable valueForKey:@"boundsWidth"] floatValue];
	
	//NSLog(@"loaded LT geometry:%@",  NSStringFromRect(NSMakeRect(x, y, width, height)));
	
	width = fmax(width, LTMinimumContentSize.width);
	height = fmax(height, LTMinimumContentSize.height);

	
	return NSMakeRect(x, y, width, height);
}

- (NSPoint)loadCenterPoint
{
	if ([[lightTableArrayController selectedObjects] count] == 0) return NSZeroPoint;
	
	id selectedTable = [[lightTableArrayController selectedObjects] objectAtIndex:0];
	
	CGFloat x = [[selectedTable valueForKey:@"centerX"] floatValue];
	CGFloat y = [[selectedTable valueForKey:@"centerY"] floatValue];
	
	return NSMakePoint(x, y);
	
	
}

- (void)saveCenterPoint:(NSPoint)centerPoint toTable:(LightTable *)table
{
	[table setValue:[NSNumber numberWithFloat:centerPoint.x] forKey:@"centerX"];
	[table setValue:[NSNumber numberWithFloat:centerPoint.y] forKey:@"centerY"];
	
	
	
}


- (NSPoint)contentViewCenterPoint
{	
	// the center point in the bounds coords of the content view!
	NSPoint centerPoint = NSMakePoint(NSMidX([[self enclosingScrollView] documentVisibleRect]), (NSMidY([[self enclosingScrollView] documentVisibleRect])));
	NSPoint contentViewCenterPoint = [contentView convertPoint:centerPoint fromView:self];
	//NSLog(@"ctp: %@", NSStringFromPoint(contentViewCenterPoint));
	
	return contentViewCenterPoint;
}


- (void)superviewDidResize:(NSNotification *)notification
{
	//[self updateContentViewBoundsForImages];

}



- (NSPoint)currentScrollCenterPoint
{
	// returns the point (in own coord system) in the middle of the scroll doc view.
	NSRect visibleRect = [[self enclosingScrollView] documentVisibleRect];
	return NSMakePoint(NSMidX(visibleRect), NSMidY(visibleRect));
}

- (void)scrollContentViewCenterPoint:(NSPoint)center
{
	// scroll to a given center point (in the bounds coordinates of the ctv)
	// convert the point to the document view coord system
	NSPoint centerPointInOwnSystem = [self convertPoint:center fromView:contentView];
	[self scrollCenterPoint:centerPointInOwnSystem];
	
}

- (void)scrollCenterPoint:(NSPoint)center
{
	// scroll to a given center point (in the bounds coordinates of the receiver)
	
	// compute origin point based on the given center and the document visible rect
	NSRect visibleRect = [[self enclosingScrollView] documentVisibleRect];
	NSPoint origin = NSMakePoint(center.x - visibleRect.size.width/2, center.y - visibleRect.size.height/2);
	[self scrollPoint:origin];
}



#pragma mark Layout guidelines
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Layout guidelines
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)addAlignLayoutElementForView:(LightTableImageView *)theView
{
	LightTableAlignLayoutElement *el = [[[LightTableAlignLayoutElement alloc] init] autorelease];
	el.associatedView = theView;
	[layoutElements addObject:el];
}

- (void)removeAlignLayoutElementForView:(LightTableImageView *)theView
{
	for (LightTableAlignLayoutElement *el in layoutElements) {
		if (el.associatedView == theView) {
			[layoutElements removeObject:el];
			return;
		}
	}
}

- (void)removeAllLayoutGuides
{
	// update only the areas needed
	for (LightTableAlignLayoutElement *element in toolView.layoutGuides) {
		[toolView setNeedsDisplayInRect:[element bounds]];
	}
	
	[toolView.layoutGuides removeAllObjects];
}

- (void)detectLayoutGuidesForFrameChange:(BOOL)move ofView:(LightTableImageView *)theView precision:(LayoutPrecision)precision keepAlive:(BOOL)keepAlive
{
	// Invoked by a view when it has been moved by user action.
	// Detects the possible layout guides between this view and every other view.
	
	// move == NO when a resize action changed the frame, YES in case of a move action. 
	// keepAlive == YES if the guides should remain active until the next update, NO if they should remain active until a certain time passes (mostly for keyboard actions)
	
	
	// ask the toolview to invalidate its old layout guide areas so the content view underneath can redraw itself if needed
	for (LightTableAlignLayoutElement *element in toolView.layoutGuides) {
		[toolView setNeedsDisplayInRect:[element bounds]];
	}
	
	// set the timeout of the guide beziers if needed
	if (!keepAlive) {
		[self disableGuideTimeout];
	} else {
		[self setGuideTimeout:1.0];
	}
	
	
	NSMutableArray *activeLayoutElements = [NSMutableArray array];
	for (LightTableAlignLayoutElement *element in layoutElements) {
		
		element.zoomLevel = 1.0;//self.zoomLevel;
		
		if ([element updateConstraintsWithFrameChange:move OfView:theView precision:precision]) {
			[activeLayoutElements addObject:element];
			// construct the constraint objects, for every active layout element.
			// these objects should constrain the ORIGIN of the imageview frame in question, if they are MOVE constraints.
			for (LightTableAlignConstraint *ct in element.constraints) {
				ct.zoomLevel = 1.0;//self.zoomLevel;
				[theView addAlignConstraint:ct];
			}
		}
	}
	
	
	// make sure every layout element is copied to the tool view, so it can draw the appropriate guide beziers.
	[toolView.layoutGuides removeAllObjects];
	[toolView.layoutGuides addObjectsFromArray:activeLayoutElements];
	
	// now invalidate the regions of the new layout guides
	for (LightTableAlignLayoutElement *element in toolView.layoutGuides) {
		[toolView setNeedsDisplayInRect:[element bounds]];
	}
	
}



- (void)setGuideTimeout:(NSTimeInterval)time
{
	// set the guides to timeout within a given time.
	// resets the internal NSTimer as needed
	if (guideTimeoutTimer) {
		// if it is not nil, it is guaranteed to be not released (by our code), so invalidate
		[guideTimeoutTimer invalidate];
	}
	guideTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(guideTimerDidEnd:) userInfo:nil repeats:NO];
	
}

- (void)disableGuideTimeout
{ 
	if (guideTimeoutTimer) {
		[guideTimeoutTimer invalidate];
		guideTimeoutTimer = nil;
	}
}

- (void)guideTimerDidEnd:(NSTimer *)timer
{
	// first and foremost, set our timer var to nil because the timer has been released
	guideTimeoutTimer = nil;
	
	[self removeAllLayoutGuides];
	
}



#pragma mark Selection rectangle
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Selection rectangle
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)updateSelectionRectangle:(NSRect)newRect
{
	[toolView setNeedsDisplayInRect:NSInsetRect(toolView.selectionRectangle, -5, -5)];
	
	// when NSZeroRect is passed, the selection rectangle needs to be erased (the dragging has ended)
	//NSBezierPath *path = NSEqualRects(newRect, NSZeroRect) ? nil : [NSBezierPath bezierPathWithRect:newRect];
	toolView.selectionRectangle = newRect;
	[toolView setNeedsDisplayInRect:NSInsetRect(newRect,-20,-20)];
}


#pragma mark Zooming + scrolling
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Zooming + scrolling
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setZoomLevel:(CGFloat)level
{
	// before adjusting anything, get our current center point
	NSPoint oldCenterPoint  = [self currentScrollCenterPoint];
	
	// Convert the browserZoom level taken from the Album entity to the Light Table zoomLevel.
	// The browserZoom ranges from 0 to 1, the LT zoomLevel ranges from 1 to 5.
	
	CGFloat adaptedLevel;
	adaptedLevel = level;
	adaptedLevel = adaptedLevel < 0 ? 0 : adaptedLevel;
	adaptedLevel = adaptedLevel > 1 ? 1 : adaptedLevel;
	
	adaptedLevel = 1 + (adaptedLevel * 5);
	//temp override: set zoom level to 1 until the model is OK
	//adaptedLevel = 1.0;
	
	//NSLog(@"setZoomLevel %f --> adaptedLevel %f", level, adaptedLevel);

	
	// adjust zoom level precision (rounded to nearest .02) so we don't get interpolation of e.g. grid and controls.
	if (zoomLevel == ((int)(adaptedLevel * 50.0)) / 50.0)
	{
		return;
	}
	zoomLevel = ((int)(adaptedLevel * 50.0)) / 50.0;
	contentView.zoomLevel = zoomLevel;
		
	[self setContentViewBounds:[contentView bounds]];
	
	if ([[lightTableImageArrayController selectedObjects] count] == 0) {
		
		// now set our center point to the point that was the center point before changing frames/bounds, if we have no selection.
		[self scrollCenterPoint:oldCenterPoint];
		
	} else {
		
		// we have a selection so the user will probably want to scroll to zoom that selection.
		// if we scroll to a point somewhere on the vector between the old scroll center and the selection bounding box center, we will reach the bounding box point at infinite zoom.
		// if we take a value far enough on the vector, the point we'll reach will be a good approximation of the desired center.
		NSMutableArray *theViews = [NSMutableArray array];
			
		for (LightTableImage *image in [lightTableImageArrayController selectedObjects]) {
			id imageView;
			if ((imageView = [contentView imageViewForImage:image]))
				[theViews addObject:imageView];
		}
				
		if ([theViews count] > 0) {
			
			
			NSRect boundingBox = [contentView boundingBoxForImageViews:theViews];
			boundingBox = [self convertRect:boundingBox fromView:contentView];
			NSPoint boundingBoxCenter = MGRectCenter(boundingBox);
			NSPoint newScrollPoint = MGPointLinearCombination(oldCenterPoint, boundingBoxCenter, 0.9);		// 0.6 means closer to the first point than to the second
			
			[self scrollCenterPoint:newScrollPoint];
			
			
		} else {
			
			// if no image has an associated view (e.g. at init of our LT), then still use the old center point...
			[self scrollCenterPoint:oldCenterPoint];
			
		}

		
		
	}
	
	
	//[self needsRecursiveDisplay];
		
}

- (CGFloat)zoomLevel
{
	return zoomLevel;
}

#pragma mark View printing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// View size View printing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (IBAction)createCollage:(id)sender
{
	[collageController showCollagePreviewWithImage:[self compositeImageWithUsedRect]];
	
}

- (NSImage *)compositeImageWithUsedRect
{
	return [contentView compositeImageFromRect:NSZeroRect constrainToImageBounds:NO sideMargins:NSMakeSize(10,10)];
}


- (void)saveCollagePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo
{
	if (returnCode == NSCancelButton) return;
	
	NSImage *composite;
	
	if (!contextInfo) {
		
		composite = [self compositeImageWithUsedRect];
		
	} else {
		
		NSRect selectionRect = [(NSValue *)contextInfo rectValue];
		[(NSValue *)contextInfo release];
		
		composite = [contentView compositeImageFromRect:selectionRect constrainToImageBounds:YES sideMargins:NSMakeSize(0,0)];
		
	}
	
	NSData *tiffData  = [composite TIFFRepresentation];
	[tiffData writeToFile:[sheet filename] atomically:YES];
}

- (IBAction)toggleOpacityHUD:(id)sender
{
	if ([opacityHUD isVisible]) {
		
		[opacityHUD orderOut:self];
		
	} else {
		
		[opacityHUD orderFront:self];
		
	}
}

#pragma mark Image Layout
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image layout and reordering (programmatically)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)resizeToMatchSelected:(id)sender
{
	[contentView resizeToMatchSelected:sender];
}

- (IBAction)resizeToRealSize:(id)sender
{
	[contentView resizeToRealSize:sender];
}

- (IBAction)resizeToMatchSelectedUsingMagnification:(id)sender
{
	[contentView resizeToMatchSelectedUsingMagnification:sender];
}

- (IBAction)arrangeAllInGrid:(id)sender
{
	NSMutableArray *views = [NSMutableArray array];
	
	NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"image.name" ascending:YES]];
	
	for (LightTableImage *image in [[lightTableImageArrayController arrangedObjects] sortedArrayUsingDescriptors:sortDescriptors]) {
		[views addObject:[contentView imageViewForImage:image]];
	}
	
	[contentView arrangeImageViewsInGrid:views animated:YES];
}


- (IBAction)resizeAndArrange:(id)sender
{
	[self resizeToMatchSelected:sender];
	// wait until animation and bindings updates have been done, then do the scond action.
	[self performSelector:@selector(arrangeAllInGrid:) withObject:sender afterDelay:0.4];
}


- (IBAction)distributeHorizontally:(id)sender
{
	[contentView distributeHorizontally:sender];
}

- (IBAction)distributeVertically:(id)sender
{
	[contentView distributeVertically:sender];
}


- (IBAction)alignTopHorizontally:(id)sender
{
	[contentView alignTopHorizontally:sender];
}

- (IBAction)alignTopVertically:(id)sender
{
	[contentView alignTopVertically:sender];
}

- (IBAction)alignBottomHorizontally:(id)sender
{
	[contentView alignBottomHorizontally:sender];
}

- (IBAction)alignBottomVertically:(id)sender
{
	[contentView alignBottomVertically:sender];
}	


#pragma mark Comment view
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Comment view
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (IBAction)cancelCommentView:(id)sender
{
	[contentView cancelCommentView:sender];
}

- (IBAction)commitCommentView:(id)sender
{
	[contentView commitCommentView:sender];
}

- (IBAction)showCommentViewForSelectedView:(id)sender
{
	[contentView showCommentViewForSelectedView:sender];
}

@end
