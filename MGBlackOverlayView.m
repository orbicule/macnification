//
//  MGBlackOverlayView.m
//  Filament
//
//  Created by Dennis Lorson on 18/11/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGBlackOverlayView.h"


@implementation MGBlackOverlayView

- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect 
{
	NSImage *image = [[[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"imageInfoBar" ofType:@"tiff"]] autorelease];
	[image drawInRect:[self bounds] fromRect:NSMakeRect(0, 0, [image size].width, [image size].height) operation:NSCompositeSourceOver fraction:0.8];
    //[[NSColor colorWithCalibratedWhite:0.0 alpha:0.5] set];
	
	//[[NSBezierPath bezierPathWithRect:[self bounds]] fill];
}

@end
