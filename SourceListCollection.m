//
//  SourceListCollection.m
//  Filament
//
//  Created by Peter Schols on 17/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SourceListCollection.h"
#import "MGLibrary.h"


@implementation SourceListCollection


- (NSImage *)iconImage; { return nil; }

- (id)parentGroup { return nil; }

- (BOOL)acceptsDroppedKeywords { return NO; }
- (BOOL)acceptsDroppedImages { return NO; }
- (BOOL)acceptsDroppedImagesFromFinder { return [[self valueForKey:@"rank"] intValue] == PROJECTS_COLLECTION_RANK; }
- (BOOL)canBeDragged { return NO; }
- (BOOL)canEdit { return NO; }


- (BOOL)canHaveAlbumAsChild:(Album *)proposedChild;
{
	return [[self valueForKey:@"rank"] intValue] == PROJECTS_COLLECTION_RANK;
}

@end
