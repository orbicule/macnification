//
//  CIFilterExtensions.h
//  Macnification
//
//  Created by Peter Schols on 02/03/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>

@interface CIFilter (CIFilterExtensions)

+ (CIImage *)filterImage:(CIImage *)original withFilters:(NSArray *)filters;

- (CIFilter *)filterByCopyingProperties;

- (NSDictionary *)dictionary;
- (void)setDictionary:(NSDictionary *)dict;

- (BOOL)isCropFilter;


@end
