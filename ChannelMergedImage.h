//
//  ChannelMergedImage.h
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>

@class Image;

@interface ChannelMergedImage : NSObject {
	
	Image *rImage, *gImage, *bImage;

	NSBitmapImageRep *mergedRep;
	
	BOOL isDirty;
	
	NSInteger imageVersion;
}

@property (readwrite, retain) Image *rImage, *gImage, *bImage;

- (NSBitmapImageRep *)mergedRep;
- (void)setImageR:(Image *)r G:(Image *)g B:(Image *)b;


@end
