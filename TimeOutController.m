//
//  TimeOutController.m
//  Filament
//
//  Created by Peter Schols on 31/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "TimeOutController.h"
#import "MGAppDelegate.h"


@implementation TimeOutController


- (id)init {
    self = [super initWithWindowNibName:@"TimeOut"];
    return self;
}


- (void)dealloc
{
	
	[super dealloc];
}

- (void)awakeFromNib;
{
	// Set the onscreen window position
	[[self window] center];
	float adaptedY = [[self window] frame].origin.y * 0.4;
	NSPoint adaptedPoint = NSMakePoint([[self window] frame].origin.x, adaptedY);
	[[self window] setFrameOrigin:adaptedPoint];

	
	// Set the progressBar
	[progressBar setFloatValue:[self daysLeft]];
	[daysLeftField setStringValue:[NSString stringWithFormat:@"%d days left", (int)[self daysLeft]]];
	
	if ([self daysLeft] < 0) {
		[progressBar setFloatValue:0.0];
		[daysLeftField setStringValue:@"The trial period has expired. Click buy online to purchase Macnification"];
	}
}




- (IBAction)showWindow:(id)sender;
{
	// Show the timeout window, but not on the first day!
	if ([self daysLeft] < 30) {
		[super showWindow:self];
	}
}




- (IBAction)buyOnline:(id)sender;
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.macnification.com/store"]];
}



- (IBAction)enterLicenseKey:(id)sender;
{
	[[self window] close];
	[MGAppDelegateInstance showRegistrationPreferences:self];
}



- (NSInteger)daysLeft;
{
	NSFileManager *fm = [NSFileManager defaultManager];
	NSDate *libraryCreationDate = [[fm attributesOfItemAtPath:[@"~/Library/Application Support/Macnification/Macnification.db" stringByExpandingTildeInPath] error:nil] objectForKey:NSFileCreationDate];
	
	double timeIntervalInSeconds = [[NSDate date] timeIntervalSinceDate:libraryCreationDate];
	NSInteger timeIntervalInDays = (NSInteger)roundf(timeIntervalInSeconds / 86400.00);
	return (30 - timeIntervalInDays);
}



@end
