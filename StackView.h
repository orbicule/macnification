//
//  StackTableView.h
//  StackTable
//
//	Main stack table view.  Contains the layer tree representing stacks and additional info.
//
//  Created by Dennis Lorson on 19/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <QTKit/QTKit.h>

@class StackLayer;
@class StackImageMainLayer;
@class StackImage;
@class MGContextualToolStrip;
@class MGEFImager;
@class ImageArrayController;


typedef enum _MGStackLayoutType
{
	
	MGTimeMachineStackLayout = 0,
	MGReorderStackLayout = 1,
	MGPerpendicularStackLayout = 2,
	MGBrowserStackLayout = 3
	
} MGStackLayoutType;

typedef enum _MGSliceControlPoint
{
	
	MGNoSliceControlPoint = 0,
	MGStartSliceControlPoint = 1,
	MGEndSliceControlPoint = 2
	
} MGSliceControlPoint;



@interface StackView : NSView <NSUserInterfaceValidations>
{
	// Controllers
	IBOutlet NSArrayController *stackArrayController;
	IBOutlet NSArrayController *stackImageArrayController;
	IBOutlet ImageArrayController *imageArrayController;
	
	// Helper objects
	MGEFImager *efImager;
	
	// Visual properties
	MGStackLayoutType layoutType;
	BOOL sliceModeOn;
	BOOL showReflections;
	BOOL animatesLayout;
	
	// Other collections
	NSMutableArray *imageLayers;			// the collection of all the ImageLayer sublayers.

	// Main layers
	CALayer *rootLayer, *backgroundLayer;		// the root layer of this view, contains the stack layer and others (info etc)
	CATiledLayer *slicePreviewLayer;
	CALayer *navigationForwardLayer;
	CALayer *navigationBackLayer;
	StackLayer *stackLayer;
	
	CALayer *leftArrowLayer, *rightArrowLayer, *upArrowLayer, *downArrowLayer;
	
	
	
	// IB Outlets : toolbar
	IBOutlet MGContextualToolStrip *toolStrip;
	IBOutlet NSView *layoutTypeControlContainer;
	IBOutlet NSButton *layoutSingleButton, *layoutTimeMachineButton, *layoutCoverFlowButton;
	
	NSButton *sliceButton, *saveSliceButton, *movieButton, *efiButton, *montageButton, *prevButton, *nextButton, *removeButton, *resetButton;
	
	
	// IB Outlets : movie
	IBOutlet QTMovieView *movieView;
	IBOutlet NSWindow *movieWindow;
	IBOutlet NSPopUpButton *movieSlideDurationPopUp;
	IBOutlet NSButton *movieSaveButton, *movieCancelButton;
	IBOutlet NSView *movieDurationView;
	IBOutlet NSProgressIndicator *movieIndicator;
	IBOutlet NSTextField *movieLoadingMessage;
	
	
	QTMovie *movie;		
	NSMutableArray *movieImages;														// temporary var to store movie images while the movie is composed
	CGFloat movieAspectRatio;
	

	
	CGImageRef fullResSlice;
	NSString *lastChosenSlicePath;
	
	IBOutlet NSWindow *saveSliceWindow;
	IBOutlet NSProgressIndicator *saveSliceIndicator;
	
	MGSliceControlPoint lastClickedControlPoint;
	NSPoint previousSliceStartPoint, previousSliceEndPoint;
	NSPoint sliceStartPoint, sliceEndPoint;										// the start and end points of the slice, in bezier layer coords.  These are not offsetted
																				// by the individual layer offsets yet!
	
	NSTimer *arrowTimeoutTimer;
	
	StackImageMainLayer *lastHitLayer;
	
	NSMutableArray *arrangedSublayers;
	
}

@property(nonatomic) BOOL showReflections;
@property(nonatomic) MGStackLayoutType layoutType;


- (void)initializeToolStripForLayoutType:(MGStackLayoutType)type;
- (void)setupLayout;


// Modification etc
- (IBAction)nextLayer:(id)sender;
- (IBAction)previousLayer:(id)sender;
- (IBAction)removeSelectedImage:(id)sender;

- (void)scrollBy:(NSInteger)scrollAmount;
- (void)removeLayer:(StackImageMainLayer *)layer;
- (void)addImageLayer:(StackImageMainLayer *)layer;
- (StackImageMainLayer *)imageLayerWithStackTableImage:(StackImage *)image;
- (NSArray *)sortedSublayers;


// Layout change
- (IBAction)switchStackLayout:(id)sender;
- (void)changeStackLayoutType:(MGStackLayoutType)newType;
- (void)updateControls;
- (void)updateStackLayout;



// Offset
- (void)initializeArrows;
- (void)setArrowTimeout:(NSTimeInterval)time;
- (void)disableArrowTimeout;
- (void)arrowTimerDidEnd:(NSTimer *)timer;
- (void)hideAllArrowLayers;
- (void)showUpArrowLayer;
- (void)showDownArrowLayer;
- (void)showLeftArrowLayer;
- (void)showRightArrowLayer;
- (IBAction)resetLayerOffsets:(id)sender;



// Slice
- (IBAction)toggleSliceTools:(id)sender;
- (IBAction)saveSliceAs:(id)sender;

- (void)updateBezierPathLayers;
- (void)refreshSliceLayer;
- (void)updateSlicePointsIfNeeded;
- (void)composeSliceInContext:(CGContextRef)ctx;
- (void)threaded_composeFullResSlice;
- (NSSize)dimensionsForCurrentSlice;
- (void)drawDotEndedLineFromPoint:(NSPoint)first toPoint:(NSPoint)second;
- (void)clearAllSliceResources;


// Movie
- (IBAction)createMovie:(id)sender;
- (IBAction)saveMovie:(id)sender;
- (IBAction)cancelMovie:(id)sender;
- (IBAction)changeMovieDuration:(id)sender;

- (void)updateMovieDuration:(CGFloat)newDurationPerImage;
- (void)composeMovie;
- (void)cleanupMovie;
- (void)loadMovieImages;
- (void)threaded_loadMovieImages;
- (void)loadMovieImagesDidEnd;



// EFI
- (IBAction)showEFISheet;


@end
