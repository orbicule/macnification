//
//  PSGradientView.h
//  Filament
//
//  Created by Peter Schols on 09/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface PSGradientView : NSView {
	
	NSColor *startColor, *endColor;

}

@property (nonatomic, retain) NSColor *startColor, *endColor;

@end
