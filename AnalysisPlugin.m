//
//  AnalysisPlugin.m
//  Filament
//
//  Created by Peter Schols on 13/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//
//
//  Plugins should be subclasses of AnalysisPlugin


#import "AnalysisPlugin.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"

@implementation AnalysisPlugin



+ (AnalysisPlugin *)analysisPlugin;
{
    return [[[self alloc] init] autorelease];
}



- (NSWindow *)analysisWindowForImages:(NSArray *)images;
{
	NSLog(@"Please subclass AnalysisPlugin for your custom plugin");	
	return nil;
}


- (IBAction)endSheet:(id)sender;
{
	[MGLibraryControllerInstance endPluginSheet:[pluginWindow retain]];
}


@end
