//
//  RegistrationController.h
//  Filament
//
//  Created by Peter Schols on 31/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface RegistrationController : NSWindowController {

	
	IBOutlet id licenseField;				// Field in which user enters license key

	IBOutlet id unRegisteredTextField;		// Message when unregistering
	IBOutlet id registeredTextField;		// Message when registering
	
	IBOutlet id tabPanel;

}


// Checking Registration
- (BOOL)isRegistered;
- (BOOL)isRegisteredOrIsWithinTrialPeriod;


// Changing Registration
- (IBAction)authorize:(id)sender;
- (IBAction)deAuthorize:(id)sender;
- (IBAction)lostLicenseKey:(id)sender;

- (void)createRegistration;
- (void)removeRegistration;


// Information needed for registration
- (NSString *)processedEtid;
- (NSString *)etid;
- (NSUInteger)daysLeft;
- (BOOL)networkReachableWithoutAnythingSpecialHappening;


@end
