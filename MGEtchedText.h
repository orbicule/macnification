/* EtchedText */

#import <Cocoa/Cocoa.h>

@interface MGEtchedText : NSTextField
{
}
+ (Class)cellClass;

-(void)setShadowColor:(NSColor *)color;

@end
