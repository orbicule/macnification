

#import "RoundedView.h"
#import "RolloverButton.h"

@implementation RoundedView


- (void)drawRect:(NSRect)rect;
{
   	[[NSImage imageNamed:@"toolb_bg"] compositeToPoint:NSMakePoint(0, 0) operation:NSCompositeSourceOver fraction:0.9];
	
}


- (void)closeAllTooltips;
{
	for (id view in [self subviews]) {
		if ([view respondsToSelector:@selector(closeToolTip)]) {
			[(RolloverButton *)view closeToolTip];	
		}
	}
}


- (void)dealloc {
    [super dealloc];
}



@end
