//
//  CaptureAdjustmentsController.h
//  Filament
//
//  Created by Dennis Lorson on 01/03/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@interface CaptureAdjustmentsController : NSObject 
{
	CIFilter *colorControlsFilter_;
	CIFilter *whitePointFilter_;
	CIFilter *exposureFilter_;

	CGFloat brightness_;
	CGFloat contrast_;
	CGFloat saturation_;
	CGFloat exposure_;
	
	BOOL useExposure_;
	
	NSPoint whitePoint_;
	NSColor *whiteColor_;
}

@property (nonatomic) CGFloat brightness;
@property (nonatomic) CGFloat contrast;
@property (nonatomic) CGFloat saturation;
@property (nonatomic) CGFloat exposure;

@property (nonatomic) NSPoint whitePoint;

- (void)resetDefaults;

- (void)setAutoWhitePoint:(BOOL)onOrOff;
- (void)setUsesExposure:(BOOL)onOrOff;

- (CIImage *)filterImage:(CIImage *)inImage;


@end
