//
//  NSObject_MGExtensions.h
//  Filament
//
//  Created by Dennis Lorson on 22/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (MGExtensions)

- (BOOL)isNull;


@end
