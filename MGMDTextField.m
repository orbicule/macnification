//
//  IFVerticallyExpandingTextfield.m
//  MetaDataView
//
//  Created by Dennis on 10/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//


#import "MGMDTextField.h"
#import "MGMDFieldView.h"
#import "QuartzCore/QuartzCore.h"
#import "ImageArrayController.h"
#import "MGMDConstants.h"



@implementation MGMDTextField


@synthesize raisedEditingActive, modelBindableValue;

- (id)initContentsFieldWithFrame:(NSRect)frame
{
	if ((self = [super initWithFrame:frame])) {
		
		[self setFocusRingType:NSFocusRingTypeNone];
		[self setAutoresizingMask:(NSViewWidthSizable | NSViewHeightSizable)];
		[self setFont:[NSFont systemFontOfSize:11]];
		[self setBordered:NO];
		[self setBezeled:NO];
		
		[[self cell] setDrawsBackground:NO];
		
	}
	
	return self;
}

- (void)bind:(NSString *)binding toObject:(id)observable withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"] && [observable isKindOfClass:[ImageArrayController class]]) {
		
		observableController = observable;
		observableKeyPath = [keyPath copy];
		
		NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithBool:NO], NSContinuouslyUpdatesValueBindingOption,
								 MGMDNoSelectionString, NSNoSelectionPlaceholderBindingOption,
								 MGMDMultipleValuesString, NSMultipleValuesPlaceholderBindingOption,
								 MGMDNullValueString, NSNullPlaceholderBindingOption, nil];
		
		
		
		[(ImageArrayController *)observable addManualObserver:self forKeyPath:keyPath];
		[observable addObserver:self forKeyPath:@"selection" options:0 context:nil];
		[super bind:@"value" toObject:self withKeyPath:@"modelBindableValue" options:options];
		
	}
	else {
		[super bind:binding toObject:observable withKeyPath:keyPath options:options];
	}
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"selection"]) {
		[self manualObserveValueForKeyPath:observableKeyPath ofObject:observableController];
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}


- (void)manualObserveValueForKeyPath:(NSString *)keypath ofObject:(NSString *)obj
{
	// set our model bindable property...for the textfield to be able to use a textvaluebinder without having to write unneeded code.
	id newVal = nil;
	
	if ([keypath isEqualToString:@"selection.pixelSizeDescription"]) {
		
		NSArray *images = [observableController selectedObjects];
		
		BOOL atLeastOneStack = NO;
		BOOL otherImagesToo = NO;
		
		for (id image in images) {
			
			if ([image isPartOfStack]) {
				atLeastOneStack = YES;
			} else {
				
				otherImagesToo = YES;
			}
		}
		
		if (atLeastOneStack && otherImagesToo)
			newVal = NSMultipleValuesMarker;
		else if (atLeastOneStack)
			newVal = [[images lastObject] pixelSizeDescription];
		
	}
	
	if (!newVal)
		newVal = [observableController valueForKeyPath:observableKeyPath];
	
	if (newVal != modelBindableValue) {
		[self willChangeValueForKey:@"modelBindableValue"];
		[modelBindableValue release];
		modelBindableValue = [newVal retain];
		[self didChangeValueForKey:@"modelBindableValue"];
		
		
	}

	if ([[self superview] isKindOfClass:[MGMDFieldView class]])
		[(MGMDFieldView *)[self superview] updateContentsSize];

	
}


- (void)setModelBindableValue:(id)val
{
	if (val != modelBindableValue) {
		
		[modelBindableValue release];
		modelBindableValue = [val copy];
		
		[observableController setValue:val forKeyPath:observableKeyPath];
	}

}


- (void) awakeFromNib 
{
	[self setFocusRingType:NSFocusRingTypeNone];
	[self setAutoresizingMask:(NSViewWidthSizable | NSViewHeightSizable)];
	[self setFont:[NSFont systemFontOfSize:11]];
	[self setBordered:NO];
	[self setBezeled:NO];
	
	[[self cell] setDrawsBackground:NO];
	
	
}

- (void)keyDown:(NSEvent *)theEvent
{
	//NSLog(@"key event");
	[super keyDown:theEvent];
	
}

- (void)setEnabled:(BOOL)flag
{
	[super setEnabled:flag];
	
	if (!flag) {
		self.raisedEditingActive = NO;
		// do this check because the view might not directly be embedded into a FieldView (this is the case with the experiment summary field)
		if ([[self superview] respondsToSelector:@selector(setRaisedEditingActive:)])
			((MGMDFieldView *)[self superview]).raisedEditingActive = NO;
	}
}


- (void)setRaisedEditingActive:(BOOL)flag
{
	raisedEditingActive = flag;

}

- (BOOL)resignFirstResponder
{	
	BOOL resign = [super resignFirstResponder];
	
	//NSLog(@"%@ resign %i", [self description], resign);

	
	self.raisedEditingActive = NO;
	((MGMDFieldView *)[self superview]).raisedEditingActive = NO;
	
	return resign;
}

- (BOOL)becomeFirstResponder
{
	BOOL become = [super becomeFirstResponder];
	
	//NSLog(@"%@ become %i", [self description], become);

	
	if ([self isEnabled]) {
		self.raisedEditingActive = YES;
		((MGMDFieldView *)[self superview]).raisedEditingActive = YES;
	}

	return become;
}

- (BOOL)acceptsFirstResponder 
{
    return YES;
}


- (NSString *)contentDescription
{
	if ([[self stringValue] length] < 30) return [self stringValue];
	
	NSString *substring = [[self stringValue] substringToIndex:29];
	
	return [substring stringByAppendingString:@"..."];
	
}

- (NSString *)contentValue
{
	return [self contentDescription];
	
}



@end