//
//  StackView (Layout)
//  StackTable
//
//  Created by Dennis Lorson on 19/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "StackView_Layout.h"

#import "StackLayer.h"
#import "StackImageMainLayer.h"
#import "StackImageReflectionLayer.h"


@implementation StackView (Layout)


NSInteger StackLayerSort(id obj1, id obj2, void *context);

- (void)setAnimatesLayout:(BOOL)flag;
{
	animatesLayout = flag;
}

- (void)setNeedsLayout;
{
	[self layoutSublayersOfLayer:stackLayer];
}

- (void)layoutSublayersOfLayer:(CALayer *)layer
{
	if (![layer isKindOfClass:[StackLayer class]])
		return;
	
	[CATransaction begin];
	
	if (!animatesLayout)
		[CATransaction setValue:[NSNumber numberWithBool:YES] forKey:kCATransactionDisableActions];
	
	// this variable will hold the size so we don't have to load it for each image (this assumes all images have the same dimensions)
	// otherwise there can be a long delay for ~500 images.
	CGSize imageSizeInLayer = CGSizeMake(0, 0);
	
	// layout all of the layers with a set distance between them (in z-direction)
	
	// used to adjust image layer layout to the stacklayer size.
	CGSize stackSize = layer.frame.size;
	
	// get the selectedLayer (which should be unique)
	StackImageMainLayer *stackSelectedLayer = ((StackLayer *)layer).selectedLayer;
	
	// sort the layers using name or creation date.
	//NSSortDescriptor *desc = [self activeSortDescriptor];
	NSArray *sortedLayers = [self arrangedStackImagelayers];//[[layer sublayers] sortedArrayUsingDescriptors:[NSArray arrayWithObject:desc]];
	
	// offset the index number by the index of the selected layer so the selected layer is frontmost/on zero position.
	int currentIndex = -[sortedLayers indexOfObject:stackSelectedLayer];
	
	CALayer *lastLayer = [sortedLayers lastObject];
	CALayer *lastLayerMinusOne = [sortedLayers count] > 1 ? [sortedLayers objectAtIndex:[sortedLayers count] - 2] : nil;
	
	[CATransaction setValue:[NSNumber numberWithFloat:0.25] forKey:kCATransactionAnimationDuration];
	
	for (StackImageMainLayer *sublayer in sortedLayers) {
		
		NSInteger lowestVisibleIndex = -10;
		NSInteger highestVisibleIndex = 20;
		
		BOOL isVisibleLayer = (currentIndex > lowestVisibleIndex && currentIndex < highestVisibleIndex);
		BOOL shouldSetGeometryOfLayer = (currentIndex > lowestVisibleIndex - 20 && currentIndex < highestVisibleIndex + 20);

		// set the contents visibility of every layer
		sublayer.contentsVisible = isVisibleLayer;		
		
		
		CGFloat layerOpacity = 1.0;
	
		
		//don't render images too far away
		NSInteger framesToFade = 6;
		
		if (currentIndex > highestVisibleIndex - framesToFade)
			layerOpacity = fmax(0.0, (highestVisibleIndex - (CGFloat)currentIndex)/(CGFloat)framesToFade);
		
		if (currentIndex < lowestVisibleIndex + framesToFade)
			layerOpacity = fmax(0.0, (labs(lowestVisibleIndex) - abs(currentIndex))/framesToFade);
		
		
		
		if (self.layoutType == MGPerpendicularStackLayout) {
			
			// index == 0, 1, 2 => 50% op
			// index == 3 => 100% op
			// index > 3 => 0 % op
			switch (currentIndex) {
					
				case 0:
					layerOpacity = 0.5;
					if (sublayer == lastLayer) layerOpacity = 1.0;
					if (sublayer == lastLayerMinusOne) layerOpacity = 0.7;
					break;
					case 1:
					layerOpacity = 0.7;
					if (sublayer == lastLayer) layerOpacity = 1.0;
					break;
					case 2:
					layerOpacity = 1.0;
					break;
					default:
					layerOpacity = 0.0;
					break;
			}
			
			
		} else if (self.layoutType == MGTimeMachineStackLayout) {
		
			if (currentIndex < 0) {
				layerOpacity = 0.0;
			}
			
		} else if (self.layoutType == MGReorderStackLayout) {
			
			// no changes
			
		}
		

		
		
		CGRect frame;
				
		// the selectedLayer indent (it jumps out of the stack when reordering mode) is dependent on the stacklayer size, in order to keep visible and consistent with other dimensions.
		CGFloat selectedLayerIndent = -1050 * fmin(1.2, stackSize.height / 400.0);
		
		if (imageSizeInLayer.width == 0)
			imageSizeInLayer = [self sizeOfImageLayer:sublayer inLayer:layer];
		frame.size = imageSizeInLayer;
		
		frame.origin.x = (currentIndex == 0 && self.layoutType == MGReorderStackLayout ? selectedLayerIndent : 0) + (layer.bounds.size.width - frame.size.width)/2;
		frame.origin.y = 0;
		
		NSPoint layerOffset = ((StackImageMainLayer *)sublayer).layerOffset;
		// need to multiply the offset ( which is a percentage of the layer dimensions) with the layer dims
		layerOffset.x *= frame.size.width;
		layerOffset.y *= frame.size.height;
		
		// do not display the layer offset on the selected image when reordering, this does not look good.
		if (self.layoutType != MGReorderStackLayout || currentIndex != 0) {
			
			frame.origin.x += layerOffset.x;
			frame.origin.y += layerOffset.y;
		}

		if (shouldSetGeometryOfLayer)
			sublayer.frame = frame;
		
		
		
		
		
		CATransform3D sublayerTransform = CATransform3DIdentity;
		
		// transform for the selected image in reordering mode
		if (currentIndex == 0 && self.layoutType == MGReorderStackLayout) {
			
			sublayerTransform = CATransform3DConcat(CATransform3DMakeRotation(-0.55, 0, 1, 0), sublayerTransform);		// apply a rotation
			
			sublayerTransform = CATransform3DConcat(CATransform3DMakeTranslation(0, 0, 350), sublayerTransform);	// position the layer correctly in the coordinate space
			
			// this transform keeps oscillating the selected image when loading other images although it is constant here, so temp. disabled
			
		} else {
			
			if (self.layoutType == MGReorderStackLayout)
				sublayerTransform = CATransform3DConcat(CATransform3DMakeRotation(-0.15, 0, 1, 0), sublayerTransform);
			
		}
		
		sublayer.transform = sublayerTransform;

		
		
		
		// reflection
		
		// change the reflection layer position and size!
		if (sublayer.reflectionLayer && shouldSetGeometryOfLayer) {
			CGFloat frac = 0.3;
			CGFloat glassThickness = 4.0 * sublayer.frame.size.height / 400;
			
			CGRect frame = sublayer.reflectionLayer.frame;
			frame.size.height = sublayer.frame.size.height * frac;
			frame.size.width = sublayer.bounds.size.width;
			frame.origin.y = - frame.size.height - glassThickness - sublayer.frame.origin.y;
			sublayer.reflectionLayer.frame = frame;
			
			// the higher the layer, the more transparent the reflection
			sublayer.reflectionLayer.opacity = 1.0 - sublayer.frame.origin.y / (180 * sublayer.frame.size.width / 800);
			
		}
		
		sublayer.opacity = layerOpacity;
		sublayer.reflectionLayer.opacity = layerOpacity * sublayer.reflectionLayer.opacity;
		
		// zPos is dependent on both the stack width (to keep approx. the same amt of items in view) and height (because space inbetween ~ layer size ~ stack height)
		// hwever, stop at a certain maximum.
		if (shouldSetGeometryOfLayer)
			sublayer.zPosition = -currentIndex * 180 * fmin(stackSize.width / 700.0 * stackSize.height / 800.0, 1.2);
		
		
		currentIndex++;
	}
	
	[CATransaction commit];

}

#pragma mark -
#pragma mark Sorting

- (void)rearrangeStackImageLayers
{
	[arrangedSublayers release];
	arrangedSublayers = nil;
}

- (NSArray *)arrangedStackImagelayers
{
	if (arrangedSublayers)
		return arrangedSublayers;
	
	// returns a list of the stack image sublayers sorted in the same manner as the stackImageArrayController objects.
	NSArray *sortedStackImages = [stackImageArrayController arrangedObjects];
	
	NSArray *layers = [stackLayer sublayers];
	
	NSArray *sortedLayers = [layers sortedArrayUsingFunction:StackLayerSort context:sortedStackImages];
	
	arrangedSublayers = [sortedLayers mutableCopy];
	
	return arrangedSublayers;
}


NSInteger StackLayerSort(id obj1, id obj2, void *context)
{
    StackImageLayer *layer1 = (StackImageLayer *)obj1;
	StackImageLayer *layer2 = (StackImageLayer *)obj2;
	
	NSArray *sortedStackImages = (NSArray *)context;
	
	NSInteger index1 = [sortedStackImages indexOfObject:layer1.stackImage];
	NSInteger index2 = [sortedStackImages indexOfObject:layer2.stackImage];
	
	// if both indices are valid, compare them.
	// if one of the indices is NSNotFound, that layer is always smaller than the other
	// if both are NSNotFound, they are ordered the same.

	if (index1 != NSNotFound && index2 != NSNotFound) {
		if (index1 < index2)
			return NSOrderedAscending;
		else if (index1 > index2)
			return NSOrderedDescending;
		else
			return NSOrderedSame;
		
	}
	
	else if (index1 != NSNotFound)
		return NSOrderedAscending;
	
	else if (index2 != NSNotFound)
		return NSOrderedDescending;
	
	return NSOrderedSame;
}


#pragma mark -
#pragma mark Sizing

- (CGSize)maximumImageLayerSizeInStackLayer:(StackLayer *)layer
{
	return CGSizeMake(0.7 * layer.bounds.size.width, 0.7 * layer.bounds.size.height);
}

- (CGSize)sizeOfImageLayer:(StackImageMainLayer *)imageLayer inLayer:(CALayer *)layer;
{
	// returns the size of a layer, given its content image.  used by the layout manager to dynamically update stack image layer sizes when:
	//		* the new image layer is inserted
	//		* another image layer is inserted, that may require resizing of the existing layers (in order to keep the relative dimensions of the images constant)
	
	CGSize maxLayerSize = [self maximumImageLayerSizeInStackLayer:(StackLayer *)layer];
	
	NSInteger imageHeight, imageWidth;
	
	NSSize fullImageSize = [imageLayer fullImageSize];

		imageWidth = fullImageSize.width;
		imageHeight = fullImageSize.height;
	
	// we need to determine the layer size that just fits the image (in aspect ratio) while resizing if needed.
	// first, get the largest height and width dimensions.  These are restrictive and the largest of the two will determine actual pixel to layer pixel ratio.
	
	// include the given image because it will be inserted soon!
	NSInteger maxHeight = imageHeight;
	NSInteger maxWidth = imageWidth;
	
	// ! assume every image has the same dimensions
	CGFloat pixelRatio = 1.0;
    if (maxLayerSize.width > 0 && maxLayerSize.height > 0)
        pixelRatio = fmax((CGFloat)maxHeight/maxLayerSize.height, (CGFloat)maxWidth/maxLayerSize.width);
	
	// every image needs to adhere to this pixel ratio, so we use this value to determine other layer sizes.
	// since the pixel ratio is the maximum scale-down factor that is needed to display everything in the req. bounds, we only need to
	// scale width and height of the given image down, without worrying if any of the dimensions will exceed the layer.
	//NSLog(@"new size of layer %f, %f", (CGFloat)imageWidth/pixelRatio, (CGFloat)imageHeight/pixelRatio);
	
    if (pixelRatio == 0)
        pixelRatio = 1;
	
	return CGSizeMake((CGFloat)imageWidth/pixelRatio, (CGFloat)imageHeight/pixelRatio);
	
	
}

- (CGSize)placeholderLayerSizeInLayer:(CALayer *)layer
{
	return [self maximumImageLayerSizeInStackLayer:(StackLayer *)layer];
}



@end
