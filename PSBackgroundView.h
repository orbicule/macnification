//
//  PSBackgroundView.h
//  Filament
//
//  Created by Peter Schols on 16/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface PSBackgroundView : NSView {
	NSColor *color;
}

@property (retain) NSColor *color;



@end
