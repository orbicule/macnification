//
//  DM3Importer.h
//  Filament
//
//  Created by Dennis Lorson on 11/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MGByteAccessTools.h"
#import "MGImageFormats.h"
#import "MGImageFileImporter.h"




@interface MGDM3FileImporter : MGImageFileImporter 
{
	
	
	BOOL useGatanMinMax;
	BOOL littleEndian;
	
	// this is the index of the largest (= most likely to be the non-thumbnail) image
	// DM3 can include multiple images, though usually only one is desired
	int chosenImage;
	
	int curGroupLevel;
	int *curGroupAtLevelX;
	NSString **curGroupNameAtLevelX;		// a NSString array
	
	int *curTagAtLevelX;
	NSString *curTagName;
	
	NSMutableArray *storedTags;
	NSMutableDictionary *tagDict;
	
}





@end
