//
//  CaptureQTDevice.h
//  Filament
//
//  Wrapper around an QTKit.framework-based device (mostly motion-based cameras and specialized devices)
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>

#import "CaptureDevice.h"
#import "UVCCameraControl.h"

@class CaptureOpenGLPreview;

@interface CaptureQTDevice : CaptureDevice 
{
	
	QTCaptureDevice *originalDevice_;
	QTCaptureSession *session_;
	
	QTCaptureDeviceInput *captureInput_;
	QTCaptureDecompressedVideoOutput *captureOutput_;
	
	BOOL shouldTakePicture_;
	
	CaptureOpenGLPreview *preview_;
    
    UVCCameraControl *control_;
    
}

- (id)initWithDevice:(QTCaptureDevice *)device;
- (NSString *)uniqueID;

@end
