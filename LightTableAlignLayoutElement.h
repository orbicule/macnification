//
//  LightTableAlignLayoutElement.h
//  LightTable
//
//  This class represents, computes and draws the possible layout guides between its associated view and a given view (often a view that was just moved)
//  It also provides the constraint object, used by the moved view to determine if it should "snap" to a certain position.
//  When asked, it will check if any alignment guides are appriopriate between the associated objects (i.e., there is some alignment between them),
//  and save those alignment guide images to draw them later on when asked.
//
//  Created by Dennis on 18/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class LightTableImageView;
@class LightTableAlignConstraint;


// enum for a direction.
enum
{
	VerticalDirection = 1,
	HorizontalDirection = 2
};

typedef NSInteger Direction;


// enum for the precision that determines the threshold from which a guide is applied.
enum
{
	StandardPrecision = 1,
	HighPrecision = 2,
	StandardResizePrecision = 3
};

typedef NSInteger LayoutPrecision;




@interface LightTableAlignLayoutElement : NSObject 
{

	LightTableImageView *associatedView;				// weak references to the view this element computes layout guides for.
	NSMutableArray *guideBeziers;						// the collection of stored guid beziers this element will draw the next time it is asked to.
	
	NSMutableArray *constraints;						// a collection of imposed constraints on the view that was last moved; the light table view will ask for these after they have been computed.
	
	CGFloat zoomLevel;
}

@property(assign, readwrite) LightTableImageView *associatedView;
@property(retain, readwrite) NSMutableArray *constraints;
@property(readwrite) CGFloat zoomLevel;


- (BOOL)updateConstraintsWithFrameChange:(BOOL)move OfView:(LightTableImageView *)theView precision:(LayoutPrecision)precision;
- (void)drawWithLineWidth:(CGFloat)width inView:(NSView *)view;
- (NSRect)bounds;



@end
