//
//  MGHUDPopUpButtonCell.m
//  Filament
//
//  Created by Dennis Lorson on 26/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDPopUpButtonCell.h"
#import "NSAttributedString_MGExtensions.h"

@implementation MGHUDPopUpButtonCell


-(void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	NSString *state =  @"";
	NSCompositingOperation comp = [self isHighlighted] ? NSCompositePlusLighter : NSCompositeSourceOver;
	CGFloat alpha = [self isEnabled] ? 1.0 : 0.35;
	NSImage *leftImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_popup_left%@.tiff", state]];
	NSImage *fillImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_popup_middle%@", state]];
	NSImage *rightImage = [NSImage imageNamed:[NSString stringWithFormat:@"hud_popup_right%@", state]];
	
	NSSize size = [leftImage size];
	float addX = size.width / 2.0;
	float y = NSMaxY(cellFrame) - (cellFrame.size.height-size.height)/2.0;
	y = floor(y);
	float x = cellFrame.origin.x+addX;
	x = floor(x);
	float fillX = x + size.width;
	float fillWidth = cellFrame.size.width - size.width - addX;
	
	[leftImage compositeToPoint:NSMakePoint(x, y) operation:comp fraction:alpha];
	
	size = [rightImage size];
	addX = size.width / 2.0;
	x = NSMaxX(cellFrame) - size.width - addX;
	x = floor(x);
	fillWidth -= size.width+addX;
	
	[rightImage compositeToPoint:NSMakePoint(x, y) operation:comp fraction:alpha];
	
	[fillImage setScalesWhenResized:YES];
	[fillImage setSize:NSMakeSize(fillWidth, [fillImage size].height)];
	[fillImage compositeToPoint:NSMakePoint(fillX, y) operation:comp fraction:alpha];
	
	[self drawInteriorWithFrame:cellFrame inView:controlView];
}

- (NSRect)titleRectForBounds:(NSRect)bounds;
{
	return NSOffsetRect([super titleRectForBounds:bounds], 0, 0);
}

-(void)drawTitleWithFrame:(NSRect)frame inView:(NSView *)view
{
    NSDictionary *props = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSFont fontWithName:@"LucidaGrande" size:11.0], NSFontAttributeName,
                           [NSColor whiteColor], NSForegroundColorAttributeName, nil];
    
    NSAttributedString *attrStr = [[self attributedTitle] attributedStringByAddingOrReplacingAttributes:props];
    
	frame = [self titleRectForBounds:frame];
	[super drawTitle:attrStr withFrame:frame inView:view];
}



@end
