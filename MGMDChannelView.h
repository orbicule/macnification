//
//  MGMDChannelView.h
//  MetaData
//
//  Created by Dennis Lorson on 24/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MGMDView.h"


@interface MGMDChannelView : NSView <MGMDManualObserver>  {

	NSMutableArray *channelAttributeKeys, *channelAttributeNames;
	
	IBOutlet NSTableView *channelAttributeTable;
	IBOutlet NSPopUpButton *channelPopUp;
	
	IBOutlet NSFormatter *gainFormatter, *powerFormatter, *offsetFormatter, *pinholeSizeFormatter, *wavelengthFormatter;
	
	IBOutlet NSButton *addButton, *removeButton;
	
	NSMutableDictionary *formatters;
	
	id observableController;
	NSString *observableKeyPath;
	
	NSString *channelName;
	
	BOOL subviewsEnabled;
	
	NSArray *channelsForCurrentSelection;							// all channel objects that are related to at least one of the selected images, and that have [self channelName] as name
	
	
}

@property(readwrite, retain) NSArray *channelsForCurrentSelection;


- (void)setFormatter:(NSFormatter *)formatter forKey:(NSString *)key;

- (IBAction)changeChannelName:(id)sender;
- (void)setChannelName:(NSString *)aName;
- (NSString *)channelName;

- (NSString *)uniqueChannelNameForSuggestedName:(NSString *)suggestedName;

- (IBAction)addChannel:(id)sender;
- (IBAction)removeSelectedChannel:(id)sender;

- (void)updateChannelsForCurrentSelection;

- (id)typeWithKey:(NSString *)key fromString:(NSString *)original;
- (NSString *)stringWithKey:(NSString *)key fromType:(id)original;

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView;
- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex;
- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex;
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex;

- (NSString *)contentDescription;
- (NSString *)contentValue;


@end
