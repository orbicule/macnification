//
//  Image.h
//  Macnification
//
//  Created by Peter Schols on 30/10/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>
#import <ImageKit/ImageKit.h>
#import "Album.h"
#import "KSExtensibleManagedObject.h"

@interface Image : KSExtensibleManagedObject 
{

	NSUInteger browserVersion;
	
	NSString *colorSpaceName;		// cached variable
	NSSize pixelSize;				// cached variable
	
	CIFilter *thresholdCompositeFilter;
		
}


// Related to stack - image difference
- (int)numberOfContainedImages;
- (Image *)displayImage;		// <self> for Image, first image for Stack


- (NSString *)colorSpaceName;

// Housekeeping
- (void)prepareForDeletion;
- (void)removeExperimenter;
- (void)removeInstrument;
- (void)removeAllStackImages;
- (void)removeAllLightTableImages;
- (void)removeExperiment;


// Paths

- (NSString *)originalImagePathExpanded:(BOOL)expanded;
- (NSString *)editedImagePathExpanded:(BOOL)expanded;
- (NSString *)activeImagePathExpanded:(BOOL)expanded;

- (NSString *)expandOriginalDataPath:(NSString *)path;
- (NSString *)expandEditedDataPath:(NSString *)path;

- (NSString *)applyOriginalImagePathWithFileName:(NSString *)name extension:(NSString *)ext;


- (void)updateThumbnail;


// Getting the image data
- (NSImage *)image;
- (CIImage *)ciImage;
- (NSData *)imageData;
- (CGImageRef)imageRef;
- (NSBitmapImageRep *)imageRep;


// Getting the filtered image data
- (NSImage *)filteredImage;
- (CIImage *)filteredCIImage;
- (NSData *)filteredImageData;
- (CGImageRef)filteredImageRef;
- (NSBitmapImageRep *)filteredImageRep;
- (CIImage *)filterImage:(CIImage *)original;

// thumbnails
- (NSImage *)thumbnail;
- (CIImage *)filteredThumbnail;


// Image properties
- (NSSize)pixelSize;
- (NSString *)pixelSizeDescription;


// Temporary image paths for drag and drop (mini browser)
- (NSString *)temporaryPathToFilteredImage;
- (NSString *)temporaryPathToFilteredImageWithScaleBar;


// Filters
- (void)updateDeprecatedFilters;
- (NSArray *)ciFilters;
- (NSArray *)ciFiltersWithThreshold;
- (void)setCIFilters:(NSArray *)newCiFilters;
- (CIFilter *)ciFilterFromString:(NSString *)filterName withAttributes:(NSDictionary *)filterAttributes;
- (BOOL)hasFilters;
- (void)setThresholdCompositeFilter:(CIFilter *)filter;
- (void)copyFiltersFromImage:(Image *)image;
- (void)removeAllFilters;

// ROI and calibration
- (float)realLengthForPixelLength:(float)pixelLength;
- (float)realAreaForPixelArea:(float)pixelArea;
- (id)addROI:(NSString *)roiType startPoint:(NSPoint)startPoint endPoint:(NSPoint)endPoint;
- (id)addROI:(NSString *)roiType withPoints:(NSArray *)pointsArray;
- (NSArray *)addROIsWithPoints:(NSArray *)roiDictionaries;
- (void)copyROIsFromImage:(Image *)image removeExisting:(BOOL)remove;
- (void)removeAllROIs;
- (int)roiCount;
- (void)calibratePoint:(NSPoint)point withPixels:(double)pixels perUnit:(NSString *)unit calibrationName:(NSString *)name;
- (BOOL)isCalibrated;
- (NSString *)allROIComments;
- (void)copyCalibrationFromImage:(Image *)image;
- (void)removeCalibration;

// Keywords
- (NSString *)allKeywords;
- (NSArray *)uniqueKeywordsArray;
- (NSString *)listOfUniqueKeywords;
- (void)addKeywords:(NSArray *)keywords;
- (void)copyKeywordsFromImage:(Image *)image;
- (void)removeAllKeywords;


// Browser subtitles / utilities
- (NSString *)ratingString;
- (NSString *)scaleBarString;
- (NSString *)keywordString;
- (void)increaseBrowserVersion;


// Scale bar
- (void)addScaleBarWithProperties:(NSDictionary *)properties;
- (NSBitmapImageRep *)filteredImageRepWithScaleBar;
- (void)removeScaleBar;



// Stacks
- (BOOL)isPartOfStack;
- (Image *)stack;


// Metadata
- (NSDictionary *)metadata;
- (NSDictionary *)spotlightMetadata;


// External editing and analyzing
- (NSString *)pathForEditingApplication;
- (void)updateExternallyEditedImage;

- (NSString *)pathForAnalysisApplication;


// Non-destructive editing
- (void)newVersionFromOriginal;
- (void)duplicate;
- (void)revertToOriginal;

// Channels
- (NSArray *)splitChannels;



// Exporting
- (NSString *)exportToFolderAtPath:(NSString *)folderPath withOptions:(IKSaveOptions *)options;
- (NSString *)tabText;


// Spotlight
- (NSDictionary *)spotlightMetadata;
- (BOOL)hasBeenChanged;



// Accessors
- (NSUInteger)browserVersion;
- (void)setBrowserVersion:(NSUInteger)value;


- (NSDictionary *)customMetadataDict;
- (void)setCustomMetadataDict:(NSDictionary *)aDict;
- (void)setCustomMetadata:(NSString *)metadata forKey:(NSString *)key;



// Accessors for dynamic properties
@property(retain) NSString *name;
@property(retain) KSExtensibleManagedObject *instrument;


@end
