//
//  FullScreenImageArrayController.h
//  Filament
//
//  Created by Dennis Lorson on 24/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@class ImageArrayController;

@interface FullScreenImageArrayController : NSArrayController
{
	
	IBOutlet ImageArrayController *imageArrayController_;

}

@end
