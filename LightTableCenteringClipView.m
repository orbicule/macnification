//
//  LightTableCenteringClipView.m
//  Filament
//
//  Created by Dennis Lorson on 12/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "LightTableCenteringClipView.h"


@implementation LightTableCenteringClipView


- (void)drawRect:(NSRect)rect {
    
	[[NSColor colorWithCalibratedWhite:0.25 alpha:1.0] set];
	NSRectFill([self bounds]);
	
}

@end
