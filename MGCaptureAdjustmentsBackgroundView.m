//
//  MGCaptureAdjustmentsBackgroundView.m
//  Filament
//
//  Created by Dennis Lorson on 05/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGCaptureAdjustmentsBackgroundView.h"


@implementation MGCaptureAdjustmentsBackgroundView

- (void)drawRect:(NSRect)rect 
{
	NSRect destRect = NSInsetRect([self bounds], 1, 1);
	
	NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:destRect xRadius:5. yRadius:5.];
	
	//destRect.origin.y += 11;
	//destRect.size.height -= 10;
	
	NSBezierPath *path2 = [NSBezierPath bezierPathWithRect:destRect];
	
    [[NSColor colorWithCalibratedWhite:37./255. alpha:0.4] set];
	[path fill];
	[path2 fill];
	
}


@end
