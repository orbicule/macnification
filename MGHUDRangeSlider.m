//
//  MGHUDRangeSlider.m
//  Filament
//
//  Created by Dennis Lorson on 06/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDRangeSlider.h"
#import "MGWidgetAttachedWindow.h"

#define RANGE_MIN_PERCENT .04

@implementation MGHUDRangeSlider

@synthesize hasMouseOver;


+ (id)defaultAnimationForKey:(NSString *)key {
    if ([key isEqualToString:@"rangeOverlayOpacity"]) {
        // By default, animate border color changes with simple linear interpolation to the new color value.
        CABasicAnimation *anim = [CABasicAnimation animation];
		[anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
		return anim;
    } else {
        // Defer to super's implementation for any keys we don't specifically handle.
        return [super defaultAnimationForKey:key];
    }
}

- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {
		


    }
    return self;
}


- (void)awakeFromNib
{
	trackImageR = [[NSImage imageNamed:@"hud_slider_right"] retain];
	trackImageM = [[NSImage imageNamed:@"hud_slider_middle"] retain];
	trackImageL = [[NSImage imageNamed:@"hud_slider_left"] retain];
	
	knob = [[NSImage imageNamed:@"hud_slider_tickmark_knob"] retain];


	[self setContinuous:YES];
	
	// set the sliders to valid values.
	lowerBound = 0;
	upperBound = 1;
	
	_minValue = 0;
	_maxValue = 1;
	
	rangeOverlayOpacity = 0.15;
	
	
	NSTrackingArea *area = [[[NSTrackingArea alloc] initWithRect:[self bounds] options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp)  owner:self userInfo:nil] autorelease];
	[self addTrackingArea:area];
	
	[self setNeedsDisplay:YES];
}

#pragma mark drawing


- (void)setTarget:(id)aTarget
{
	target = aTarget;	
}

- (void)setAction:(SEL)action
{
	
	selector = action;
}

- (void)setMouseOverAction:(SEL)action
{
	mouseOverSelector = action;
	
}

- (void)setMouseUpAction:(SEL)action
{
	mouseUpSelector = action;
	
}

- (void)setColorOverlay:(NSGradient *)overlay
{
	if (colorOverlay != overlay) {
		
		[colorOverlay release];
		colorOverlay = [overlay retain];
		
	}
	[self setNeedsDisplay:YES];
}

- (void)setRangeOverlayOpacity:(CGFloat)opacity
{
	rangeOverlayOpacity = opacity;
	[self setNeedsDisplay:YES];
}

- (CGFloat)rangeOverlayOpacity
{
	return rangeOverlayOpacity;
	
}

- (void)setRangeOverlayActive:(BOOL)active
{
	[[self animator] setRangeOverlayOpacity:active ? .4 : .2];	
}

- (void)drawRect:(NSRect)rect 
{
    // Drawing code here.
	[self drawTrack];
	[self drawLowerBoundSlider];
	[self drawUpperBoundSlider];
	
}


- (void)drawTrack
{
	NSRect bounds = [self bounds];
	NSSize knobSize = [knob size];
	
	NSSize size = [trackImageL size];
	float y = NSMidY(bounds) - size.height/2.0;
	float x = NSMinX(bounds) + knobSize.width / 2.0 - 2;
	[trackImageL compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver];
	
	
	CGFloat fillWidth = NSWidth(bounds) - (knobSize.width / 2.0 - 2) - size.width * 2 -  (knobSize.width / 2.0 - 2);
	x = NSMinX(bounds) + (knobSize.width / 2.0 - 2) + size.width;
	
	[trackImageM setScalesWhenResized:YES];
	[trackImageM setSize:NSMakeSize(fillWidth, [trackImageM size].height)];
	[trackImageM compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver];
	
	
	size = [trackImageR size];
	x = NSMaxX(bounds) - (knobSize.width / 2.0 - 2) - size.width;
	[trackImageR compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver];
	
	// background gradient
	NSRect gradientRect;
	gradientRect.origin.x = [self locationInBoundsForValue:[self minValue]] + 1;
	gradientRect.size.width = [self locationInBoundsForValue:[self maxValue]] + 1 - gradientRect.origin.x;
	gradientRect.origin.y = y+1;
	gradientRect.size.height = size.height-2;
	
	[colorOverlay drawInBezierPath:[NSBezierPath bezierPathWithRect:gradientRect] angle:0];

	// make the range lighter
	NSRect rangeRect;
	rangeRect.origin.x = [self locationInBoundsForValue:[self lowerBound]];
	rangeRect.size.width = [self locationInBoundsForValue:[self upperBound]] - rangeRect.origin.x;
	rangeRect.origin.y = y;//+1;
	rangeRect.size.height = size.height;//-2;
	
	NSBezierPath *range = [NSBezierPath bezierPathWithRect:rangeRect];
	[[NSColor colorWithCalibratedWhite:1.0 alpha:rangeOverlayOpacity] set];
	[range fill];
	
	return;
}

- (void)drawLowerBoundSlider
{
	
	float x = [self locationInBoundsForValue:[self lowerBound]] - [knob size].width / 2;
	float y = NSMidY([self bounds]) - [knob size].height/2.0;
	
	[knob compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:1];
	
	
}

- (void)drawUpperBoundSlider
{
	float x = [self locationInBoundsForValue:[self upperBound]] - [knob size].width / 2;
	float y = NSMidY([self bounds]) - [knob size].height/2.0;
	
	[knob compositeToPoint:NSMakePoint(x, y) operation:NSCompositeSourceOver fraction:1];
	
	
}

- (NSRect)trackRect
{
	CGFloat trackHeight = [trackImageR size].height;
	return NSInsetRect([self bounds], [knob size].width / 2.0, (NSHeight([self bounds]) - trackHeight)/2.);
	
}

#pragma mark Accessors

- (CGFloat)lowerBound
{
	return lowerBound;
}

- (CGFloat)upperBound
{
	return upperBound;
}

- (CGFloat)maxValue
{
	return _maxValue;
}

- (CGFloat)minValue
{
	return _minValue;
}

- (void)setMaxValue:(CGFloat)value
{
	_maxValue = value;
	upperBound = value;
	
}

- (void)setMinValue:(CGFloat)value
{
	_minValue = value;
	lowerBound = value;
}

- (void)setLowerBound:(CGFloat)bound
{
	lowerBound = MIN(bound, upperBound - RANGE_MIN_PERCENT * (_maxValue - _minValue));
	

	[self setNeedsDisplay:YES];
	
}

- (void)setUpperBound:(CGFloat)bound
{
	upperBound = MAX(bound, lowerBound + RANGE_MIN_PERCENT * (_maxValue - _minValue));

	
	[self setNeedsDisplay:YES];
	
}


#pragma mark Event handling


- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

- (void)mouseDown:(NSEvent *)theEvent
{
	NSPoint locationInBounds = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	CGFloat distanceToLower = fabs(locationInBounds.x - [self locationInBoundsForValue:[self lowerBound]]);
	CGFloat distanceToUpper = fabs(locationInBounds.x - [self locationInBoundsForValue:[self upperBound]]);
	
	if (distanceToLower < distanceToUpper)
		activeSliderKnob = MGLowerBoundSliderKnob;
	else
		activeSliderKnob = MGUpperBoundSliderKnob;

	switch (activeSliderKnob) {
			
		case MGNoSliderKnob:
			break;
		case MGLowerBoundSliderKnob:
			[self setLowerBound:[self valueForLocationInBounds:locationInBounds]];
			break;
		case MGUpperBoundSliderKnob:
			[self setUpperBound:[self valueForLocationInBounds:locationInBounds]];
			break;
	}
	
	[target performSelector:selector withObject:self];

	isDragging = YES;
	
	[self showTooltipWindowForKnob:activeSliderKnob animate:YES];
	
	[super mouseDown:theEvent];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	NSPoint locationInBounds = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	switch (activeSliderKnob) {
			
		case MGNoSliderKnob:
			break;
		case MGLowerBoundSliderKnob:
			[self setLowerBound:[self valueForLocationInBounds:locationInBounds]];
			break;
		case MGUpperBoundSliderKnob:
			[self setUpperBound:[self valueForLocationInBounds:locationInBounds]];
			break;
	}
	
	[self setNeedsDisplay:YES];
	
	if (isDragging)  // to prevent other sliders activating
		[self showTooltipWindowForKnob:activeSliderKnob animate:NO];
	
	[target performSelector:selector withObject:self];
	
	[super mouseDragged:theEvent];
}

- (void)mouseUp:(NSEvent *)theEvent
{
	activeSliderKnob = MGNoSliderKnob;
	
	isDragging = NO;
	
	if (suppressedExitEvent) {
		
		if (!NSPointInRect([self convertPoint:[theEvent locationInWindow] fromView:nil], [self bounds])) {
			[target performSelector:mouseOverSelector withObject:self];

			[self setRangeOverlayActive:NO];

		}

		
	}
	
	[target performSelector:mouseUpSelector withObject:self];

	
	[self closeToolTip];
	
	[super mouseUp:theEvent];
}



- (void)mouseEntered:(NSEvent *)theEvent
{
	//NSLog(@"entered %@", [self description]);
	[self setRangeOverlayActive:YES];
	self.hasMouseOver = YES;
	
	[target performSelector:mouseOverSelector withObject:self];
	
}

- (void)mouseExited:(NSEvent *)theEvent
{
	//NSLog(@"exited %@", [self description]);
	self.hasMouseOver = NO;
	
	if (isDragging) {
		
		suppressedExitEvent = YES;
		
	} else {
		[self setRangeOverlayActive:NO];
		[target performSelector:mouseOverSelector withObject:self];
		
	}
}

- (CGFloat)percentageForLocationInBounds:(NSPoint)location
{
	CGFloat x = location.x;
	
	NSRect sliderRangeRect = NSInsetRect([self bounds], [knob size].width / 2.0, 0);
	
	CGFloat percentage = (x - NSMinX(sliderRangeRect)) / sliderRangeRect.size.width;
	
	return MIN(MAX(percentage, 0), 1);
	
}

- (CGFloat)valueForLocationInBounds:(NSPoint)location
{
	return ([self maxValue] - [self minValue]) * [self percentageForLocationInBounds:location];
}

- (CGFloat)locationInBoundsForPercentage:(CGFloat)percentage
{
	NSRect sliderRangeRect = NSInsetRect([self bounds], [knob size].width / 2.0, 0);
	
	return NSMinX([self bounds]) + [knob size].width / 2.0 + sliderRangeRect.size.width * percentage;
	
}

- (CGFloat)locationInBoundsForValue:(CGFloat)value
{
	return [self locationInBoundsForPercentage:value/([self maxValue] - [self minValue])];
}



- (void)showTooltipWindowForKnob:(MGSliderKnob)aKnob animate:(BOOL)fadeIn
{
	
	NSPoint attachPoint;
	attachPoint.x = (aKnob == MGUpperBoundSliderKnob) ? [self locationInBoundsForValue:[self upperBound]] + 30 : [self locationInBoundsForValue:[self lowerBound]] - 30;
	attachPoint.y = NSMidY([self bounds]) - 26;
	
	NSPoint pointToAttach = [self convertPoint:attachPoint toView:nil];
	
	NSString *title = [NSString stringWithFormat:@"%ld", (long)((aKnob == MGUpperBoundSliderKnob) ? (NSInteger)([self upperBound]) : (NSInteger)([self lowerBound]))];
	
	// Create the text field we will use to draw the tooltip label
	if (!tooltipTextField) {
		
		tooltipTextField = [[NSTextField alloc] init];
		[tooltipTextField setEditable:NO];
		[tooltipTextField setSelectable:NO];
		[tooltipTextField setDrawsBackground:NO];
		[tooltipTextField setBordered:NO];
		[tooltipTextField setBezeled:NO];
		[tooltipTextField setAlignment:NSCenterTextAlignment];
		[tooltipTextField setFont:[NSFont systemFontOfSize:9]];
		[tooltipTextField setTextColor:[NSColor blackColor]];
		[tooltipTextField setFrameSize:NSMakeSize(26, 12)];
		
	}

	[tooltipTextField setStringValue:title];
	

	
	// Create the MAAttachedWindow
	if (!tooltipWindow) {
		
		
		NSView *enclosingView = [[[NSView alloc] initWithFrame:NSInsetRect([tooltipTextField frame], -1, 1)] autorelease];
		
		[tooltipTextField setFrameOrigin:NSMakePoint(1, -1)];
		[enclosingView addSubview:tooltipTextField];
		
		
		tooltipWindow = [[MGWidgetAttachedWindow alloc] initWithView:enclosingView attachedToPoint:pointToAttach 
															  inWindow:[self window] onSide:MAPositionTop atDistance:3.0];
		[tooltipWindow setReleasedWhenClosed:NO];
		[tooltipWindow setBorderWidth:0.0];
		
	}
	
	if (fadeIn) {
		[tooltipWindow setAlphaValue:0];
		[[tooltipWindow animator] setAlphaValue:1.0];
	} else {
		[tooltipWindow setAlphaValue:1.0];
	}
	
	[tooltipWindow setPoint:pointToAttach];
	
	// Show the window
	[tooltipWindow orderFront:self];
}


- (void)closeToolTip;
{
	if (tooltipWindow) {
		[[tooltipWindow animator] setAlphaValue:0.0];
		[tooltipWindow performSelector:@selector(close) withObject:nil afterDelay:.25];
	}
}



- (void)dealloc
{
	[trackImageR release]; 
	[trackImageM release];
	[trackImageL release];
    [tooltipWindow release];
    [tooltipTextField release];
	[knob release];
	
	[super dealloc];
}


@end
