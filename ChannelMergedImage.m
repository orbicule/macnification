//
//  ChannelMergedImage.m
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ChannelMergedImage.h"
#import "Image.h"
#import "NSBitmapImageRep_Conversion.h"

@implementation ChannelMergedImage

@synthesize rImage, gImage, bImage;


- (id) init
{
	self = [super init];
	if (self != nil) {
		
		imageVersion = 0;
		
	}
	return self;
}

- (NSUInteger) imageVersion;
{
	return imageVersion;
	
}

- (NSString *)imageUID
{
	return [NSString stringWithFormat:@"%ld", (unsigned long)[self hash]];
	
}


- (id)imageRepresentation
{
	if (!mergedRep || isDirty) {
		[mergedRep release];
		mergedRep = [[self mergedRep] retain];
	}
	
	isDirty = NO;
	
	return mergedRep;
}

- (NSString *)imageRepresentationType
{
	return IKImageBrowserNSBitmapImageRepresentationType;
}

- (NSString *)imageSubtitle
{
	return @"";
}

- (NSBitmapImageRep *)mergedRep
{
	NSBitmapImageRep *r = [self.rImage imageRep];
	if (!r) r = (id)[NSNull null];
	NSBitmapImageRep *g = [self.gImage imageRep];
	if (!g) g = (id)[NSNull null];
	NSBitmapImageRep *b = [self.bImage imageRep];
	if (!b) b = (id)[NSNull null];
	
	return [NSBitmapImageRep imageRepWithMergedChannels:[NSArray arrayWithObjects:r, g, b, nil]];
	
}


- (void)setImageR:(Image *)r G:(Image *)g B:(Image *)b
{
	isDirty = (r != self.rImage || g != self.gImage || b != self.bImage);
	imageVersion += isDirty ? 1 : 0;
	
	self.rImage = r;
	self.gImage = g;
	self.bImage = b;
}


- (void) dealloc
{
	self.rImage = nil;
	self.gImage = nil;
	self.bImage = nil;
	
	[mergedRep release];
	
	[super dealloc];
}



@end
