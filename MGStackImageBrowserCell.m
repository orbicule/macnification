//
//  MGMainImageBrowserCell.m
//  Filament
//
//  Created by Dennis Lorson on 10/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGStackImageBrowserCell.h"


@interface MGStackImageBrowserCell ()

- (NSBitmapImageRep *)badgeImageForNumber:(NSInteger)number;
- (NSInteger)badgeIndexForBadgeNumber:(NSInteger)number;

@end


@implementation MGStackImageBrowserCell


@synthesize numberOfImages = numberOfImages_;
@synthesize imageWidth = imageWidth_;


static NSMutableArray *BadgeImages = nil;


- (NSBitmapImageRep *)badgeImageForNumber:(NSInteger)number
{
	@synchronized ([self class]) {
	
		
		if (!BadgeImages) {
			
			BadgeImages = [[NSMutableArray array] retain];
			
			for (int i = 0; i < 4; i++) {
				NSImage *image = [NSImage imageNamed:[NSString stringWithFormat:@"dragBadge%i", i]];
				[BadgeImages addObject:[[image representations] objectAtIndex:0]];
			}
		}
			
		return [BadgeImages objectAtIndex:[self badgeIndexForBadgeNumber:number]];
	} 
	
	return nil;
}

- (NSInteger)badgeIndexForBadgeNumber:(NSInteger)number
{
	if (number >= 1000)
		return 3;
	if (number >= 100)
		return 2;
	if (number >= 10)
		return 1;
	
	return 0;
}


/*
- (CALayer *)layerForType:(NSString *)type;
{
	if (type == IKImageBrowserCellForegroundLayer) {
		
		//if([self cellState] != IKImageStateReady)
		//	return nil;
		
		NSBitmapImageRep *badgeImage = [self badgeImageForNumber:self.numberOfImages];
		
		NSRect imageFrame = [self frame];
		
		CALayer *layer = [CALayer layer]; 
		layer.frame = NSRectToCGRect(imageFrame);
		
		NSRect badgeFrame = CGRectMake(CGRectGetMinX(layer.bounds), CGRectGetMaxY(layer.bounds) - [badgeImage pixelsHigh], [badgeImage pixelsWide], [badgeImage pixelsHigh]);
		NSRect textFrame = badgeFrame;
		textFrame.origin.y -= 2;
		
		CALayer *badgeLayer = [CALayer layer];
		badgeLayer.contents = (id)[badgeImage CGImage];
		
		CATextLayer *textLayer = [CATextLayer layer];
		textLayer.string = [NSString stringWithFormat:@"%i", self.numberOfImages];
		textLayer.frame = textFrame;
		textLayer.fontSize = 15;
		textLayer.font = (CFTypeRef)[NSFont boldSystemFontOfSize:17];
		textLayer.alignmentMode = kCAAlignmentCenter;

		NSRect destFrame;
		destFrame.size.width = [badgeImage pixelsWide];
		destFrame.size.height = [badgeImage pixelsHigh];
		destFrame.origin = imageFrame.origin;
		badgeLayer.frame = badgeFrame;
		
		[layer addSublayer:badgeLayer];
		[layer addSublayer:textLayer];
		
		return layer;
		
	}	
	
	return [super layerForType:type];
}
*/

- (CALayer *)layerForType:(NSString *)type;
{
	if (type == IKImageBrowserCellForegroundLayer) {
		
		if([self cellState] != IKImageStateReady)
			return nil;
		
		NSRect frame = [self frame];		
		NSRect imageFrame = [self imageFrame];
		
		NSRect backgroundFrame = imageFrame;
		backgroundFrame.origin.x -= frame.origin.x;
		backgroundFrame.origin.y -= frame.origin.y;
		backgroundFrame.size.height = 15;;
		
		// account for images that do not have the full width of the placeholder
		backgroundFrame.origin.x += 0.5 * (1. - self.imageWidth) * backgroundFrame.size.width;
		backgroundFrame.size.width *= self.imageWidth;
		
		// inset a bit to avoid overlap with the image borders
		//backgroundFrame = NSInsetRect(backgroundFrame, 0.02 * backgroundFrame.size.width, 0.02 * backgroundFrame.size.width);
		
		CALayer *layer = [CALayer layer]; 
		layer.frame = NSRectToCGRect(frame);
		
		CALayer *backgroundLayer = [CALayer layer];
		backgroundLayer.backgroundColor = CGColorCreateGenericRGB(0, 0, 0, 0.5);
		backgroundLayer.frame = NSRectToCGRect(backgroundFrame);

		
		CATextLayer *textLayer = [CATextLayer layer];
		textLayer.string = [NSString stringWithFormat:@"%i", (int)self.numberOfImages];
		textLayer.frame = NSRectToCGRect(backgroundFrame);
		textLayer.fontSize = 10;
		textLayer.font = (CFTypeRef)[NSFont boldSystemFontOfSize:17];
		textLayer.alignmentMode = kCAAlignmentCenter;
		textLayer.foregroundColor = CGColorCreateGenericRGB(1, 1, 1, 1);
		
		[layer addSublayer:backgroundLayer];
		[layer addSublayer:textLayer];
		
		return layer;
		
	}	
	
	return [super layerForType:type];
}



@end
