//
//  MGGenericImageFileImporter.m
//  Filament
//
//  Created by Dennis Lorson on 19/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGGenericImageFileImporter.h"

#import "MGMetadataSet.h"
#import "MGTIFFMetadataReader.h"
#import "MGImageFileImporter_Private.h"


@interface MGGenericImageFileImporter ()

- (void)_createImageSourceIfNeeded;

- (int)_numberOfImageRepresentationsAssumingNoSubImporters;

@end


@implementation MGGenericImageFileImporter


+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	return YES;
}


+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:
			@"jpg", @"png", @"bmp", @"tif", @"tiff", @"jpeg", nil];
}


- (NSString *)destinationPathExtension;
{
	NSString *ext = [[filePath_ pathExtension] lowercaseString];
	
	if ([ext isEqual:@"png"] || [ext isEqual:@"jpeg"] || [ext isEqual:@"jpg"])
		return ext;
	
	return @"tiff";
}

- (BOOL)destinationNeedsCompression;
{
	return ![[self destinationPathExtension] isEqual:@"tiff"] && [[[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"retainCompressedFormats"] boolValue];
}


- (void) dealloc
{
	if (isrc_)
		CFRelease(isrc_);
	
	[super dealloc];
}






- (MGMetadataSet *)_generateMetadata;
{
	MGMetadataSet *set = [super _generateMetadata];
	
	// Set the endianness tag from the bitmap
	// again, may be superseded by subclasses
	
	NSDictionary *tiffTags = [MGTIFFMetadataReader metadataFromFile:filePath_];
    
        if ([tiffTags objectForKey:MGMetadataImageTIFFLittleEndianKey])
            [set setValue:[tiffTags objectForKey:MGMetadataImageTIFFLittleEndianKey] forKey:MGMetadataImageTIFFLittleEndianKey];

    

	// We're going to add channel info (for the importer to be able to stack); this is not supplied in the file but multi-tiffs are usually stacked...
	// Assume 1 channel; if the reps are R1, G1, B1, R2, G2,... the user will have to unstack/restack manually
	int nReps = [self _numberOfImageRepresentationsAssumingNoSubImporters];
	
	if (nReps > 1 && ![set valueForKey:MGMetadataImagePlanesKey]) {
		NSMutableArray *planes = [NSMutableArray array];
        

		for (int i = 0; i < nReps; i++)
			[planes addObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0] forKey:MGMetadataImagePlaneCKey]];
		
		[set setValue:planes forKey:MGMetadataImagePlanesKey];
	}
    
    
    [self _createImageSourceIfNeeded];
    
    if (isrc_ && CGImageSourceGetCount(isrc_) > 0) {
        NSDictionary *props = [(id)CGImageSourceCopyPropertiesAtIndex(isrc_, 0, NULL) autorelease];
        
        NSDictionary *iptc = [props objectForKey:(NSString *)kCGImagePropertyIPTCDictionary];
        
        NSString *caption = [iptc objectForKey:(NSString *)kCGImagePropertyIPTCCaptionAbstract];
        
        if (caption && ![set valueForKey:MGMetadataImageCommentKey]) {
            [set setValue:caption forKey:MGMetadataImageCommentKey];
        }
        
    }
    
    
		
	return set;
}

- (int)numberOfImageRepresentations
{
	[self _createImageSourceIfNeeded];
	
    if (!isrc_)
        return 0;
    
	return CGImageSourceGetCount(isrc_);
}


- (int)_numberOfImageRepresentationsAssumingNoSubImporters
{
	[self _createImageSourceIfNeeded];
    

	
    if (!isrc_)
        return 0;
    
	return CGImageSourceGetCount(isrc_);
}


- (NSBitmapImageRep *)imageRepresentationAtIndex:(int)index
{
	[self _createImageSourceIfNeeded];
	
	NSAssert(index < CGImageSourceGetCount(isrc_), @"Invalid image source index");

	CGImageRef cgimg = CGImageSourceCreateImageAtIndex(isrc_, index, nil);

	if (!cgimg)
		return nil;
	
	NSBitmapImageRep *rep = [[[NSBitmapImageRep alloc] initWithCGImage:cgimg] autorelease];
	
	CGImageRelease(cgimg);
	
	
	return [self _postProcessRepresentationBitmapData:rep];
	
}


- (void)_createImageSourceIfNeeded
{
	if (isrc_)
		return;
	
	isrc_ = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:[self representationFilePath]], nil);
}





@end
