//
//  MGScrollView.m
//  MetaData
//
//  Created by Dennis Lorson on 27/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGScrollView.h"


@implementation MGScrollView


- (void)scrollWheel:(NSEvent *)theEvent
{
	// disable wheel scrolling, instead forward to the MDView
	[[[self superview] enclosingScrollView] scrollWheel:theEvent];
	
}



@end
