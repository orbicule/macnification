//
//  NSBezierPath_MGAdditions.m
//  Filament
//
//  Created by Dennis Lorson on 18/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "NSBezierPath_MGAdditions.h"


@implementation NSBezierPath (MGAdditions)


- (NSInteger)area
{
	
	// construct an array with the associated point values
	NSMutableArray *points = [NSMutableArray array];
	NSInteger n = [self elementCount];
	
	NSPoint elementPoints[3];
	
	int i;
	for (i = 0; i < n; i++) {
		
		NSBezierPathElement el = [self elementAtIndex:i associatedPoints:elementPoints];
		if (el == NSMoveToBezierPathElement || el == NSLineToBezierPathElement || el == NSClosePathBezierPathElement) {
			
			[points addObject:[NSValue valueWithPoint:elementPoints[0]]];
			
		} else {
			
			return -1;
		}
		
		
	}
	
	n = [points count];
	
	
	NSInteger area = 0;
	
	
	NSInteger j;
	for (i = 0; i < n; i++) {
		j = (i + 1) % n;
		
		NSPoint first = [[points objectAtIndex:i] pointValue];
		NSPoint second = [[points objectAtIndex:j] pointValue];
		
		area += first.x * second.y;
		area -= first.y * second.x;
	}
	
	area /= 2;
	
	return ABS(area);
}



- (CGPathRef)CGPath 
{
	CGMutablePathRef thePath = CGPathCreateMutable();
	if (!thePath) return nil;
	
	NSInteger elementCount = [self elementCount];
	
	// The maximum number of points is 3 for a NSCurveToBezierPathElement.
	// (controlPoint1, controlPoint2, and endPoint)
	NSPoint controlPoints[3];
	
	NSInteger i;
	for (i = 0; i < elementCount; i++) {
		switch ([self elementAtIndex:i associatedPoints:controlPoints]) {
			case NSMoveToBezierPathElement:
				CGPathMoveToPoint(thePath, &CGAffineTransformIdentity, 
								  controlPoints[0].x, controlPoints[0].y);
				break;
			case NSLineToBezierPathElement:
				CGPathAddLineToPoint(thePath, &CGAffineTransformIdentity, 
									 controlPoints[0].x, controlPoints[0].y);
				break;
			case NSCurveToBezierPathElement:
				CGPathAddCurveToPoint(thePath, &CGAffineTransformIdentity, 
									  controlPoints[0].x, controlPoints[0].y,
									  controlPoints[1].x, controlPoints[1].y,
									  controlPoints[2].x, controlPoints[2].y);
				break;
			case NSClosePathBezierPathElement:
				CGPathCloseSubpath(thePath);
				break;
			default:  // COV_NF_START
				NSLog(@"Unknown element at [NSBezierPath CGPath]");
				break;  // COV_NF_END
		};
	}
	return (CGPathRef)[(id)thePath autorelease];
}


@end
