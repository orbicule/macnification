//
//  CaptureDevice.h
//  Filament
//
//  Wrapper around an imaging device, abstracting the underlying framework (QT/IC)
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageCaptureCore/ImageCaptureCore.h>
#import <QTKit/QTKit.h>
//#import <MexJCam/MexCam.h>
#import <dc1394/dc1394.h>

#define pr_device_id		uint64_t//unsigned __int64
#define dc1394_device_id	uint64_t

extern NSString *CaptureAdjustmentGain;
extern NSString *CaptureAdjustmentBrightness;
extern NSString *CaptureAdjustmentGamma;
extern NSString *CaptureAdjustmentSaturation;
extern NSString *CaptureAdjustmentExposure;

typedef enum _CaptureAdjustmentMapping
{
	CaptureAdjustmentMappingLinear,
	CaptureAdjustmentMappingExponential
	
} CaptureAdjustmentMapping;

typedef enum _CaptureExposureMode
{
    CaptureExposureModeNone = 0,
    CaptureExposureModeAuto,
    CaptureExposureModeManual
}
CaptureExposureMode;

typedef struct _CaptureAdjustmentParameters
{
	CGFloat min;
	CGFloat max;
	CGFloat def;
	CaptureAdjustmentMapping mapping;
	
} CaptureAdjustmentParameters;

typedef enum _CapturePreviewType
{
	CapturePreviewOpenGL = 1,
	CapturePreviewQT,
	CapturePreviewNone
} CapturePreviewType;


typedef struct _CaptureAdjustmentRange
{
    float min;
    float max;
    BOOL linear;
} CaptureAdjustmentRange;


@class CaptureDevice;


@protocol CaptureDeviceDelegate <NSObject>

- (void)captureDeviceDidOpenSession:(CaptureDevice *)device;
- (void)captureDevice:(CaptureDevice *)device didReceivePicturesForImport:(NSArray *)pictures;

- (CIImage *)captureDevice:(CaptureDevice *)device adjustedImage:(CIImage *)original;

@end

																	#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
@interface CaptureDevice : NSObject <NSSoundDelegate>
																	#else
@interface CaptureDevice : NSObject
																	#endif

{
	id <CaptureDeviceDelegate> delegate_;
    
    int indexSuffix_;
}

@property (nonatomic, retain) id <CaptureDeviceDelegate> delegate;
@property (readonly) id originalDevice;

// only create devices using these methods!
+ (CaptureDevice *)deviceWithQTDevice:(QTCaptureDevice *)device;
+ (CaptureDevice *)deviceWithICDevice:(ICCameraDevice *)device;
+ (CaptureDevice *)deviceWithPRDeviceID:(pr_device_id)guid;
+ (CaptureDevice *)deviceWithDC1394DeviceID:(dc1394_device_id)guid controller:(dc1394_t *)ctl;

// info
- (NSString *)uidString;
- (NSString *)name;
- (NSString *)uniqueName;
- (int)indexSuffix;
- (void)setIndexSuffix:(int)suffix;
- (BOOL)canTakePicture;
- (BOOL)hasStorage;
- (BOOL)hasStoredItems;
- (NSInteger)numberOfStoredItems;

- (BOOL)isValid;

- (CapturePreviewType)previewType;
// connect to the preview, either an NSImageView or a QTCaptureView / OGLView
- (void)connectPreviewToView:(NSView *)view;
- (void)disconnectPreviewFromView:(NSView *)view;


- (void)startSession;
- (void)stopSession;

- (void)takePicture;

- (void)downloadCurrentItem;
- (void)downloadAllItems;
- (void)cancelDownload;

- (NSImage *)iconImage;


// adjustments

- (CaptureExposureMode)exposureModes;

- (void)resetExposure;

- (void)setExposure:(CGFloat)exposure;
- (CGFloat)exposure;

- (void)setAutoExposure:(BOOL)autoExposure;
- (BOOL)autoExposure;

- (NSString *)exposureDescription;


- (void)setExposureAbsolute:(CGFloat)exposure;
- (CGFloat)absoluteExposure;

- (CaptureAdjustmentRange)exposureRange;


@end
