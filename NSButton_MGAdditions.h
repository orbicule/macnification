//
//  NSButton_MGAdditions.h
//  toolbarTest
//
//  Created by Dennis Lorson on 20/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSButton (MGAdditions)

//static CGFloat BUTTON_HEIGHT = 50;


+ (id)toolStripButtonWithImage:(NSImage *)image title:(NSString *)title;



@end
