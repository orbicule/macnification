//
//  MGImageFileImporter_Private.h
//  Filament
//
//  Created by Dennis Lorson on 20/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGImageFileImporter (Private)



- (id)initWithFilePath:(NSString *)filePath;
+ (BOOL)_canOpenFilePath:(NSString *)path;
- (MGMetadataSet *)_generateMetadata;

- (NSBitmapImageRep *)_postProcessRepresentationBitmapData:(NSBitmapImageRep *)rep;


@end
