//
//  MGDatePredicateRowTemplate.m
//  Filament
//
//  Created by Dennis Lorson on 17/12/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGDatePredicateRowTemplate.h"


@implementation MGDatePredicateRowTemplate

- (NSArray *)templateViews
{
	NSArray *views = [super templateViews];

	if (!datePicker_) {
		datePicker_ = [views objectAtIndex:2];
		
		// TODO: don't set the current date here, it will be called for every opening of the predicate editor
		// we should set it somewhere in the smart folder default values?
		//[datePicker_ setDateValue:[NSDate date]];
	}

	return views;
}

- (NSPredicate *)predicateWithSubpredicates:(NSArray *)subpredicates
{
    NSComparisonPredicate *predicate = (NSComparisonPredicate *)[super predicateWithSubpredicates:subpredicates];

	NSDate *originalDate = [datePicker_ dateValue];
	
	// set the date to 00:00 on the same day
	NSDateComponents *comps = [[NSCalendar currentCalendar] components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSEraCalendarUnit) fromDate:originalDate];
	NSDate *dayStartDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
	
	// generate the second date
#ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
	NSDate *dayEndDate = [dayStartDate dateByAddingTimeInterval:23.9999 * 3600];
#else
	NSDate *dayEndDate = [dayStartDate addTimeInterval:23.9999 * 3600];
#endif
	
	NSExpression *dayStartDateExpression = [NSExpression expressionForConstantValue:dayStartDate];
	NSExpression *dayEndDateExpression = [NSExpression expressionForConstantValue:dayEndDate];

	
	NSPredicate *modifiedPredicate;
	
	if ([predicate predicateOperatorType] == NSGreaterThanPredicateOperatorType) {
		
		NSPredicate *gtPredicate = [NSComparisonPredicate predicateWithLeftExpression:[predicate leftExpression] 
																	  rightExpression:dayEndDateExpression 
																			 modifier:NSDirectPredicateModifier
																				 type:NSGreaterThanPredicateOperatorType
																			  options:0];
		modifiedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObject:gtPredicate]];
		
	} else if ([predicate predicateOperatorType] == NSLessThanPredicateOperatorType) {
		
		NSPredicate *stPredicate = [NSComparisonPredicate predicateWithLeftExpression:[predicate leftExpression] 
																	  rightExpression:dayStartDateExpression 
																			 modifier:NSDirectPredicateModifier
																				 type:NSLessThanPredicateOperatorType
																			  options:0];
		modifiedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObject:stPredicate]];
		
	} else {
		
		NSPredicate *gtPredicate = [NSComparisonPredicate predicateWithLeftExpression:[predicate leftExpression] 
																	  rightExpression:dayStartDateExpression 
																			 modifier:NSDirectPredicateModifier
																				 type:NSGreaterThanPredicateOperatorType
																			  options:0];
		NSPredicate *stPredicate = [NSComparisonPredicate predicateWithLeftExpression:[predicate leftExpression] 
																	  rightExpression:dayEndDateExpression 
																			 modifier:NSDirectPredicateModifier
																				 type:NSLessThanPredicateOperatorType
																			  options:0];
		
		modifiedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:stPredicate, gtPredicate, nil]];
		
	}
	
	/*
	if ([predicate predicateOperatorType] == NSGreaterThanPredicateOperatorType) {
		modifiedPredicateString = [NSString stringWithFormat:@"%@ > %@", [predicate leftExpression], dayEndDateExpression];
	} else if ([predicate predicateOperatorType] == NSLessThanPredicateOperatorType) {
		modifiedPredicateString = [NSString stringWithFormat:@"%@ < %@", [predicate leftExpression], dayStartDateExpression];
	} else {
		modifiedPredicateString = [NSString stringWithFormat:@"%@ <= %@ && %@ => %@", [predicate leftExpression], dayEndDateExpression, [predicate leftExpression], dayStartDateExpression];
	}
	
	NSPredicate *modifiedPredicate = [NSPredicate predicateWithFormat:modifiedPredicateString];*/
	
	return modifiedPredicate;
}

- (double)matchForPredicate:(NSPredicate *)predicate
{
	if (![predicate isKindOfClass:[NSCompoundPredicate class]])
		return 0.0;
	
	if ([[(NSCompoundPredicate *)predicate subpredicates] count] == 0)
		return 0.0;
	
	NSPredicate *firstPredicate = [[(NSCompoundPredicate *)predicate subpredicates] objectAtIndex:0];
	
	// check if this firstPredicate isn't a compound predicate
	// if it is, it means that the method argument predicate is the root "any/all" predicate
	if ([firstPredicate isKindOfClass:[NSCompoundPredicate class]])
		return 0.0;
	
	NSString *predicateString = [firstPredicate predicateFormat];
		
	if ([predicateString rangeOfString:@"CAST"].location != NSNotFound && [predicateString rangeOfString:@"\"NSDate\""].location != NSNotFound) {
		//NSLog(@"match");
		return 1.0;

	}
	
	return 0.0;
}

- (void)setPredicate:(NSPredicate *)predicate
{
	// for some reason, the entire thing is again wrapped into a compound predicate
	NSCompoundPredicate *originalPredicate = (NSCompoundPredicate *)predicate;
	
	NSPredicate *modifiedPredicate;
	
	if ([[originalPredicate subpredicates] count] == 2) {
		// it's an "equal to" comparison
		
		NSComparisonPredicate *subPredicate = (NSComparisonPredicate *)[[originalPredicate subpredicates] objectAtIndex:0];

		
		modifiedPredicate = [NSComparisonPredicate predicateWithLeftExpression:[subPredicate leftExpression]
															   rightExpression:[subPredicate rightExpression]
																	  modifier:NSDirectPredicateModifier
																		  type:NSEqualToPredicateOperatorType 
																	   options:0];
	} else {
		
		NSComparisonPredicate *subPredicate = (NSComparisonPredicate *)[[originalPredicate subpredicates] objectAtIndex:0];
		
		modifiedPredicate = [NSComparisonPredicate predicateWithLeftExpression:[subPredicate leftExpression]
															   rightExpression:[subPredicate rightExpression]
																	  modifier:NSDirectPredicateModifier
																		  type:[subPredicate predicateOperatorType]
																	   options:0];
		
	}
	
	[super setPredicate:modifiedPredicate];
	
}

@end
