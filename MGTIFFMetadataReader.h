//
//  MGTIFFMetadataReader.h
//  Filament
//
//  Created by Dennis Lorson on 17/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGTIFFMetadataReader : NSObject {

}


+ (NSDictionary *)metadataFromFile:(NSString *)filePath;

@end
