//
//  ImageFusion.h
//  ImageAlignment
//
//  Created by Dennis Lorson on 30/10/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <vImage/vImage.h>
#import <QuartzCore/QuartzCore.h>
#import "MGImageAlignmentTypes.h"


#define UPSAMPLE_CONV_PADDING	3
#define DOWNSAMPLE_CONV_PADDING 4
#define MAX_CONV_PADDING		MAX(UPSAMPLE_CONV_PADDING, DOWNSAMPLE_CONV_PADDING)
#define ROI_OFFSET				0

@interface MGImageFusion : NSObject {
	
	IFSample *scratchOne[4], *scratchTwo[4];

	IFFusionImage *image, *referenceImage;
	
}

- (id)initWithImage:(CIImage *)img referenceImage:(CIImage *)refImg;
- (IAImage *)fusionImage;


- (IFFusionImage *)imageFromCIImage:(CIImage *)img referenceImage:(CIImage *)refImg;
- (IAImage *)unpaddedImageFromImage:(IFFusionImage *)img size:(CGSize)size;
- (void)saveImage:(IFFusionImage *)image withName:(NSString *)name;
- (void)convertImageToKRGB:(IFFusionImage *)img;
- (int)padImageForSubsampling:(IFFusionImage *)img;

// LPT
- (IFLaplacian *)constructLPTWithLevelCount:(size_t)nLevels fromImage:(IFFusionImage *)img;
- (void)performConstructionStep:(size_t)stepIndex onLPT:(IFLaplacian *)lpt;

- (IFFusionImage *)collapseLPT:(IFLaplacian *)lpt;
- (void)performCollapseStep:(size_t)stepIndex onLPT:(IFLaplacian *)lpt;

// SML
- (void)computeSMLForLevel:(size_t)level ofLPT:(IFLaplacian *)lpt;
- (void)mergeLPT:(IFLaplacian *)from toLPT:(IFLaplacian *)to onLevel:(size_t)level;



@end
