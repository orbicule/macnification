//
//  MGCaptureScrollView.m
//  Filament
//
//  Created by Dennis Lorson on 13/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGCaptureScrollView.h"
#import "MGCaptureScroller.h"


@implementation MGCaptureScrollView


//--------------------------------------------------------------//
#pragma mark -- Initialize --
//--------------------------------------------------------------//

- (void)_init
{
    // Configure itself
    NSScroller*     scroller;
    MGCaptureScroller *  blkScroller;
    if ([self hasHorizontalScroller]) {
        scroller = [self horizontalScroller];
        if (scroller) {
            blkScroller = [[MGCaptureScroller alloc] initWithFrame:[scroller frame]];
            [blkScroller setControlSize:[scroller controlSize]];
            [blkScroller setArrowsPosition:NSScrollerArrowsNone];
            [self setHorizontalScroller:blkScroller];
            [blkScroller release];
        }
    }
    if ([self hasVerticalScroller]) {
        scroller = [self verticalScroller];
        if (scroller) {
            blkScroller = [[MGCaptureScroller alloc] initWithFrame:[scroller frame]];
            [blkScroller setArrowsPosition:NSScrollerArrowsNone];
            [blkScroller setControlSize:[scroller controlSize]];
            [self setVerticalScroller:blkScroller];
			NSRect frame = [[self verticalScroller] frame];
			frame.size.width -= 1;
			[[self verticalScroller] setFrame:frame];
            [blkScroller release];
            
        }
    }
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    // Common init
    [self _init];
    
    return self;
}

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (!self) {
        return nil;
    }
    
    // Common init
    [self _init];
    
    return self;
}


+ (Class)_verticalScrollerClass;
{
	return [MGCaptureScroller class];
    
	
	
}

+ (Class)_horizontalScrollerClass;
{
	return [MGCaptureScroller class];
    
	
	
}

//--------------------------------------------------------------//
#pragma mark -- Drawing --
//--------------------------------------------------------------//

- (void)drawRect:(NSRect)rect
{
    // Get bounds
    NSRect  bounds;
    bounds = [self bounds];
    
    // Draw grid
    [[NSColor colorWithCalibratedWhite:0.4 alpha:1.0] set];
    
    NSRect  gridRect;
    gridRect.origin.x = bounds.origin.x + 1;
    gridRect.origin.y = bounds.origin.y;
    gridRect.size.width = bounds.size.width - 2;
    gridRect.size.height = 1;
    NSFrameRect(gridRect);
    
	[[NSColor colorWithCalibratedWhite:0.4 alpha:1.0] set];
    
	
    gridRect.origin.x = bounds.origin.x + 1;
    gridRect.origin.y = bounds.origin.y + bounds.size.height - 1;
    gridRect.size.width = bounds.size.width - 2;
    gridRect.size.height = 1;
    NSFrameRect(gridRect);
    

}



@end
