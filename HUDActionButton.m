//
//  HUDActionButton.m
//  Filament
//
//  Created by Peter Schols on 29/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "HUDActionButton.h"


@implementation HUDActionButton


- (void)mouseDown:(NSEvent *)theEvent 
{ 
	[super mouseDown:theEvent];
	[self displayMenu:theEvent];
} 


- (void)displayMenu:(NSEvent *)theEvent;
{
	int yCoordinate = [self frame].origin.y - 1;
	int xCoordinate = [self frame].origin.x + 2;
	NSPoint overflowButtonLocationInLRFilterBar = NSMakePoint(xCoordinate, yCoordinate);
	NSPoint overflowButtonLocationInWindow = [[self superview] convertPoint:overflowButtonLocationInLRFilterBar toView:nil];
	
	NSEvent *click = [NSEvent mouseEventWithType:[theEvent type] location:overflowButtonLocationInWindow modifierFlags:[theEvent modifierFlags] timestamp:[theEvent timestamp] windowNumber:[theEvent windowNumber] context:[theEvent context] eventNumber:[theEvent eventNumber] clickCount:[theEvent clickCount] pressure:[theEvent pressure]]; 
	[NSMenu popUpContextMenu:[self menu] withEvent:click forView:self];
}   





@end
