//
//  MGImageView.m
//  Filament
//
//  Created by Dennis Lorson on 06/09/09.
//  Copyright 2009 Orbicule. All rights reserved.
//

#import "MGImageView.h"
#import "MGImageLayer.h"
#import "CIFilterExtensions.h"
#import "MGImageFilters.h"


@interface MGImageView (Private)

- (void)updateLayoutWithAnimation:(BOOL)animated;
- (void)layoutImageLayer;
- (void)layoutOverlay;

- (void)createLayers;
- (void)createImageLayer;
- (void)createOverlay;

- (CGFloat)zoomFactorToFit;

- (void)setZoomFactor:(CGFloat)zoomFactor animated:(BOOL)animated;
- (void)compensateMidpointForZoom;

- (void)drawRect:(NSRect)rect ofOverlay:(NSView *)view;

- (CIImage *)adjustSizeOfImageToScreenCapabilities:(CIImage *)anImage;

- (CGFloat)textureScaleFactor;


@end


@interface NSEvent (Delta)

- (CGFloat)deviceDeltaX;
- (CGFloat)deviceDeltaY;
- (CGFloat)deviceDeltaZ;

- (BOOL)_continuousScroll;

@end


#pragma mark -



@interface MGImageOverlayView : NSView
{
	MGImageView *_drawDelegate;
}

@property (readwrite, assign) MGImageView *drawDelegate;

@end


@implementation MGImageOverlayView

@synthesize drawDelegate = _drawDelegate;

- (void)drawRect:(NSRect)rect
{
	[super drawRect:rect];
	
	[_drawDelegate drawRect:rect ofOverlay:self];
}


@end



#pragma mark -



@implementation MGImageView

@synthesize toolMode = toolMode_;
@synthesize zoomFactor = zoomFactor_;
@synthesize image = image_;


#pragma mark -
#pragma mark Init

- (id)init;
{	
	if ((self = [super init])) 
        [self initCommon];

	return self;
}


- (id)initWithCoder:(NSCoder *)coder;
{	
	if ((self = [super initWithCoder:coder]))      
        [self initCommon];

	return self;
}

- (id)initWithFrame:(NSRect)frame;
{	
	if ((self = [super initWithFrame:frame]))
        [self initCommon];
    
	return self;
}

- (void)initCommon
{
    [self setAcceptsTouchEvents:YES];
    [self createLayers];
}


- (void) dealloc
{
	self.image = nil;
	
	[super dealloc];
}


- (void)awakeFromNib
{
	
}

- (BOOL) acceptsFirstResponder 
{ 
	return YES;
}


#pragma mark -
#pragma mark Filters

- (void)setFilters:(NSArray *)filters;
{
    
    // we need to apply the transformation that was applied to the image, to the binary filter too (since it has an independent source image)    
    
    if ([self textureScaleFactor] != 1.0) {
        
        for (CIFilter *filter in filters) {
            
            if (![filter isKindOfClass:[ThresholdCompositorFilter class]] || ![filter valueForKey:@"inputBinaryImage"])
                continue;
            
            CIImage *binaryImage = [filter valueForKey:@"inputBinaryImage"];
            binaryImage = [self adjustSizeOfImageToScreenCapabilities:binaryImage];
            [filter setValue:binaryImage forKey:@"inputBinaryImage"];
            /*
            NSAffineTransform *trafo = [NSAffineTransform transform];
            [trafo scaleBy:[self textureScaleFactor]];
            
            CIFilter *trafoFilter = [CIFilter filterWithName:@"CIAffineTransform"];
            [trafoFilter setValue:trafo forKey:@"inputTransform"];
            [trafoFilter setValue:binaryImage forKey:@"inputImage"];
            [filter setValue:[trafoFilter valueForKey:@"outputImage"] forKey:@"inputBinaryImage"];*/
            
        }

    }

    
    imageLayer_.image = [CIFilter filterImage:image_ withFilters:filters];			
    
    [imageLayer_ setNeedsDisplay];
}


#pragma mark -
#pragma mark Tool mode

- (void)setToolMode:(MGToolMode)mode
{
	if (toolMode_ == mode)
		return;
	
	toolMode_ = mode;
	[[self window] invalidateCursorRectsForView:self];
}


- (void)resetCursorRects
{
	[self discardCursorRects];
    
    if (toolMode_ == MGToolModeSelect)
		[self addCursorRect:[self bounds] cursor:[NSCursor crosshairCursor]];
	
}

#pragma mark -
#pragma mark Accessors

- (void)setImageRep:(NSBitmapImageRep *)rep
{
	NSBitmapImageRep *resized = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		 pixelsWide:[rep pixelsWide]/2
																		 pixelsHigh:[rep pixelsHigh]/2
																	  bitsPerSample:8
																	samplesPerPixel:4
																		   hasAlpha:YES 
																		   isPlanar:NO
																	 colorSpaceName:NSDeviceRGBColorSpace
																	   bitmapFormat:NSAlphaFirstBitmapFormat
																		bytesPerRow:0
																	   bitsPerPixel:32] autorelease];
	
	[NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithBitmapImageRep:resized]];
	[rep drawInRect:NSMakeRect(0, 0, [resized pixelsWide], [resized pixelsHigh])];
	
	[self setImage:[[[CIImage alloc] initWithBitmapImageRep:resized] autorelease]];
}

- (void)setImage:(CIImage *)image
{
    originalImageSize_ = [image extent].size;
    
	CIImage *adjustedImg = [self adjustSizeOfImageToScreenCapabilities:image];
	
	if (image_ != adjustedImg) {
		[image_ release];
		image_ = [adjustedImg retain];
	}

	imageLayer_.image = adjustedImg;
	    
	// reset the image position and bounds
	imagePosition_ = CGPointMake(CGRectGetMidX(self.layer.bounds), CGRectGetMidY(self.layer.bounds));
	CGRect bounds = CGRectZero;
	if (image_) {
		bounds.size = [imageLayer_.image extent].size;
	}
	else
		bounds.size = CGSizeMake(2, 2); // some small valid size
	imageBounds_ = bounds;
	    
	imageLayer_.hidden = YES;
	[self updateLayoutWithAnimation:NO];

	imageLayer_.hidden = NO;    
}

- (CIImage *)adjustSizeOfImageToScreenCapabilities:(CIImage *)anImage
{
	NSInteger maxTextureSize = [imageLayer_ maximumTextureSize];
    maxTextureSize = MIN(maxTextureSize, 2048);
	
	CGSize imageSize = [anImage extent].size;
	
	CGFloat largestScaleFactor = MAX(imageSize.width/(float)maxTextureSize, imageSize.height/(float)maxTextureSize);
	
	if (largestScaleFactor <= 1.0)
		return anImage;
	
	NSAffineTransform *trafo = [NSAffineTransform transform];
	[trafo scaleBy:1./(largestScaleFactor * 1.0 + 0.001)];
	
	CIFilter *trafoFilter = [CIFilter filterWithName:@"CIAffineTransform"];
	[trafoFilter setValue:anImage forKey:@"inputImage"];
	[trafoFilter setValue:trafo forKey:@"inputTransform"];
	
	CIImage *scaled = [trafoFilter valueForKey:@"outputImage"];
	
	//NSLog(@"%@: scaled down image to %dx%d (max texture size %d)", [self description], (int)([scaled extent].size.width), (int)([scaled extent].size.height), (int)maxTextureSize);

    //int w = (int)([scaled extent].size.width);
   // int h = (int)([scaled extent].size.height);
    
    //NSLog(@"size: %i * %i * 4 == %ib, %ikB, %iMB", w, h, w*h*4, (w*h*4/1024),(w*h*4/(1024 * 1024)));
	
	return scaled;
}


#pragma mark -
#pragma mark Layer/view creation

- (void)createLayers
{
	zoomFactor_ = 1.0;
	
	[self setWantsLayer:YES];

	[self createImageLayer];
	[self createOverlay];
	
	[self updateLayoutWithAnimation:NO];
	
}

- (void)createImageLayer
{

	self.layer = [CALayer layer];
	self.layer.backgroundColor = CGColorCreateGenericRGB(0, 0, 0, 1);

	imageLayer_ = [[MGImageLayer alloc] init];
	
	imageLayer_.minificationFilter = kCAFilterLinear;
	imageLayer_.magnificationFilter = kCAFilterNearest;
	imageLayer_.needsDisplayOnBoundsChange = YES;
	
	[self.layer addSublayer:imageLayer_];
	
	imageLayer_.backgroundColor = CGColorCreateGenericRGB(0, 0, 0, 1);


}

- (void)createOverlay
{
	overlayView_ = [[MGImageOverlayView alloc] initWithFrame:NSRectFromCGRect(imageLayer_.frame)];
	overlayView_.drawDelegate = self;
	
	NSRect bounds = overlayView_.bounds;
	bounds.origin = NSZeroPoint;
	overlayView_.bounds = bounds;
	
	[self addSubview:overlayView_];
}


#pragma mark -
#pragma mark Layer layout

- (void)updateLayoutWithAnimation:(BOOL)animated
{
	[CATransaction begin];
	[CATransaction setValue:[NSNumber numberWithBool:!animated] forKey:kCATransactionDisableActions];
	
	[self layoutImageLayer];
	[self layoutOverlay];
	
	[CATransaction commit];
}

- (void)layoutImageLayer
{
	CGFloat factor = MAX(0.05, zoomFactor_);
	
	CATransform3D zoom = CATransform3DMakeTranslation(.5, .5, 0);
	zoom = CATransform3DScale(zoom, factor, factor, 1);
	zoom = CATransform3DTranslate(zoom, -.5, -.5, 0);
    
    [CATransaction begin];
    

	imageLayer_.transform = zoom;
	imageLayer_.position = imagePosition_;
	imageLayer_.bounds = imageBounds_;
    
    
    
    
    // constrain to visible bounds
    // the visible bounds should be contained within the image frame
    // adjust "position" to satisfy this
    
    // Compute the image layer frame in the view coordinate system
    CGRect imageLayerFrame = imageLayer_.frame;        
    CGRect hostLayerBounds = [[self layer] bounds];
    
    
    CGPoint adjustedPosition = imagePosition_;
    
    
    if ((CGRectGetWidth(imageLayerFrame) < CGRectGetWidth(hostLayerBounds)) && (CGRectGetHeight(imageLayerFrame) < CGRectGetHeight(hostLayerBounds))) {
        
        // smaller than view
        // make sure it stays centered
        adjustedPosition = CGPointMake(CGRectGetMidX(hostLayerBounds), CGRectGetMidY(hostLayerBounds));
        
        
    }
    
    else {
        
        // larger than view
        
        if (CGRectGetMaxX(hostLayerBounds) > CGRectGetMaxX(imageLayerFrame))
            adjustedPosition.x -= (CGRectGetMaxX(imageLayerFrame) - CGRectGetMaxX(hostLayerBounds));
        
        if (CGRectGetMinX(hostLayerBounds) < CGRectGetMinX(imageLayerFrame))
            adjustedPosition.x -= (CGRectGetMinX(imageLayerFrame) - CGRectGetMinX(hostLayerBounds));
        
        
        if (CGRectGetMaxY(hostLayerBounds) > CGRectGetMaxY(imageLayerFrame))
            adjustedPosition.y -= (CGRectGetMaxY(imageLayerFrame) - CGRectGetMaxY(hostLayerBounds));
        
        if (CGRectGetMinY(hostLayerBounds) < CGRectGetMinY(imageLayerFrame))
            adjustedPosition.y -= (CGRectGetMinY(imageLayerFrame) - CGRectGetMinY(hostLayerBounds));
        
        
    }

        

    
    imagePosition_ = adjustedPosition;
    imageLayer_.position = imagePosition_;
        
    [CATransaction commit];

    
}

- (void)layoutOverlay
{
	// only set the frame here.
	// the bounds cannot be changed (since the frame/bounds relationship is fixed with layer backed views).
	// instead, we use a transform to convert the bounds properly.

	CGRect overlayFrame = CGRectIntersection(imageLayer_.frame, NSRectToCGRect(self.bounds));
	
	//if ([CATransaction disableActions])
		overlayView_.frame = NSRectFromCGRect(overlayFrame);
	//else
	//	[[_overlayView animator] setFrame:NSRectFromCGRect(overlayFrame)];

	
}

- (void)resizeWithOldSuperviewSize:(NSSize)oldBoundsSize
{
	[super resizeWithOldSuperviewSize:oldBoundsSize];
	
	imagePosition_ = CGPointMake(CGRectGetMidX(self.layer.bounds), CGRectGetMidY(self.layer.bounds));
	[self updateLayoutWithAnimation:NO];
}

#pragma mark -
#pragma mark Overlay

- (void)hideOverlay;
{
	[overlayView_ setHidden:YES];
}

- (void)showOverlay;
{
	[overlayView_ setHidden:NO];
}

- (BOOL)isOverlayHidden;
{
	return [overlayView_ isHidden];
}

- (void)setOverlayNeedsDisplay;
{
	[overlayView_ setNeedsDisplay:YES];
}

- (void)setOverlayNeedsDisplayInRect:(NSRect)rect;
{
	[overlayView_ setNeedsDisplayInRect:rect];
}

- (void)drawRect:(NSRect)rect ofOverlay:(NSView *)view;
{
	// ------------------------------------------------------------------------------------------------------
	// Compute the bounds of the area that should be drawn

    
	
	// the visible bounds are either the entire image rect (if zoomed out sufficiently) or  the visible part of it
	//CGRect completeImageBounds = imageLayer_.bounds;
	CGRect visibleImageBounds = NSRectToCGRect([self visibleImageRect]);
		
	// ------------------------------------------------------------------------------------------------------
	// Setup the CTM to transform the drawing commands (in imageLayer coordinate space) to the coordinate space of the overlay
	
	// overlay view bounds: (0, 0, view.frame.size)
	// image view visible bounds: == imageBounds
 
    
    NSBezierPath *path = [NSBezierPath bezierPathWithRect:rect];
    [path addClip];
	
	[[NSGraphicsContext currentContext] saveGraphicsState];
	
	NSAffineTransform *trafo = [NSAffineTransform transform];
	[trafo scaleBy:zoomFactor_];
    

    // if the image was scaled down due to texture size, compensate
    if ([self textureScaleFactor] != 1.0)
        [trafo scaleBy:[self textureScaleFactor]];
    
	[trafo translateXBy:-visibleImageBounds.origin.x yBy:-visibleImageBounds.origin.y];
    


	[trafo concat];
    
    
    //[[NSColor redColor] set];
    //NSFrameRect(NSInsetRect(NSRectFromCGRect(visibleImageBounds), 2, 2)); 
	
	//NSLog(@"overlay: drawing bounds %@ -- image bounds %@", NSStringFromRect(_overlayView.bounds), NSStringFromRect(NSRectFromCGRect(visibleImageBounds)));

	
	[self drawOverlayRectInBounds:NSRectFromCGRect(visibleImageBounds) withTransform:trafo inView:view];
	
	[[NSGraphicsContext currentContext] restoreGraphicsState];
}

- (void)drawOverlayRectInBounds:(NSRect)bounds withTransform:(NSAffineTransform *)trafo inView:(NSView *)view;
{
	// subclass impl
}

#pragma mark -
#pragma mark Zooming

- (void)zoomInWithAnimation:(BOOL)animated;
{
	[self setZoomFactor:self.zoomFactor * 1.5 animated:animated];
}

- (void)zoomOutWithAnimation:(BOOL)animated;
{
	[self setZoomFactor:self.zoomFactor / 1.5 animated:animated];
}

- (void)zoomImageToFitWithAnimation:(BOOL)animated;
{
	CGFloat z = [self zoomFactorToFit];
	[self setZoomFactor:z animated:animated];
}

- (CGFloat)zoomFactorToFit
{
	//NSLog(@"self layer %@, imageLayer %@", NSStringFromSize(NSSizeFromCGSize(self.layer.bounds.size)), NSStringFromSize(NSSizeFromCGSize(_imageLayer.bounds.size)));
	CGFloat fitZoomFactor = MIN(self.layer.bounds.size.width / imageLayer_.bounds.size.width, self.layer.bounds.size.height / imageLayer_.bounds.size.height);
	return fitZoomFactor * 0.99;
}


- (void)setZoomFactor:(CGFloat)zoomFactor animated:(BOOL)animated
{
	if (zoomFactor > 15.0 || zoomFactor < 0.1)
		return;
	
	zoomFactor_ = zoomFactor;
	
	// make sure the original midpoint stays in the same place
	[self compensateMidpointForZoom];
	
	[self updateLayoutWithAnimation:animated];
	[overlayView_ setNeedsDisplay:YES];
}

- (void)compensateMidpointForZoom
{
	// Modify _imagePosition so that the centered image point stays the same after zooming
	// Behavior:
	// 1) in normal cases (zoomed to a point where the image fully covers the view):
	//   -> retain the centered image point
	// 2) the image doesn't fully cover the view along one or two axes:
	//    2a) because it is zoomed out so an edge becomes visible, but the image dimension is still large enough to cover:
	//        -> adjust the centered point so that the image just covers the view
	//    2b) because it is zoomed out so both edges become visible:
	//        -> set the midpoint of the image to be the centered point.
	
	CGPoint newImagePosition;

	// manually get the image dimensions based on the new zoom factor
	CGSize imageSize = imageLayer_.bounds.size;
	imageSize.width *= zoomFactor_;
	imageSize.height *= zoomFactor_;
	
	NSSize viewSize = self.bounds.size;
	
	
	// -------------------------------------------------------------------------------------------------------
	// CASE 1: compute the standard position (regardless of whether the image fills the view)
	
	CGPoint currentImageMidPoint = [imageLayer_ convertPoint:CGPointMake(NSMidX(self.bounds), NSMidY(self.bounds)) fromLayer:self.layer];
	
	CGPoint currentImageMidPointOffsetFromImageCenter;
	currentImageMidPointOffsetFromImageCenter.x = (currentImageMidPoint.x - CGRectGetMidX(imageLayer_.bounds));
	currentImageMidPointOffsetFromImageCenter.y = (currentImageMidPoint.y - CGRectGetMidY(imageLayer_.bounds));
	
	// multiply the offset by the new zoomFactor (can't use the layer point conversion because the new zoom trafo is not set yet)
	currentImageMidPointOffsetFromImageCenter.x *= zoomFactor_;
	currentImageMidPointOffsetFromImageCenter.y *= zoomFactor_;
	
	// thus we know the new image position
	newImagePosition.x = NSMidX(self.bounds) - currentImageMidPointOffsetFromImageCenter.x;
	newImagePosition.y = NSMidY(self.bounds) - currentImageMidPointOffsetFromImageCenter.y;
	
	
	// -------------------------------------------------------------------------------------------------------
	// CASE 2a: should the midpoint position be adjusted for one or two dimensions?
	// check if an edge is visible, for the 4 possible edges, and change the position if necessary
	CGFloat newImageMaxX = newImagePosition.x + imageSize.width * 0.5;
	if (newImageMaxX < NSMaxX(self.bounds))
		newImagePosition.x = NSMaxX(self.bounds) - imageSize.width * 0.5;

	CGFloat newImageMinX = newImagePosition.x - imageSize.width * 0.5;
	if (newImageMinX > NSMinX(self.bounds))
		newImagePosition.x = NSMinX(self.bounds) + imageSize.width * 0.5;
	
	CGFloat newImageMaxY = newImagePosition.y + imageSize.height * 0.5;
	if (newImageMaxY < NSMaxY(self.bounds))
		newImagePosition.y = NSMaxY(self.bounds) - imageSize.height * 0.5;
	
	CGFloat newImageMinY = newImagePosition.y - imageSize.height * 0.5;
	if (newImageMinY > NSMinY(self.bounds))
		newImagePosition.y = NSMinY(self.bounds) + imageSize.height * 0.5;
	
	// -------------------------------------------------------------------------------------------------------
	// CASE 2b: see if any of the axes should be centered in the view -> change the position already given
	if (imageSize.width <= viewSize.width)
		newImagePosition.x = NSMidX(self.bounds);
	if (imageSize.height <= viewSize.height)
		newImagePosition.y = NSMidY(self.bounds);	
	
	
	
	imagePosition_ = newImagePosition;
	
}


- (BOOL)imageIsCompletelyVisible;
{
	return CGRectContainsRect(self.layer.bounds, imageLayer_.frame);
}


#pragma mark -
#pragma mark Event handling


- (void)scrollWheel:(NSEvent *)theEvent
{
    [super scrollWheel:theEvent];
    
    CGFloat scrollX = 0, scrollY = 0;
    
    if (![theEvent respondsToSelector:@selector(_continuousScroll)] || ![theEvent _continuousScroll]) {

        scrollX = [theEvent deltaX] * 4.0;
        scrollY = [theEvent deltaY] * 4.0;
    }
    
    else {
        
        scrollX = [theEvent deviceDeltaX];
        scrollY = [theEvent deviceDeltaY];
    }
    

    
    [self scrollByX:scrollX y:scrollY];
    
}




#pragma mark -
#pragma mark Scrolling

- (void)scrollByX:(CGFloat)x y:(CGFloat)y
{
    imagePosition_.x += x;
    imagePosition_.y -= y;
    
    // will be constrained by layout method
    
    [self updateLayoutWithAnimation:NO];
	
	[overlayView_ setNeedsDisplay:YES];
}

- (void)scrollToPoint:(NSPoint)aPoint;
{
	
	// scroll imageLayer so that the given point (image coords) is centered in the view.
	CGPoint locationInViewCoordinates = NSPointToCGPoint([self convertImagePointToViewPoint:aPoint]);
	
	CGPoint newImagePosition;
	newImagePosition.x = NSMidX(self.bounds) - (locationInViewCoordinates.x - CGRectGetMidX(imageLayer_.frame));
	newImagePosition.y = NSMidY(self.bounds) - (locationInViewCoordinates.y - CGRectGetMidY(imageLayer_.frame));
	
	imagePosition_ = newImagePosition;
	
	
	[self updateLayoutWithAnimation:NO];
	
	[overlayView_ setNeedsDisplay:YES];
}

- (void)scrollToRect:(NSRect)aRect;
{
	
}


- (NSRect)visibleImageRect
{
	CGRect completeImageBounds = imageLayer_.bounds;
	CGRect visibleImageBounds = CGRectIntersection(completeImageBounds, [imageLayer_ convertRect:self.layer.bounds fromLayer:self.layer]);
	
    visibleImageBounds.origin.x /= [self textureScaleFactor];
    visibleImageBounds.origin.y /= [self textureScaleFactor];
    visibleImageBounds.size.width /= [self textureScaleFactor];
    visibleImageBounds.size.height /= [self textureScaleFactor];
    
	return NSRectFromCGRect(visibleImageBounds);
}

#pragma mark -
#pragma mark Coordinate conversion


- (CGFloat)textureScaleFactor;
{
    return imageLayer_.bounds.size.width/originalImageSize_.width;
}

- (NSPoint)convertViewPointToImagePoint:(NSPoint)viewPoint;
{
	CGPoint original = NSPointToCGPoint(viewPoint);
	
	CGPoint imagePoint = [imageLayer_ convertPoint:original fromLayer:self.layer];
    
    // if the image was scaled down due to texture size, compensate
   imagePoint.x /= [self textureScaleFactor];
   imagePoint.y /= [self textureScaleFactor];
		
	return NSPointFromCGPoint(imagePoint);

}

- (NSPoint)convertImagePointToViewPoint:(NSPoint)imagePoint;
{
	CGPoint original = NSPointToCGPoint(imagePoint);
	
    // if the image was scaled down due to texture size, compensate
    original.x *= [self textureScaleFactor];
    original.y *= [self textureScaleFactor];
    
	CGPoint viewPoint = [imageLayer_ convertPoint:original toLayer:self.layer];
    
	
	return NSPointFromCGPoint(viewPoint);
}

- (NSRect)convertImageRectToViewRect:(NSRect)rect
{
    CGRect original = NSRectToCGRect(rect);
    
    original.origin.x *= [self textureScaleFactor];
    original.origin.y *= [self textureScaleFactor];
    original.size.width *= [self textureScaleFactor];
    original.size.height *= [self textureScaleFactor];
    
    return NSRectFromCGRect([imageLayer_ convertRect:original toLayer:self.layer]);
}


@end
