//
//  StackTableImage.m
//  LightTable
//
//  Created by Dennis Lorson on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "StackImage.h"
#import "NSImage_MGAdditions.h"


@implementation StackImage


/*
- (void)setImage:(id)image
{
	// BUGFIX 30/04/2010
	// Extremely important:  will-/didChangeValueForKey!!, otherwise the inverse isn't set
 
	// when the image relationship is established for the first time, this method will initialize the light table properties of this image,
	// such as its dimensions in the light table.
	[self willChangeValueForKey:@"image"];
	[self setPrimitiveValue:image forKey:@"image"];
	[self didChangeValueForKey:@"image"];
}*/


- (CGFloat)imageMagnification
{
	// return the scale factor of this image, that is, its current size relative to its actual size (the size defined by the NSImageRep)
	NSSize originalImageSize = [[self valueForKeyPath:@"image.filteredImage"] bestRepresentationSize];
	CGFloat currentHeight = [[self valueForKeyPath:@"height"] floatValue];
	CGFloat currentWidth = [[self valueForKeyPath:@"width"] floatValue];
	return fmin(currentWidth/originalImageSize.width, currentHeight/originalImageSize.height);
}

- (NSSize)actualSize
{
	// returns the actual size of the image, taking into account the magnification property of the Image.
	NSImage *image = [self valueForKeyPath:@"image.filteredImage"];
	NSSize originalSize = [image bestRepresentationSize];
	
	return NSMakeSize(originalSize.width/[[self valueForKey:@"magnification"] floatValue],originalSize.height/[[self valueForKey:@"magnification"] floatValue]);
}

- (void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
	[super setValue:value forKeyPath:keyPath];
}

@end
