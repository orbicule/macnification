//
//  ROI.m
//  Macnification
//
//  Created by Peter Schols on 04/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ROI.h"
#import "Image.h"
#import "ROIColorOperation.h"
#import "MGAppDelegate.h"
#import "MGLibraryController.h"


@implementation ROI


static NSString *ColorExtractionDidEndNotification = @"ColorExtractionOperationDidEndNotification";



- (void)dealloc 
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ColorExtractionDidEndNotification object:nil];
    
    [super dealloc];
}


# pragma mark -
# pragma mark Clone a ROI (for use on another image)


- (ROI *)clone;
{
	NSEntityDescription *entity = [self entity];
	NSString *name = [entity name];
	ROI *newObject = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:[self managedObjectContext]];
	
	// copy only the required attrs.
	NSArray *properties = [NSArray arrayWithObjects:@"comment", @"rank", @"allPoints", nil]; 
	
	for (NSString *propertyKey in properties) {
		[newObject setValue:[self valueForKey:propertyKey] forKey:propertyKey];
	}
	
	// Copy the image relationship -- DISABLED: we use this for "copy ROIs to the whole stack"
	//[newObject setValue:[self valueForKey:@"image"] forKey:@"image"];
	
	return newObject;
}



- (ROI *)cloneAndAssignToSameImage;
{
	NSEntityDescription *entity = [self entity];
	NSString *name = [entity name];
	ROI *newObject = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:[self managedObjectContext]];
	
	// copy only the required attrs.
	NSArray *properties = [NSArray arrayWithObjects:@"comment", @"rank", @"allPoints", nil]; 
	
	for (NSString *propertyKey in properties) {
		[newObject setValue:[self valueForKey:propertyKey] forKey:propertyKey];
	}
	
	[newObject setValue:[self valueForKey:@"image"] forKey:@"image"];
	
	return newObject;
}




# pragma mark -
# pragma mark Drawing handles and resizing


- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
	// see (some) subclasses
}

- (void)drawHandleAtPoint:(NSPoint)point withScale:(float)scale transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
    
	NSAffineTransform *inverted = [[trafo copy] autorelease];
	[inverted invert];
	
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
    // Figure out a rectangle that's centered on the point
	NSRect handleBounds = [self drawingRectForHandleAtPoint:point withScale:scale];
	
	NSRect handleBoundsInViewCoords;
	handleBoundsInViewCoords.origin = [trafo transformPoint:handleBounds.origin];
	handleBoundsInViewCoords.size = [trafo transformSize:handleBounds.size];
	
	NSRect handleBoundsInBase = [view convertRectToBase:handleBoundsInViewCoords];
	handleBoundsInBase = NSIntegralRect(handleBoundsInBase);
	handleBoundsInBase.origin.x += 0.5;
	handleBoundsInBase.origin.y += 0.5;
	
	handleBoundsInViewCoords = [view convertRectFromBase:handleBoundsInBase];
	
	handleBounds.origin = [inverted transformPoint:handleBoundsInViewCoords.origin];
	handleBounds.size = [inverted transformSize:handleBoundsInViewCoords.size];
    
    
    
	// Draw the handle itself.
	[[NSColor knobColor] set];
	NSBezierPath *path = [NSBezierPath bezierPathWithRect:handleBounds];
	[path setLineWidth:scale * 0.25];
	[path fill];
	[[NSColor whiteColor] set];
	[path stroke];
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];

}

- (NSRect)drawingRectForHandleAtPoint:(NSPoint)point withScale:(float)scale;
{
	// Figure out a rectangle that's centered on the point
    NSRect handleBounds;
    handleBounds.origin.x = point.x - ([self handleSize]/2. *scale);
    handleBounds.origin.y = point.y - ([self handleSize]/2. *scale);
    handleBounds.size.width = handleBounds.size.height = [self handleSize] * scale;
    
	return handleBounds;
}

- (NSRect)hitRectForHandleAtPoint:(NSPoint)point withScale:(float)scale
{
	// Figure out a rectangle that's centered on the point
    NSRect handleBounds;
    handleBounds.origin.x = point.x - ([self handleHitAreaSize]/2. *scale);
    handleBounds.origin.y = point.y - ([self handleHitAreaSize]/2. *scale);
    handleBounds.size.width = handleBounds.size.height = [self handleHitAreaSize] * scale;
	
	return handleBounds;
}

// Calculate the best handle size based on the image size
- (int)handleSize;
{
	return 2;
}

- (int)handleHitAreaSize;
{
	return 14;
}

- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
{
}



- (int)handleUnderPoint:(NSPoint)point;
{
	return 0;
}


- (BOOL)isHandleAtPoint:(NSPoint)point;
{
	if ([self handleUnderPoint:point] > 0) {
		return YES;	
	}
	return NO;
}


- (NSPoint)oppositePointForHandle:(int)handle;
{
	return NSZeroPoint;
}

- (BOOL)containsPoint:(NSPoint)point;
{
	return NO;
}


# pragma mark -
# pragma mark Drawing note icon


- (void)drawNoteIconWithScale:(CGFloat)scale transform:(NSAffineTransform *)trafo view:(NSView *)view;
{
	NSString *comment = [self primitiveValueForKey:@"comment"];
	if (comment && [comment length] > 0){
		NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
		BOOL showNoteIcon = [[userDefaults valueForKey:@"showNoteIcon"] boolValue];
		if (showNoteIcon) {
            
            NSAffineTransform *inverted = [[trafo copy] autorelease];
            [inverted invert];
            
            [[NSGraphicsContext currentContext] setShouldAntialias:NO];
            
            NSRect rect = [self noteIconRect];
            
            NSRect rectInViewCoords;
            rectInViewCoords.origin = [trafo transformPoint:rect.origin];
            rectInViewCoords.size = [trafo transformSize:rect.size];
            
            NSRect handleBoundsInBase = [view convertRectToBase:rectInViewCoords];
            handleBoundsInBase = NSIntegralRect(handleBoundsInBase);
            handleBoundsInBase.origin.x += 0.5;
            handleBoundsInBase.origin.y += 0.5;
            
            rectInViewCoords = [view convertRectFromBase:handleBoundsInBase];
            
            rect.origin = [inverted transformPoint:rectInViewCoords.origin];
            rect.size = [inverted transformSize:rectInViewCoords.size];
            
			[self drawNoteIconInRect:rect withScale:scale];
            
            [[NSGraphicsContext currentContext] setShouldAntialias:YES];

            
		}
	}
}


- (void)drawNoteIconInRect:(NSRect)rect withScale:(float)scale;
{
	NSImage *noteImage = [NSImage imageNamed:@"comment"];
	NSRect adaptedRect = NSMakeRect(rect.origin.x, rect.origin.y, [self handleSize]*4*scale, [self handleSize]*4*scale);
	[noteImage drawInRect:adaptedRect fromRect:NSMakeRect(0, 0, [noteImage size].width, [noteImage size].height) operation:NSCompositeSourceOver fraction:0.8];
}


- (NSRect)noteIconRect;
{
	return NSZeroRect;
}


# pragma mark -
# pragma mark Points


// On-demand get accessor for the customMetadata
- (NSArray *)allPoints;
{
    [self willAccessValueForKey:@"allPoints"];
    NSArray *allPoints = [self primitiveValueForKey:@"allPoints"];
    [self didAccessValueForKey:@"allPoints"];
    if (allPoints == nil) {
        NSData *allPointsData = [self valueForKey:@"allPointsData"];
        if (allPointsData != nil) {
            allPoints = [NSUnarchiver unarchiveObjectWithData:allPointsData];
			[self willChangeValueForKey:@"allPoints"];
            [self setPrimitiveValue:allPoints forKey:@"allPoints"];
			[self didChangeValueForKey:@"allPoints"];
        }
    }
	//NSLog(@"allPoints in ROI.m: %@", allPoints);
    return allPoints;
}



// Immediate-Update Set Accessor
- (void)setAllPoints:(NSArray *)array;
{
	//NSLog(@"setAllPoints in ROI.m: %@", array);
	[self willChangeValueForKey:@"allPoints"];
    [self setPrimitiveValue:[[array copy]autorelease] forKey:@"allPoints"];
    [self didChangeValueForKey:@"allPoints"];
    [self setValue:[NSArchiver archivedDataWithRootObject:[[array copy]autorelease]] forKey:@"allPointsData"];
}




# pragma mark -
# pragma mark Moving ROI


- (void)moveWithDelta:(NSPoint)deltaPoint;
{
	
}



# pragma mark -
# pragma mark Drawing ROI

- (NSBezierPath *)pathWithScale:(float)scale;
{
	return nil;	
}


- (NSRect)invalidatedRect;
{
    return NSInsetRect([self boundingRect], -20, -20);
}


# pragma mark -
# pragma mark Color contents

- (void)calculateColorContents;
{
	BOOL needsToPostChangeNotification = [[[MGLibraryControllerInstance imageArrayController] selectedObjects] containsObject:[self valueForKey:@"image"]];
	NSBezierPath *path = [self pathWithScale:1.0];
	// Create a new ROIColorOperation
		
	ROIColorOperation *mainOp = [[[ROIColorOperation alloc] initWithImageRep:[[self valueForKey:@"image"] imageRep] path:path] autorelease];
	mainOp.triggersNotifications = needsToPostChangeNotification;
	NSOperationQueue *queue = [MGAppDelegateInstance sharedOperationQueue];//[[NSOperationQueue alloc] init];
	
	// Register for the end notification of this ROIColorOperation
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(anyThread_mainOperationDidEnd:) name:ColorExtractionDidEndNotification object:mainOp];
	
    // Don't allow parallel ops; there may be other ops sharing one image and they need to be executed serially
	[queue setMaxConcurrentOperationCount:1];
	[queue addOperation:mainOp];
}

- (void)calculateColorContentsWithSharedImage:(NSBitmapImageRep *)img;
{
	BOOL needsToPostChangeNotification = [[[MGLibraryControllerInstance imageArrayController] selectedObjects] containsObject:[self valueForKey:@"image"]];
	NSBezierPath *path = [self pathWithScale:1.0];
	// Create a new ROIColorOperation
	ROIColorOperation *mainOp = [[[ROIColorOperation alloc] initWithImageRep:img path:path] autorelease];
	mainOp.triggersNotifications = needsToPostChangeNotification;
	NSOperationQueue *queue = [MGAppDelegateInstance sharedOperationQueue];
	
	// Register for the end notification of this ROIColorOperation
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(anyThread_mainOperationDidEnd:) name:ColorExtractionDidEndNotification object:mainOp];
	
	// Since the ops share the same image, this should NOT be concurrent
	[queue setMaxConcurrentOperationCount:1];
	[queue addOperation:mainOp];
}


// Called when an ROIColorOperation ends
- (void)anyThread_mainOperationDidEnd:(NSNotification *)note
{
	[self performSelectorOnMainThread:@selector(mainOperationDidEnd:) withObject:note waitUntilDone:NO];	
}


- (void)mainOperationDidEnd:(NSNotification *)note
{
	NSDictionary *info = [note userInfo];
	
	NSMutableDictionary *colorInfo = [[[note userInfo] mutableCopy] autorelease];
	[colorInfo removeObjectForKey:@"postChangeNotification"];
	
	NSEnumerator *e = [colorInfo keyEnumerator];
	
	BOOL postNotifications = [[info objectForKey:@"postChangeNotification"] boolValue];
	
	for (NSString *key in e) {
		if (postNotifications)
			[self setValue:[colorInfo objectForKey:key] forKey:key];
		else 
			[self setPrimitiveValue:[colorInfo objectForKey:key] forKey:key];
	}
	
	if (postNotifications) {
		[self willChangeValueForKey:@"colorContentsSummary"];
		[self didChangeValueForKey:@"colorContentsSummary"];
	}

	//NSLog(@"mainOp Done");
}	


- (NSAttributedString *)colorContentsSummary;
{
	
	if ([[[self valueForKey:@"image"] colorSpaceName] isEqualTo:NSCalibratedWhiteColorSpace] || [[[self valueForKey:@"image"] colorSpaceName] isEqualTo:NSDeviceWhiteColorSpace]) {
		// Get the white string

		NSString *whiteString = [NSString stringWithFormat:@"%@ <%2.3f-%2.3f> µ:%2.3f ∂:%2.3f\n", 
								 @"w", 
								 [[self valueForKey:@"minimumWhite"]floatValue], 
								 [[self valueForKey:@"maximumWhite"]floatValue], 
								 [[self valueForKey:@"averageWhite"]floatValue], 
								 [[self valueForKey:@"stdevWhite"]floatValue]];
		
		NSMutableAttributedString *whiteAttributedString = [[[NSMutableAttributedString alloc] initWithString:whiteString] autorelease];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:11], NSFontAttributeName, [NSColor whiteColor], NSForegroundColorAttributeName, nil];
		// Add the attributes to the first character and to the first character after the line break
		[whiteAttributedString addAttributes:dict range:NSMakeRange(0, 1)];
		return whiteAttributedString; 
	}
	else {
		
		NSString *redString = [NSString stringWithFormat:@"%@ <%2.3f-%2.3f> µ:%2.3f ∂:%2.3f\n", 
							   @"r", 
							   [[self valueForKey:@"minimumRed"]floatValue], 
							   [[self valueForKey:@"maximumRed"]floatValue], 
							   [[self valueForKey:@"averageRed"]floatValue], 
							   [[self valueForKey:@"stdevRed"]floatValue]];
		
		NSMutableAttributedString *redAttributedString = [[[NSMutableAttributedString alloc] initWithString:redString] autorelease];
		NSDictionary *redDict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:11], NSFontAttributeName, [NSColor colorWithCalibratedRed:0.972 green:0.380 blue:0.333 alpha:0.9], NSForegroundColorAttributeName, nil];
		// Add the attributes to the first character and to the first character after the line break
		[redAttributedString addAttributes:redDict range:NSMakeRange(0, 1)];
		
		NSString *greenString = [NSString stringWithFormat:@"%@ <%2.3f-%2.3f> µ:%2.3f ∂:%2.3f\n", 
								 @"g", 
								 [[self valueForKey:@"minimumGreen"]floatValue], 
								 [[self valueForKey:@"maximumGreen"]floatValue], 
								 [[self valueForKey:@"averageGreen"]floatValue], 
								 [[self valueForKey:@"stdevGreen"]floatValue]];
		
		NSMutableAttributedString *greenAttributedString = [[[NSMutableAttributedString alloc] initWithString:greenString] autorelease];
		NSDictionary *greenDict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:11], NSFontAttributeName, [NSColor colorWithCalibratedRed:0.537 green:0.917 blue:0.352 alpha:0.9], NSForegroundColorAttributeName, nil];
		// Add the attributes to the first character and to the first character after the line break
		[greenAttributedString addAttributes:greenDict range:NSMakeRange(0, 1)];
		
		NSString *blueString = [NSString stringWithFormat:@"%@ <%2.3f-%2.3f> µ:%2.3f ∂:%2.3f", 
								@"b", 
								[[self valueForKey:@"minimumBlue"]floatValue], 
								[[self valueForKey:@"maximumBlue"]floatValue], 
								[[self valueForKey:@"averageBlue"]floatValue], 
								[[self valueForKey:@"stdevBlue"]floatValue]];
		
		NSMutableAttributedString *blueAttributedString = [[[NSMutableAttributedString alloc] initWithString:blueString] autorelease];
		NSDictionary *blueDict = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont fontWithName:@"Osaka" size:11], NSFontAttributeName, [NSColor colorWithCalibratedRed:0 green:0.517 blue:0.937 alpha:0.9], NSForegroundColorAttributeName, nil];
		// Add the attributes to the first character and to the first character after the line break
		[blueAttributedString addAttributes:blueDict range:NSMakeRange(0, 1)];
		
		NSMutableAttributedString *finalAttributedString = [[[NSMutableAttributedString alloc] initWithString:@""] autorelease];
		[finalAttributedString appendAttributedString:redAttributedString];
		[finalAttributedString appendAttributedString:greenAttributedString];
		[finalAttributedString appendAttributedString:blueAttributedString];
		
		return finalAttributedString;
	}
	
}


# pragma mark -
# pragma mark Measurements


- (void)calculateLength;
{	
}


- (void)calculateArea;
{
}


- (void)calculatePerimeter;
{
}


- (void)updateMeasurements;
{
	
}


- (NSRect)boundingRect;
{
	return [[self pathWithScale:1.0] bounds];
}


- (NSNumber *)boundingRectX;
{
	return [NSNumber numberWithFloat:[self boundingRect].origin.x];	
}


- (NSNumber *)boundingRectY;
{
	return [NSNumber numberWithFloat:[self boundingRect].origin.y];	
}


- (NSNumber *)boundingRectWidth;
{
	return [NSNumber numberWithFloat:[self boundingRect].size.width];	
}


- (NSNumber *)boundingRectHeight;
{
	return [NSNumber numberWithFloat:[self boundingRect].size.height];	
}



# pragma mark -
# pragma mark Summary

// Returns the color that will be used in the summary descriptions. This color is based on the ROI color in the preferences
- (NSColor *)summaryColor; 
{
	NSDictionary *userDefaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	NSColor *deselectedROIColor = [NSUnarchiver unarchiveObjectWithData:[userDefaults valueForKey:@"deselectedROIColor"]];
	return [NSColor colorWithCalibratedRed:[deselectedROIColor redComponent] green:[deselectedROIColor greenComponent] blue:[deselectedROIColor blueComponent] alpha:1.0];
}


# pragma mark -
# pragma mark Exporting to Tab delimited text


// Returns the comment without line breaks, so that it can be used in tab delimited text
- (NSString *)comments;
{
	if (![self valueForKey:@"comment"] || [[self valueForKey:@"comment"] isEqualTo:@""]) {
		return @"";
	}
	return [[[self valueForKey:@"comment"] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
}



- (NSString *)tabText;
{
	NSArray *ROIProperties = [ROI ROIProperties];
	
	NSMutableString *tabText = [NSMutableString stringWithCapacity:100];
	
	for (id key in ROIProperties) {
		if (([[[self entity] attributesByName] objectForKey:key] && [self valueForKey:key]) || ([self respondsToSelector:NSSelectorFromString(key)] && [self valueForKey:key])) {
			if ([[self valueForKey:key] respondsToSelector:@selector(descriptionWithLocale:)]) {
				[tabText appendString:[[self valueForKey:key]descriptionWithLocale:[NSLocale currentLocale]]];
			}
			else {
				[tabText appendString:[self valueForKey:key]];
			}
		}
		else {
			[tabText appendString:@"N/A"];
		}
		[tabText appendString:@"\t"];
	}
	return tabText;
}


+ (NSArray *)ROIProperties;
{
	return [NSArray arrayWithObjects:@"rank", @"comments", @"pixelLength", @"length", @"pixelArea", @"area", @"pixelPerimeter", @"perimeter", @"boundingRectX", @"boundingRectY", @"boundingRectWidth", @"boundingRectHeight", @"circularity", @"minimumWhite", @"maximumWhite", @"averageWhite", @"medianWhite", @"stdevWhite", @"integratedDensityWhite", @"minimumRed", @"maximumRed", @"averageRed", @"medianRed", @"stdevRed", @"integratedDensityRed", @"minimumGreen", @"maximumGreen", @"averageGreen", @"medianGreen", @"stdevGreen", @"integratedDensityGreen", @"minimumBlue", @"maximumBlue", @"averageBlue", @"medianBlue", @"stdevBlue", @"integratedDensityBlue", nil];
}



- (void)setMovedPoint:(NSPoint)value {

}


@end
