//
//  ROIColorOperation.m
//  Filament
//
//  Created by Peter Schols on 08/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ROIColorOperation.h"
#import "Image.h"
#import "NSBezierPath_MGAdditions.h"


#define RED 0
#define GREEN 1
#define BLUE 2
#define WHITE 3

static NSString *ColorExtractionDidEndNotification = @"ColorExtractionOperationDidEndNotification";

float MGMedian(unsigned char elements[], NSInteger count);
float MGSelect(int k, int n, unsigned char arr[]);




@interface ROIColorOperation ()


- (NSDictionary *)extractColors:(BOOL)useApproximation;
- (void)sendColors:(NSDictionary *)colorInfo;

@end



@implementation ROIColorOperation


@synthesize triggersNotifications = triggersNotifications_;



- (id)initWithImageRep:(NSBitmapImageRep *)rep path:(NSBezierPath *)path;
{
	if ((self = [super init])) {
		rep_ = [rep copy];
		path_ = [path retain];
		
		self.triggersNotifications = YES;

	}
	
	return self;
}

- (void)dealloc
{
	[rep_ release];
	[path_ release];
	
	[super dealloc];
}





- (void)main
{	
	if (!path_)
		NSLog(@"%@: No ROI supplied.  Operation stopped.", [self description]);
	
	if (!rep_)
		NSLog(@"%@: No image supplied.  Operation stopped.", [self description]);
	
	NSDictionary *colorInfo = [self extractColors:YES];
	[self sendColors:colorInfo];
}



- (NSDictionary *)extractColors:(BOOL)useApproximation;
{
	CGImageRef img = [rep_ CGImage];


	// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
	// Extract the ROI pixels before analyzing them:
	// - cut out the ROI bounds
	// - make sure all pixels in the ROI bounds that do not belong to the ROI, have alpha == 0.0
	// In that way, we can just count everything in the cutout that doesn't have zero alpha value.
	
	// determine dimensions
	
	int imgW = CGImageGetWidth(img);
	int imgH = CGImageGetHeight(img);
	
	NSRect roiRect = [path_ bounds];
	// expand outwards
	roiRect = NSIntegralRect(roiRect);
	
	int roiX = roiRect.origin.x;
	int roiY = roiRect.origin.y;
	int roiW = roiRect.size.width;
	int roiH = roiRect.size.height;
	
	// create cutout bitmap
	
	unsigned char *data = (unsigned char *)calloc(4 * roiW * roiH, sizeof(unsigned char));
	
	CGContextRef ctx = CGBitmapContextCreate(data, roiW, roiH, 8, 4 * roiW, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
	
	CGMutablePathRef cgPath = CGPathCreateMutable();
	CGPathAddPath(cgPath, NULL, [path_ CGPath]);
	
	// from here on, we need to transform, since the path (and image) are offset with (roiX, roiY) w.r.t. the cutout
	CGContextSaveGState(ctx);
	CGAffineTransform trafo = CGAffineTransformMakeTranslation(-roiX, -roiY);
	CGContextConcatCTM(ctx, trafo);
	
	CGContextAddPath(ctx, cgPath);
	CGContextEOClip(ctx);
	
	CGContextDrawImage(ctx, CGRectMake(0, 0, imgW, imgH), img);
	
	//CGImageRef g = CGBitmapContextCreateImage(ctx);
	//NSBitmapImageRep *p = [[[NSBitmapImageRep alloc] initWithCGImage:g] autorelease];
	//[[p TIFFRepresentation] writeToFile:[NSHomeDirectory() stringByAppendingPathComponent:@"result.tiff"] atomically:YES];
	
	
	CGContextRestoreGState(ctx);
	CGContextRelease(ctx);

	
	
	CGPathRelease(cgPath);
	
	// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Do the actual analysis
	
	
	int bufferSize = roiW * roiH;
	
	// Check the colorspace
	BOOL isRGBImage = !([[rep_ colorSpaceName] isEqualTo:NSCalibratedWhiteColorSpace] ||
						[[rep_ colorSpaceName] isEqualTo:NSDeviceWhiteColorSpace]);
	
	// Declare our variables
	float val[4], avg[4], min[4], max[4], sum[4], stdDev[4], median[4];
	
	// initialize them
	int comp;
	for(comp = 0; comp < 4; comp++) {
		val[comp] = 0; avg[comp] = 0.0; min[comp] = 255.0; max[comp] = 0.0; sum[comp] = 0.0; stdDev[comp] = 0.0; median[comp] = 0.0;
	}
	

	
	// the gathered pixels from the 4 channels RGB/W, the start addresses are put into an array to make iterating over the three components easier and shorter.
	unsigned char *gathered[4];

	// this memory allocation might be superfluous, but we are only doing one or two ops at a time so it should be acceptable.
	// they will only partly be filled...up till <count>.
	gathered[RED] =		malloc(sizeof(unsigned char) * bufferSize);
	gathered[GREEN] =	malloc(sizeof(unsigned char) * bufferSize);
	gathered[BLUE] =	malloc(sizeof(unsigned char) * bufferSize);
	gathered[WHITE] =	malloc(sizeof(unsigned char) * bufferSize);
	
	
	// the current pixel address
	unsigned char *pixelAddr;
	
	// the number of accumulated pixels per channel
	int count = 0;
	
	// Iterate the entire ROI
	int x, y = 0;
	int spp = 4;
	
	for (y = 0; y < roiH; y++) {
		
		for (x = 0; x < roiW; x++) {
			
			// adjust the row to be flipped
			pixelAddr = &(data[spp * (x + (roiH - y - 1) * roiW)]);
			
			if (pixelAddr[3] < 0.05)
				continue;
			
			// i iterates over the channels
			for (comp = 0; comp < 3; comp++) {
				
				
				val[comp] = (float)(pixelAddr[comp]);
				
				// sum
				sum[comp] += val[comp];
				
				// min
				if (val[comp] < min[comp]) min[comp] = val[comp];
				
				// max
				if (val[comp] > max[comp]) max[comp] = val[comp];
				
				// add to gathered pixels
				gathered[comp][count] = val[comp];
			}
			
			// if we have a grayscale, retain the color data (we'll dispose of it later anyway) and fill in the white channel data.
			if (!isRGBImage) {
				
				val[WHITE] = 0.299 * val[RED] + 0.587 * val[GREEN] + 0.114 * val[BLUE];
				
				// sum
				sum[WHITE] += val[WHITE];
				
				// min
				if (val[WHITE] < min[WHITE]) min[WHITE] = val[WHITE];
				
				// max
				if (val[WHITE] > max[WHITE]) max[WHITE] = val[WHITE];
				
				// add to gathered pixels
				gathered[WHITE][count] = val[WHITE];
				
				
			}
			
			count ++;	
			
			
			
		}
	}
	
	
	// Calculate statistics	
	if (isRGBImage) {
		
		for (comp = 0; comp < 3; comp++) {
			
			// average over the channel
			avg[comp] = sum[comp]/count;
			
			CGFloat sum = 0.0;
			
			// for each sample, add the square of the difference between sample and average
			int i;
			for(i = 0; i < count; i++) {
				float diff = (gathered[comp][i] - avg[comp]);
				sum += diff * diff;//powf((gathered[comp][i] - avg[comp]), 2);

			}
			
			stdDev[comp] = sqrt(sum/(count - 1));
			
			median[comp] = MGMedian(gathered[comp], count);
			
		}
	}
			
		
	else {
		// average over the channel
		avg[WHITE] = sum[WHITE]/count;
		
		CGFloat sum = 0.0;
		
		// for each sample, add the square of the difference between sample and average
		int i;
		for(i = 0; i < count; i++)
			sum += (gathered[WHITE][i] - avg[WHITE]) * (gathered[WHITE][i] - avg[WHITE]); //pow((gathered[WHITE][i] - avg[WHITE]), 2);
		
		stdDev[WHITE] = sqrt(sum/(count - 1));
		
		median[WHITE] = MGMedian(gathered[WHITE], count);
		
	}

	
	for(comp = 0; comp < 4; comp++)
		free(gathered[comp]);
	
	
	// divide the components by the max value
	CGFloat maxVal = 255.0;
	for(comp = 0; comp < 4; comp++) {
		val[comp] /= maxVal; avg[comp] /= maxVal; min[comp] /= maxVal; max[comp] /= maxVal; sum[comp] /= maxVal; stdDev[comp] /= maxVal; median[comp] /= maxVal;
	}

	
	free(data);
	
	// Put all the statistics in an NSDictionary and return it
	NSDictionary *colorInformation = [NSDictionary dictionaryWithObjectsAndKeys:
									  [NSNumber numberWithFloat:min[WHITE]], @"minimumWhite", 
									  [NSNumber numberWithFloat:max[WHITE]], @"maximumWhite", 
									  [NSNumber numberWithFloat:avg[WHITE]], @"averageWhite", 
									  [NSNumber numberWithFloat:stdDev[WHITE]], @"stdevWhite", 
									  [NSNumber numberWithFloat:median[WHITE]], @"medianWhite", 
									  [NSNumber numberWithFloat:min[RED]], @"minimumRed", 
									  [NSNumber numberWithFloat:max[RED]], @"maximumRed", 
									  [NSNumber numberWithFloat:avg[RED]], @"averageRed", 
									  [NSNumber numberWithFloat:stdDev[RED]], @"stdevRed",
									  [NSNumber numberWithFloat:median[RED]], @"medianRed",
									  [NSNumber numberWithFloat:min[GREEN]], @"minimumGreen", 
									  [NSNumber numberWithFloat:max[GREEN]], @"maximumGreen", 
									  [NSNumber numberWithFloat:avg[GREEN]], @"averageGreen", 
									  [NSNumber numberWithFloat:stdDev[GREEN]], @"stdevGreen", 
									  [NSNumber numberWithFloat:median[GREEN]], @"medianGreen",
									  [NSNumber numberWithFloat:min[BLUE]], @"minimumBlue", 
									  [NSNumber numberWithFloat:max[BLUE]], @"maximumBlue", 
									  [NSNumber numberWithFloat:avg[BLUE]], @"averageBlue", 
									  [NSNumber numberWithFloat:stdDev[BLUE]], @"stdevBlue", 
									  [NSNumber numberWithFloat:median[BLUE]], @"medianBlue",
									  [NSNumber numberWithBool:self.triggersNotifications], @"postChangeNotification", nil];
	

	return colorInformation;
}





- (void)sendColors:(NSDictionary *)colorInfo;
{
	 // Send the results through a notification.
	 // the receiver will have to call -performSelectorOnMainThread with the given userInfo.
	NSNotification *note = [NSNotification notificationWithName:ColorExtractionDidEndNotification object:self userInfo:colorInfo];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}


// Wrapper for the select function
float MGMedian(unsigned char elements[], NSInteger count) 
{	
	return MGSelect((unsigned long)floor((CGFloat)count/2.0), (unsigned long)count - 1, elements);	
}



#define SWAP(a,b) temp = (a); (a) = (b); (b) = temp;

// The fastest known median searching algorithm from http://ndevilla.free.fr/median/median/src/quickselect.c
// This is about 6 times faster than a normal quicksort.  Benchmark: returns median out of 1 million elements in under 2 secs on a Pentium II 400 Mhz in 1998

// returns the kth element (in ascending order) from the array of given elements (need not be sorted)
// the input array will be altered!
float MGSelect(int k, int n, unsigned char arr[])
{
	int i, ir, j, l, mid;
	CGFloat a, temp;
	
	l = 1;
	ir = n;
	for(;;) {
		
		if (ir <= l + 1) {
			if (ir == l + 1 && arr[ir] < arr[l]) {
				
				SWAP(arr[l], arr[ir]);

			}
			return arr[k];
			
		} else {
			
			mid = (l+ir) >> 1;
			SWAP(arr[mid], arr[l+1]);
			
			if (arr[l] > arr[ir]) {
				
				SWAP(arr[l], arr[ir]);

			}
			
			if (arr[l+1] > arr[ir]) {
				
				SWAP(arr[l+1], arr[ir]);

			}
			
			if (arr[l] > arr[l+1]) {
				
				SWAP(arr[l], arr[l+1]);

			}
			
			i = l + 1;
			j = ir;
			a = arr[l+1];
			
			for(;;) {
				
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				
				if (j < i) break;
				
				SWAP(arr[i], arr[j]);

			}
			
			arr[l+1] = arr[j];
			arr[j] = a;
			
			if (j >= k) ir = j - 1;
			if (j <= k) l = i;
			
		}
	}
}



@end
