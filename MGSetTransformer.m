//
//  MGSetTransformer.m
//  MetaData
//
//  Created by Dennis Lorson on 8/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import "MGSetTransformer.h"


@implementation MGSetTransformer


+ (Class)transformedValueClass
{
    return [NSArray class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)beforeObject
{
	//NSLog(@"transform %@", [beforeObject description]);
	
    if (beforeObject == nil) return nil;
	
	return [(NSSet *)beforeObject allObjects];
	
}

- (id)reverseTransformedValue:(id)beforeObject
{
    if (beforeObject == nil) return nil;
	
	return [NSSet setWithArray:(NSArray *)beforeObject];
	
}


@end
