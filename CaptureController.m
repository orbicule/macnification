
//
//  CaptureController.m
//  Filament
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureController.h"

#import "CaptureDevice.h"
#import "CaptureICDevice.h"
#import "MGContextualToolStrip.h"
#import "NSButton_MGAdditions.h"
#import "MGImageBatchImporter.h"
#import "SourceListTreeController.h"
#import "MGAppDelegate.h"
#import "ImageArrayController.h"
#import "Image.h"
#import "CapturePRPreview.h"
#import "MGHUDBox.h"
#import "MGOverlaySettingsWindow.h"
#import "MGHUDSlider.h"
#import "MGHUDSliderCell.h"
#import "MGHUDButton.h"
#import "MGHUDButtonCell.h"
#import "CaptureOpenGLPreview.h"
#import "CaptureAdjustmentsController.h"
#import "MGCaptureAdjustmentsWindow.h"
#import "MGLibraryController.h"
#import "MGAppDelegate.h"

@interface CaptureController ()

- (void)populateToolstripForDevice:(CaptureDevice *)device;


- (void)showCapturedImageViewWithHelp:(BOOL)showHelp showSwitchToPreviewButton:(BOOL)showSwitchButton;
- (void)showQTpreview;
- (void)showOGLPreview;

- (void)createAdjustmentsHUD;
- (void)stopChoosingWhitePoint;

- (void)showAdjustmentsUI;
- (void)hideAdjustmentsUI;
- (void)updateAdjustmentsForNewDevice;

- (void)_loadExperiments;


@end




@implementation CaptureController

@synthesize sourceListTreeController = sourceListTreeController_;


#pragma mark -
#pragma mark Init/dealloc


- (id)initWithSuperview:(NSView *)superview;
{
	self = [super init];
	if(self)
	{
		
		if([NSBundle loadNibNamed:@"Capture" owner:self])
		{

			[view_ setFrame:[superview frame]];
			[superview addSubview:view_];
			
			metadataName_ = [@"" retain];
			metadataNameUseSeriesNumber_ = YES;
			metadataSeriesNumber_ = 1;
			
			adjustmentsController_ = [[CaptureAdjustmentsController alloc] init];
			
									
		}
	}
	return self;
}


- (void)awakeFromNib
{
	[historyBrowserView_ setZoomValue:1.00];
	
	[historyBrowserView_ setContentResizingMask:NSViewWidthSizable];

	
	[historyBrowserView_ setDataSource:self];
	[historyBrowserView_ setDelegate:self];

	[historyBrowserView_ setCellsStyleMask:IKCellsStyleShadowed];

	
	[historyBrowserView_ setDraggingDestinationDelegate:self];
	
	[historyBrowserView_ setValue:[NSColor whiteColor] forKey:IKImageBrowserSelectionColorKey];
	[historyBrowserView_ setValue:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Lucida Grande" size:10], NSFontAttributeName, [NSColor whiteColor], @"NSColor", nil] forKey:IKImageBrowserCellsTitleAttributesKey];
	[historyBrowserView_ setValue:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Lucida Grande" size:10], NSFontAttributeName, [NSColor whiteColor], @"NSColor", nil] forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];

	
	[historyBrowserView_ setValue:[NSColor colorWithCalibratedRed:0./255. green:0./255. blue:170./255. alpha:1] forKey:IKImageBrowserSelectionColorKey];
		
	[[[historyBrowserView_ enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[historyBrowserView_ enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];

	
	
	CALayer *patternLayer = [CALayer layer];
	NSImage *image = [NSImage imageNamed:@"captureImageBrowserBackground"];
	patternLayer.contents = (id)[[[image representations] objectAtIndex:0] CGImage];
	[historyBrowserView_ setBackgroundLayer:patternLayer];
	
	
	[historyBrowserView_ setAnimates:NO];
	
	oglCaptureView_.delegate = self;
	
	
	[[exposureSlider_ cell] setControlTint:NSGraphiteControlTint];
	[[brightnessSlider_ cell] setControlTint:NSGraphiteControlTint];
	[[contrastSlider_ cell] setControlTint:NSGraphiteControlTint];
	[[saturationSlider_ cell] setControlTint:NSGraphiteControlTint];
	[[exposureAutoCheckbox_ cell] setControlTint:NSGraphiteControlTint];
	[[whitePointAutoCheckbox_ cell] setControlTint:NSGraphiteControlTint];
    
    exposureSlider_.delegate = self;
	
}

- (void)dealloc
{	
	[adjustmentsController_ release];
	
	[super dealloc];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"arrangedObjects"]) {
		[historyBrowserView_ reloadData];
		[[historyBrowserView_ enclosingScrollView] setFrame:[[historyBrowserView_ enclosingScrollView] frame]];
	} 
	
	else if ([keyPath isEqualToString:@"selection"]) {
		ImageArrayController *imageArrayController = [MGLibraryControllerInstance imageArrayController];
		
		[historyBrowserView_ setSelectionIndexes:[imageArrayController selectionIndexes] byExtendingSelection:NO];
	
	}
	
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}



- (void)removeFilterPredicate;
{
	
}

- (void)setDevice:(CaptureDevice *)device;
{
	if (device != device_) {
		
		[device_ stopSession];
		[device_ release];
		device_ = [device retain];
	}
	
	if (!device_)
		return;
	
	[self createAdjustmentsHUD];
	
	[self updateAdjustmentsForNewDevice];
	
	device_.delegate = self;
	
	
	if ([device canTakePicture]) {
		[infoBarText_ setStringValue:@"Click the Capture button to take a picture."];
	}
	else {
		[infoBarText_ setStringValue:@"Press the shutter button on the camera to take a picture."];
	}
	
	
	[device_ startSession];
	
	
	 if ([device_ previewType] == CapturePreviewOpenGL || [device_ previewType] == CapturePreviewQT) {
		
		[self showOGLPreview];
		[device_ connectPreviewToView:oglCaptureView_];
		
	}
	
	else {
		
		[self showCapturedImageViewWithHelp:YES showSwitchToPreviewButton:NO];
		
	}
	
	
	[self populateToolstripForDevice:device_];
	
}


- (void)stopSessions;
{
	[device_ stopSession];
	
	if ([device_ previewType] == CapturePreviewQT)
		[device_ disconnectPreviewFromView:qtCapturePreview_];
	
	else if ([device_ previewType] == CapturePreviewOpenGL)
		[device_ disconnectPreviewFromView:oglCaptureView_];
	
}

- (void)open
{
	if (isOpen_)
		return;
	
	ImageArrayController *imageArrayController = [MGLibraryControllerInstance imageArrayController];
	[imageArrayController addObserver:self forKeyPath:@"arrangedObjects" options:0 context:nil];
	[imageArrayController addObserver:self forKeyPath:@"selection" options:0 context:nil];
	
	
	[self showAdjustmentsUI];
	[self removeFilterPredicate];
	
	isOpen_ = YES;
}

- (void)close
{
	if (!isOpen_)
		return;
	
	ImageArrayController *imageArrayController = [MGLibraryControllerInstance imageArrayController];
	[imageArrayController removeObserver:self forKeyPath:@"arrangedObjects"];
	[imageArrayController removeObserver:self forKeyPath:@"selection"];

	[self hideAdjustmentsUI];
	[self closeAdjustmentsHUD];
	
	[self stopSessions];
	[self setDevice:nil];
	
	isOpen_ = NO;
}

#pragma mark -
#pragma mark Actions

- (IBAction)takePicture:(id)sender
{
	[device_ takePicture];
}

- (IBAction)downloadAll:(id)sender
{
	[device_ downloadAllItems];
}


- (IBAction)downloadCurrent:(id)sender
{
	[device_ downloadCurrentItem];
}

#pragma mark -
#pragma mark UI update

- (void)populateToolstripForDevice:(CaptureDevice *)device
{
	[toolStrip_ removeItems];
	adjustmentsButton_ = nil;

	NSImage *setMD = [NSImage imageNamed:@"capture_toolstrip_autoname"];
	NSImage *importAll = [NSImage imageNamed:@"capture_toolstrip_importall"];
	NSImage *import = [NSImage imageNamed:@"capture_toolstrip_importall"];
	NSImage *takePicture = [NSImage imageNamed:@"capture_toolstrip_takepicture"];
	NSImage *adjustments = [NSImage imageNamed:@"capture_toolstrip_adjust"];

	[setMD setSize:NSMakeSize(32, 32)];
	[importAll setSize:NSMakeSize(32, 32)];
	[import setSize:NSMakeSize(32, 32)];
	[takePicture setSize:NSMakeSize(32, 32)];	
	[adjustments setSize:NSMakeSize(32, 32)];	

	NSButton *setMDButton = [NSButton toolStripButtonWithImage:setMD title:[NSString stringWithFormat:@"Auto-Metadata"]];
	NSButton *importAllButton = [NSButton toolStripButtonWithImage:importAll title:[NSString stringWithFormat:@"Import All (%i)", (int)[device numberOfStoredItems]]];
	NSButton *importButton = [NSButton toolStripButtonWithImage:import title:[NSString stringWithFormat:@"Import"]];
	NSButton *takePictureButton = [NSButton toolStripButtonWithImage:takePicture title:@"Capture"];
	NSButton *adjustmentsButton = [NSButton toolStripButtonWithImage:adjustments title:@"Adjustments"];

	[setMDButton setEnabled:YES];
	[adjustmentsButton setEnabled:YES];
	[importAllButton setEnabled:[device hasStoredItems]];
	[importButton setEnabled:([device hasStoredItems])];
	[takePictureButton setEnabled:[device canTakePicture]];
	
	[setMDButton setToolTip:@"Automatically set the image name for newly captured images"];
	if ([device canTakePicture])
		[takePictureButton setToolTip:@"Take a picture with the device and import it into Macnification"];
	else
		[takePictureButton setToolTip:@"Your camera cannot be controlled from within Macnification.  Please use the shutter button on the camera"];
	[importAllButton setToolTip:@"Import all images already present on the camera"];
	[adjustmentsButton setToolTip:@"Change camera capture settings"];

	
	[setMDButton setTarget:self];
	[importAllButton setTarget:self];
	[importButton setTarget:self];
	[takePictureButton setTarget:self];	
	[adjustmentsButton setTarget:self];	
	
	
	[setMDButton setAction:@selector(showMetadataSheet:)];
	[toolStrip_ addToolView:setMDButton withAlignment:MGLeftViewAlignment];
	[toolStrip_ addSeparatorWithAlignment:MGLeftViewAlignment];
	
	if ([device isMemberOfClass:[CaptureICDevice class]]) {
		
		[importAllButton setAction:@selector(downloadAll:)];
		[importButton setAction:@selector(downloadCurrent:)];
		[takePictureButton setAction:@selector(takePicture:)];
		
		[toolStrip_ addToolView:takePictureButton withAlignment:MGLeftViewAlignment];
		[toolStrip_ addWhitespaceWithAlignment:MGCenterViewAlignment];
		[toolStrip_ addToolView:importAllButton withAlignment:MGRightViewAlignment];
	} else {
		
		[takePictureButton setAction:@selector(takePicture:)];
		[toolStrip_ addToolView:takePictureButton withAlignment:MGLeftViewAlignment];
	}

	if (![device_ isKindOfClass:[CaptureICDevice class]]) {
		adjustmentsButton_ = adjustmentsButton;
		[adjustmentsButton setAction:@selector(showAdjustmentsHUD:)];	
		[toolStrip_ addToolView:adjustmentsButton withAlignment:MGLeftViewAlignment];
	}
	

}



- (void)showCapturedImageViewWithHelp:(BOOL)showHelp showSwitchToPreviewButton:(BOOL)showSwitchButton
{
	[adjustmentsButton_ setEnabled:NO];
	[oglCaptureView_ setHidden:YES];
	[historyImageView_ setHidden:NO];
	[infoBarView_ setHidden:!showHelp];
	[liveViewButton_ setHidden:!showSwitchButton];
	
	[qtCapturePreview_ setHidden:YES];
}

- (void)showOGLPreview
{
	[adjustmentsButton_ setEnabled:YES];
	[oglCaptureView_ setHidden:NO];
	[historyImageView_ setHidden:YES];
	[infoBarView_ setHidden:YES];
	[liveViewButton_ setHidden:YES];

	[qtCapturePreview_ setHidden:YES];
	
	[oglCaptureView_ drawFrame];
	
}

- (void)showQTpreview
{
	[adjustmentsButton_ setEnabled:YES];

	[oglCaptureView_ setHidden:YES];
	[historyImageView_ setHidden:YES];
	[infoBarView_ setHidden:YES];
	[liveViewButton_ setHidden:YES];

	[qtCapturePreview_ setHidden:NO];
}

- (IBAction)showLivePreview:(id)sender
{
	// easiest is to deselect all items from the browser
	[historyBrowserView_ setSelectionIndexes:[NSIndexSet indexSet] byExtendingSelection:NO];
}


#pragma mark -
#pragma mark Image adjustments

- (void)updateAdjustmentsForNewDevice
{
    [exposureSlider_ setMinValue:0.];
    [exposureSlider_ setMaxValue:1.];
    
    CaptureExposureMode modes = [device_ exposureModes];
    
    [exposureSlider_ setFloatValue:[device_ exposure]];

    
	if (modes & CaptureExposureModeManual) {
		
        // based on exposure *TIME*.
        
        [device_ resetExposure];
        [exposureAutoCheckbox_ setEnabled:YES];
        [exposureAutoCheckbox_ setState:[device_ autoExposure] ? NSOnState : NSOffState];
        [exposureSlider_ setEnabled:![device_ autoExposure]];


		        
	} else {
        
		// +/-EV
		[exposureSlider_ setFloatValue:0.5];
        [exposureSlider_ setEnabled:YES];
        [exposureAutoCheckbox_ setEnabled:NO];
		
		adjustmentsController_.exposure = 0.0;
	}
	
}

- (void)showAdjustmentsUI
{
	[adjustmentsView_ setFrame:[controlsEmbeddingView_ bounds]];
	[controlsEmbeddingView_ addSubview:adjustmentsView_];
}

- (void)hideAdjustmentsUI
{

}

- (void)showAdjustmentsHUD:(id)sender
{
	
	if (adjustmentsHUDisVisible_) {
		[self closeAdjustmentsHUD];
		return;
	}
		
	if (!adjustmentsHUD_)
		[self createAdjustmentsHUD];
	
	if ([sender isKindOfClass:[NSButton class]]) {
		NSRect buttonRectInBaseCoords = [sender convertRectToBase:[(NSView *)sender bounds]];
		NSPoint dstPointInBase = NSMakePoint(NSMidX(buttonRectInBaseCoords), NSMaxY(buttonRectInBaseCoords));
		NSPoint dstPointInScreen = [[sender window] convertBaseToScreen:dstPointInBase];
		
		dstPointInScreen.y += 5;
		[adjustmentsHUD_ showWithArrowAtPoint:dstPointInScreen];
	}
    
    [adjustmentsHUD_ setFloatingPanel:YES];

	
	[[toolStrip_ window] addChildWindow:adjustmentsHUD_ ordered:NSWindowAbove];
	[adjustmentsHUD_ makeKeyAndOrderFront:self];
	
	adjustmentsHUDisVisible_ = YES;
	
}

- (void)_positionAdjustmentsHUDAtPoint:(NSPoint)point
{
	
}


- (void)createAdjustmentsHUD
{	

	
}


- (void)updateAdjustmentsHUDPosition
{
    if (![adjustmentsHUD_ isAttached])
        return;
    
    NSRect buttonRectInBaseCoords = [adjustmentsButton_ convertRectToBase:[adjustmentsButton_ bounds]];
    NSPoint dstPointInBase = NSMakePoint(NSMidX(buttonRectInBaseCoords), NSMaxY(buttonRectInBaseCoords));
    NSPoint dstPointInScreen = [[adjustmentsButton_ window] convertBaseToScreen:dstPointInBase];
    
    dstPointInScreen.y += 5;
    [adjustmentsHUD_ showWithArrowAtPoint:dstPointInScreen];
}

- (void)closeAdjustmentsHUD
{
	if (isPickingWhitePoint_)
		return;
	
	[[adjustmentsHUD_ parentWindow] removeChildWindow:adjustmentsHUD_];
	[adjustmentsHUD_ orderOut:self];		
	adjustmentsHUDisVisible_ = NO;
	
	[self stopChoosingWhitePoint];
}

- (CIImage *)captureDevice:(CaptureDevice *)device adjustedImage:(CIImage *)original;
{
	return [adjustmentsController_ filterImage:original];
}

#pragma mark -

- (IBAction)setBrightness:(id)sender;
{
	adjustmentsController_.brightness = [brightnessSlider_ floatValue];
}

- (IBAction)setContrast:(id)sender;
{
	adjustmentsController_.contrast = [contrastSlider_ floatValue];

}

- (IBAction)setSaturation:(id)sender;
{
	adjustmentsController_.saturation = [saturationSlider_ floatValue];
}

- (IBAction)setExposure:(id)sender;
{
    CaptureExposureMode modes = [device_ exposureModes];

	if (modes & CaptureExposureModeManual) {
		[adjustmentsController_ setUsesExposure:NO];

		[device_ setExposure:[sender floatValue]];	
	} 
    else {
		[adjustmentsController_ setUsesExposure:YES];
		adjustmentsController_.exposure = [sender floatValue];
	}
}

- (IBAction)setAutoExposure:(id)sender;
{
	if ([(NSCell *)sender state] == NSOnState) {

		[exposureSlider_ setEnabled:NO];
		[device_ setAutoExposure:YES];
		
	} else {
		
		[exposureSlider_ setEnabled:YES];
        [device_ setAutoExposure:NO];
        [exposureSlider_ setFloatValue:[device_ exposure]];

	}
}

- (IBAction)resetAdjustmentValues:(id)sender;
{
	[adjustmentsController_ resetDefaults];
	
	[saturationSlider_ setFloatValue:adjustmentsController_.saturation];
	[brightnessSlider_ setFloatValue:adjustmentsController_.brightness];
	[contrastSlider_ setFloatValue:adjustmentsController_.contrast];
	
    CaptureExposureMode modes = [device_ exposureModes];
    
    if (modes & CaptureExposureModeManual) {
        [device_ resetExposure];
        [exposureSlider_ setFloatValue:[device_ exposure]];

    }
    else
        [exposureSlider_ setFloatValue:0.5];

	[exposureAutoCheckbox_ setState:NSOffState];
	[exposureSlider_ setEnabled:YES];
}

- (IBAction)chooseWhitePoint:(id)sender;
{
	if ([(NSCell *)sender state] == NSOnState) {
		//[whitePointButton_ setState:NSOffState];
		isPickingWhitePoint_ = YES;
	}
	else
		isPickingWhitePoint_ = NO;
	
	[(NSCursor *)[[NSCursor alloc] initWithImage:[NSImage imageNamed:@"targetCursor"] hotSpot:NSMakePoint(11, 11)] set];	
}

- (void)view:(NSView *)view wasPickedAtPoint:(NSPoint)point;
{
	if (isPickingWhitePoint_) {
		adjustmentsController_.whitePoint = point;
	}
	
	[self stopChoosingWhitePoint];
}

- (void)stopChoosingWhitePoint
{
	[whitePointButton_ setState:NSOffState];
	isPickingWhitePoint_ = NO;
}

- (NSString *)valueToolTipForHUDSlider:(MGHUDSlider *)slider
{
    if (slider != exposureSlider_)
        return nil;
    
    return [device_ exposureDescription];
}

#pragma mark -
#pragma mark Metadata

- (IBAction)showMetadataSheet:(id)sender;
{
	[metadataNameField_ setStringValue:metadataName_];
    
    [self _loadExperiments];
    
	[NSApp beginSheet:metadataSheet_ modalForWindow:[historyBrowserView_ window] modalDelegate:nil didEndSelector:nil contextInfo:nil];
	
}

- (IBAction)endMetadataSheet:(id)sender;
{
	[NSApp endSheet:metadataSheet_];
	[metadataSheet_ orderOut:self];
	
	// cancel
	if ([sender tag] == 0) {
		[metadataNameField_ setStringValue:metadataName_];
		return;
	}
    
    
    NSMenuItem *selectedExpItem = [metadataExperimentsPopup_ selectedItem];
    Experiment *exp = [selectedExpItem representedObject];
    
    [selectedExperiment_ release];
    selectedExperiment_ = [exp retain];
    
    
	
	// if the "use series number" is different from the previous value, reset the counter
	if (metadataNameUseSeriesNumber_ != ([metadataNameUseSeriesNumberButton_ state] == NSOnState))
		metadataSeriesNumber_ = 1;
	
	metadataNameUseSeriesNumber_ = ([metadataNameUseSeriesNumberButton_ state] == NSOnState);
	
	if ([metadataName_ isEqualToString:[metadataNameField_ stringValue]])
		return;
	
	[metadataName_ release];
	metadataName_ = [[metadataNameField_ stringValue] copy];
	metadataSeriesNumber_ = 1;
    


    
}



- (void)_loadExperiments
{
    [metadataExperimentsPopup_ removeAllItems];
    
    if ([selectedExperiment_ isFault]) {
        [selectedExperiment_ release];
        selectedExperiment_ = nil;
    }
    
    
    NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
    [req setEntity:[NSEntityDescription entityForName:@"Experiment" inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]]];
    [req setSortDescriptors:[NSArray arrayWithObjects:
                             [NSSortDescriptor sortDescriptorWithKey:@"experimentName" ascending:YES],
                             [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES], nil]];
    
    NSArray *experiments = [[MGLibraryControllerInstance managedObjectContext] executeFetchRequest:req error:nil];
    
    NSMenu *menu = [[[NSMenu alloc] initWithTitle:@""] autorelease];

    [menu addItem:[[[NSMenuItem alloc] initWithTitle:@"None" action:nil keyEquivalent:@""] autorelease]];
    [menu addItem:[NSMenuItem separatorItem]];
    
    
    for (Experiment *exp in experiments) {
        
        NSString *name = [exp valueForKey:@"experimentName"] ? [exp valueForKey:@"experimentName"] : @"untitled experiment";
        
        NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:name action:nil keyEquivalent:@""] autorelease];
        [item setRepresentedObject:exp];
        
        if (exp == selectedExperiment_)
            [item setState:NSOnState];
        
        [menu addItem:item];
        
    }
    
    [metadataExperimentsPopup_ setMenu:menu];
    
}


#pragma mark -
#pragma mark Capture device delegate

- (void)captureDeviceDidOpenSession:(CaptureDevice *)device;
{
	[self populateToolstripForDevice:device];
}

- (void)captureDevice:(CaptureDevice *)device didReceivePicturesForImport:(NSArray *)paths;
{		
	// We're responsible for removing the files ourselves!
    // So retain the array
    
    if (!filesBeingImported_)
        filesBeingImported_ = [[NSMutableArray array] retain];
    
    [filesBeingImported_ addObjectsFromArray:paths];
        
    
	// import it...		
	MGImageBatchImporter *imp = [[[MGImageBatchImporter alloc] init] autorelease];
	imp.showsProgressSheet = NO;
	imp.album = nil;
	imp.libraryAlbum = [[MGLibraryControllerInstance library] libraryGroup];
	imp.lastImportAlbum = [[MGLibraryControllerInstance library] lastImportGroup];
	imp.projectsAlbum = [[MGLibraryControllerInstance library] projectsCollection];
    imp.delegate = self;
	
	[imp importImageFilesAndDirectories:paths];
}


- (void)imageImporter:(MGImageBatchImporter *)importer didImportImages:(NSArray *)images;
{
    /////////////////////////////////////////////////////////
	// Add metadata
	
	NSString *baseName;
	
	if (metadataName_ && [metadataName_ length])
		baseName = [[metadataName_ copy] autorelease];
	else
		baseName = @"Captured Image";
	
	
	for (Image *image in images) {
		
		NSString *name = baseName;
		
		if (metadataNameUseSeriesNumber_)
			name = [baseName stringByAppendingFormat:@" - %i", (int)metadataSeriesNumber_];
        
		[image setValue:name forKey:@"name"];
		
		metadataSeriesNumber_++;
	}
	
    
    if ([selectedExperiment_ isFault]) {
        [selectedExperiment_ release];
        selectedExperiment_ = nil;
    }
    
    
    for (Image *image in images)
        [image setValue:selectedExperiment_ forKey:@"experiment"];

    
    
    
	
	/////////////////////////////////////////////////////////
    
		
	NSMutableSet *capturedImages = [[[MGLibraryControllerInstance library] lastCaptureGroup] mutableSetValueForKey:@"images"];
	[capturedImages addObjectsFromArray:images];
	
	[historyBrowserView_ setSelectionIndexes:[NSIndexSet indexSetWithIndex:[self numberOfItemsInImageBrowser:historyBrowserView_] - 1] byExtendingSelection:NO];
	
	[historyBrowserView_ setAnimates:NO];
    
    
    /////////////////////////////////////////////////////////

    
    for (NSString *path in filesBeingImported_)
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    
    [filesBeingImported_ removeAllObjects];
    
}



- (IBAction)toggleMDSplitView:(id)sender;
{
    
	[MGLibraryControllerInstance toggleMdSplitView:sender];
}


#pragma mark -
#pragma mark IKIB datasource

- (NSUInteger) numberOfItemsInImageBrowser:(IKImageBrowserView *) aBrowser;
{		
	ImageArrayController *imageArrayController = [MGLibraryControllerInstance imageArrayController];

	return [[imageArrayController arrangedObjects] count];
}


- (id) imageBrowser:(IKImageBrowserView *) aBrowser itemAtIndex:(NSUInteger)index;
{
	ImageArrayController *imageArrayController = [MGLibraryControllerInstance imageArrayController];

	return [[imageArrayController arrangedObjects] objectAtIndex:index];
}

# pragma mark -
# pragma mark IKImageBrowserView delegate

- (void) imageBrowserSelectionDidChange:(IKImageBrowserView *) aBrowser;
{
	ImageArrayController *ctl = [MGLibraryControllerInstance imageArrayController];
	[ctl setSelectionIndexes:[historyBrowserView_ selectionIndexes]];
	
	if ([[ctl selectionIndexes] count] == 1) {
		Image *img = [[ctl selectedObjects] objectAtIndex:0];
		[historyImageView_ setImage:[img filteredImage]];
	} else {
		[historyImageView_ setImage:nil];
	}
	
	if (([device_ previewType] == CapturePreviewOpenGL || [device_ previewType] == CapturePreviewQT) && [[historyBrowserView_ selectionIndexes] count] == 0) {
		[self showOGLPreview];
	}
	else
		[self showCapturedImageViewWithHelp:([[historyBrowserView_ selectionIndexes] count] == 0) showSwitchToPreviewButton:([device_ previewType] != CapturePreviewNone)];

	if ([[historyBrowserView_ selectionIndexes] count] == 0)
		[self showAdjustmentsUI];
	else
		[self hideAdjustmentsUI];
	
}


- (void)imageBrowser:(IKImageBrowserView *)aBrowser cellWasDoubleClickedAtIndex:(NSUInteger) index;
{
	[[MGLibraryControllerInstance imageArrayController] enterFullScreen];
}

- (NSUInteger)imageBrowser:(IKImageBrowserView *)aBrowser writeItemsAtIndexes:(NSIndexSet *)itemIndexes toPasteboard:(NSPasteboard *)pasteboard;
{
	return [[MGLibraryControllerInstance imageArrayController] imageBrowser:aBrowser writeItemsAtIndexes:itemIndexes toPasteboard:pasteboard];
}

- (void)imageBrowser:(IKImageBrowserView *)view removeItemsAtIndexes:(NSIndexSet *)indexes
{
	[[MGLibraryControllerInstance imageArrayController] remove:self];
}

@end
