//
//  NSImageRep_MGAdditions.h
//  LightTable
//
//  Created by Dennis Lorson on 29/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSImageRep (MGAdditions)


- (CGFloat)dpiX;
- (CGFloat)dpiY;
- (NSSize)dpi;

- (void)setDpiX:(CGFloat)dpi;
- (void)setDpiY:(CGFloat)dpi;
- (void)setDpi:(NSSize)dpi;



@end
