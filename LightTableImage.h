//
//  LightTableImage.h
//  LightTable
//
//	The model object for a light table image.
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KSExtensibleManagedObject.h"


static const CGFloat defaultImageWidth = 60.0;			// the dimensions used when an image object is added to the light table for the first time
static const CGFloat defaultImageHeight = 45.0;


@interface LightTableImage : KSExtensibleManagedObject 
{

}

@end
