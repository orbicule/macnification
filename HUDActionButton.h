//
//  HUDActionButton.h
//  Filament
//
//  Created by Peter Schols on 29/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface HUDActionButton : NSButton {

}


- (void)displayMenu:(NSEvent *)theEvent;


@end
