//
//  KeywordAnimationController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 4 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "KeywordAnimationView.h"

@interface KeywordAnimationController : NSWindowController {
	
	IBOutlet KeywordAnimationView *keywordAnimationView;
}


- (void)startAnimation:(NSString *)animation withSymbol:(NSString *)symbol;
- (void)setRectsArray:(NSArray *)array browserFrame:(NSRect)windowFrame;




@end
