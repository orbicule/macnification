//
//  CaptureOpenGLPreview.h
//  Filament
//
//  Created by Dennis Lorson on 25/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGL/OpenGL.h>
#import <CoreVideo/CoreVideo.h>

@protocol ViewPickerDelegate <NSObject>

- (void)view:(NSView *)view wasPickedAtPoint:(NSPoint)point;

@end

@interface CaptureOpenGLPreview : NSOpenGLView 
{
	NSSize frameSize_;
	GLuint frameTexture_;
	
	CIImage *image_;
    CVImageBufferRef imageBacking_;
	CIContext *ciCtx_;
	
	id <ViewPickerDelegate> delegate_;
	
	GLuint shadowTexture_;
}

@property (readwrite, assign) id <ViewPickerDelegate> delegate;


- (void)drawFrame;
- (void)setImage:(unsigned char *)image width:(NSInteger)w height:(NSInteger)h;
- (void)setImage:(CIImage *)image;
- (void)setImage:(CIImage *)image backingBuffer:(CVImageBufferRef)buffer;

@end
