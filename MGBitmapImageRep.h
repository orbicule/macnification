//
//  NSBitmapImageRep_MGAdditions.h
//  StackTable
//
//  Created by Dennis Lorson on 29/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGBitmapImageRep : NSBitmapImageRep
{
    unsigned char *ucharBitmapData_;



}

- (void)pixelData:(unsigned char **)data count:(NSInteger *)pixelCount fromPoint:(NSPoint)first toPoint:(NSPoint)second;





@end
