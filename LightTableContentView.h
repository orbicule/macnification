//
//  LightTableContentView.h
//  LightTable
//
//	This class handles the contents of the light table (images, background) and most of the interaction between those images.
//  It has subviews of the LightTableImageView class.
//	It is a subview of a LightTableView.
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class LightTableImageView;
@class LightTableCommentView;
@class LightTableImage;

extern CGFloat MGTableMargin;



typedef enum _LayoutSide
{
	LayoutMinX = 1,
	LayoutMinY = 2,
	LayoutMaxX = 3,
	LayoutMaxY = 4
} LayoutSide;


@interface LightTableContentView : NSView 
{

	NSMenu *imageViewContextualMenu, *tableContextualMenu;
	
	NSMutableArray *imageViews;									// the subview imageviews that the light table is currently displaying.
	
	NSArrayController *lightTableArrayController;				// weak ref (otherwise retain loop)
	NSArrayController *lightTableImageArrayController;			// weak ref (otherwise retain loop)
	NSArrayController *imageArrayController;
	
	BOOL isDragging;											// is the user dragging the mouse? (starting in this view)
	NSPoint dragStartPoint;										// the point where the user started dragging
	
	NSWindow *commentWindow;
	LightTableCommentView *commentView;
	
	CGFloat gridOpacity;
	CGFloat gradientOpacity;
	CGFloat zoomLevel;
	BOOL drawsGrid;
	
	BOOL hidesEventsFromSubviews;								// determine whether this content view does not allow its subviews to receive events (e.g. when dragging a print frame rectangle)
	BOOL printRectangleMode;									// determines if the user is trying to drag a rectangle for the print frame.
	LightTableImageView *lastClickedView;						// the view that was last clicked on.
	

}

@property(readonly) BOOL isDragging;
@property(nonatomic) CGFloat zoomLevel;
@property(nonatomic, assign) NSMenu *imageViewContextualMenu, *tableContextualMenu;
@property(nonatomic, assign) LightTableCommentView *commentView;
@property(nonatomic, assign) LightTableImageView *lastClickedView;

- (void)addImageView:(LightTableImageView *)theView;
- (void)removeImageView:(LightTableImageView *)theView;

- (BOOL)subview:(LightTableImageView *)theView shouldBeSelectedWithEvent:(NSEvent *)theEvent;

- (void)reassignControlPointsRectIfNeededWithEvent:(NSEvent *)theEvent;

- (void)enterPrintRectangleMode;
- (void)leavePrintRectangleMode;

- (IBAction)resizeToMatchSelected:(id)sender;
- (IBAction)resizeToRealSize:(id)sender;
- (IBAction)resizeToPreviousSize:(id)sender;
- (IBAction)resizeToMatchSelectedUsingMagnification:(id)sender;

- (IBAction)distributeHorizontally:(id)sender;
- (IBAction)distributeVertically:(id)sender;

- (IBAction)alignTopHorizontally:(id)sender;
- (IBAction)alignTopVertically:(id)sender;
- (IBAction)alignBottomHorizontally:(id)sender;
- (IBAction)alignBottomVertically:(id)sender;

- (IBAction)showCommentViewForSelectedView:(id)sender;

- (IBAction)cancelCommentView:(id)sender;
- (IBAction)commitCommentView:(id)sender;

- (void)setOriginPositionsForImages:(NSArray *)lightTableImages gridCenterPoint:(NSPoint)location;
- (void)offsetImageOriginsWithPoint:(NSPoint)theOffset eventView:(LightTableImageView *)sender;
- (void)arrangeImageViewsInGrid:(NSArray *)views animated:(BOOL)animated;

- (void)shouldRemoveSelectedObjects;
- (int)numberOfSelectedViews;
- (LightTableImageView *)imageViewForImage:(LightTableImage *)image;
- (void)selectNextImageView:(LightTableImageView *)previous;
- (void)selectPreviousImageView:(LightTableImageView *)original;
- (void)selectAll;


- (NSRect)boundingBoxForImageViews:(NSArray *)theViews;
- (NSRect)imageViewsBoundingBox;
- (NSRect)selectedImageViewsBoundingBox;
- (BOOL)viewOverlapsWithOtherImageView:(LightTableImageView *)view;

- (NSArray *)imageViews;


- (void)setDrawsGrid:(BOOL)flag;

- (NSImage *)compositeImageFromRect:(NSRect)rect constrainToImageBounds:(BOOL)constrain sideMargins:(NSSize)margins;


@end
