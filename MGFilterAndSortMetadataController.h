//
//  MGFilterAndSortMetadataController.h
//  Filament
//
//  Created by Dennis Lorson on 24/09/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGFilterAndSortMetadataController : NSObject
{

}

+ (MGFilterAndSortMetadataController *)sharedController;


- (NSArray *)searchAttributeKeyPaths;
- (NSArray *)exportAttributeKeyPaths;
- (NSArray *)sortAttributeKeyPaths;
- (NSArray *)predicateEditorRowTemplatesForFilterAttributes;

- (NSString *)humanReadableNameForAttributeKeyPath:(NSString *)keyPath;
- (NSPredicate *)searchPredicateForAttributeKeyPath:(NSString *)keyPath prefix:(NSString *)prefix searchString:(NSString *)searchString;

- (void)adjustSmartAlbumPredicate:(NSPredicate *)predicate fetchPredicate:(NSPredicate **)fetchPredicate inMemoryPredicate:(NSPredicate **)inMemoryPredicate compoundType:(NSCompoundPredicateType *)compoundType;


@end
