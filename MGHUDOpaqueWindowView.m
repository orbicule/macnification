//
//  MGHUDOpaqueWindowView.m
//  Filament
//
//  Created by Dennis Lorson on 18/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDOpaqueWindowView.h"


@implementation MGHUDOpaqueWindowView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect 
{
	NSRect destRect = NSInsetRect([self bounds], 1, 1);
	
	NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:destRect xRadius:5. yRadius:5.];
	
	destRect.origin.y += 11;
	destRect.size.height -= 10;
	
	NSBezierPath *path2 = [NSBezierPath bezierPathWithRect:destRect];

    [[NSColor colorWithCalibratedWhite:37./255. alpha:1.0] set];
	[path fill];
	[path2 fill];
	
}



- (void)mouseDown:(NSEvent *)theEvent
{
	[super mouseDown:theEvent];
	[[self window] makeFirstResponder:nil];
	
}

@end
