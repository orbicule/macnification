//
//  LightTableContentView.m
//  LightTable
//
//	The main light table view.
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableContentView.h"
#import "LightTableImageView.h"
#import "LightTableImage.h"
#import "LightTableCommentView.h"
#import "LightTable.h"
#import "LightTableView.h"
#import "NSView_MGAdditions.h"
#import "NSImage_MGAdditions.h"
#import "LightTablePrintContentView.h"
#import "MGFunctionAdditions.h"
#import "LightTableAlignLayoutElement.h"
#import "Image.h"
#import "MGAppDelegate.h"
#import "KeywordTreeController.h"
#import "MGLibraryController.h"
#import "ImageArrayController.h"

#import <QuartzCore/QuartzCore.h>

CGFloat MGTableMargin = 7.0;


@interface NSEvent (NSEventPrivateStuffThatReallyShouldNotBePrivate)

- (CGFloat)magnification;

@end




@interface LightTableContentView (Private)

- (LightTableImageView *)imageViewWithLightTableImage:(LightTableImage *)ltImage;

- (LightTableImageView *)controlPointsViewForLocation:(NSPoint)theLocation;

- (void)drawGridWithSize:(NSSize)theSize inRect:(NSRect)rect;

- (void)updateDragRectWithEvent:(NSEvent *)theEvent;
- (NSArray *)imagesInRectOfContentView:(NSRect)rect;

- (void)setGridOpacity:(CGFloat)opacity;
- (void)setGradientOpacity:(CGFloat)opacity;

- (void)showCommentViewForView:(LightTableImageView *)theView;
- (void)initializeCommentViewItems;

- (NSRect)commentWindowFrameForView:(LightTableImageView *)theView;
- (NSRect)convertSubviewRectToScreen:(NSRect)frame;

- (void)animateToNewFrame:(NSRect)newFrame ofView:(LightTableImageView *)view withDuration:(NSTimeInterval)duration updateFrame:(BOOL)updateFrame;
- (void)updateLightTableImage:(NSDictionary *)userInfo;

@end


@implementation LightTableContentView

@synthesize isDragging, zoomLevel, imageViewContextualMenu, commentView, lastClickedView, tableContextualMenu;


#pragma mark Init/dealloc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init/dealloc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
		imageViews = [[NSMutableArray array] retain];
		drawsGrid = NO;
		gradientOpacity = 1.0;
		gridOpacity = 0.0;
		zoomLevel = 1.0;
		
		[self registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, @"keywordsPboardType", nil]];
		
		
		// set the image array controller variable.
		// the light table will use the image array controller to keep the app array in sync with its own array of images, change the selection accordingly, etc.
		imageArrayController = [MGLibraryControllerInstance imageArrayController];
		
		
    }
    return self;
}

- (void)dealloc
{
	[imageViews release];
	
	[commentView release];
	[commentWindow release];
	
	[super dealloc];
}



#pragma mark Bindings/observing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bindings/observing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	NSKeyValueObservingOptions opt = (NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial);
	
	if ([binding isEqualToString:@"imageSelection"]) {
		// this binding is used for handling the selected image.
		// the object bound to is the LTimage array ctl
		lightTableImageArrayController = observableController;
		[observableController addObserver:self forKeyPath:keyPath options:opt context:nil];
		
	}
	
	else if ([binding isEqualToString:@"imageContent"]) {
		// this binding is used for handling the array of images that this view displays.
		lightTableImageArrayController = observableController;
		[observableController addObserver:self forKeyPath:keyPath options:opt context:nil];
		
	}
	
	else if ([binding isEqualToString:@"lightTableSelection"]) {
		// this binding is used to update all other light table properties like its name, when displayed in this view.
		lightTableArrayController = observableController;
		[observableController addObserver:self forKeyPath:keyPath options:opt context:nil];
		
	} 
	
	else {
		
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
		
	}
}

- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"imageSelection"]) {

		[lightTableArrayController removeObserver:self forKeyPath:@"selectedObjects"];
		
	}
	
	else if ([binding isEqualToString:@"imageContent"]) {

		[lightTableArrayController removeObserver:self forKeyPath:@"arrangedObjects"];
		
	}
	
	else if ([binding isEqualToString:@"lightTableSelection"]) {

		[lightTableArrayController removeObserver:self forKeyPath:@"selectedObjects"];
		
	} 
	
	else {
		
		[super unbind:binding];
		
	}
	
	
	
}
		
		
		

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"selectedObjects"] && object == lightTableImageArrayController) {
		// image selection changed
		
		// deselect first
		NSMutableArray *viewsToDeselect = [[imageViews mutableCopy] autorelease];
		// construct an array of imgviews whose model objects are still selected
		NSMutableArray *viewsToKeepSelected = [NSMutableArray array];
		for (LightTableImageView *view in imageViews) {
			if ([[lightTableImageArrayController selectedObjects] containsObject:view.lightTableImage]) {
				[viewsToKeepSelected addObject:view];
			}
		}

		[viewsToDeselect removeObjectsInArray:viewsToKeepSelected];
		
		for (LightTableImageView *view in viewsToDeselect) {
			view.selected = NO;
		}
		
		// now select according to the array ctl.
		for (LightTableImageView *view in imageViews) {
			view.selected = [[lightTableImageArrayController selectedObjects] containsObject:view.lightTableImage];
			
			// move the views to the front
			if ([[lightTableImageArrayController selectedObjects] containsObject:view.lightTableImage]) [self addSubview:view];
		}
	
		// now make sure that the light table view scrolls to center the selected images (if more than one selected)
		if ([[lightTableImageArrayController selectedObjects] count] > 0) {
			NSRect selectionBoundingBox = [self selectedImageViewsBoundingBox];
			[[self superview] scrollRectToVisible:[[self superview] convertRect:selectionBoundingBox fromView:self]];
		}
		
		
		// make the image array controller select the new selection too.
		NSMutableArray *newlySelectedImages = [NSMutableArray array];
		for (LightTableImage *ltImg in [lightTableImageArrayController selectedObjects]) {
			
			[newlySelectedImages addObject:[ltImg valueForKey:@"image"]];
			
		}
		
		[imageArrayController setSelectedObjects:newlySelectedImages];
		
		
	}
	else if ([keyPath isEqualToString:@"selectedObjects"] && object == lightTableArrayController) {
		// light table selection changed
		//NSLog(@"close workflow");
		[self leavePrintRectangleMode];
		((LightTableView *)[self superview]).printingRectMode = NO;
	
	}

	else if ([keyPath isEqualToString:@"arrangedObjects"] && object == lightTableImageArrayController) {
		
				
		
		// array of images in current light table changed
		
		// now remove the old ones
		NSMutableArray *viewsToRemove = [[imageViews mutableCopy] autorelease];
		
		// construct an array of imgviews whose model objects are still in the arrayctl
		NSMutableArray *viewsToRetain = [NSMutableArray array];
		for (LightTableImageView *view in imageViews) {
			if ([[lightTableImageArrayController arrangedObjects] containsObject:view.lightTableImage]) {
				[viewsToRetain addObject:view];
			}
		}
		
		[viewsToRemove removeObjectsInArray:viewsToRetain];
		
		for (LightTableImageView *view in viewsToRemove) {
			[self removeImageView:view];
		}
		
		
		// get the new images (that weren't there before)
		// The KVO change dictionaries don't work (even mmalc says so) so we have to use our own variable to keep track of the old image array.
		NSMutableArray *imagesToAdd = [[[lightTableImageArrayController valueForKey:@"arrangedObjects"] mutableCopy] autorelease];
		for (LightTableImageView *view in imageViews) {
			[imagesToAdd removeObject:view.lightTableImage];
		}
		
		// insert them
		NSMutableArray *insertedImages = [NSMutableArray array];

		NSMutableArray *imagesToArrange = [NSMutableArray array];

		NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"image.name" ascending:YES]];

		for (LightTableImage *ltImage in [imagesToAdd sortedArrayUsingDescriptors:sortDescriptors]) {
			LightTableImageView *newView = [self imageViewWithLightTableImage:ltImage];
			
			// if the ltImage has coords 0, 0, arrange it!
			if ([newView frame].origin.x == 0 && [newView frame].origin.y == 0)
				[imagesToArrange addObject:newView];
			
			[newView setAlphaValue:0.0];
			[self addImageView:newView];
			[insertedImages addObject:newView];
			
			
		}
		
		[NSAnimationContext beginGrouping];
		[[NSAnimationContext currentContext] setDuration:0.15];
		for (LightTableImageView *newView in insertedImages) {
			[[newView animator] setAlphaValue:1.0];
		}
		[NSAnimationContext endGrouping];
		
		
		// DO NOT update the table size here.  arrangedObjects update is called before the light table selection bindings update is called (weird!) so changing here would interfere with setting the
		// bounds when the selection bindings update is called.
		// not calling it here is not bad since every new object provokes a size update elsewhere.
		//[(LightTableView *)[self superview] updateFrameSize];
		
		// arrange all items that have no valid coordinate
		[self arrangeImageViewsInGrid:imagesToArrange animated:NO];
		
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:nil];
	}


}



- (NSArray *)exposedBindings
{
	return [NSArray arrayWithObjects:@"imageSelection", @"imageContent", @"lightTableSelection", nil];
}

/*
- (void)setBounds:(NSRect)bounds
{
	[super setBounds:bounds];
	
	NSLog(@"ctv setBounds :%@", NSStringFromRect(bounds));
}

- (void)setFrame:(NSRect)bounds
{
	[super setFrame:bounds];
	
	NSLog(@"ctv setFrame :%@", NSStringFromRect(bounds));
}*/


#pragma mark Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)drawRect:(NSRect)rect 
{
	//[[NSColor yellowColor] set];
	//[[NSColor colorWithCalibratedWhite:0.25 alpha:1.0] set];
	//NSRectFill([self bounds]);

	
	//NSGradient *gradient = [[[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.5 alpha:1.0] 
	//													  endingColor:[NSColor colorWithCalibratedWhite:0.2 alpha:1.0]] autorelease];

	
	//[gradient drawInRect:[self bounds] angle:90.0];

	// should be optimized by doing a quartz call instead of using bezier paths
	NSRect overlayRect = NSInsetRect([self bounds], MGTableMargin/zoomLevel, MGTableMargin/zoomLevel);
	NSBezierPath *overlay = [NSBezierPath bezierPathWithRoundedRect:overlayRect xRadius:2*MGTableMargin/zoomLevel yRadius:2*MGTableMargin/zoomLevel];
	//[[NSColor colorWithCalibratedWhite:0.2 alpha:1.0] set];
	[[NSColor colorWithCalibratedWhite:0.18 alpha:1.0] set];
	[overlay fill];
	//NSRectFill([self bounds]);
	
	[overlay addClip];

	// should be optimized by doing a quartz call instead of using bezier paths
	[self drawGridWithSize:NSMakeSize(3*MGTableMargin, 3*MGTableMargin) inRect:rect];
	

}

- (BOOL)isOpaque
{
	return NO;
}

- (void)drawGridWithSize:(NSSize)theSize inRect:(NSRect)rect
{
	return;
	//if (gridOpacity == 0.0) return;
	// draw a grid in the given rect.
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	CGFloat x = NSMinX([self bounds]);
	CGFloat y = NSMinY([self bounds]);
	
	while (x <= NSMaxX([self bounds])) {
		NSPoint startPoint = MGDeviceIntegralPoint(NSMakePoint(x,NSMinY([self bounds])));
		NSPoint endPoint = MGDeviceIntegralPoint(NSMakePoint(x, NSMaxY([self bounds])));
		[path moveToPoint:startPoint];
		[path lineToPoint:endPoint];
		x += theSize.width;
	}
	while (y <= NSMaxY([self bounds])) {
		NSPoint startPoint = MGDeviceIntegralPoint(NSMakePoint(NSMinX([self bounds]), y));
		NSPoint endPoint = MGDeviceIntegralPoint(NSMakePoint(NSMaxX([self bounds]), y));
		[path moveToPoint:startPoint];
		[path lineToPoint:endPoint];
		
		y += theSize.height;
	}
	
	// if we set this width small enough, the line will be single pixel width on every zoom size...
	[path setLineWidth:1.0/zoomLevel];
	
	[NSGraphicsContext saveGraphicsState];
	NSRect overlayRect = NSInsetRect([self bounds], MGTableMargin/zoomLevel, MGTableMargin/zoomLevel);
	NSBezierPath *overlay = [NSBezierPath bezierPathWithRoundedRect:overlayRect xRadius:0.5 * MGTableMargin/zoomLevel yRadius:0.5 * MGTableMargin/zoomLevel];

	[overlay addClip];
	
	[[NSColor colorWithCalibratedWhite:0.13 alpha:1.0] set];
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	[path stroke];
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
	
	[NSGraphicsContext restoreGraphicsState];

}

- (LightTableImageView *)imageViewWithLightTableImage:(LightTableImage *)ltImage
{
	LightTableImageView *newView = [[[LightTableImageView alloc] initWithFrame:NSMakeRect(0,0,10, 10)] autorelease];

	// set the model reference.
	newView.lightTableImage = ltImage;
	
	[newView initSubviews];
	
	newView.toolView.showsScalebarIfAvailable = ((LightTableView *)[self superview]).showsScalebarIfAvailable;
	
	// make the necessary bindings.
	NSDictionary *bindingOptions = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSContinuouslyUpdatesValueBindingOption, nil];
	
	[newView bind:@"frame" toObject:ltImage withKeyPath:@"frame" options:bindingOptions];
	[ltImage bind:@"frame" toObject:newView withKeyPath:@"frame" options:bindingOptions];
	
	[newView bind:@"image" toObject:ltImage withKeyPath:@"image.filteredImageRepWithScaleBar" options:nil];
	
	return newView;
}

- (void)addImageView:(LightTableImageView *)theView
{
	[imageViews addObject:theView];
	[(LightTableView *)[self superview] addAlignLayoutElementForView:theView];
	[self addSubview:theView];
}

- (void)removeImageView:(LightTableImageView *)theView
{
	[imageViews removeObject:theView];
	
	[theView unbind:@"frame"];
	[theView.lightTableImage unbind:@"frame"];
	[theView unbind:@"image"];
	
	[(LightTableView *)[self superview] removeAlignLayoutElementForView:theView];
	[theView removeFromSuperview];
	
}

- (BOOL)drawsGrid
{
	return drawsGrid;
}
	
- (void)setDrawsGrid:(BOOL)flag
{
	drawsGrid = flag;
	[[self animator] setGridOpacity:(flag ? 1.0 : 0.0)];
	[[self animator] setGradientOpacity:(flag ? 0.0 : 1.0)];
}

- (void)setGridOpacity:(CGFloat)opacity
{
	gridOpacity = opacity;
	[self setNeedsDisplay:YES];
}

- (void)setGradientOpacity:(CGFloat)opacity
{
	gradientOpacity = opacity;
	[self setNeedsDisplay:YES];
}

- (void)setZoomLevel:(CGFloat)level
{
	zoomLevel = level;
	[self setNeedsDisplay:YES];	
}



#pragma mark Mouse events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse events
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)enterPrintRectangleMode
{
	printRectangleMode = YES;
	((LightTableView *)[self superview]).printingRectMode = YES;
}

- (void)leavePrintRectangleMode
{
	printRectangleMode = NO;
}

- (BOOL)subview:(LightTableImageView *)theView shouldBeSelectedWithEvent:(NSEvent *)theEvent
{
	// delegate method invoked by the subview. THe subview will select itself if it returns YES.
	// this gives the superview (self) the possibility to make global content view changes.
	
	// deselect the others, if no command or shift key used...
	if (!([theEvent modifierFlags] & NSCommandKeyMask)) {
		
		NSMutableArray *viewsToDeselect = [[imageViews mutableCopy] autorelease];
		NSArray *viewsToKeepSelected = [NSArray arrayWithObject:theView];
		[viewsToDeselect removeObjectsInArray:viewsToKeepSelected];
		
		for (LightTableImageView *view in viewsToDeselect) {
			view.selected = NO;
		}
	}
	
	// update the controller
	NSMutableArray *newSelectedObjects = [NSMutableArray array];
	[newSelectedObjects addObject:theView.lightTableImage];
	
	if ([theEvent modifierFlags] & NSCommandKeyMask) {
		[newSelectedObjects addObjectsFromArray:[lightTableImageArrayController selectedObjects]];
	}
	
	[lightTableImageArrayController setSelectedObjects:newSelectedObjects];
	
	return YES;
}




- (NSArray *)imagesInRectOfContentView:(NSRect)rect
{
	// returns an array of all light table images whose image view frame intersects the given rect. (used for the selection rectangle)
	NSMutableArray *intersectingImages = [NSMutableArray array];
	for (LightTableImage *img in [lightTableImageArrayController arrangedObjects]) {
		if (NSIntersectsRect([[self imageViewForImage:img] frame], rect)) {
			[intersectingImages addObject:img];
		}
	}
	return intersectingImages;
}


- (void)reassignControlPointsRectIfNeededWithEvent:(NSEvent *)theEvent
{
	// this method is invoked by a subview whenever it receives a -mouseEntered or -mouseExited event.
	// it will assign the "permission" to display the control points to another view, or none if no views apply for this permission.
	LightTableImageView *newControlPointsView = [self controlPointsViewForLocation:[self convertPoint:[theEvent locationInWindow] fromView:nil]];
	for (LightTableImageView *view in [self subviews]) {
		view.displaysControlPoints = (view == newControlPointsView);
	}
}

- (void)offsetImageOriginsWithPoint:(NSPoint)theOffset eventView:(LightTableImageView *)sender
{
	// invoked by a imageview that receives an mouse dragged event not on any of its control points.
	// this method should check if other views are selected and if so, move them too.
	for (LightTableImage *image in [lightTableImageArrayController selectedObjects]) {
		if (sender.lightTableImage != image) {
			NSRect originalFrame = [[image valueForKey:@"frame"] rectValue];
			originalFrame.origin.x += theOffset.x;
			originalFrame.origin.y -= theOffset.y;
			[image setValue:[NSValue valueWithRect:originalFrame] forKey:@"frame"];
		}
	}
}

- (LightTableImageView *)controlPointsViewForLocation:(NSPoint)theLocation
{
	// this method will determine which view gets to display the control bounds (as there can only be one), for a specific pointer location in the coord system of this view.
	// this view is the topmost view of which the frame rect encircles the given location.
	NSEnumerator *viewsEnum = [[self subviews] reverseObjectEnumerator];
	// ordered from back to front, so reverse enum -> first in enum is frontmost view
	for (LightTableImageView *view in viewsEnum) {
		if (NSPointInRect(theLocation, [view frame])) return view;
	}
	return nil;
}

- (void)mouseDown:(NSEvent *)theEvent
{
	[lightTableImageArrayController setSelectedObjects:[NSArray array]];

	// close the comments view if opened.
	[commentView removeFromSuperview];
	[[self window] makeFirstResponder:self];
	
	dragStartPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
}

- (void)mouseUp:(NSEvent *)theEvent
{
	// remove selection rect
	[(LightTableView *)[self superview] updateSelectionRectangle:NSZeroRect];
	
	isDragging = NO;
	
	if (printRectangleMode) {
		
		printRectangleMode = NO;
		((LightTableView *)[self superview]).printingRectMode = NO;
		
		// compute the new (print) selection rectangle.
		NSPoint firstPoint = dragStartPoint;
		NSPoint secondPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		NSPoint origin = NSMakePoint(fmin(firstPoint.x, secondPoint.x), fmin(firstPoint.y, secondPoint.y));
		NSSize frameSize = NSMakeSize(fabs(firstPoint.x - secondPoint.x), fabs(firstPoint.y - secondPoint.y));
		NSRect selectionRectFrame;
		selectionRectFrame.origin = origin;
		selectionRectFrame.size = frameSize;
		
		if (selectionRectFrame.size.width == 0 || selectionRectFrame.size.height == 0) {
			NSBeep();
			return;
		}
		
		NSSavePanel *savePanel = [NSSavePanel savePanel];
		[savePanel setExtensionHidden:YES];
		[savePanel setRequiredFileType:@"tiff"];
		[savePanel beginSheetForDirectory:nil 
									 file:[NSString stringWithFormat:@"Collage of %@.tiff", [[[lightTableArrayController selectedObjects] objectAtIndex:0] valueForKey:@"name"]]
						   modalForWindow:[NSApp mainWindow] 
							modalDelegate:[self superview] 
						   didEndSelector:@selector(saveCollagePanelDidEnd:returnCode:contextInfo:) 
							  contextInfo:[[NSValue valueWithRect:selectionRectFrame] retain]];
		
		
		/*NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Create an image of the selected area?", nil) 
										 defaultButton:NSLocalizedString(@"Create",nil)
									   alternateButton:NSLocalizedString(@"Don't create",nil) 
										   otherButton:nil 
							 informativeTextWithFormat:NSLocalizedString(@"Macnification will create an image of the selected area of the light table, and save it in your library.", nil)];
		
		int returnCode = [alert runModal];
		*/

		
		
	}		
	
}

- (void)rightMouseDown:(NSEvent *)theEvent
{
	[lightTableImageArrayController setSelectedObjects:[NSArray array]];

	[super rightMouseDown:theEvent];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	isDragging = YES;
	
	// compute the new selection rectangle.
	NSPoint firstPoint = dragStartPoint;
	NSPoint secondPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	NSPoint origin = NSMakePoint(fmin(firstPoint.x, secondPoint.x), fmin(firstPoint.y, secondPoint.y));
	NSSize frameSize = NSMakeSize(fabs(firstPoint.x - secondPoint.x), fabs(firstPoint.y - secondPoint.y));
	NSRect selectionRectFrame;
	selectionRectFrame.origin = origin;
	selectionRectFrame.size = frameSize;
	
	// update the graphical rep of the selection rectangle
	[(LightTableView *)[self superview] updateSelectionRectangle:selectionRectFrame];
	
	
	if (printRectangleMode) {
		// do nothing else (wait on a mouseUp)
	} else {
		// select the intersecting images
		[lightTableImageArrayController setSelectedObjects:[self imagesInRectOfContentView:selectionRectFrame]];
	}
}


- (void)keyDown:(NSEvent *)theEvent
{
	unichar key = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
	
	// if the event regards image views (moving, etc...) redirect it to them.  Use a special method that doesn't call super again.
	if (key == NSUpArrowFunctionKey || key == NSDownArrowFunctionKey || key == NSLeftArrowFunctionKey || key == NSRightArrowFunctionKey) {
		for (LightTableImage *image in [lightTableImageArrayController selectedObjects]) {
			[[self imageViewForImage:image] keyDownOnSuperview:theEvent];
		}
	}
	
	// events that we can better handle in self:
	else if (key == NSDeleteCharacter) {
		[self shouldRemoveSelectedObjects];
	}
	
	else if (key == NSTabCharacter) {
		if ([[lightTableImageArrayController selectedObjects] count] == 1) {
			LightTableImageView *selectedView = [self imageViewForImage:[[lightTableImageArrayController selectedObjects] objectAtIndex:0]];
			([theEvent modifierFlags] & NSShiftKeyMask) ? [self selectPreviousImageView:selectedView] : [self selectNextImageView:selectedView];
		}
	}
	
	else if (key == 'a' && ([theEvent modifierFlags] & NSCommandKeyMask)) {
		[self selectAll];
	}
	
	// escape char
	else if ([theEvent keyCode] == 53) {
		NSLog(@"LT escape");
		[self leavePrintRectangleMode];
		((LightTableView *)[self superview]).printingRectMode = NO;
		
	}
	
	else [super keyDown:theEvent];
	
}


- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	if ([[lightTableImageArrayController selectedObjects] count] > 0)
		return imageViewContextualMenu;
	else
		return tableContextualMenu;
	
}

- (NSView *)hitTest:(NSPoint)thePoint
{
	if (printRectangleMode) {
		// do not let subviews receive this event, handle it ourselves
		return self;
	} else {
		return [super hitTest:thePoint];
	}
}


- (void)magnifyWithEvent:(NSEvent *)theEvent;
{
	CGFloat magnification = (float)[(id)theEvent magnification];
	
	// hack: because the LT keeps a different zoom range, we need to map it back to (0, 1).
	CGFloat oldZoomLevel = ([(LightTableView *)[self superview] zoomLevel] - 1.)*0.2;
	CGFloat newVal = oldZoomLevel + magnification*1.;
	
	if (newVal > 0.99 || newVal < 0.01) return;
	[(LightTableView *)[self superview] setZoomLevel:newVal];
	
	[[MGLibraryControllerInstance sourcelistTreeController] setValue:[NSNumber numberWithFloat:newVal] forKeyPath:@"selection.browserZoom"];

}




#pragma mark Drag and drop
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Drag and drop
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSDragOperation)draggingEntered:(id < NSDraggingInfo >)sender
{
	if ([[[sender draggingPasteboard] types] containsObject:@"keywordsPboardType"]) {
		
		return NSDragOperationCopy;
		
	} else {
		return NSDragOperationNone; 
		
	}
	
}


- (NSDragOperation)draggingUpdated:(id < NSDraggingInfo >)sender
{
	if ([[[sender draggingPasteboard] types] containsObject:@"keywordsPboardType"]) {
		
		
		// Get the image we are dragging on
		NSRect dragLocationRect;
		dragLocationRect.origin = [self convertPoint:[sender draggingLocation] fromView:nil];
		dragLocationRect.size = NSMakeSize(.1,.1);
		
		NSArray *possibleImageViews = [self imagesInRectOfContentView:dragLocationRect];
		LightTableImageView *topmostImageView = nil;
		
		// search for the topmost image
		for (LightTableImageView *subview in [self subviews]) {
			
			if ([possibleImageViews containsObject:subview.lightTableImage]) {
				
				topmostImageView = subview;
				break;
				
			}
			
		}
		
		if (!topmostImageView) {
			
			// restore all fades
			for (LightTableImageView *view in [self subviews]) {
				
				if (!view.selected) [[view.toolView animator] setSelectionRectangleOpacity:0.0];
				
			}
			
			return NSDragOperationNone;
			
		}
		
		
		// make all other images fade
		for (LightTableImageView *view in [self subviews]) {
			
			if (!view.selected && view == topmostImageView) [[view.toolView animator] setSelectionRectangleOpacity:0.5];
			
		}
			
		return NSDragOperationCopy;
		
	}
	
	return NSDragOperationNone; 
	
	
}

- (void)draggingEnded:(id < NSDraggingInfo >)sender
{
	
	// restore all fades
	for (LightTableImageView *view in [self subviews]) {
		
		if (!view.selected) [[view.toolView animator] setSelectionRectangleOpacity:0.0];
		
	}
	
	
}

- (void)draggingExited:(id < NSDraggingInfo >)sender
{
	
	// restore all fades
	for (LightTableImageView *view in [self subviews]) {
		
		if (!view.selected) [[view.toolView animator] setSelectionRectangleOpacity:0.0];
		
	}
	
	
	
}


- (BOOL)prepareForDragOperation:(id < NSDraggingInfo >)sender
{
	return YES;
}


- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender
{
	if ([[[sender draggingPasteboard] types] containsObject:@"keywordsPboardType"]) {
				
		
		// Get the keywords that are dragged from the keywordTreeController
		NSMutableSet *draggedKeywords = [(KeywordTreeController *)[[sender draggingSource]delegate] draggedKeywords];
		NSArray *draggedKeywordsArray = [draggedKeywords allObjects];
		
		// Get the image we are dragging on
		NSRect dragLocationRect;
		dragLocationRect.origin = [self convertPoint:[sender draggingLocation] fromView:nil];
		dragLocationRect.size = NSMakeSize(.1,.1);
		
		NSArray *possibleImageViews = [self imagesInRectOfContentView:dragLocationRect];
		LightTableImageView *topmostImageView = nil;
		
		// search for the topmost image
		for (LightTableImageView *subview in [self subviews]) {
			
			if ([possibleImageViews containsObject:subview.lightTableImage]) {
				
				topmostImageView = subview;
				break;
				
			}
			
		}
		
		LightTableImage *ltImage = topmostImageView.lightTableImage;
		Image *image = [ltImage valueForKey:@"image"];
		
		// Add them to the image
		[image addKeywords:draggedKeywordsArray];

		return YES;
		
		
	}
	return NO;	
}


#pragma mark Image scaling
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image scaling
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// TODO aspect ratio is not always retained after executing
- (void)scaleImageViews:(NSArray *)otherImages toSizeOfImageView:(LightTableImageView *)imageView withMagnification:(BOOL)includeMagnification
{
	// this method will scale "otherImages" to the same size as "image" when includeMagnification == NO.
	// when includeMagnification == YES, it will scale the images to the same relative size as the image (taking into account the magnifications)
	NSSize originalImageScreenSize = NSMakeSize([[imageView.lightTableImage valueForKey:@"width"] floatValue],[[imageView.lightTableImage valueForKey:@"height"] floatValue]);
	NSSize originalImageActualSize = [[imageView.lightTableImage valueForKeyPath:@"image.pixelSize"] sizeValue];
	CGFloat originalImagePPU = [[imageView.lightTableImage valueForKeyPath:@"image.calibration.horizontalPixelsForUnit"] floatValue];
	
	NSSize originalImageScreenPPU;
	if (originalImagePPU > 0)
		originalImageScreenPPU = NSMakeSize(originalImageScreenSize.width / originalImageActualSize.width * originalImagePPU, 
											originalImageScreenSize.height / originalImageActualSize.height * originalImagePPU);
	else
		originalImageScreenPPU = NSMakeSize(originalImageScreenSize.width / originalImageActualSize.width, 
											originalImageScreenSize.height / originalImageActualSize.height);
	
	
	BOOL didNotResizeAll = NO;
	
	
	for (LightTableImageView *view in otherImages) {
		
		if (includeMagnification) {

			NSSize currentImageScreenSize = NSMakeSize([[view.lightTableImage valueForKey:@"width"] floatValue],[[view.lightTableImage valueForKey:@"height"] floatValue]);
			NSSize currentImageActualSize = [[view.lightTableImage valueForKeyPath:@"image.pixelSize"] sizeValue];

			CGFloat currentImagePPU = [[view.lightTableImage valueForKeyPath:@"image.calibration.horizontalPixelsForUnit"] floatValue];
			
			if (currentImagePPU <= 0.0)
				currentImagePPU = 1.0;

			// make sure we retain the aspect ratio of this image!
			NSSize newImageScreenSize = NSMakeSize(originalImageScreenPPU.width * currentImageActualSize.width / currentImagePPU,
												   originalImageScreenPPU.width * currentImageActualSize.width / currentImagePPU * currentImageScreenSize.height / currentImageScreenSize.width);
			
			NSPoint oldImageOrigin = NSMakePoint([[view.lightTableImage valueForKey:@"x"] floatValue],[[view.lightTableImage valueForKey:@"y"] floatValue]);
			NSRect newImageFrame = NSMakeRect(oldImageOrigin.x + (currentImageScreenSize.width - newImageScreenSize.width)/2.0,
											  oldImageOrigin.y + (currentImageScreenSize.height - newImageScreenSize.height)/2.0, newImageScreenSize.width, newImageScreenSize.height);
			
			if (currentImagePPU > 0) {
				if (newImageFrame.size.width > LightTableMinimumDimension && newImageFrame.size.height > LightTableMinimumDimension) {
					
					[self animateToNewFrame:newImageFrame ofView:view withDuration:0.2 updateFrame:YES];
					
				} else {
					didNotResizeAll = YES;
					
				}
				
			} 
			
		} else {
			
			NSSize oldImageSize = NSMakeSize([[view.lightTableImage valueForKey:@"width"] floatValue],[[view.lightTableImage valueForKey:@"height"] floatValue]);
			NSSize newImageSize = NSMakeSize(originalImageScreenSize.width, originalImageScreenSize.width * oldImageSize.height / oldImageSize.width);
			NSPoint oldImageOrigin = NSMakePoint([[view.lightTableImage valueForKey:@"x"] floatValue],[[view.lightTableImage valueForKey:@"y"] floatValue]);
			NSRect newImageFrame = NSMakeRect(oldImageOrigin.x + (oldImageSize.width - newImageSize.width)/2.0,
											  oldImageOrigin.y + (oldImageSize.height - newImageSize.height)/2.0,  newImageSize.width, newImageSize.height);
			
			[self animateToNewFrame:newImageFrame ofView:view withDuration:0.2 updateFrame:YES];
			
		}
	}
	
	
	if (didNotResizeAll) {
		
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"OK"];
		[alert setMessageText:@"Not all of the images could be correctly resized."];
		[alert setInformativeText:@"Some images would have too small dimensions if correctly resized.  Please adjust the size of the selected image before resizing all others."];
		
		[alert setAlertStyle:NSWarningAlertStyle];
		[alert runModal];	
		
	}
}

- (IBAction)resizeToRealSize:(id)sender
{
	for (LightTableImage *image in [lightTableImageArrayController selectedObjects]) {
		LightTableImageView *view = [self imageViewForImage:image];
		
		// constrain to the viewable size.
		NSRect currentFrame = [view frame];
		NSSize realSize = [[view image] bestRepresentationSize];
		NSSize viewVisibleSize = MGScaleSize([[self enclosingScrollView] documentVisibleRect].size, 0.85);
		NSSize newSize;
		
		// the lowest of these factors is determining for which dimension is the restricting one.
		CGFloat widthFactor = viewVisibleSize.width / realSize.width;
		CGFloat heightFactor = viewVisibleSize.height / realSize.height;
		
		
		if (widthFactor < heightFactor) {
			newSize.width = fmin(viewVisibleSize.width, realSize.width);
			newSize.height = newSize.width * realSize.height / realSize.width;
		} else {
			newSize.height = fmin(viewVisibleSize.height, realSize.height);
			newSize.width = newSize.height * realSize.width / realSize.height;
		}
		
		NSRect newFrame = NSMakeRect(currentFrame.origin.x + (currentFrame.size.width - newSize.width) / 2, currentFrame.origin.y + (currentFrame.size.height - newSize.height) / 2, newSize.width, newSize.height);
		
		[self animateToNewFrame:newFrame ofView:view withDuration:0.2 updateFrame:YES];
		if ([[lightTableImageArrayController selectedObjects] count] == 1) {
			[[self enclosingScrollView] scrollRectToVisible:newFrame];
		}
	}
}

- (IBAction)resizeToPreviousSize:(id)sender
{
	for (LightTableImage *image in [lightTableImageArrayController selectedObjects]) {
		LightTableImageView *view = [self imageViewForImage:image];
		
		NSRect currentFrame = [view frame];
		NSSize previousSize = view.previousSize;
		
		NSRect newFrame = NSMakeRect(currentFrame.origin.x + (currentFrame.size.width - previousSize.width) / 2,
									 currentFrame.origin.y + (currentFrame.size.height - previousSize.height) / 2,
									 previousSize.width,
									 previousSize.height);
		
		[self animateToNewFrame:newFrame ofView:view withDuration:0.2 updateFrame:YES];
	}
}


- (IBAction)resizeToMatchSelected:(id)sender
{
	// need a selection
	if ([[lightTableImageArrayController selectedObjects] count] == 0) return;
	
	// get an array of every imageview not belonging to the image selection, then resize.
	NSMutableArray *others = [[[lightTableImageArrayController arrangedObjects] mutableCopy] autorelease];
	[others removeObject:[[lightTableImageArrayController selectedObjects] objectAtIndex:0]];
	NSMutableArray *otherViews = [[imageViews mutableCopy] autorelease];
	LightTableImageView *resizeView = nil;
	
	for (LightTableImageView *view in otherViews) {
		if ([[lightTableImageArrayController selectedObjects] objectAtIndex:0] == view.lightTableImage) {
			resizeView = view;
		}
	}
	
	[others removeObject:resizeView];
	[self scaleImageViews:otherViews toSizeOfImageView:resizeView withMagnification:NO];
}

- (IBAction)resizeToMatchSelectedUsingMagnification:(id)sender
{
	// get an array of every imageview not belonging to the image selection, then resize.
	NSMutableArray *others = [[[lightTableImageArrayController arrangedObjects] mutableCopy] autorelease];
	[others removeObject:[[lightTableImageArrayController selectedObjects] objectAtIndex:0]];
	NSMutableArray *otherViews = [[imageViews mutableCopy] autorelease];
	LightTableImageView *resizeView = nil;
	
	for (LightTableImageView *view in otherViews) {
		if ([[lightTableImageArrayController selectedObjects] objectAtIndex:0] == view.lightTableImage) {
			resizeView = view;
		}
	}
	
	[others removeObject:resizeView];
	[self scaleImageViews:otherViews toSizeOfImageView:resizeView withMagnification:YES];
}



#pragma mark Image Layout (grid)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image layout (grid)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)arrangeImageViewsInGrid:(NSArray *)views animated:(BOOL)animated
{
	// redistribute the given images in a grid with a set margin and retaining their dimensions.
	NSSize interImageMargin = NSMakeSize(6.0, 6.0);
	
	// get the max size of the images
	NSSize maxSize = NSMakeSize(1,1);
	for (LightTableImageView *imageView in views) {
		maxSize.width = fmax(maxSize.width, [imageView frame].size.width);
		maxSize.height = fmax(maxSize.height, [imageView frame].size.height);
	}
	
	// this max size is the size of a unit rectangle regardless of the size of the imageview contained in it.
	// compute how many we can stack vertically including a margin, in order to fill the screen (or less of course).
	int numberOfRows = fmin(sqrt([views count]),floorf(([self bounds].size.height) / (interImageMargin.height + maxSize.height)));
	// make sure we have at least one row!
	numberOfRows = fmax(numberOfRows, 1);
	int numberOfColumns = ceilf(((CGFloat)[views count])/numberOfRows);
	
	// determine the origin position of the grid.
	CGFloat gridWidth = numberOfColumns * maxSize.width + (numberOfColumns - 1) * interImageMargin.width;
	CGFloat gridHeight = numberOfRows * maxSize.height + (numberOfRows - 1) * interImageMargin.height;
	
	// Bugfix 17/02 : changed the bounds definition hre: should center in visibleRect.
	NSRect bounds = [self bounds];
		
	NSPoint gridOrigin = NSMakePoint(bounds.origin.x + (bounds.size.width - gridWidth)/2, bounds.origin.y + (bounds.size.height - gridHeight)/2);
	
	// compute and set the new imageview frame origins.
	int i = 0;
	while (i != [views count]) {
		int currentRow = i % numberOfRows;
		int currentColumn = (i - currentRow) / numberOfRows;
		
		// compute the origin of the unit rectangle containing this imgview
		CGFloat x = gridOrigin.x + currentColumn * maxSize.width + (currentColumn - 1) * interImageMargin.width;
		CGFloat y = gridOrigin.y + currentRow * maxSize.height + (currentRow - 1) * interImageMargin.height;
		
		// compute the origin of the imageview in this unit rectangle
		x += (maxSize.width - [(NSView *)[views objectAtIndex:i] frame].size.width) / 2;
		y += (maxSize.height - [(NSView *)[views objectAtIndex:i] frame].size.height) / 2;
		
		// set the frame
		NSRect newFrame = NSMakeRect(x, y, [(NSView *)[views objectAtIndex:i] frame].size.width, [(NSView *)[views objectAtIndex:i] frame].size.height);
		
		if (animated)
			[self animateToNewFrame:newFrame ofView:[views objectAtIndex:i] withDuration:0.2  updateFrame:YES];
		else {
			LightTableImageView *view = [views objectAtIndex:i];
			[view setFrame:newFrame];
			[view.lightTableImage setValue:[NSValue valueWithRect:newFrame] forKey:@"frame"];
		}

		i++;
	}
}

- (void)setOriginPositionsForImages:(NSArray *)lightTableImages gridCenterPoint:(NSPoint)location
{
	// invoked by the light table when inserting an array of objects dragged in.
	
	// first, we set the frame of an image to be centered on the drop point. 
	for (LightTableImage *image in lightTableImages) {
		CGFloat width = defaultImageWidth;
		CGFloat height = defaultImageHeight;
		[image setValue:[NSNumber numberWithFloat:location.x - width/2] forKey:@"x"];
		[image setValue:[NSNumber numberWithFloat:location.y - height/2] forKey:@"y"];
	}
	
	// then, we wait a set time until all imageviews have been inserted (by bindings update), then animate the imageviews to their positions.
	[self performSelector:@selector(setFinalOriginPositionsForImages:) withObject:lightTableImages afterDelay:0.25];

}

- (void)setFinalOriginPositionsForImages:(NSArray *)lightTableImages
{
	// return a point grid and animate the image positions to it.
	
	// first, get the original drop point.  we can derive this from the (DEFAULT) size and origin of one of the images.
	NSSize imgSize = NSMakeSize(defaultImageWidth,defaultImageHeight);
	NSPoint imgPosition = NSMakePoint([[[lightTableImages objectAtIndex:0] valueForKey:@"x"] floatValue],[[[lightTableImages objectAtIndex:0] valueForKey:@"y"] floatValue]);

	NSPoint originalDropLocation = NSMakePoint(imgPosition.x + imgSize.width/2, imgPosition.y + imgSize.height/2);
	
	// the margin between 2 images hor/vert
	NSSize margin = NSMakeSize(10,10);
	
	// try to get approx. as many rows as columns.
	// compute the grid layout (number of rows, columns, number of items on top row)
	int numberOfElements = [lightTableImages count];
	int numberOfColumns = ceilf(sqrtf(numberOfElements));
	int numberOfRows = floorf(numberOfElements/numberOfColumns);

	// compute grid size to deterimne grid origin
	CGFloat gridHeight = numberOfRows * imgSize.height + (numberOfRows - 1) * margin.height;
	CGFloat gridWidth = numberOfColumns * imgSize.width + (numberOfColumns - 1) * margin.width;
	NSPoint gridOrigin = NSMakePoint(originalDropLocation.x - gridWidth/2, originalDropLocation.y - gridHeight/2);

	// apply the grid positions
	int i = 0;
	while (i < numberOfElements) {
		int currentColumn = i % numberOfColumns;
		int currentRow = (i - currentColumn) / numberOfColumns;
		
		CGFloat newX = gridOrigin.x + currentColumn * imgSize.width + (currentColumn - 1) * margin.width;
		CGFloat newY = gridOrigin.y + currentRow * imgSize.height + (currentRow - 1) * margin.height;
		
		// now we assume that the imageview belonging to the image has been created as subview of self.
		// the functionality of this method breaks if it does not.
		
		LightTableImageView *imageView = [self imageViewForImage:[lightTableImages objectAtIndex:i]];
		NSSize imageViewSize = [imageView frame].size;
		[self animateToNewFrame:NSMakeRect(newX, newY, imageViewSize.width, imageViewSize.height) ofView:imageView withDuration:0.2 updateFrame:YES];
		
		i++;
	}
	
	
}

#pragma mark Image Layout (bounding box)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image layout (bounding box)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSRect)boundingBoxForImageViews:(NSArray *)theViews
{
	// returns the bounding box that encloses all of the given views.
	if([theViews count] == 0) return NSMakeRect(0,0,30,30);
	NSRect compositeRect = [(LightTableImageView *)[theViews objectAtIndex:0] frame];
	for (LightTableImageView *view in theViews) {
		compositeRect = NSUnionRect(compositeRect, [view frame]);
	}
	return compositeRect;
}

- (NSRect)imageViewsBoundingBox
{
	// returns the bounding box that encloses all of this content views subviews.
	NSMutableArray *views = [NSMutableArray array];
	for (LightTableImage *image in [lightTableImageArrayController arrangedObjects]) {
		id view;
		if ((view = [self imageViewForImage:image]))
			[views addObject:view];
	}
	
	return [self boundingBoxForImageViews:views];
}

- (NSRect)selectedImageViewsBoundingBox
{
	// returns the bounding box that encloses all of this content views subviews.
	NSMutableArray *views = [NSMutableArray array];
	for (LightTableImage *image in [lightTableImageArrayController selectedObjects]) {
		id view;
		if ((view = [self imageViewForImage:image]))
			[views addObject:view];
	}
	
	return [self boundingBoxForImageViews:views];
}

- (BOOL)viewOverlapsWithOtherImageView:(LightTableImageView *)view
{
	for (LightTableImage *image in [lightTableImageArrayController arrangedObjects]) {
		
		LightTableImageView *otherView = [self imageViewForImage:image];

		if (NSIntersectsRect([view frame], [otherView frame])) {
			
			if (view != otherView)
				return YES;
			
		} 
		
	}
	
	return NO;
	
}

- (NSArray *)imageViews;
{
    NSMutableArray *views = [NSMutableArray array];
    
    for (LightTableImage *image in [lightTableImageArrayController arrangedObjects])
		[views addObject:[self imageViewForImage:image]];
    
    return views;
}

#pragma mark Image Layout (distribute)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image layout (distribute)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)distributeSelectedImageViewsinDirection:(Direction)direction
{
	// distribute the selected views in the given direction, keeping the given view static (i.e. distribute to this view)
	// "Distribute" == take coords of two outer frames in the given direction, and put the centers of the others at equal distances between those two extremes.
	
	// sort the selected objects based on the relevant frame property.
	SEL sortSel = (direction == VerticalDirection) ? @selector(compareFrameCenterY:) : @selector(compareFrameCenterX:);
	
	NSMutableArray *views = [NSMutableArray array];
	for (LightTableImage *img in [lightTableImageArrayController selectedObjects]) {
		[views addObject:[self imageViewForImage:img]];
	}
	
	NSArray *sortedViews = [views sortedArrayUsingSelector:sortSel];
	
	CGFloat minCoord = (direction == VerticalDirection) ? MGRectCenter([(NSView *)[sortedViews objectAtIndex:0] frame]).y : MGRectCenter([(NSView *)[sortedViews objectAtIndex:0] frame]).x;
	CGFloat maxCoord = (direction == VerticalDirection) ? MGRectCenter([(NSView *)[sortedViews lastObject] frame]).y : MGRectCenter([(NSView *)[sortedViews lastObject] frame]).x;
	CGFloat individualDistance = (maxCoord - minCoord) / ([views count] - 1);
	
	int i = 1;
	while (i < [sortedViews count] - 1) {
		LightTableImageView *view = [sortedViews objectAtIndex:i];
		
		NSRect newFrame = [view frame];
		if (direction == VerticalDirection) {
			newFrame.origin.y = minCoord + i * individualDistance - newFrame.size.height/2;
		} else {
			newFrame.origin.x = minCoord + i * individualDistance - newFrame.size.width/2;
		}
		
		[self animateToNewFrame:newFrame ofView:view withDuration:0.2 updateFrame:YES];
		
		i++;
	}
		
}


- (IBAction)distributeHorizontally:(id)sender
{
	[self distributeSelectedImageViewsinDirection:HorizontalDirection];
}

- (IBAction)distributeVertically:(id)sender
{
	[self distributeSelectedImageViewsinDirection:VerticalDirection];
}


#pragma mark Image Layout (align)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image layout (align)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)alignSelectedImageViewsWithSide:(LayoutSide)side toView:(LightTableImageView *)view
{	
	LightTableImageView *selectedView;
	for (LightTableImage *img in [lightTableImageArrayController selectedObjects]) {
		if ((selectedView = [self imageViewForImage:img]) != view) {
			
			NSRect frame = [selectedView frame];
			
			switch (side) {
					
				case LayoutMinX:
					frame.origin.x = [view frame].origin.x;
					break;
				case LayoutMinY:
					frame.origin.y = [view frame].origin.y;
					break;
				case LayoutMaxX:
					frame.origin.x = [view frame].origin.x - [view frame].size.width + [selectedView frame].size.width;
					break;
				case LayoutMaxY:
					frame.origin.y = [view frame].origin.y + [view frame].size.height - [selectedView frame].size.height;
					break;
					
			}
			
			[self animateToNewFrame:frame ofView:selectedView withDuration:0.2 updateFrame:YES];
			
		}
	}
}
	


- (IBAction)alignTopHorizontally:(id)sender
{
	if (self.lastClickedView)
		[self alignSelectedImageViewsWithSide:LayoutMaxX toView:self.lastClickedView];
}

- (IBAction)alignTopVertically:(id)sender
{
	if (self.lastClickedView)
		[self alignSelectedImageViewsWithSide:LayoutMaxY toView:self.lastClickedView];
}

- (IBAction)alignBottomHorizontally:(id)sender
{
	if (self.lastClickedView)
		[self alignSelectedImageViewsWithSide:LayoutMinX toView:self.lastClickedView];
}

- (IBAction)alignBottomVertically:(id)sender
{
	if (self.lastClickedView)
		[self alignSelectedImageViewsWithSide:LayoutMinY toView:self.lastClickedView];
}	



#pragma mark Image Layout Animation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Image Layout (animation)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)animateToNewFrame:(NSRect)newFrame ofView:(LightTableImageView *)view withDuration:(NSTimeInterval)duration updateFrame:(BOOL)updateFrame
{
	// this method will bypass the limitation of updating both the model and view animated.
	// doing this in a normal way is not possible:
	// - calling animator to change will not trigger a bindings update to the model.
	// - calling the model to change makes animation impossible because the view animator is not messaged.
	// - calling both at the same time prevents animation fully or partly.
	// The solution: call the animator to animate to the new value, then call the model to update when the animation should have ended.
	// May be obsolete in future seeds (needed as of 9A499) if animator triggers bindings (as can be expected in a correctly working framework)
	
	[(NSView *)[view animator] setFrame:newFrame];
		
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithRect:newFrame], @"frame", view.lightTableImage, @"image", [NSNumber numberWithBool:updateFrame], @"updateFrame", nil];
	[self performSelector:@selector(updateLightTableImage:) withObject:userInfo afterDelay:duration + 0.2];
	
}

- (void)updateLightTableImage:(NSDictionary *)userInfo
{
	// model update to the object specified in the dictionary.
	LightTableImage *image = [userInfo valueForKey:@"image"];
	NSValue *frameValue = [userInfo valueForKey:@"frame"];
	
	[image setValue:frameValue forKey:@"frame"];
	
	// now is a good time to check if the frame size needs to be updated
	if ([[userInfo valueForKey:@"updateFrame"] boolValue]) [(LightTableView *)[self superview] updateContentViewBoundsForImages];
}


	

#pragma mark Other utilities
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Other utilities
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)shouldRemoveSelectedObjects
{
	// Invoked by e.g. a subview. Removes the image controller selection from the controller
	NSArray *objectsToRemove = [lightTableImageArrayController selectedObjects];
	LightTable *selectedLT = [[lightTableArrayController selectedObjects] objectAtIndex:0];
	NSMutableSet *images = [selectedLT mutableSetValueForKey:@"lightTableImages"];
	NSMutableSet *imagesFromSuperEntity = [selectedLT mutableSetValueForKey:@"images"];
	
	for (id ltImage in objectsToRemove) {
		[images removeObject:ltImage];
		id image = [ltImage valueForKey:@"image"];
		if (image) [imagesFromSuperEntity removeObject:image];

	}
	
}

- (int)numberOfSelectedViews
{
	return [[lightTableImageArrayController selectedObjects] count];
}

- (LightTableImageView *)imageViewForImage:(LightTableImage *)image
{
	for (LightTableImageView *view in imageViews) {
		if (view.lightTableImage == image) return view;
	}
	return nil;
}

- (void)selectNextImageView:(LightTableImageView *)original
{
	// selects the view coming after the selected view (in the arraycontroller), if only one view selected.  If none selected, pick -1 (then the next index is 0).
	if ([[lightTableImageArrayController selectedObjects] count] > 1) {
		NSBeep();
		return;
	}
	
	NSInteger previousIndex = [[lightTableImageArrayController selectedObjects] count] == 1 ? [[lightTableImageArrayController arrangedObjects] indexOfObject:original.lightTableImage] : -1;
	LightTableImage *nextObj = [[lightTableImageArrayController arrangedObjects] objectAtIndex:(previousIndex + 1) % [[lightTableImageArrayController arrangedObjects] count]];
	[lightTableImageArrayController setSelectedObjects:[NSArray arrayWithObject:nextObj]];
	
	// make sure this next obj can respond to a tab too
	[[self window] makeFirstResponder:[self imageViewForImage:nextObj]];
}

- (void)selectPreviousImageView:(LightTableImageView *)original
{
	// selects the view coming after the selected view (in the arraycontroller), if only one view selected.  If none selected, pick -1 (then the next index is 0).
	if ([[lightTableImageArrayController selectedObjects] count] > 1) {
		NSBeep();
		return;
	}
	
	NSInteger previousIndex = [[lightTableImageArrayController selectedObjects] count] == 1 ? [[lightTableImageArrayController arrangedObjects] indexOfObject:original.lightTableImage] : -1;
	LightTableImage *nextObj = [[lightTableImageArrayController arrangedObjects] objectAtIndex:(previousIndex - 1) % [[lightTableImageArrayController arrangedObjects] count]];
	[lightTableImageArrayController setSelectedObjects:[NSArray arrayWithObject:nextObj]];
	
	// make sure this next obj can respond to a tab too
	[[self window] makeFirstResponder:[self imageViewForImage:nextObj]];
}

- (void)selectAll
{
	[lightTableImageArrayController setSelectedObjects:[lightTableImageArrayController arrangedObjects]];
}



#pragma mark Comments
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Comments
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)showCommentViewForSelectedView:(id)sender
{
	if ([[lightTableImageArrayController selectedObjects] count] == 1) {
		[self showCommentViewForView:[self imageViewForImage:[[lightTableImageArrayController selectedObjects] objectAtIndex:0]]];
	}
}

- (void)initializeCommentViewItems
{
	NSShadow *shadow = [[[NSShadow alloc] init] autorelease];
	[shadow setShadowColor:[NSColor blackColor]];
	[shadow setShadowBlurRadius:10.0];
	[shadow setShadowOffset:NSMakeSize(0,0)];
	[commentView setShadow:shadow];
	// placeholder view that will become the content view of the window. It will contain one subview: the comment view.
	NSView *placeholderView = [[[NSView alloc] initWithFrame:NSMakeRect(0,0,200,200)] autorelease];
	[placeholderView addSubview:commentView];
	//[commentView setAutoresizingMask:(NSViewHeightSizable | NSViewWidthSizable)];

	
	commentWindow = [[NSWindow alloc] initWithContentRect:NSMakeRect(0,0,200,200) styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
	[commentWindow setLevel:NSModalPanelWindowLevel];
	[commentWindow setHasShadow:NO];
	[commentWindow setContentView:placeholderView];
	[commentWindow setOpaque:NO];
	[commentWindow setBackgroundColor:[NSColor colorWithCalibratedRed:0.0 green:0.0 blue:1.0 alpha:0.05]];
	
	[[commentWindow contentView] setWantsLayer:YES];

}

- (NSRect)commentWindowFrameForView:(LightTableImageView *)theView
{
	// return the frame (in base coords) that is appropriate for displaying comments for a given view.
	NSRect viewFrame = [theView frame];
	NSRect commentViewFrame = viewFrame;//NSInsetRect(viewFrame, 50, 50);
	return [self convertSubviewRectToScreen:commentViewFrame];
	
}

- (NSRect)convertSubviewRectToScreen:(NSRect)frame
{
	NSPoint frameOriginInThisView = frame.origin;
	NSPoint frameOriginInWindow = [self convertPointToBase:frameOriginInThisView];
	NSPoint frameOriginInScreen = [[self window] convertBaseToScreen:frameOriginInWindow];
	
	NSRect screenFrame;
	screenFrame.origin = frameOriginInScreen;
	screenFrame.size = frame.size;
	
	return screenFrame;
}


- (void)showCommentViewForView:(LightTableImageView *)theView
{
	if (!commentWindow) {
		[self initializeCommentViewItems];
	}
	[commentWindow orderOut:self];
	[commentWindow setFrame:[self commentWindowFrameForView:theView] display:YES];
	
	[commentView setFrame: NSMakeRect(0,0,30,60)];
	
	[NSAnimationContext beginGrouping];
	[[NSAnimationContext currentContext] setDuration:0.15];
	[(NSView *)[commentView animator] setFrame: NSMakeRect(20,20,[commentWindow frame].size.width - 40,[commentWindow frame].size.height - 40)];
	[NSAnimationContext endGrouping];
	[commentWindow makeKeyAndOrderFront:self];
	[commentWindow makeFirstResponder:commentView];
	[self layer].opacity = 0.5;
	[NSApp performSelector:@selector(runModalForWindow:) withObject:commentWindow afterDelay:0.2];

}

- (IBAction)cancelCommentView:(id)sender
{
	[NSApp stopModalWithCode:1];
	[commentWindow orderOut:self];
}

- (IBAction)commitCommentView:(id)sender
{
	[NSApp stopModalWithCode:0];
	[commentWindow orderOut:self];
}



#pragma mark View printing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// View printing
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// print view
//
// general strategy: 
//		- create new offscreen print view with special drawing configs
//		- copy all subviews to it
//		- ask the view to print itself to a pdf/nsimage.

- (NSImage *)compositeImageFromRect:(NSRect)rect constrainToImageBounds:(BOOL)constrain sideMargins:(NSSize)margins;
{
	// Creates a NSImage from the view including its subviews, constrained to the given rect.
	// Applies custom drawing (e.g. white background) optimized for print.
	// if rect == NSZeroRect, uses the bounding box of the image collection.
	
	// initing a printview will copy all the needed items & subviews.
	LightTablePrintContentView *printView = [[[LightTablePrintContentView alloc] initWithContentView:self] autorelease];
	
	return [printView compositeImageFromRect:rect constrainToImageBounds:constrain sideMargins:margins];
}





@end
