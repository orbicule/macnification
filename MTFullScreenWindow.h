/* MTFullScreenWindow */

#import <Cocoa/Cocoa.h>
#import "CGSPrivate.h"

@interface MTFullScreenWindow : NSWindow
{
	int transitionType;
}

- (id)initWithDelegate:(id)delegate;

- (void)enterFullScreen;
- (void)exitFullScreen;
- (void)setScreen:(NSScreen *)screen;


@end
