//
//  SourceListTreeController.m
//  Filament
//
//  Created by Peter Schols on 14/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SourceListTreeController.h"
#import "NSTreeController_DMExtensions.h"
#import "Album.h"
#import "SmartAlbum.h"
#import "LightTable.h"
#import "MGImageBatchImporter.h"
#import "StackController.h"
#import "LightTableController.h"
#import "KeywordTreeController.h"
#import "MGAppDelegate.h"
#import "RegistrationController.h"
#import "MetadataController.h"
#import "NSManagedObjectExtensions.h"
#import "StringExtensions.h"
#import "Stack.h"
#import "MGSourceOutlineCell.h"
#import "MGFilterAndSortMetadataController.h"
#import "Group.h"
#import "MGLibraryController.h"

@interface SourceListTreeController ()

- (Album *)albumForOutlineItem:(id)item;
- (id)selectedItem;
- (void)setup;


@end

@interface NSCoreUIImageRep : NSObject
{
	
}

- (id)initWithCoreUIDrawOptions:(id)options size:(NSSize)size;
- (void)setSuppressesCache:(BOOL)flag;

@end


@implementation SourceListTreeController


NSString *draggedImageType = @"DRAGGED_IMAGES_TYPE";
NSString *draggedCollectionType = @"DRAGGED_COLLECTIONS_TYPE";


# pragma mark -
# pragma mark Housekeeping

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if ((self = [super initWithCoder:aDecoder]))
		[self setup];

	return self;
}

- (id) init
{
	if ((self = [super init]))
		[self setup];
		
	return self;
}

- (void)setup
{
	// the main source list items
	allImagesGroupItem = [@"AllImagesGroupItem" retain];
	recentGroupItem = [@"RecentGroupItem" retain];
	devicesGroupItem = [@"DevicesGroupItem" retain];
	projectsGroupItem = [@"ProjectsGroupItem" retain];
	// the devices and project items will only be added when their data is available
	mainSourceListItems = [[NSMutableArray arrayWithObjects:allImagesGroupItem, recentGroupItem, nil] retain];
	
	
	NSSortDescriptor *sortRank = [[[NSSortDescriptor alloc] initWithKey:@"rank" ascending:YES] autorelease];
	NSSortDescriptor *sortName = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];
	sortDescriptors = [[NSArray arrayWithObjects:sortRank, sortName, nil] retain];
	
}


- (void)awakeFromNib;
{
	[super awakeFromNib];
	
	// Sort the source list
	[self setSortDescriptors:sortDescriptors];
	
	// Check the presence of fixed collections
	[[MGLibraryControllerInstance library] projectsCollection];
	[[MGLibraryControllerInstance library] libraryGroup];
	[[MGLibraryControllerInstance library] lastImportGroup];
	[[MGLibraryControllerInstance library] lastMonthGroup];
	[[[MGLibraryControllerInstance library] lastCaptureGroup] setValue:nil forKey:@"images"];
	
	[sourceOutlineView setDelegate:self];
	[sourceOutlineView setDataSource:self];
	
	// do an initial load of the items in the outline view, and expand the standard items
	[self expandStandardGroups];
	
	// retain the accessory view, otherwise it gets dealloced by the openpanel
	[importAccessoryView retain];
	
	// Register for drag and drop
    [sourceOutlineView registerForDraggedTypes: [NSArray arrayWithObjects: draggedCollectionType, draggedImageType, @"keywordsPboardType", @"StackImagesDragType", NSFilenamesPboardType, nil]];
	
	// Prepare the predicate editor
	[predicateEditor setRowTemplates:[[MGFilterAndSortMetadataController sharedController] predicateEditorRowTemplatesForFilterAttributes]];

	[[[sourceOutlineView enclosingScrollView] horizontalScroller] setControlTint:NSGraphiteControlTint];
	[[[sourceOutlineView enclosingScrollView] verticalScroller] setControlTint:NSGraphiteControlTint];
	
	sourceOutlineView.eventHandlingDelegate = self;
		
	// Register as an observer for our selected objects
	[self addObserver:self forKeyPath:@"arrangedObjects" options:(NSKeyValueObservingOptionNew) context:NULL];
	[self addObserver:self forKeyPath:@"selection" options:(NSKeyValueObservingOptionNew) context:NULL];
	
	[[MGCustomMetadataController sharedController] addObserver:self];
    
    if ([[MGLibraryControllerInstance library] libraryGroup])
        [self setSelectedObjects:[NSArray arrayWithObject:[[MGLibraryControllerInstance library] libraryGroup]]];
	
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
{	
	if ([keyPath isEqualTo:@"arrangedObjects"]) {
				
		// check the amount of children in the projects collection
		// if there are projects, make that group available
		[mainSourceListItems removeObject:projectsGroupItem];
		id projects = [[MGLibraryControllerInstance library] projectsCollection];
		if ([[projects valueForKey:@"children"] count] > 0)
			[mainSourceListItems addObject:projectsGroupItem];
		
		[self expandStandardGroups];
		
		// reload the source list
		[sourceOutlineView reloadData];
		
		if (!initialSourceListExpansionDone) {
			
			initialSourceListExpansionDone = YES;

			// select the library collection
			//[self selectItem:[[MGLibraryControllerInstance library] libraryGroup]];
            if ([[MGLibraryControllerInstance library] libraryGroup])
                [self setSelectedObjects:[NSArray arrayWithObject:[[MGLibraryControllerInstance library] libraryGroup]]];

			
			[sourceOutlineView setAutosaveName:@"sourceListItems"];
			[sourceOutlineView setAutosaveExpandedItems:YES];			
			
		}

	}
    
    else if ([keyPath isEqualTo:@"selection"]) {
        

    }
	
	else {
		
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
		
	}
}

- (void)customMetadataControllerDidChangeFields:(MGCustomMetadataController *)controller;
{
	[predicateEditor setRowTemplates:[[MGFilterAndSortMetadataController sharedController] predicateEditorRowTemplatesForFilterAttributes]];

	// if a smart album is selected, refresh its contents
	if ([self selectedCollectionIsSmartFolder])
		[[self selectedCollection] setValue:[[self selectedCollection] valueForKey:@"predicateData"] forKey:@"predicateData"];
	
}


- (BOOL)setSelectionIndexPaths:(NSArray *)indexPaths
{
	@try {
		BOOL ok = [super setSelectionIndexPaths:indexPaths];	
		//[self rearrangeObjects];
		return ok;
	} @catch (NSException *exc) {
		NSLog(@"%@ -- catching NSTreeController bug", [self description]);
		return [super setSelectionIndexPaths:indexPaths];
	}
	return YES;
}


- (void)dealloc;
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}


# pragma mark -
# pragma mark Stacks

- (void)showStack:(Stack *)stack;
{
	[mainTabView selectTabViewItemAtIndex:2];
	
	if ([[mainTabView window] isVisible]) {
		NSTabViewItem *tabItem = [mainTabView tabViewItemAtIndex:2];
		
		if (!stackController)
			stackController = [[StackController alloc] initWithSuperview:[tabItem view]];
		
		[self disconnectImageArrayController];

		[stackController bindToStack:stack];
		[imageArrayController bind:@"contentArray" toObject:[stackController contentController] withKeyPath:@"arrangedObjects.image" 
						   options:[NSDictionary dictionaryWithObjectsAndKeys:
									[NSNumber numberWithBool:YES], NSConditionallySetsEditableBindingOption, 
									[NSNumber numberWithBool:YES], NSRaisesForNotApplicableKeysBindingOption, nil]];
		
		
		[stackController setImageArrayController:imageArrayController];
		
		// so that the metadata view gets the same selection...
		if ([[imageArrayController arrangedObjects] count])
			[imageArrayController setSelectionIndex:0];
				
		[stackController removeFilterPredicate];
		
		
		// make the UI connections
		[[stackController backButton] setTarget:self];
		[[stackController backButton] setAction:@selector(hideStack:)];
		
		isShowingStack = YES;
	}
	
	
}

- (void)hideStack:(id)sender
{
	// trigger a selection update
	[self outlineViewSelectionDidChange:nil];
	
}

- (void)hideStack
{
	isShowingStack = NO;
	
	[self connectImageArrayController];
}

- (BOOL)isShowingStack
{
	return isShowingStack;
}


# pragma mark -
# pragma mark Capture

- (void)captureDeviceBrowser:(CaptureDeviceBrowser *)browser didAddDevice:(CaptureDevice *)device;
{
	[mainSourceListItems removeObject:devicesGroupItem];
	[mainSourceListItems insertObject:devicesGroupItem atIndex:2];
	
	id item = [self selectedItem];
	[sourceOutlineView reloadData];
	[self selectItem:item];
	
	[self expandStandardGroups];
}

- (void)captureDeviceBrowser:(CaptureDeviceBrowser *)browser didRemoveDevice:(CaptureDevice *)device;
{
	if ([self selectedItem] == device)
		[self selectItem:[[MGLibraryControllerInstance library] libraryGroup]];
	
	if ([[browser devices] count] == 0)
		[mainSourceListItems removeObject:devicesGroupItem];
	
	[sourceOutlineView reloadData];
}


# pragma mark -
# pragma mark Conversion between OLV item and Album

- (Album *)albumForOutlineItem:(id)item
{
	if ([item isKindOfClass:[Album class]])
		return item;
	
	if (item == projectsGroupItem)
		return [[MGLibraryControllerInstance library] projectsCollection];
	
	return nil;
}

# pragma mark -
# pragma mark Selection

- (id)selectedItem;
{
	NSInteger row = [[sourceOutlineView selectedRowIndexes] firstIndex];
	
	if (row > -1)
		return [sourceOutlineView itemAtRow:row];
				
	return nil;	
}

- (Album *)selectedCollection;
{
	return [self albumForOutlineItem:[self selectedItem]];
}


// Used to bind menu items based on the current selection
- (BOOL)selectedCollectionIsGroup;
{
	return [[self selectedCollection] isGroup];
}

- (BOOL)selectedCollectionIsFolder;
{
	return [[self selectedCollection] isFolder];
}

- (BOOL)selectedCollectionIsSmartFolder;
{
	return [[self selectedCollection] isSmartAlbum];
}

- (BOOL)selectedCollectionIsLightTable;
{
	return [[self selectedCollection] isLightTable];
}

- (BOOL)selectedItemIsDevice;
{
	return ([[self selectedItem] isKindOfClass:[CaptureDevice class]]);
}

# pragma mark -
# pragma mark Outline View Datasource methods


- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
	// the main items (no parent)
	if (item == nil)
		return [mainSourceListItems objectAtIndex:index];
	
	// the direct children of these main items
	if (item == allImagesGroupItem)
		return [[MGLibraryControllerInstance library] libraryGroup];
	
	if (item == recentGroupItem) {
		if (index == 0)
			return [[MGLibraryControllerInstance library] lastImportGroup];
		if (index == 1)
			return [[MGLibraryControllerInstance library] lastMonthGroup];
	}
	
	if (item == devicesGroupItem)
		return [[[CaptureDeviceBrowser sharedBrowser] devices] objectAtIndex:index];
	
	if (item == projectsGroupItem)
		return [[[MGLibraryControllerInstance library] projectsCollection] childAtIndex:index];
	
	if ([item isKindOfClass:[Album class]])
		return [(Album *)item childAtIndex:index];
	
	
	
	return nil;
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
	// main items (group style)
	if (item == nil)
		return [mainSourceListItems count];
	
	if (item == allImagesGroupItem)
		return 1;
	
	if (item == recentGroupItem)
		return 2;
	
	if (item == devicesGroupItem)
		return [[[CaptureDeviceBrowser sharedBrowser] devices] count];
	
	// projects group
	if (item == projectsGroupItem)
		return [[[[MGLibraryControllerInstance library] projectsCollection] valueForKey:@"children"] count];
	
	if ([item isKindOfClass:[Album class]])
		return [[item valueForKey:@"children"] count];
	
	
	return 0;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
	if (![[tableColumn identifier] isEqualToString:@"name"])
		return nil;
	
	// the main groups
	if (item == allImagesGroupItem)
		return @"ALL IMAGES";
	if (item == recentGroupItem)
		return @"RECENT";
	if (item == devicesGroupItem)
		return @"CAMERAS";
	if (item == projectsGroupItem)
		return @"PROJECTS";
	
	// the hardcoded subgroups
	if (item == [[MGLibraryControllerInstance library] libraryGroup])
		return @"Library";
	if (item == [[MGLibraryControllerInstance library] lastImportGroup])
		return @"Last Import";
	if (item == [[MGLibraryControllerInstance library] lastMonthGroup])
		return @"Last Month";
	
	if ([item isKindOfClass:[Album class]])
		return [item valueForKey:@"name"];
	
	if ([item isKindOfClass:[CaptureDevice class]])
		return [(CaptureDevice *)item uniqueName];

	return [item description];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
	return [self outlineView:outlineView numberOfChildrenOfItem:item] > 0;
}



- (id)outlineView:(NSOutlineView *)outlineView itemForPersistentObject:(id)object
{
	// try all the outline items
	NSInteger row = 0;
	
	while (YES) {
				
		id item = [outlineView itemAtRow:row];
		
		if (!item)
			return nil;
		
		Album *album = nil;
		if ((album = [self albumForOutlineItem:item]) && [[[[album objectID] URIRepresentation] absoluteString] isEqualToString:object])
			return album;
		
		row++;
	}
	
	return nil;
}

- (id)outlineView:(NSOutlineView *)outlineView persistentObjectForItem:(id)item;
{	
    return [[[[self albumForOutlineItem:item] objectID] URIRepresentation] absoluteString];
}

- (BOOL)outlineView:(NSOutlineView *)sender isGroupItem:(id)item;
{
	return [mainSourceListItems containsObject:item];
}


- (void)outlineView:(NSOutlineView *)outlineView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
	if (![[tableColumn identifier] isEqualToString:@"name"])
		return;
	
	if (![self albumForOutlineItem:item])
		return;
	
	[[self albumForOutlineItem:item] setValue:object forKey:@"name"];
	
	id selectedItem = [self selectedItem];
	
	// this reload is necessary...
	[self rearrangeObjects];
	[sourceOutlineView reloadData];
	
	[self selectItem:selectedItem];
}

# pragma mark -
# pragma mark Outline View Delegate methods

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	return ([item isKindOfClass:[Album class]] && [(Album *)item canEdit]);
}

- (BOOL) outlineView:(NSOutlineView*) outlineView shouldSelectItem:(id) item
{
	return ![mainSourceListItems containsObject:item];
}

- (CGFloat) outlineView:(NSOutlineView*) outlineView heightOfRowByItem:(id) item
{
	if ([self outlineView:outlineView isGroupItem:item])
		return 26;
	else
		return 22;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldShowOutlineCellForItem:(id)item
{
	return ![self outlineView:outlineView isGroupItem:item];
}


- (NSCell *)outlineView:(NSOutlineView *)outlineView dataCellForTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	if (!tableColumn)
		return nil;
	
	if (![[tableColumn identifier] isEqualToString:@"name"])
		return nil;
	
	if ([self outlineView:outlineView isGroupItem:item]) {
		
		id cell = [[[NSTextFieldCell alloc] init]autorelease];
		[cell setFont:[NSFont systemFontOfSize:11]];
		return cell;	
		
	}
	
	else {
		
		MGSourceOutlineCell* smallCell = [[[MGSourceOutlineCell alloc] init] autorelease];
		[smallCell setImage:[(Album *)item iconImage]];
		[smallCell setFont:[[tableColumn dataCell] font]];
		[smallCell setLineBreakMode:[[tableColumn dataCell] lineBreakMode]];
		[smallCell setFocusRingType:NSFocusRingTypeExterior];
		[smallCell setEditable:YES];
		
		if ([[self albumForOutlineItem:item] isSmartAlbum] && item != [[MGLibraryControllerInstance library] lastMonthGroup])
			[smallCell setBadgeImage:[NSImage imageNamed:@"action.tif"]];
		
		return smallCell;	
		
	}
	
		
	
}


- (void)outlineView:(NSOutlineView *)outlineView willDisplayOutlineCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	NSArray *reps = [[cell image] representations];
	if ([reps count] == 1) {
		
		NSImageRep *coreUIImageRep = [reps objectAtIndex:0];
		
		// private API
		NSDictionary *coreUIDrawOptions = (NSDictionary *)[(id)coreUIImageRep performSelector:@selector(coreUIDrawOptions)];
		NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:coreUIDrawOptions];
		
		
		//NSLog([dict description]);
		
		[dict setObject:@"normal" forKey:@"state"];
		
		if ([outlineView isItemExpanded:item]) 
			[dict setObject:@"down" forKey:@"direction"];
		else
			[dict setObject:@"right" forKey:@"direction"];
		
		
		
		NSImageRep *newRep = (NSImageRep *)[[[NSCoreUIImageRep alloc] initWithCoreUIDrawOptions:dict size:[coreUIImageRep size]] autorelease];
		[(id)newRep setSuppressesCache:YES];
		[(id)newRep setTemplate:YES];
		
		
		NSImage *newImage = [[[NSImage alloc] init] autorelease];
		[newImage addRepresentation:newRep];
		
		[cell setImage:newImage];
	}	
}


- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{		
	if (isShowingStack)
		[self hideStack];
	
	// close all capture connections/UI items, if the capture was opened
	if (captureController)
		[captureController close];
	
	
	if (isShowingLightTable) {
		isShowingLightTable = NO;
		[self connectImageArrayController];
	}
	
	if (isShowingSmartAlbum) {
		isShowingSmartAlbum = NO;
		[self connectImageArrayController];
	}
		
	
	
	
	// clear the image array ctl selection
	[imageArrayController setSelectedObjects:[NSArray array]];
		
	if ([self selectedItemIsDevice])
	{			
		[mainTabView selectTabViewItemAtIndex:4];
		
		if ([[mainTabView window] isVisible]) {
			NSTabViewItem *tabItem = [mainTabView tabViewItemAtIndex:4];
			if (!captureController) {
				captureController = [[CaptureController alloc] initWithSuperview:[tabItem view]];
				captureController.sourceListTreeController = self;
			}
			[captureController open];
			[captureController setDevice:(CaptureDevice *)[self selectedItem]];
			
		}
	}
	
	
	else if ([self selectedCollectionIsSmartFolder] || [self selectedCollectionIsFolder]) 
	{
		[mainTabView selectTabViewItemAtIndex:0];
		
		
		if ([self selectedCollectionIsSmartFolder]) {
			
			isShowingSmartAlbum = YES;
			
			[self disconnectImageArrayController];
			
			[imageArrayController bind:@"contentSet" 
							  toObject:self 
						   withKeyPath:@"selection.dynamicImages" 
							   options:[NSDictionary dictionaryWithObjectsAndKeys:
										[NSNumber numberWithBool:YES], NSConditionallySetsEditableBindingOption, 
										[NSNumber numberWithBool:YES], NSRaisesForNotApplicableKeysBindingOption, nil]];
		}
		
	}
	
	else if ([self selectedCollectionIsGroup]) 
	{
		// Switch to the group tab pane
		[mainTabView selectTabViewItemAtIndex:1];
	}
	
	else if ([self selectedCollectionIsLightTable]) {
		
		isShowingLightTable = YES;

		
		[mainTabView selectTabViewItemAtIndex:3];
		NSTabViewItem *tabItem = [mainTabView tabViewItemAtIndex:3];
		if (!lightTableController) {
			lightTableController = [[LightTableController alloc] initWithSuperview:[tabItem view]];
			
		}
		[lightTableController removeFilterPredicate];
		
		[self disconnectImageArrayController];
		
		[imageArrayController bind:@"contentArray" toObject:[lightTableController contentController] withKeyPath:@"arrangedObjects.image" 
						   options:[NSDictionary dictionaryWithObjectsAndKeys:
									[NSNumber numberWithBool:YES], NSConditionallySetsEditableBindingOption, 
									[NSNumber numberWithBool:YES], NSRaisesForNotApplicableKeysBindingOption, nil]];
	}	
	
	// Remove the current predicate to avoid confusion
	[imageArrayController setFilterPredicate:nil];
	
	
	// set the correct internal collection (== the set of which the images will be displayed by imageArrayController)
	if ([self selectedCollection])
		[self setSelectedObjects:[NSArray arrayWithObject:[self selectedCollection]]];
	else if ([self selectedItemIsDevice]) {
		NSSortDescriptor *sortDescDate = [[[NSSortDescriptor alloc] initWithKey:@"creationDate" ascending:YES] autorelease];
		[imageArrayController setSortDescriptors:[NSArray arrayWithObject:sortDescDate]];
		[self setSelectedObjects:[NSArray arrayWithObject:[[MGLibraryControllerInstance library] lastCaptureGroup]]];
	}
	else
		[self setSelectedObjects:nil];

}



- (BOOL)outlineView:(NSOutlineView *)outlineView shouldTrackCell:(NSCell *)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	return YES;
}


# pragma mark -
# pragma mark Outline Cell delegate (predicate window)


- (BOOL)sourceOutlineView:(MGSourceOutlineView *)view wasClickedAtPoint:(NSPoint)point forRow:(NSInteger)row;
{
	// returns YES to let the outline view continue normal event handling
	
	Album *album = [self albumForOutlineItem:[view itemAtRow:row]];
	
	if (![album isSmartAlbum] || album == [[MGLibraryControllerInstance library] lastMonthGroup])
		return YES;
	
	if (NSMaxX([view bounds]) - point.x > 25)
		return YES;
	
	
	NSRect rowRect = [view frameOfCellAtColumn:0 row:row];
	NSPoint rowMiddlePoint = NSMakePoint(NSMaxX(rowRect), NSMidY(rowRect));
	
	NSPoint basePoint = [[view window] convertBaseToScreen:[view convertPoint:rowMiddlePoint toView:nil]];
	
	basePoint.y -= 1;
	
	[sourceOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
	[self showPredicateWindowAtPoint:basePoint forRow:row];
	
	return YES;
}



# pragma mark -
# pragma mark Outline View control methods

- (void)expandStandardGroups
{
	[sourceOutlineView reloadData];
	
	[sourceOutlineView expandItem:allImagesGroupItem];
	[sourceOutlineView expandItem:recentGroupItem];
	[sourceOutlineView expandItem:devicesGroupItem];
	[sourceOutlineView expandItem:projectsGroupItem];
}

- (void)expandCollection:(id)aFolder;
{
	[sourceOutlineView expandItem:aFolder];
}

- (void)selectItem:(id)aFolder;
{
	NSInteger row = [sourceOutlineView rowForItem:aFolder];
	
	if (row > -1)
		[sourceOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
}

- (void)editCollection:(id)aFolder;
{
	[self selectItem:aFolder];
	
	NSInteger row = [sourceOutlineView rowForItem:aFolder];
	
	if (row > -1)
		[sourceOutlineView editColumn:0 row:row withEvent:nil select:YES];
}


# pragma mark -
# pragma mark Drag and drop

- (BOOL)outlineView:(NSOutlineView *)outlineView writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard;
{
	for (id item in items) {
		Album *album = [self albumForOutlineItem:item];
		
		if (!album || ![album canBeDragged]) {
			return NO;
		}
	}
		
	[self setItemsBeingDragged:items];
		
	NSString *draggedImageType = @"DRAGGED_COLLECTIONS_TYPE";
    NSArray *typesArray = [NSArray arrayWithObject: draggedImageType];
    [pboard declareTypes:typesArray owner:self];
	
	
	return YES;
}





- (NSDragOperation)outlineView:(NSOutlineView *)outlineView validateDrop:(id < NSDraggingInfo >)info proposedItem:(id)item proposedChildIndex:(NSInteger)index;
{
	Album *droppedOnAlbum = [self albumForOutlineItem:item];
    	
	if (!droppedOnAlbum)
		return NSDragOperationNone;
	
	NSPasteboard *pboard = [info draggingPasteboard];
	
	if ([info draggingSource] == nil) {
		// We are dragging from the Finder
		if ([droppedOnAlbum acceptsDroppedImagesFromFinder]) {
			[outlineView setDropItem:item dropChildIndex:NSOutlineViewDropOnItemIndex];
			return NSDragOperationCopy;
		} else {
			return NSDragOperationNone;
		}
	}
	
	else if ([info draggingSource] == outlineView) {
		
		// We are dragging collections around
		if ([droppedOnAlbum canHaveAlbumsAsChildren:[self itemsBeingDragged]]) {
			[outlineView setDropItem:item dropChildIndex:index];
			return NSDragOperationMove;
		}
		else {
			
			// try to retarget the operation
			
			
			return NSDragOperationNone;
		}
		
	}
	
	else if ([pboard dataForType:@"StackImagesDragType"]) {
		
		if ([droppedOnAlbum acceptsDroppedImages] || droppedOnAlbum == [[MGLibraryControllerInstance library] libraryGroup]) {
			[outlineView setDropItem:item dropChildIndex:NSOutlineViewDropOnItemIndex];
			return NSDragOperationCopy;
		} else {
			return NSDragOperationNone;
		}
		
	}
	
	else {
		if ([[pboard types] containsObject:draggedImageType]) {
			// We are dragging images around
			if ([droppedOnAlbum acceptsDroppedImages]) {
				
				NSArray *droppedImages = [(ImageArrayController *)imageArrayController selectedObjects];
				BOOL containsStack = NO;
				for (id image in droppedImages)
					if ([image isKindOfClass:[Stack class]])
						containsStack = YES;
				
				if (containsStack && ![droppedOnAlbum isMemberOfClass:[Album class]]) {
					return NSDragOperationNone;
				} else {
					[outlineView setDropItem:item dropChildIndex:NSOutlineViewDropOnItemIndex];
					return NSDragOperationCopy;
				}
			}
			else {
				// If it's a smart album or the Library, don't accept a drop!
				return NSDragOperationNone;
			}
		}
		else {
			// We are dragging keywords
			if ([droppedOnAlbum acceptsDroppedKeywords]) {
				[outlineView setDropItem:item dropChildIndex:NSOutlineViewDropOnItemIndex];
				return NSDragOperationCopy;
			}
			else {
				// If it's a smart album or the Library, don't accept a drop!
				return NSDragOperationNone;
			}
		}
	}
}



- (BOOL)outlineView:(NSOutlineView *)outlineView acceptDrop:(id <NSDraggingInfo>)info item:(id)item childIndex:(NSInteger)index;
{
	
	Album *droppedOnAlbum = [self albumForOutlineItem:item];
	
	if (!droppedOnAlbum)
		return NO;
	
	NSPasteboard *pboard = [info draggingPasteboard];
	
	
	if ([info draggingSource] == nil) {
		// Dragging from the Finder
		if ([[pboard types] containsObject:NSFilenamesPboardType]) {
            
            // don't do this -- it screws up the drag session...
            //[NSApp activateIgnoringOtherApps:YES];
            //[[[NSApplication sharedApplication] mainWindow] makeKeyAndOrderFront:self];
            
			NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
			MGImageBatchImporter *imp = [[[MGImageBatchImporter alloc] init] autorelease];
			imp.album = droppedOnAlbum;
			imp.libraryAlbum = [[MGLibraryControllerInstance library] libraryGroup];
			imp.lastImportAlbum = [[MGLibraryControllerInstance library] lastImportGroup];
			imp.projectsAlbum = [[MGLibraryControllerInstance library] projectsCollection];
			[imp importImageFilesAndDirectories:files];
			return YES;
            
            
		}
		return NO;
	}
	
	
	else if ([info draggingSource] == outlineView) {
		// We are dragging collections around

		
		
		if ([droppedOnAlbum canHaveAlbumsAsChildren:[self itemsBeingDragged]]) {
		
			for (id item in [self itemsBeingDragged]) {
				[droppedOnAlbum addChild:item atIndex:index];
				
				id selectedItem = [self selectedItem];
				
				// this reload is necessary...
				[self rearrangeObjects];
				[sourceOutlineView reloadData];
				[sourceOutlineView expandItem:droppedOnAlbum];
				
				[self selectItem:selectedItem];
				
			}
			return YES;
		}
		else {
			return NO;
		}
		
	}
	
	
	else if ([pboard dataForType:@"StackImagesDragType"]) {
		
		NSAlert *alert = [NSAlert alertWithMessageText:@"These stack images will be duplicated"
										 defaultButton:@"OK"
									   alternateButton:nil
										   otherButton:nil
							 informativeTextWithFormat:@"Macnification will create a copy of the dragged images in the Library, and will add the duplicates to \"%@\".", [droppedOnAlbum valueForKey:@"name"]];
		
		[alert setShowsSuppressionButton:YES];
		[[alert suppressionButton] setTitle:@"Do not show this warning again"];

		if (![[NSUserDefaults standardUserDefaults] boolForKey:@"suppressDuplicateStackImageWarning"]) {
			
			[alert runModal];
			if ([[alert suppressionButton] state] == NSOnState)
				[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressDuplicateStackImageWarning"];
			
		}
		
		
		
		// copy the image and add it to
		// - the Library (because it is an explicit copy)
		// - the dropped on Album (because the user wants to add it to this album)
		
		NSArray *droppedImages = [stackController draggedImages];
		
		
		for (Image *image in droppedImages) {
			
			Image *duplicate = (Image *)[image clone];
			
            
            [duplicate applyOriginalImagePathWithFileName:[duplicate valueForKey:@"name"] extension:[[image originalImagePathExpanded:NO] pathExtension]];
			
			// copy the image file
			NSString *sourcePath = [image originalImagePathExpanded:YES];
			NSString *dstPath = [duplicate originalImagePathExpanded:YES];
			
            			
			if (dstPath)
				[[NSFileManager defaultManager] copyItemAtURL:[NSURL fileURLWithPath:sourcePath] toURL:[NSURL fileURLWithPath:dstPath] error:nil];
			
			[duplicate setValue:[NSNumber numberWithBool:NO] forKey:@"hasDuplicate"];
			
            
            [duplicate updateThumbnail];

			// remove all unnecessary bindings
			[duplicate removeAllStackImages];
			[duplicate removeAllLightTableImages];
			
			// remove existing albums
			NSMutableSet *albums = [duplicate mutableSetValueForKey:@"albums"];
			[albums removeAllObjects];
			
			// add the new image to the appropriate albums
			[droppedOnAlbum addImages:[NSArray arrayWithObjects:duplicate, nil]];
			[[[MGLibraryControllerInstance library] libraryGroup] addImages:[NSArray arrayWithObjects:duplicate, nil]];
			
			
		}
		
		// save.
		// if we don't do this and the created images are removed again before any save occurred, the "after deletion" save wil fail.
		NSError *error = nil;
		if (![[self managedObjectContext] save:&error])
			[[NSApplication sharedApplication] presentError:error];
		
		
		return YES;
		
	}
	
	else {
		if ([[pboard types] containsObject:draggedImageType]) {
			// We are dragging images around
			if ([droppedOnAlbum acceptsDroppedImages]) {
				NSArray *droppedImages = [(ImageArrayController *)imageArrayController selectedObjects];
				
				[droppedOnAlbum addImages:droppedImages];
								
				[imageArrayController observeValueForKeyPath:@"arrangedObjects" ofObject:imageArrayController change:nil context:NULL];
				
				return YES;
			}
			else {
				// If it's a smart album or the Library, don't accept a drop!
				return NO;
			}
		}
		else {
			// We are dragging keywords
			if ([droppedOnAlbum acceptsDroppedKeywords]) {
				// Get the keywords that are dragged from the keywordTreeController
				NSMutableSet *draggedKeywords = [(KeywordTreeController *)[[info draggingSource]delegate] draggedKeywords];
				NSArray *draggedKeywordsArray = [draggedKeywords allObjects];
				// Add them to the album
				[droppedOnAlbum addKeywords:draggedKeywordsArray];
				return YES;
			}
			else {
				// If it's a smart album or the Library, don't accept a drop!
				return NO;
			}
		}
	}
	return NO;
}







# pragma mark -
# pragma mark Smart Albums

// Called by the predicate editor whenever the predicate of the selected SmartAlbum changes
- (IBAction)setPredicate:(id)sender;
{
	if ([self selectedCollectionIsSmartFolder])
		[(SmartAlbum *)[self selectedCollection] setValue:[NSKeyedArchiver archivedDataWithRootObject:[predicateEditor objectValue]] forKey:@"predicateData"];
}



- (void)showPredicateWindowAtPoint:(NSPoint)point forRow:(NSInteger)row;
{
	id album = [self albumForOutlineItem:[sourceOutlineView itemAtRow:row]];
	
	
	// Only show this window for a smart album
	if ([album isKindOfClass:[SmartAlbum class]]) {
		NSData *predicateData = [(SmartAlbum *)album valueForKey:@"predicateData"];
		if (predicateData) {
			// If the smart album already has a predicate, use it
			NSPredicate *predicate = [NSKeyedUnarchiver unarchiveObjectWithData:predicateData];
			[predicateEditor setObjectValue:predicate];
		}
		
		while ([predicateEditor numberOfRows] < 2)
			[predicateEditor addRow:self];
		
		// Show the sheet and move it a bit to the right and bottom to match the position of the action icon
		if (!predicateWindow) {
			predicateWindow = [[MAAttachedWindow alloc] initWithView:predicateView attachedToPoint:point inWindow:mainWindow onSide:MAPositionRight atDistance:0.0];
			[predicateWindow setBackgroundColor:[NSColor colorWithCalibratedRed:0.909 green:0.909 blue:0.909 alpha:1.0]];
			[predicateWindow setHasArrow:YES];
			[predicateWindow setBorderWidth:0.0];
			[predicateView setColor:[NSColor colorWithCalibratedRed:0.909 green:0.909 blue:0.909 alpha:1.0]];
		}
		
		if (![predicateWindow isVisible]) {
			
			
			[predicateWindow setAlphaValue:0.0];
			[mainWindow addChildWindow:predicateWindow ordered:NSWindowAbove];
			[predicateWindow setFrameOrigin:NSMakePoint(point.x + 5, point.y - [predicateView frame].size.height/2)];
			[predicateWindow makeKeyAndOrderFront:self];
			[[predicateWindow animator] setAlphaValue:0.9];
		} else {
			
			[predicateWindow setFrameOrigin:NSMakePoint(point.x + 5, point.y - [predicateView frame].size.height/2)];
			[predicateWindow makeKeyAndOrderFront:self];
		}

	}
}


// Save the predicate and close the window
- (IBAction)savePredicateWindow:(id)sender;
{
	// Only save a predicate for a smart album
	if ([self smartAlbumIsSelected]) {
		[self setPredicate:self];
	}
	[self closePredicateWindow:self];
}

- (NSWindow *)predicateWindow
{
	return predicateWindow;
}

// Close the window without saving (Cancel button)
- (IBAction)closePredicateWindow:(id)sender;
{
	if (predicateWindow) {
		[mainWindow removeChildWindow:predicateWindow];
        [predicateWindow orderOut:self];
	}
}



- (BOOL)smartAlbumIsSelected;
{
	if ([[self selectedObjects] count] > 0  && [[[[self selectedCollection] entity]name]isEqualTo:@"SmartAlbum"] ) {
		return YES;
	}
	return NO;	
}


- (void)searchCurrentFolderWithPredicate:(NSPredicate *)filterPredicate naturalLanguageString:(NSString *)description;
{
	
	id selectedCollection = [self selectedCollection];
	NSString *selectedCollectionEntityName = [[selectedCollection entity] name];
	
	if ([selectedCollection isMemberOfClass:[Album class]]) {
		[imageArrayController searchCurrentFolderWithPredicate:filterPredicate naturalLanguageString:description];
	}
	else if ([selectedCollectionEntityName isEqualTo:@"Stack"]) {
		NSPredicate *newPred = [NSPredicate predicateWithFormat:[@"image." stringByAppendingString:[filterPredicate predicateFormat]]];
		
		[stackController searchCurrentFolderWithPredicate:newPred naturalLanguageString:description];
	}
	else if ([selectedCollectionEntityName isEqualTo:@"LightTable"]) {
		NSPredicate *newPred = [NSPredicate predicateWithFormat:[@"image." stringByAppendingString:[filterPredicate predicateFormat]]];
		
		[lightTableController searchCurrentFolderWithPredicate:newPred naturalLanguageString:description];
	}		
	
}




# pragma mark -
# pragma mark Adding Collections

- (IBAction)addGroup:(id)sender;
{
	// Add the Project object
	id list = [NSEntityDescription insertNewObjectForEntityForName:@"Group" inManagedObjectContext:[super managedObjectContext]];
	[list setValue:@"untitled project" forKey:@"name"];
	
	[[[MGLibraryControllerInstance library] projectsCollection] addChildToEnd:list];
	
	[sourceOutlineView reloadData];
	[self rearrangeObjects];
	
	
	[self performSelector:@selector(editCollection:) withObject:list afterDelay:0.05];
}



- (IBAction)addSubCollection:(id)sender;
{
	NSString *entity;
	NSString *defaultName;
	//NSNumber *rank;
	switch ([sender tag]) {
		case 0:
		default:
			entity = @"Album";
			defaultName = @"untitled folder";
			break;
		case 1:
			entity = @"SmartAlbum";
			defaultName = @"untitled smart folder";
			break;
		case 2:
			entity = @"LightTable";
			defaultName = @"untitled light table";
			break;
	}
	
	id list = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:[super managedObjectContext]];
	[list setValue:defaultName forKey:@"name"];
	
	if ([self selectedCollectionIsGroup])
		[[self selectedCollection] addChildToEnd:list];
	else if ([[[self selectedCollection] parentGroup] isGroup])
		[[[self selectedCollection] parentGroup] addChildToEnd:list];
	else
		[[[MGLibraryControllerInstance library] projectsCollection] addChildToEnd:list];
	
	[sourceOutlineView reloadData];
	[self rearrangeObjects];
	
	[self expandCollection:[list valueForKey:@"parent"]];
	
	[self performSelector:@selector(editCollection:) withObject:list afterDelay:0.05];
}



- (IBAction)addFolderWithSelectedImages:(id)sender;
{
	NSArray *selectedImages = [[[imageArrayController selectedObjects] copy] autorelease];
	
	id list = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext:[super managedObjectContext]];
	if ([self selectedCollectionIsGroup])
		[[self selectedCollection] addChildToEnd:list];
	else if ([[[self selectedCollection] parentGroup] isGroup])
		[[[self selectedCollection] parentGroup] addChildToEnd:list];
	else
		[[[MGLibraryControllerInstance library] projectsCollection] addChildToEnd:list];

	[list setValue:@"untitled folder" forKey:@"name"];
	[list setValue:[NSSet setWithArray:selectedImages] forKey:@"images"];
	
	[sourceOutlineView reloadData];
	[self rearrangeObjects];
	
	[self expandCollection:[list valueForKey:@"parent"]];
	
	[self performSelector:@selector(editCollection:) withObject:list afterDelay:0.05];	
}



- (IBAction)addLightTableWithSelectedImages:(id)sender;
{
	NSArray *selectedImages = [[[imageArrayController selectedObjects] copy] autorelease];
	
	id list = [NSEntityDescription insertNewObjectForEntityForName:@"LightTable" inManagedObjectContext:[super managedObjectContext]];
	
	if ([self selectedCollectionIsGroup])
		[[self selectedCollection] addChildToEnd:list];
	else if ([[[self selectedCollection] parentGroup] isGroup])
		[[[self selectedCollection] parentGroup] addChildToEnd:list];
	else
		[[[MGLibraryControllerInstance library] projectsCollection] addChildToEnd:list];

	[list addImages:selectedImages];
	[list setValue:@"untitled light table" forKey:@"name"];

	[sourceOutlineView reloadData];
	[self rearrangeObjects];
	
	[self expandCollection:[list valueForKey:@"parent"]];
	
	[self performSelector:@selector(editCollection:) withObject:list afterDelay:0.05];	

}



# pragma mark -
# pragma mark Add and remove




// Warn the user when removing a list
- (void)remove:(id)sender
{
	id selectedCollection = [self selectedCollection];
	if (selectedCollection == [[MGLibraryControllerInstance library] libraryGroup] || 
        selectedCollection == [[MGLibraryControllerInstance library] lastImportGroup] || 
        selectedCollection == [[MGLibraryControllerInstance library] lastMonthGroup] ) {
		NSBeep();
	}
	
	else if ([selectedCollection isGroup]) {
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"Delete"];
		[alert addButtonWithTitle:@"Cancel"];
		[alert setMessageText:@"Are you sure you want to delete the selected Project?"];
		[alert setInformativeText:@"All (smart) folders, light tables and stacks in this project will be removed.  Images will remain in the Library."];
		[alert setShowsSuppressionButton:YES];
		[[alert suppressionButton] setTitle:@"Do not show this warning again"];
		
		[alert setAlertStyle:NSWarningAlertStyle];
		// don't run the alert when suppressed
		if ([[NSUserDefaults standardUserDefaults] boolForKey:@"suppressRemoveCollectionWarning"] || [alert runModal] == NSAlertFirstButtonReturn) {
			
			// first, set the suppression value in the prefs (only do this if "Cancel" wasn't chosen)
			if ([[alert suppressionButton] state] == NSOnState)
				[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressRemoveCollectionWarning"];
			
			
			// Get the current selection index path and calculate the index path for the Group above it
			// If there is no Group above it, select the library
			NSIndexPath *oldIndexPath = [self selectionIndexPath];
			int secondIndex = [oldIndexPath indexAtPosition:1];
			NSIndexPath *indexToSelectAfterDeletion;
			if (secondIndex == 0) {
				NSUInteger indexPath[2] = {0, 0};
				indexToSelectAfterDeletion = [NSIndexPath indexPathWithIndexes:indexPath length:2];
			}
			else {
				indexToSelectAfterDeletion = [[oldIndexPath indexPathByRemovingLastIndex] indexPathByAddingIndex:secondIndex - 1];
			}
			
			// remove the children completely from the data model
			NSMutableSet *children = [selectedCollection mutableSetValueForKey:@"children"];
			for (id album in children) {
				
				[[self managedObjectContext] deleteObject:album];

				// Prepare the album for deletion
				[album prepareForDeletion];
				
				// Remove every album from the MOC
				[[self managedObjectContext] deleteObject:album];
				
			}
			
			// Remove all children from the Projects folder children's relationship
			[children removeAllObjects];
			
			// Remove the project folder itself
			[[self managedObjectContext] deleteObject:selectedCollection];
			
			// Save the MOC
			[MGLibraryControllerInstance save];
			
			// Select the library (to avoid selecting the PROJECTS or ALL IMAGES headers)
			//[self selectCollection:[self libraryGroup]];
			[self setSelectionIndexPath:indexToSelectAfterDeletion];
		}
		
		[alert release];
	}
	
	
	else if ([selectedCollection isSmartAlbum]) {
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"Delete"];
		[alert addButtonWithTitle:@"Cancel"];
		[alert setMessageText:@"Are you sure you want to delete the selected Smart Album?"];
		[alert setInformativeText:@"The images in this Smart Album will remain in the Library."];
		[alert setShowsSuppressionButton:YES];
		[[alert suppressionButton] setTitle:@"Do not show this warning again"];
		
		[alert setAlertStyle:NSWarningAlertStyle];

		
		// don't run the alert when suppressed
		if ([[NSUserDefaults standardUserDefaults] boolForKey:@"suppressRemoveCollectionWarning"] || [alert runModal] == NSAlertFirstButtonReturn) {
			
			// first, set the suppression value in the prefs (only do this if "Cancel" wasn't chosen)
			if ([[alert suppressionButton] state] == NSOnState)
				[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressRemoveCollectionWarning"];
			
			
			// Remove the project folder itself
			[[self managedObjectContext] deleteObject:selectedCollection];
			
			// Save the MOC
			[MGLibraryControllerInstance save];

			// Select the library (to avoid selecting the PROJECTS or ALL IMAGES headers)
			[self selectItem:[[MGLibraryControllerInstance library] libraryGroup]];
		}
		
		[alert release];
	}
	
	
	
	
	else {
		
		NSString *albumDescriptor = @"folder";
		if ([[[selectedCollection entity] name] isEqualTo:@"LightTable"])
			albumDescriptor = @"light table";
		
		// Create the alert panel and show it
		NSAlert *alert = [[NSAlert alloc] init];
		[alert addButtonWithTitle:@"Delete"];
		[alert addButtonWithTitle:@"Cancel"];
		[alert setMessageText:[NSString stringWithFormat:@"Are you sure you want to delete the selected %@?", albumDescriptor]];
		[alert setInformativeText:[NSString stringWithFormat:@"The images in this %@ will remain in the Library.", albumDescriptor]];
		[alert setShowsSuppressionButton:YES];
		[[alert suppressionButton] setTitle:@"Do not show this warning again"];
		
		[alert setAlertStyle:NSWarningAlertStyle];

		
		// don't run the alert when suppressed
		if ([[NSUserDefaults standardUserDefaults] boolForKey:@"suppressRemoveCollectionWarning"] || [alert runModal] == NSAlertFirstButtonReturn) {
			
			// first, set the suppression value in the prefs (only do this if "Cancel" wasn't chosen)
			if ([[alert suppressionButton] state] == NSOnState)
				[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"suppressRemoveCollectionWarning"];
			
		
			// Remove the images from the collection 
			if (![[[selectedCollection entity] name] isEqualTo:@"SmartAlbum"]) {
				[selectedCollection setValue:nil forKey:@"images"];
			}
			
			[super remove:self];
		}
		[alert release];
	}
	
	[[self managedObjectContext] processPendingChanges];
	
	// we need to reload data here, so that deleted objects are removed from the source list.
	// if not, the display routine will cause the managed object to be asked for invalid attributes (faulting error)
	[sourceOutlineView reloadData];
}






# pragma mark -
# pragma mark Importing Images


- (IBAction)importImages:(id)sender;
{
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	[panel setCanChooseDirectories:YES];
	[panel setAllowsMultipleSelection:YES];
	[panel setPrompt:@"Import"];
	//[panel setAccessoryView:importAccessoryView];
	[panel beginSheetForDirectory:nil file:nil types:[MGImageBatchImporter supportedFileTypes] modalForWindow:mainWindow modalDelegate:self didEndSelector:@selector(openPanelDidEnd:returnCode:contextInfo:) contextInfo:nil];
}


- (void)openPanelDidEnd:(NSOpenPanel *)openPanel returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{
	if (returnCode == NSOKButton) {
		NSArray *files = [openPanel filenames];
		[openPanel close];
		[mainWindow makeKeyAndOrderFront:self];
		[self importFiles:files];
	}
}


- (void)importFiles:(NSArray *)files;
{
        
    // Get the selected Album
    id album = [self selectedCollection];
    
    // Send the array of selected files to the ImageImporter
    MGImageBatchImporter *imp = [[[MGImageBatchImporter alloc] init] autorelease];
    imp.splitChannels = ([importSplitChannelsButton state] == NSOnState);
    imp.album = album;
    imp.libraryAlbum = [[MGLibraryControllerInstance library] libraryGroup];
    imp.lastImportAlbum = [[MGLibraryControllerInstance library] lastImportGroup];
    imp.projectsAlbum = [[MGLibraryControllerInstance library] projectsCollection];

    [imp importImageFilesAndDirectories:files];
    
}

// auxiliary methods that decouple the array controller from the data model while importing, to avoid artifacts and unneccessary updates.
- (void)disconnectImageArrayController;
{
	[imageArrayController unbind:@"contentSet"];	
}

- (void)connectImageArrayController;
{
    
	[imageArrayController bind:@"contentSet" 
					  toObject:self 
				   withKeyPath:@"selection.images" 
					   options:[NSDictionary dictionaryWithObjectsAndKeys:
								[NSNumber numberWithBool:YES], NSConditionallySetsEditableBindingOption, 
								[NSNumber numberWithBool:YES], NSRaisesForNotApplicableKeysBindingOption, nil]];

}


# pragma mark -
# pragma mark Menu validation

- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
    SEL theAction = [anItem action];
	
    if (theAction == @selector(addFolderWithSelectedImages:) || theAction == @selector(addLightTableWithSelectedImages:))    {
        if ([[imageArrayController selectedObjects] count] > 0) {
            return YES;
        }
        return NO;
    } 
	
	else if (theAction == @selector(addStackWithSelectedImages:)) {
        if ([[imageArrayController selectedObjects] count] > 0) {
			return YES;
		}
		return NO;
	}
	
	else if (theAction == @selector(remove:)) {
		id selectedAlbum = (Album *)[self selectedCollection];
		return [selectedAlbum canEdit];
	}
	
	return YES;
}



# pragma mark -
# pragma mark Slideshows

- (IBAction)runSlideshow:(id)sender;
{
	if ([[[self selectedCollection] valueForKey:@"images"] count] > 0) {
		// Only run a slideshow if there are images available
		[[IKSlideshow sharedSlideshow] runSlideshowWithDataSource: (id<IKSlideshowDataSource>)self inMode:IKSlideshowModeImages options:nil];
	}
    ;

}



- (NSUInteger)numberOfSlideshowItems;
{
	return [[[self selectedCollection] valueForKey:@"images"] count];
}


- (id)slideshowItemAtIndex:(NSUInteger)index;
{
    ;

	NSSet *allImagesSet = [[self selectedCollection] valueForKey:@"images"];
	NSArray *allImagesArray = [allImagesSet allObjects];
	NSSortDescriptor *sortD = [[NSSortDescriptor alloc] initWithKey:@"creationDate" ascending:YES];
	NSSortDescriptor *sortD2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortedImages = [allImagesArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortD, sortD2, nil]];
	[sortD release];
	[sortD2 release];
	return [[sortedImages objectAtIndex:index] valueForKey:@"filteredImage"];
}





# pragma mark -
# pragma mark Accessors

- (NSArray *)itemsBeingDragged {
	return [[itemsBeingDragged retain] autorelease];
}

- (void)setItemsBeingDragged:(NSArray *)value {
	if (itemsBeingDragged != value) {
		[itemsBeingDragged release];
		itemsBeingDragged = [value retain];
	}
}

- (StackController *)stackController;
{
	return stackController;
	
}

- (CaptureController *)captureController;
{
	return captureController;
}

@end
