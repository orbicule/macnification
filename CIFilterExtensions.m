//
//  CIFilterExtensions.m
//  Macnification
//
//  Created by Peter Schols on 02/03/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "CIFilterExtensions.h"


@implementation CIFilter (CIFilterExtensions)


+ (CIImage *)filterImage:(CIImage *)original withFilters:(NSArray *)filters
{
    if (!original)
        return nil;
    
	if ([filters count] == 0) return original;
	
	NSInteger numberOfAppliedFilters = 0;
	
	for (CIFilter *filter in filters) {
		
		CIImage *inputImage;
		if (numberOfAppliedFilters == 0) {
			inputImage = original;
		} 
		else {
			inputImage = [[filters objectAtIndex:(numberOfAppliedFilters-1)] valueForKey:@"outputImage"];  
		}
		
		// Bind the inputImage to the outputImage of the previous filter
		[filter setValue:inputImage forKey:@"inputImage"];
		
		numberOfAppliedFilters++;
	}
	
	//NSLog(@"returning %@", [[[filters lastObject] valueForKey:@"outputImage"] description]);
	
	// Get the outputImage of the last filter
	return [[filters lastObject] valueForKey:@"outputImage"];
	
}


- (CIFilter *)filterByCopyingProperties
{
	NSDictionary *props = [self dictionary];
	
	CIFilter *copy = [CIFilter filterWithName:[self name]];
	[copy setDictionary:props];
	
	return copy;
}

- (NSDictionary *)dictionary;
{
	NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:5];
	NSArray *inputKeys = [self inputKeys];
	for (NSString *key in inputKeys) {
		if (![key isEqualTo:@"inputImage"]) {
			[dict setObject:[self valueForKey:key] forKey:key];
		}
	}
	//NSLog(@"Getting CIFilter attributes for CDFilter: %@", dict);
	return [NSDictionary dictionaryWithDictionary:dict];
}



- (void)setDictionary:(NSDictionary *)dict;
{
	//NSLog(@"Creating CIFilter with attributes: %@", dict);

	NSArray *allKeys = [dict allKeys];
	for (NSString *key in allKeys) {
		if (![key isEqualTo:@"inputImage"]) {
			[self setValue:[dict objectForKey:key] forKey:key];
		}
	}
}


- (BOOL)isCropFilter;
{
	return [[[self attributes] valueForKey:@"CIAttributeFilterName"] isEqualTo:@"CICrop"];
}


@end
