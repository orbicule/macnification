//
//  CaptureDeviceBrowserModule.h
//  Filament
//
//  Created by Dennis Lorson on 16/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>
#import <ImageCaptureCore/ImageCaptureCore.h>
//#import <MexJCam/MexCC.h>
//#import <MexJCam/MexCam.h>
#import <dc1394/dc1394.h>


@class CaptureDevice;
@class CaptureDeviceBrowserModule;

@protocol CaptureDeviceBrowserModuleDelegate <NSObject>

- (void)captureDeviceBrowserModule:(CaptureDeviceBrowserModule *)module didAddDevice:(CaptureDevice *)device;
- (void)captureDeviceBrowserModule:(CaptureDeviceBrowserModule *)module didRemoveDevice:(CaptureDevice *)device;

@end



@interface CaptureDeviceBrowserModule : NSObject
{
	id <CaptureDeviceBrowserModuleDelegate> delegate_;

	NSMutableArray *devices_;
}

@property (readwrite, assign) id <CaptureDeviceBrowserModuleDelegate> delegate;


+ (CaptureDeviceBrowserModule *)sharedBrowserModule;

- (void)startSearchingForDevices;
- (void)stopSearchingForDevices;

@end



@interface CaptureDeviceBrowserModuleQT : CaptureDeviceBrowserModule
{
	
}

@end


@interface CaptureDeviceBrowserModuleIC : CaptureDeviceBrowserModule <ICDeviceBrowserDelegate>
{
	ICDeviceBrowser *icDeviceBrowser_;

}

@end


@interface CaptureDeviceBrowserModulePR : CaptureDeviceBrowserModule
{
}

@end


@interface CaptureDeviceBrowserModuleDC1394 : CaptureDeviceBrowserModule
{
	NSTimer *pollTimer_;
	
	dc1394_t * dc1394_;
}

@end