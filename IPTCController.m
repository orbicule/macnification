//
//  IPTCController.h
//  IPTC Controller
//
//  Created by Peter Schols on 02/06/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "IPTCController.h"



@implementation IPTCController

/*
 // Test for this class
 - (IBAction)getImageInfo:(id)sender;
 {
 // Get image info
 NSArray *keywords = [self IPTCKeywordsForImageAtPath:@"/Users/peter/Desktop/iptc.tif"];
 NSLog(@"Tags: %@", [self IPTCTagsForImageAtPath:@"/Users/peter/Desktop/Diervilla rivularis A 05.tiff"]);
 NSLog(@"Keywords: %@", keywords);
 
 // Write image info
 [self setIPTCKeywords:[NSArray arrayWithObjects:@"olala", @"schatteke", nil] forImageAtPath:@"/Users/peter/Desktop/test.tif"];
 [self setIPTCTags:[NSDictionary dictionaryWithObject:@"Catheleyne D'hondt" forKey:@"Credit"] forImageAtPath:@"/Users/peter/Desktop/test.tif"];
 [self setIPTCTags:[NSDictionary dictionaryWithObject:@"(C) 2007 Peter Schols" forKey:@"CopyrightNotice"] forImageAtPath:@"/Users/peter/Desktop/test.tif"];
 [self setString:@"2007-05-25" forTag:@"DateCreated" atPath:@"/Users/peter/Desktop/test.tif"];
 }
 */



# pragma mark -
# pragma mark Getting and setting IPTC tags


- (NSDictionary *)IPTCTagsForImageAtPath:(NSString *)imagePath;
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
		NSURL *imageURL = [NSURL fileURLWithPath:imagePath];
		CGImageSourceRef isrc = CGImageSourceCreateWithURL((CFURLRef)imageURL, nil); 
		
		CFDictionaryRef props = CGImageSourceCopyPropertiesAtIndex(isrc,0,nil); 
		NSDictionary *allTags = [NSDictionary dictionaryWithDictionary:(NSDictionary *)props];
		CFRelease(isrc);
		CFRelease(props);
		return allTags;
	}
	else {
		return [NSDictionary dictionary];	
	}
}



- (void)setIPTCTags:(NSDictionary *)iptcDictionary forImageAtPath:(NSString *)imagePath;
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
		
		NSDictionary *newImageProperties = [NSDictionary dictionaryWithObject:iptcDictionary forKey:(NSString *)kCGImagePropertyIPTCDictionary];
		
		NSMutableData *newImageFileData = [[NSMutableData alloc] init];
		CGImageSourceRef imageSource = CGImageSourceCreateWithURL ((CFURLRef) [NSURL fileURLWithPath:imagePath], nil);
		CGImageDestinationRef imageDestination = CGImageDestinationCreateWithData((CFMutableDataRef)newImageFileData, CGImageSourceGetType(imageSource), 1, NULL);
		
		CGImageDestinationAddImageFromSource(imageDestination, imageSource, 0, (CFDictionaryRef) newImageProperties);
		
		if (CGImageDestinationFinalize(imageDestination))
			[newImageFileData writeToFile:imagePath atomically:YES];
		
		CFRelease(imageDestination);
		CFRelease(imageSource);
		[newImageFileData release];
	}
}



- (void)setString:(NSString *)aString forTag:(NSString *)aTag atPath:(NSString *)imagePath;
{
	if (aString) {
		[self setIPTCTags:[NSDictionary dictionaryWithObject:aString forKey:aTag] forImageAtPath:imagePath];
	}
}



# pragma mark -
# pragma mark Getting and setting IPTC keywords


- (NSArray *)IPTCKeywordsForImageAtPath:(NSString *)imagePath;
{
	return [[self IPTCTagsForImageAtPath:imagePath] objectForKey:@"kCGImagePropertyIPTCKeywords"];
}



- (void)setIPTCKeywords:(NSArray *)arrayOfKeywords forImageAtPath:(NSString *)imagePath;
{		
	NSMutableDictionary *iptcDictionary = [[NSDictionary dictionaryWithObject:arrayOfKeywords forKey:(NSString *)kCGImagePropertyIPTCKeywords] mutableCopy];
	[self setIPTCTags:iptcDictionary forImageAtPath:imagePath];
}




@end
