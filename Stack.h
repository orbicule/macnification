//
//  Stack.h
//  Filament
//
//  Created by Peter Schols on 09/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Image.h"
#import "StackImage.h"


@interface Stack : Image 
{
	NSArray* sortedImageArray_;
	
	CGFloat imageWidth_;
}

- (void)addImages:(NSArray *)images;
- (void)removeImages;

- (NSArray *)images;


- (CGFloat)imageWidth;

@end
