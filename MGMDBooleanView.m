//
//  MGMDBooleanView.m
//  Filament
//
//  Created by Dennis Lorson on 1/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDBooleanView.h"
#import "ImageArrayController.h"


CGFloat MGMDSideMargin = 0.0;

@implementation MGMDBooleanView


@synthesize enabled;

- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];
	
    if (self) {
		
		yesImage = [[NSImage imageNamed:@"BooleanYes.tiff"] retain];
		noImage = [[NSImage imageNamed:@"BooleanNo.tiff"] retain];
		multValImage = [[NSImage imageNamed:@"BooleanMultVal.tiff"] retain];
		
		self.enabled = YES;
		
    }
	
    return self;
}


- (void)dealloc
{
	[yesImage release];
	[noImage release];
	
	[super dealloc];
}

- (void)setEnabled:(BOOL)flag
{
	enabled = flag;
	
	
}

- (BOOL)isEnabled
{
	return enabled;
	
	
}
/*
- (void)bind:(NSString *)binding toObject:(id)observableController withKeyPath:(NSString *)aPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		[observableController addObserver:self forKeyPath:aPath options:0 context:0];

		controller = observableController;
		keyPath = [aPath retain];
	}
	
	
}

- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		[controller removeObserver:self forKeyPath:keyPath];
		[keyPath release];
		keyPath = nil;
	}
	
}
*/

- (void)bind:(NSString *)binding toObject:(id)observable withKeyPath:(NSString *)aPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		observableController = observable;
		observableKeyPath = [aPath retain];
		
		[(ImageArrayController *)observableController addManualObserver:self forKeyPath:aPath];
		[observableController addObserver:self forKeyPath:@"selection" options:0 context:nil];
		
	}
	
	
}

- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		[observableController removeObserver:self forKeyPath:@"selection"];
		[(ImageArrayController *)observableController removeManualObserver:self];
		
		[observableKeyPath release];
		observableKeyPath = nil;
	}
	else
		[super unbind:binding];
}


- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object
{
	[self observeValueForKeyPath:observableKeyPath ofObject:observableController change:0 context:nil];
	
	
}

- (void) observeValueForKeyPath:(NSString *)aPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (observableController == object) {
		
		id value = [observableController valueForKeyPath:observableKeyPath];
		
		if (value == NSNoSelectionMarker || value == NSNotApplicableMarker) {
			
			boolValue = -1;
			
		} else if (value == NSMultipleValuesMarker) {
			
			// TODO: multiple values marker
			boolValue = -1;
			
		} else {
			
			boolValue = [value boolValue] ? 1 : 0;
			
		}
		[self setNeedsDisplay:YES];
		
	}
	else {
		[super observeValueForKeyPath:aPath ofObject:object change:change context:context];
	}
}

- (BOOL)mouseDownCanMoveWindow
{
	return NO;
}

- (BOOL)acceptsFirstResponder
{
	return NO;
}

- (BOOL)becomeFirstResponder
{
	return NO;
}

- (void)drawRect:(NSRect)rect 
{
	
	NSImage *image;
	
	switch (boolValue) {
			
		case -1:
			image = multValImage;
			break;
		case 0:
		default:
			image = noImage;
			break;
		case 1:
			image = yesImage;
			break;
			
	}
	
    NSPoint currentPoint = NSMakePoint(MGMDSideMargin + 2.5, 2);
	//CGFloat width = [[[image representations] objectAtIndex:0] pixelsWide];
	//CGFloat height = [[[image representations] objectAtIndex:0] pixelsHigh];
	
	
	CGFloat alpha = [self isEnabled] ? 1.0 : 0.3;
	
	[image compositeToPoint:currentPoint operation:NSCompositeSourceOver fraction:alpha];
	
	//CGFloat radius = 1.5;
		
	
}


- (NSString *)contentDescription
{
	return boolValue > 0 ? @"YES" : @"NO";
	
}

- (NSString *)contentValue
{
	return boolValue > 0 ? @"YES" : @"NO";
	
}

@end
