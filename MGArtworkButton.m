//
//  MGArtworkButton.m
//  Filament
//
//  Created by Dennis Lorson on 24/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGArtworkButton.h"


@implementation MGArtworkButton

- (void)highlight:(BOOL)flag
{
	// do nothing
}
- (BOOL)allowsMixedState
{
	
	return NO;
}

- (void)awakeFromNib
{
	
	[[self cell] setHighlightsBy:NSChangeBackgroundCellMask];
}


@end
