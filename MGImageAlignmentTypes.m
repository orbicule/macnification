/*
 *  ImageAlignmentTypes.c
 *  ImageAlignment
 *
 *  Created by Dennis Lorson on 05/11/09.
 *  Copyright 2009 Orbicule. All rights reserved.
 *
 */

#import "MGImageAlignmentTypes.h"

void IFLaplacianRelease(IFLaplacian *lpt)
{
	for (int i = 0; i < lpt->nLevels + 1; i++) {
		IFImageRelease(lpt->images[i]);
	}
	free(lpt);
}

void IFImageRelease(IFFusionImage *image)
{
	if (!image) return;

	for (int j = 0; j < 4; j++)
		free(image->data[j]);
	free(image);	
}

void IAImageRelease(IAImage *image)
{
	if (!image) return;
	
	free(image->data);
	free(image);	
}

IAImage *IAImageCopy(IAImage *original)
{
	IAImage *copy = malloc(sizeof(IAImage));
	
	copy->h = original->h;
	copy->w = original->w;
	copy->data = malloc(4 * copy->w * copy->h);
	
	memcpy(copy->data, original->data, 4 * copy->w * copy->h);
	
	return copy;
}