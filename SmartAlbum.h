//
//  SmartAlbum.h
//  Macnification
//
//  Created by Peter Schols on 22/11/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Album.h"


@interface SmartAlbum : Album 
{
	
	
}

- (NSData *)predicateDataForLastMonthGroup;
- (NSSet *)dynamicImages;



@end
