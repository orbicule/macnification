//
//  MGEFIFilterChain.m
//  StackTable
//
//  Created by Dennis Lorson on 10/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "MGEFIFilterChain.h"


@implementation MGEFIFilterChain


+ (NSDictionary *)applyOnFirst:(NSDictionary *)firstSource second:(NSDictionary *)secondSource
{
	// applies one offset filter (if applicable) to the inputs and combines them using the EFI filter.
	// the source dicts should have the image for key "image", and the offset as a point NSValue for key "offset"
	// the offset must be absolute, (meaning the offset between the two image origins), should be adjusted by the StackTableView.
	
	// returns the combined offset and the result image in a new dict.
	
	// important : to load nonstandard image units, call +[CIPlugIn loadAllPlugIns] somewhere!
	
	NSPoint firstOffset = [[firstSource valueForKey:@"offset"] pointValue];
	NSPoint secondOffset = [[secondSource valueForKey:@"offset"] pointValue];
	
	//NSLog(@"%@   ---   %@",NSStringFromPoint(firstOffset),NSStringFromPoint(secondOffset));
	
	CIImage *firstImage = (CIImage *)[firstSource valueForKey:@"image"];
	CIImage *secondImage = (CIImage *)[secondSource valueForKey:@"image"];
	
	// the vector difference between offset 1 and 2 is the affine transform offset for image 2.
	
	NSAffineTransform *transform = [NSAffineTransform transform];
	[transform translateXBy:(firstOffset.x - secondOffset.x) yBy:(firstOffset.y - secondOffset.y)];
	
	CIFilter *offsetFilter = [CIFilter filterWithName:@"CIAffineTransform"];
	[offsetFilter setValue:transform forKey:@"inputTransform"];
	[offsetFilter setValue:firstImage forKey:@"inputImage"];
	
	CIImage *firstImageWithOffset = [offsetFilter valueForKey:@"outputImage"];
	
	
	
	// debug
	
	CIFilter *compFilter = [CIFilter filterWithName:@"CIDifferenceBlendMode"];
	[compFilter setValue:firstImageWithOffset forKey:@"inputBackgroundImage"];
	[compFilter setValue:secondImage forKey:@"inputImage"];
	
	CIFilter *efiFilter = [CIFilter filterWithName:@"EFIUnitFilter"];
	[efiFilter setValue:firstImageWithOffset forKey:@"inputImage1"];
	[efiFilter setValue:secondImage forKey:@"inputImage2"];
	
	// since we applied the offset to perform a translation to image 2 coord system, we use the image 2 offset as the final offset for this filter application.
	
	return [NSDictionary dictionaryWithObjectsAndKeys:[efiFilter valueForKey:@"outputImage"], @"image", [NSValue valueWithPoint:secondOffset], @"offset", nil];	
}

+ (CIImage *)applyOnImages:(NSArray *)images
{
	// wrapper for the recursive function
	return [[[[self class] rec_applyOnImages:images] objectAtIndex:0] valueForKey:@"image"];
}


+ (NSArray *)rec_applyOnImages:(NSArray *)images
{
	// the recursive function
	// pick elements from the array by two, and filter them into one new image.
	// then recurse on the array formed by the new CIImages.
	
	NSMutableArray *stepResults = [NSMutableArray array];
	
	// if we've got only one element left, return it.
	if ([images count] == 1) return images;
	
	NSInteger currentOp = 0;
	BOOL evenNbElements = ((CGFloat)[images count] / 2.0 == floorf((CGFloat)[images count] / 2.0));
	NSInteger operationCount = floorf((CGFloat)[images count] / 2.0);
	
	//NSLog(@"operating on array of %i elements, in %i operations", [images count], operationCount);
	
	while (currentOp != operationCount) {
		
		NSInteger firstIndex = currentOp * 2;
		NSInteger secondIndex = currentOp * 2 + 1;
		
		NSDictionary *result = [self applyOnFirst:(NSDictionary *)[images objectAtIndex:firstIndex] second:(NSDictionary *)[images objectAtIndex:secondIndex]];
		
		[stepResults addObject:result];
		
		currentOp++;
		
		
	}
	
	// if we have an odd nb of objects, add the last one as-is.
	if (!evenNbElements) [stepResults addObject:[images lastObject]];
	
	return [[self class] rec_applyOnImages:stepResults];
	
}



@end
