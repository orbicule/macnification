//
//  MGMenuButton.h
//  MetaDataView
//
//  Created by Dennis on 28/11/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGMenuButton : NSButton {
	
	
	IBOutlet NSMenu *buttonMenu;

}

@end
