//
//  MGByteAccessTools.h
//  Filament
//
//  Created by Dennis Lorson on 11/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>





const static int MG_UNSIGNED = 1;
const static int MG_SIGNED = 2;
const static int MG_FLOATING_POINT = 3;

const static int MG_SHORT = 3;
const static int MG_LONG = 4;

char MGTIFFReadByte(FILE *file);


int MGTIFFReadInt(FILE *file, BOOL littleEndian);

short MGTIFFReadShort(FILE *file, BOOL littleEndian);

long long MGTIFFReadLongLong(FILE *file, BOOL littleEndian);

double MGTIFFReadDouble(FILE *file, BOOL littleEndian);
int MGTIFFReadValue(FILE *file, BOOL littleEndian, int fieldType, int count);
float MGTIFFReadFloat(FILE *file, BOOL littleEndian);


@interface MGByteAccessTools : NSObject {

}

@end
