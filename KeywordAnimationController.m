//
//  KeywordAnimationController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 4 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import "KeywordAnimationController.h"
#import "MGAppDelegate.h"
#import "Image.h"


@implementation KeywordAnimationController



- (id)init {
    self = [super initWithWindowNibName:@"KeywordAnimation"];
    return self;
}


- (void)startAnimation:(NSString *)animation withSymbol:(NSString *)symbol;
{
	[[self window] makeKeyAndOrderFront:self];
	[keywordAnimationView startAnimation:animation withSymbol:symbol];	
}


- (void)setRectsArray:(NSArray *)array browserFrame:(NSRect)browserFrame;
{
	[keywordAnimationView setRectsArray:array];	
	//NSLog(@"rectsArray: %@", array);
	[keywordAnimationView setBrowserFrame:browserFrame];
}



@end
