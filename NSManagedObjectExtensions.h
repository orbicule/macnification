//
//  NSManagedObjectExtensions.h
//  Filament
//
//  Created by Peter Schols on 24/05/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSManagedObject (NSManagedObjectExtensions) 


- (NSManagedObject *)clone;



@end


