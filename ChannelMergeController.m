//
//  ChannelMergeController.m
//  Filament
//
//  Created by Dennis Lorson on 23/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ChannelMergeController.h"
#import "ChannelMergedImage.h"
#import "Image.h"
#import "ChannelMergeBinView.h"
#import "NSBitmapImageRep_Conversion.h"
#import "Image.h"
#import "MGAppDelegate.h"
#import "NSManagedObjectExtensions.h"
#import "StringExtensions.h"
#import "SourceListTreeController.h"
#import "Stack.h"
#import "MGLibraryController.h"

#define MERGE_IMAGE_TYPE @"merge_image_type"


@interface ChannelMergeController ()

- (Image *)mergeAndAddSingleImageFromChannelsR:(Image *)r G:(Image *)g B:(Image *)b;


@end



@implementation ChannelMergeController

@synthesize destinationFolder;




- (void)awakeFromNib
{
	rBin.channel = MergeChannelRed;
	gBin.channel = MergeChannelGreen;
	bBin.channel = MergeChannelBlue;
	cBin.channel = MergeChannelComposite;
}

- (void)dealloc 
{
    [destinationFolder release];
    
    
    [super dealloc];
}


- (void)openMergeSheet:(id)sender
{
	if (![self window])
		[NSBundle loadNibNamed:@"MergeChannels" owner:self];

	
	[rBin setImages:[NSArray array]];
	[gBin setImages:[NSArray array]];
	[bBin setImages:[NSArray array]];
	
	[NSApp beginSheet:[self window] modalForWindow:[NSApp mainWindow] modalDelegate:self didEndSelector:nil contextInfo:nil];

	[self distributeImagesInBins];
}


- (void)closeMergeSheet:(id)sender
{
	if (sender == mergeButton) {
		
		[self addMergedImagesToLibrary];
		
	}
	
	[NSApp endSheet:[self window]];
	
	[[self window] orderOut:self];
		
}

- (BOOL)distributeImagesInBins
{
	NSArray *images = [[MGLibraryControllerInstance imageArrayController] selectedObjects];
	
	// if the selection is simple, just add the single images to the bins
	
	if ([images count] > 0 && [images count] < 4) {
		
		int i;
		for (i = 0; i < [images count]; i++) {
			
			if (i == 0)
				[rBin setImages:[NSArray arrayWithObject:[images objectAtIndex:i]]];
			
			if (i == 1)
				[gBin setImages:[NSArray arrayWithObject:[images objectAtIndex:i]]];
			
			if (i == 2)
				[bBin setImages:[NSArray arrayWithObject:[images objectAtIndex:i]]];
			
		}
		
		[self updateCompositeImage];
		
		return YES;
		
	} 
	
	return NO;

}


- (void)updateCompositeImage
{
	[cBin removeAllImageLayers];
	
	int maxNbImages = MIN(5, MAX(MAX([[rBin images] count], [[gBin images] count]), [[bBin images] count]));
	
	int i;
	for (i = 0; i < maxNbImages; i++) {
		
		Image *r = (i < [[rBin images] count]) ? [[rBin images] objectAtIndex:i] : nil;
		Image *g = (i < [[gBin images] count]) ? [[gBin images] objectAtIndex:i] : nil;
		Image *b = (i < [[bBin images] count]) ? [[bBin images] objectAtIndex:i] : nil;
		
		ChannelMergedImage *merged = [[[ChannelMergedImage alloc] init] autorelease];
		
		[merged setImageR:r G:g B:b];
		
		NSBitmapImageRep *rep = [merged mergedRep];
		
		[cBin addImageLayerForCompositeImage:rep];
	}
	

}


- (void)addMergedImagesToLibrary
{
	// let's disconnect the IAC so we get no updates
	[[MGLibraryControllerInstance sourcelistTreeController] disconnectImageArrayController];
		
	NSDate *newDate = [NSDate date];
	
	// generate the rep
	Image *r = [[rBin images] count] ? [[rBin images] objectAtIndex:0] : nil;
	Image *g = [[gBin images] count] ? [[gBin images] objectAtIndex:0] : nil;
	Image *b = [[bBin images] count] ? [[bBin images] objectAtIndex:0] : nil;
	
	Image *firstValidImage = r ? r : (g ? g : b);
	
	
	// three stacks
	if ([firstValidImage isKindOfClass:[Stack class]]) {
		
		Stack *originalStack = (Stack *)firstValidImage;
		
		NSMutableArray *images = [NSMutableArray array];
		
		
		// be sure to sort the images from the three stacks in the same order
		NSArray *sortDescs = [NSArray arrayWithObjects:
							  [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],
							  [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES],
							  nil];
		
		NSArray *r_set = [[[r valueForKeyPath:@"stackImages.image"] allObjects] sortedArrayUsingDescriptors:sortDescs];
		NSArray *g_set = [[[g valueForKeyPath:@"stackImages.image"] allObjects] sortedArrayUsingDescriptors:sortDescs];
		NSArray *b_set = [[[b valueForKeyPath:@"stackImages.image"] allObjects] sortedArrayUsingDescriptors:sortDescs];
		
		NSInteger maxSetCount = MAX(MAX([r_set count], [g_set count]), [b_set count]);
		
		for (int i = 0; i < maxSetCount; i++) {
			
			Image *r_i = (i < [r_set count]) ? [r_set objectAtIndex:i] : nil;
			Image *g_i = (i < [g_set count]) ? [g_set objectAtIndex:i] : nil;
			Image *b_i = (i < [b_set count]) ? [b_set objectAtIndex:i] : nil;
			
			NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
			
			Image *merged = [[self mergeAndAddSingleImageFromChannelsR:r_i G:g_i B:b_i] retain];
			
			[pool drain];
			
			[merged setValue:newDate forKey:@"creationDate"];
			[merged setValue:newDate forKey:@"importDate"];
			
			[images addObject:merged];
			
			[merged release];
		}
		
		
		// create a new stack
		Stack *duplicate = (Stack *)[originalStack clone];
		[duplicate setValue:[[images objectAtIndex:0] valueForKey:@"name"] forKey:@"name"];
		
		[duplicate addImages:images];	
		
		NSMutableSet *albums = [duplicate mutableSetValueForKey:@"albums"];
		[albums addObject:[[MGLibraryControllerInstance library] libraryGroup]];
		
        
        if (self.destinationFolder)
            [albums addObject:self.destinationFolder];
		
		
	}
	
	
	// three single images
	else {
		
		Image *merged = [self mergeAndAddSingleImageFromChannelsR:r G:g B:b];
		
		id libraryCollection = [[MGLibraryControllerInstance library] libraryGroup];
        
        NSMutableSet *albums = [merged mutableSetValueForKey:@"albums"];
        [albums addObject:libraryCollection];

        if (self.destinationFolder)
            [albums addObject:self.destinationFolder];
		
		[merged setValue:newDate forKey:@"creationDate"];
		[merged setValue:newDate forKey:@"importDate"];
		
	}
		
	
	// reconnect the IAC
	[[MGLibraryControllerInstance sourcelistTreeController] connectImageArrayController];
	
}


- (Image *)mergeAndAddSingleImageFromChannelsR:(Image *)r G:(Image *)g B:(Image *)b
{
	ChannelMergedImage *merged = [[[ChannelMergedImage alloc] init] autorelease];
	
	[merged setImageR:r G:g B:b];
	
	NSBitmapImageRep *rep = [merged mergedRep];
	
	// pick an image to derive from (the first non-nil one)
	Image *originalImage = r ? r : (g ? g : b);
	
	// clone the image, and save the rep as the file for this image.
	Image *compositedImage = (Image *)[originalImage clone];
	
	// derive the new name
	NSString *compositeName;
	NSScanner *scanner = [NSScanner scannerWithString:[originalImage valueForKey:@"name"]];
	[scanner scanUpToString:@"_ch" intoString:&compositeName];
	compositeName = [compositeName stringByAppendingString:@"_merged"];
    
    [compositedImage setValue:compositeName forKey:@"name"];

    [compositedImage applyOriginalImagePathWithFileName:compositeName extension:[[originalImage originalImagePathExpanded:NO] pathExtension]];
        
    NSString *dstPath = [compositedImage originalImagePathExpanded:YES];
		
	// copy the new image
	NSData *imgData = [rep TIFFRepresentation];
	[imgData writeToFile:dstPath atomically:YES];
    
    [compositedImage updateThumbnail];
	
	// Set the calibration relationships to be identical
	[compositedImage setValue:[originalImage valueForKey:@"calibration"] forKey:@"calibration"];
	[compositedImage setValue:[originalImage valueForKey:@"instrument"] forKey:@"instrument"];
	[compositedImage setValue:[originalImage valueForKey:@"experiment"] forKey:@"experiment"];

    
	return compositedImage;
}



@end
