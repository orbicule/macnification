//
//  CaptureDeviceBrowser.m
//  Filament
//
//  Created by Dennis Lorson on 02/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureDeviceBrowser.h"
#import "CaptureDevice.h"
#import "CaptureDC1394Device.h"
#import "CaptureQTDevice.h"

static CaptureDeviceBrowser *sharedBrowser = nil;


@interface CaptureDeviceBrowser ()

- (BOOL)scheduleDeviceForAddition:(CaptureDevice *)device;
- (BOOL)scheduleQTDeviceForAddition:(CaptureQTDevice *)device;
- (BOOL)scheduleDCDeviceForAddition:(CaptureDC1394Device *)device;


- (CaptureDevice *)deviceWithID:(NSNumber *)guid;
- (CaptureDevice *)deviceWithUniqueID:(NSString *)guid;

- (void)_updateDeviceIndexSuffixes;
- (void)_sortDevices;


@end


@implementation CaptureDeviceBrowser

@synthesize delegate = delegate_;


+ (CaptureDeviceBrowser *)sharedBrowser;
{
	if (!sharedBrowser)
		sharedBrowser = [[CaptureDeviceBrowser alloc] init];
	
	return sharedBrowser;
}


- (id)init
{
	self = [super init];
	if (self != nil) {
		
		devices_ = [[NSMutableArray array] retain];
		modules_ = [[NSMutableArray array] retain];
		duplicateDevices_ = [[NSMutableArray array] retain];
		delayedDevices_ = [[NSMutableArray array] retain];
		
		[modules_ addObject:[CaptureDeviceBrowserModuleQT sharedBrowserModule]];
		[modules_ addObject:[CaptureDeviceBrowserModuleIC sharedBrowserModule]];
		//[modules_ addObject:[CaptureDeviceBrowserModulePR sharedBrowserModule]];
		[modules_ addObject:[CaptureDeviceBrowserModuleDC1394 sharedBrowserModule]];
		
	}
	return self;
}

- (void) dealloc
{
	[devices_ release];
	[modules_ release];
	[duplicateDevices_ release];
	[delayedDevices_ release];
	
	[super dealloc];
}

- (void)stopAllSessions
{
	for (CaptureDevice *device in devices_)
		[device stopSession];
}


#pragma mark -
#pragma mark Accessors

- (NSArray *)devices
{
	return [[devices_ copy] autorelease];
}


#pragma mark -
#pragma mark Searching

- (void)stopSearchingForDevices;
{
	for (CaptureDeviceBrowserModule *module in modules_)
		[module stopSearchingForDevices];
}


- (void)startSearchingForDevices;
{ 
	for (CaptureDeviceBrowserModule *module in modules_) {
		module.delegate = self;
		[module startSearchingForDevices];
	}
}


#pragma mark -
#pragma mark Device update

- (void)_updateDeviceIndexSuffixes
{
    [self _sortDevices];
    
    NSMutableDictionary *totals = [NSMutableDictionary dictionary];
    
    for (CaptureDevice *device in devices_) {
        if (![device name])
            continue;
        
        int currentCountForDeviceName = [[totals objectForKey:[device name]] intValue];
        currentCountForDeviceName++;
        
        [totals setObject:[NSNumber numberWithInt:currentCountForDeviceName] forKey:[device name]];
    }
    
    NSMutableDictionary *currentDeviceIndices = [NSMutableDictionary dictionary];
    
    for (CaptureDevice *device in devices_) {
        if (![device name])
            continue;
        
        int totalForDeviceName = [[totals objectForKey:[device name]] intValue];
        if (totalForDeviceName < 2)
            continue;
        
        int currentDeviceIndex = [[currentDeviceIndices objectForKey:[device name]] intValue];
        
        [device setIndexSuffix:currentDeviceIndex];
        
        currentDeviceIndex++;
        [currentDeviceIndices setObject:[NSNumber numberWithInt:currentDeviceIndex] forKey:[device name]];

    }
    
}

- (void)_sortDevices
{
    [devices_ sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uidString" ascending:YES]]];
}


- (void)captureDeviceBrowserModule:(CaptureDeviceBrowserModule *)module didAddDevice:(CaptureDevice *)device;
{
	if ([self scheduleDeviceForAddition:device]) {
		
		[devices_ addObject:device];
        
        [self _updateDeviceIndexSuffixes];
		
		if ([self.delegate respondsToSelector:@selector(captureDeviceBrowser:didAddDevice:)])
			[self.delegate captureDeviceBrowser:self didAddDevice:device];
	}
	
	else {
		
		[duplicateDevices_ addObject:device];
	}

}

- (void)captureDeviceBrowserModule:(CaptureDeviceBrowserModule *)module didRemoveDevice:(CaptureDevice *)device;
{
	[devices_ removeObject:device];
    
    [self _updateDeviceIndexSuffixes];

	if ([self.delegate respondsToSelector:@selector(captureDeviceBrowser:didRemoveDevice:)])
		[self.delegate captureDeviceBrowser:self didRemoveDevice:device];	
}

#pragma mark -
#pragma mark Device mediator

- (BOOL)scheduleDeviceForAddition:(CaptureDevice *)device
{
	if ([device isKindOfClass:[CaptureDC1394Device class]])
		return [self scheduleDCDeviceForAddition:(CaptureDC1394Device *)device];
	
	if ([device isKindOfClass:[CaptureQTDevice class]])
		return [self scheduleQTDeviceForAddition:(CaptureQTDevice *)device];
	
	return YES;
}

- (BOOL)scheduleQTDeviceForAddition:(CaptureQTDevice *)device
{
	// if the device is already in the list of devices (DC module), skip this device outright: keep it in a separate array.
	// if not, let the device wait for a few seconds to prevent an unstable UI...
	//if (duplicate = [self deviceWithID:[NSNumber numberWithUnsignedLongLong:device.guid]]) {
	
	if ([self deviceWithUniqueID:[device uniqueID]])
		return NO;
	
	[delayedDevices_ addObject:device];
	
	[self performSelector:@selector(delayedAddDevice:) withObject:device afterDelay:3.0];
	
	return NO;
}

- (BOOL)scheduleDCDeviceForAddition:(CaptureDC1394Device *)device
{
	// if a QT device with the same ID is already in the devices list, remove that one first!!
	CaptureDevice *duplicate;
	
	if ((duplicate = [self deviceWithID:[NSNumber numberWithUnsignedLongLong:device.guid]])) {
		NSLog(@"!!! Duplicate entry: %@ and %@", [device name], [duplicate name]);
		// simulate a removal of the earlier device
		[self captureDeviceBrowserModule:nil didRemoveDevice:duplicate];
	}
	return YES;

}

- (void)delayedAddDevice:(CaptureDevice *)device
{
	if (![delayedDevices_ containsObject:device])
		return;
	
	[delayedDevices_ removeObject:device];
	
	if ([self deviceWithUniqueID:[(CaptureQTDevice *)device uniqueID]])
		return;
	
	// finally, add it
	[devices_ addObject:device];
    
    [self _updateDeviceIndexSuffixes];
	
	if ([self.delegate respondsToSelector:@selector(captureDeviceBrowser:didAddDevice:)])
		[self.delegate captureDeviceBrowser:self didAddDevice:device];
	
}

- (CaptureDevice *)deviceWithUniqueID:(NSString *)guid
{	
	for (CaptureDevice *dev in devices_) {
		
		if ([dev isKindOfClass:[CaptureQTDevice class]]) {
			NSString *uid = [(CaptureQTDevice *)dev uniqueID];
			//NSLog(@"comparing %@ -- %@", uid, guid);
			if ([guid rangeOfString:uid options:NSCaseInsensitiveSearch].location != NSNotFound)
				return dev;
		}
		
		if ([dev isKindOfClass:[CaptureDC1394Device class]]) {
			uint64_t uid = ((CaptureDC1394Device *)dev).guid;
			NSString *uidString = [NSString stringWithFormat:@"%llx", uid];
			if ([guid rangeOfString:uidString options:NSCaseInsensitiveSearch].location != NSNotFound)
				return dev;
		}
	}
	return nil;
}
	
- (CaptureDevice *)deviceWithID:(NSNumber *)guid
{
	uint64_t value = [guid unsignedLongLongValue];
	
	for (CaptureDevice *dev in devices_) {
		
		if ([dev isKindOfClass:[CaptureQTDevice class]]) {
			NSString *uid = [(CaptureQTDevice *)dev uniqueID];
			NSString *guidString = [NSString stringWithFormat:@"%llx", value];
			//NSLog(@"comparing %@ -- %@", uid, guidString);
			if ([uid rangeOfString:guidString options:NSCaseInsensitiveSearch].location != NSNotFound)
				return dev;
		}
		
		if ([dev isKindOfClass:[CaptureDC1394Device class]]) {
			uint64_t uid = ((CaptureDC1394Device *)dev).guid;
			if (value == uid)
				return dev;
		}
		
	}
	
	return nil;
}

@end
