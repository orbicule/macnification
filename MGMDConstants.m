//
//  MGMDConstants.m
//  MetaData
//
//  Created by Dennis Lorson on 27/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDConstants.h"

NSString *MGMDNoSelectionString = @"No Selection";
NSString *MGMDNullValueString = @"None Specified";
NSString *MGMDMultipleValuesString = @"Multiple Values";
NSString *MGMDNotApplicableString = @"Not Applicable";


@implementation MGMDConstants

@end
