//
//  MGMDExperimentView.m
//  Filament
//
//  Created by Dennis Lorson on 02/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDExperimentView.h"
#import "MGMDTextField.h"
#import "MGMDFieldView.h"
#import "MGAppDelegate.h"
#import "ImageArrayController.h"
#import "MGLibraryController.h"

@implementation MGMDExperimentView


- (id)init
{
	if ((self = [super init])) {
		
		
	}
	return self;
}


- (BOOL)acceptsFirstResponder
{
	
	return [experimentPopup acceptsFirstResponder];
}

- (BOOL)becomeFirstResponder
{
	return [experimentPopup becomeFirstResponder];
}


- (void) dealloc
{
	[self unbind:@"experimentsController"];
	
	[super dealloc];
}



- (void)setEnabled:(BOOL)flag
{		
	[experimentPopup setEnabled:flag];
}



- (void)bind:(NSString *)binding toObject:(id)ctl withKeyPath:(NSString *)aPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		observableController = ctl;
		observableKeyPath = [aPath retain];
		//[observableController addObserver:self forKeyPath:keyPath options:0 context:nil];
		[(ImageArrayController *)observableController addManualObserver:self forKeyPath:observableKeyPath];
		
		[(ImageArrayController *)observableController addObserver:self forKeyPath:@"selection" options:0 context:nil];
		
		// initialize the popup for a first time..apparently this isn't invoked automatically
		//[observableController performSelector:@selector(didChangeValueForKey:) withObject:keyPath afterDelay:0.3];
		
	} else if ([binding isEqualToString:@"experimentsController"]) {
			
		[experimentsController bind:@"managedObjectContext" toObject:ctl withKeyPath:aPath options:options];
		[experimentsController addObserver:self forKeyPath:@"arrangedObjects" options:0 context:nil];

		[experimentsController addObserver:self forKeyPath:@"selection.summary" options:0 context:nil];
		[experimentsController addObserver:self forKeyPath:@"arrangedObjects.experimentName" options:0 context:nil];

		
	} else {
		
		[super bind:binding toObject:observableController withKeyPath:observableKeyPath options:options];
		
	}
	
	
}

- (void)manualObserveValueForKeyPath:(NSString *)aPath ofObject:(id)object;
{
	// the experiments value of the selection has changed.
	// just forward this to our real observeValue method.
	
	[self observeValueForKeyPath:aPath ofObject:object change:0 context:nil];
	
	
}


- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		[observableController removeObserver:self forKeyPath:observableKeyPath];
		[observableKeyPath release];
		observableKeyPath = nil;
		
		
	} else if ([binding isEqualToString:@"experimentsController"]) {
		
		[experimentsController unbind:@"managedObjectContext"];
		
		[experimentsController removeObserver:self forKeyPath:@"arrangedObjects"];

		[experimentsController removeObserver:self forKeyPath:@"selection.summary"];

		
		
	} else {
		
		[super unbind:binding];
	}
}


- (NSMenu *)menuWithAvailableExperiments:(NSArray *)experiments addMultipleStates:(BOOL)flag
{
	NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11], NSFontAttributeName, nil];

	NSMenu *menu = [[[NSMenu alloc] init] autorelease];
	[menu setAutoenablesItems:NO];

	if (flag) {
		
		
		// the "multiple values" experiment
		
		NSString *multName = @"Multiple Values";
		NSAttributedString *attrMultName = [[[NSAttributedString alloc] initWithString:multName attributes:attrs] autorelease];
		
		NSMenuItem *multItem = [[[NSMenuItem alloc] init] autorelease];
		[multItem setAttributedTitle:attrMultName];
		
		[multItem setRepresentedObject:NSMultipleValuesMarker];
		
		[multItem setTarget:self];
		[multItem setAction:@selector(changeExperiment:)];
		[menu addItem:multItem];
		
		
		// a separator
		[menu addItem:[NSMenuItem separatorItem]];

		
	}

	NSString *descName = @"Assignable Experiments";
	NSAttributedString *attrDescName = [[[NSAttributedString alloc] initWithString:descName attributes:attrs] autorelease];
	
	NSMenuItem *descItem = [[[NSMenuItem alloc] init] autorelease];
	[descItem setAttributedTitle:attrDescName];
	
	[descItem setEnabled:NO];
	[menu addItem:descItem];
	
	
	// the "null" experiment
	
	NSString *nullName = @"None";
	NSAttributedString *attrNullName = [[[NSAttributedString alloc] initWithString:nullName attributes:attrs] autorelease];
	
	NSMenuItem *nullItem = [[[NSMenuItem alloc] init] autorelease];
	[nullItem setAttributedTitle:attrNullName];
	
	[nullItem setRepresentedObject:[NSNull null]];
	
	[nullItem setTarget:self];
	[nullItem setAction:@selector(changeExperiment:)];
	[menu addItem:nullItem];
	
	
	// the available experiments
		
	for (id experiment in experiments) {
		
		NSString *name = [experiment valueForKey:@"experimentName"];
		if (!name) name = @"untitled experiment";
		NSAttributedString *attrName = [[[NSAttributedString alloc] initWithString:name attributes:attrs] autorelease];
		
		NSMenuItem *expItem = [[[NSMenuItem alloc] init] autorelease];
		[expItem setAttributedTitle:attrName];
		
		[expItem setRepresentedObject:experiment];
		
		[expItem setTarget:self];
		[expItem setAction:@selector(changeExperiment:)];
		
		[menu addItem:expItem];
		
	}
	
	// separator
		
	[menu addItem:[NSMenuItem separatorItem]];
	
	// the "manage" item
	
	NSString *manageName = @"Manage Experiments...";
	NSAttributedString *attrManageName = [[[NSAttributedString alloc] initWithString:manageName attributes:attrs] autorelease];
	
	NSMenuItem *manageItem = [[[NSMenuItem alloc] init] autorelease];
	[manageItem setAttributedTitle:attrManageName];
	[manageItem setTarget:self];
	[manageItem setAction:@selector(manageExperiments:)];
	[menu addItem:manageItem];
	
	
	return menu;
}


- (void)changeExperiment:(id)sender
{
	// invoked by the popup
	id experiment = [sender representedObject];
	
	// if "no experiment assigned" is selected
	if (experiment == [NSNull null]) experiment = nil;
	
	// do nothing if "multiple values" is selected.
	if (experiment == NSMultipleValuesMarker) return;
	
	//NSLog(@"%@ setexpt: %@ forKP %@", observableController, experiment, keyPath);
	[observableController setValue:experiment forKeyPath:observableKeyPath];
	if (experiment)
		[experimentsController setSelectedObjects:[NSArray arrayWithObject:experiment]];
	
}

- (void)manageExperiments:(id)sender
{
	[MGLibraryControllerInstance showExperimentsWindow:self];
	
	// restore the selection (manage experiments cannot be a selected item)
	[self observeValueForKeyPath:observableKeyPath ofObject:observableController change:nil context:nil];
	
}


- (void)selectExperimentInPopup:(id)experiment
{
	id representedObject = experiment ? experiment : [NSNull null];
	
	NSInteger indexToSelect = [[experimentPopup menu] indexOfItemWithRepresentedObject:representedObject];
	
	[experimentPopup selectItemAtIndex:indexToSelect];
	
	if (experiment)
		[experimentsController setSelectedObjects:[NSArray arrayWithObject:representedObject]];
	else
		[experimentsController setSelectedObjects:[NSArray array]];

}

- (void)observeValueForKeyPath:(NSString *)aPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	// this observe is triggered for both selection change and content change within the selection.
    if (object == observableController) {
		
		// the object is our parent MGMDFieldView.
		
		id value = [observableController valueForKeyPath:observableKeyPath];
		
		if (value == NSNoSelectionMarker) {
			
			[self selectExperimentInPopup:nil];

			[experimentPopup setEnabled:NO];
			
		} else {

			[(MGMDFieldView *)[self superview] updateContentsSize];
			
			// the experiment assigned to the current image changed
			// update the popup items (as "multiple values" may be added to it)
			// then select the image assigned experiment
		
			[experimentPopup setEnabled:YES];
			
			id value = [observableController valueForKeyPath:observableKeyPath];
			NSMenu *popupMenu = [self menuWithAvailableExperiments:[experimentsController arrangedObjects] addMultipleStates:(value == NSMultipleValuesMarker)];
			
			[experimentPopup setMenu:popupMenu];
			
			[self selectExperimentInPopup:value];
			
		}
		
	} else if ([aPath isEqualToString:@"arrangedObjects.experimentName"]) {
		id value = [observableController valueForKeyPath:observableKeyPath];
		NSMenu *popupMenu = [self menuWithAvailableExperiments:[experimentsController arrangedObjects] addMultipleStates:(value == NSMultipleValuesMarker)];
		
		[experimentPopup setMenu:popupMenu];
		
		[self selectExperimentInPopup:value];
		
		
			
	} else if (object == experimentsController) {
		
		if ([aPath isEqualToString:@"selection.summary"]) {
			
			// the content of the summary field may have changed
			// so relayout
			// do an update in 0.2s because the update might trigger the display of scroll bars, which in turn requires an update from our field.
			[(MGMDFieldView *)[self superview] updateContentsSize];
			[(MGMDFieldView *)[self superview] performSelector:@selector(updateContentsSize) withObject:nil afterDelay:0.2];
			
			
			
		}
		else if ([aPath isEqualToString:@"arrangedObjects"]) {
			
			
			// the experiments array changed
			// update the popup menu but retain the currently selected experiment
			id oldSelectedValue = [[experimentPopup selectedItem] representedObject];
			
			NSMenu *popupMenu = [self menuWithAvailableExperiments:[experimentsController arrangedObjects] addMultipleStates:([observableController valueForKeyPath:observableKeyPath] == NSMultipleValuesMarker)];
			
			[experimentPopup setMenu:popupMenu];
			
			[self selectExperimentInPopup:oldSelectedValue];
			
			
			
		}

			
		
	}
	else {
		[super observeValueForKeyPath:aPath ofObject:object change:change context:context];
	}
}


- (CGFloat)contentsHeight
{
	CGFloat oldEditorHeight = NSHeight([experimentSummaryField frame]);
	
	CGFloat editorHeight = [[(MGMDTextField *)experimentSummaryField cell] cellSizeForBounds: NSMakeRect(0.0, 0.0, NSWidth([experimentSummaryField frame]), 100000.0)].height;

	return NSHeight([self frame]) - oldEditorHeight + editorHeight;
	
	
}

- (NSString *)contentDescription
{
	if ([[experimentsController selectedObjects] count] == 0) return nil;
	
	return [[[experimentsController selectedObjects] objectAtIndex:0] valueForKey:@"experimentName"];

	
}


- (NSString *)contentValue
{
	return [self contentDescription];
	
	
}

@end
