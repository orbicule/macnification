//
//  LightTableToolView.m
//  LightTable
//
//  Created by Dennis on 18/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableToolView.h"
#import "LightTableAlignLayoutElement.h"
#import "NSImage_MGAdditions.h"
#import "LightTableView.h"

@interface LightTableToolView (Private)

- (NSImage *)cameraImage;


@end


@implementation LightTableToolView

@synthesize layoutGuides, selectionRectangle, printRectangleMode;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		layoutGuides = [[NSMutableArray array] retain];
		selectionRectangle = NSZeroRect;
    }
    return self;
}

- (void)dealloc
{
	[layoutGuides release];
	[cameraImage release];

	
	[super dealloc];
}


- (void)drawRect:(NSRect)rect 
{	
	CGFloat zoomLevel = ((LightTableView *)[self superview]).zoomLevel;

	//NSLog(@"zoomLevel == %f", zoomLevel);
	for (LightTableAlignLayoutElement *element in layoutGuides) {
		[element drawWithLineWidth:1.0 / zoomLevel inView:self];
	}
	
	if (!NSEqualRects(selectionRectangle, NSZeroRect)) {
		if (printRectangleMode) {
			
			CGFloat pattern[2] = {4 / zoomLevel, 4 / zoomLevel};
			
			NSBezierPath *selectionRectanglePath = [NSBezierPath bezierPathWithRect:selectionRectangle];
			
			[selectionRectanglePath setLineDash:pattern count:2 phase:0];
			[selectionRectanglePath setLineWidth:2.0/zoomLevel];
			
			//[selectionRectangle setLineWidth:0.01];
			[[NSColor colorWithCalibratedRed:187.0/255 green:213.0/255 blue:237.0/255 alpha:0.3] set];
			//[[NSColor colorWithCalibratedRed:0.9 green:0.6 blue:0.0 alpha:0.15] set];

			[selectionRectanglePath fill];
			[[NSColor colorWithCalibratedRed:187.0/255 green:213.0/255 blue:237.0/255 alpha:1.0] set];
			//[[NSColor colorWithCalibratedRed:0.8 green:0.4 blue:0.0 alpha:1.0] set];

			[[NSGraphicsContext currentContext] setShouldAntialias:NO];
			[selectionRectanglePath stroke];
			[[NSGraphicsContext currentContext] setShouldAntialias:YES];
			
			
		} else {
			
			// convert to a pixel-aligned rect
			NSRect selectionRectInBase = [self convertRectToBase:selectionRectangle];
			selectionRectInBase = NSIntegralRect(selectionRectInBase);
			selectionRectInBase.origin.x += 0.5;
			selectionRectInBase.origin.y += 0.5;
			
			NSRect selectionRectAligned = [self convertRectFromBase:selectionRectInBase];
			
			NSBezierPath *copy = [NSBezierPath bezierPathWithRect:selectionRectAligned];
			[[NSGraphicsContext currentContext] setShouldAntialias:NO];

			[copy setLineWidth:0.5/zoomLevel];
			[[NSColor colorWithCalibratedWhite:1.0 alpha:0.3] set];
			[copy fill];
			[[NSColor colorWithCalibratedWhite:1.0 alpha:1.0] set];
			[copy stroke];
			[[NSGraphicsContext currentContext] setShouldAntialias:YES];
			
		}
	}
	 
}

/*
- (void)setBounds:(NSRect)bounds
{
	
	[super setBounds:bounds];
	NSLog(@"setbounds: %@", NSStringFromRect(bounds));
}

- (void)setFrame:(NSRect)bounds
{
	
	[super setFrame:bounds];
	NSLog(@"setFrame: %@", NSStringFromRect(bounds));
}
*/

- (NSImage *)cameraImage
{
	if (!cameraImage) {
		cameraImage = [[NSImage imageNamed:@"camera"] retain];
	}
	
	return cameraImage;
}


@end
