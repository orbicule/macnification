#import <Foundation/Foundation.h>
#include <CoreFoundation/CoreFoundation.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/IOMessage.h>
#include <IOKit/IOCFPlugIn.h>
#include <IOKit/usb/IOUSBLib.h>

#import <QTKit/QTKit.h>


@interface UVCCameraControl : NSObject 
{
	long dataBuffer;
	IOUSBInterfaceInterface190 **interface;
    
    UInt32 locationID_;
    UInt16 product_, vendor_;
}

- (id)initWithDevice:(QTCaptureDevice *)device;
- (id)initWithLocationID:(UInt32)locationID;

- (BOOL)supportsManualExposure;

- (BOOL)setAutoExposure:(BOOL)enabled;
- (BOOL)getAutoExposure;

- (BOOL)setExposure:(int)value;
- (int)getExposure;

- (int)getExposureRelative;

- (int)getExposureMax;
- (int)getExposureMin;
- (int)getExposureDefault;
- (void)resetExposure;

@end
