//
//  NSAttributedString_MGExtensions.m
//  Filament
//
//  Created by Dennis Lorson on 07/07/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "NSAttributedString_MGExtensions.h"


@implementation NSAttributedString (MGExtensions)

- (NSAttributedString *)attributedStringByAddingOrReplacingAttributes:(NSDictionary *)attrs
{
    if (!attrs || [[attrs allKeys] count] == 0 || [self length] == 0)
        return [[self copy] autorelease];
    
    NSMutableDictionary *existingAttrs = [[[self attributesAtIndex:0 effectiveRange:NULL] mutableCopy] autorelease];
    
    [existingAttrs addEntriesFromDictionary:attrs];
    
    return [[[NSAttributedString alloc] initWithString:[self string] attributes:existingAttrs] autorelease];
}

@end
