//
//  PolygonROI.h
//  Macnification
//
//  Created by Peter Schols on 12/04/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ROI.h"
#import "AreaROI.h"


@interface PolygonROI : AreaROI {

	BOOL isCurrentlyBeingDrawn;
	NSPoint movedPoint;
}


- (void)setPolygonPoints:(NSArray *)pointsArray;


//- (void)calculateLength;
- (void)calculateArea;
- (void)calculatePerimeter;



- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;


- (BOOL)containsPoint:(NSPoint)point;
- (NSBezierPath *)pathWithScale:(float)scale;

- (float)pixelLengthBetweenPoint:(NSPoint)point1 andPoint:(NSPoint)point2;


// Accessors
- (BOOL)isCurrentlyBeingDrawn;
- (void)setIsCurrentlyBeingDrawn:(BOOL)value;
- (NSPoint)movedPoint;
- (void)setMovedPoint:(NSPoint)value;



@end
