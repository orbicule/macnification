//
//  main.m
//  rsa_verify
//
//  Created by Dennis Lorson on 16/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bn.h"


int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];


    //assert(argc == 4);
    
    NSString *msgPath = @"/Users/dennis/Desktop/rsa/dgst";//[NSString stringWithCString:argv[1] encoding:NSASCIIStringEncoding];
    NSString *keyPath = @"/Users/dennis/Desktop/rsa/pub.bin";//[NSString stringWithCString:argv[2] encoding:NSASCIIStringEncoding];
    NSString *signaturePath = @"/Users/dennis/Desktop/rsa/signature";//[NSString stringWithCString:argv[3] encoding:NSASCIIStringEncoding];
    
    NSData *msgData = [NSData dataWithContentsOfFile:msgPath];
    NSData *keyData = [NSData dataWithContentsOfFile:keyPath];
    NSData *signatureData = [NSData dataWithContentsOfFile:signaturePath];
    
    m_BN_CTX *c = m_BN_CTX_new();
    
    m_BN_CTX_init(c);
    
    M_BIGNUM msgBN;
    m_BN_init(&msgBN);
    m_BN_bin2bn([msgData bytes], (int)[msgData length], &msgBN);
    
    M_BIGNUM keyBN;
    m_BN_init(&keyBN);
    m_BN_bin2bn([keyData bytes], (int)[keyData length], &keyBN);
    
    M_BIGNUM signatureBN;
    m_BN_init(&signatureBN);
    m_BN_bin2bn([signatureData bytes], (int)[signatureData length], &signatureBN);

    unsigned char e[3];
    e[0] = 0x1;
    e[1] = 0x0;
    e[2] = 0x1;
    M_BIGNUM exp;
    m_BN_init(&exp);
    m_BN_bin2bn(e, 3, &exp);
    
    M_BIGNUM res;
    m_BN_init(&res);
    // args: 2 ^ 3 mod 4
    m_BN_mod_exp(&res, &signatureBN, &exp, &keyBN, c);
    
    m_BN_mask_bits(&res, 512);
    
    if(m_BN_cmp(&res, &msgBN) == 0)
        NSLog(@"Signature correct");
    else
        NSLog(@"Signature incorrect");
        
    

    [pool drain];
    return 0;
}


extern inline void m_BN_init(M_BIGNUM *a) 
{
	memset(a,0,sizeof(M_BIGNUM));
	m_BN_check_top(a);
}


