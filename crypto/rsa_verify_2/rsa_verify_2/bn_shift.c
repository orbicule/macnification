/* crypto/bn/m_BN_shift.c */
/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 * 
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 * 
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */

#include <stdio.h>

#include "BN_lcl.h"

int m_BN_lshift1(M_BIGNUM *r, const M_BIGNUM *a)
	{
	register m_BN_ULONG *ap,*rp,t,c;
	int i;

	m_BN_check_top(r);
	m_BN_check_top(a);

	if (r != a)
		{
		r->neg=a->neg;
		if (m_BN_wexpand(r,a->top+1) == NULL) return(0);
		r->top=a->top;
		}
	else
		{
		if (m_BN_wexpand(r,a->top+1) == NULL) return(0);
		}
	ap=a->d;
	rp=r->d;
	c=0;
	for (i=0; i<a->top; i++)
		{
		t= *(ap++);
		*(rp++)=((t<<1)|c)&m_BN_MASK2;
		c=(t & m_BN_TBIT)?1:0;
		}
	if (c)
		{
		*rp=1;
		r->top++;
		}
	m_BN_check_top(r);
	return(1);
	}

int m_BN_rshift1(M_BIGNUM *r, const M_BIGNUM *a)
	{
	m_BN_ULONG *ap,*rp,t,c;
	int i;

	m_BN_check_top(r);
	m_BN_check_top(a);

	if (m_BN_is_zero(a))
		{
		m_BN_zero(r);
		return(1);
		}
	if (a != r)
		{
		if (m_BN_wexpand(r,a->top) == NULL) return(0);
		r->top=a->top;
		r->neg=a->neg;
		}
	ap=a->d;
	rp=r->d;
	c=0;
	for (i=a->top-1; i>=0; i--)
		{
		t=ap[i];
		rp[i]=((t>>1)&m_BN_MASK2)|c;
		c=(t&1)?m_BN_TBIT:0;
		}
	m_BN_correct_top(r);
	m_BN_check_top(r);
	return(1);
	}

int m_BN_lshift(M_BIGNUM *r, const M_BIGNUM *a, int n)
	{
	int i,nw,lb,rb;
	m_BN_ULONG *t,*f;
	m_BN_ULONG l;

	m_BN_check_top(r);
	m_BN_check_top(a);

	r->neg=a->neg;
	nw=n/m_BN_BITS2;
	if (m_BN_wexpand(r,a->top+nw+1) == NULL) return(0);
	lb=n%m_BN_BITS2;
	rb=m_BN_BITS2-lb;
	f=a->d;
	t=r->d;
	t[a->top+nw]=0;
	if (lb == 0)
		for (i=a->top-1; i>=0; i--)
			t[nw+i]=f[i];
	else
		for (i=a->top-1; i>=0; i--)
			{
			l=f[i];
			t[nw+i+1]|=(l>>rb)&m_BN_MASK2;
			t[nw+i]=(l<<lb)&m_BN_MASK2;
			}
	memset(t,0,nw*sizeof(t[0]));
/*	for (i=0; i<nw; i++)
		t[i]=0;*/
	r->top=a->top+nw+1;
	m_BN_correct_top(r);
	m_BN_check_top(r);
	return(1);
	}

int m_BN_rshift(M_BIGNUM *r, const M_BIGNUM *a, int n)
	{
	int i,j,nw,lb,rb;
	m_BN_ULONG *t,*f;
	m_BN_ULONG l,tmp;

	m_BN_check_top(r);
	m_BN_check_top(a);

	nw=n/m_BN_BITS2;
	rb=n%m_BN_BITS2;
	lb=m_BN_BITS2-rb;
	if (nw >= a->top || a->top == 0)
		{
		m_BN_zero(r);
		return(1);
		}
	if (r != a)
		{
		r->neg=a->neg;
		if (m_BN_wexpand(r,a->top-nw+1) == NULL) return(0);
		}
	else
		{
		if (n == 0)
			return 1; /* or the copying loop will go berserk */
		}

	f= &(a->d[nw]);
	t=r->d;
	j=a->top-nw;
	r->top=j;

	if (rb == 0)
		{
		for (i=j; i != 0; i--)
			*(t++)= *(f++);
		}
	else
		{
		l= *(f++);
		for (i=j-1; i != 0; i--)
			{
			tmp =(l>>rb)&m_BN_MASK2;
			l= *(f++);
			*(t++) =(tmp|(l<<lb))&m_BN_MASK2;
			}
		*(t++) =(l>>rb)&m_BN_MASK2;
		}
	m_BN_correct_top(r);
	m_BN_check_top(r);
	return(1);
	}
