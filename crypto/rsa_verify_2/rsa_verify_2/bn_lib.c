/* crypto/bn/m_BN_lib.c */
/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 * 
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 * 
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */

#ifndef m_BN_DEBUG
# undef NDEBUG /* avoid conflicting definitions */
# define NDEBUG
#endif

#include <assert.h>
#include <limits.h>
#include <stdio.h>

#include "BN_lcl.h"

const char m_BN_version[]="Big Number";

/* This stuff appears to be completely unused, so is deprecated */
#ifndef OPENSSL_NO_DEPRECATED
/* For a 32 bit machine
 * 2 -   4 ==  128
 * 3 -   8 ==  256
 * 4 -  16 ==  512
 * 5 -  32 == 1024
 * 6 -  64 == 2048
 * 7 - 128 == 4096
 * 8 - 256 == 8192
 */
static int m_BN_limit_bits=0;
static int m_BN_limit_num=8;        /* (1<<m_BN_limit_bits) */
static int m_BN_limit_bits_low=0;
static int m_BN_limit_num_low=8;    /* (1<<m_BN_limit_bits_low) */
static int m_BN_limit_bits_high=0;
static int m_BN_limit_num_high=8;   /* (1<<m_BN_limit_bits_high) */
static int m_BN_limit_bits_mont=0;
static int m_BN_limit_num_mont=8;   /* (1<<m_BN_limit_bits_mont) */

void m_BN_set_params(int mult, int high, int low, int mont)
	{
	if (mult >= 0)
		{
		if (mult > (int)(sizeof(int)*8)-1)
			mult=sizeof(int)*8-1;
		m_BN_limit_bits=mult;
		m_BN_limit_num=1<<mult;
		}
	if (high >= 0)
		{
		if (high > (int)(sizeof(int)*8)-1)
			high=sizeof(int)*8-1;
		m_BN_limit_bits_high=high;
		m_BN_limit_num_high=1<<high;
		}
	if (low >= 0)
		{
		if (low > (int)(sizeof(int)*8)-1)
			low=sizeof(int)*8-1;
		m_BN_limit_bits_low=low;
		m_BN_limit_num_low=1<<low;
		}
	if (mont >= 0)
		{
		if (mont > (int)(sizeof(int)*8)-1)
			mont=sizeof(int)*8-1;
		m_BN_limit_bits_mont=mont;
		m_BN_limit_num_mont=1<<mont;
		}
	}

int m_BN_get_params(int which)
	{
	if      (which == 0) return(m_BN_limit_bits);
	else if (which == 1) return(m_BN_limit_bits_high);
	else if (which == 2) return(m_BN_limit_bits_low);
	else if (which == 3) return(m_BN_limit_bits_mont);
	else return(0);
	}
#endif

const M_BIGNUM *m_BN_value_one(void)
	{
	static const m_BN_ULONG data_one=1L;
	static const M_BIGNUM const_one={(m_BN_ULONG *)&data_one,1,1,0,m_BN_FLG_STATIC_DATA};

	return(&const_one);
	}

char *m_BN_options(void)
	{
	static int init=0;
	static char data[16];

	if (!init)
		{
		init++;
#ifdef m_BN_LLONG
		//BIO_snprintf(data,sizeof data,"bn(%d,%d)",
		//	     (int)sizeof(m_BN_ULLONG)*8,(int)sizeof(m_BN_ULONG)*8);
#else
		//BIO_snprintf(data,sizeof data,"bn(%d,%d)",
		//	     (int)sizeof(m_BN_ULONG)*8,(int)sizeof(m_BN_ULONG)*8);
#endif
		}
	return(data);
	}

int m_BN_num_bits_word(m_BN_ULONG l)
	{
	static const unsigned char bits[256]={
		0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4,
		5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
		6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
		6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
		7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
		7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
		7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
		7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
		};

#if defined(SIXTY_FOUR_BIT_LONG)
	if (l & 0xffffffff00000000L)
		{
		if (l & 0xffff000000000000L)
			{
			if (l & 0xff00000000000000L)
				{
				return(bits[(int)(l>>56)]+56);
				}
			else	return(bits[(int)(l>>48)]+48);
			}
		else
			{
			if (l & 0x0000ff0000000000L)
				{
				return(bits[(int)(l>>40)]+40);
				}
			else	return(bits[(int)(l>>32)]+32);
			}
		}
	else
#else
#ifdef SIXTY_FOUR_BIT
	if (l & 0xffffffff00000000LL)
		{
		if (l & 0xffff000000000000LL)
			{
			if (l & 0xff00000000000000LL)
				{
				return(bits[(int)(l>>56)]+56);
				}
			else	return(bits[(int)(l>>48)]+48);
			}
		else
			{
			if (l & 0x0000ff0000000000LL)
				{
				return(bits[(int)(l>>40)]+40);
				}
			else	return(bits[(int)(l>>32)]+32);
			}
		}
	else
#endif
#endif
		{
#if defined(THIRTY_TWO_BIT) || defined(SIXTY_FOUR_BIT) || defined(SIXTY_FOUR_BIT_LONG)
		if (l & 0xffff0000L)
			{
			if (l & 0xff000000L)
				return(bits[(int)(l>>24L)]+24);
			else	return(bits[(int)(l>>16L)]+16);
			}
		else
#endif
			{
#if defined(THIRTY_TWO_BIT) || defined(SIXTY_FOUR_BIT) || defined(SIXTY_FOUR_BIT_LONG)
			if (l & 0xff00L)
				return(bits[(int)(l>>8)]+8);
			else	
#endif
				return(bits[(int)(l   )]  );
			}
		}
	}

int m_BN_num_bits(const M_BIGNUM *a)
	{
	int i = a->top - 1;
	m_BN_check_top(a);

	if (m_BN_is_zero(a)) return 0;
	return ((i*m_BN_BITS2) + m_BN_num_bits_word(a->d[i]));
	}

void m_BN_clear_free(M_BIGNUM *a)
	{
	int i;

	if (a == NULL) return;
	m_BN_check_top(a);
	if (a->d != NULL)
		{
		//OPENSSL_cleanse(a->d,a->dmax*sizeof(a->d[0]));
		if (!(m_BN_get_flags(a,m_BN_FLG_STATIC_DATA)))
			free(a->d);
		}
	i=m_BN_get_flags(a,m_BN_FLG_MALLOCED);
	//OPENSSL_cleanse(a,sizeof(M_BIGNUM));
	if (i)
		free(a);
	}

inline void m_BN_free(M_BIGNUM *a)
	{
	if (a == NULL) return;
	m_BN_check_top(a);
	if ((a->d != NULL) && !(m_BN_get_flags(a,m_BN_FLG_STATIC_DATA)))
		free(a->d);
	if (a->flags & m_BN_FLG_MALLOCED)
		free(a);
	else
		{
#ifndef OPENSSL_NO_DEPRECATED
		a->flags|=m_BN_FLG_FREE;
#endif
		a->d = NULL;
		}
	}

extern inline void m_BN_init(M_BIGNUM *a) 
	{
	memset(a,0,sizeof(M_BIGNUM));
	m_BN_check_top(a);
	}

M_BIGNUM *m_BN_new(void)
	{
	M_BIGNUM *ret;

	if ((ret=(M_BIGNUM *)malloc(sizeof(M_BIGNUM))) == NULL)
		{
		return(NULL);
		}
	ret->flags=m_BN_FLG_MALLOCED;
	ret->top=0;
	ret->neg=0;
	ret->dmax=0;
	ret->d=NULL;
	m_BN_check_top(ret);
	return(ret);
	}

/* This is used both by m_BN_expand2() and m_BN_dup_expand() */
/* The caller MUST check that words > b->dmax before calling this */
static m_BN_ULONG *m_BN_expand_internal(const M_BIGNUM *b, int words)
	{
	m_BN_ULONG *A,*a = NULL;
	const m_BN_ULONG *B;
	int i;

	m_BN_check_top(b);

	if (words > (INT_MAX/(4*m_BN_BITS2)))
		{
		//BNerr(m_BN_F_m_BN_EXPAND_INTERNAL,m_BN_R_M_BIGNUM_TOO_LONG);
		return NULL;
		}
	if (m_BN_get_flags(b,m_BN_FLG_STATIC_DATA))
		{
		//BNerr(m_BN_F_m_BN_EXPAND_INTERNAL,m_BN_R_EXPAND_ON_STATIC_M_BIGNUM_DATA);
		return(NULL);
		}
	a=A=(m_BN_ULONG *)malloc(sizeof(m_BN_ULONG)*words);
	if (A == NULL)
		{
		return(NULL);
		}
#if 1
	B=b->d;
	/* Check if the previous number needs to be copied */
	if (B != NULL)
		{
		for (i=b->top>>2; i>0; i--,A+=4,B+=4)
			{
			/*
			 * The fact that the loop is unrolled
			 * 4-wise is a tribute to Intel. It's
			 * the one that doesn't have enough
			 * registers to accomodate more data.
			 * I'd unroll it 8-wise otherwise:-)
			 *
			 *		<appro@fy.chalmers.se>
			 */
			m_BN_ULONG a0,a1,a2,a3;
			a0=B[0]; a1=B[1]; a2=B[2]; a3=B[3];
			A[0]=a0; A[1]=a1; A[2]=a2; A[3]=a3;
			}
		switch (b->top&3)
			{
		case 3:	A[2]=B[2];
		case 2:	A[1]=B[1];
		case 1:	A[0]=B[0];
		case 0: /* workaround for ultrix cc: without 'case 0', the optimizer does
		         * the switch table by doing a=top&3; a--; goto jump_table[a];
		         * which fails for top== 0 */
			;
			}
		}

#else
	memset(A,0,sizeof(m_BN_ULONG)*words);
	memcpy(A,b->d,sizeof(b->d[0])*b->top);
#endif
		
	return(a);
	}

/* This is an internal function that can be used instead of m_BN_expand2()
 * when there is a need to copy M_BIGNUMs instead of only expanding the
 * data part, while still expanding them.
 * Especially useful when needing to expand M_BIGNUMs that are declared
 * 'const' and should therefore not be changed.
 * The reason to use this instead of a m_BN_dup() followed by a m_BN_expand2()
 * is memory allocation overhead.  A m_BN_dup() followed by a m_BN_expand2()
 * will allocate new memory for the M_BIGNUM data twice, and free it once,
 * while m_BN_dup_expand() makes sure allocation is made only once.
 */

#ifndef OPENSSL_NO_DEPRECATED
M_BIGNUM *m_BN_dup_expand(const M_BIGNUM *b, int words)
	{
	M_BIGNUM *r = NULL;

	m_BN_check_top(b);

	/* This function does not work if
	 *      words <= b->dmax && top < words
	 * because m_BN_dup() does not preserve 'dmax'!
	 * (But m_BN_dup_expand() is not used anywhere yet.)
	 */

	if (words > b->dmax)
		{
		m_BN_ULONG *a = m_BN_expand_internal(b, words);

		if (a)
			{
			r = m_BN_new();
			if (r)
				{
				r->top = b->top;
				r->dmax = words;
				r->neg = b->neg;
				r->d = a;
				}
			else
				{
				/* r == NULL, m_BN_new failure */
				free(a);
				}
			}
		/* If a == NULL, there was an error in allocation in
		   m_BN_expand_internal(), and NULL should be returned */
		}
	else
		{
		r = m_BN_dup(b);
		}

	m_BN_check_top(r);
	return r;
	}
#endif

/* This is an internal function that should not be used in applications.
 * It ensures that 'b' has enough room for a 'words' word number
 * and initialises any unused part of b->d with leading zeros.
 * It is mostly used by the various M_BIGNUM routines. If there is an error,
 * NULL is returned. If not, 'b' is returned. */

M_BIGNUM *m_BN_expand2(M_BIGNUM *b, int words)
	{
	m_BN_check_top(b);

	if (words > b->dmax)
		{
		m_BN_ULONG *a = m_BN_expand_internal(b, words);
		if(!a) return NULL;
		if(b->d) free(b->d);
		b->d=a;
		b->dmax=words;
		}

/* None of this should be necessary because of what b->top means! */
#if 0
	/* NB: m_BN_wexpand() calls this only if the M_BIGNUM really has to grow */
	if (b->top < b->dmax)
		{
		int i;
		m_BN_ULONG *A = &(b->d[b->top]);
		for (i=(b->dmax - b->top)>>3; i>0; i--,A+=8)
			{
			A[0]=0; A[1]=0; A[2]=0; A[3]=0;
			A[4]=0; A[5]=0; A[6]=0; A[7]=0;
			}
		for (i=(b->dmax - b->top)&7; i>0; i--,A++)
			A[0]=0;
		assert(A == &(b->d[b->dmax]));
		}
#endif
	m_BN_check_top(b);
	return b;
	}

M_BIGNUM *m_BN_dup(const M_BIGNUM *a)
	{
	M_BIGNUM *t;

	if (a == NULL) return NULL;
	m_BN_check_top(a);

	t = m_BN_new();
	if (t == NULL) return NULL;
	if(!m_BN_copy(t, a))
		{
		m_BN_free(t);
		return NULL;
		}
	m_BN_check_top(t);
	return t;
	}

M_BIGNUM *m_BN_copy(M_BIGNUM *a, const M_BIGNUM *b)
	{
	int i;
	m_BN_ULONG *A;
	const m_BN_ULONG *B;

	m_BN_check_top(b);

	if (a == b) return(a);
	if (m_BN_wexpand(a,b->top) == NULL) return(NULL);

#if 1
	A=a->d;
	B=b->d;
	for (i=b->top>>2; i>0; i--,A+=4,B+=4)
		{
		m_BN_ULONG a0,a1,a2,a3;
		a0=B[0]; a1=B[1]; a2=B[2]; a3=B[3];
		A[0]=a0; A[1]=a1; A[2]=a2; A[3]=a3;
		}
	switch (b->top&3)
		{
		case 3: A[2]=B[2];
		case 2: A[1]=B[1];
		case 1: A[0]=B[0];
		case 0: ; /* ultrix cc workaround, see comments in m_BN_expand_internal */
		}
#else
	memcpy(a->d,b->d,sizeof(b->d[0])*b->top);
#endif

	a->top=b->top;
	a->neg=b->neg;
	m_BN_check_top(a);
	return(a);
	}

void m_BN_swap(M_BIGNUM *a, M_BIGNUM *b)
	{
	int flags_old_a, flags_old_b;
	m_BN_ULONG *tmp_d;
	int tmp_top, tmp_dmax, tmp_neg;
	
	m_BN_check_top(a);
	m_BN_check_top(b);

	flags_old_a = a->flags;
	flags_old_b = b->flags;

	tmp_d = a->d;
	tmp_top = a->top;
	tmp_dmax = a->dmax;
	tmp_neg = a->neg;
	
	a->d = b->d;
	a->top = b->top;
	a->dmax = b->dmax;
	a->neg = b->neg;
	
	b->d = tmp_d;
	b->top = tmp_top;
	b->dmax = tmp_dmax;
	b->neg = tmp_neg;
	
	a->flags = (flags_old_a & m_BN_FLG_MALLOCED) | (flags_old_b & m_BN_FLG_STATIC_DATA);
	b->flags = (flags_old_b & m_BN_FLG_MALLOCED) | (flags_old_a & m_BN_FLG_STATIC_DATA);
	m_BN_check_top(a);
	m_BN_check_top(b);
	}

void m_BN_clear(M_BIGNUM *a)
	{
	m_BN_check_top(a);
	if (a->d != NULL)
		memset(a->d,0,a->dmax*sizeof(a->d[0]));
	a->top=0;
	a->neg=0;
	}

m_BN_ULONG m_BN_get_word(const M_BIGNUM *a)
	{
	if (a->top > 1)
		return m_BN_MASK2;
	else if (a->top == 1)
		return a->d[0];
	/* a->top == 0 */
	return 0;
	}

int m_BN_set_word(M_BIGNUM *a, m_BN_ULONG w)
	{
	m_BN_check_top(a);
	if (m_BN_expand(a,(int)sizeof(m_BN_ULONG)*8) == NULL) return(0);
	a->neg = 0;
	a->d[0] = w;
	a->top = (w ? 1 : 0);
	m_BN_check_top(a);
	return(1);
	}

M_BIGNUM *m_BN_bin2bn(const unsigned char *s, int len, M_BIGNUM *ret)
	{
	unsigned int i,m;
	unsigned int n;
	m_BN_ULONG l;
	M_BIGNUM  *bn = NULL;

	if (ret == NULL)
		ret = bn = m_BN_new();
	if (ret == NULL) return(NULL);
	m_BN_check_top(ret);
	l=0;
	n=len;
	if (n == 0)
		{
		ret->top=0;
		return(ret);
		}
	i=((n-1)/m_BN_BYTES)+1;
	m=((n-1)%(m_BN_BYTES));
	if (m_BN_wexpand(ret, (int)i) == NULL)
		{
		if (bn) m_BN_free(bn);
		return NULL;
		}
	ret->top=i;
	ret->neg=0;
	while (n--)
		{
		l=(l<<8L)| *(s++);
		if (m-- == 0)
			{
			ret->d[--i]=l;
			l=0;
			m=m_BN_BYTES-1;
			}
		}
	/* need to call this due to clear byte at top if avoiding
	 * having the top bit set (-ve number) */
	m_BN_correct_top(ret);
	return(ret);
	}

/* ignore negative */
int m_BN_bn2bin(const M_BIGNUM *a, unsigned char *to)
	{
	int n,i;
	m_BN_ULONG l;

	m_BN_check_top(a);
	n=i=m_BN_num_bytes(a);
	while (i--)
		{
		l=a->d[i/m_BN_BYTES];
		*(to++)=(unsigned char)(l>>(8*(i%m_BN_BYTES)))&0xff;
		}
	return(n);
	}

int m_BN_ucmp(const M_BIGNUM *a, const M_BIGNUM *b)
	{
	int i;
	m_BN_ULONG t1,t2,*ap,*bp;

	m_BN_check_top(a);
	m_BN_check_top(b);

	i=a->top-b->top;
	if (i != 0) return(i);
	ap=a->d;
	bp=b->d;
	for (i=a->top-1; i>=0; i--)
		{
		t1= ap[i];
		t2= bp[i];
		if (t1 != t2)
			return((t1 > t2) ? 1 : -1);
		}
	return(0);
	}

int m_BN_cmp(const M_BIGNUM *a, const M_BIGNUM *b)
	{
	int i;
	int gt,lt;
	m_BN_ULONG t1,t2;

	if ((a == NULL) || (b == NULL))
		{
		if (a != NULL)
			return(-1);
		else if (b != NULL)
			return(1);
		else
			return(0);
		}

	m_BN_check_top(a);
	m_BN_check_top(b);

	if (a->neg != b->neg)
		{
		if (a->neg)
			return(-1);
		else	return(1);
		}
	if (a->neg == 0)
		{ gt=1; lt= -1; }
	else	{ gt= -1; lt=1; }

	if (a->top > b->top) return(gt);
	if (a->top < b->top) return(lt);
	for (i=a->top-1; i>=0; i--)
		{
		t1=a->d[i];
		t2=b->d[i];
		if (t1 > t2) return(gt);
		if (t1 < t2) return(lt);
		}
	return(0);
	}

int m_BN_set_bit(M_BIGNUM *a, int n)
	{
	int i,j,k;

	if (n < 0)
		return 0;

	i=n/m_BN_BITS2;
	j=n%m_BN_BITS2;
	if (a->top <= i)
		{
		if (m_BN_wexpand(a,i+1) == NULL) return(0);
		for(k=a->top; k<i+1; k++)
			a->d[k]=0;
		a->top=i+1;
		}

	a->d[i]|=(((m_BN_ULONG)1)<<j);
	m_BN_check_top(a);
	return(1);
	}

int m_BN_clear_bit(M_BIGNUM *a, int n)
	{
	int i,j;

	m_BN_check_top(a);
	if (n < 0) return 0;

	i=n/m_BN_BITS2;
	j=n%m_BN_BITS2;
	if (a->top <= i) return(0);

	a->d[i]&=(~(((m_BN_ULONG)1)<<j));
	m_BN_correct_top(a);
	return(1);
	}

int m_BN_is_bit_set(const M_BIGNUM *a, int n)
	{
	int i,j;

	m_BN_check_top(a);
	if (n < 0) return 0;
	i=n/m_BN_BITS2;
	j=n%m_BN_BITS2;
	if (a->top <= i) return 0;
	return (int)(((a->d[i])>>j)&((m_BN_ULONG)1));
	}

int m_BN_mask_bits(M_BIGNUM *a, int n)
	{
	int b,w;

	m_BN_check_top(a);
	if (n < 0) return 0;

	w=n/m_BN_BITS2;
	b=n%m_BN_BITS2;
	if (w >= a->top) return 0;
	if (b == 0)
		a->top=w;
	else
		{
		a->top=w+1;
		a->d[w]&= ~(m_BN_MASK2<<b);
		}
	m_BN_correct_top(a);
	return(1);
	}

void m_BN_set_negative(M_BIGNUM *a, int b)
	{
	if (b && !m_BN_is_zero(a))
		a->neg = 1;
	else
		a->neg = 0;
	}

int m_BN_cmp_words(const m_BN_ULONG *a, const m_BN_ULONG *b, int n)
	{
	int i;
	m_BN_ULONG aa,bb;

	aa=a[n-1];
	bb=b[n-1];
	if (aa != bb) return((aa > bb)?1:-1);
	for (i=n-2; i>=0; i--)
		{
		aa=a[i];
		bb=b[i];
		if (aa != bb) return((aa > bb)?1:-1);
		}
	return(0);
	}

/* Here follows a specialised variants of m_BN_cmp_words().  It has the
   property of performing the operation on arrays of different sizes.
   The sizes of those arrays is expressed through cl, which is the
   common length ( basicall, min(len(a),len(b)) ), and dl, which is the
   delta between the two lengths, calculated as len(a)-len(b).
   All lengths are the number of m_BN_ULONGs...  */

int m_BN_cmp_part_words(const m_BN_ULONG *a, const m_BN_ULONG *b,
	int cl, int dl)
	{
	int n,i;
	n = cl-1;

	if (dl < 0)
		{
		for (i=dl; i<0; i++)
			{
			if (b[n-i] != 0)
				return -1; /* a < b */
			}
		}
	if (dl > 0)
		{
		for (i=dl; i>0; i--)
			{
			if (a[n+i] != 0)
				return 1; /* a > b */
			}
		}
	return m_BN_cmp_words(a,b,cl);
	}
