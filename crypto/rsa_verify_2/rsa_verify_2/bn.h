/* crypto/bn/bn.h */
/* Copyright (C) 1995-1997 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 * 
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 * 
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */
/* ====================================================================
 * Copyright (c) 1998-2006 The OpenSSL Project.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
 *
 * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission. For written permission, please contact
 *    openssl-core@openssl.org.
 *
 * 5. Products derived from this software may not be called "OpenSSL"
 *    nor may "OpenSSL" appear in their names without prior written
 *    permission of the OpenSSL Project.
 *
 * 6. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
 *
 * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */
/* ====================================================================
 * Copyright 2002 Sun Microsystems, Inc. ALL RIGHTS RESERVED.
 *
 * Portions of the attached software ("Contribution") are developed by 
 * SUN MICROSYSTEMS, INC., and are contributed to the OpenSSL project.
 *
 * The Contribution is licensed pursuant to the Eric Young open source
 * license provided above.
 *
 * The binary polynomial arithmetic software is originally written by 
 * Sheueling Chang Shantz and Douglas Stebila of Sun Microsystems Laboratories.
 *
 */

#ifndef HEADER_m_BN_H
#define HEADER_m_BN_H

#include <stdlib.h>
#include <string.h>

#ifndef OPENSSL_NO_FP_API
#include <stdio.h> /* FILE */
#endif

#ifdef  __cplusplus
extern "C" {
#endif

/* These preprocessor symbols control various aspects of the M_BIGNUM headers and
 * library code. They're not defined by any "normal" configuration, as they are
 * intended for development and testing purposes. NB: defining all three can be
 * useful for debugging application code as well as openssl itself.
 *
 * m_BN_DEBUG - turn on various debugging alterations to the M_BIGNUM code
 * m_BN_DEBUG_RAND - uses random poisoning of unused words to trip up
 * mismanagement of M_BIGNUM internals. You must also define m_BN_DEBUG.
 */
/* #define m_BN_DEBUG */
/* #define m_BN_DEBUG_RAND */

#ifndef OPENSSL_SMALL_FOOTPRINT
#define m_BN_MUL_COMBA
#define m_BN_SQR_COMBA
#define m_BN_RECURSION
#endif

/* This next option uses the C libraries (2 word)/(1 word) function.
 * If it is not defined, I use my C version (which is slower).
 * The reason for this flag is that when the particular C compiler
 * library routine is used, and the library is linked with a different
 * compiler, the library is missing.  This mostly happens when the
 * library is built with gcc and then linked using normal cc.  This would
 * be a common occurrence because gcc normally produces code that is
 * 2 times faster than system compilers for the big number stuff.
 * For machines with only one compiler (or shared libraries), this should
 * be on.  Again this in only really a problem on machines
 * using "long long's", are 32bit, and are not using my assembler code. */
#if defined(OPENSSL_SYS_MSDOS) || defined(OPENSSL_SYS_WINDOWS) || \
    defined(OPENSSL_SYS_WIN32) || defined(linux)
# ifndef m_BN_DIV2W
#  define m_BN_DIV2W
# endif
#endif

/* assuming long is 64bit - this is the DEC Alpha
 * unsigned long long is only 64 bits :-(, don't define
 * m_BN_LLONG for the DEC Alpha */
#ifdef SIXTY_FOUR_BIT_LONG
#define m_BN_ULLONG	unsigned long long
#define m_BN_ULONG	unsigned long
#define m_BN_LONG		long
#define m_BN_BITS		128
#define m_BN_BYTES	8
#define m_BN_BITS2	64
#define m_BN_BITS4	32
#define m_BN_MASK		(0xffffffffffffffffffffffffffffffffLL)
#define m_BN_MASK2	(0xffffffffffffffffL)
#define m_BN_MASK2l	(0xffffffffL)
#define m_BN_MASK2h	(0xffffffff00000000L)
#define m_BN_MASK2h1	(0xffffffff80000000L)
#define m_BN_TBIT		(0x8000000000000000L)
#define m_BN_DEC_CONV	(10000000000000000000UL)
#define m_BN_DEC_FMT1	"%lu"
#define m_BN_DEC_FMT2	"%019lu"
#define m_BN_DEC_NUM	19
#define m_BN_HEX_FMT1	"%lX"
#define m_BN_HEX_FMT2	"%016lX"
#endif

/* This is where the long long data type is 64 bits, but long is 32.
 * For machines where there are 64bit registers, this is the mode to use.
 * IRIX, on R4000 and above should use this mode, along with the relevant
 * assembler code :-).  Do NOT define m_BN_LLONG.
 */
#ifdef SIXTY_FOUR_BIT
#undef m_BN_LLONG
#undef m_BN_ULLONG
#define m_BN_ULONG	unsigned long long
#define m_BN_LONG		long long
#define m_BN_BITS		128
#define m_BN_BYTES	8
#define m_BN_BITS2	64
#define m_BN_BITS4	32
#define m_BN_MASK2	(0xffffffffffffffffLL)
#define m_BN_MASK2l	(0xffffffffL)
#define m_BN_MASK2h	(0xffffffff00000000LL)
#define m_BN_MASK2h1	(0xffffffff80000000LL)
#define m_BN_TBIT		(0x8000000000000000LL)
#define m_BN_DEC_CONV	(10000000000000000000ULL)
#define m_BN_DEC_FMT1	"%llu"
#define m_BN_DEC_FMT2	"%019llu"
#define m_BN_DEC_NUM	19
#define m_BN_HEX_FMT1	"%llX"
#define m_BN_HEX_FMT2	"%016llX"
#endif

#ifdef THIRTY_TWO_BIT
    
#define m_BN_ULONG	unsigned int
#define m_BN_LONG		int
#define m_BN_BITS		64
#define m_BN_BYTES	4
#define m_BN_BITS2	32
#define m_BN_BITS4	16
#define m_BN_MASK2	(0xffffffffL)
#define m_BN_MASK2l	(0xffff)
#define m_BN_MASK2h1	(0xffff8000L)
#define m_BN_MASK2h	(0xffff0000L)
#define m_BN_TBIT		(0x80000000L)
#define m_BN_DEC_CONV	(1000000000L)
#define m_BN_DEC_FMT1	"%u"
#define m_BN_DEC_FMT2	"%09u"
#define m_BN_DEC_NUM	9
#define m_BN_HEX_FMT1	"%X"
#define m_BN_HEX_FMT2	"%08X"
    
#ifdef m_BN_LLONG
# if defined(_WIN32) && !defined(__GNUC__)
#  define m_BN_ULLONG	unsigned __int64
#  define m_BN_MASK	(0xffffffffffffffffI64)
# else
#  define m_BN_ULLONG	unsigned long long
#  define m_BN_MASK	(0xffffffffffffffffLL)
# endif
#endif

#endif

    
    
#define m_BN_DEFAULT_BITS	1280

#define m_BN_FLG_MALLOCED		0x01
#define m_BN_FLG_STATIC_DATA	0x02
#define m_BN_FLG_CONSTTIME	0x04 /* avoid leaking exponent information through timing,
                                      * m_BN_mod_exp_mont() will call m_BN_mod_exp_mont_consttime,
                                      * m_BN_div() will call m_BN_div_no_branch,
                                      * m_BN_mod_inverse() will call m_BN_mod_inverse_no_branch.
                                      */

#ifndef OPENSSL_NO_DEPRECATED
#define m_BN_FLG_EXP_CONSTTIME m_BN_FLG_CONSTTIME /* deprecated name for the flag */
                                      /* avoid leaking exponent information through timings
                                      * (m_BN_mod_exp_mont() will call m_BN_mod_exp_mont_consttime) */
#endif

#ifndef OPENSSL_NO_DEPRECATED
#define m_BN_FLG_FREE		0x8000	/* used for debuging */
#endif
#define m_BN_set_flags(b,n)	((b)->flags|=(n))
#define m_BN_get_flags(b,n)	((b)->flags&(n))

/* get a clone of a M_BIGNUM with changed flags, for *temporary* use only
 * (the two M_BIGNUMs cannot not be used in parallel!) */
#define m_BN_with_flags(dest,b,n)  ((dest)->d=(b)->d, \
                                  (dest)->top=(b)->top, \
                                  (dest)->dmax=(b)->dmax, \
                                  (dest)->neg=(b)->neg, \
                                  (dest)->flags=(((dest)->flags & m_BN_FLG_MALLOCED) \
                                                 |  ((b)->flags & ~m_BN_FLG_MALLOCED) \
                                                 |  m_BN_FLG_STATIC_DATA \
                                                 |  (n)))

/* Already declared in ossl_typ.h */
typedef struct m_M_BIGNUM_st M_BIGNUM;
/* Used for temp variables (declaration hidden in m_BN_lcl.h) */
typedef struct M_BIGNUM_ctx m_BN_CTX;
typedef struct m_BN_blinding_st m_BN_BLINDING;
typedef struct m_BN_mont_ctx_st m_BN_MONT_CTX;
typedef struct m_BN_recp_ctx_st m_BN_RECP_CTX;
typedef struct m_BN_gencb_st m_BN_GENCB;

struct m_M_BIGNUM_st
	{

	m_BN_ULONG *d;	/* Pointer to an array of 'm_BN_BITS2' bit chunks. */
	int top;	/* Index of last used d +1. */
	/* The next are internal book keeping for m_BN_expand. */
	int dmax;	/* Size of the d array. */
	int neg;	/* one if the number is negative */
	int flags;
	};

/* Used for montgomery multiplication */
struct m_BN_mont_ctx_st
	{
	int ri;        /* number of bits in R */
	M_BIGNUM RR;     /* used to convert to montgomery form */
	M_BIGNUM N;      /* The modulus */
	M_BIGNUM Ni;     /* R*(1/R mod N) - N*Ni = 1
	                * (Ni is only stored for M_BIGNUM algorithm) */
	m_BN_ULONG n0[2];/* least significant word(s) of Ni;
	                  (type changed with 0.9.9, was "m_BN_ULONG n0;" before) */
	int flags;
	};

/* Used for reciprocal division/mod functions
 * It cannot be shared between threads
 */
struct m_BN_recp_ctx_st
	{
	M_BIGNUM N;	/* the divisor */
	M_BIGNUM Nr;	/* the reciprocal */
	int num_bits;
	int shift;
	int flags;
	};

/* Used for slow "generation" functions. */
struct m_BN_gencb_st
	{
	unsigned int ver;	/* To handle binary (in)compatibility */
	void *arg;		/* callback-specific data */
	union
		{
		/* if(ver==1) - handles old style callbacks */
		void (*cb_1)(int, int, void *);
		/* if(ver==2) - new callback style */
		int (*cb_2)(int, int, m_BN_GENCB *);
		} cb;
	};
/* Wrapper function to make using m_BN_GENCB easier,  */
int m_BN_GENCB_call(m_BN_GENCB *cb, int a, int b);
/* Macro to populate a m_BN_GENCB structure with an "old"-style callback */
#define m_BN_GENCB_set_old(gencb, callback, cb_arg) { \
		m_BN_GENCB *tmp_gencb = (gencb); \
		tmp_gencb->ver = 1; \
		tmp_gencb->arg = (cb_arg); \
		tmp_gencb->cb.cb_1 = (callback); }
/* Macro to populate a m_BN_GENCB structure with a "new"-style callback */
#define m_BN_GENCB_set(gencb, callback, cb_arg) { \
		m_BN_GENCB *tmp_gencb = (gencb); \
		tmp_gencb->ver = 2; \
		tmp_gencb->arg = (cb_arg); \
		tmp_gencb->cb.cb_2 = (callback); }

#define m_BN_prime_checks 0 /* default: select number of iterations
			     based on the size of the number */

/* number of Miller-Rabin iterations for an error rate  of less than 2^-80
 * for random 'b'-bit input, b >= 100 (taken from table 4.4 in the Handbook
 * of Applied Cryptography [Menezes, van Oorschot, Vanstone; CRC Press 1996];
 * original paper: Damgaard, Landrock, Pomerance: Average case error estimates
 * for the strong probable prime test. -- Math. Comp. 61 (1993) 177-194) */
#define m_BN_prime_checks_for_size(b) ((b) >= 1300 ?  2 : \
                                (b) >=  850 ?  3 : \
                                (b) >=  650 ?  4 : \
                                (b) >=  550 ?  5 : \
                                (b) >=  450 ?  6 : \
                                (b) >=  400 ?  7 : \
                                (b) >=  350 ?  8 : \
                                (b) >=  300 ?  9 : \
                                (b) >=  250 ? 12 : \
                                (b) >=  200 ? 15 : \
                                (b) >=  150 ? 18 : \
                                /* b >= 100 */ 27)

#define m_BN_num_bytes(a)	((m_BN_num_bits(a)+7)/8)

/* Note that m_BN_abs_is_word didn't work reliably for w == 0 until 0.9.8 */
#define m_BN_abs_is_word(a,w) ((((a)->top == 1) && ((a)->d[0] == (m_BN_ULONG)(w))) || \
				(((w) == 0) && ((a)->top == 0)))
#define m_BN_is_zero(a)       ((a)->top == 0)
#define m_BN_is_one(a)        (m_BN_abs_is_word((a),1) && !(a)->neg)
#define m_BN_is_word(a,w)     (m_BN_abs_is_word((a),(w)) && (!(w) || !(a)->neg))
#define m_BN_is_odd(a)	    (((a)->top > 0) && ((a)->d[0] & 1))

#define m_BN_one(a)	(m_BN_set_word((a),1))
#define m_BN_zero_ex(a) \
	do { \
		M_BIGNUM *_tmp_bn = (a); \
		_tmp_bn->top = 0; \
		_tmp_bn->neg = 0; \
	} while(0)
#ifdef OPENSSL_NO_DEPRECATED
#define m_BN_zero(a)	m_BN_zero_ex(a)
#else
#define m_BN_zero(a)	(m_BN_set_word((a),0))
#endif

const M_BIGNUM *m_BN_value_one(void);
char *	m_BN_options(void);
m_BN_CTX *m_BN_CTX_new(void);
#ifndef OPENSSL_NO_DEPRECATED
void	m_BN_CTX_init(m_BN_CTX *c);
#endif
void	m_BN_CTX_free(m_BN_CTX *c);
void	m_BN_CTX_start(m_BN_CTX *ctx);
M_BIGNUM *m_BN_CTX_get(m_BN_CTX *ctx);
void	m_BN_CTX_end(m_BN_CTX *ctx);
int     m_BN_rand(M_BIGNUM *rnd, int bits, int top,int bottom);
int     m_BN_pseudo_rand(M_BIGNUM *rnd, int bits, int top,int bottom);
static int	m_BN_rand_range(M_BIGNUM *rnd, const M_BIGNUM *range);
int	m_BN_pseudo_rand_range(M_BIGNUM *rnd, const M_BIGNUM *range);
int	m_BN_num_bits(const M_BIGNUM *a);
int	m_BN_num_bits_word(m_BN_ULONG a);
M_BIGNUM *m_BN_new(void);
void	m_BN_init(M_BIGNUM *a);
void	m_BN_clear_free(M_BIGNUM *a);
M_BIGNUM *m_BN_copy(M_BIGNUM *a, const M_BIGNUM *b);
void	m_BN_swap(M_BIGNUM *a, M_BIGNUM *b);
M_BIGNUM *m_BN_bin2bn(const unsigned char *s,int len,M_BIGNUM *ret);
int	m_BN_bn2bin(const M_BIGNUM *a, unsigned char *to);
M_BIGNUM *m_BN_mpi2bn(const unsigned char *s,int len,M_BIGNUM *ret);
int	m_BN_bn2mpi(const M_BIGNUM *a, unsigned char *to);
int	m_BN_sub(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b);
int	m_BN_usub(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b);
int	m_BN_uadd(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b);
int	m_BN_add(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b);
int	m_BN_mul(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b, m_BN_CTX *ctx);
int	m_BN_sqr(M_BIGNUM *r, const M_BIGNUM *a,m_BN_CTX *ctx);
/** m_BN_set_negative sets sign of a M_BIGNUM
 * \param  b  pointer to the M_BIGNUM object
 * \param  n  0 if the M_BIGNUM b should be positive and a value != 0 otherwise 
 */
void	m_BN_set_negative(M_BIGNUM *b, int n);
/** m_BN_is_negative returns 1 if the M_BIGNUM is negative
 * \param  a  pointer to the M_BIGNUM object
 * \return 1 if a < 0 and 0 otherwise
 */
#define m_BN_is_negative(a) ((a)->neg != 0)

int	m_BN_div(M_BIGNUM *dv, M_BIGNUM *rem, const M_BIGNUM *m, const M_BIGNUM *d,
	m_BN_CTX *ctx);
#define m_BN_mod(rem,m,d,ctx) m_BN_div(NULL,(rem),(m),(d),(ctx))
int	m_BN_nnmod(M_BIGNUM *r, const M_BIGNUM *m, const M_BIGNUM *d, m_BN_CTX *ctx);
int	m_BN_mod_add(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b, const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_mod_add_quick(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b, const M_BIGNUM *m);
int	m_BN_mod_sub(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b, const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_mod_sub_quick(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b, const M_BIGNUM *m);
int	m_BN_mod_mul(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_mod_sqr(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_mod_lshift1(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_mod_lshift1_quick(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *m);
int	m_BN_mod_lshift(M_BIGNUM *r, const M_BIGNUM *a, int n, const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_mod_lshift_quick(M_BIGNUM *r, const M_BIGNUM *a, int n, const M_BIGNUM *m);

m_BN_ULONG m_BN_mod_word(const M_BIGNUM *a, m_BN_ULONG w);
m_BN_ULONG m_BN_div_word(M_BIGNUM *a, m_BN_ULONG w);
int	m_BN_mul_word(M_BIGNUM *a, m_BN_ULONG w);
int	m_BN_add_word(M_BIGNUM *a, m_BN_ULONG w);
int	m_BN_sub_word(M_BIGNUM *a, m_BN_ULONG w);
int	m_BN_set_word(M_BIGNUM *a, m_BN_ULONG w);
m_BN_ULONG m_BN_get_word(const M_BIGNUM *a);

int	m_BN_cmp(const M_BIGNUM *a, const M_BIGNUM *b);
void	m_BN_free(M_BIGNUM *a);
int	m_BN_is_bit_set(const M_BIGNUM *a, int n);
int	m_BN_lshift(M_BIGNUM *r, const M_BIGNUM *a, int n);
int	m_BN_lshift1(M_BIGNUM *r, const M_BIGNUM *a);
int	m_BN_exp(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,m_BN_CTX *ctx);

int	m_BN_mod_exp(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	const M_BIGNUM *m,m_BN_CTX *ctx);
int	m_BN_mod_exp_mont(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	const M_BIGNUM *m, m_BN_CTX *ctx, m_BN_MONT_CTX *m_ctx);
int m_BN_mod_exp_mont_consttime(M_BIGNUM *rr, const M_BIGNUM *a, const M_BIGNUM *p,
	const M_BIGNUM *m, m_BN_CTX *ctx, m_BN_MONT_CTX *in_mont);
int	m_BN_mod_exp_mont_word(M_BIGNUM *r, m_BN_ULONG a, const M_BIGNUM *p,
	const M_BIGNUM *m, m_BN_CTX *ctx, m_BN_MONT_CTX *m_ctx);
int	m_BN_mod_exp2_mont(M_BIGNUM *r, const M_BIGNUM *a1, const M_BIGNUM *p1,
	const M_BIGNUM *a2, const M_BIGNUM *p2,const M_BIGNUM *m,
	m_BN_CTX *ctx,m_BN_MONT_CTX *m_ctx);
int	m_BN_mod_exp_simple(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	const M_BIGNUM *m,m_BN_CTX *ctx);

int	m_BN_mask_bits(M_BIGNUM *a,int n);
#ifndef OPENSSL_NO_FP_API
int	m_BN_print_fp(FILE *fp, const M_BIGNUM *a);
#endif
#ifdef HEADER_BIO_H
int	m_BN_print(BIO *fp, const M_BIGNUM *a);
#else
int	m_BN_print(void *fp, const M_BIGNUM *a);
#endif
int	m_BN_reciprocal(M_BIGNUM *r, const M_BIGNUM *m, int len, m_BN_CTX *ctx);
int	m_BN_rshift(M_BIGNUM *r, const M_BIGNUM *a, int n);
int	m_BN_rshift1(M_BIGNUM *r, const M_BIGNUM *a);
void	m_BN_clear(M_BIGNUM *a);
M_BIGNUM *m_BN_dup(const M_BIGNUM *a);
int	m_BN_ucmp(const M_BIGNUM *a, const M_BIGNUM *b);
int	m_BN_set_bit(M_BIGNUM *a, int n);
int	m_BN_clear_bit(M_BIGNUM *a, int n);
char *	m_BN_bn2hex(const M_BIGNUM *a);
char *	m_BN_bn2dec(const M_BIGNUM *a);
int 	m_BN_hex2bn(M_BIGNUM **a, const char *str);
int 	m_BN_dec2bn(M_BIGNUM **a, const char *str);
int	m_BN_asc2bn(M_BIGNUM **a, const char *str);
int	m_BN_gcd(M_BIGNUM *r,const M_BIGNUM *a,const M_BIGNUM *b,m_BN_CTX *ctx);
int	m_BN_kronecker(const M_BIGNUM *a,const M_BIGNUM *b,m_BN_CTX *ctx); /* returns -2 for error */
M_BIGNUM *m_BN_mod_inverse(M_BIGNUM *ret,
	const M_BIGNUM *a, const M_BIGNUM *n,m_BN_CTX *ctx);
M_BIGNUM *m_BN_mod_sqrt(M_BIGNUM *ret,
	const M_BIGNUM *a, const M_BIGNUM *n,m_BN_CTX *ctx);

/* Deprecated versions */
#ifndef OPENSSL_NO_DEPRECATED
M_BIGNUM *m_BN_generate_prime(M_BIGNUM *ret,int bits,int safe,
	const M_BIGNUM *add, const M_BIGNUM *rem,
	void (*callback)(int,int,void *),void *cb_arg);
int	m_BN_is_prime(const M_BIGNUM *p,int nchecks,
	void (*callback)(int,int,void *),
	m_BN_CTX *ctx,void *cb_arg);
int	m_BN_is_prime_fasttest(const M_BIGNUM *p,int nchecks,
	void (*callback)(int,int,void *),m_BN_CTX *ctx,void *cb_arg,
	int do_trial_division);
#endif /* !defined(OPENSSL_NO_DEPRECATED) */

/* Newer versions */
int	m_BN_generate_prime_ex(M_BIGNUM *ret,int bits,int safe, const M_BIGNUM *add,
		const M_BIGNUM *rem, m_BN_GENCB *cb);
int	m_BN_is_prime_ex(const M_BIGNUM *p,int nchecks, m_BN_CTX *ctx, m_BN_GENCB *cb);
int	m_BN_is_prime_fasttest_ex(const M_BIGNUM *p,int nchecks, m_BN_CTX *ctx,
		int do_trial_division, m_BN_GENCB *cb);

m_BN_MONT_CTX *m_BN_MONT_CTX_new(void );
void m_BN_MONT_CTX_init(m_BN_MONT_CTX *ctx);
int m_BN_mod_mul_montgomery(M_BIGNUM *r,const M_BIGNUM *a,const M_BIGNUM *b,
	m_BN_MONT_CTX *mont, m_BN_CTX *ctx);
#define m_BN_to_montgomery(r,a,mont,ctx)	m_BN_mod_mul_montgomery(\
	(r),(a),&((mont)->RR),(mont),(ctx))
int m_BN_from_montgomery(M_BIGNUM *r,const M_BIGNUM *a,
	m_BN_MONT_CTX *mont, m_BN_CTX *ctx);
void m_BN_MONT_CTX_free(m_BN_MONT_CTX *mont);
int m_BN_MONT_CTX_set(m_BN_MONT_CTX *mont,const M_BIGNUM *mod,m_BN_CTX *ctx);
m_BN_MONT_CTX *m_BN_MONT_CTX_copy(m_BN_MONT_CTX *to,m_BN_MONT_CTX *from);
m_BN_MONT_CTX *m_BN_MONT_CTX_set_locked(m_BN_MONT_CTX **pmont, int lock,
					const M_BIGNUM *mod, m_BN_CTX *ctx);

/* m_BN_BLINDING flags */
#define	m_BN_BLINDING_NO_UPDATE	0x00000001
#define	m_BN_BLINDING_NO_RECREATE	0x00000002

m_BN_BLINDING *m_BN_BLINDING_new(const M_BIGNUM *A, const M_BIGNUM *Ai, M_BIGNUM *mod);
void m_BN_BLINDING_free(m_BN_BLINDING *b);
int m_BN_BLINDING_update(m_BN_BLINDING *b,m_BN_CTX *ctx);
int m_BN_BLINDING_convert(M_BIGNUM *n, m_BN_BLINDING *b, m_BN_CTX *ctx);
int m_BN_BLINDING_invert(M_BIGNUM *n, m_BN_BLINDING *b, m_BN_CTX *ctx);
int m_BN_BLINDING_convert_ex(M_BIGNUM *n, M_BIGNUM *r, m_BN_BLINDING *b, m_BN_CTX *);
int m_BN_BLINDING_invert_ex(M_BIGNUM *n, const M_BIGNUM *r, m_BN_BLINDING *b, m_BN_CTX *);
#ifndef OPENSSL_NO_DEPRECATED
unsigned long m_BN_BLINDING_get_thread_id(const m_BN_BLINDING *);
void m_BN_BLINDING_set_thread_id(m_BN_BLINDING *, unsigned long);
#endif
unsigned long m_BN_BLINDING_get_flags(const m_BN_BLINDING *);
void m_BN_BLINDING_set_flags(m_BN_BLINDING *, unsigned long);
m_BN_BLINDING *m_BN_BLINDING_create_param(m_BN_BLINDING *b,
	const M_BIGNUM *e, M_BIGNUM *m, m_BN_CTX *ctx,
	int (*m_BN_mod_exp)(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
			  const M_BIGNUM *m, m_BN_CTX *ctx, m_BN_MONT_CTX *m_ctx),
	m_BN_MONT_CTX *m_ctx);

#ifndef OPENSSL_NO_DEPRECATED
void m_BN_set_params(int mul,int high,int low,int mont);
int m_BN_get_params(int which); /* 0, mul, 1 high, 2 low, 3 mont */
#endif

void	m_BN_RECP_CTX_init(m_BN_RECP_CTX *recp);
m_BN_RECP_CTX *m_BN_RECP_CTX_new(void);
void	m_BN_RECP_CTX_free(m_BN_RECP_CTX *recp);
int	m_BN_RECP_CTX_set(m_BN_RECP_CTX *recp,const M_BIGNUM *rdiv,m_BN_CTX *ctx);
int	m_BN_mod_mul_reciprocal(M_BIGNUM *r, const M_BIGNUM *x, const M_BIGNUM *y,
	m_BN_RECP_CTX *recp,m_BN_CTX *ctx);
int	m_BN_mod_exp_recp(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	const M_BIGNUM *m, m_BN_CTX *ctx);
int	m_BN_div_recp(M_BIGNUM *dv, M_BIGNUM *rem, const M_BIGNUM *m,
	m_BN_RECP_CTX *recp, m_BN_CTX *ctx);

/* Functions for arithmetic over binary polynomials represented by M_BIGNUMs. 
 *
 * The M_BIGNUM::neg property of M_BIGNUMs representing binary polynomials is
 * ignored.
 *
 * Note that input arguments are not const so that their bit arrays can
 * be expanded to the appropriate size if needed.
 */

int	m_BN_GF2m_add(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b); /*r = a + b*/
#define m_BN_GF2m_sub(r, a, b) m_BN_GF2m_add(r, a, b)
int	m_BN_GF2m_mod(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p); /*r=a mod p*/
int	m_BN_GF2m_mod_mul(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const M_BIGNUM *p, m_BN_CTX *ctx); /* r = (a * b) mod p */
int	m_BN_GF2m_mod_sqr(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	m_BN_CTX *ctx); /* r = (a * a) mod p */
int	m_BN_GF2m_mod_inv(M_BIGNUM *r, const M_BIGNUM *b, const M_BIGNUM *p,
	m_BN_CTX *ctx); /* r = (1 / b) mod p */
int	m_BN_GF2m_mod_div(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const M_BIGNUM *p, m_BN_CTX *ctx); /* r = (a / b) mod p */
int	m_BN_GF2m_mod_exp(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const M_BIGNUM *p, m_BN_CTX *ctx); /* r = (a ^ b) mod p */
int	m_BN_GF2m_mod_sqrt(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	m_BN_CTX *ctx); /* r = sqrt(a) mod p */
int	m_BN_GF2m_mod_solve_quad(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p,
	m_BN_CTX *ctx); /* r^2 + r = a mod p */
#define m_BN_GF2m_cmp(a, b) m_BN_ucmp((a), (b))
/* Some functions allow for representation of the irreducible polynomials
 * as an unsigned int[], say p.  The irreducible f(t) is then of the form:
 *     t^p[0] + t^p[1] + ... + t^p[k]
 * where m = p[0] > p[1] > ... > p[k] = 0.
 */
int	m_BN_GF2m_mod_arr(M_BIGNUM *r, const M_BIGNUM *a, const int p[]);
	/* r = a mod p */
int	m_BN_GF2m_mod_mul_arr(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const int p[], m_BN_CTX *ctx); /* r = (a * b) mod p */
int	m_BN_GF2m_mod_sqr_arr(M_BIGNUM *r, const M_BIGNUM *a, const int p[],
	m_BN_CTX *ctx); /* r = (a * a) mod p */
int	m_BN_GF2m_mod_inv_arr(M_BIGNUM *r, const M_BIGNUM *b, const int p[],
	m_BN_CTX *ctx); /* r = (1 / b) mod p */
int	m_BN_GF2m_mod_div_arr(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const int p[], m_BN_CTX *ctx); /* r = (a / b) mod p */
int	m_BN_GF2m_mod_exp_arr(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *b,
	const int p[], m_BN_CTX *ctx); /* r = (a ^ b) mod p */
int	m_BN_GF2m_mod_sqrt_arr(M_BIGNUM *r, const M_BIGNUM *a,
	const int p[], m_BN_CTX *ctx); /* r = sqrt(a) mod p */
int	m_BN_GF2m_mod_solve_quad_arr(M_BIGNUM *r, const M_BIGNUM *a,
	const int p[], m_BN_CTX *ctx); /* r^2 + r = a mod p */
int	m_BN_GF2m_poly2arr(const M_BIGNUM *a, int p[], int max);
int	m_BN_GF2m_arr2poly(const int p[], M_BIGNUM *a);

/* faster mod functions for the 'NIST primes' 
 * 0 <= a < p^2 */
int m_BN_nist_mod_192(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p, m_BN_CTX *ctx);
int m_BN_nist_mod_224(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p, m_BN_CTX *ctx);
int m_BN_nist_mod_256(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p, m_BN_CTX *ctx);
int m_BN_nist_mod_384(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p, m_BN_CTX *ctx);
int m_BN_nist_mod_521(M_BIGNUM *r, const M_BIGNUM *a, const M_BIGNUM *p, m_BN_CTX *ctx);

const M_BIGNUM *m_BN_get0_nist_prime_192(void);
const M_BIGNUM *m_BN_get0_nist_prime_224(void);
const M_BIGNUM *m_BN_get0_nist_prime_256(void);
const M_BIGNUM *m_BN_get0_nist_prime_384(void);
const M_BIGNUM *m_BN_get0_nist_prime_521(void);

/* library internal functions */

#define m_BN_expand(a,bits) ((((((bits+m_BN_BITS2-1))/m_BN_BITS2)) <= (a)->dmax)?\
	(a):m_BN_expand2((a),(bits+m_BN_BITS2-1)/m_BN_BITS2))
#define m_BN_wexpand(a,words) (((words) <= (a)->dmax)?(a):m_BN_expand2((a),(words)))
M_BIGNUM *m_BN_expand2(M_BIGNUM *a, int words);
#ifndef OPENSSL_NO_DEPRECATED
M_BIGNUM *m_BN_dup_expand(const M_BIGNUM *a, int words); /* unused */
#endif

/* M_BIGNUM consistency macros
 * There is one "API" macro, m_BN_fix_top(), for stripping leading zeroes from
 * M_BIGNUM data after direct manipulations on the data. There is also an
 * "internal" macro, m_BN_check_top(), for verifying that there are no leading
 * zeroes. Unfortunately, some auditing is required due to the fact that
 * m_BN_fix_top() has become an overabused duct-tape because M_BIGNUM data is
 * occasionally passed around in an inconsistent state. So the following
 * changes have been made to sort this out;
 * - m_BN_fix_top()s implementation has been moved to m_BN_correct_top()
 * - if m_BN_DEBUG isn't defined, m_BN_fix_top() maps to m_BN_correct_top(), and
 *   m_BN_check_top() is as before.
 * - if m_BN_DEBUG *is* defined;
 *   - m_BN_check_top() tries to pollute unused words even if the M_BIGNUM 'top' is
 *     consistent. (ed: only if m_BN_DEBUG_RAND is defined)
 *   - m_BN_fix_top() maps to m_BN_check_top() rather than "fixing" anything.
 * The idea is to have debug builds flag up inconsistent M_BIGNUMs when they
 * occur. If that occurs in a m_BN_fix_top(), we examine the code in question; if
 * the use of m_BN_fix_top() was appropriate (ie. it follows directly after code
 * that manipulates the M_BIGNUM) it is converted to m_BN_correct_top(), and if it
 * was not appropriate, we convert it permanently to m_BN_check_top() and track
 * down the cause of the bug. Eventually, no internal code should be using the
 * m_BN_fix_top() macro. External applications and libraries should try this with
 * their own code too, both in terms of building against the openssl headers
 * with m_BN_DEBUG defined *and* linking with a version of OpenSSL built with it
 * defined. This not only improves external code, it provides more test
 * coverage for openssl's own code.
 */

#ifdef m_BN_DEBUG

/* We only need assert() when debugging */
#include <assert.h>

#ifdef m_BN_DEBUG_RAND
/* To avoid "make update" cvs wars due to m_BN_DEBUG, use some tricks */
#ifndef RAND_pseudo_bytes
int RAND_pseudo_bytes(unsigned char *buf,int num);
#define m_BN_DEBUG_TRIX
#endif
#define m_BN_pollute(a) \
	do { \
		const M_BIGNUM *_bnum1 = (a); \
		if(_bnum1->top < _bnum1->dmax) { \
			unsigned char _tmp_char; \
			/* We cast away const without the compiler knowing, any \
			 * *genuinely* constant variables that aren't mutable \
			 * wouldn't be constructed with top!=dmax. */ \
			m_BN_ULONG *_not_const; \
			memcpy(&_not_const, &_bnum1->d, sizeof(m_BN_ULONG*)); \
			RAND_pseudo_bytes(&_tmp_char, 1); \
			memset((unsigned char *)(_not_const + _bnum1->top), _tmp_char, \
				(_bnum1->dmax - _bnum1->top) * sizeof(m_BN_ULONG)); \
		} \
	} while(0)
#ifdef m_BN_DEBUG_TRIX
#undef RAND_pseudo_bytes
#endif
#else
#define m_BN_pollute(a)
#endif
#define m_BN_check_top(a) \
	do { \
		const M_BIGNUM *_bnum2 = (a); \
		if (_bnum2 != NULL) { \
			assert((_bnum2->top == 0) || \
				(_bnum2->d[_bnum2->top - 1] != 0)); \
			m_BN_pollute(_bnum2); \
		} \
	} while(0)

#define m_BN_fix_top(a)		m_BN_check_top(a)

#else /* !m_BN_DEBUG */

#define m_BN_pollute(a)
#define m_BN_check_top(a)
#define m_BN_fix_top(a)		m_BN_correct_top(a)

#endif

#define m_BN_correct_top(a) \
        { \
        m_BN_ULONG *ftl; \
	int tmp_top = (a)->top; \
	if (tmp_top > 0) \
		{ \
		for (ftl= &((a)->d[tmp_top-1]); tmp_top > 0; tmp_top--) \
			if (*(ftl--)) break; \
		(a)->top = tmp_top; \
		} \
	m_BN_pollute(a); \
	}

m_BN_ULONG m_BN_mul_add_words(m_BN_ULONG *rp, const m_BN_ULONG *ap, int num, m_BN_ULONG w);
m_BN_ULONG m_BN_mul_words(m_BN_ULONG *rp, const m_BN_ULONG *ap, int num, m_BN_ULONG w);
void     m_BN_sqr_words(m_BN_ULONG *rp, const m_BN_ULONG *ap, int num);
m_BN_ULONG m_BN_div_words(m_BN_ULONG h, m_BN_ULONG l, m_BN_ULONG d);
m_BN_ULONG m_BN_add_words(m_BN_ULONG *rp, const m_BN_ULONG *ap, const m_BN_ULONG *bp,int num);
m_BN_ULONG m_BN_sub_words(m_BN_ULONG *rp, const m_BN_ULONG *ap, const m_BN_ULONG *bp,int num);

/* Primes from RFC 2409 */
M_BIGNUM *get_rfc2409_prime_768(M_BIGNUM *bn);
M_BIGNUM *get_rfc2409_prime_1024(M_BIGNUM *bn);

/* Primes from RFC 3526 */
M_BIGNUM *get_rfc3526_prime_1536(M_BIGNUM *bn);
M_BIGNUM *get_rfc3526_prime_2048(M_BIGNUM *bn);
M_BIGNUM *get_rfc3526_prime_3072(M_BIGNUM *bn);
M_BIGNUM *get_rfc3526_prime_4096(M_BIGNUM *bn);
M_BIGNUM *get_rfc3526_prime_6144(M_BIGNUM *bn);
M_BIGNUM *get_rfc3526_prime_8192(M_BIGNUM *bn);

int m_BN_bntest_rand(M_BIGNUM *rnd, int bits, int top,int bottom);

/* BEGIN ERROR CODES */
/* The following lines are auto generated by the script mkerr.pl. Any changes
 * made after this point may be overwritten when the script is next run.
 */
void ERR_load_m_BN_strings(void);

/* Error codes for the BN functions. */

/* Function codes. */
#define m_BN_F_BNRAND					 127
#define m_BN_F_m_BN_BLINDING_CONVERT_EX			 100
#define m_BN_F_m_BN_BLINDING_CREATE_PARAM			 128
#define m_BN_F_m_BN_BLINDING_INVERT_EX			 101
#define m_BN_F_m_BN_BLINDING_NEW				 102
#define m_BN_F_m_BN_BLINDING_UPDATE				 103
#define m_BN_F_m_BN_BN2DEC					 104
#define m_BN_F_m_BN_BN2HEX					 105
#define m_BN_F_m_BN_CTX_GET					 116
#define m_BN_F_m_BN_CTX_NEW					 106
#define m_BN_F_m_BN_CTX_START				 129
#define m_BN_F_m_BN_DIV					 107
#define m_BN_F_m_BN_DIV_NO_BRANCH				 138
#define m_BN_F_m_BN_DIV_RECP				 130
#define m_BN_F_m_BN_EXP					 123
#define m_BN_F_m_BN_EXPAND2					 108
#define m_BN_F_m_BN_EXPAND_INTERNAL				 120
#define m_BN_F_m_BN_GF2M_MOD				 131
#define m_BN_F_m_BN_GF2M_MOD_EXP				 132
#define m_BN_F_m_BN_GF2M_MOD_MUL				 133
#define m_BN_F_m_BN_GF2M_MOD_SOLVE_QUAD			 134
#define m_BN_F_m_BN_GF2M_MOD_SOLVE_QUAD_ARR			 135
#define m_BN_F_m_BN_GF2M_MOD_SQR				 136
#define m_BN_F_m_BN_GF2M_MOD_SQRT				 137
#define m_BN_F_m_BN_MOD_EXP2_MONT				 118
#define m_BN_F_m_BN_MOD_EXP_MONT				 109
#define m_BN_F_m_BN_MOD_EXP_MONT_CONSTTIME			 124
#define m_BN_F_m_BN_MOD_EXP_MONT_WORD			 117
#define m_BN_F_m_BN_MOD_EXP_RECP				 125
#define m_BN_F_m_BN_MOD_EXP_SIMPLE				 126
#define m_BN_F_m_BN_MOD_INVERSE				 110
#define m_BN_F_m_BN_MOD_INVERSE_NO_BRANCH			 139
#define m_BN_F_m_BN_MOD_LSHIFT_QUICK			 119
#define m_BN_F_m_BN_MOD_MUL_RECIPROCAL			 111
#define m_BN_F_m_BN_MOD_SQRT				 121
#define m_BN_F_m_BN_MPI2BN					 112
#define m_BN_F_m_BN_NEW					 113
#define m_BN_F_m_BN_RAND					 114
#define m_BN_F_m_BN_RAND_RANGE				 122
#define m_BN_F_m_BN_USUB					 115

/* Reason codes. */
#define m_BN_R_ARG2_LT_ARG3				 100
#define m_BN_R_BAD_RECIPROCAL				 101
#define m_BN_R_M_BIGNUM_TOO_LONG				 114
#define m_BN_R_CALLED_WITH_EVEN_MODULUS			 102
#define m_BN_R_DIV_BY_ZERO				 103
#define m_BN_R_ENCODING_ERROR				 104
#define m_BN_R_EXPAND_ON_STATIC_M_BIGNUM_DATA		 105
#define m_BN_R_INPUT_NOT_REDUCED				 110
#define m_BN_R_INVALID_LENGTH				 106
#define m_BN_R_INVALID_RANGE				 115
#define m_BN_R_NOT_A_SQUARE				 111
#define m_BN_R_NOT_INITIALIZED				 107
#define m_BN_R_NO_INVERSE					 108
#define m_BN_R_NO_SOLUTION				 116
#define m_BN_R_P_IS_NOT_PRIME				 112
#define m_BN_R_TOO_MANY_ITERATIONS			 113
#define m_BN_R_TOO_MANY_TEMPORARY_VARIABLES		 109

#ifdef  __cplusplus
}
#endif
#endif
