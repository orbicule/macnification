//
//  main.m
//  rsa_verify_2
//
//  Created by Dennis Lorson on 22/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#include <openssl/bio.h> 
#include <openssl/pem.h> 
#include <openssl/err.h> 
#import "sha2.h"



NSString *macID()
{
    CFArrayRef interfaces = (CFArrayRef)[(id)SCNetworkInterfaceCopyAll() autorelease];
    
    for (int i = 0; i < CFArrayGetCount(interfaces); i++) {
        SCNetworkInterfaceRef interface = CFArrayGetValueAtIndex(interfaces, i);
        
        if (!CFEqual(SCNetworkInterfaceGetBSDName(interface), CFSTR("en0")))
            continue;
        
        NSString *macID = (NSString *)SCNetworkInterfaceGetHardwareAddressString(interface);
        
        return macID;
    }
    
    return nil;
}


NSData *signature()
{

}

NSString *b64Encode(NSData *original)
{
    // Construct an OpenSSL context
    BIO *context = BIO_new(BIO_s_mem());
    
    // Tell the context to encode base64
    BIO *command = BIO_new(BIO_f_base64());
    BIO_set_flags(command, BIO_FLAGS_BASE64_NO_NL);
    context = BIO_push(command, context);
    
    // Encode all the data
    BIO_write(context, [original bytes], [original length]);
    int i = BIO_flush(context);
    i = i;
    
    // Get the data out of the context
    char *outputBuffer;
    long outputLength = BIO_get_mem_data(context, &outputBuffer);
    NSString *encodedString = [NSString stringWithCString:outputBuffer length:outputLength];
    
    BIO_free_all(context);
    
    return encodedString;
}

NSData *b64Decode(NSString *decode)
{
    NSData *data = [decode dataUsingEncoding:NSASCIIStringEncoding];
    
    // Construct an OpenSSL context
    BIO *command = BIO_new(BIO_f_base64());
    BIO_set_flags(command, BIO_FLAGS_BASE64_NO_NL);
    BIO *context = BIO_new_mem_buf((void *)[data bytes], [data length]);
    
    // Tell the context to encode base64
    context = BIO_push(command, context);
    
    // Encode all the data
    NSMutableData *outputData = [NSMutableData data];
    
#define BUFFSIZE__ 256
    int len = 0;
    char inbuf[BUFFSIZE__];
    while ((len = BIO_read(context, inbuf, BUFFSIZE__)) > 0)
    {
        [outputData appendBytes:inbuf length:len];
    }
    
    BIO_free_all(context);
    [data self]; // extend GC lifetime of data to here
    
    return outputData;
}




int main (int argc, const char * argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    SHA512_CTX ctx;
    SHA512_Init(&ctx);
    
    
    NSString *mac = macID();
    
    NSData *data = [mac dataUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char md[64];
    
    SHA512_Update(&ctx, [data bytes], [data length]);
    SHA512_Final(md, &ctx);
    
    
    
    
    for (int i = 0; i < 64; i++)
        printf("%x-", md[i]);
    
    [pool release];
    
    return 0;
}
