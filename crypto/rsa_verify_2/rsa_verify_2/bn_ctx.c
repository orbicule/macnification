/* crypto/bn/m_BN_ctx.c */
/* Written by Ulf Moeller for the OpenSSL project. */
/* ====================================================================
 * Copyright (c) 1998-2004 The OpenSSL Project.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
 *
 * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission. For written permission, please contact
 *    openssl-core@openssl.org.
 *
 * 5. Products derived from this software may not be called "OpenSSL"
 *    nor may "OpenSSL" appear in their names without prior written
 *    permission of the OpenSSL Project.
 *
 * 6. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
 *
 * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */

#if !defined(m_BN_CTX_DEBUG) && !defined(m_BN_DEBUG)
#ifndef NDEBUG
#define NDEBUG
#endif
#endif

#include <stdio.h>
#include <assert.h>


#include "BN_lcl.h"

/* TODO list
 *
 * 1. Check a bunch of "(words+1)" type hacks in various M_BIGNUM functions and
 * check they can be safely removed.
 *  - Check +1 and other ugliness in m_BN_from_montgomery()
 *
 * 2. Consider allowing a m_BN_new_ex() that, at least, lets you specify an
 * appropriate 'block' size that will be honoured by m_BN_expand_internal() to
 * prevent piddly little reallocations. OTOH, profiling M_BIGNUM expansions in
 * m_BN_CTX doesn't show this to be a big issue.
 */

/* How many M_BIGNUMs are in each "pool item"; */
#define m_BN_CTX_POOL_SIZE	16
/* The stack frame info is resizing, set a first-time expansion size; */
#define m_BN_CTX_START_FRAMES	32

/***********/
/* m_BN_POOL */
/***********/

/* A bundle of M_BIGNUMs that can be linked with other bundles */
typedef struct M_BIGNUM_pool_item
	{
	/* The M_BIGNUM values */
	M_BIGNUM vals[m_BN_CTX_POOL_SIZE];
	/* Linked-list admin */
	struct M_BIGNUM_pool_item *prev, *next;
	} m_BN_POOL_ITEM;
/* A linked-list of M_BIGNUMs grouped in bundles */
typedef struct M_BIGNUM_pool
	{
	/* Linked-list admin */
	m_BN_POOL_ITEM *head, *current, *tail;
	/* Stack depth and allocation size */
	unsigned used, size;
	} m_BN_POOL;
static void		m_BN_POOL_init(m_BN_POOL *);
static void		m_BN_POOL_finish(m_BN_POOL *);
#ifndef OPENSSL_NO_DEPRECATED
static void		m_BN_POOL_reset(m_BN_POOL *);
#endif
static M_BIGNUM *		m_BN_POOL_get(m_BN_POOL *);
static void		m_BN_POOL_release(m_BN_POOL *, unsigned int);

/************/
/* m_BN_STACK */
/************/

/* A wrapper to manage the "stack frames" */
typedef struct M_BIGNUM_ctx_stack
	{
	/* Array of indexes into the M_BIGNUM stack */
	unsigned int *indexes;
	/* Number of stack frames, and the size of the allocated array */
	unsigned int depth, size;
	} m_BN_STACK;
static void		m_BN_STACK_init(m_BN_STACK *);
static void		m_BN_STACK_finish(m_BN_STACK *);
#ifndef OPENSSL_NO_DEPRECATED
static void		m_BN_STACK_reset(m_BN_STACK *);
#endif
static int		m_BN_STACK_push(m_BN_STACK *, unsigned int);
static unsigned int	m_BN_STACK_pop(m_BN_STACK *);

/**********/
/* m_BN_CTX */
/**********/

/* The opaque m_BN_CTX type */
struct M_BIGNUM_ctx
	{
	/* The M_BIGNUM bundles */
	m_BN_POOL pool;
	/* The "stack frames", if you will */
	m_BN_STACK stack;
	/* The number of M_BIGNUMs currently assigned */
	unsigned int used;
	/* Depth of stack overflow */
	int err_stack;
	/* Block "gets" until an "end" (compatibility behaviour) */
	int too_many;
	};

/* Enable this to find m_BN_CTX bugs */
#ifdef m_BN_CTX_DEBUG
static const char *ctxdbg_cur = NULL;
static void ctxdbg(m_BN_CTX *ctx)
	{
	unsigned int bnidx = 0, fpidx = 0;
	m_BN_POOL_ITEM *item = ctx->pool.head;
	m_BN_STACK *stack = &ctx->stack;
	fprintf(stderr,"(%08x): ", (unsigned int)ctx);
	while(bnidx < ctx->used)
		{
		fprintf(stderr,"%03x ", item->vals[bnidx++ % m_BN_CTX_POOL_SIZE].dmax);
		if(!(bnidx % m_BN_CTX_POOL_SIZE))
			item = item->next;
		}
	fprintf(stderr,"\n");
	bnidx = 0;
	fprintf(stderr,"          : ");
	while(fpidx < stack->depth)
		{
		while(bnidx++ < stack->indexes[fpidx])
			fprintf(stderr,"    ");
		fprintf(stderr,"^^^ ");
		bnidx++;
		fpidx++;
		}
	fprintf(stderr,"\n");
	}
#define CTXDBG_ENTRY(str, ctx)	do { \
				ctxdbg_cur = (str); \
				fprintf(stderr,"Starting %s\n", ctxdbg_cur); \
				ctxdbg(ctx); \
				} while(0)
#define CTXDBG_EXIT(ctx)	do { \
				fprintf(stderr,"Ending %s\n", ctxdbg_cur); \
				ctxdbg(ctx); \
				} while(0)
#define CTXDBG_RET(ctx,ret)
#else
#define CTXDBG_ENTRY(str, ctx)
#define CTXDBG_EXIT(ctx)
#define CTXDBG_RET(ctx,ret)
#endif

/* This function is an evil legacy and should not be used. This implementation
 * is WYSIWYG, though I've done my best. */
#ifndef OPENSSL_NO_DEPRECATED
void m_BN_CTX_init(m_BN_CTX *ctx)
	{
	/* Assume the caller obtained the context via m_BN_CTX_new() and so is
	 * trying to reset it for use. Nothing else makes sense, least of all
	 * binary compatibility from a time when they could declare a static
	 * variable. */
	m_BN_POOL_reset(&ctx->pool);
	m_BN_STACK_reset(&ctx->stack);
	ctx->used = 0;
	ctx->err_stack = 0;
	ctx->too_many = 0;
	}
#endif

m_BN_CTX *m_BN_CTX_new(void)
	{
	m_BN_CTX *ret = malloc(sizeof(m_BN_CTX));
	if(!ret)
		{
		return NULL;
		}
	/* Initialise the structure */
	m_BN_POOL_init(&ret->pool);
	m_BN_STACK_init(&ret->stack);
	ret->used = 0;
	ret->err_stack = 0;
	ret->too_many = 0;
	return ret;
	}

void m_BN_CTX_free(m_BN_CTX *ctx)
	{
	if (ctx == NULL)
		return;
#ifdef m_BN_CTX_DEBUG
	{
	m_BN_POOL_ITEM *pool = ctx->pool.head;
	fprintf(stderr,"m_BN_CTX_free, stack-size=%d, pool-M_BIGNUMs=%d\n",
		ctx->stack.size, ctx->pool.size);
	fprintf(stderr,"dmaxs: ");
	while(pool) {
		unsigned loop = 0;
		while(loop < m_BN_CTX_POOL_SIZE)
			fprintf(stderr,"%02x ", pool->vals[loop++].dmax);
		pool = pool->next;
	}
	fprintf(stderr,"\n");
	}
#endif
	m_BN_STACK_finish(&ctx->stack);
	m_BN_POOL_finish(&ctx->pool);
	free(ctx);
	}

void m_BN_CTX_start(m_BN_CTX *ctx)
	{
	CTXDBG_ENTRY("m_BN_CTX_start", ctx);
	/* If we're already overflowing ... */
	if(ctx->err_stack || ctx->too_many)
		ctx->err_stack++;
	/* (Try to) get a new frame pointer */
	else if(!m_BN_STACK_push(&ctx->stack, ctx->used))
		{
		//BNerr(m_BN_F_m_BN_CTX_START,m_BN_R_TOO_MANY_TEMPORARY_VARIABLES);
		ctx->err_stack++;
		}
	CTXDBG_EXIT(ctx);
	}

void m_BN_CTX_end(m_BN_CTX *ctx)
	{
	CTXDBG_ENTRY("m_BN_CTX_end", ctx);
	if(ctx->err_stack)
		ctx->err_stack--;
	else
		{
		unsigned int fp = m_BN_STACK_pop(&ctx->stack);
		/* Does this stack frame have anything to release? */
		if(fp < ctx->used)
			m_BN_POOL_release(&ctx->pool, ctx->used - fp);
		ctx->used = fp;
		/* Unjam "too_many" in case "get" had failed */
		ctx->too_many = 0;
		}
	CTXDBG_EXIT(ctx);
	}

M_BIGNUM *m_BN_CTX_get(m_BN_CTX *ctx)
	{
	M_BIGNUM *ret;
	CTXDBG_ENTRY("m_BN_CTX_get", ctx);
	if(ctx->err_stack || ctx->too_many) return NULL;
	if((ret = m_BN_POOL_get(&ctx->pool)) == NULL)
		{
		/* Setting too_many prevents repeated "get" attempts from
		 * cluttering the error stack. */
		ctx->too_many = 1;
		//BNerr(m_BN_F_m_BN_CTX_GET,m_BN_R_TOO_MANY_TEMPORARY_VARIABLES);
		return NULL;
		}
	/* OK, make sure the returned M_BIGNUM is "zero" */
	m_BN_zero(ret);
	ctx->used++;
	CTXDBG_RET(ctx, ret);
	return ret;
	}

/************/
/* m_BN_STACK */
/************/

static void m_BN_STACK_init(m_BN_STACK *st)
	{
	st->indexes = NULL;
	st->depth = st->size = 0;
	}

static void m_BN_STACK_finish(m_BN_STACK *st)
	{
	if(st->size) free(st->indexes);
	}

#ifndef OPENSSL_NO_DEPRECATED
static void m_BN_STACK_reset(m_BN_STACK *st)
	{
	st->depth = 0;
	}
#endif

static int m_BN_STACK_push(m_BN_STACK *st, unsigned int idx)
	{
	if(st->depth == st->size)
		/* Need to expand */
		{
		unsigned int newsize = (st->size ?
				(st->size * 3 / 2) : m_BN_CTX_START_FRAMES);
		unsigned int *newitems = malloc(newsize *
						sizeof(unsigned int));
		if(!newitems) return 0;
		if(st->depth)
			memcpy(newitems, st->indexes, st->depth *
						sizeof(unsigned int));
		if(st->size) free(st->indexes);
		st->indexes = newitems;
		st->size = newsize;
		}
	st->indexes[(st->depth)++] = idx;
	return 1;
	}

static unsigned int m_BN_STACK_pop(m_BN_STACK *st)
	{
	return st->indexes[--(st->depth)];
	}

/***********/
/* m_BN_POOL */
/***********/

static void m_BN_POOL_init(m_BN_POOL *p)
	{
	p->head = p->current = p->tail = NULL;
	p->used = p->size = 0;
	}

static void m_BN_POOL_finish(m_BN_POOL *p)
	{
	while(p->head)
		{
		unsigned int loop = 0;
		M_BIGNUM *bn = p->head->vals;
		while(loop++ < m_BN_CTX_POOL_SIZE)
			{
			if(bn->d) m_BN_clear_free(bn);
			bn++;
			}
		p->current = p->head->next;
		free(p->head);
		p->head = p->current;
		}
	}

#ifndef OPENSSL_NO_DEPRECATED
static void m_BN_POOL_reset(m_BN_POOL *p)
	{
	m_BN_POOL_ITEM *item = p->head;
	while(item)
		{
		unsigned int loop = 0;
		M_BIGNUM *bn = item->vals;
		while(loop++ < m_BN_CTX_POOL_SIZE)
			{
			if(bn->d) m_BN_clear(bn);
			bn++;
			}
		item = item->next;
		}
	p->current = p->head;
	p->used = 0;
	}
#endif

static M_BIGNUM *m_BN_POOL_get(m_BN_POOL *p)
	{
	if(p->used == p->size)
		{
		M_BIGNUM *bn;
		unsigned int loop = 0;
		m_BN_POOL_ITEM *item = malloc(sizeof(m_BN_POOL_ITEM));
		if(!item) return NULL;
		/* Initialise the structure */
		bn = item->vals;
		while(loop++ < m_BN_CTX_POOL_SIZE)
			m_BN_init(bn++);
		item->prev = p->tail;
		item->next = NULL;
		/* Link it in */
		if(!p->head)
			p->head = p->current = p->tail = item;
		else
			{
			p->tail->next = item;
			p->tail = item;
			p->current = item;
			}
		p->size += m_BN_CTX_POOL_SIZE;
		p->used++;
		/* Return the first M_BIGNUM from the new pool */
		return item->vals;
		}
	if(!p->used)
		p->current = p->head;
	else if((p->used % m_BN_CTX_POOL_SIZE) == 0)
		p->current = p->current->next;
	return p->current->vals + ((p->used++) % m_BN_CTX_POOL_SIZE);
	}

static void m_BN_POOL_release(m_BN_POOL *p, unsigned int num)
	{
	unsigned int offset = (p->used - 1) % m_BN_CTX_POOL_SIZE;
	p->used -= num;
	while(num--)
		{
		m_BN_check_top(p->current->vals + offset);
		if(!offset)
			{
			offset = m_BN_CTX_POOL_SIZE - 1;
			p->current = p->current->prev;
			}
		else
			offset--;
		}
	}

