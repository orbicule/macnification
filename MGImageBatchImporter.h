//
//  ImageImporter.h
//  Macnification
//
//  Created by Peter Schols on 30/10/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@class Album;
@class Image;
@class MGImageBatchImporter;

@protocol MGImageBatchImporterDelegate <NSObject>

- (void)imageImporter:(MGImageBatchImporter *)importer didImportImages:(NSArray *)images;

@end


@interface MGImageBatchImporter : NSObject
{	
    
    id <MGImageBatchImporterDelegate> delegate_;
	
	BOOL splitChannels;
	
	BOOL showsProgressSheet_;
			
	Album *album_;
	Album *libraryAlbum_;
	Album *lastImportAlbum_;
	Album *projectsAlbum_;
	
	// File level counter
	int currentImageFileIndex_;
	int numberOfImageFilesToImport_;
	NSArray *imageFileSizes_;
	NSArray *cumulativeImageFileSizes_;
	unsigned long long totalImageFileSize_;
	
	// Image rep level counter
	int currentImageIndex_;
	int numberOfImagesToImport_;
	
}

@property (readwrite, assign) id <MGImageBatchImporterDelegate> delegate;

@property (readwrite) BOOL showsProgressSheet;

@property (readwrite, retain) Album *album;
@property (readwrite, retain) Album *libraryAlbum;
@property (readwrite, retain) Album *lastImportAlbum;
@property (readwrite, retain) Album *projectsAlbum;

@property(readwrite) BOOL splitChannels;


// Importing images
- (void)importImageFilesAndDirectories:(NSArray *)files;
+ (NSArray *)supportedFileTypes;





@end
