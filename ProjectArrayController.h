//
//  ProjectArrayController.h
//  Filament
//
//  Created by Peter Schols on 20/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SourceListTreeController;

@interface ProjectArrayController : NSArrayController {
	IBOutlet SourceListTreeController *sourceListTreeController;
}


- (IBAction)switchToClickedCollection:(id)sender;

@end
