//
//  MGHUDScrollView.m
//  Filament
//
//  Created by Dennis Lorson on 27/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGHUDScrollView.h"

#import "MGHUDScroller.h"

@implementation MGHUDScrollView



//--------------------------------------------------------------//
#pragma mark -- Initialize --
//--------------------------------------------------------------//

- (void)_init
{
    // Configure itself
    NSScroller*     scroller;
    MGHUDScroller*  blkScroller;
    if ([self hasHorizontalScroller]) {
        scroller = [self horizontalScroller];
        if (scroller) {
            blkScroller = [[MGHUDScroller alloc] initWithFrame:[scroller frame]];
            [blkScroller setArrowsPosition:NSScrollerArrowsMaxEnd];
            [self setHorizontalScroller:blkScroller];
            [blkScroller release];
        }
    }
    if ([self hasVerticalScroller]) {
        scroller = [self verticalScroller];
        if (scroller) {
            blkScroller = [[MGHUDScroller alloc] initWithFrame:[scroller frame]];
            [blkScroller setArrowsPosition:NSScrollerArrowsMaxEnd];
			[blkScroller setControlSize:NSRegularControlSize];
            [self setVerticalScroller:blkScroller];
			NSRect frame = [[self verticalScroller] frame];
			frame.size.width -= 1;
			[[self verticalScroller] setFrame:frame];
            [blkScroller release];
        }
    }
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    // Common init
    [self _init];
    
    return self;
}

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (!self) {
        return nil;
    }
    
    // Common init
    [self _init];
    
    return self;
}


+ (Class)_verticalScrollerClass;
{
	return [MGHUDScroller class];

	
	
}

//--------------------------------------------------------------//
#pragma mark -- Drawing --
//--------------------------------------------------------------//

- (void)drawRect:(NSRect)rect
{
    // Get bounds
    NSRect  bounds;
    bounds = [self bounds];
    
    // Draw grid
    [[NSColor colorWithCalibratedWhite:0.4 alpha:1.0] set];
    
    NSRect  gridRect;
    gridRect.origin.x = bounds.origin.x + 1;
    gridRect.origin.y = bounds.origin.y;
    gridRect.size.width = bounds.size.width - 2;
    gridRect.size.height = 1;
    NSFrameRect(gridRect);
    
	[[NSColor colorWithCalibratedWhite:0.4 alpha:1.0] set];

	
    gridRect.origin.x = bounds.origin.x + 1;
    gridRect.origin.y = bounds.origin.y + bounds.size.height - 1;
    gridRect.size.width = bounds.size.width - 2;
    gridRect.size.height = 1;
    NSFrameRect(gridRect);
}



@end
