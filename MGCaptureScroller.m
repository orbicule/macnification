//
//  MGCaptureScroller.m
//  Filament
//
//  Created by Dennis Lorson on 13/04/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGCaptureScroller.h"


enum {
    NSScrollerPartWhole = 0, 
    NSScrollerPartUpperExtra = 1, 
    NSScrollerPartKnob = 2, 
    NSScrollerPartLowerExtra = 3, 
    NSScrollerPartUpArrow = 4, 
    NSScrollerPartDownArrow = 5, 
    NSScrollerPartWithoutArrow = 6, 
};


@implementation MGCaptureScroller

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}




- (void)drawArrow:(NSScrollerArrow)arrow highlightPart:(NSInteger)part
{
    [self drawKnobSlotInRect:[self rectForPart:NSScrollerIncrementLine] highlight:NO];
    [self drawKnobSlotInRect:[self rectForPart:NSScrollerDecrementLine] highlight:NO];

}

- (void)drawKnob
{    
    NSRect rect = [self rectForPart:NSScrollerPartKnob];
    
    rect = NSInsetRect(rect, 0, 2);
    rect = NSOffsetRect(rect, 0, 0);
    
    
    [[NSGraphicsContext currentContext] saveGraphicsState];
    
    NSAffineTransform *trafo = [NSAffineTransform transform];
    [trafo scaleXBy:1.0 yBy:-1.0];
    [trafo translateXBy:0.0 yBy:-[self bounds].size.height];
    [trafo concat];
    
    NSDrawThreePartImage(rect, [NSImage imageNamed:@"captureImageBrowserScrollerKnobL"], [NSImage imageNamed:@"captureImageBrowserScrollerKnobM"], [NSImage imageNamed:@"captureImageBrowserScrollerKnobR"], NO, NSCompositeSourceOver, 1.0, NO);
    [[NSGraphicsContext currentContext] restoreGraphicsState];

}

- (void)drawKnobSlotInRect:(NSRect)rect highlight:(BOOL)highlight
{	
    [[NSGraphicsContext currentContext] saveGraphicsState];
    
    NSAffineTransform *trafo = [NSAffineTransform transform];
    [trafo scaleXBy:1.0 yBy:-1.0];
    [trafo translateXBy:0.0 yBy:-[self bounds].size.height];
    [trafo concat];
    
    [[NSImage imageNamed:@"captureImageBrowserScrollerBackground"] drawInRect:rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    
    [[NSGraphicsContext currentContext] restoreGraphicsState];
}



@end
