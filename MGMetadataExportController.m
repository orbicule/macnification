//
//  MGMetadataExportController.m
//  Filament
//
//  Created by Dennis Lorson on 08/10/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGMetadataExportController.h"
#import "MGFilterAndSortMetadataController.h"
#import "Image.h"
#import "Stack.h"
#import "ROI.h"

#define METADATA_EXPORT_DRAG_TYPE @"METADATA_EXPORT_DRAG_TYPE"


#define LARGE_TEXT_CONTAINER_SIZE 1e6

#define MAX_PLAIN_PREVIEW_ENTRIES	50
#define MAX_TABLE_PREVIEW_ENTRIES	200


@interface MGMetadataExportController ()

- (void)_setContentType:(MGMetadataExportContent)contentType;
- (void)_setStyle:(MGMetadataExportStyle)style;

- (NSArray *)_sortedImages;
- (NSArray *)_sortedROIs;
- (NSArray *)_sortedSelectedAttributes;
- (void)_reloadAttributes;
- (NSArray *)_expandStacks:(NSArray *)original;

- (NSDictionary *)_generalMetadataFields;
- (NSDictionary *)_roiMetadataFields;

- (NSAttributedString *)_RTFStringForPreview:(BOOL)previewOnly;
- (NSString *)_TDFStringForGeneralMetadataPreview:(BOOL)previewOnly;
- (NSString *)_TDFStringForROIMetadataPreview:(BOOL)previewOnly;
- (void)_updatePreview;
- (NSString *)_stringForAttributeValue:(id)value;
- (NSString *)_stringByEllipsingOrPaddingString:(NSString *)original length:(int)length;
- (BOOL)_showsStacksAsOneEntry;

- (NSString *)_separator;

- (NSArray *)_preferenceAttributesForContent:(MGMetadataExportContent)content;
- (NSArray *)_preferenceSelectedAttributesForContent:(MGMetadataExportContent)content;

- (NSArray *)_attributeArraySortedWithPreferencesForContent:(MGMetadataExportContent)content original:(NSArray *)original;
- (void)_saveAttrsToPrefs;

- (NSArray *)_defaultGeneralFieldOrder;
- (NSArray *)_defaultROIFieldOrder;



@end

// --------------------------------------------------------------------------------------------------------------


@implementation MGMetadataExportController

#pragma mark -
#pragma mark Public


- (id) init
{
	self = [super init];
	if (self != nil) {
		sortedAttributes_ = [[NSMutableArray array] retain];
		selectedAttributes_ = [[NSMutableArray array] retain];
		
		[NSBundle loadNibNamed:@"MetadataExport" owner:self];
	}
		
	return self;
}

- (void) dealloc
{	
	[sortedAttributes_ release];
	[selectedAttributes_ release];
	[humanReadableAttributeNames_ release];
	[images_ release];
	//[textStorage_ release];

	[super dealloc];
}



- (void)setImages:(NSArray *)images;
{
	if (images_ != images) {
		[images_ release];
		images_ = [images copy];
	}
}

- (void)showSheet;
{
	[self _updatePreview];

	[self retain];
	[NSApp beginSheet:sheet_ modalForWindow:[NSApp mainWindow] modalDelegate:self didEndSelector:@selector(exportSheetDidEnd:returnCode:contextInfo:) contextInfo:nil];
}


#pragma mark -
#pragma mark UI

- (void)awakeFromNib
{
	[includedAttributesTableView_ setDataSource:self];
	[includedAttributesTableView_ setDelegate:self];
	
	[includedAttributesTableView_ registerForDraggedTypes:[NSArray arrayWithObject:METADATA_EXPORT_DRAG_TYPE]];
	
	textStorage_ = [[NSTextStorage alloc] initWithString:@""];
	NSLayoutManager *layoutManager = [[[NSLayoutManager alloc] init] autorelease];
    [textStorage_ addLayoutManager:layoutManager];
	
	NSTextContainer *textContainer = [[[NSTextContainer alloc] initWithContainerSize:NSMakeSize(LARGE_TEXT_CONTAINER_SIZE, LARGE_TEXT_CONTAINER_SIZE)] autorelease];
	[textContainer setWidthTracksTextView:NO];
    [textContainer setHeightTracksTextView:NO];
	[layoutManager addTextContainer:textContainer];
	
	[tabDelimitedPreview_ setTextContainer:textContainer];
	[tabDelimitedPreview_ setHorizontallyResizable:YES];
	[tabDelimitedPreview_ setVerticallyResizable:YES];
	[tabDelimitedPreview_ setAutoresizingMask:NSViewNotSizable];
	[tabDelimitedPreview_ setMaxSize:NSMakeSize(LARGE_TEXT_CONTAINER_SIZE, LARGE_TEXT_CONTAINER_SIZE)];
	
	// trigger first load from prefs
	[self _reloadAttributes];
	
	[self changeStyle:styleMatrix_];
	[self changeContentType:contentPopUpButton_];
	[self changeSeparator:separatorPopUpButton_];
	[self changePlaceholderText:placeholderTextField_];
	
}


- (IBAction)changeContentType:(id)sender;
{
	if ([sender selectedTag] == 0) 
		[self _setContentType:MGMetadataExportContentGeneralMetadata];
	else
		[self _setContentType:MGMetadataExportContentROIMetadata];
	
}

- (IBAction)changeStyle:(id)sender;
{
	if ([sender selectedTag] == 0) 
		[self _setStyle:MGMetadataExportStyleTable];
	else
		[self _setStyle:MGMetadataExportStylePlain];
	
	[self _updatePreview];
}

- (IBAction)changePlaceholderText:(id)sender;
{
	[self _updatePreview];
}

- (IBAction)changeSeparator:(id)sender;
{
	[self _updatePreview];

}

- (IBAction)changeShowStacksAsOneEntry:(id)sender;
{
	[self _updatePreview];
}

- (IBAction)closeSheet:(id)sender;
{
	shouldExport_ = ([sender tag] == 0);
	
	[NSApp endSheet:sheet_];
	
}


- (IBAction)selectAllAttributes:(id)sender;
{
	[selectedAttributes_ removeAllObjects];
	[selectedAttributes_ addObjectsFromArray:sortedAttributes_];
	[includedAttributesTableView_ reloadData];
	[self _updatePreview];
}

- (IBAction)selectNoAttributes:(id)sender;
{
	[selectedAttributes_ removeAllObjects];
	[includedAttributesTableView_ reloadData];
	[self _updatePreview];
}


- (void)exportSheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;
{
	[sheet_ orderOut:self];
	
	NSSavePanel *panel = [NSSavePanel savePanel];
	[panel setExtensionHidden:YES];
	
	if (style_ == MGMetadataExportStylePlain)
		[panel setAllowedFileTypes:[NSArray arrayWithObject:@"rtf"]];
	else
		[panel setAllowedFileTypes:[NSArray arrayWithObject:@"txt"]];
	
	if (shouldExport_) {
		[panel beginSheetForDirectory:nil 
								 file:content_ == MGMetadataExportContentGeneralMetadata ? @"Macnification metadata" : @"Macnification ROI data"
					   modalForWindow:[NSApp mainWindow]
						modalDelegate:self 
					   didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:) 
						  contextInfo:nil];
	}
	
	else {
		[sheet close];
		[self release];
	}
	

	
}

- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo;
{
	if (returnCode == NSOKButton) {
		
		if (style_ == MGMetadataExportStylePlain) {
			NSAttributedString *string = [self _RTFStringForPreview:NO];
			[[string RTFFromRange:NSMakeRange(0, [string length]) documentAttributes:nil] writeToFile:[sheet filename] atomically:YES];
		}
		
		else if (content_ == MGMetadataExportContentGeneralMetadata) {
			[[self _TDFStringForGeneralMetadataPreview:NO] writeToFile:[sheet filename] atomically:YES encoding:NSUTF8StringEncoding error:nil];
		}
		
		else if (content_ == MGMetadataExportContentROIMetadata) {
			[[self _TDFStringForROIMetadataPreview:NO] writeToFile:[sheet filename] atomically:YES encoding:NSUTF8StringEncoding error:nil];
		}
		

	}
	
	[sheet close];
	[self release];
}
	 
#pragma mark -
#pragma mark Accessors



- (void)_setContentType:(MGMetadataExportContent)contentType;
{	
	[self _saveAttrsToPrefs];

	content_ = contentType;
	
	[contentPopUpButton_ selectItemWithTag:contentType == MGMetadataExportContentGeneralMetadata ? 0 : 1];
	
	[self _reloadAttributes];
	
	if (contentType == MGMetadataExportContentROIMetadata) {
		[self _setStyle:MGMetadataExportStyleTable];
		[styleMatrix_ setEnabled:NO];		
	}
	else
		[styleMatrix_ setEnabled:YES];
	
	
	[self _updatePreview];

}

- (void)_setStyle:(MGMetadataExportStyle)style;
{
	style_ = style;
	
	if (style == MGMetadataExportStylePlain)
		[fileFormatTextField_ setStringValue:@"An RTF file will be created, which can be opened with Pages or Word."];
	else
		[fileFormatTextField_ setStringValue:@"A TSV file will be created, which can be opened with Numbers or Excel."];

	
	
	[styleMatrix_ selectCellWithTag:style == MGMetadataExportStyleTable ? 0 : 1];
	
	[separatorPopUpButton_ setEnabled:style == MGMetadataExportStylePlain];

}

- (void)_reloadAttributes
{
	
	[sortedAttributes_ removeAllObjects];
	
	NSDictionary *newAttributesAndNames = nil;
	
	if (content_ == MGMetadataExportContentGeneralMetadata)
		newAttributesAndNames = [self _generalMetadataFields];
	else
		newAttributesAndNames = [self _roiMetadataFields];
	
	// sort them according to the prefs
	NSArray *newAttributes = [newAttributesAndNames allKeys];
	newAttributes = [self _attributeArraySortedWithPreferencesForContent:content_ original:newAttributes];
	
	
	[sortedAttributes_ addObjectsFromArray:newAttributes];
	
	
	[selectedAttributes_ removeAllObjects];
	// select some (get from prefs, or all if not available in prefs)
	NSArray *prefsSelectedAttrs = [self _preferenceSelectedAttributesForContent:content_];
	if (!prefsSelectedAttrs)
		[selectedAttributes_ addObjectsFromArray:sortedAttributes_];
	else {
		
		// first filter the selected attrs with those actually available
		NSMutableArray *prefsSelectedAttrsFiltered = [[prefsSelectedAttrs mutableCopy] autorelease];
		for (NSString *attr in prefsSelectedAttrs)
			if (![sortedAttributes_ containsObject:attr])
				[prefsSelectedAttrsFiltered removeObject:attr];
		
		[selectedAttributes_ addObjectsFromArray:prefsSelectedAttrsFiltered];
	}
		
	
	[humanReadableAttributeNames_ release];
	humanReadableAttributeNames_ = [newAttributesAndNames retain];
	
	
	
	[includedAttributesTableView_ reloadData];
	[includedAttributesTableView_ deselectAll:self];

}
	
- (NSDictionary *)_generalMetadataFields;
{
	NSMutableDictionary *fields = [NSMutableDictionary dictionary];
	
	for (NSString *keyPath in [[MGFilterAndSortMetadataController sharedController] exportAttributeKeyPaths])
		[fields setObject:[[MGFilterAndSortMetadataController sharedController] humanReadableNameForAttributeKeyPath:keyPath] forKey:keyPath];
	
	return fields;
}

- (NSArray *)_defaultGeneralFieldOrder
{
	return [[MGFilterAndSortMetadataController sharedController] exportAttributeKeyPaths];
}

- (NSDictionary *)_roiMetadataFields;
{
	NSMutableDictionary *fields = [NSMutableDictionary dictionary];

	[fields setObject:@"Image/Stack Name" forKey:@"image.name"];
	[fields setObject:@"Rank" forKey:@"rank"];
	[fields setObject:@"Comments" forKey:@"comments"];
	[fields setObject:@"Pixel Length" forKey:@"pixelLength"];
	[fields setObject:@"Length" forKey:@"length"];
	[fields setObject:@"Pixel Area" forKey:@"pixelArea"];
	[fields setObject:@"Area" forKey:@"area"];
	[fields setObject:@"Pixel Perimeter" forKey:@"pixelPerimeter"];
	[fields setObject:@"Perimeter" forKey:@"perimeter"];
	[fields setObject:@"Bounding Rect X" forKey:@"boundingRectX"];
	[fields setObject:@"Bounding Rect Y" forKey:@"boundingRectY"];
	[fields setObject:@"Bounding Rect Width" forKey:@"boundingRectWidth"];
	[fields setObject:@"Bounding Rect Height" forKey:@"boundingRectHeight"];
	[fields setObject:@"Circularity" forKey:@"circularity"];
	[fields setObject:@"White, Minimum" forKey:@"minimumWhite"];
	[fields setObject:@"White, Maximum" forKey:@"maximumWhite"];
	[fields setObject:@"White, Average" forKey:@"averageWhite"];
	[fields setObject:@"White, Median" forKey:@"medianWhite"];
	[fields setObject:@"White, Stdev" forKey:@"stdevWhite"];
	[fields setObject:@"White, Integrated Density" forKey:@"integratedDensityWhite"];
	[fields setObject:@"Red, Minimum" forKey:@"minimumRed"];
	[fields setObject:@"Red, Maximum" forKey:@"maximumRed"];
	[fields setObject:@"Red, Average" forKey:@"averageRed"];
	[fields setObject:@"Red, Median" forKey:@"medianRed"];
	[fields setObject:@"Red, Stdev" forKey:@"stdevRed"];
	[fields setObject:@"Red, Integrated Density" forKey:@"integratedDensityRed"];
	[fields setObject:@"Green, Minimum" forKey:@"minimumGreen"];
	[fields setObject:@"Green, Maximum" forKey:@"maximumGreen"];
	[fields setObject:@"Green, Average" forKey:@"averageGreen"];
	[fields setObject:@"Green, Median" forKey:@"medianGreen"];
	[fields setObject:@"Green, Stdev" forKey:@"stdevGreen"];
	[fields setObject:@"Green, Integrated Density" forKey:@"integratedDensityGreen"];
	[fields setObject:@"Blue, Minimum" forKey:@"minimumBlue"];
	[fields setObject:@"Blue, Maximum" forKey:@"maximumBlue"];
	[fields setObject:@"Blue, Average" forKey:@"averageBlue"];
	[fields setObject:@"Blue, Median" forKey:@"medianBlue"];
	[fields setObject:@"Blue, Stdev" forKey:@"stdevBlue"];
	[fields setObject:@"Blue, Average" forKey:@"averageBlue"];
	[fields setObject:@"Blue, Integrated Density" forKey:@"integratedDensityBlue"];
	
	return fields;
	
}


- (NSArray *)_defaultROIFieldOrder
{
	return [NSArray arrayWithObjects:
			@"image.name",
			@"rank",
			@"comments",
			@"pixelLength",
			@"length",
			@"pixelArea",
			@"area",
			@"pixelPerimeter",
			@"perimeter",
			@"boundingRectX",
			@"boundingRectY",
			@"boundingRectWidth",
			@"boundingRectHeight",
			@"circularity",
			@"minimumWhite",
			@"maximumWhite",
			@"averageWhite",
			@"medianWhite",
			@"stdevWhite",
			@"integratedDensityWhite",
			@"minimumRed",
			@"maximumRed",
			@"averageRed",
			@"medianRed",
			@"stdevRed",
			@"integratedDensityRed",
			@"minimumGreen",
			@"maximumGreen",
			@"averageGreen",
			@"medianGreen",
			@"stdevGreen",
			@"integratedDensityGreen",
			@"minimumBlue",
			@"maximumBlue",
			@"averageBlue",
			@"medianBlue",
			@"stdevBlue",
			@"averageBlue",
			@"integratedDensityBlue",
			nil];
}

#pragma mark -
#pragma mark Table data source


- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
	return [sortedAttributes_ count];
}

- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	if ([anObject intValue] == NSOnState)
		[selectedAttributes_ addObject:[sortedAttributes_ objectAtIndex:rowIndex]];
	else
		[selectedAttributes_ removeObject:[sortedAttributes_ objectAtIndex:rowIndex]];
	
	[self _saveAttrsToPrefs];
	[includedAttributesTableView_ reloadData];
	[self _updatePreview];
	
}


- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	if ([[aTableColumn identifier] isEqualToString:@"key"]) {
		
		NSString *identifier = [humanReadableAttributeNames_ objectForKey:[sortedAttributes_ objectAtIndex:rowIndex]];
		
		if ([[sortedAttributes_ objectAtIndex:rowIndex] isEqualToString:@"name"] ||  [[sortedAttributes_ objectAtIndex:rowIndex] isEqualToString:@"image.name"])
			return [[[NSAttributedString alloc] initWithString:@"Image/Stack Name" 
													attributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12], NSFontAttributeName, nil]] 
					autorelease];
		
		else
			return identifier;
		
	}
			
	else
		return ([selectedAttributes_ containsObject:[sortedAttributes_ objectAtIndex:rowIndex]]) ? [NSNumber numberWithInt:NSOnState] : [NSNumber numberWithInt:NSOffState];
}

- (BOOL)tableView:(NSTableView *)aTableView shouldSelectRow:(NSInteger)rowIndex
{
	return YES;
}


#pragma mark -
#pragma mark Dragging

- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard
{
	draggedTableViewIndex_ = [rowIndexes firstIndex];
	
	[pboard setData:[NSData data] forType:METADATA_EXPORT_DRAG_TYPE];
	
	return YES;
}

- (NSDragOperation)tableView:(NSTableView *)aTableView validateDrop:(id < NSDraggingInfo >)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation
{
	if ([info draggingSource] != aTableView)
		return NSDragOperationNone;
	
	if (operation == NSTableViewDropOn) {
		int newRow = MAX(0, MIN([aTableView numberOfRows], row));
		[aTableView setDropRow:newRow dropOperation:NSTableViewDropAbove];
	}
	
	return NSDragOperationMove;
}

- (BOOL)tableView:(NSTableView *)aTableView acceptDrop:(id < NSDraggingInfo >)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation
{
	if (operation == NSTableViewDropOn)
		return NO;
	
	if (draggedTableViewIndex_ == -1 || draggedTableViewIndex_ > [sortedAttributes_ count] - 1)
		return NO;
	
	// identity operation
	if (row == draggedTableViewIndex_)
		return YES;
	
	// move the item to the correct array index
	NSString *item = [sortedAttributes_ objectAtIndex:draggedTableViewIndex_];
	
	[sortedAttributes_ insertObject:item atIndex:row];
	
	// if the dst index < the src index, we need to add one to the src index to find the index to remove
	if (draggedTableViewIndex_ > row)
		[sortedAttributes_ removeObjectAtIndex:draggedTableViewIndex_ + 1];
	else
		[sortedAttributes_ removeObjectAtIndex:draggedTableViewIndex_];
	
	[includedAttributesTableView_ reloadData];
	[self _saveAttrsToPrefs];
	[self _updatePreview];


	return YES;
}


- (void)_updatePreview
{
	NSTextStorage *storage = (style_ == MGMetadataExportStylePlain) ? [rtfPreview_ textStorage] : textStorage_;
	NSTextView *textView = (style_ == MGMetadataExportStylePlain) ? rtfPreview_ : tabDelimitedPreview_;
	NSTextView *otherTextView = (style_ == MGMetadataExportStylePlain) ?  tabDelimitedPreview_ : rtfPreview_;
	
	[[textView enclosingScrollView] setHidden:NO];
	[[otherTextView enclosingScrollView] setHidden:YES];

	
	NSMutableParagraphStyle *style = [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
	[style setDefaultTabInterval:15.0];
	[style setTabStops:[NSArray array]];
	
	if (style_ == MGMetadataExportStylePlain) {
		NSMutableAttributedString *string = [[[self _RTFStringForPreview:YES] mutableCopy] autorelease];
		[string addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [string length])];
		[storage setAttributedString:string];
		
	}
	
	else {
		NSString *string = content_ == MGMetadataExportContentROIMetadata ? [self _TDFStringForROIMetadataPreview:YES] : [self _TDFStringForGeneralMetadataPreview:YES];
		

		
		NSAttributedString *str = [[[NSAttributedString alloc] initWithString:string 
																   attributes:[NSDictionary dictionaryWithObjectsAndKeys:
																			   [NSFont userFixedPitchFontOfSize:0.0],NSFontAttributeName,
																			   style, NSParagraphStyleAttributeName,
																			   nil]]
								   autorelease];
		

		[storage setAttributedString:str];
	}
	
	
	[textView setNeedsDisplay:YES];
	[textView sizeToFit];

}


#pragma mark -
#pragma mark Exporting

- (NSString *)_separator;
{
	switch ([separatorPopUpButton_ selectedTag]) {
		case 0:
		default:
			return [NSString stringWithFormat:@"\n"];
		case 1:
			return [NSString stringWithFormat:@" "];
		case 2:
			return [NSString stringWithFormat:@"\t"];
		case 3:
			return [NSString stringWithFormat:@"; "];
		case 4:
			return [NSString stringWithFormat:@", "];
		case 5:
			return [NSString stringWithFormat:@" - "];
	}
}


- (NSAttributedString *)_RTFStringForPreview:(BOOL)previewOnly;
{
	

	NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@""];
	
	NSDictionary *boldAttrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:10], NSFontAttributeName, nil];
	NSDictionary *regularAttrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:10], NSFontAttributeName, nil];
	NSDictionary *largeHeaderAttrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:13], NSFontAttributeName, nil];
	NSDictionary *largeSubHeaderAttrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11], NSFontAttributeName, nil];

	NSDictionary *smallHeaderAttrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:11], NSFontAttributeName, nil];
	
	
	NSString *separator = [self _separator];
	
	NSArray *sortedImages = [self _sortedImages];
	

	
	
	[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Metadata from %i images\n", (int)[sortedImages count]]
																	attributes:largeHeaderAttrs] autorelease]];
	[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Exported from Macnification on %@\n", [NSCalendarDate calendarDate]]
																	attributes:largeSubHeaderAttrs] autorelease]];
	
	
	

	BOOL omitted = NO;
	if (previewOnly && [sortedImages count] > MAX_PLAIN_PREVIEW_ENTRIES) {
		sortedImages = [sortedImages subarrayWithRange:NSMakeRange(0, MIN([sortedImages count], MAX_PLAIN_PREVIEW_ENTRIES))];
		omitted = YES;
	}
	
	NSArray *sortedSelectedAttrs = [self _sortedSelectedAttributes];

	
	for (Image *image in sortedImages) {
		
		[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@\n\n", [image valueForKey:@"name"]]
																		attributes:smallHeaderAttrs] autorelease]];
		
		
		for (NSString *keyPath in sortedSelectedAttrs) {
			
			id value = [image valueForKeyPath:keyPath];
			NSString *description = [self _stringForAttributeValue:value];
			
			[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: ", [humanReadableAttributeNames_ objectForKey:keyPath]]
																			attributes:boldAttrs] autorelease]];
			
			[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", description, separator]
																			attributes:regularAttrs] autorelease]];
			
		}
		
		[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n"]
																		attributes:regularAttrs] autorelease]];
		
		
	}
	
	
	if (omitted)
		[string appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\n\n(Remainder of images omitted from preview)\n"]
																		attributes:largeHeaderAttrs] autorelease]];
	
	
	return string;
}

- (NSString *)_TDFStringForGeneralMetadataPreview:(BOOL)previewOnly
{
	NSMutableString *string = [[@"" mutableCopy] autorelease];
	
	NSArray *sortedImages = [self _sortedImages];
	
	
	[string appendString:[NSString stringWithFormat:@"Metadata from %i images\n", (int)[sortedImages count]]];
	[string appendString:[NSString stringWithFormat:@"Exported from Macnification on %@\n\n", [NSCalendarDate calendarDate]]];
	
	
	BOOL omitted = NO;
	if (previewOnly && [sortedImages count] > MAX_TABLE_PREVIEW_ENTRIES) {
		sortedImages = [sortedImages subarrayWithRange:NSMakeRange(0, MIN([sortedImages count], MAX_TABLE_PREVIEW_ENTRIES))];
		omitted = YES;
	}
	
	
	// Headers
	
	
	NSArray *sortedSelectedAttrs = [self _sortedSelectedAttributes];
	
	int attributeIndex = 0;
	
	for (NSString *attr in sortedSelectedAttrs) {
		
		if (previewOnly && attributeIndex > 4)
			continue;
		
		NSString *description = [humanReadableAttributeNames_ objectForKey:attr];
		
		if (previewOnly)
			description = [self _stringByEllipsingOrPaddingString:description length:attributeIndex == 0 ? 25 : 18];
		
		if (attributeIndex == 0)
			[string appendString:description];
		else
			if (previewOnly)
				[string appendFormat:@"\t|\t%@", description];
			else
				[string appendFormat:@"\t%@", description];
		
		
		attributeIndex++;
	}
	
	if (previewOnly)
		[string appendString:@"\t\t(........)"];
	
	[string appendString:@"\n"];
	[string appendString:@"\n"];
	[string appendString:@"\n"];
	
	
	// Content
	
	for (Image *image in sortedImages) {
		
		int attributeIndex = 0;
		
		for (NSString *attr in sortedSelectedAttrs) {
			
			if (previewOnly && attributeIndex > 4)
				continue;
			
			id value = [image valueForKeyPath:attr];
			NSString *description = [self _stringForAttributeValue:value];
			
			if (previewOnly)
				description = [self _stringByEllipsingOrPaddingString:description length:attributeIndex == 0 ? 25 : 18];
			
			if (attributeIndex == 0)
				[string appendString:description];
			else
				if (previewOnly)
					[string appendFormat:@"\t|\t%@", description];
				else
					[string appendFormat:@"\t%@", description];
			
			
			attributeIndex++;
		}
		
		if (previewOnly)
			[string appendString:@"\t|\t(........)"];
		
		[string appendString:@"\n"];
		
	}
	
	if (previewOnly)
		[string appendString:@"\n\n\n\n\n(Values, columns and rows abbreviated in or omitted from preview)"];
	
	return string;
	
}


- (NSString *)_TDFStringForROIMetadataPreview:(BOOL)previewOnly
{
	
	NSMutableString *string = [[@"" mutableCopy] autorelease];
		
	NSArray *sortedROIs = [self _sortedROIs];
	
	int nImages = ![self _showsStacksAsOneEntry] ? [[self _expandStacks:images_] count] : [images_ count];
		
	[string appendString:[NSString stringWithFormat:@"%i ROIs from %i images\n", (int)[sortedROIs count], (int)nImages]];
	[string appendString:[NSString stringWithFormat:@"Exported from Macnification on %@\n\n", [NSCalendarDate calendarDate]]];
	
	BOOL omitted = NO;
	if (previewOnly && [sortedROIs count] > MAX_TABLE_PREVIEW_ENTRIES) {
		sortedROIs = [sortedROIs subarrayWithRange:NSMakeRange(0, MIN([sortedROIs count], MAX_TABLE_PREVIEW_ENTRIES))];
		omitted = YES;
	}

	
	
	// Headers
	
	
	NSArray *sortedSelectedAttrs = [self _sortedSelectedAttributes];
	
	
	
	
	
	int attributeIndex = 0;
	
	for (NSString *attr in sortedSelectedAttrs) {
		
		if (previewOnly && attributeIndex > 4)
			continue;
		
		NSString *description = [humanReadableAttributeNames_ objectForKey:attr];
		
		if (previewOnly)
			description = [self _stringByEllipsingOrPaddingString:description length:attributeIndex == 0 ? 25 : 18];
		
		if (attributeIndex == 0)
			[string appendString:description];
		else
			if (previewOnly)
				[string appendFormat:@"\t|\t%@", description];
			else
				[string appendFormat:@"\t%@", description];
		
		
		attributeIndex++;
	}
	
	if (previewOnly)
		[string appendString:@"\t\t(........)"];
	
	[string appendString:@"\n"];
	[string appendString:@"\n"];
	[string appendString:@"\n"];
	
	
	
	BOOL shouldUseStackNameInsteadOfImageName = [self _showsStacksAsOneEntry];
	
	BOOL didSortByImageName = ([[sortedSelectedAttrs objectAtIndex:0] isEqual:@"image.name"]);
	Image *currentImage = nil;
	
	
	// Content
	
	for (ROI *roi in sortedROIs) {
		
		// introduce an empty line if we started a new image
		if (didSortByImageName) {
			Image *newImage = [roi valueForKey:@"image"];
			
			if (shouldUseStackNameInsteadOfImageName && [newImage stack])
				newImage = [newImage stack];
			
			if (currentImage && newImage != currentImage)
				[string appendFormat:@"\n"];
			
			currentImage = newImage;
		}
		
		
		
		
		int attributeIndex = 0;
		
		for (NSString *attr in sortedSelectedAttrs) {
			
			if (previewOnly && attributeIndex > 4)
				continue;
			
			NSString *adjustedAttr = attr;
			if (shouldUseStackNameInsteadOfImageName && [[roi valueForKey:@"image"] stack] && [adjustedAttr isEqual:@"image.name"])
				adjustedAttr = @"image.stack.name";
			
			id value = [roi valueForKeyPath:adjustedAttr];
			NSString *description = [self _stringForAttributeValue:value];
			
			if (previewOnly)
				description = [self _stringByEllipsingOrPaddingString:description length:attributeIndex == 0 ? 25 : 18];
			
			if (attributeIndex == 0)
				[string appendString:description];
			else
				if (previewOnly)
					[string appendFormat:@"\t|\t%@", description];
				else
					[string appendFormat:@"\t%@", description];
			
			
			attributeIndex++;
		}
		
		if (previewOnly)
			[string appendString:@"\t|\t(........)"];
		
		[string appendString:@"\n"];
		
	}
	
	if (previewOnly)
		[string appendString:@"\n\n\n\n\n(Values, columns and rows abbreviated or omitted from preview)"];
	
	return string;
	
	
}

- (NSString *)_stringForAttributeValue:(id)value
{
	if (value == NULL)
		return [placeholderTextField_ stringValue];
	
	if (value == NSMultipleValuesMarker)
		return @"[Multiple]";
	
	if ([value isKindOfClass:[NSString class]]) {
		if ([value length] == 0)
			return @"-";
		else
			return value;		
	}
	
	if ([value isKindOfClass:[NSDate class]])
		return [value description];
	
	
	return [value descriptionWithLocale:[NSLocale currentLocale]];
}

- (NSString *)_stringByEllipsingOrPaddingString:(NSString *)original length:(int)length
{
	if ([original length] == length)
		return original;
	
	int diff = [original length] - length;
	
	if (diff < 0)
		return [original stringByPaddingToLength:length withString:@" " startingAtIndex:0];
	
	// create ellipsis
	return [[original stringByPaddingToLength:length - 3 withString:@" " startingAtIndex:0] stringByAppendingString:@"..."];
		
	
}


- (NSArray *)_sortedROIs
{
	NSArray *images = images_;
	
	
	// always unstack for ROIs -- we'll group by stack name instead of image name later
	
	//if (![[NSUserDefaults standardUserDefaults] boolForKey:@"metadataExportShowAsOneEntry"])
		images = [self _expandStacks:images];

	
	NSMutableSet *allROIs = [NSMutableSet set];
	for (Image *image in images)
		[allROIs unionSet:[image valueForKey:@"rois"]];
	
	BOOL shouldUseStackNameInsteadOfImageName = [self _showsStacksAsOneEntry];

	// get the keypaths for sorting (relative to ROI!)
	NSMutableArray *sortKeyPaths = [[[self _sortedSelectedAttributes] mutableCopy] autorelease];
	
	// If we need to use stack names to sort, just insert the descriptor before the first one, provided that the former first one is also a name descriptor
	// For all nil values, this won't have any influence
	if ([[sortKeyPaths objectAtIndex:0] isEqual:@"image.name"] && shouldUseStackNameInsteadOfImageName)
		[sortKeyPaths addObject:@"image.stack.name"];
		
	
	
	// create descriptors
	NSMutableArray *sortDescriptors = [NSMutableArray array];
	for (NSString *keyPath in sortKeyPaths) {
		if ([sortDescriptors count] >= 5)
			continue;
		[sortDescriptors addObject:[NSSortDescriptor sortDescriptorWithKey:keyPath ascending:YES]];
	}
	
	
	NSArray *sorted = [[allROIs allObjects] sortedArrayUsingDescriptors:sortDescriptors];
	
	
	return sorted;
}


- (BOOL)_showsStacksAsOneEntry
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"metadataExportShowAsOneEntry"];
}


- (NSArray *)_sortedImages
{
	NSMutableArray *sortDescriptors = [NSMutableArray array];
	for (NSString *keyPath in [self _sortedSelectedAttributes]) {
		
		if ([sortDescriptors count] >= 5)
			continue;
		
		[sortDescriptors addObject:[NSSortDescriptor sortDescriptorWithKey:keyPath ascending:YES]];
	}
	
	NSArray *sorted = images_;
	
	if (![self _showsStacksAsOneEntry])
		sorted = [self _expandStacks:sorted];
	

	sorted = [sorted sortedArrayUsingDescriptors:sortDescriptors];
	

	return sorted;
}

- (NSArray *)_sortedSelectedAttributes
{
	NSMutableArray *sortedSelected = [NSMutableArray array];
	
	for (NSString *keyPath in sortedAttributes_)
		if ([selectedAttributes_ containsObject:keyPath])
			[sortedSelected addObject:keyPath];
	
	return sortedSelected;
	
}

- (NSArray *)_expandStacks:(NSArray *)original
{	
	NSMutableArray *stacksExpanded = [NSMutableArray array];
	for (Image *image in original) {
		if ([image isKindOfClass:[Stack class]])
			[stacksExpanded addObjectsFromArray:[(Stack *)image images]];
		else
			[stacksExpanded addObject:image];
	}
	
	return stacksExpanded;
}


#pragma mark -
#pragma mark Prefs (attributes)

- (NSArray *)_attributeArraySortedWithPreferencesForContent:(MGMetadataExportContent)content original:(NSArray *)original
{
	NSArray *prefsArray = [self _preferenceAttributesForContent:content];
	
	if (!prefsArray)
		return original;
	
	NSMutableArray *sorted = [NSMutableArray array];
	
	// filter the prefs: only retain what is in the original array.  This way we retain the sort order from the prefs array.
	for (NSString *attr in prefsArray)
		if ([original containsObject:attr])
			[sorted addObject:attr];
	
	// add any attrs that are in original, but not in the prefs
	NSMutableArray *newAttrs = [[[original mutableCopy] mutableCopy] autorelease]; 
	[newAttrs removeObjectsInArray:sorted];
	
	[sorted addObjectsFromArray:newAttrs];
	
	return sorted;
	
}

- (NSArray *)_preferenceAttributesForContent:(MGMetadataExportContent)content
{
	NSString *prefKey = (content == MGMetadataExportContentGeneralMetadata) ? @"metadataExportAttributesGeneral" : @"metadataExportAttributesROI";
	
	if ([[NSUserDefaults standardUserDefaults] objectForKey:prefKey])
		return[[NSUserDefaults standardUserDefaults] arrayForKey:prefKey];
	
	return (content == MGMetadataExportContentGeneralMetadata) ? [self _defaultGeneralFieldOrder] : [self _defaultROIFieldOrder];
}


- (NSArray *)_preferenceSelectedAttributesForContent:(MGMetadataExportContent)content
{
	NSString *prefKey = (content == MGMetadataExportContentGeneralMetadata) ? @"metadataExportAttributesGeneralSelected" : @"metadataExportAttributesROISelected";
	
	return [[NSUserDefaults standardUserDefaults] arrayForKey:prefKey];
}

- (void)_saveAttrsToPrefs
{
	NSString *prefSelectedKey = (content_ == MGMetadataExportContentGeneralMetadata) ? @"metadataExportAttributesGeneralSelected" : @"metadataExportAttributesROISelected";
	NSString *prefKey = (content_ == MGMetadataExportContentGeneralMetadata) ? @"metadataExportAttributesGeneral" : @"metadataExportAttributesROI";

	[[NSUserDefaults standardUserDefaults] setObject:sortedAttributes_ forKey:prefKey];
	[[NSUserDefaults standardUserDefaults] setObject:selectedAttributes_ forKey:prefSelectedKey];

	
}


																										   
@end
