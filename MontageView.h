//
//  StackMontageView.h
//  Stack
//
//	This view class is responsible for rendering the montage content, using MontageUnitView instances for every image 
//	and delegating layout to a Montage[Type]Layout class.
//
//  Created by Dennis Lorson on 09/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>

@class MontageView;

@protocol MontageViewDataSource <NSObject>

// Image
- (int)montageViewNumberOfImages:(MontageView *)view;
- (NSImage *)montageView:(MontageView *)view imageAtIndex:(int)index;

// Metadata
- (int)montageViewNumberOfMetadataFields:(MontageView *)view;
- (NSString *)montageView:(MontageView *)view valueForMetadataFieldAtIndex:(int)metadataIndex imageIndex:(int)imageIndex;

// Attributes
- (int)montageViewImagesPerRow:(MontageView *)view;
- (NSColor *)backgroundColor;
- (BOOL)displaysTitle;
- (NSString *)title;
- (BOOL)displaysDate;


@end


@interface MontageView : NSView 
{
	id <MontageViewDataSource> dataSource_;
		
	BOOL rendersPreview;										// does this view use thumbnails or full-scale mages?
	
	NSMutableDictionary *layoutParameters;						// these are temporarily retained for the pdf renderer to know the rects to render.

}

@property(readwrite, assign) id <MontageViewDataSource> dataSource;							// weak ref, otherwise we get a retain loop
@property(readwrite) BOOL rendersPreview;


- (PDFDocument *)montagePDF;
- (void)updateLayout;
	


@end
