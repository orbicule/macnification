//
//  ROIColorOperation.h
//  Filament
//
//  Created by Peter Schols on 08/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ROI.h"


@interface ROIColorOperation : NSOperation 
{
	NSBezierPath *path_;
	NSBitmapImageRep *rep_;
	BOOL triggersNotifications_;
}

@property(readwrite) BOOL triggersNotifications;

- (id)initWithImageRep:(NSBitmapImageRep *)rep path:(NSBezierPath *)path;



@end
