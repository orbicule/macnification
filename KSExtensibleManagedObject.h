//
//  KSExtensibleManagedObject.h
//
//  Created by Mike Abdullah on 25/08/2007.
//  Copyright 2007-2008 Karelia Software. All rights reserved.
//
//  THIS SOFTWARE IS PROVIDED BY KARELIA SOFTWARE AND ITS CONTRIBUTORS "AS-IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUR OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.
//
//
//	A special kind of managed object that allows you to use -valueForKey: and
//	-setValueForKey: using any key. If the object does not normally accept this
//	key, it is stored internally in a dictionary and then archived as data.


#import <Cocoa/Cocoa.h>


@interface KSExtensibleManagedObject : NSManagedObject
{
	@private
	NSMutableDictionary	*myExtensibleProperties;
	}

+ (NSString *)extensiblePropertiesDataKey;

- (NSDictionary *)extensibleProperties;

/*!	These two methods are called by KSExtensibleManagedObject when archiving or unarchiving
 *	the dictionary it uses in-memory. You can override them in a subclass to tweak the
 *	behaviour. e.g. To use an encoding method other than NSKeyedArchiver.
 */
- (NSDictionary *)unarchiveExtensibleProperties:(NSData *)propertiesData;
- (NSData *)archiveExtensibleProperties:(NSDictionary *)properties;



@end
