//
//  LightTableAlignConstraint.h
//  LightTable
//
//  Objects of this class represent a coordinate constraint, i.e. the coordinate a views frame can snap to if desired.
//  It contains the coordinate itself, which direction it is in (x or y - horizontal or vertical), and from which threshold distance to the coordinate snapping is allowed.
//	Additionally, it contains a type (MoveAlignConstraint/ResizeAlignConstraint) because of different behavior, and temp. vars for the cursor positions, to determine if snap out is needed.
//
//	Constraints are added (imposed) to an LightTableImageView; this view consults these constraints to see if the desired frame position should be restricted (== "elastic" snap)
//	They are created by a layout element.
//
//  Created by Dennis on 19/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>

static const CGFloat snapOutThreshold = 8.0;
static const CGFloat snapOutMargin = 2.0;

// enum for the direction of this constraint (horizontal means: corresponding to a horizontal guide bezier)

enum
{
	HorizontalConstraintDirection = 1,
	VerticalConstraintDirection = 2
};

typedef NSInteger ConstraintDirection;


// enum for the type of this constraint

enum
{
	ResizeAlignConstraint = 1,
	MoveAlignConstraint = 2
};

typedef NSInteger ConstraintType;




@interface LightTableAlignConstraint : NSObject 
{

	ConstraintDirection direction;				// the direction this constraint is active in
	ConstraintType type;						// the type of this constraint (resize or move).  Determines whether the constraint is allowed to impose given frame size _OR_ origin.
	CGFloat threshold;							// the threshold value for this constraint (i.e., from which distance will the elastic effect take place)
	CGFloat coordinate;							// the coordinate value of the constraint.
	BOOL isActive;								// YES if this constraint is still active, NO if it can be safely removed by its managing object (because the user has snapped out of it).
	
	NSPoint currentCursorPosition;				// the position of the cursor during the current/next constraint evaluation
	NSPoint originalCursorPosition;				// the position of the cursor when this constraint was first imposed.
	
	CGFloat zoomLevel;
	

}

@property(readwrite) ConstraintDirection direction;
@property(readwrite) CGFloat threshold, coordinate;
@property(readwrite) NSPoint currentCursorPosition;
@property(readonly) BOOL isActive;
@property(readwrite) ConstraintType type;
@property(readwrite) CGFloat zoomLevel;


- (NSRect)constrainedFrameWithFrame:(NSRect)originalFrame;


@end
