//
//  ImageArrayController+MenuValidation.m
//  Filament
//
//  Created by Peter Schols on 09/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "ImageArrayControllerMenuValidation.h"
#import "MGAppDelegate.h"
#import "MGLibrary.h"
#import "Stack.h"
#import "SourceListTreeController.h"
#import "MGLibraryController.h"

@implementation ImageArrayController (ImageArrayControllerMenuValidation)


- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
    SEL theAction = [anItem action];
	
	
	// More than one image selected
	
    if (theAction == @selector(duplicate:) || 
		theAction == @selector(newVersionFromMaster:) ||
		theAction == @selector(revertToOriginal:) ||
		theAction == @selector(analyzeInExternalApplication:) ||
		theAction == @selector(editInExternalApplication:) ||
		theAction == @selector(pasteFilters:) ||
		theAction == @selector(removeAllFilters:) ||
		theAction == @selector(copyKeywords:) ||
		theAction == @selector(pasteKeywords:) ||
		theAction == @selector(removeAllKeywords:) ||
		theAction == @selector(copyCalibration:) ||
		theAction == @selector(pasteCalibration:) ||
		theAction == @selector(removeCalibration:) ||
		theAction == @selector(setRating:) ||
		theAction == @selector(copyROIs:) ||
		theAction == @selector(pasteROIs:) ||
		theAction == @selector(removeAllROIs:) ||
		theAction == @selector(removeScaleBar:) ||
		theAction == @selector(splitChannels:) ||
		theAction == @selector(mergeChannels:) ||
		theAction == @selector(exportImageMetadata:)
		)
    {
        return [[self selectedObjects] count] > 0;		
    }
	
	
	
	// Exactly one image selected
	
	if (theAction == @selector(copyFilters:) ||
		theAction == @selector(copyKeywords:) ||
		theAction == @selector(copyCalibration:) ||
		theAction == @selector(copyROIs:)
		)
	{
		
		return [[self selectedObjects] count] == 1;		
	}
	
	
	
	
	
	if (theAction == @selector(stackImages:))
		return ![self selectionContainsStack];
	
	
	if (theAction == @selector(unstackImages:))
		return [self selectionContainsStack];
	
	
	if (theAction == @selector(emailImages:) ||
		theAction == @selector(createMontage:) ||
		theAction == @selector(exportImages:)
		)
	{
		if (([[self arrangedObjects] count] > 0 && [sourceListTreeController selectedCollection] != [[MGLibraryControllerInstance library] libraryGroup]) 
			|| [[self selectedObjects] count] > 0) {
			return YES;
        }
        return NO;		
		
	}
	


	
	if (theAction == @selector(remove:)) {
		// Disable the remove image menu when a smart folder is selected
		if ([[self selectedObjects] count] > 0 && ![sourceListTreeController smartAlbumIsSelected]) {
			return YES;
		}
		return NO;
	}

	return YES;
}


@end
