//
//  MGStackTitleView.m
//  Filament
//
//  Created by Dennis Lorson on 22/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "MGStackTitleView.h"


@implementation MGStackTitleView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect 
{
    // Drawing code here.
	NSImage *bg = [NSImage imageNamed:@"browser_titlebar_bg"];
	[bg drawInRect:[self bounds] fromRect:NSMakeRect(0, 0, [bg size].width, [bg size].height) operation:NSCompositeSourceOver fraction:1.0];
}

@end
