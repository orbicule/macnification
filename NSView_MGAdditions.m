//
//  NSView_MGAdditions.m
//  LightTable
//
//  Created by Dennis Lorson on 19/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "NSView_MGAdditions.h"
#import "MGFunctionAdditions.h"

#import <QuartzCore/QuartzCore.h>


@implementation NSView (MGAdditions)



- (NSImage *)viewImage
{
	if (![self layer]) {
		
		[self lockFocus];
		NSImage *image = [[[NSImage alloc] initWithData:[self dataWithPDFInsideRect:[self bounds]]] autorelease];
		[self unlockFocus];
		
		
		
		NSImage *alphaImg = [[[NSImage alloc] initWithSize:[image size]] autorelease];
		[alphaImg lockFocus];
		[image drawAtPoint:NSZeroPoint fromRect:NSMakeRect(0, 0, [image size].width, [image size].height) operation:NSCompositeSourceOver fraction:0.65];
		[alphaImg unlockFocus];
		
		
		return alphaImg;
		
	} else {
		
		CGRect bounds = [self layer].bounds;
		
		NSInteger width = bounds.size.width;
		NSInteger height = bounds.size.height;
		size_t bytesPerRow = 4 * width;
		
		void *memData = calloc(bytesPerRow * height, 1);
		
		CGContextRef context = CGBitmapContextCreate(memData, width, height, 8, bytesPerRow, CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB), kCGImageAlphaPremultipliedLast);
		
		[[self layer] renderInContext:context];
		
		CGImageRef cgImg = CGBitmapContextCreateImage(context);
		NSBitmapImageRep *imageRep = [[[NSBitmapImageRep alloc] initWithCGImage:cgImg] autorelease];
		CGImageRelease(cgImg);
				
		NSImage *img = [[[NSImage alloc] init] autorelease];
		[img addRepresentation:imageRep];
		
		NSImage *alphaImg = [[[NSImage alloc] initWithSize:[img size]] autorelease];
		[alphaImg lockFocus];
		[img drawAtPoint:NSZeroPoint fromRect:NSMakeRect(0, 0, [img size].width, [img size].height) operation:NSCompositeSourceOver fraction:0.65];
		[alphaImg unlockFocus];
		
		CGContextRelease(context);
		free(memData);
		
		return alphaImg;

		
	}
	
	
}



- (NSPoint)convertPointToGlobal:(NSPoint)viewPoint
{
	// converts a point in the views coordinate system to the global coordinate system, taking into account the screen the view is in.
	NSPoint positionInView = viewPoint;
	NSPoint positionOnScreen = [[self window] convertBaseToScreen:positionInView];
	
	NSScreen *standardScreen = [[NSScreen screens] objectAtIndex:0];
	NSScreen *activeScreen = [[self window] screen];
	
	CGFloat screenDelta = [standardScreen frame].size.height - [activeScreen frame].size.height;
	
	NSPoint flippedPositionOnScreen = NSMakePoint(positionOnScreen.x, [activeScreen frame].size.height - positionOnScreen.y);
	
	return NSMakePoint(flippedPositionOnScreen.x, flippedPositionOnScreen.y + screenDelta);
}


- (NSRect)frameForNewSizeRetainingCenter:(NSSize)newFrameSize
{
	NSRect oldFrame = [self frame];
	CGFloat dx = oldFrame.origin.x - (oldFrame.size.width - newFrameSize.width)/2;
	CGFloat dy = oldFrame.origin.y - (oldFrame.size.height - newFrameSize.height)/2;
	//NSLog(NSStringFromRect(NSMakeRect(oldFrame.origin.x - dx, oldFrame.origin.y - dy, newFrameSize.width, newFrameSize.height)));
	
	return NSMakeRect(oldFrame.origin.x - dx, oldFrame.origin.y - dy, newFrameSize.width, newFrameSize.height);
}




// ordering functions used in distributing etc.

- (NSComparisonResult)compareFrameX:(NSView *)theView
{
	if ([self frame].origin.x > [theView frame].origin.x) return NSOrderedDescending;
	if ([self frame].origin.x < [theView frame].origin.x) return NSOrderedAscending;
	return NSOrderedSame;
}

- (NSComparisonResult)compareFrameY:(NSView *)theView
{
	if ([self frame].origin.y > [theView frame].origin.y) return NSOrderedDescending;
	if ([self frame].origin.y < [theView frame].origin.y) return NSOrderedAscending;
	return NSOrderedSame;
}

- (NSComparisonResult)compareFrameCenterX:(NSView *)theView
{
	if (MGRectCenter([self frame]).x > MGRectCenter([theView frame]).x) return NSOrderedDescending;
	if (MGRectCenter([self frame]).x < MGRectCenter([theView frame]).x) return NSOrderedAscending;
	return NSOrderedSame;
}

- (NSComparisonResult)compareFrameCenterY:(NSView *)theView
{
	if (MGRectCenter([self frame]).y > MGRectCenter([theView frame]).y) return NSOrderedDescending;
	if (MGRectCenter([self frame]).y < MGRectCenter([theView frame]).y) return NSOrderedAscending;
	return NSOrderedSame;
}

- (NSComparisonResult)compareFrameWidth:(NSView *)theView
{
	if ([self frame].size.width > [theView frame].size.width) return NSOrderedDescending;
	if ([self frame].size.width < [theView frame].size.width) return NSOrderedAscending;
	return NSOrderedSame;
}

- (NSComparisonResult)compareFrameHeight:(NSView *)theView
{
	if ([self frame].size.height > [theView frame].size.height) return NSOrderedDescending;
	if ([self frame].size.height < [theView frame].size.height) return NSOrderedAscending;
	return NSOrderedSame;
}


- (void)needsRecursiveDisplay
{
	for (NSView *subview in [self subviews]) {
		[subview setNeedsDisplay:YES];
		[subview needsRecursiveDisplay];
	}
}


@end
