//
//  Montage.h
//  Stack
//
//  Created by Dennis Lorson on 2/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>

#import "MontageView.h"

@class MGPDFHUDView;
@class MGMouseOverView;
@class MontageView;
@class Image;


@interface Montage : NSObject <NSTableViewDelegate, NSTableViewDataSource, MontageViewDataSource>
{
	
	id delegate;
	SEL didEndSelector;
	
	PDFDocument *finalMontage;
	
	BOOL wantsPrint;
	BOOL callsDelegate;
	BOOL didCancel;
	
	
	// IB Outlets for the preview
	
	IBOutlet NSWindow *montageWindow;
	IBOutlet NSColorWell *montageBackgroundColorWell;
	IBOutlet NSPopUpButton *montageItemsPerRowButton;
	IBOutlet NSButton *montageDisplayDateCheckBox, *montageDisplayImageNamesCheckBox, *montageDisplayTitleCheckBox;
	IBOutlet NSTextField *montageCustomTitleField;
	
	IBOutlet PDFView *montagePDFPreview;
	IBOutlet MGPDFHUDView *pdfHUDView;
	IBOutlet MGMouseOverView *pdfHUDContainer;
	
	IBOutlet NSTableView *includedAttributesTableView_;
	
	IBOutlet NSProgressIndicator *progressIndicator;
	IBOutlet NSTextField *progressField;
	
	
	MontageView *montagePreview;
	
	// attributes
	
	NSInteger imagesPerRow;
	NSString *title;
	NSColor *backgroundColor;
	
	BOOL displaysDate;
	BOOL displaysTitle;
	BOOL displaysScaleBars;

	
	// model data
	NSMutableArray *images;
	
	// Metadata
	NSMutableArray *sortedAttributes_;			// all available attrs (sorted)
	NSMutableArray *selectedAttributes_;		// those attrs that have been selected (unsorted)
	NSDictionary *humanReadableAttributeNames_;
	NSInteger draggedTableViewIndex_;
}

@property(readonly, assign) NSMutableArray *images;

@property(nonatomic) NSInteger imagesPerRow;
@property(nonatomic, retain) NSColor *backgroundColor;
@property(nonatomic, retain) NSString *title;
@property(nonatomic) BOOL displaysDate;
@property(nonatomic) BOOL displaysTitle;


- (void)addImage:(Image *)image;

// will invoke a didEnd with or without data.  didEndSelector needs the following signature:  - (void)montage:(Montage *)montage didEndCreation:(PDFDocument *)doc
// the returned document will be NULL if the user has cancelled the creation.
- (void)showPreviewForWindow:(NSWindow *)window delegate:(id)delegate didEndSelector:(SEL)didEndSelector;



// these are private (here for IB only)
- (IBAction)createMontage:(id)sender;
- (IBAction)printMontage:(id)sender;
- (IBAction)cancelMontage:(id)sender;

- (IBAction)selectAllAttributes:(id)sender;
- (IBAction)selectNoAttributes:(id)sender;

- (IBAction)changeDisplaysScalebars:(id)sender;

@end
