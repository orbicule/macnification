//
//  LineROI.h
//  Macnification
//
//  Created by Peter Schols on 04/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ROI.h"
#import "DistanceROI.h"


@interface LineROI : DistanceROI {

}

- (void)setStartPoint:(NSPoint)startPoint endPoint:(NSPoint)endPoint;

- (NSPoint)startPoint;
- (NSPoint)endPoint;


- (void)calculateLength;


- (BOOL)containsPoint:(NSPoint)point;

- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
- (NSBezierPath *)pathWithScale:(float)scale;




@end
