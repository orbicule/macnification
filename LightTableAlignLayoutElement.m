//
//  LightTableAlignLayoutElement.m
//  LightTable
//
//  Created by Dennis on 18/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "LightTableAlignLayoutElement.h"
#import "LightTableAlignConstraint.h"
#import "LightTableImageView.h"


// constants

static const CGFloat guideDashPattern[] = {40.0, 10.0, 20.0, 10.0};		// striped pattern for the alignment guide
static const CGFloat guideEndingMargin = 0.0;							// margin between the end of both shapes and the end of the guide

static const CGFloat standardThreshold = 1.5;							// threshold to add a layout guide		* when dragging frame
static const CGFloat precisionThreshold = 0.1;							//										* when keyboard moving
static const CGFloat standardResizeThreshold = 1.0;						//										* when resizing by the control points

static const CGFloat constraintThreshold = 1;							// threshold to get in the snap accompanying the guide.

static const CGFloat proximityDistance = 150;							// the maximal distance between the associated view and another in order to consider any layout guides/constraints



@interface LightTableAlignLayoutElement (Private)

- (void)setGuideExcludingMarginsFromPoint:(NSPoint)first toPoint:(NSPoint)second;
- (void)setGuideFromPoint:(NSPoint)first toPoint:(NSPoint)second;
- (void)addGuide:(NSBezierPath *)path;

- (void)addConstraintWithDirection:(ConstraintDirection)theDirection threshold:(CGFloat)theThreshold coordinate:(CGFloat)theCoordinate type:(ConstraintType)type;
- (void)removeConstraints;

- (BOOL)frame:(NSRect)first isBelowProximityDistanceFromFrame:(NSRect)second direction:(Direction)direction;
- (BOOL)value:(CGFloat)first isBelowProximityDistanceFrom:(CGFloat)second;
- (BOOL)value:(CGFloat)first isBelowThresholdDistanceFrom:(CGFloat)second precision:(LayoutPrecision)precision;


@end

@implementation LightTableAlignLayoutElement



@synthesize associatedView, constraints, zoomLevel;


#pragma mark Initialization
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (id)init
{
	if ((self = [super init])) {
		
		self.constraints = [NSMutableArray array];
		guideBeziers = [[NSMutableArray array] retain];
		self.zoomLevel = 1.0;
	}
	return self;
}

- (void)dealloc
{
	self.constraints = nil;
	[guideBeziers release];
	
	[super dealloc];
}

#pragma mark Updating constraints + beziers
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Updating constraints + beziers
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (BOOL)updateConstraintsWithFrameChange:(BOOL)move OfView:(LightTableImageView *)theView precision:(LayoutPrecision)precision
{
	// determine if the current element should be an active guide (i.e. it should be drawn and movement should be constrained to it.)
	// return YES if it should be active.
	
	// if this method returns YES, it is expected to have updated the guide bezier so that sender can invoke -draw.
	
	// to determine if it is active, compare all frame properties of associated view and the moved one.
	// there can only be one guide per element at a time, so prioritize if more than one possibility.
	
	// "move" == NO when a resize action changed the frame, YES in case of a move action. 
	
	[self removeConstraints];
	[guideBeziers removeAllObjects];
	
	if (!self.associatedView || !theView || theView == self.associatedView) return NO;
	 
	BOOL constraintFound = NO;
	
	ConstraintType type = move ? MoveAlignConstraint : ResizeAlignConstraint;
		
	NSRect firstFrame = [theView frame];
	NSRect secondFrame = [associatedView frame];
	
	// compare center alignments (the most important ones)
	
	NSPoint firstCenter = NSMakePoint(firstFrame.origin.x + firstFrame.size.width/2, firstFrame.origin.y + firstFrame.size.height/2);
	NSPoint secondCenter = NSMakePoint(secondFrame.origin.x + secondFrame.size.width/2, secondFrame.origin.y + secondFrame.size.height/2);
	
	// vertical
	if ([self value:firstCenter.x isBelowThresholdDistanceFrom:secondCenter.x precision:precision]) {
	
		// are we close enough to the other frame? otherwise, ignore
		// ADDED: we don't want center alignments when resizing. So check for the type of action taken by the user.
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:VerticalDirection] && move) {
		
			// set vertical guide fixed to the position of the associated object (not the one that is being moved)
			NSPoint firstPoint = NSMakePoint(secondCenter.x, fmax(NSMaxY(firstFrame), NSMaxY(secondFrame)));
			NSPoint secondPoint = NSMakePoint(secondCenter.x, fmin(NSMinY(firstFrame), NSMinY(secondFrame)));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			// set the constraint that will influence the frame position
			CGFloat xCoord = secondFrame.origin.x + (secondFrame.size.width - firstFrame.size.width)/2;
			[self addConstraintWithDirection:VerticalConstraintDirection threshold:constraintThreshold coordinate:xCoord type:type];
			
			constraintFound = YES;
			
		} else {
			// do nothing (try if we meet the requirements for other alignment guides)
		}
	}
	
	// horizontal
	if ([self value:firstCenter.y isBelowThresholdDistanceFrom:secondCenter.y precision:precision]) {
		
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:HorizontalDirection] && move) {
			
			NSPoint firstPoint = NSMakePoint(fmax(NSMaxX(firstFrame), NSMaxX(secondFrame)), secondCenter.y);
			NSPoint secondPoint = NSMakePoint(fmin(NSMinX(firstFrame), NSMinX(secondFrame)), secondCenter.y);
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat yCoord = secondFrame.origin.y + (secondFrame.size.height - firstFrame.size.height)/2;
			[self addConstraintWithDirection:HorizontalConstraintDirection threshold:constraintThreshold coordinate:yCoord type:type];
			
			constraintFound = YES;
			
		} else {
			// do nothing (try if we meet the requirements for other alignment guides)
		}
	}
	
	
	// compare side alignments (same sides)
	
	if ([self value:NSMaxX(firstFrame) isBelowThresholdDistanceFrom:NSMaxX(secondFrame) precision:precision]) {
	
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:VerticalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(NSMaxX(secondFrame), fmin(NSMinY(firstFrame), NSMinY(secondFrame)));
			NSPoint secondPoint = NSMakePoint(NSMaxX(secondFrame), fmax(NSMaxY(firstFrame), NSMaxY(secondFrame)));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			// the coordinate for the constraint depends on the align type: when resizing, it is the edge coordinate of the second frame.  when moving, it is the desired origin of the first frame.
			CGFloat xCoord = move ? secondFrame.origin.x + (secondFrame.size.width - firstFrame.size.width) : NSMaxX(secondFrame);
			[self addConstraintWithDirection:VerticalConstraintDirection threshold:constraintThreshold coordinate:xCoord type:type];
			
			constraintFound = YES;
			
		}
	}
	
	if ([self value:NSMinX(firstFrame) isBelowThresholdDistanceFrom:NSMinX(secondFrame) precision:precision]) {
		
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:VerticalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(NSMinX(secondFrame), fmin(NSMinY(firstFrame), NSMinY(secondFrame)));
			NSPoint secondPoint = NSMakePoint(NSMinX(secondFrame), fmax(NSMaxY(firstFrame), NSMaxY(secondFrame)));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat xCoord = move ? secondFrame.origin.x : NSMinX(secondFrame);
			[self addConstraintWithDirection:VerticalConstraintDirection threshold:constraintThreshold coordinate:xCoord type:type];
			
			constraintFound = YES;
			
		}
	}
	
	if ([self value:NSMaxY(firstFrame) isBelowThresholdDistanceFrom:NSMaxY(secondFrame) precision:precision]) {
		
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:HorizontalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(fmin(NSMinX(firstFrame), NSMinX(secondFrame)), NSMaxY(secondFrame));
			NSPoint secondPoint = NSMakePoint(fmax(NSMaxX(firstFrame), NSMaxX(secondFrame)), NSMaxY(secondFrame));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat yCoord = move ? secondFrame.origin.y + (secondFrame.size.height - firstFrame.size.height) : NSMaxY(secondFrame);
			[self addConstraintWithDirection:HorizontalConstraintDirection threshold:constraintThreshold coordinate:yCoord type:type];
			
			constraintFound = YES;
			
		}

	}
	
	if ([self value:NSMinY(firstFrame) isBelowThresholdDistanceFrom:NSMinY(secondFrame) precision:precision]) {
		
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:HorizontalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(fmin(NSMinX(firstFrame), NSMinX(secondFrame)), NSMinY(secondFrame));
			NSPoint secondPoint = NSMakePoint(fmax(NSMaxX(firstFrame), NSMaxX(secondFrame)), NSMinY(secondFrame));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat yCoord = move ? secondFrame.origin.y : NSMinY(secondFrame);
			[self addConstraintWithDirection:HorizontalConstraintDirection threshold:constraintThreshold coordinate:yCoord type:type];
			
			constraintFound = YES;
			
		}
	}
	
	// compare side alignments (opposite sides)
	
	
	if ([self value:NSMaxY(firstFrame) isBelowThresholdDistanceFrom:NSMinY(secondFrame) precision:precision]) {
	
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:HorizontalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(fmin(NSMinX(firstFrame), NSMinX(secondFrame)), NSMinY(secondFrame));
			NSPoint secondPoint = NSMakePoint(fmax(NSMaxX(firstFrame), NSMaxX(secondFrame)), NSMinY(secondFrame));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat yCoord = move ? secondFrame.origin.y - firstFrame.size.height : NSMinY(secondFrame);
			[self addConstraintWithDirection:HorizontalConstraintDirection threshold:constraintThreshold coordinate:yCoord type:type];
			
			constraintFound = YES;
			
		}

		
	}
	
	if ([self value:NSMaxX(firstFrame) isBelowThresholdDistanceFrom:NSMinX(secondFrame) precision:precision]) {
		
			if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:VerticalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(NSMinX(secondFrame), fmin(NSMinY(firstFrame), NSMinY(secondFrame)));
			NSPoint secondPoint = NSMakePoint(NSMinX(secondFrame), fmax(NSMaxY(firstFrame), NSMaxY(secondFrame)));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat xCoord = move ? secondFrame.origin.x - firstFrame.size.width : NSMinX(secondFrame);
			[self addConstraintWithDirection:VerticalConstraintDirection threshold:constraintThreshold coordinate:xCoord type:type];
			
			constraintFound = YES;
			
		}

	}
	
	if ([self value:NSMinY(firstFrame) isBelowThresholdDistanceFrom:NSMaxY(secondFrame) precision:precision]) {
		
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:HorizontalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(fmin(NSMinX(firstFrame), NSMinX(secondFrame)), NSMaxY(secondFrame));
			NSPoint secondPoint = NSMakePoint(fmax(NSMaxX(firstFrame), NSMaxX(secondFrame)), NSMaxY(secondFrame));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat yCoord = move ? secondFrame.origin.y + secondFrame.size.height : NSMaxY(secondFrame);
			[self addConstraintWithDirection:HorizontalConstraintDirection threshold:constraintThreshold coordinate:yCoord type:type];
			
			constraintFound = YES;
			
		}
		
	}
	
	if ([self value:NSMinX(firstFrame) isBelowThresholdDistanceFrom:NSMaxX(secondFrame) precision:precision]) {
		
		if ([self frame:firstFrame isBelowProximityDistanceFromFrame:secondFrame direction:VerticalDirection]) {
			
			NSPoint firstPoint = NSMakePoint(NSMaxX(secondFrame), fmin(NSMinY(firstFrame), NSMinY(secondFrame)));
			NSPoint secondPoint = NSMakePoint(NSMaxX(secondFrame), fmax(NSMaxY(firstFrame), NSMaxY(secondFrame)));
			[self setGuideExcludingMarginsFromPoint:firstPoint toPoint:secondPoint];
			
			CGFloat xCoord = move ? secondFrame.origin.x + secondFrame.size.width : NSMaxX(secondFrame);
			[self addConstraintWithDirection:VerticalConstraintDirection threshold:constraintThreshold coordinate:xCoord type:type];
			
			constraintFound = YES;
			
		}
	}

		
	return constraintFound;	
}


- (void)drawWithLineWidth:(CGFloat)width inView:(NSView *)view
{
	
	NSSize widthSize = NSMakeSize(.5, 1);
	widthSize = [view convertSize:widthSize fromView:nil];
	
	// draw the guide this element represents in the locked on view.
	NSDictionary *defaults = [[NSUserDefaultsController sharedUserDefaultsController] values];
	NSColor *color = [NSUnarchiver unarchiveObjectWithData:[defaults valueForKey:@"layoutGuideColor"]];
	[color set];
	[[NSGraphicsContext currentContext] setShouldAntialias:NO];
	for (NSBezierPath *path in guideBeziers) {
		NSBezierPath *copy = [[path copy] autorelease];
		NSPoint currentElementPoint[3];
		
		int i;
		for (i = 0; i < [path elementCount]; i++) {
			
			[path elementAtIndex:i associatedPoints:currentElementPoint];
			NSPoint basePoint = [view convertPointToBase:currentElementPoint[0]];
			basePoint.x = floor(basePoint.x) + 0.5;
			basePoint.y = floor(basePoint.y) + 0.5;
			currentElementPoint[0] = [view convertPointFromBase:basePoint];
			
			[copy setAssociatedPoints:currentElementPoint atIndex:i];
		}
		
		// small enough so it measures 1px at each zoom level
		[copy setLineWidth:width * 0.5];
		[copy stroke];
	}
	
	[[NSGraphicsContext currentContext] setShouldAntialias:YES];
}

- (NSRect)bounds
{
	// returns the bounding box (union rect) of the guide bounding boxes of this element.
	// since the bezier line paths return rects with zero dimension in the direction perpendicular to the line, we need to adjust this dimension and the corresponding origin coordinate.
	NSRect unionRect = NSZeroRect;
	for (NSBezierPath *path in guideBeziers) {
		NSRect bounds = [path bounds];
		
		if (bounds.size.width == 0) {
			bounds.size.width = 3;
			bounds.origin.x--;
		}
		if (bounds.size.height == 0) {
			bounds.size.height = 3;
			bounds.origin.y--;
		}
		unionRect = NSUnionRect(bounds, unionRect);
	}
	return unionRect;	
}

#pragma mark Convenience methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Convenience methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)addConstraintWithDirection:(ConstraintDirection)theDirection threshold:(CGFloat)theThreshold coordinate:(CGFloat)theCoordinate type:(ConstraintType)type
{
	LightTableAlignConstraint *newConstraint = [[[LightTableAlignConstraint alloc] init] autorelease];
	newConstraint.threshold = theThreshold;
	newConstraint.coordinate = theCoordinate;
	newConstraint.direction = theDirection;
	newConstraint.type = type;
	//NSLog(@"add ct:%@", [newConstraint description]);
	[self.constraints addObject:newConstraint];
}

- (void)removeConstraints
{
	[constraints removeAllObjects];
}

- (void)setGuideExcludingMarginsFromPoint:(NSPoint)first toPoint:(NSPoint)second
{
	// apply a margin to the line formed by the given points, before invoking setGuideFromPoint:toPoint:
	
	BOOL verticalGuide = ([self value:first.x isBelowThresholdDistanceFrom:second.x precision:StandardPrecision]);
	NSPoint delta = NSMakePoint( verticalGuide ? 0 : guideEndingMargin / self.zoomLevel, verticalGuide ? guideEndingMargin / self.zoomLevel : 0);
	NSPoint highestPoint, lowestPoint;
	if (verticalGuide) {
		highestPoint = (first.y > second.y) ? first : second;
		lowestPoint = (first.y > second.y) ? second : first;
	} else {
		highestPoint = (first.x > second.x) ? first : second;
		lowestPoint = (first.x > second.x) ? second : first;
	}
	highestPoint.x += delta.x;
	highestPoint.y += delta.y;
	
	lowestPoint.x -= delta.x;
	lowestPoint.y -= delta.y;
	
	[self setGuideFromPoint:lowestPoint toPoint:highestPoint];
	
}

- (void)setGuideFromPoint:(NSPoint)first toPoint:(NSPoint)second
{
	// set the guide to be the line between the given two points.
	NSBezierPath *path = [NSBezierPath bezierPath];
	[path setLineDash:guideDashPattern count:4 phase:0];
	[path moveToPoint:first];
	[path lineToPoint:second];
	
	[self addGuide:path];
}

- (void)addGuide:(NSBezierPath *)path
{
	[guideBeziers addObject:path];
}


#pragma mark Measuring utilities
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Measuring utilities
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (BOOL)frame:(NSRect)first isBelowProximityDistanceFromFrame:(NSRect)second direction:(Direction)direction
{
	// determine if the frames are close enough to consider guides
	
	if (NSIntersectsRect(first, second)) return YES;
	if (direction == VerticalDirection) {
		if ([self value:NSMaxY(first) isBelowProximityDistanceFrom:NSMaxY(second)]) return YES;
		if ([self value:NSMaxY(first) isBelowProximityDistanceFrom:NSMinY(second)]) return YES;
		if ([self value:NSMinY(first) isBelowProximityDistanceFrom:NSMaxY(second)]) return YES;
		if ([self value:NSMinY(first) isBelowProximityDistanceFrom:NSMinY(second)]) return YES;
	} else {
		if ([self value:NSMaxX(first) isBelowProximityDistanceFrom:NSMaxX(second)]) return YES;
		if ([self value:NSMaxX(first) isBelowProximityDistanceFrom:NSMinX(second)]) return YES;
		if ([self value:NSMinX(first) isBelowProximityDistanceFrom:NSMaxX(second)]) return YES;
		if ([self value:NSMinX(first) isBelowProximityDistanceFrom:NSMinX(second)]) return YES;
	}
	return NO;
}


- (BOOL)value:(CGFloat)first isBelowThresholdDistanceFrom:(CGFloat)second precision:(LayoutPrecision)precision
{
	// specifies whether two point coords are close enough to eachother to apply a layout guide.
	CGFloat thresholdDistance;
	switch (precision) {
		case StandardResizePrecision:
			thresholdDistance = standardResizeThreshold / self.zoomLevel;
			break;
		case StandardPrecision:
			thresholdDistance = standardThreshold / self.zoomLevel;
			break;
		case HighPrecision:
			thresholdDistance = precisionThreshold / self.zoomLevel;
			break;
	}
	
	return (fabs(first - second) <= thresholdDistance);
}

- (BOOL)value:(CGFloat)first isBelowProximityDistanceFrom:(CGFloat)second
{
	// specifies whether two point coords are close enough to eachother to compare their frames for possible layout guides
	return (fabs(first - second) <= proximityDistance);
}


@end
