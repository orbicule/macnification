//
//  MGImageFilters.m
//  Filament
//
//  Created by Dennis Lorson on 22/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGImageFilters.h"



void MGRegisterImageFilters()
{
    [ThresholdCompositorFilter registerFilterName];
    [ThresholdFilter registerFilterName];
    [LUTColormapFilter registerFilterName];
    [DODGaussianBlurFilter registerFilterName];
    [ColorspaceConversionFilter registerFilterName];
    [InvertFilter registerFilterName];
    [WhiteBalanceCorrectionFilter registerFilterName];

}


@implementation MGRegisteredFilter

+ (void)registerFilterName
{
    NSString *filterName = NSStringFromClass([self class]);
	
    [CIFilter registerFilterName:filterName constructor:(id<CIFilterConstructor>)self classAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
																			  filterName, kCIAttributeFilterDisplayName,
																			  [NSArray arrayWithObjects:kCICategoryColorAdjustment, nil], kCIAttributeFilterCategories,
																			  nil]];
}


@end


#pragma mark -


@implementation ThresholdCompositorFilter

static CIKernel *_ThresholdCompositorFilterKernel = nil;

- (id)init
{
    if(_ThresholdCompositorFilterKernel == nil)
    {
		NSBundle    *bundle = [NSBundle mainBundle];
		NSString    *code = [NSString stringWithContentsOfFile:[bundle pathForResource:@"ThresholdCompositorFilterKernel" ofType:@"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
		NSArray     *kernels = [CIKernel kernelsWithString:code];
        
		_ThresholdCompositorFilterKernel = [[kernels objectAtIndex:0] retain];
    }
    return [super init];
}

/*
- (CGRect)regionOf: (int)sampler  destRect: (CGRect)rect  userInfo: (NSNumber *)radius
{
    return CGRectInset(rect, -[radius floatValue], 0);
}*/


- (NSDictionary *)customAttributes
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            
            [NSDictionary dictionaryWithObjectsAndKeys:
             nil],                               @"inputBinaryImage",
            
            [NSDictionary dictionaryWithObjectsAndKeys:
             [NSNumber numberWithBool:YES],		kCIAttributeDefault,
             kCIAttributeTypeBoolean,			kCIAttributeType,
			 nil],                               @"inputIsActive",
			
            [NSDictionary dictionaryWithObjectsAndKeys:
             [CIColor colorWithRed:1 green:1 blue:1 alpha:1], kCIAttributeDefault,
             kCIAttributeTypeOpaqueColor,           kCIAttributeType,
             nil],                               @"inputOverlayColor",
            
            nil];
}

// called when setting up for fragment program and also calls fragment program
- (CIImage *)outputImage
{
	if (![inputIsActive boolValue]) return inputImage;
    
    CISampler *src = [CISampler samplerWithImage:inputImage];
    CISampler *binary = [CISampler samplerWithImage:inputBinaryImage];
    
    return [self apply:_ThresholdCompositorFilterKernel, src, binary, inputOverlayColor, kCIApplyOptionDefinition, [src definition] , nil];
}

@end



#pragma mark -


@implementation ThresholdFilter

static CIKernel *_ThresholdFilterKernel = nil;

- (id)init
{
    if(_ThresholdFilterKernel == nil)
    {
		NSBundle    *bundle = [NSBundle mainBundle];
		NSString    *code = [NSString stringWithContentsOfFile:[bundle pathForResource:@"ThresholdFilterKernel" ofType:@"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
		NSArray     *kernels = [CIKernel kernelsWithString:code];
        
		_ThresholdFilterKernel = [[kernels objectAtIndex:0] retain];
    }
    return [super init];
}


- (CGRect)regionOf: (int)sampler  destRect: (CGRect)rect  userInfo: (NSNumber *)radius
{
    return rect;
}


- (NSDictionary *)customAttributes
{
	// the inputThreshold order does not matter: the filter will use the lowest number as lower threshold.
	// <inputBlockRange> signifies whether to allow or block the specified range.
	
    return [NSDictionary dictionaryWithObjectsAndKeys:
			
			[NSDictionary dictionaryWithObjectsAndKeys:
			 [CIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0], kCIAttributeDefault,
			 kCIAttributeTypeOpaqueColor,			kCIAttributeType,
			 nil],                               @"inputLowerThreshold",
            
			[NSDictionary dictionaryWithObjectsAndKeys:
			 [CIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0], kCIAttributeDefault,
			 kCIAttributeTypeOpaqueColor,			kCIAttributeType,
			 nil],                               @"inputUpperThreshold",
			
			[NSDictionary dictionaryWithObjectsAndKeys:
			 [CIColor colorWithRed:1 green:1 blue:1 alpha:1.0], kCIAttributeDefault,
			 kCIAttributeTypeOpaqueColor,			kCIAttributeType,
			 nil],                               @"inputBlockVector",
            
			
			[NSDictionary dictionaryWithObjectsAndKeys:
			 [NSNumber numberWithDouble:  0.00], kCIAttributeMin,
			 [NSNumber numberWithDouble:  0.00], kCIAttributeSliderMin,
			 [NSNumber numberWithDouble:  20.00], kCIAttributeSliderMax,
			 [NSNumber numberWithDouble:  0.00], kCIAttributeDefault,
			 [NSNumber numberWithDouble:  0.00], kCIAttributeIdentity,
			 kCIAttributeTypeScalar,           kCIAttributeType,
			 nil],                               @"inputBlurRadius",
			
			
			nil];
}


- (CIImage *)outputImage
{
	
	CISampler *src = nil;
	CIImage *thresholded = nil;
	
	if ([inputBlurRadius floatValue] > 0) {
        
		CIFilter *blur = [CIFilter filterWithName:@"DODGaussianBlurFilter"];
		[blur setValue:inputBlurRadius forKey:@"inputRadius"];
		[blur setValue:inputImage forKey:@"inputImage"];
		
		src = [CISampler samplerWithImage:[blur valueForKey:@"outputImage"]];
		
	} else {
		
		src = [CISampler samplerWithImage:inputImage];
		
	}
	
	thresholded = [self apply:_ThresholdFilterKernel, src, inputLowerThreshold, inputUpperThreshold, inputBlockVector, kCIApplyOptionDefinition, [src definition], nil];
	
	return thresholded;
	
}

@end

#pragma mark -



@interface LUTColormapFilter ()

- (NSString *)adjustFilePathIfNeeded:(NSString *)original;
- (CIImage *)lut;

@end


@implementation LUTColormapFilter

static CIKernel *_LUTColormapFilterKernel = nil;

- (id)init
{
    if(_LUTColormapFilterKernel == nil)
    {
		NSBundle    *bundle = [NSBundle mainBundle];
		NSString    *code = [NSString stringWithContentsOfFile:[bundle pathForResource:@"LUTColormapFilterKernel" ofType:@"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
		NSArray     *kernels = [CIKernel kernelsWithString:code];
        
		_LUTColormapFilterKernel = [[kernels objectAtIndex:0] retain];
    }
    return [super init];
}


- (void) dealloc
{
	[lut release];
	
	[super dealloc];
}


- (NSDictionary *)customAttributes
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            
            [NSDictionary dictionaryWithObjectsAndKeys:
             
             nil],                               @"inputLUTPath",
            
            nil];
}

- (CGRect) regionOf:(int)samplerIndex destRect:(CGRect)r userInfo:obj;
{
	if (samplerIndex == 0) {
		// the image
		return r;
		
	} else {
		
		return CGRectMake(0, 0, 256, 1);
		
	}
	
	
}

// called when setting up for fragment program and also calls fragment program
- (CIImage *)outputImage
{
	if (![self lut])
		return nil;
	
    CISampler *srcSampler = [CISampler samplerWithImage:inputImage];
	CISampler *lutSampler = [CISampler samplerWithImage:[self lut]];
    
    
    return [self apply:_LUTColormapFilterKernel, srcSampler, lutSampler, kCIApplyOptionDefinition, [srcSampler definition], nil];
}


- (CIImage *)lut
{
	if (lut)
		return lut;
	
	if (!inputLUTPath)
		return nil;
	
	if (![self importLUT:inputLUTPath])
		return nil;
	
	
	// generate lookup table image
	NSInteger width = 260;
	unsigned char *buff = (unsigned char *) malloc(width * 4);
	
	int i, j;
	for (i = 0; i < width; i++) {
		//alpha
		buff[i * 4] = 255;
		
		for (j = 1; j < 4; j++) {
			buff[i * 4 + j] = i < 256 ? colormap[i][j - 1] : 255.;
		}		
	}
	
	// takes ownership
	NSData *data = [NSData dataWithBytesNoCopy:buff length:width * 4 freeWhenDone:YES];
	CIImage *lutImage = [CIImage imageWithBitmapData:data bytesPerRow:width * 4 size:CGSizeMake(width,1) format:kCIFormatARGB8 colorSpace:CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB)];
	
	lut = [lutImage retain];
	
	return lut;
}



- (BOOL)importLUT:(NSString *)file
{	
	file = [self adjustFilePathIfNeeded:file];
	
	if (![self importAsciiLUT:file]) {
		if (![self importBinaryLUT:file hasHeader:NO]) {
			if (![self importBinaryLUT:file hasHeader:YES]) {
				return NO;
			}
		}
	}
	return YES;
}

- (NSString *)adjustFilePathIfNeeded:(NSString *)original
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:original])
		return original;
	
	NSString *filename = [original lastPathComponent];
	
	NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
	
	NSString *lutPath = [resourcePath stringByAppendingPathComponent:@"LUT"];
	
	NSArray *subdirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:lutPath error:nil];
	
	for (NSString *subdir in subdirs) {
		
		NSString *subdirpath = [lutPath stringByAppendingPathComponent:subdir];
		
		BOOL isDir = NO;
		[[NSFileManager defaultManager] fileExistsAtPath:subdirpath isDirectory:&isDir];
		
		if (!isDir)
			continue;
		
		NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:subdirpath error:nil];
        
		for (NSString *file in files) {
			
			if ([file isEqualTo:filename])
				return [subdirpath stringByAppendingPathComponent:file];
			
		}
	}
	
	return nil;
}


- (BOOL)importBinaryLUT:(NSString *)file hasHeader:(BOOL)hasHeader
{
	NSData *data = [NSData dataWithContentsOfFile:file];
	
	if (!data) return NO;
	
	unsigned char *colorBuffer;
	
	if (hasHeader) {
		
		if ([data length] != 800) {
			return NO;
		}
		
		
		colorBuffer = (unsigned char *)[data bytes];
		
		int32_t header = *(((int32_t *)colorBuffer));
		
		if (header != 1280262985) {
			return NO;
		}
		
		colorBuffer = &colorBuffer[32];
		
	} else {
		
		if ([data length] != 768) {
			return NO;
		}
		
		colorBuffer = (unsigned char *)[data bytes];
	}
	
	
	
	int i,j;
	for (i = 0; i < 256; i++) {
		for (j = 0; j < 3; j++) {
			colormap[i][j] = (int32_t)(colorBuffer[j * 256 + i]);
		}
	}
	
	return YES;
}

- (BOOL)importAsciiLUT:(NSString *)file
{
	NSString *fileString = [NSString stringWithContentsOfFile:file encoding:NSISOLatin1StringEncoding error:nil];
	
	if (!fileString) return NO;
	
	NSArray *lines = [fileString componentsSeparatedByString:@"\n"];
	
	if ([lines count] < 256) {
		return NO;
	}
	
	NSMutableArray *lineComponents = [NSMutableArray arrayWithCapacity:255];
	
	NSInteger currentLine = 0;
	for (NSString *line in lines) {
		
		if (currentLine > 255) break;
		
		NSString *adjLine = [line stringByReplacingOccurrencesOfString:@"  " withString:@" "];
		adjLine = [line stringByReplacingOccurrencesOfString:@"   " withString:@" "];
		adjLine = [line stringByReplacingOccurrencesOfString:@"    " withString:@" "];
		
		adjLine = [adjLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		
		NSArray *channelComponents = [adjLine componentsSeparatedByString:@" "];
		
		if ([channelComponents count] < 3) {
			
			channelComponents = [adjLine componentsSeparatedByString:@" "];
			
			if ([channelComponents count] < 3) {
				
				return NO;
				
			}
		}
		
		// remove whitespaces
		NSMutableArray *copy = [NSMutableArray array];
		for (NSString *comp in channelComponents) {
			
			if ([comp rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound)
				[copy addObject:comp];
			
		}
		
		
		[lineComponents addObject:copy];
		currentLine++;
	}
	
	int i, j;
	for (i = 0; i < 256; i++) {
		for (j = 0; j < 3; j++) {
			NSString *comp = [[lineComponents objectAtIndex:i] objectAtIndex:j];
			comp = [comp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			colormap[i][j] = [comp integerValue];
		}
	}
	
	return YES;
}





@end

#pragma mark -


@implementation DODGaussianBlurFilter


- (CGRect)regionOf: (int)sampler  destRect: (CGRect)rect  userInfo: (NSNumber *)radius
{
    return CGRectInset(rect, 0, 0);
}


- (NSDictionary *)customAttributes
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            
            
            [NSDictionary dictionaryWithObjectsAndKeys:
             [NSNumber numberWithDouble:  0.00], kCIAttributeMin,
             [NSNumber numberWithDouble:  0.00], kCIAttributeSliderMin,
             [NSNumber numberWithDouble:100.00], kCIAttributeSliderMax,
             [NSNumber numberWithDouble:1.00], kCIAttributeDefault,
             [NSNumber numberWithDouble:0.00], kCIAttributeIdentity,
             kCIAttributeTypeDistance,           kCIAttributeType,
             nil],                               @"inputRadius",
            
            
            
            nil];
}

// called when setting up for fragment program and also calls fragment program
- (CIImage *)outputImage
{
	CGRect cropRect = [inputImage extent];
	
    
	CIFilter *stdBlur = [CIFilter filterWithName:@"CIGaussianBlur"];
	[stdBlur setValue:inputImage forKey:@"inputImage"];
	[stdBlur setValue:inputRadius forKey:@"inputRadius"];
    
	// Add a crop with the original image extent!
	//CIVector *cropVector = [CIVector vectorWithX:0 Y:0 Z:originalImageRect.size.width + 2 * originalImageRect.origin.x W:originalImageRect.size.height + 2 * originalImageRect.origin.y];
	CIVector *cropVector = [CIVector vectorWithX:cropRect.origin.x Y:cropRect.origin.y Z:cropRect.size.width W:cropRect.size.height];
	
	CIFilter *cropFilter = [CIFilter filterWithName:@"CICrop"];
	[cropFilter setValue:[stdBlur valueForKey:@"outputImage"] forKey:@"inputImage"];
	[cropFilter setValue:cropVector forKey:@"inputRectangle"];
	return [cropFilter valueForKey:@"outputImage"];
	
}

@end


#pragma mark -


@implementation ColorspaceConversionFilter

static CIKernel *rgb2hslKernel = nil, *hsl2rgbKernel = nil, *rgb2grayKernel = nil;

- (id)init
{
    if(rgb2hslKernel == nil)
    {
		NSBundle    *bundle = [NSBundle mainBundle];
		NSString    *code = [NSString stringWithContentsOfFile:[bundle pathForResource:@"ColorspaceConversionFilterKernel" ofType:@"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
		NSArray     *kernels = [CIKernel kernelsWithString:code];
        
		rgb2hslKernel = [[kernels objectAtIndex:0] retain];
		hsl2rgbKernel = [[kernels objectAtIndex:1] retain];
		rgb2grayKernel = [[kernels objectAtIndex:2] retain];  
        
    }
    return [super init];
}



- (NSDictionary *)customAttributes
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            
            [NSDictionary dictionaryWithObjectsAndKeys:
             [NSNumber numberWithDouble:  1], kCIAttributeMin,
             [NSNumber numberWithDouble:  1], kCIAttributeSliderMin,
             [NSNumber numberWithDouble:  3], kCIAttributeSliderMax,
             [NSNumber numberWithDouble:  1], kCIAttributeDefault,
             [NSNumber numberWithDouble:  1], kCIAttributeIdentity,
             kCIAttributeTypeCount,           kCIAttributeType,
             nil],                               @"inputConversionType",
            
            nil];
}

// called when setting up for fragment program and also calls fragment program
- (CIImage *)outputImage
{
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:kCISamplerFilterNearest, kCISamplerFilterMode, nil];
    CISampler *src = [CISampler samplerWithImage:inputImage options:options];
	CIKernel *kernel;
	
	switch ([inputConversionType intValue]) {
			
		case RGB2HSLConversion:
			kernel = rgb2hslKernel;
			break;
		case HSL2RGBConversion:
			kernel = hsl2rgbKernel;
			break;
		case RGB2GrayConversion:
		default:
			kernel = rgb2grayKernel;
			break;
			
	}
	
	return [self apply:kernel, src, kCIApplyOptionDefinition, [src definition], nil];
    
	
}

@end

#pragma mark -


@implementation InvertFilter

static CIKernel *invertKernel = nil;

- (id)init
{
    if (invertKernel == nil) {
		NSBundle    *bundle = [NSBundle mainBundle];
        NSString    *code = [NSString stringWithContentsOfFile: [bundle pathForResource:@"invert" ofType: @"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
        NSArray     *kernels = [CIKernel kernelsWithString: code];
        invertKernel = [[kernels objectAtIndex:0] retain];
    }
	
    return [super init];
}



- (NSDictionary *)customAttributes;
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
			@"InvertFilter", kCIAttributeFilterName,
			[NSDictionary dictionaryWithObjectsAndKeys:
			 [NSNumber numberWithDouble:  0.00], kCIAttributeMin,
			 [NSNumber numberWithDouble:  1.00], kCIAttributeMax,
			 [NSNumber numberWithDouble:  0.00], kCIAttributeSliderMin,
			 [NSNumber numberWithDouble:  1.00], kCIAttributeSliderMax,
			 [NSNumber numberWithDouble:  0.00], kCIAttributeDefault,
			 [NSNumber numberWithDouble:  0.00], kCIAttributeIdentity,
			 kCIAttributeTypeScalar,            kCIAttributeType,
			 nil],                              @"inputEnabled",
			nil];
}




- (CIImage *)outputImage;
{
	// Convert the NSNumber values to floats and back to NSNumbers (to make the interface work).
	NSNumber *enabled = [NSNumber numberWithFloat:[inputEnabled floatValue]];
	CISampler *src = [CISampler samplerWithImage: inputImage];
    return [self apply: invertKernel, src, enabled, kCIApplyOptionDefinition, [src definition], nil];
}



@end

#pragma mark -


@implementation WhiteBalanceCorrectionFilter

static CIKernel *whiteBalanceKernel = nil;

- (id)init
{
    if(whiteBalanceKernel == nil)
    {
		NSBundle    *bundle = [NSBundle mainBundle];
        NSString    *code = [NSString stringWithContentsOfFile: [bundle pathForResource:@"whiteBalanceCorrection" ofType: @"cikernel"] encoding:NSISOLatin1StringEncoding error:nil];
        NSArray     *kernels = [CIKernel kernelsWithString: code];
        whiteBalanceKernel = [[kernels objectAtIndex:0] retain];
    }
	
    return [super init];
}



- (NSDictionary *)customAttributes;
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
			@"WhiteBalanceCorrectionFilter", kCIAttributeFilterName,
			[NSDictionary dictionaryWithObjectsAndKeys:
			 [CIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0], kCIAttributeDefault, nil], @"inputColor", nil];
}




- (CIImage *)outputImage;
{
	// Convert the NSNumber values to floats and back to NSNumbers (to make the interface work).
	//NSNumber *threshold = [NSNumber numberWithFloat:[inputThreshold floatValue]];
	CISampler *src = [CISampler samplerWithImage: inputImage];
    return [self apply: whiteBalanceKernel, src, inputColor, kCIApplyOptionDefinition, [src definition], nil];
}



@end


