//
//  MGMDChannelView.m
//  MetaData
//
//  Created by Dennis Lorson on 24/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDChannelView.h"
#import "MGMDConstants.h"
#import "KSExtensibleManagedObject.h"
#import "ImageArrayController.h"


@implementation MGMDChannelView


@synthesize channelsForCurrentSelection;

#pragma mark -
#pragma mark Init/dealloc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init/dealloc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)awakeFromNib
{
	
	channelAttributeKeys = [[NSMutableArray arrayWithObjects:@"channelName", @"contrastMethod", @"gain", @"lightSourceName", @"lightSourceType", @"medium",
							 @"mode", @"offset", @"pinholeSize", @"power", @"wavelength", nil] retain];
	channelAttributeNames = [[NSMutableArray arrayWithObjects:@"Name", @"Contrast Method", @"Gain", @"Light Source (Name)", @"Light Source (Type)", @"Medium",
							  @"Mode", @"Offset", @"Pinhole Size", @"Power", @"Wavelength", nil] retain];
	
	
	formatters = [[NSMutableDictionary dictionary] retain];
	
	[self setFormatter:gainFormatter forKey:@"gain"];
	[self setFormatter:wavelengthFormatter forKey:@"wavelength"];
	[self setFormatter:pinholeSizeFormatter forKey:@"pinholeSize"];
	[self setFormatter:offsetFormatter forKey:@"offset"];
	[self setFormatter:powerFormatter forKey:@"power"];
	
	
}

- (void)dealloc
{
	[formatters release];
	
	[channelAttributeKeys release];
	[channelAttributeNames release];
		
	[super dealloc];
}




#pragma mark -
#pragma mark Accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)setFormatter:(NSFormatter *)formatter forKey:(NSString *)key
{
	[formatters setValue:formatter forKey:key];
	
}

- (IBAction)changeChannelName:(id)sender
{
	[self setChannelName:[sender titleOfSelectedItem]];
	
	[self updateChannelsForCurrentSelection];

}

- (void)setChannelName:(NSString *)aName
{
	if (channelName != aName) {
		
		[channelName release];
		channelName = [aName retain];
	}
	
}

- (NSString *)channelName
{
	return channelName;
}

- (void)setEnabled:(BOOL)flag
{
	if (!flag) {
		
		// disable the subviews but retain their original state (if they were unavailable due to bindings, don't let them be enabled again when this method is invoked.	
		subviewsEnabled = [channelAttributeTable isEnabled];
		
		[channelAttributeTable setEnabled:flag];
		[channelPopUp setEnabled:flag];
		
	} else {
		
		[channelAttributeTable setEnabled:subviewsEnabled];
		[channelPopUp setEnabled:subviewsEnabled];
		
	}
	
}

- (NSString *)uniqueChannelNameForSuggestedName:(NSString *)suggestedName
{
	// returns a name for a new channel that is unique in the current selection
	
	// check if no channel has the "untitled channel" name, if it does, add a "untitled x" channel
	NSInteger channelNameSuffixNumber = 1;
	NSString *newChannelName = suggestedName;
	BOOL isUniqueName = NO;
	while (!isUniqueName) {
		
		if (channelNameSuffixNumber > 1) 
			newChannelName = [NSString stringWithFormat:@"%@ %i", suggestedName, (int)channelNameSuffixNumber];
		
		
		
		// the "add channel" functionality will only be available when all selected images share the same channels, so we only need to check the channels in one of the images
		id firstImage = [[observableController selectedObjects] objectAtIndex:0];
		NSSet *channelsOfSelectedImage = [firstImage valueForKeyPath:@"instrument.channels"];
		NSArray *channelsWithSameName = [[channelsOfSelectedImage allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"channelName like %@", newChannelName]];
		
		isUniqueName = ([channelsWithSameName count] == 0);
		
		channelNameSuffixNumber++;
	}
	

	return newChannelName;
	
}


- (IBAction)addChannel:(id)sender
{
	// get all the possible instruments that belong to the image selection
	NSMutableSet *allInstruments = [NSMutableSet set];
	
	for (id image in [observableController selectedObjects]) {
		
		[allInstruments addObject:[image valueForKey:@"instrument"]];
	
	}
	
	NSString *newChannelName = [self uniqueChannelNameForSuggestedName:@"untitled channel"];
	
	// after the bindings update, we'll try to select this channel!
	[self setChannelName:newChannelName];
	
	// add a new channel to each of those instruments
	// this is wasteful but it is a compromise solution.
	
	for (id instrument in allInstruments) {
		
		KSExtensibleManagedObject *newChannel = [[[KSExtensibleManagedObject alloc] initWithEntity:[NSEntityDescription entityForName:@"Channel" inManagedObjectContext:[observableController managedObjectContext]]
												insertIntoManagedObjectContext:[observableController managedObjectContext]] autorelease];
		
		[newChannel setValue:newChannelName forKey:@"channelName"];
		
		NSMutableSet *channels = [instrument mutableSetValueForKeyPath:@"channels"];
		[channels addObject:newChannel];
		[instrument setValue:channels forKeyPath:@"channels"];
		
		
	}
}

- (IBAction)removeSelectedChannel:(id)sender
{
	// just remove this channel from the whole data model
	// TODO: should this just be the relationship between the two???

	NSArray *channelsToBeRemoved = self.channelsForCurrentSelection;
	
	if (channelsToBeRemoved) {
		
		int code = NSRunAlertPanel(@"Remove the selected channel?", 
								   @"If you choose \"Remove\", it will be removed from every image that has the channel as its property.",
								   @"Remove", @"Cancel", nil);
		
		if (code == NSAlertDefaultReturn) {
			
			for (id channel in channelsToBeRemoved)
				[[channel managedObjectContext] deleteObject:channel];

		}
		
	}
	
}




#pragma mark -
#pragma mark Bindings
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bindings
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




- (void)bind:(NSString *)binding toObject:(id)ctl withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		
		observableController = ctl;
		observableKeyPath = [keyPath retain];

		[(ImageArrayController *)observableController addManualObserver:self forKeyPath:keyPath];
		[observableController addObserver:self forKeyPath:@"selection" options:0 context:nil];
		
		[channelAttributeTable performSelector:@selector(reloadData) withObject:nil afterDelay:0.2];

		
	} else {
		
		[super bind:binding toObject:observableController withKeyPath:keyPath options:options];
				
	}
	
}

- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object
{
	// forward this to our standard observer method
	[self observeValueForKeyPath:keyPath ofObject:object change:0 context:nil];
	
	
}


- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		
		[observableController removeObserver:self forKeyPath:@"selection"];
		[(ImageArrayController *)observableController removeManualObserver:self];
		
		[observableKeyPath release];
		observableKeyPath = nil;
	}
	else
		[super unbind:binding];
}

- (void)setDisplayValueWithModelValue:(id)modelValue atKeyPath:(NSString *)keyPath ofObject:(id)object;
{
	
	
	
}

- (void)updateChannelsForCurrentSelection
{
	if (![self channelName]) {
		
		self.channelsForCurrentSelection = [NSArray array];
		return;
		
	} 
	
	// we have a unique channel name, and all objects selected should have a channel object with this name
	NSPredicate *channelPredicate = [NSPredicate predicateWithFormat:@"channelName like[c] %@", [self channelName]];
	
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	[req setEntity:[NSEntityDescription entityForName:@"Channel" inManagedObjectContext:[observableController managedObjectContext]]];
	[req setPredicate:channelPredicate];
	
	NSArray *channels = [[observableController managedObjectContext] executeFetchRequest:req error:nil];
	
	NSMutableArray *channelsInSelection = [NSMutableArray array];
	
	// before returning a channel in the array, make sure it has an image in the current selection
	for (id channel in channels) {
		
		for (id image in [observableController selectedObjects]) {
			
			if ([[image valueForKeyPath:@"instrument.channels"] containsObject:channel]) 		
				
				[channelsInSelection addObject:channel];
			
		}
		
	}
	
	self.channelsForCurrentSelection = channelsInSelection;
	
	[channelAttributeTable reloadData];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == observableController) {
		
		// update the possible channels in the popup button
		if ([[observableController selectedObjects] count] == 0) {
			
			// disable the button and set a placeholder string
			NSMenu *menu = [[[NSMenu alloc] init] autorelease];
			[menu addItem:[[[NSMenuItem alloc] initWithTitle:MGMDNoSelectionString action:nil keyEquivalent:@""] autorelease]];
			
			[channelPopUp setMenu:menu];
			[channelPopUp setEnabled:NO];
			
			[channelAttributeTable setEnabled:NO];
			
			[addButton setEnabled:NO];
			[removeButton setEnabled:NO];
			
			
		} else {
			
			NSMenu *menu = [[[NSMenu alloc] init] autorelease];

			NSSet *channelNames = [observableController valueForKeyPath:@"selection.instrument.channels.channelName"];
			
			
			if (channelNames == NSMultipleValuesMarker) {
				
				// this occurs if multiple strings are returned., e.g. one image has an array with "channel 1", another image has no channel
				// returning the intersection is not appropriate because the user may think that suddenly, less channels are available
				
				NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:@"Multiple values" action:nil keyEquivalent:@""] autorelease];
				
				[menu addItem:item];
				[channelPopUp setEnabled:NO];
				[channelAttributeTable setEnabled:NO];
				[addButton setEnabled:NO];
				[removeButton setEnabled:NO];
				[self setChannelName:nil];
				
				[channelPopUp setMenu:menu];
				
			} else {
				
				
				
				
				// sort the channels, as we would like the options to appear in the same order every time to ensure consistency
				NSSortDescriptor *desc = [[[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES] autorelease];
				NSArray *sortedChannels = [[channelNames allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:desc]];
				
				//NSLog([sortedChannels description]);

				
				for (NSString *channel in sortedChannels) {
					
					NSMenuItem *item = [[[NSMenuItem alloc] initWithTitle:channel action:nil keyEquivalent:@""] autorelease];
					//[item setRepresentedObject:channel];
					[menu addItem:item];
					
					
				}
				
				if ([sortedChannels count] == 0) {
					
					[menu addItem:[[[NSMenuItem alloc] initWithTitle:MGMDNullValueString action:nil keyEquivalent:@""] autorelease]];
					[channelPopUp setEnabled:NO];
					
					[channelAttributeTable setEnabled:NO];
					
					[channelPopUp setMenu:menu];
					
					[addButton setEnabled:YES];
					[removeButton setEnabled:NO];
					
				} else {
					
					[addButton setEnabled:YES];
					[removeButton setEnabled:YES];
					
					[channelPopUp setEnabled:YES];
					[channelAttributeTable setEnabled:YES];
					
					[channelPopUp setMenu:menu];
					
					// can we restore the previously selected channel (based on its name)?					
					NSString *previousName = [self channelName];
					NSArray *filteredSortedChannels = [sortedChannels filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self like %@", previousName]];
					BOOL previousChannelNameInNewArray = ([filteredSortedChannels count] != 0);
					// if we don't have a channel name yet, set it to the first one in the array
					if (!previousChannelNameInNewArray) {
						
						[self setChannelName:[sortedChannels objectAtIndex:0]];
						
						//NSLog(@"previous channel name not found in new image selection", previousName);

						
					} else {
						
						//NSLog(@"previous name %@ in new image selection too, reselecting", previousName);

												
						// set the popup selection too
						[channelPopUp selectItemWithTitle:previousName];
						
					}
						
				}
				
			}
			
		}
		
		[self updateChannelsForCurrentSelection];
		
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (id)typeWithKey:(NSString *)key fromString:(NSString *)original
{
	// convert the given string into the correct type using the supplied formatter
	NSFormatter *formatter;
	
	if (!(formatter = [formatters valueForKey:key])) return original;
		
	NSNumberFormatter *nbFormatter = (NSNumberFormatter *)formatter;
		
	return [nbFormatter numberFromString:original];
	
	
}

- (NSString *)stringWithKey:(NSString *)key fromType:(id)original
{
	// convert the given string into the correct type using the supplied formatter
	NSFormatter *formatter;
		
	if (!(formatter = [formatters valueForKey:key])) return original;
		
	NSNumberFormatter *nbFormatter = (NSNumberFormatter *)formatter;
	
	return [nbFormatter stringFromNumber:original];
	
	
}


#pragma mark -
#pragma mark Table view datasrc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Table view datasrc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
	return [channelAttributeNames count];
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	if ([[aTableColumn identifier] isEqualToString:@"key"]) {
		
		return [channelAttributeNames objectAtIndex:rowIndex];
		
	}
	
	if ([[aTableColumn identifier] isEqualToString:@"value"]) {
		
		// TODO: for now, it is assumed that the popup button will only show channels in single selection.
		// for the future, should extend this to the intersection of available channels in multiple selection.
		
		// these channels belong to some selected image AND have [self channelName] as name
		NSArray *channels = self.channelsForCurrentSelection;
		
		if (!channels || [channels count] == 0) return nil;
		
		// return the value associated to the correct key
		NSArray *values = [self valueForKeyPath:[@"channelsForCurrentSelection.@distinctUnionOfObjects." stringByAppendingString:[channelAttributeKeys objectAtIndex:rowIndex]]];
		//NSLog(@"values for %@ %@", [channelAttributeKeys objectAtIndex:rowIndex], [values description]);
		
		if (!values || values == NSMultipleValuesMarker || values == NSNoSelectionMarker || [values count] != 1)
			return nil;
		
		id value = [values objectAtIndex:0];
		
		if (value == [NSNull null]) return nil;
		
		return [self stringWithKey:[channelAttributeKeys objectAtIndex:rowIndex] fromType:value];
		
	}
	
	return nil;
	
}



- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	// only the "value" column can be edited
	// we assume there is a valid channel active to set the property of (as controlled by the shouldEditTableColumn: delegate method)
	
	NSArray *channels = self.channelsForCurrentSelection;
	
	if (!channels) NSBeep();
	
	// check if the proposed string is valid: for some props, need to have at least one character
	if ([[channelAttributeKeys objectAtIndex:rowIndex] isEqualToString:@"channelName"]) {
		
		NSString *newName = [self typeWithKey:[channelAttributeKeys objectAtIndex:rowIndex] fromString:anObject];
		
		if ([newName length] == 0) {
			
			NSBeep();
			return;
			
		}
		
		NSString *uniqueString = [self uniqueChannelNameForSuggestedName:newName];
		
		// set the model value
		[self setValue:uniqueString forKeyPath:[@"channelsForCurrentSelection." stringByAppendingString:[channelAttributeKeys objectAtIndex:rowIndex]]];
		
		// update the channel name in the popup
		[self setChannelName:uniqueString];
		[[channelPopUp selectedItem] setTitle:uniqueString];
		
		
	} else {
		
		
		id proposedValue = [self typeWithKey:[channelAttributeKeys objectAtIndex:rowIndex] fromString:anObject];
		
		// set the model value
		[self setValue:proposedValue forKeyPath:[@"channelsForCurrentSelection." stringByAppendingString:[channelAttributeKeys objectAtIndex:rowIndex]]];
		
		
	}
	
	[aTableView reloadData];
}

- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
	NSMutableSet *channels = [observableController valueForKeyPath:@"selection.instrument.channels"];
	
	if (channels == NSNoSelectionMarker || channels == NSMultipleValuesMarker || channels == NSNotApplicableMarker)
		return NO;
	
	id selectedChannel = nil;
	for (id channel in channels) {
		
		NSString *name = [channel valueForKey:@"channelName"];
		
		if (name != NSNoSelectionMarker && name != NSMultipleValuesMarker && [name isEqualToString:[self channelName]]) {
			
			selectedChannel = channel;
			break;
			
		}
	}
	if (!selectedChannel) 
		return NO;
	
	return YES;
	
	
}

- (NSString *)contentDescription
{
	if (![[[channelPopUp selectedItem] title] isEqualToString:@"None Specified"]) return [[channelPopUp selectedItem] title];
	
	return nil;
}


- (NSString *)contentValue
{
	return [self contentDescription];
}

@end
