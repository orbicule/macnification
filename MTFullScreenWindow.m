#import "MTFullScreenWindow.h"
#import <Carbon/Carbon.h>
#import "FullScreenController.h"

#define TRANSITION_DURATION 0.4

@implementation MTFullScreenWindow


- (id)initWithDelegate:(id)delegate;
{
    // Create the full-screen window.
    NSScreen* mainScreen = [[NSApp mainWindow] screen]; 
    NSRect winRect = [mainScreen frame];
    if((self = [super initWithContentRect:winRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO screen:[[NSApp mainWindow] screen] ])){
        
		// Establish the window attributes.
		[self setReleasedWhenClosed: YES];
		[self setDisplaysWhenScreenProfileChanges: YES];
		[self setDelegate: delegate];
		[self setBackgroundColor: [NSColor blackColor]];
		[self setOpaque:YES];
		[self setAcceptsMouseMovedEvents:YES];
	}
	
    return self;
}

- (void)setScreen:(NSScreen *)screen
{
	[self setFrame:[screen frame] display:NO animate:NO];
}

- (BOOL)canBecomeKeyWindow;
{
    // So we can get ESC
    return YES;
}


- (void)keyDown:(NSEvent *)theEvent;
{

}



// Intercept mouseDown so we can close on ESC
- (void) sendEvent: (NSEvent *) theEvent
{
	if ([theEvent type] == NSKeyDown) {
		unichar key = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];

		if ([theEvent keyCode] == 53) {
			[(FullScreenController *)[self delegate] exitFullScreen:self];
		}
		else if (key == NSRightArrowFunctionKey && ([theEvent modifierFlags] & NSCommandKeyMask)) {
			[(FullScreenController *)[self delegate] selectNext:self];
		}	
		else if (key == NSRightArrowFunctionKey) {
			[(FullScreenController *)[self delegate] selectNext:self];
		}	
		else if (key == NSDownArrowFunctionKey) {
			[(FullScreenController *)[self delegate] selectNext:self];
		}	
		else if (key == NSLeftArrowFunctionKey && ([theEvent modifierFlags] & NSCommandKeyMask)) {
			[(FullScreenController *)[self delegate] selectPrevious:self];
		}
		else if (key == NSLeftArrowFunctionKey) {
			[(FullScreenController *)[self delegate] selectPrevious:self];
		}
		else if (key == NSUpArrowFunctionKey) {
			[(FullScreenController *)[self delegate] selectPrevious:self];
		}
	}
    [super sendEvent: theEvent];
}


- (void)enterFullScreen
{    	
	// Show the window.
	[self makeKeyAndOrderFront: self];	
	
	
	// Hide menu and dock
	if ([[NSScreen screens] objectAtIndex:0] == [self screen])
		SetSystemUIMode(kUIModeAllHidden, /*kUIOptionAutoShowMenuBar |*/ kUIOptionDisableHide | kUIOptionDisableProcessSwitch);
}



- (void)exitFullScreen;
{
	// Make transparent background
	[self setBackgroundColor: [NSColor clearColor]];
	
	// Close the fullscreen window
	[self orderOut: nil];
	
	// Show menu and dock
	SetSystemUIMode(kUIModeNormal, 0);
}




@end
