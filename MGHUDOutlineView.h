//
//  MGHUDOutlineView.h
//  Filament
//
//  Created by Dennis Lorson on 30/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MGHUDOutlineView : NSOutlineView {

}

- (void)_sendDelegateWillDisplayCell:(id)cell forColumn:(id)column row:(int)row;


@end
