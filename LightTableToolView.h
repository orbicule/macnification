//
//  LightTableToolView.h
//  LightTable
//
//	This class handles the drawing of all "tool" elements global to the light table: a selection rectangle, the layout guides.
//	It is a subview of a LightTableView.
//
//  Created by Dennis on 18/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface LightTableToolView : NSView 
{

	NSMutableArray *layoutGuides;
	NSRect selectionRectangle;
	BOOL printRectangleMode;
	
	NSImage *cameraImage;

}

@property(readonly) NSMutableArray *layoutGuides;
@property(readwrite) NSRect selectionRectangle;
@property(readwrite) BOOL printRectangleMode;

@end
