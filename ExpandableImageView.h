//
//  ExpandableImageView.h
//  LightTable
//
//  A NSImageView subclass that correctly scales "to fit", e.g. expands the drawn image as needed regardless of the NSImageRep size.
//
//  Created by Dennis on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ExpandableImageView : NSView 
{


	NSImage *_image;
    NSImageScaling _scaling;
	
}


- (void)setImage:(NSImage*)image;
- (void)setImageScaling:(NSImageScaling)newScaling;
- (NSImage*)image;
- (NSImageScaling)imageScaling;


@end
