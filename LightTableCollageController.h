//
//  LightTableCollageController.h
//  Filament
//
//  Created by Dennis Lorson on 22/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>
#import "MGImageBatchImporter.h"

@class LightTableCollagePreview;


@interface LightTableCollageController : NSObject <MGImageBatchImporterDelegate>
{
	
	
	IBOutlet LightTableCollagePreview *collageView;
	IBOutlet NSWindow *collageWindow;
	
	IBOutlet NSButton *useBackgroundColorCheckbox;
	IBOutlet NSColorWell *backgroundColorWell;
	IBOutlet NSTextField *collageNameTextField;
	
	
	NSImage *collage;
	IKSaveOptions *saveOptions;
    
    NSString *tmpImportPath;

}

- (void)showCollagePreviewWithImage:(NSImage *)image;


- (IBAction)cancelCollage:(id)sender;
- (IBAction)importCollage:(id)sender;
- (IBAction)exportCollage:(id)sender;

- (IBAction)changeBackgroundColor:(id)sender;


@end
