//
//  CaptureQTDevice.m
//  Filament
//
//  Created by Dennis Lorson on 14/01/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureQTDevice.h"
#import "CaptureOpenGLPreview.h"
#import "UVCCameraControl.h"


@implementation CaptureQTDevice

@synthesize originalDevice = originalDevice_;

- (id)initWithDevice:(QTCaptureDevice *)device
{
	if ((self = [super init])) {
		originalDevice_ = [device retain];
	}
	return self;
}

- (CapturePreviewType)previewType; 
{ 
	return CapturePreviewQT;
}

- (NSString *)name;
{
	return [originalDevice_ localizedDisplayName];
}

- (NSString *)uniqueID;
{
	return [originalDevice_ uniqueID];
}

- (NSString *)uidString;
{
    return [[super uidString] stringByAppendingFormat:@":%@", [self uniqueID]];
}


- (void)startSession;
{
	if (!originalDevice_)
		return;
	
	if (session_)
		return;
	
	session_ = [[QTCaptureSession alloc] init];
	
	NSError *error = nil;
	[originalDevice_ open:&error];
	if (error)
		NSLog(@"%@", [error description]);
    
    
    control_ = [[UVCCameraControl alloc] initWithDevice:originalDevice_];

	
	captureInput_ = [[QTCaptureDeviceInput alloc] initWithDevice:originalDevice_];
	[session_ addInput:captureInput_ error:nil];
	
	captureOutput_ = [[QTCaptureDecompressedVideoOutput alloc] init];
	[captureOutput_ setDelegate:self];
	[session_ addOutput:captureOutput_ error:nil];
	
	// Start the session
	[session_ startRunning];


	if ([self.delegate respondsToSelector:@selector(captureDeviceDidOpenSession:)])
		[self.delegate captureDeviceDidOpenSession:self];
	
}

- (void)stopSession;
{
	[session_ removeInput:captureInput_];
	[session_ removeOutput:captureOutput_];
    [session_ stopRunning];
	[[captureInput_ device] close];
    
    [control_ release];
    control_ = nil;
	
	[session_ release]; session_ = nil;
	[captureInput_ release]; captureInput_ = nil;
	[captureOutput_ release]; captureOutput_ = nil;
}

- (BOOL)canTakePicture;
{
	return YES;
}

- (void)takePicture;
{
	[super takePicture];
	
	@synchronized(self) {
		shouldTakePicture_ = YES;
	}
}

- (void)captureOutput:(QTCaptureOutput *)captureOutput 
  didOutputVideoFrame:(CVImageBufferRef)videoFrame 
	 withSampleBuffer:(QTSampleBuffer *)sampleBuffer 
	   fromConnection:(QTCaptureConnection *)connection
{
    [self performSelectorOnMainThread:@selector(_handleCaptureOutput:) withObject:[NSValue valueWithPointer:videoFrame] waitUntilDone:YES];
	
}


- (void)_handleCaptureOutput:(NSValue *)videoFrameValue
{
    @synchronized (self) {
        
        CVImageBufferRef videoFrame = (CVImageBufferRef)[videoFrameValue pointerValue];
        
		CIImage *capturedImage = [CIImage imageWithCVImageBuffer:videoFrame];
        
		CIImage *adjusted;
		if ([self.delegate respondsToSelector:@selector(captureDevice:adjustedImage:)])
			adjusted = [self.delegate captureDevice:self adjustedImage:capturedImage];
		else
			adjusted = capturedImage;
		
		// send the frame to our preview
		[preview_ setImage:adjusted backingBuffer:videoFrame];
		
		// take a pic if needed
		
		if (!shouldTakePicture_)
			return;
		
		shouldTakePicture_ = NO;
		
		
		
		NSImage *image = [[[NSImage alloc] initWithSize:NSMakeSize([adjusted extent].size.width, [adjusted extent].size.height)] autorelease];
		[image addRepresentation:[NSCIImageRep imageRepWithCIImage:adjusted]];
		
		[self performSelectorOnMainThread:@selector(didCaptureOutput:) withObject:image waitUntilDone:NO modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
		
	}
}

- (void)didCaptureOutput:(NSImage *)outputImage
{
	NSString *filePath = @"/var/tmp/Captured Image.tiff";
	[[outputImage TIFFRepresentation] writeToFile:filePath atomically:YES];
	
	if ([self.delegate respondsToSelector:@selector(captureDevice:didReceivePicturesForImport:)])
		[self.delegate captureDevice:self didReceivePicturesForImport:[NSArray arrayWithObject:filePath]];
    
    // delegate is responsible for removing files

}

- (void)disconnectPreviewFromView:(NSView *)view;
{
	if (session_)
		[self stopSession];
	
	[preview_ release];
	preview_ = nil;
}

- (void)connectPreviewToView:(NSView *)view;
{
	if (!session_)
		[self startSession];
	
	[preview_ release];
	preview_ = (CaptureOpenGLPreview *)[view retain];

	//[(QTCaptureView *)view setCaptureSession:session_];
	//[(QTCaptureView *)view setVideoPreviewConnection:[[(QTCaptureView *)view availableVideoPreviewConnections] objectAtIndex:0]];

	[session_ startRunning];

}

- (NSImage *)iconImage;
{
	NSImage *iconImage = [NSImage imageNamed:@"capture_device_qt"];
	
	[iconImage setSize:NSMakeSize(16, 16)];
	return iconImage;
}

- (void) dealloc
{
	if (session_)
		[self stopSession];
    
    [control_ release]; control_ = nil;
	
	[originalDevice_ release]; originalDevice_ = nil;

	
	[super dealloc];
}

- (CaptureAdjustmentRange)exposureRange
{
    CaptureAdjustmentRange range;
    
    range.min = [control_ getExposureMin];
    range.max = [control_ getExposureMax];
    range.linear = NO;
    
    return range;

}

- (void)resetExposure
{
    if (!([self exposureModes] & CaptureExposureModeManual))
        return;
    
    [control_ resetExposure];
}

- (CGFloat)absoluteExposure 
{ 
    return [control_ getExposure];
}

- (CaptureExposureMode)exposureModes 
{
    if ([control_ supportsManualExposure])
        return CaptureExposureModeAuto | CaptureExposureModeManual;
    
    return CaptureExposureModeAuto;
}


- (void)setExposureAbsolute:(CGFloat)exposure 
{
    if (!([self exposureModes] & CaptureExposureModeManual))
        return;
    
    [control_ setExposure:exposure];

    

}

- (void)setAutoExposure:(BOOL)autoExposure
{        
    [control_ setAutoExposure:autoExposure];
}

- (BOOL)autoExposure 
{
    return [control_ getAutoExposure];
}




@end
