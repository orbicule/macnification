//
//  StackLayer.h
//  StackTable
//
//	CALayer subclass representing the image stack.  Contains state-defining variables etc.
//
//  Created by Dennis Lorson on 19/09/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>


@class StackImageMainLayer;
@class Stack;

@interface StackLayer : CALayer {
	
	
	StackImageMainLayer *selectedLayer;

}

- (NSArray *)sortedSublayers;

@property(readwrite, retain) StackImageMainLayer *selectedLayer;



@end
