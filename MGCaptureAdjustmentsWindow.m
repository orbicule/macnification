//
//  MGCaptureAdjustmentsWindow.m
//  Filament
//
//  Created by Dennis Lorson on 05/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGCaptureAdjustmentsWindow.h"

#define ARROW_WIDTH		25
#define ARROW_HEIGHT	20


@interface MGCaptureAdjustmentsArrowView : NSView
{
	
}


@end


@interface MGCaptureAdjustmentsWindow ()

- (void)_createArrowWindowIfNeeded;

@end



@implementation MGCaptureAdjustmentsWindow


- (void)showWithArrowAtPoint:(NSPoint)point;
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_windowWillMove:) name:NSWindowWillMoveNotification object:self];
	
	[self _createArrowWindowIfNeeded];
	
    [self setHidesOnDeactivate:YES];

    
	NSRect arrowWindowFrame = [arrowWindow_ frame];
	arrowWindowFrame.origin.x = NSMidX([self frame]) - 0.5 * NSWidth(arrowWindowFrame);
	arrowWindowFrame.origin.y = NSMinY([self frame]) - NSHeight(arrowWindowFrame);
	[arrowWindow_ setFrame:arrowWindowFrame display:YES];
	[self addChildWindow:arrowWindow_ ordered:NSWindowBelow];
	[arrowWindow_ orderFront:self];
	
	
	// move the main window (self) to correspond to the given point
	NSRect frame = [self frame];
	frame.origin.x = point.x - 0.5 * NSWidth(frame);
	frame.origin.y = point.y + NSHeight(arrowWindowFrame);
	
	[self setFrame:frame display:YES];
}


- (void)_createArrowWindowIfNeeded
{
	if (arrowWindow_)
		return;
	
	arrowWindow_ = [[NSPanel alloc] initWithContentRect:NSMakeRect(0, 0, ARROW_WIDTH, ARROW_HEIGHT) styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:YES];
	[arrowWindow_ setOpaque:NO];
	[arrowWindow_ setBackgroundColor:[NSColor clearColor]];
	[arrowWindow_ setHasShadow:YES];
	[arrowWindow_ setReleasedWhenClosed:NO];
    [arrowWindow_ setHidesOnDeactivate:YES];
	[arrowWindow_ setContentView:[[[MGCaptureAdjustmentsArrowView alloc] initWithFrame:NSMakeRect(0, 0, ARROW_WIDTH, ARROW_HEIGHT)] autorelease]];
	
}

- (BOOL)isAttached;
{
    return [arrowWindow_ isVisible];
}

- (void)hideArrow;
{
	[self removeChildWindow:arrowWindow_];
	[arrowWindow_ orderOut:self];
}

- (IBAction)orderOut:(id)sender
{
	[self hideArrow];

	[super orderOut:sender];
}

- (void)close
{
	[[self parentWindow] removeChildWindow:self];
	
	[self hideArrow];

	[super close];
}

- (void)_windowWillMove:(NSNotification *)notification
{
	if ([notification object] == self) {
		[[self parentWindow] removeChildWindow:self];
		[self hideArrow];
        [self setFloatingPanel:YES];
	}
}


@end



@implementation MGCaptureAdjustmentsArrowView

- (void)drawRect:(NSRect)dirtyRect
{		
	NSRect dstRect = [self bounds];
	
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	[path moveToPoint:NSMakePoint(NSMinX(dstRect), NSMaxY(dstRect))];
	[path lineToPoint:NSMakePoint(NSMaxX(dstRect), NSMaxY(dstRect))];
	[path lineToPoint:NSMakePoint(NSMidX(dstRect), NSMinY(dstRect))];
	[path closePath];
		
    [[NSColor colorWithCalibratedWhite:32./255. alpha:0.772] set];
	[path fill];
    [[NSColor colorWithCalibratedWhite:75./255. alpha:0.772] set];
	[path stroke];

}


@end
