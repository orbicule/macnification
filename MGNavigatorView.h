//
//  MGNavigatorView.h
//  ImageNavigator
//
//  Created by Dennis Lorson on 6/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@class PSImageView;


@interface MGNavigatorView : NSView {
	
	CIImage *image;
	CGImageRef thumbnail;
	PSImageView *imageView;
	CGSize thumbnailSize;
	
	NSRect visibleRect;			// this rect is assumed to be in the 0-1 range for both w and h.

}

@property(nonatomic) NSRect visibleRect;

@property (readonly) CGSize thumbnailSize;
@property (nonatomic, retain) CIImage *image;

- (void)setImage:(CIImage *)anImage forView:(PSImageView *)view;

- (CGSize)thumbnailSizeForImage:(CIImage *)img;
- (CGImageRef)thumbnailWithImage:(CGImageRef)img;

- (NSRect)adjustedVisibleRect;

- (void)centerVisibleRectOnLocation:(NSPoint)location;



@end
