//
//  BlobController.m
//  Filament
//
//  Created by Peter Schols on 08/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "BlobController.h"
#import "Image.h"
#import "ROI.h"
#import "MGAppDelegate.h"
#import "MGHUDRangeSlider.h"
#import "MGHistogramView.h"
#import "ImageArrayController.h"
#import "MGHUDSlider.h"
#import "BlobExtractOperation.h"
#import "MGLibraryController.h"


@class MGAppDelegate;

@implementation BlobController


@synthesize inputImage, lastSelectedImage, activeColorspace, thresholdFilterActive;



- (id)init 
{
    self = [super initWithWindowNibName:@"BlobExtraction"];
    return self;
}


- (void)dealloc
{
	[thresholdFilter release];
	[extractionQueue release];
	[lastBlobresults release];
	
	self.inputImage = nil;
	
	[super dealloc];
}

- (void)awakeFromNib;
{
	// setup the queue
	extractionQueue = [[NSOperationQueue alloc] init];
	[extractionQueue setMaxConcurrentOperationCount:1];
	
	// Register for blob detection notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(extractionOperationDidEnd:) name:BlobExtractionOperationDidEndNotification object:nil];
	
	imageArrayController = [MGLibraryControllerInstance imageArrayController];
	fullscreenImageView = [imageArrayController fullscreenView];
    
	
	thresholdFilter = [[CIFilter filterWithName:@"ThresholdFilter"] retain];
	
	// setup UI items
	[rSlider setColorOverlay:[[[NSGradient alloc] initWithStartingColor:[[NSColor blackColor] colorWithAlphaComponent:0.3] endingColor:[[NSColor redColor] colorWithAlphaComponent:0.3]] autorelease]];
	[gSlider setColorOverlay:[[[NSGradient alloc] initWithStartingColor:[[NSColor blackColor] colorWithAlphaComponent:0.3] endingColor:[[NSColor greenColor] colorWithAlphaComponent:0.3]] autorelease]];
	[bSlider setColorOverlay:[[[NSGradient alloc] initWithStartingColor:[[NSColor blackColor] colorWithAlphaComponent:0.3] endingColor:[[NSColor blueColor] colorWithAlphaComponent:0.3]] autorelease]];
	
	
	NSArray *colors = [NSArray arrayWithObjects:
					   
					   [[NSColor redColor] colorWithAlphaComponent:0.3],
					   [[NSColor yellowColor] colorWithAlphaComponent:0.3],
					   [[NSColor greenColor] colorWithAlphaComponent:0.3],
					   [[NSColor cyanColor] colorWithAlphaComponent:0.3],
					   [[NSColor blueColor] colorWithAlphaComponent:0.3],
					   [[NSColor purpleColor] colorWithAlphaComponent:0.3],
					   [[NSColor redColor] colorWithAlphaComponent:0.3],
					   
					   nil];
	
	[hSlider setColorOverlay:[[[NSGradient alloc] initWithColors:colors] autorelease]];
	[sSlider setColorOverlay:[[[NSGradient alloc] initWithStartingColor:[[NSColor whiteColor] colorWithAlphaComponent:0.3] endingColor:[[NSColor redColor] colorWithAlphaComponent:0.3]] autorelease]];
	[lSlider setColorOverlay:[[[NSGradient alloc] initWithStartingColor:[[NSColor blackColor] colorWithAlphaComponent:0.3] endingColor:[[NSColor whiteColor] colorWithAlphaComponent:0.3]] autorelease]];

	[kSlider setColorOverlay:[[[NSGradient alloc] initWithStartingColor:[[NSColor blackColor] colorWithAlphaComponent:0.3] endingColor:[[NSColor whiteColor] colorWithAlphaComponent:0.3]] autorelease]];

	
	[rSlider setTag:0];
	[gSlider setTag:1];
	[bSlider setTag:2];
	[hSlider setTag:0];
	[sSlider setTag:1];
	[lSlider setTag:2];
	[kSlider setTag:0];
	
	
	[rSlider setMouseOverAction:@selector(changeActiveChannel:)];
	[gSlider setMouseOverAction:@selector(changeActiveChannel:)];
	[bSlider setMouseOverAction:@selector(changeActiveChannel:)];
	[hSlider setMouseOverAction:@selector(changeActiveChannel:)];
	[sSlider setMouseOverAction:@selector(changeActiveChannel:)];
	[lSlider setMouseOverAction:@selector(changeActiveChannel:)];
	[kSlider setMouseOverAction:@selector(changeActiveChannel:)];
	
	[rSlider setMouseUpAction:@selector(startBlobDetection:)];
	[gSlider setMouseUpAction:@selector(startBlobDetection:)];
	[bSlider setMouseUpAction:@selector(startBlobDetection:)];
	[hSlider setMouseUpAction:@selector(startBlobDetection:)];
	[sSlider setMouseUpAction:@selector(startBlobDetection:)];
	[lSlider setMouseUpAction:@selector(startBlobDetection:)];
	[kSlider setMouseUpAction:@selector(startBlobDetection:)];

	[blurSlider setTarget:self];
	[interpolationAccuracySlider setTarget:self];

	[blurSlider setMouseUpAction:@selector(startBlobDetection:)];
	[interpolationAccuracySlider setMouseUpAction:@selector(startBlobDetection:)];
	
	
	[self resetAnalysisParameters];

	
	self.activeColorspace = IHRGBColorspace;
	
	[self displaySlidersForColorspace:self.activeColorspace];
	
	[blobWindow setDelegate:self];
	
	[helpButton setTarget:self];
	[helpButton setAction:@selector(loadContextHelp:)];
	
		
}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == imageArrayController) {
		
		if ([[imageArrayController selectedObjects] count] == 1) {
			
			[fullscreenImageView setTemporaryROIs:nil];

			
			Image *image = [[imageArrayController selectedObjects] objectAtIndex:0];
			
			self.inputImage = [image ciImage];
			
			[self updateHistogram];
			
			if (self.lastSelectedImage)
				[self saveAnalysisParametersToImage:self.lastSelectedImage];
			
			[self resetAnalysisParameters];
			[self loadAnalysisParametersFromImage:image];
			
			if (self.thresholdFilterActive)
				[self updateThresholdedImage];
			
			//NSLog(@"start observing %@     ----    stop observing %@", [image description], [self.lastSelectedImage description]);
			[image addObserver:self forKeyPath:@"CIFilters" options:0 context:nil];
			
			[self.lastSelectedImage removeObserver:self forKeyPath:@"CIFilters"];
			[self setThresholdActive:NO forImage:self.lastSelectedImage];
			
			self.lastSelectedImage = image;
			
		} else {
			
			if (self.lastSelectedImage)
				[self.lastSelectedImage removeObserver:self forKeyPath:@"CIFilters"];
			self.lastSelectedImage = nil;
			
		}
		

		
	} else if (object == lastSelectedImage) {
				
		[self updateHistogram];
		
		if (self.thresholdFilterActive)
			[self updateThresholdedImage];
		
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}


- (void)setInputImage:(CIImage *)theImage
{
	if (theImage != inputImage) {
		
		[inputImage release];
		inputImage = [theImage retain];
		
	}
	
	[self adjustInterpolationAccuracyBounds];
}


#pragma mark IB Actions

- (IBAction)loadContextHelp:(id)sender
{
	NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
	[[NSHelpManager sharedHelpManager] openHelpAnchor:@"AutoDetect"  inBook:locBookName];
	
	
}

- (void)adjustInterpolationAccuracyBounds
{
	CGRect extent = [self.inputImage extent];
	
	CGFloat dimension = (extent.size.width + extent.size.height)/2.;
	
	[interpolationAccuracySlider setMinValue:dimension/400.];
	[interpolationAccuracySlider setMaxValue:dimension/50.];
	
}

- (IBAction)changeThresholdValue:(id)sender
{
	switch ([sender tag]) {
			
		case 0:		// r -- h -- k
			[histogramView setLowerRangeBound:[(MGHUDRangeSlider *)sender lowerBound]/255. forChannel:0];
			[histogramView setUpperRangeBound:[(MGHUDRangeSlider *)sender upperBound]/255. forChannel:0];
			break;

		case 1:		// g -- s
			[histogramView setLowerRangeBound:[(MGHUDRangeSlider *)sender lowerBound]/255. forChannel:1];
			[histogramView setUpperRangeBound:[(MGHUDRangeSlider *)sender upperBound]/255. forChannel:1];

			break;
			
		case 2:		// b -- l
			[histogramView setLowerRangeBound:[(MGHUDRangeSlider *)sender lowerBound]/255. forChannel:2];
			[histogramView setUpperRangeBound:[(MGHUDRangeSlider *)sender upperBound]/255. forChannel:2];

			break;
			
	}
	
	[self updateThresholdedImage];
	
	[self saveAnalysisParametersToImage:self.lastSelectedImage];

}

- (IBAction)changeColorspace:(id)sender
{
	IHColorspace newSpace = IHRGBColorspace;
	switch ([[sender selectedItem] tag]) {
		
			// force updates of the ranges in the histogram
		case 0:
			newSpace = IHRGBColorspace;
			[self changeThresholdValue:rSlider];
			[self changeThresholdValue:gSlider];
			[self changeThresholdValue:bSlider];
			break;
		case 1:
			newSpace = IHHSLColorspace;
			[self changeThresholdValue:hSlider];
			[self changeThresholdValue:sSlider];
			[self changeThresholdValue:lSlider];
			break;
		case 2:
			newSpace = IHMonochromeColorspace;
			[self changeThresholdValue:kSlider];
			break;
			
	}
	
		
	self.activeColorspace = newSpace;
	
	[self displaySlidersForColorspace:newSpace];
	
	[histogramView beginAnimationBlock];

	[self updateHistogram];
	
	[histogramView endAnimationBlock];
	
	[self updateThresholdedImage];
	
	[self saveAnalysisParametersToImage:self.lastSelectedImage];

	
}

- (IBAction)changeBlur:(id)sender
{
	[self updateThresholdedImage];
	
	[self saveAnalysisParametersToImage:self.lastSelectedImage];

}

- (IBAction)changeActiveChannel:(id)sender
{
	[histogramView beginAnimationBlock];

	
	if (!((MGHUDRangeSlider *)sender).hasMouseOver) {
		
		[histogramView setActiveChannel:-1];
	}
	else {
		switch ([sender tag]) {
				
			case 0:
				[histogramView setActiveChannel:0];
				break;
				
			case 1:
				[histogramView setActiveChannel:1];
				break;
				
			case 2:
				[histogramView setActiveChannel:2];
				break;
				
		}
		
	}

	[histogramView endAnimationBlock];

	[self saveAnalysisParametersToImage:self.lastSelectedImage];

}


#pragma mark Parameter persistence

- (void)saveAnalysisParametersToImage:(Image *)image
{
	NSMutableDictionary *analysisParameters = [NSMutableDictionary dictionary];
	
	NSArray *sliders = [NSArray arrayWithObjects:rSlider, gSlider, bSlider, hSlider, sSlider, lSlider, kSlider, nil];
	NSArray *keySuffixes = [NSArray arrayWithObjects:@"rgb_r", @"rgb_g", @"rgb_b", @"hsl_h", @"hsl_s", @"hsl_l", @"k_k", nil];
	
	for (MGHUDRangeSlider *slider in sliders) {
		
		NSInteger currentThresholdValue = [slider lowerBound];
		if (currentThresholdValue < 255 && currentThresholdValue > 0)
			[analysisParameters setObject:[NSNumber numberWithInt:currentThresholdValue] forKey:[NSString stringWithFormat:@"threshold_lower_%@", [keySuffixes objectAtIndex:[sliders indexOfObject:slider]]]];
		
		currentThresholdValue = [slider upperBound];
		if (currentThresholdValue < 255 && currentThresholdValue > 0)
			[analysisParameters setObject:[NSNumber numberWithInt:currentThresholdValue] forKey:[NSString stringWithFormat:@"threshold_upper_%@", [keySuffixes objectAtIndex:[sliders indexOfObject:slider]]]];	
		
	}
	
	
	// save blur
	[analysisParameters setObject:[NSNumber numberWithFloat:[blurSlider floatValue]] forKey:@"threshold_blur"];
	
	
	// save checkboxes and min blob size
	[analysisParameters setObject:[NSNumber numberWithInteger:[minimumBlobSizeTextField floatValue]] forKey:@"threshold_minblobsize_value"];
	[analysisParameters setObject:[NSNumber numberWithBool:([setMinimumBlobSizeActiveCheckbox state] == NSOnState)] forKey:@"threshold_minblobsize_active"];
	[analysisParameters setObject:[NSNumber numberWithBool:([innerBlobCheckbox state] == NSOnState)] forKey:@"threshold_nestedblobs_active"];

	
	[image setValue:analysisParameters forKey:@"analysisParameters"];
	
}

- (void)resetAnalysisParameters
{
	NSArray *sliders = [NSArray arrayWithObjects:rSlider, gSlider, bSlider, hSlider, sSlider, lSlider, kSlider, nil];
	
	for (MGHUDRangeSlider *slider in sliders) {
		[slider setMinValue:0];
		[slider setMaxValue:255];
		
		[slider setLowerBound:0];
		[slider setUpperBound:255];

	}
	
	[histogramView resetRanges];
	
	
}

- (void)loadAnalysisParametersFromImage:(Image *)image
{
	
	NSDictionary *analysisParameters = [image valueForKey:@"analysisParameters"];
	
	if (!analysisParameters) return;
	
	NSArray *sliders = [NSArray arrayWithObjects:rSlider, gSlider, bSlider, hSlider, sSlider, lSlider, kSlider, nil];
	NSArray *keySuffixes = [NSArray arrayWithObjects:@"rgb_r", @"rgb_g", @"rgb_b", @"hsl_h", @"hsl_s", @"hsl_l", @"k_k", nil];
	
	NSNumber *currentThresholdValue = nil;
	for (NSString *suffix in keySuffixes) {
		
		if ((currentThresholdValue = [analysisParameters objectForKey:[NSString stringWithFormat:@"threshold_upper_%@", suffix]])) {
			
			MGHUDRangeSlider *slider = [sliders objectAtIndex:[keySuffixes indexOfObject:suffix]];
			[slider setUpperBound:[currentThresholdValue intValue]];
			[self changeThresholdValue:slider];

		}
		if ((currentThresholdValue = [analysisParameters objectForKey:[NSString stringWithFormat:@"threshold_lower_%@", suffix]])) {
			
			MGHUDRangeSlider *slider = [sliders objectAtIndex:[keySuffixes indexOfObject:suffix]];
			[slider setLowerBound:[currentThresholdValue intValue]];
			[self changeThresholdValue:slider];

		}
		
		
		// change the ranges
		if ([hslSliderContainer superview] == [[self window] contentView]) {
			[self changeThresholdValue:hSlider];
			[self changeThresholdValue:sSlider];
			[self changeThresholdValue:lSlider];
			
			
		} else if ([rgbSliderContainer superview] == [[self window] contentView]) {
			[self changeThresholdValue:rSlider];
			[self changeThresholdValue:gSlider];
			[self changeThresholdValue:bSlider];

			
		} else if ([kSliderContainer superview] == [[self window] contentView]) {
			[self changeThresholdValue:kSlider];
		}

	}
	
	// load blur
	if ([analysisParameters objectForKey:@"threshold_blur"])
		[blurSlider setFloatValue:[[analysisParameters objectForKey:@"threshold_blur"] floatValue]];
	
	// load checkboxes and min blob size
	if ([analysisParameters objectForKey:@"threshold_minblobsize_value"])
		[minimumBlobSizeTextField setIntValue:[[analysisParameters objectForKey:@"threshold_minblobsize_value"] integerValue]];
	
	[setMinimumBlobSizeActiveCheckbox setState:([[analysisParameters objectForKey:@"threshold_minblobsize_active"] boolValue]) ? NSOnState : NSOffState];
	[innerBlobCheckbox setState:([[analysisParameters objectForKey:@"threshold_nestedblobs_active"] boolValue]) ? NSOnState : NSOffState];
	
}


- (void)displaySlidersForColorspace:(IHColorspace)space
{
	
	NSView *currentSliderContainerView = nil;
	// find out which slider view we're currently hosting
	if ([hslSliderContainer superview] == [[self window] contentView])
		currentSliderContainerView = hslSliderContainer;
		
	if ([rgbSliderContainer superview] == [[self window] contentView])
		currentSliderContainerView = rgbSliderContainer;
	
	if ([kSliderContainer superview] == [[self window] contentView])
		currentSliderContainerView = kSliderContainer;
	
	NSView *newContainerView = nil;
	if (space == IHRGBColorspace)
		newContainerView = rgbSliderContainer;
	if (space == IHHSLColorspace)
		newContainerView = hslSliderContainer;
	if (space == IHMonochromeColorspace)
		newContainerView = kSliderContainer;
	
	CGFloat previousViewHeight = currentSliderContainerView ? [currentSliderContainerView frame].size.height : 0;
	CGFloat newViewHeight = [newContainerView frame].size.height;
	
	CGFloat previousWindowHeight = [[self window] frame].size.height;
	CGFloat newWindowHeight = previousWindowHeight - previousViewHeight + newViewHeight;
	
	
	NSRect newWindowFrame = [[self window] frame];
	newWindowFrame.origin.y -= newWindowHeight - previousWindowHeight;
	newWindowFrame.size.height = newWindowHeight;

	
	[currentSliderContainerView removeFromSuperview];
	
	[[self window] setFrame:newWindowFrame display:YES animate:YES];

	[newContainerView setFrameOrigin:NSMakePoint(NSMinX([containerPlaceholder frame]), NSMaxY([containerPlaceholder frame]) - NSHeight([newContainerView frame]))];
	[[[self window] contentView] addSubview:newContainerView];
	
}

#pragma mark Image processing


- (void)updateHistogram
{
	//NSLog(@"update histogram");
	CIImage *convImage = [self colorConvertedImage];
	
	// apply blur to equalize the histogram a bit
	CIFilter *equalizer = [CIFilter filterWithName:@"DODGaussianBlurFilter"];
	[equalizer setValue:convImage forKey:@"inputImage"];
	[equalizer setValue:[NSNumber numberWithFloat:1.0] forKeyPath:@"inputRadius"];
	CIImage *blurred = [equalizer valueForKey:@"outputImage"];
	
	[histogramView setCIImage:blurred];
	[histogramView setColorspace:self.activeColorspace];
	
}


- (NSBitmapImageRep *)renderCIImage:(CIImage *)image;
{
	return [[[NSBitmapImageRep alloc] initWithCIImage:image] autorelease];
}



- (void)setThresholdActive:(BOOL)flag forImage:(Image *)image
{
	
	CIFilter *thresholdCompositor = [CIFilter filterWithName:@"ThresholdCompositorFilter"];
	
	[thresholdCompositor setValue:[NSNumber numberWithBool:flag] forKey:@"inputIsActive"];
	[image setThresholdCompositeFilter:thresholdCompositor];
	[imageArrayController filtersDidChange];	
	
}

- (void)updateThresholdedImage
{
	//NSLog(@"update image");
	
	if ([[imageArrayController selectedObjects] count] != 1) {
		NSLog(@"%@ wants to connect to threshold compositor, but more than one image selected?", [self description]);
		return;
	}
	
	
	Image *image = [[imageArrayController selectedObjects] objectAtIndex:0];
	
	
	CIFilter *thresholdCompositor = [CIFilter filterWithName:@"ThresholdCompositorFilter"];
	
	if (!self.thresholdFilterActive) {
		
		[thresholdCompositor setValue:[NSNumber numberWithBool:NO] forKey:@"inputIsActive"];
		[image setThresholdCompositeFilter:thresholdCompositor];
		[imageArrayController filtersDidChange];
		return;
		
	}
	
	CIImage *convertedImage = [self colorConvertedImage];
	CIImage *binaryImage = [self binaryImageFromImage:convertedImage];
	
	[thresholdCompositor setValue:binaryImage forKey:@"inputBinaryImage"];
	[thresholdCompositor setValue:[NSNumber numberWithBool:YES] forKey:@"inputIsActive"];
	[thresholdCompositor setValue:[CIColor colorWithRed:1 green:1 blue:1 alpha:1] forKey:@"inputOverlayColor"];
	[image setThresholdCompositeFilter:thresholdCompositor];
	
	[imageArrayController filtersDidChange];
	
}



- (CIImage *)colorConvertedImage
{
	assert([[imageArrayController selectedObjects] count] == 1);
	
	CIImage *filteredImage = self.inputImage;
	filteredImage = [[[imageArrayController selectedObjects] objectAtIndex:0] filterImage:filteredImage];
	
	if (self.activeColorspace == IHRGBColorspace) {
		
		return filteredImage;
		
	} else if (self.activeColorspace == IHHSLColorspace) {
		//NSLog(@"->HSL");
		CIFilter *conversion = [CIFilter filterWithName:@"ColorspaceConversionFilter"];
		[conversion setValue:[NSNumber numberWithInt:1] forKey:@"inputConversionType"];
		[conversion setValue:filteredImage forKey:@"inputImage"];
		return [conversion valueForKey:@"outputImage"];
		
	} else if (self.activeColorspace == IHMonochromeColorspace) {
		//NSLog(@"->gray");
		CIFilter *conversion = [CIFilter filterWithName:@"ColorspaceConversionFilter"];
		[conversion setValue:[NSNumber numberWithInt:3] forKey:@"inputConversionType"];
		[conversion setValue:filteredImage forKey:@"inputImage"];
		return [conversion valueForKey:@"outputImage"];
		
	}
	return nil;
}

- (CIImage *)binaryImageFromImage:(CIImage *)original
{
	
	[thresholdFilter setValue:original forKey:@"inputImage"];
	
	CIColor *lowerThresholdColor, *upperThresholdColor, *rangeBlockColor;
	
	switch (self.activeColorspace) {
			
		case IHRGBColorspace:
		default:
			lowerThresholdColor = [CIColor colorWithRed:[rSlider lowerBound]/255.
												  green:[gSlider lowerBound]/255.
												   blue:[bSlider lowerBound]/255.
												  alpha:1.];
			upperThresholdColor = [CIColor colorWithRed:[rSlider upperBound]/255.
												  green:[gSlider upperBound]/255.
												   blue:[bSlider upperBound]/255.
												  alpha:1.];
			rangeBlockColor = [CIColor colorWithRed:0
											  green:0
											   blue:0
											  alpha:0];
			break;
		case IHHSLColorspace:
			lowerThresholdColor = [CIColor colorWithRed:[hSlider lowerBound]/255.
												  green:[sSlider lowerBound]/255.
												   blue:[lSlider lowerBound]/255.
												  alpha:1.];
			upperThresholdColor = [CIColor colorWithRed:[hSlider upperBound]/255.
												  green:[sSlider upperBound]/255.
												   blue:[lSlider upperBound]/255.
												  alpha:1.];
			rangeBlockColor = [CIColor colorWithRed:0
											  green:0
											   blue:0
											  alpha:0];
			
			break;
		case IHMonochromeColorspace:
			lowerThresholdColor = [CIColor colorWithRed:[kSlider lowerBound]/255.
												  green:[kSlider lowerBound]/255.
												   blue:[kSlider lowerBound]/255.
												  alpha:1.];
			upperThresholdColor = [CIColor colorWithRed:[kSlider upperBound]/255.
												  green:[kSlider upperBound]/255.
												   blue:[kSlider upperBound]/255.
												  alpha:1.];
			rangeBlockColor = [CIColor colorWithRed:0
											  green:0
											   blue:0
											  alpha:0];
			break;
			
			
	}
	
	NSNumber *blurRadius = [NSNumber numberWithFloat:[blurSlider floatValue]];
	
	[thresholdFilter setValue:lowerThresholdColor forKey:@"inputLowerThreshold"];
	[thresholdFilter setValue:upperThresholdColor forKey:@"inputUpperThreshold"];
	[thresholdFilter setValue:rangeBlockColor forKey:@"inputBlockVector"];
	[thresholdFilter setValue:blurRadius forKey:@"inputBlurRadius"];
	
	
	if (![thresholdFilter valueForKey:@"outputImage"])
		NSLog(@"no binary image created");
	
	CIImage *output = [thresholdFilter valueForKey:@"outputImage"];
			
	return output;
}



#pragma mark ROI Extraction


// Start the blob analysis
- (IBAction)startBlobDetection:(id)sender;
{
	[minimumBlobSizeTextField setEnabled:([setMinimumBlobSizeActiveCheckbox state] == NSOnState)];
	
	CIImage *colorConvertedImage = [self colorConvertedImage];
	CIImage *binaryImage = [self binaryImageFromImage:colorConvertedImage];
	
	CGRect extent = [binaryImage extent];
	CGFloat area = CGRectGetWidth(extent) * CGRectGetHeight(extent);
	
	if (area > 1500000.) {
		downsamplingFactor = 2.0;//sqrt(area / (1500000.));
		CIFilter *scale = [CIFilter filterWithName:@"CIAffineTransform"];
		NSAffineTransform *trafo = [NSAffineTransform transform];
		[trafo scaleBy:0.5];
		[scale setValue:trafo forKey:@"inputTransform"];
		[scale setValue:binaryImage forKey:@"inputImage"];
		binaryImage = [scale valueForKey:@"outputImage"];
	}
	
	else {
		downsamplingFactor = 1.0;
	}

	NSBitmapImageRep *repToProcess = [self renderCIImage:binaryImage];

	BlobExtractOperation *op = [[[BlobExtractOperation alloc] initWithBinaryImage:repToProcess] autorelease];
	op.searchesInnerBlobs = [innerBlobCheckbox state];
	op.maxInterpolationError = 1.5;
	op.minimumBlobArea = [minimumBlobSizeTextField isEnabled] ? [minimumBlobSizeTextField floatValue] : 0;
	
	[blobProgressIndicator setHidden:NO];
	[blobProgressIndicator setIndeterminate:YES];
	//[blobProgressIndicator setUsesThreadedAnimation:YES];
	[blobProgressIndicator startAnimation:self];
	
	[extractionQueue cancelAllOperations];
	[extractionQueue addOperation:op];
}


// Blob detection did end
- (void)extractionOperationDidEnd:(NSNotification *)note
{
	[self performSelectorOnMainThread:@selector(processBlobsWithDictionary:) withObject:[[[note userInfo] copy] autorelease] waitUntilDone:NO];
}


- (void)saveBlobsAlertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	
	@try {
		
		[blobWindow orderOut:self];
		
		[self performCloseCleanup];
		
		if (returnCode == NSAlertAlternateReturn) return;
		
		id image = [[imageArrayController selectedObjects] objectAtIndex:0];
		
		if (returnCode != NSAlertDefaultReturn)
			[image removeAllROIs];
                
		NSBitmapImageRep *sharedRep = [image imageRep];
		
		NSMutableArray *roiDictionaries = [NSMutableArray array];
		for (NSArray *blob in lastBlobresults)
			[roiDictionaries addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"PolygonROI", @"type", blob, @"points", nil]];
		
		NSArray *rois = [image addROIsWithPoints:roiDictionaries];
		
		for (ROI *roi in rois)
			[roi calculateColorContentsWithSharedImage:sharedRep];
		
		
	} @catch (NSException *e) {
		
		NSLog(@"%@ -- exception when saving blobs", [self description]);
		
		
	}

	
}

- (IBAction)saveBlobs:(id)sender
{
	id image = [[imageArrayController selectedObjects] objectAtIndex:0];
	if ([image roiCount] > 0) {
		
		NSAlert *alert = [NSAlert alertWithMessageText:@"Do you want to replace the existing ROIs in this image or just add the new ones?"
										 defaultButton:@"Add" alternateButton:@"Cancel" otherButton:@"Replace" 
							 informativeTextWithFormat:@""];
		
		
		[alert beginSheetModalForWindow:[self window] modalDelegate:self didEndSelector:@selector(saveBlobsAlertDidEnd:returnCode:contextInfo:) contextInfo:nil];
		
	} else {
		
		[self saveBlobsAlertDidEnd:nil returnCode:NSAlertDefaultReturn contextInfo:nil];
		
	}
	

}

// Process the blobs
- (void)processBlobsWithDictionary:(NSDictionary *)dict
{
	if (![[self window] isVisible])
		return;
	
	[lastBlobresults release];
	lastBlobresults = [[self upsampleBlobs:[dict valueForKey:@"rois"]] retain];
	

	[fullscreenImageView setTemporaryROIs:lastBlobresults];

	[blobProgressIndicator setHidden:YES];
	[blobProgressIndicator stopAnimation:self];
	
	
	if ([lastBlobresults count] > 0)
		[saveBlobsButton setEnabled:YES];
	
}

- (NSArray *)upsampleBlobs:(NSArray *)originalBlobs
{
	//if (downsamplingFactor == 1.0)
	//	return originalBlobs;
		
	NSMutableArray *upsampledBlobs = [NSMutableArray array];
	
	for (NSArray *blob in originalBlobs) {
		
		NSMutableArray *upsampledBlob = [NSMutableArray array];

		for (NSValue *point in blob) {
			
			NSPoint original = [point pointValue];
			original.x *= downsamplingFactor;
			original.y *= downsamplingFactor;
			
			original.x += 1;
			original.y += 1;
			
			NSValue *upsampled = [NSValue valueWithPoint:original];
			
			[upsampledBlob addObject:upsampled];
			
		}
		
		[upsampledBlobs addObject:upsampledBlob];
		
	}
	
	return upsampledBlobs;
}


#pragma mark Opening and closing the workflow

- (IBAction)toggleBlobDetectionWindow:(id)sender;
{
	Image *image = [[imageArrayController selectedObjects] objectAtIndex:0];
	if ([image isCalibrated]) {
		[unitTextField setStringValue:[image valueForKeyPath:@"calibration.unit"]];
	}
	else {
		[unitTextField setStringValue:@"pixels"];
	}
	
	// Show the Open Adjustments panel if the image has not been thresholded
	[blobProgressIndicator stopAnimation:self];
	[blobProgressIndicator setHidden:YES];
	
	
	if ([[self window] isVisible]) {
		[[self window]  orderOut:self];
		[self performCloseCleanup];


		
	}
	else {
		[[self window]  setAlphaValue:1.0];
		[[self window]  makeKeyAndOrderFront:sender];
		//[[[self window]  animator] setAlphaValue:1.0];
		self.thresholdFilterActive = YES;
        
		[imageArrayController addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionInitial context:nil];
        isObservingSelection = YES;
		
		[self performSelector:@selector(startBlobDetection:) withObject:self afterDelay:0.5];
	}
	
}


- (void)close
{
	if ([[self window] isVisible]) {
		
		[[self window] orderOut:self];
		[self performCloseCleanup];


	}
	
}

- (void)windowWillClose:(NSNotification *)notification
{
	[[self window] orderOut:self];
	
	[self performCloseCleanup];

}


- (void)performCloseCleanup
{
	//NSLog(@"close");

	
	self.thresholdFilterActive = NO;
	[self updateThresholdedImage];
	
	[self.lastSelectedImage removeObserver:self forKeyPath:@"CIFilters"];
	self.lastSelectedImage = nil;
	
    if (isObservingSelection)
        [imageArrayController removeObserver:self forKeyPath:@"selection"];
	
    isObservingSelection = NO;
    
	[fullscreenImageView setTemporaryROIs:nil];
	[saveBlobsButton setEnabled:NO];
	
	
}



@end
