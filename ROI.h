//
//  ROI.h
//  Macnification
//
//  Created by Peter Schols on 04/04/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KSExtensibleManagedObject.h"


@interface ROI : KSExtensibleManagedObject 
{
    NSRect invalidatedRect_;
}

- (ROI *)clone;
- (ROI *)cloneAndAssignToSameImage;

+ (NSArray *)ROIProperties;
- (NSString *)tabText;


- (NSArray *)allPoints;
- (void)setAllPoints:(NSArray *)array;

- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
- (void)drawHandleAtPoint:(NSPoint)point withScale:(float)scale transform:(NSAffineTransform *)trafo view:(NSView *)view;
- (NSRect)drawingRectForHandleAtPoint:(NSPoint)point withScale:(float)scale;
- (NSRect)hitRectForHandleAtPoint:(NSPoint)point withScale:(float)scale;

- (int)handleSize;
- (int)handleHitAreaSize;

- (BOOL)containsPoint:(NSPoint)point;


- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
- (int)handleUnderPoint:(NSPoint)point;
- (BOOL)isHandleAtPoint:(NSPoint)point;
- (NSPoint)oppositePointForHandle:(int)handle;
- (void)moveWithDelta:(NSPoint)deltaPoint;
- (void)updateMeasurements;
- (NSRect)boundingRect;

// the rectangle that was invalidated since the last transforming operation (move handle, move ROI, ...)
// requesting the property resets the rect to zero
- (NSRect)invalidatedRect;

- (NSRect)noteIconRect;
- (void)drawNoteIconInRect:(NSRect)rect withScale:(float)scale;
- (void)drawNoteIconWithScale:(CGFloat)scale transform:(NSAffineTransform *)trafo view:(NSView *)view;


- (void)calculateLength;
- (void)calculateArea;
- (void)calculatePerimeter;
- (void)calculateColorContents;
- (void)calculateColorContentsWithSharedImage:(NSBitmapImageRep *)img;

- (NSColor *)summaryColor; 


@end
