//
//  CurvedLineROI.h
//  Filament
//
//  Created by Peter Schols on 14/01/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ROI.h"
#import "DistanceROI.h"

@interface CurvedLineROI : DistanceROI {
	
	BOOL isCurrentlyBeingDrawn;
	NSPoint movedPoint;
}




// Defines the start and end of the curved line segment
- (void)setPolygonPoints:(NSArray *)pointsArray;

- (void)resizeByMovingHandle:(int)handle toPoint:(NSPoint)point withOppositePoint:(NSPoint)oppositePoint;
- (void)updateMeasurements;

- (void)calculateLength;

- (float)pixelLengthBetweenPoint:(NSPoint)point1 andPoint:(NSPoint)point2;
- (void)drawWithScale:(float)scale stroke:(float)stroke isSelected:(BOOL)isSelected transform:(NSAffineTransform *)trafo view:(NSView *)view;
- (NSBezierPath *)pathWithScale:(float)scale;
- (BOOL)containsPoint:(NSPoint)point;
- (NSString *)summary;


// Accessors
- (BOOL)isCurrentlyBeingDrawn;
- (void)setIsCurrentlyBeingDrawn:(BOOL)value;
- (NSPoint)movedPoint;
- (void)setMovedPoint:(NSPoint)value;

@end
