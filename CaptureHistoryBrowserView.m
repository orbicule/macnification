//
//  CaptureHistoryBrowserView.m
//  Filament
//
//  Created by Dennis Lorson on 11/02/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import "CaptureHistoryBrowserView.h"
#import "MGCaptureImageBrowserCell.h"

@implementation CaptureHistoryBrowserView


- (IKImageBrowserCell *) newCellForRepresentedItem:(id)anItem;
{
	// DO NOT release
	return [[MGCaptureImageBrowserCell alloc] init];
}




@end
