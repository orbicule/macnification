//
//  LightTableImageView.h
//  LightTable
//
//	The container view of one light table image.  Bound to its model object (LightTableImage).
//	Contains the actual NSImageView subclass as subview, and does the custom event handling and drawing needed for resizing etc.
//  Contains a LightTableImageToolView for custom drawing of controls, bounding box,...
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>

extern CGFloat LightTableMinimumDimension;

@class LightTableImage;
@class LightTableImageContentView;
@class LightTableImageToolView;
@class LightTableAlignConstraint;

#import "LightTableImageToolView.h"

@interface LightTableImageView : NSView <NSCopying> 
{

	LightTableImage *lightTableImage;			// the core data managed object associated with this view.  This reference is needed to correctly update the views,
												// and it makes a direct binding between the view and the model object possible (without the main LT view having to take care of
												// every image-level change)
	LightTableImageContentView *imageView;		// the actual image subview.
	LightTableImageToolView *toolView;			// the tool view, used to display the control points etc.  This is the top one of the subviews.
	
	NSMutableArray *layoutAlignConstraints;		// the layout constraints imposed BY this image view ON other image views.

	
	// temporary and state variables
			
	BOOL selected;
	BOOL hasDragged;
	BOOL hasMouseDown;
	BOOL isMoving;
	BOOL displaysControlPoints;
	ControlPoint currentDragControlPoint;		// the control point the user is currently dragging.  NoControlPoint if not dragging any.
	NSTimer *mouseDownTimer;					// responsible for invoking an autoscroll update every time it fires. (only activated during drag)
	
	NSTimer *keyboardTimer;						// internal timer to control keyboard action timeout.
	
	NSSize previousSize;						// stores the current size when an image is scaled to actual size.  used to toggle between minimized and actual size with a double click.
	BOOL isRealSize;							// determines whether this image is at real size.
	
	CGFloat imageWidthToHeightRatio;

}

@property(assign, nonatomic) LightTableImage *lightTableImage;		// do not retain (View -> Controller)
@property(readonly) LightTableImageToolView *toolView;
@property(nonatomic) BOOL selected, displaysControlPoints;
@property(nonatomic) NSRect frame;
@property(nonatomic) NSSize previousSize;

- (void)initSubviews;

- (NSRect)imageViewFrame;
- (void)setImage:(NSImage *)theImage;
- (NSImage *)image;
- (CGFloat)imageWidthToHeightRatio;


- (void)keyDownOnSuperview:(NSEvent *)theEvent;

- (void)setFrame:(NSRect)frame withEvent:(NSEvent *)theEvent constrainIfNeeded:(BOOL)constrain move:(BOOL)move;
- (NSRect)resizedFrameWithOldFrame:(NSRect)oldFrame newSize:(NSSize)newSize controlPoint:(ControlPoint)controlPoint;


- (void)addAlignConstraint:(LightTableAlignConstraint *)constraint;
- (void)removeLayoutConstraints;


@end
