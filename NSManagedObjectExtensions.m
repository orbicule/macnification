//
//  NSManagedObjectExtensions.m
//  Filament
//
//  Created by Peter Schols on 24/05/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "NSManagedObjectExtensions.h"


@implementation NSManagedObject (NSManagedObjectExtensions)


- (NSManagedObject *)clone;
{
	NSEntityDescription *entity = [self entity];
	NSString *name = [entity name];
	NSManagedObject *newObject = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:[self managedObjectContext]];
	
	// Copy all attributes, not the relationships
	NSDictionary *properties = [entity attributesByName];
	
	NSEnumerator *e = [properties keyEnumerator];
	NSString *propertyKey;
	
	while ((propertyKey = [e nextObject])) {
		[newObject setValue:[self valueForKey:propertyKey] forKey:propertyKey];
	}
		
	return newObject;
}

- (NSString *)description
{
	// return the nsobject description
	return [super description];
	
}


@end
