//
//  MGMetadataExportController.h
//  Filament
//
//  Created by Dennis Lorson on 08/10/10.
//  Copyright 2010 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


// --------------------------------------------------------------------------------------------------------------


typedef enum MGMetadataExportContent_
{
	MGMetadataExportContentGeneralMetadata,
	MGMetadataExportContentROIMetadata
}
MGMetadataExportContent;

typedef enum MGMetadataExportStyle_
{
	MGMetadataExportStyleTable,
	MGMetadataExportStylePlain
}
MGMetadataExportStyle;


// --------------------------------------------------------------------------------------------------------------




@interface MGMetadataExportController : NSObject <NSTableViewDelegate, NSTableViewDataSource>
{
	
	IBOutlet NSWindow *sheet_;
	
	IBOutlet NSPopUpButton *contentPopUpButton_;
	IBOutlet NSPopUpButton *separatorPopUpButton_;
	IBOutlet NSMatrix *styleMatrix_;
	IBOutlet NSTableView *includedAttributesTableView_;
	IBOutlet NSTextField *placeholderTextField_;
	IBOutlet NSTextField *fileFormatTextField_;
	IBOutlet NSTextField *noPreviewField_;
	
	IBOutlet NSTextView *rtfPreview_;
	IBOutlet NSTextView *tabDelimitedPreview_;
	NSTextStorage *textStorage_;
	
	MGMetadataExportContent content_;
	MGMetadataExportStyle style_;
	
	NSArray *images_;
	NSMutableArray *sortedAttributes_;			// all available attrs (sorted)
	NSMutableArray *selectedAttributes_;		// those attrs that have been selected (unsorted)
	NSDictionary *humanReadableAttributeNames_;
	
	int draggedTableViewIndex_;
	
	BOOL shouldExport_;
	
}

- (void)setImages:(NSArray *)images;
- (void)showSheet;



- (IBAction)changeContentType:(id)sender;
- (IBAction)changeStyle:(id)sender;
- (IBAction)closeSheet:(id)sender;
- (IBAction)changePlaceholderText:(id)sender;
- (IBAction)changeSeparator:(id)sender;
- (IBAction)changeShowStacksAsOneEntry:(id)sender;

- (IBAction)selectAllAttributes:(id)sender;
- (IBAction)selectNoAttributes:(id)sender;

@end
