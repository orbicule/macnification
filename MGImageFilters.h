//
//  MGImageFilters.h
//  Filament
//
//  Created by Dennis Lorson on 22/06/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>


void MGRegisterImageFilters();


@interface MGRegisteredFilter : CIFilter
{

}

+ (void)registerFilterName;


@end

@interface ThresholdCompositorFilter : MGRegisteredFilter
{
    CIImage      *inputImage;
    CIImage		 *inputBinaryImage;
    CIColor      *inputOverlayColor;
	
	NSNumber	 *inputIsActive;
	
}

@end


@interface ThresholdFilter : MGRegisteredFilter
{
    CIImage      *inputImage;
	CIColor		 *inputLowerThreshold;
	CIColor		 *inputUpperThreshold;
	CIColor		 *inputBlockVector;
	NSNumber	 *inputBlurRadius;
    
    
}

@end



@interface LUTColormapFilter : MGRegisteredFilter
{
    CIImage      *inputImage;
	NSString     *inputLUTPath;
	
	CIImage *lut;
	
	NSInteger colormap[256][3];
    
}


- (BOOL)importLUT:(NSString *)file;
- (BOOL)importBinaryLUT:(NSString *)file hasHeader:(BOOL)hasHeader;
- (BOOL)importAsciiLUT:(NSString *)file;


@end



@interface DODGaussianBlurFilter : MGRegisteredFilter
{
    CIImage      *inputImage;
    NSNumber     *inputRadius;
    
}

@end


typedef enum _ColorspaceConversionType 
{
	
	RGB2HSLConversion = 1,
	HSL2RGBConversion = 2,
	RGB2GrayConversion = 3,
	
	
} ColorspaceConversionType;

@interface ColorspaceConversionFilter : MGRegisteredFilter
{
    CIImage      *inputImage;
	NSNumber	 *inputConversionType;
}

@end


@interface InvertFilter : MGRegisteredFilter 
{
	CIImage *inputImage;
	NSNumber *inputEnabled;
}


@end

@interface WhiteBalanceCorrectionFilter : MGRegisteredFilter 
{
	CIImage *inputImage;
	CIColor *inputColor;
}


@end

