//
//  DM3Importer.m
//  Filament
//
//  Created by Dennis Lorson on 11/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MGDM3FileImporter.h"
#import "MGByteAccessTools.h"
#import "MGBitmapImporter.h"
#import "MGMetadataSet.h"
#import "MGImageFileImporter_Private.h"

#include <stdio.h>


#define DM3_MAXDEPTH 64
#define DM3_IMGLIST @"root.ImageList."
#define DM3_OBJLIST @"root.DocumentObjectList."

#define DM3_SHORT     2
#define DM3_LONG      3
#define DM3_USHORT    4
#define DM3_ULONG     5
#define DM3_FLOAT     6
#define DM3_DOUBLE    7
#define DM3_BOOLEAN   8
#define DM3_CHAR      9
#define DM3_OCTET     10
#define DM3_STRUCT    15
#define DM3_STRING    18
#define DM3_ARRAY     20


//#define DEBUG_LOG


@interface MGDM3FileImporter ()

- (void)_parseDM3:(NSString *)filePath;
- (NSDictionary *)_dm3FileInfo:(NSString *)filePath;
- (NSDictionary *)_dm3CalibrationInfo;
- (NSDictionary *)_metadataDict;


- (int)_readTagGroup:(FILE *)file;
- (NSString *)_makeGroupString;
- (int)_readTagEntry:(FILE *)file;
- (NSString *)_makeGroupNameString;
- (int)_readTagType:(FILE *)file;
- (int)_readAnyData:(FILE *)file;
- (NSNumber *)_readNativeDataOfType:(int)encodedType size:(int)size inFile:(FILE *)file;
- (NSString *)_readStringData:(int)size inFile:(FILE *)file;
- (NSArray *)_readArrayTypes:(FILE *)file;
- (int)_readArrayData:(NSArray *)arrayTypes inFile:(FILE *)file;
- (NSArray *)_readStructTypes:(FILE *)file;
- (int)_readStructData:(NSArray *)structTypes inFile:(FILE *)file;
- (int)_encodedTypeSize:(int)encodedType;
- (void)_storeTag:(id)tag forKey:(NSString *)key;
- (NSString *)_readString:(int)size inFile:(FILE *)file;




@end



@implementation MGDM3FileImporter

+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	return [[[path lastPathComponent] lowercaseString] hasSuffix:@"dm3"];
}

+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:@"dm3", nil];
}

- (float)fractionOfImportTimeForNonRepresentationWork;
{
	return 0.2;
}



- (id)initWithFilePath:(NSString *)filePath
{
	if ((self = [super initWithFilePath:filePath])) {
		
		useGatanMinMax = YES;
		littleEndian = YES;
		
		chosenImage = 1;
		
		curGroupLevel = -1;
		curGroupAtLevelX = (int *)calloc(DM3_MAXDEPTH, sizeof(int));
		curTagAtLevelX = (int *)calloc(DM3_MAXDEPTH, sizeof(int));
		curGroupNameAtLevelX = (NSString **)calloc(DM3_MAXDEPTH, sizeof(NSString **));

		storedTags = [[NSMutableArray array] retain];
		tagDict = [[NSMutableDictionary dictionary] retain];
		
		
		[self _parseDM3:filePath];
		
	}
	
	return self;
	
}

- (void)dealloc
{
	free(curGroupAtLevelX);
	free(curGroupNameAtLevelX);
	free(curTagAtLevelX);
	
	[storedTags release];
	[tagDict release];
	
	[super dealloc];
}

- (MGMetadataSet *)_generateMetadata;
{
	NSDictionary *dict = [self _metadataDict];
	
	MGMetadataSet *set = [super _generateMetadata];
	
	for (NSString *key in [dict allKeys])
		[set setValue:[dict valueForKey:key] forKey:key];
	
	
	return set;
}

- (NSDictionary *)_metadataDict
{
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	
	[dict addEntriesFromDictionary:[self _dm3FileInfo:filePath_]];
	NSDictionary *calibration = [self _dm3CalibrationInfo];
  
	[dict addEntriesFromDictionary:calibration];

	return dict;
}


- (int)numberOfImageRepresentations
{
	return 1;
}

- (NSBitmapImageRep *)imageRepresentationAtIndex:(int)index
{
	// MGBitmapImporter takes care of endianness and range, so don't duplicate this
	
	if (index == 0)
		return [MGBitmapImporter importRepWithMetadata:[self _metadataDict]];
	
	return nil;
}



#pragma mark -
#pragma mark Interpreting DM3 tags



- (NSDictionary *)_dm3FileInfo:(NSString *)filePath
{
	NSMutableDictionary *md = [NSMutableDictionary dictionary];
	
	NSNumber *value;
	NSString *key;
	
	[md setObject:filePath forKey:@"filePath"];
	[md setObject:[NSNumber numberWithBool:littleEndian] forKey:@"littleEndian"];
	
	chosenImage = 0;
	
	int i = 0;
	long largestDataSizeSoFar = 0;
	
	while (YES) {
		
		NSString *rString = @".ImageData.Data.Size";
		
		key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)i, rString];
		if ((value = [tagDict objectForKey:key])) {
			
			long dataSize = [value longValue];
			
			if (dataSize > largestDataSizeSoFar) {
				largestDataSizeSoFar = dataSize;
				chosenImage = i;
			}
			
			i++;
		} else {
			break;
		}
		
	}
    
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.DataType"];
	value = [tagDict objectForKey:key];
	int dataType = [value intValue];
	
	
	switch (dataType) {
			
		case 1:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY16_SIGNED] forKey:@"fileType"];
			break;
		case 10:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY16_UNSIGNED] forKey:@"fileType"];
			break;
		case 2:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY32_FLOAT] forKey:@"fileType"];
			break;
		case 6:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY8] forKey:@"fileType"];
			break;
		case 7:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY32_INT] forKey:@"fileType"];
			break;
		case 11:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY32_UNSIGNED] forKey:@"fileType"];
			break;
		case 8:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_RGB] forKey:@"fileType"];
			break;
		case 14:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_BITMAP] forKey:@"fileType"];
			break;
		case 23:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_ARGB] forKey:@"fileType"];
			break;
		default:
			[md setObject:[NSNumber numberWithInt:MG_IMAGE_GRAY16_UNSIGNED] forKey:@"fileType"];
			NSLog(@"DM3Importer: unsuitable image format found");
			break;
			
	}
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.Dimensions.0"];
	value = [tagDict objectForKey:key];
	[md setObject:value forKey:@"imageWidth"];
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.Dimensions.1"];
	value = [tagDict objectForKey:key];
	[md setObject:value forKey:@"imageHeight"];
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.Data.Offset"];
	value = [tagDict objectForKey:key];
	[md setObject:value forKey:@"imageOffset"];
	
	
	// range of the brightness
	for (NSString *key in [tagDict allKeys]) {
		
		NSRange range = [key rangeOfString:@"ImageDisplayInfo.HighLimit"];
		
		if (range.location != NSNotFound && (range.location + range.length == [key length])) {
			
			[md setObject:[tagDict objectForKey:key] forKey:@"imageBrightnessRangeMax"];
		}
		
		range = [key rangeOfString:@"ImageDisplayInfo.LowLimit"];
		
		if (range.location != NSNotFound && (range.location + range.length == [key length])) {
			
			[md setObject:[tagDict objectForKey:key] forKey:@"imageBrightnessRangeMin"];
		}
		
	}
	
	// other metadata
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageTags.Microscope Info.Indicated Magnification"];
	value = [tagDict objectForKey:key];
	if (value)
		[md setObject:value forKey:MGMetadataInstrumentMagnificationKey];
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageTags.Microscope Info.Voltage"];
	value = [tagDict objectForKey:key];
	if (value)
		[md setObject:value forKey:MGMetadataInstrumentVoltageKey];
	
	
	
	NSString *imageName = [[filePath lastPathComponent] stringByDeletingPathExtension];
	
	[md setObject:imageName forKey:MGMetadataImageNameKey];
	
	return md;
}

- (NSDictionary *)_dm3CalibrationInfo
{
	NSMutableDictionary *calMD = [NSMutableDictionary dictionary];
	
	NSNumber *value;
	NSString *key;
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.Calibrations.Dimension.0.Units"];
	NSString *unit = [tagDict objectForKey:key];
	
	if (!unit) return nil;
	
	if ([unit length] > 1 && [[unit substringToIndex:2] isEqualToString:@"1/"])
		unit = [unit substringFromIndex:2];
	
	[calMD setObject:unit forKey:MGMetadataCalibrationUnitKey];
	
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.Calibrations.Dimension.0.Scale"];
	value = [tagDict objectForKey:key];
	if (!value) return nil;
	[calMD setObject:value forKey:MGMetadataCalibrationPixelWidthKey];
	
	key = [NSString stringWithFormat:@"%@%i%@", DM3_IMGLIST, (int)chosenImage, @".ImageData.Calibrations.Dimension.1.Scale"];
	value = [tagDict objectForKey:key];
	if (!value) return nil;
	[calMD setObject:value forKey:MGMetadataCalibrationPixelHeightKey];
	
	return calMD;
}



#pragma mark -
#pragma mark DM3



- (void)_parseDM3:(NSString *)filePath
{	
	const char *cfilename = [[filePath stringByExpandingTildeInPath] cStringUsingEncoding:NSISOLatin1StringEncoding];
	FILE *file = fopen(cfilename, "r");
	
	int fileVersion = MGTIFFReadInt(file, NO);
	if (fileVersion != 3) NSLog(@"DM3Importer: wrong DM3 file version (%d)", fileVersion);
	
	MGTIFFReadInt(file, NO);	// file size
	littleEndian = MGTIFFReadInt(file, NO);
	
	curGroupNameAtLevelX[0] = @"root";
	
	[self _readTagGroup:file];
	
	fclose(file);
}


- (int)_readTagGroup:(FILE *)file
{
	curGroupLevel += 1;
	curGroupAtLevelX[curGroupLevel]++;
	curTagAtLevelX[curGroupLevel] = -1;
	
	MGTIFFReadByte(file);		// isSorted
	MGTIFFReadByte(file);		// isOpen
	int nTags = MGTIFFReadInt(file, NO);
	
	int i;
	for (i = 0; i < nTags; i++) {
#ifdef DEBUG_LOG
		NSLog(@"Reading tag entry %i", i);
#endif
		[self _readTagEntry:file];
	}
	
	curGroupLevel -= 1;
	
	return 1;
}

- (NSString *)_makeGroupString
{
	NSMutableString *string = [NSMutableString string];
	
	[string appendFormat:@"%i", (int)curGroupAtLevelX[0]];
	
	int i;
	for(i = 0; i <= curGroupLevel; i++)
		[string appendFormat:@".%i", (int)curGroupAtLevelX[i]];
	
	return string;
}


- (int)_readTagEntry:(FILE *)file
{
	int isData = MGTIFFReadByte(file);
	
	curTagAtLevelX[curGroupLevel]++;
	
	int lenTagLabel = MGTIFFReadShort(file, NO);
	
	NSString *tagLabel;
	if (lenTagLabel != 0)
		tagLabel = [self _readString:lenTagLabel inFile:file];
	else
		tagLabel = [NSString stringWithFormat:@"%i", (int)curTagAtLevelX[curGroupLevel]];
	
#ifdef DEBUG_LOG
	NSLog(@"isData %i, tagLabel %@, tagLength %i", isData, tagLabel, lenTagLabel);
#endif
	
	if (isData == 21) {
		
		curTagName = [NSString stringWithFormat:@"%@.%@", [self _makeGroupNameString], tagLabel];
		
#ifdef DEBUG_LOG
		NSLog(@"curTagName: %@", curTagName);
#endif
		
		[self _readTagType:file];
		
	} else {
		
		curGroupNameAtLevelX[curGroupLevel + 1] = tagLabel;
		[self _readTagGroup:file];
		
	}
	
	return 1;
}

- (NSString *)_makeGroupNameString
{
	NSMutableString *string = [NSMutableString stringWithFormat:@"%@", curGroupNameAtLevelX[0]];
	
	int i;
	for (i = 1; i <= curGroupLevel; i++)
		[string appendFormat:@".%@", curGroupNameAtLevelX[i]];
	
	return string;
}

- (int)_readTagType:(FILE *)file
{
	int delim = MGTIFFReadInt(file, NO);
	
	if (delim != 0x25252525)
		NSLog(@"DM3Importer: ERROR: tag type delimiter incorrect");

	MGTIFFReadInt(file, NO);	// nInTag
	
	[self _readAnyData:file];
	
	return 1;
	
}


- (int)_readAnyData:(FILE *)file
{
	int encodedType = MGTIFFReadInt(file, NO);
	
	int etSize = [self _encodedTypeSize:encodedType];
	
#ifdef DEBUG_LOG
	NSLog(@"encodedType %i, etSize %i", encodedType, etSize);
#endif

	
	if(etSize > 0) {
		[self _storeTag:[self _readNativeDataOfType:encodedType size:etSize inFile:file] forKey:curTagName];
		
	} else if (encodedType == DM3_STRING) {

		
		int stringSize = MGTIFFReadInt(file, NO);
		[self _readStringData:stringSize inFile:file];
	} else if (encodedType == DM3_STRUCT) {
		
		NSArray *structTypes = [self _readStructTypes:file];
		[self _readStructData:structTypes inFile:file];
		
	} else if (encodedType == DM3_ARRAY) {
		
		NSArray *arrayTypes = [self _readArrayTypes:file];
		[self _readArrayData:arrayTypes inFile:file];
	} else {
		
		NSLog(@"DM3Importer: ERROR: Unknown encoded type");
		
	}
	return 1;
}


- (NSNumber *)_readNativeDataOfType:(int)encodedType size:(int)size inFile:(FILE *)file
{
	NSNumber *val = nil;
	
	if (encodedType == DM3_SHORT) {
		val = [NSNumber numberWithShort:MGTIFFReadShort(file, littleEndian)];
	}
	else if (encodedType == DM3_LONG) 
	{
		val = [NSNumber numberWithInt:MGTIFFReadInt(file, littleEndian)];
	}
	else if (encodedType == DM3_USHORT) 
	{
		val = [NSNumber numberWithShort:MGTIFFReadShort(file, littleEndian)];	// ushort should be the same as short
	}
	else if (encodedType == DM3_ULONG) 
	{
		val = [NSNumber numberWithInteger:MGTIFFReadInt(file, littleEndian)];
	}
	else if (encodedType == DM3_FLOAT) 
	{
		val = [NSNumber numberWithFloat:MGTIFFReadFloat(file, littleEndian)];
	}
	else if (encodedType == DM3_DOUBLE) 
	{
		val = [NSNumber numberWithDouble:MGTIFFReadDouble(file, littleEndian)];
	}
	else if (encodedType == DM3_BOOLEAN) 
	{
		val = [NSNumber numberWithBool:MGTIFFReadByte(file)];
	}
	else if (encodedType == DM3_CHAR || encodedType == DM3_OCTET) 
	{
		val = [NSNumber numberWithChar:MGTIFFReadByte(file)];
	} else {
		
		NSLog(@"DM3Importer: ERROR: Unknown encoded type");
	}
	
	return val;
	
}

- (NSString *)_readStringData:(int)size inFile:(FILE *)file
{
	
	if (size < 1) return [NSString string];
	
	if (size > 100000) {
		NSLog(@"DM3Importer: ERROR: String size too large");
	}
	
	char *buf = (char *)calloc(size + 1, sizeof(char));
	
	fread(buf, sizeof(char), size, file);
	
	buf[size] = '\0';
	
	NSMutableString *str = [NSMutableString string];
	
	int i;
	for (i = 0; i < size; i+=2) {
		[str appendFormat:@"%c", (buf[(littleEndian ? i+1 : i)]&0xFF) <<8 | (buf[(littleEndian ? i : i+1)] & 0xFF)];
	}
	
	// the normal NSString method does not work (it quits on encountering a \0 sign)
	//NSString *rString = [NSString stringWithCString:buf 
	//		encoding:((littleEndian) ? NSUTF16LittleEndianStringEncoding : NSUTF16BigEndianStringEncoding)];
	
	[self _storeTag:str forKey:curTagName];
	
	free(buf);

	return str;
}


- (NSArray *)_readArrayTypes:(FILE *)file
{
	int arrayType = MGTIFFReadInt(file, NO);
	
	NSMutableArray *itemTypes = [NSMutableArray array];
	
	if (arrayType == DM3_STRUCT)
		itemTypes = [[[self _readStructTypes:file] mutableCopy] autorelease];
	else if (arrayType == DM3_ARRAY)
		itemTypes = [[[self _readArrayTypes:file] mutableCopy] autorelease];
	else
		[itemTypes addObject:[NSNumber numberWithInt:arrayType]];
	
	return itemTypes;
}

- (int)_readArrayData:(NSArray *)arrayTypes inFile:(FILE *)file
{
	int arraySize = MGTIFFReadInt(file, NO);
	
	int itemSize = 0;
	
	int encodedType = 0;
	
	int i;
	for(i = 0; i < [arrayTypes count]; i++) {
		
		encodedType = [[arrayTypes objectAtIndex:i] intValue];
		int etSize = [self _encodedTypeSize:encodedType];
		
		itemSize += etSize;
		
	}
	
	long bufSize = (long)arraySize * (long)itemSize;
	
	NSRange range = [curTagName rangeOfString:@"ImageData.Data"];
	
	if (encodedType == DM3_USHORT && arraySize < 256 && [arrayTypes count] == 1 
		&& !(range.location != NSNotFound && (range.location + range.length == [curTagName length]))) 
	{
		[self _readStringData:bufSize inFile:file]; // NSString *val
	}
	else 
	{
		[self _storeTag:[NSNumber numberWithLong:bufSize] forKey:[NSString stringWithFormat:@"%@.Size", curTagName]];		
		[self _storeTag:[NSNumber numberWithLong:ftell(file)] forKey:[NSString stringWithFormat:@"%@.Offset", curTagName]];
		
		fseek(file, bufSize, SEEK_CUR);
	}
	
	return 1;
}


- (NSArray *)_readStructTypes:(FILE *)file
{
	MGTIFFReadInt(file, NO); //int structNameLength = 
	int nFields = MGTIFFReadInt(file, NO);
	
	if (nFields > 100) {
		NSLog(@"DM3Importer: ERROR: too many fields");
		return [NSArray array];
	}
	
	NSMutableArray *fieldTypes = [NSMutableArray array];
	
	int nameLength = 0;
	int i;
	for (i = 0; i < nFields; i++) {
		nameLength = MGTIFFReadInt(file, NO);
		int fieldType = MGTIFFReadInt(file, NO);
		
		[fieldTypes addObject:[NSNumber numberWithInt:fieldType]];
	}
	
	return fieldTypes;
	
}


- (int)_readStructData:(NSArray *)structTypes inFile:(FILE *)file
{
	NSMutableString *structAsString = [NSMutableString string];
	
	int i;
	for (i = 0; i < [structTypes count]; i++) {
		
		int encodedType = [[structTypes objectAtIndex:i] intValue];
		
		int etSize = [self _encodedTypeSize:encodedType];
		
		[structAsString appendFormat:@"%f", [[self _readNativeDataOfType:encodedType size:etSize inFile:file] floatValue]];
	}
	
	[self _storeTag:[NSString stringWithFormat:@"{%@}", structAsString] forKey:curTagName];
	return 1;
}

- (int)_encodedTypeSize:(int)encodedType
{
	int width = -1;
	
	switch(encodedType) {
			
		case MG_IMAGE_GRAY8:
			width = 0;
			break;
			
		case MG_IMAGE_BITMAP:
		case MG_IMAGE_ARGB:
		case MG_IMAGE_BGR:
			width = 1;
			break;
			
		case MG_IMAGE_GRAY16_UNSIGNED:
		case MG_IMAGE_GRAY32_FLOAT:
			width = 2;
			break;
			
		case MG_IMAGE_GRAY32_INT:
		case MG_IMAGE_COLOR8:
		case MG_IMAGE_RGB:
			width = 4;
			break;
			
		case MG_IMAGE_RGB_PLANAR:
			width = 8;
			break;
	}
	
	return width;
	
}


- (void)_storeTag:(id)tag forKey:(NSString *)key
{

	
	if ([tag isKindOfClass:[NSString class]])
		[storedTags addObject:[NSString stringWithFormat:@"%@ = %@", key, tag]];
	else if ([tag isKindOfClass:[NSNumber class]])
		[storedTags addObject:[NSString stringWithFormat:@"%@ = %f", key, [tag floatValue]]];
	else
		[storedTags addObject:[NSString stringWithFormat:@"%@ = <other data type>", key]];

	[tagDict setObject:tag forKey:key];
}


- (NSString *)_readString:(int)length inFile:(FILE *)file
{
	char *buf = (char *)calloc(length+1, sizeof(char));
	
	fread(buf, sizeof(char), length, file);
	
	buf[length] = '\0';
	
	NSString *string = [NSString stringWithCString:buf encoding:NSISOLatin1StringEncoding];
	
	free(buf);
	
	return string;
}



@end
