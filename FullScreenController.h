//
//  FullScreenController.h
//  Filament
//
//  Created by Peter Schols on 27/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ImageKit/ImageKit.h>

@class AdjustmentsController;
@class MTFullScreenWindow;
@class MAAttachedWindow;
@class PSImageView;
@class BlobController;
@class FullScreenImageArrayController;

@interface FullScreenController : NSObject {
	
	NSScreen *mainWindowScreen;
	
	
	BOOL ROIHUDWasOpen;
	BOOL isInFullScreen;
	
	IBOutlet id ROIHUD;
	
	IBOutlet NSWindow *mainWindow;
	IBOutlet NSWindow *fullScreenWindow;
	IBOutlet NSArrayController *imageArrayController;
	IBOutlet FullScreenImageArrayController *fullScreenImageArrayController;
	IBOutlet NSWindow *calibrationHUD;
	IBOutlet NSWindow *infoPanel;
	
	IBOutlet NSSplitView *fsSplitView;
	
	MTFullScreenWindow *fsWindow;

	// Tools HUD
	MAAttachedWindow *toolsHUD;
	IBOutlet id toolsHUDView;

	IBOutlet IKImageBrowserView *imageBrowser;
	IBOutlet NSTextField *toggleROITextField;
	IBOutlet PSImageView *imageView;
	
	AdjustmentsController *adjustmentsController;
	BlobController *blobController;
	
	// Toolstrip
	IBOutlet NSWindow *toolstrip;
	
}

- (IBAction)enterFullScreen:(id)sender;
- (IBAction)exitFullScreen:(id)sender;
- (IBAction)toggleROIs:(id)sender;

- (BOOL)isInFullScreen;

- (void)fadeInToolstrip;

// Selection
- (IBAction)selectNext:(id)sender;
- (IBAction)selectPrevious:(id)sender;

- (IBAction)showAdjustmentsPanel:(id)sender;
- (IBAction)showBlobExtractionPanel:(id)sender;
- (IBAction)adjustSelectedImage:(id)sender;


- (void)updateScreens;


@end
