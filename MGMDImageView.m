//
//  MGMDImageView.m
//  Filament
//
//  Created by Dennis Lorson on 1/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDImageView.h"
#import "NSImage_MGAdditions.h"
#import "ImageArrayController.h"
#import "MGCIContextManager.h"

@implementation MGMDImageView

@synthesize ciImage;

- (void)awakeFromNib
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redrawStatusChanged:) name:@"DidEnterFullscreen" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redrawStatusChanged:) name:@"DidLeaveFullscreen" object:nil];	
}


- (void)redrawStatusChanged:(NSNotification *)not
{
	if ([[not name] isEqualToString:@"DidEnterFullscreen"]) {
		ignoreViewUpdates = YES;

	}
	
	if ([[not name] isEqualToString:@"DidLeaveFullscreen"]) {
		ignoreViewUpdates = NO;	
		[self setNeedsDisplay:YES];
	}
}


- (void)setCiImage:(CIImage *)img
{
	if (ciImage != img) {
		
		[ciImage release];
		ciImage = [img retain];
		
		
		[self setNeedsDisplay:YES];
	}
}

- (void)drawRect:(NSRect)frame
{
	//return;
	if (ignoreViewUpdates) return;

	if (!self.ciImage) {
		
		NSRect rect = NSInsetRect([self bounds], 12, 10);
		rect.origin.y = 3;
		rect.size.height = [self bounds].size.height - 10;
		
		NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:rect xRadius:5 yRadius:5];
		
		const CGFloat dash[2] = {5, 5};
		[path setLineDash:dash count:2 phase:0];
		
		[path setLineWidth:2.0];
		
		[[NSColor colorWithCalibratedWhite:0.65 alpha:1.0] set];
		
		[path stroke];
		
	} else {
				
		NSRect bounds = [self bounds];
		
		NSSize pixelSize = NSMakeSize([self.ciImage extent].size.width, [self.ciImage extent].size.height);
		NSSize boundsSize = [self bounds].size;
		
		NSSize destSize;
		
		if (pixelSize.width/boundsSize.width > pixelSize.height/boundsSize.height) {
			
			destSize.width = boundsSize.width;
			destSize.height = boundsSize.width * pixelSize.height / pixelSize.width;
			
		} else {
			
			destSize.height = boundsSize.height;
			destSize.width = boundsSize.height * pixelSize.width / pixelSize.height;
			
		}
		
		NSRect destRect;
		destRect.size = destSize;
		destRect.origin.x = NSMidX(bounds) - NSWidth(destRect)/2;
		destRect.origin.y = NSMidY(bounds) - NSHeight(destRect)/2;
		
		[[MGCIContextManager sharedContextManager] renderImage:self.ciImage inRect:NSRectToCGRect(destRect)];
		
	}
}

- (void)manualObserveValueForKeyPath:(NSString *)keyPath ofObject:(id)object
{
	if ([[observableController selectedObjects] count] == 1) {
		
		self.ciImage = [observableController valueForKeyPath:observableKeyPath];
		
	} else {
		
		self.ciImage = nil;
	}
	
}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"selection"]) {
		[self manualObserveValueForKeyPath:observableKeyPath ofObject:observableController];
	}
	else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}


- (void)unbind:(NSString *)binding
{
	if ([binding isEqualToString:@"value"]) {
		[(ImageArrayController *)observableController removeManualObserver:self];
		[observableController removeObserver:self forKeyPath:@"selection"];
		self.ciImage = nil;
		[observableKeyPath release];
		observableKeyPath = nil;
	} else {
		
		[super unbind:binding];
	}
	
}

- (void) bind:(NSString *)binding toObject:(id)observable withKeyPath:(NSString *)keyPath options:(NSDictionary *)options
{
	if ([binding isEqualToString:@"value"]) {
		[(ImageArrayController *)observable addManualObserver:self forKeyPath:keyPath];
		[observable addObserver:self forKeyPath:@"selection" options:0 context:nil];
		
		observableKeyPath = [keyPath copy];
		observableController = observable;
	}
	else {
		[super bind:binding toObject:observable withKeyPath:keyPath options:options];
	}
}



- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.ciImage = nil;
	[super dealloc];
}

#pragma mark -
#pragma mark dragging

- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	return NSDragOperationCopy;
	
	
}

- (id)delegate
{
	return nil;
}

- (NSImage *)changeSizeAndAlphaOfImage:(NSImage *)image
{
	
	NSSize pixelSize = [image size];
	NSSize boundsSize = [self bounds].size;
	
	NSSize destSize;
	
	if (pixelSize.width/boundsSize.width > pixelSize.height/boundsSize.height) {
		
		destSize.width = boundsSize.width;
		destSize.height = boundsSize.width * pixelSize.height / pixelSize.width;
		
	} else {
		
		destSize.height = boundsSize.height;
		destSize.width = boundsSize.height * pixelSize.width / pixelSize.height;
		
	}

	NSImage *copy = [[[NSImage alloc] initWithSize:destSize] autorelease];
	
	[copy lockFocus];
	[image drawInRect:NSMakeRect(0, 0, [copy size].width, [copy size].height) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:0.5];
	[copy unlockFocus];
	
	[copy setSize:destSize];
	
	return copy;
	
}


- (void)mouseDown:(NSEvent *)theEvent
{
	//[super mouseDown:theEvent];
	
	if (!self.ciImage) return;
	
	Image *image = [[observableController selectedObjects] objectAtIndex:0];
	NSImage *filteredImage = [image filteredImage];
	
	NSArray *typesArray = [NSArray arrayWithObjects:NSTIFFPboardType, NSFilenamesPboardType, nil];
	
	NSString *filePath = [image temporaryPathToFilteredImageWithScaleBar];	

    NSSize dragOffset = NSMakeSize(0.0, 0.0);

    NSPasteboard *pboard = [NSPasteboard pasteboardWithName:NSDragPboard];
	[pboard declareTypes:typesArray owner:self];
	[pboard setPropertyList:[NSArray arrayWithObject:filePath] forType:NSFilenamesPboardType];
	
    [self dragImage:[self changeSizeAndAlphaOfImage:filteredImage] at:NSMakePoint(NSMinX([self bounds]), NSMinY([self bounds])) offset:dragOffset event:theEvent pasteboard:pboard source:self slideBack:YES];
	
    return;
}


@end
