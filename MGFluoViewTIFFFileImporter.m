//
//  MGFluoViewTIFFImporter.m
//  Filament
//
//  Created by Dennis Lorson on 18/01/11.
//  Copyright 2011 Orbicule. All rights reserved.
//

#import "MGFluoViewTIFFFileImporter.h"


@interface MGFluoViewTIFFFileImporter ()

+ (NSString *)_startOfFile:(NSString *)path;


@end



@implementation MGFluoViewTIFFFileImporter

+ (BOOL)_canOpenFilePath:(NSString *)path;
{
	return [[[self class] _startOfFile:path] rangeOfString:@"FLUOVIEW Version" options:NSCaseInsensitiveSearch].location != NSNotFound;
}

+ (NSArray *)supportedPathExtensions;
{
	return [NSArray arrayWithObjects:@"tiff", @"tif", nil];
}



+ (NSString *)_startOfFile:(NSString *)path;
{
    

	NSString *file = [path stringByExpandingTildeInPath];
	
	NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:file];
	
	unsigned long long size = [handle seekToEndOfFile];
	
	unsigned long long startPos = 0;//MAX(0, (long long)(size - 1024 * 20));
	
	[handle seekToFileOffset:startPos];
	
	NSMutableData *data = [[[handle readDataOfLength:MIN(size - 1, 1024 * 20)] mutableCopy] autorelease];
	
	char *buff = (char *)[data mutableBytes];
	
	// remove all NULL chars from the data
	NSInteger len = [data length];
	for (int i = 0; i < len; i++) {
		if (buff[i] == '\0')
			buff[i] = '*';
	}
	
	NSString *string = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
	
	return string;
}



@end
