#import "KeywordAnimationWindow.h"


@implementation KeywordAnimationWindow


- (id)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag;
{
	NSScreen* mainScreen = [[NSApp mainWindow] screen]; 
    NSRect winRect = [mainScreen frame];

	if ((self = [super initWithContentRect:winRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO])) {
        [self setLevel: NSNormalWindowLevel];
        [self setBackgroundColor: [NSColor clearColor]];
        [self setAlphaValue:1.0];
        [self setOpaque:NO];
        [self setHasShadow:NO];
		[self setFrame:[mainScreen frame] display:NO animate:YES];
        return self;
    }
	return nil;
}







@end
