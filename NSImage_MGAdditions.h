//
//  NSImage_MGAdditions.h
//  LightTable
//
//  Created by Dennis Lorson on 11/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSImage (MGAdditions) 

- (CGImageRef)cgImage;
- (NSSize)bestRepresentationSize;
- (NSImageRep *)representation;

- (void)setSizeIgnoringDPI;

@end
