//
//  RolloverButton.h
//  Tracker
//
//  Created by Peter Schols on 10/04/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MAAttachedWindow.h"


@interface RolloverButton : NSButton {
	IBOutlet NSView *contentViewForTooltip;
	
	MAAttachedWindow *tooltipWindow;
}


- (void)showTooltipWindow;
- (void)closeToolTip;


@end
