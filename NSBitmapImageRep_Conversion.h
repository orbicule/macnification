//
//  NSBitmapImageRep_PlanarMeshConversion.h
//  Filament
//
//  Created by Dennis Lorson on 22/08/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSBitmapImageRep (Conversion)



+ (NSBitmapImageRep *)imageRepWithMergedChannels:(NSArray *)channels;
- (NSArray *)splitChannels;


- (NSBitmapImageRep *)imageRepWithHostEndiannessFromSourceEndianness:(CFByteOrder)origEndianness;
- (NSBitmapImageRep *)imageRepWithAdjustedDepth;

- (NSBitmapImageRep *)imageRepWithScaledRangeWithMin:(long)min max:(long)max;
- (NSBitmapImageRep *)imageRepWithScaledRange;


@end
