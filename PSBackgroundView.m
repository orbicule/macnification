//
//  PSBackgroundView.m
//  Filament
//
//  Created by Peter Schols on 16/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "PSBackgroundView.h"


@implementation PSBackgroundView


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		self.color = [NSColor colorWithCalibratedRed:0.18 green:0.18 blue:0.18 alpha:1];
    }
    return self;
}


- (void)drawRect:(NSRect)rect;
{
    // Drawing code here.
	[color set];
	[NSBezierPath fillRect:rect];
}


- (BOOL)becomeFirstResponder
{
	
	return YES;
}

@synthesize color;


@end
