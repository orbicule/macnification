#import "SBCenteringClipView.h"

@implementation SBCenteringClipView



- (NSPoint)centerPoint;
{
	NSRect docRect = [(NSView *)[self documentView] frame];
	NSRect clipRect = [self bounds];
	NSPoint point;
	
	// Work out the x co-ordinate that is at the center of the NSScrollView
	if (docRect.size.width > clipRect.size.width)
		point.x = roundf(clipRect.size.width / 2.0 + clipRect.origin.x);
	else
		point.x = roundf(docRect.size.width / 2.0);
	
	// Work out the y co-ordinate that is at the center of the NSScrollView
	if (docRect.size.height > clipRect.size.height)
		point.y = roundf(clipRect.size.height / 2.0 + clipRect.origin.y);
	else
		point.y = roundf(docRect.size.height / 2.0);
		
	return point;
}



- (void)setCenterPoint:(NSPoint)centerPoint;
{
	NSRect docRect = [(NSView *)[self documentView] frame];
	NSRect clipRect = [self bounds];
	NSPoint point;
	
	// If the document is horizontally larger than the scroll view...
	if (docRect.size.width > clipRect.size.width) {
		
		// Request the scroll bars be shown and make the given point the center
		point.x = roundf(centerPoint.x - clipRect.size.width / 2.0);
		
	}
	
	// If the document is vertically larger than the scroll view...
	if (docRect.size.height > clipRect.size.height) {
		
		// Request the scroll bars be shown and make the given point the center
		point.y = roundf(centerPoint.y - clipRect.size.height / 2.0);
		
	}
	
	// Move in to position
	[self scrollToPoint:[self constrainScrollPoint:point]];
	[(NSScrollView *)[self superview] setNeedsDisplay:YES];
	[(NSScrollView *)[self superview] reflectScrolledClipView:self];
}





// ----------------------------------------

-(void)centerDocument
{
    NSRect docRect = [(NSView *)[self documentView] frame];
    NSRect clipRect = [self bounds];
    
    // We can leave these values as integers (don't need the "2.0")
    if( docRect.size.width < clipRect.size.width ) 
        clipRect.origin.x = roundf( ( docRect.size.width - clipRect.size.width ) / 2.0 );
    
    if( docRect.size.height < clipRect.size.height ) 
        clipRect.origin.y = roundf( ( docRect.size.height - clipRect.size.height ) / 2.0 );
   
        
    // Probably the most efficient way to move the bounds origin.
    [self scrollToPoint:clipRect.origin];
    
   

    
    // We could use this instead since it allows a scroll view to coordinate scrolling between multiple clip views.
     //[[self superview] scrollClipView:self toPoint:clipRect.origin];
}

// ----------------------------------------
// We need to override this so that the superclass doesn't override our new origin point.

-(NSPoint)constrainScrollPoint:(NSPoint)proposedNewOrigin
{
    NSRect docRect = [(NSView *)[self documentView] frame];
    NSRect clipRect = [self bounds];
    NSPoint newScrollPoint = proposedNewOrigin;
    float maxX = docRect.size.width - clipRect.size.width;
    float maxY = docRect.size.height - clipRect.size.height;
    
    // If the clip view is wider than the doc, we can't scroll horizontally
    if( docRect.size.width < clipRect.size.width )
        newScrollPoint.x = roundf( maxX / 2.0 );
    
    else
        newScrollPoint.x = roundf( MAX(0,MIN(newScrollPoint.x,maxX)) );
    
        
    // If the clip view is taller than the doc, we can't scroll vertically
    if( docRect.size.height < clipRect.size.height )
        newScrollPoint.y = roundf( maxY / 2.0 );
    
    else
        newScrollPoint.y = roundf( MAX(0,MIN(newScrollPoint.y,maxY)) );
     
    
   
    return newScrollPoint;
}

// ----------------------------------------
// These two methods get called whenever the subview changes

-(void)viewBoundsChanged:(NSNotification *)notification
{
    [super viewBoundsChanged:notification];
    [self centerDocument];
}

-(void)viewFrameChanged:(NSNotification *)notification
{
    [super viewFrameChanged:notification];
    [self centerDocument];
}

// ----------------------------------------
// These superclass methods change the bounds rect directly without sending any notifications,
// so we're not sure what other work they silently do for us. As a result, we let them do their
// work and then swoop in behind to change the bounds origin ourselves. This appears to work
// just fine without us having to reinvent the methods from scratch.

- (void)setFrame:(NSRect)frameRect
{
    [super setFrame:frameRect];
    [self centerDocument];
}

- (void)setFrameOrigin:(NSPoint)newOrigin
{
    [super setFrameOrigin:newOrigin];
    [self centerDocument];
}

- (void)setFrameSize:(NSSize)newSize
{
    [super setFrameSize:newSize];
    [self centerDocument];
}

- (void)setFrameRotation:(CGFloat)angle
{
    [super setFrameRotation:angle];
    [self centerDocument];
}

@end
