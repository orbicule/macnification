//
//  MGMDReadOnlyTextField.m
//  Filament
//
//  Created by Dennis Lorson on 1/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MGMDReadOnlyTextField.h"


@implementation MGMDReadOnlyTextField


- (id) init
{
	self = [super init];
	if (self != nil) {
		[self setEditable:NO];
		[self setEnabled:YES];

	}
	return self;
}


- (id)initContentsFieldWithFrame:(NSRect)frame
{
	if ((self = [super initContentsFieldWithFrame:frame])) {
		
		[self setEditable:NO];
		[self setEnabled:YES];

	}
	return self;
	
}

- (void)setEditable:(BOOL)flag
{	
	[super setEditable:NO];
}


- (void)awakeFromNib
{
	[super awakeFromNib];
	[self setEnabled:YES];
	[self setEditable:NO];
	[self setSelectable:NO];
	
}

- (BOOL)isEditable
{
	return NO;
}

- (BOOL)becomeFirstResponder
{
	return NO;
}

@end
