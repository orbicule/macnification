//
//  ScaleBarPositionView.h
//  Filament
//
//  Created by Peter Schols on 09/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <ImageKit/ImageKit.h>

@interface ScaleBarPositionView : IKImageView {

}

- (void)setScaleBarLayer;
- (IBAction)reDraw:(id)sender;


- (IBAction)filterArray:(id)sender;


@end
