//
//  SmartAlbum.m
//  Macnification
//
//  Created by Peter Schols on 22/11/06.
//  Copyright 2006 Orbicule. All rights reserved.
//

#import "SmartAlbum.h"
#import "MGAppDelegate.h"
#import "MGFilterAndSortMetadataController.h"
#import "SourceListTreeController.h"

@interface SmartAlbum ()

- (NSArray *)imagesUsingStorePredicate:(NSPredicate *)predicate;

@end



@implementation SmartAlbum


+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
	if ([key isEqual:@"dynamicImages"])
		return [NSSet setWithObject:@"predicateData"];
	
	return [NSSet set];
}


#pragma mark Initialisation

- (void)awakeFromInsert  
{
	[super awakeFromInsert];
	//[self setValue:[NSNumber numberWithInt:20] forKey:@"rank"];
}



- (void)prepareForDeletion;
{	
	// This is needed to avoid a validation error
	for (id image in [self primitiveValueForKey:@"images"]) {
		NSMutableSet *albumsForImage = [image mutableSetValueForKey:@"albums"];
		[albumsForImage removeObject:self];
	}

	[self setValue:nil forKey:@"predicateData"];
}



- (NSData *)predicateDataForLastMonthGroup;
{
	// Get the current date
	NSDate *now = [NSDate date];
	
	// Get the date 30 days prior to current date. Because CoreData uses NSDate rather than NSCalendarDate, we'll  use an NSCalendar to do the math
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *dateComps = [[NSDateComponents alloc] init];
	[dateComps setDay:-30];
	NSDate *date30DaysAgo = [calendar dateByAddingComponents:dateComps toDate:now options:0];
	[dateComps release];
	NSPredicate *dateInLast30DaysPredicate = [NSPredicate predicateWithFormat:@"creationDate > %@", date30DaysAgo];
	NSCompoundPredicate *compoundPredicate = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObject:dateInLast30DaysPredicate]];
	return [NSKeyedArchiver archivedDataWithRootObject:compoundPredicate];
}



- (BOOL)acceptsDroppedKeywords;
{
	return YES;
}


- (BOOL)acceptsDroppedImages;
{
	return NO;
}

- (BOOL)acceptsDroppedImagesFromFinder;
{
	return NO;	
}

- (BOOL)canBeDragged;
{
	return [[self valueForKey:@"rank"] intValue] != LAST_MONTH_COLLECTION_RANK;
}


- (BOOL)canEdit;
{
	return [[self valueForKey:@"rank"] intValue] != LAST_MONTH_COLLECTION_RANK;
}

- (int)countOfImages
{
	return [[self dynamicImages] count];
}




#pragma mark Get the images for our smart album


- (NSSet *)dynamicImages;
{	
	if ([self isDeleted] || ![self valueForKey:@"predicateData"])
		return nil;	
	
	
	NSPredicate *predicate = [NSKeyedUnarchiver unarchiveObjectWithData:[self valueForKey:@"predicateData"]];
	
	// override if we're the last month group
	if ([[self valueForKey:@"rank"] intValue] == LAST_MONTH_COLLECTION_RANK)
		predicate = [NSKeyedUnarchiver unarchiveObjectWithData:[self predicateDataForLastMonthGroup]];
		
	
	NSPredicate *fetchPredicate, *inMemoryPredicate;
	NSCompoundPredicateType compoundType;
	
	[[MGFilterAndSortMetadataController sharedController] adjustSmartAlbumPredicate:predicate fetchPredicate:&fetchPredicate inMemoryPredicate:&inMemoryPredicate compoundType:&compoundType];
	
	
	// Just to be safe: two NIL predicates is invalid (equal to TRUEPREDICATE, which makes no sense)
	if (!fetchPredicate && !inMemoryPredicate)
		return nil;
	
	
	
	NSArray *finalResults = nil;

	
	if (inMemoryPredicate && compoundType == NSOrPredicateType) {
		
		// Fetch everything from the store, then OR filter with both predicates
		NSArray *images = [self imagesUsingStorePredicate:nil];
		
		NSMutableArray *subpreds = [NSMutableArray arrayWithObject:inMemoryPredicate];
		if (fetchPredicate)
			[subpreds addObject:fetchPredicate];
		
		NSPredicate *pred = [NSCompoundPredicate orPredicateWithSubpredicates:subpreds];
		
		finalResults = [images filteredArrayUsingPredicate:pred];
		
	}
	
	else {
		
		finalResults = [self imagesUsingStorePredicate:fetchPredicate];
		
		if (inMemoryPredicate)	// must be AND
			finalResults = [finalResults filteredArrayUsingPredicate:inMemoryPredicate];
		
		
	}
	
	
	return [NSSet setWithArray:finalResults];
	
}



- (NSArray *)imagesUsingStorePredicate:(NSPredicate *)predicate;
{
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Image" inManagedObjectContext:[MGLibraryControllerInstance managedObjectContext]];
	NSFetchRequest *req = [[[NSFetchRequest alloc] init] autorelease];
	if (predicate)
		[req setPredicate:predicate];
	[req setEntity:entity];
	return [[MGLibraryControllerInstance managedObjectContext] executeFetchRequest:req error:nil];
}


- (NSImage *)iconImage;
{
	NSImage *iconImage;
	if ([[self valueForKey:@"rank"] intValue] == LAST_MONTH_COLLECTION_RANK)
		iconImage = [NSImage imageNamed:@"lastMonthSmall"];

	else
		iconImage = [NSImage imageNamed:@"smartFolderSmall"];
	
	[iconImage setSize:NSMakeSize(16, 16)];
	return 	iconImage;
}

- (NSImage *)largeIconImage;
{
	return [NSImage imageNamed:@"smartFolderLarge"];
}


- (NSData *)actionImageData;
{
	if ([[self valueForKey:@"rank"] intValue] == LAST_MONTH_COLLECTION_RANK)
		return nil;

	return [[NSImage imageNamed:@"action.tif"] TIFFRepresentation];
}


- (id)parentGroup;
{
	return [self valueForKey:@"parent"];	
}




@end
