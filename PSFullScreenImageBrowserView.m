//
//  PSFullScreenImageBrowserView.m
//  Filament
//
//  Created by Peter Schols on 19/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "PSFullScreenImageBrowserView.h"
#import "PSImageView.h"

@implementation PSFullScreenImageBrowserView


// We don't want our full screen image broser to become first responder
// because we want all keyboard events to be forwarded to the PSImageView


- (void)awakeFromNib;
{
    
	[self setValue:[NSColor colorWithCalibratedWhite:33.0/255.0 alpha:1.0] forKey:IKImageBrowserBackgroundColorKey];
	[self setValue:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Lucida Grande" size:9.5], NSFontAttributeName, [NSColor whiteColor], @"NSColor", nil] forKey:IKImageBrowserCellsTitleAttributesKey];
	[self setValue:[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Lucida Grande" size:9.5], NSFontAttributeName, [NSColor whiteColor], @"NSColor", nil] forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];
	
	[self setPostsFrameChangedNotifications:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateZoomLevel:) name:NSViewFrameDidChangeNotification object:self];
	
}


- (void)updateZoomLevel:(NSNotification *)not
{
	[self setZoomValue:1.0];
	
}



// Forward all key events to the imageView, so it can handle the Cmd+, Cmd-, Esc, ...
- (void)keyDown:(NSEvent *)theEvent;
{	
	[imageView keyDown:theEvent];
}






@end
