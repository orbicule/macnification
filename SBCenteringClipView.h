/* SBCenteringClipView */

#import <Cocoa/Cocoa.h>

@interface SBCenteringClipView : NSClipView
{
}

- (void)centerDocument;
- (NSPoint)centerPoint;
- (void)setCenterPoint:(NSPoint)centerPoint;
@end

