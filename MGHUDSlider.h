//
//  MGHUDSlider.h
//  Filament
//
//  Created by Dennis Lorson on 14/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MGHUDSlider;
@class MGWidgetAttachedWindow;

@protocol MGHUDSliderDelegate <NSObject>

- (NSString *)valueToolTipForHUDSlider:(MGHUDSlider *)slider;

@end

@interface MGHUDSlider : NSSlider 
{
	SEL mouseUpSelector;
	id target;
    
    NSTimer *draggingTimer_;
    
    NSTextField *tooltipTextField;
	
	MGWidgetAttachedWindow *tooltipWindow;
    
    id <MGHUDSliderDelegate> delegate;
}

@property (readwrite, assign) id <MGHUDSliderDelegate> delegate;

- (void)setMouseUpAction:(SEL)action;

@end
