//
//  NSButton_MGAdditions.m
//  toolbarTest
//
//  Created by Dennis Lorson on 20/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "NSButton_MGAdditions.h"


@implementation NSButton (MGAdditions)


+ (id)toolStripButtonWithImage:(NSImage *)image title:(NSString *)title
{
	
	// set the default properties
	id button = [[[[self class] alloc] initWithFrame:NSMakeRect(0,0,50,46)] autorelease];
	
	[button setBordered:NO];
	[button setBezelStyle:NSRegularSquareBezelStyle];
	
	if (title) {
		NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:[NSColor colorWithCalibratedWhite:0.0 alpha:1.0], NSForegroundColorAttributeName, [NSFont systemFontOfSize:11], NSFontAttributeName, nil];
		NSAttributedString *attrTitle = [[[NSAttributedString alloc] initWithString:title attributes:attrs] autorelease];
		[button setAttributedTitle:attrTitle];
		[button setFont:[NSFont systemFontOfSize:11]];
		[button setImagePosition:NSImageAbove];
	}

	[button setImage:image];
	[button setButtonType:NSMomentaryChangeButton];
	
	[button sizeToFit];
	
	return button;
}



@end
