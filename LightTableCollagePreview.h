
//
//  LightTableMontagePreview.h
//  Filament
//
//  Created by Dennis Lorson on 21/07/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "LightTableImageToolView.h"


@interface LightTableCollagePreview : NSView {

	NSImage *image;
	NSRect selectionRect;
	NSRect imageRect;
	NSPoint dragStartPoint;
	NSColor *backgroundColor;
	
	ControlPoint currentControlPoint;
}


@property(nonatomic, retain) NSImage *image;


- (void)setBackgroundColor:(NSColor *)color;


- (NSRect)convertRect:(NSRect)normalized toRect:(NSRect)dest;
- (NSRect)convertRect:(NSRect)original fromRect:(NSRect)src;

- (void)resetSelectionRect;

- (void)drawShadeInOuterRect:(NSRect)outer innerRect:(NSRect)inner;
- (void)drawHandlesInRect:(NSRect)rect;
- (void)drawBackgroundPatternInRect:(NSRect)rect;


- (NSBezierPath *)pathForControlPoint:(ControlPoint)thePoint inRect:(NSRect)rect;
- (ControlPoint)controlPointAtPoint:(NSPoint)point inRect:(NSRect)rect;


- (NSImage *)selectedImage;
- (void)releaseResources;

@end
