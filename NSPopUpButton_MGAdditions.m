//
//  NSPopupButton_MGAdditions.m
//  StackTable
//
//  Created by Dennis Lorson on 22/10/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import "NSPopUpButton_MGAdditions.h"


@implementation NSPopUpButton (MGAdditions)


+ (NSPopUpButton *)toolStripButtonWithImage:(NSImage *)image title:(NSString *)title
{
	
	// set the default properties
	NSPopUpButton *button = [[[NSPopUpButton alloc] initWithFrame:NSMakeRect(0,0,50,46)] autorelease];
	
	[button setBordered:NO];
	[button setBezelStyle:NSRegularSquareBezelStyle];
	
	if (title) {
		[button setTitle:title];
		[button setFont:[NSFont systemFontOfSize:11]];
		[button setImagePosition:NSImageAbove];
	}
	
	[button setImage:image];
	[button setButtonType:NSMomentaryChangeButton];
	
	[button sizeToFit];
	
	return button;
}


@end
