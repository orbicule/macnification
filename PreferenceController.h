//
//  PreferenceController.h
//  Macnification
//
//  Created by Peter Schols on Wed Oct 4 2007.
//  Copyright (c) 2007 Orbicule. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface PreferenceController : NSWindowController <NSOpenSavePanelDelegate>
{

	IBOutlet NSButton *externalEditorButton;
	IBOutlet NSTextField *externalEditorField;	
	IBOutlet NSButton *externalAnalyzerButton;
	IBOutlet NSTextField *externalAnalyzerField;
	IBOutlet NSTextField *spreadsheetApplicationField;
	IBOutlet NSButton *spreadsheetApplicationButton;
	
	// Preference pane outlets
	IBOutlet NSToolbar *toolbar;
	IBOutlet NSView *view1, *view2, *view3, *view4, *view5, *view6, *view7, *view8, *view9;
	IBOutlet NSView *emptyView;
}

- (IBAction)restoreWarningsAndDialogs:(id)sender;
- (IBAction)chooseApplication:(id)sender;
- (void)updateExternalEditorButton;
- (void)updateExternalAnalyzerButton;
- (void)updateSpreadsheetAppButton;


// Preference pane actions
- (IBAction)switchToPane:(id)sender;
- (void)switchToPreferencePane:(NSUInteger)paneNumber;
- (void)switchToPreferencePaneAndSelectPreference:(NSUInteger)paneNumber;

- (IBAction)changeLibraryPath:(id)sender;



@end
