//
//  NSView_MGAdditions.h
//  LightTable
//
//  Created by Dennis Lorson on 19/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSView (MGAdditions)

- (NSPoint)convertPointToGlobal:(NSPoint)viewPoint;
- (NSRect)frameForNewSizeRetainingCenter:(NSSize)newFrameSize;


- (NSComparisonResult)compareFrameX:(NSView *)theView;
- (NSComparisonResult)compareFrameY:(NSView *)theView;

- (NSComparisonResult)compareFrameCenterX:(NSView *)theView;
- (NSComparisonResult)compareFrameCenterY:(NSView *)theView;

- (NSComparisonResult)compareFrameWidth:(NSView *)theView;
- (NSComparisonResult)compareFrameHeight:(NSView *)theView;


- (NSImage *)viewImage;


@end
