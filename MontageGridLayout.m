//
//  MontageGridLayout.m
//  Stack
//
//  Created by Dennis Lorson on 2/02/08.
//  Copyright 2008 Orbicule. All rights reserved.
//

#import "MontageGridLayout.h"

#import "Montage.h"

@interface MontageGridLayout (Private)

+ (NSDictionary *)adjustNbRowsForUnitToFit:(int)unitIndex withParameters:(NSDictionary *)parameters;

@end


@implementation MontageGridLayout


+ (NSDictionary *)pageAndFrameForUnit:(int)unitIndex withParameters:(NSDictionary *)parameters
{
	// given the index of a visual unit (image + attrs) in the montage array, this method will return a suitable rect for this unit, in the coordinate space of the units.
	// assume all images have the same dimensions (if not, larger images will be scaled later on?)
	
	// parameters contains the composite size, unit size, image margin.
	NSSize unitMargin = [[parameters valueForKey:@"unitMargin"] sizeValue];
	NSInteger unitsPerRow = [[parameters valueForKey:@"unitsPerRow"] intValue];
	//NSInteger nbUnits = [[parameters valueForKey:@"nbUnits"] intValue];
	
	CGFloat attributeHeight = [[parameters valueForKey:@"attributeHeight"] floatValue];
	NSSize compositeSize = [[parameters valueForKey:@"compositeSize"] sizeValue];
	NSSize desireImagedSize = [[parameters valueForKey:@"desiredImagedSize"] sizeValue];			// the desired size of the images (the ideal width/height ratio).  Based on one of the images.
	
	// fit on one page?
	if (unitsPerRow == -1) return [[self class] adjustNbRowsForUnitToFit:unitIndex withParameters:parameters];
	
	// determine the size of one image based on papersize and nb images in one row.
	CGFloat imageWidth = (compositeSize.width - (unitsPerRow - 1) * unitMargin.width) / ((CGFloat)unitsPerRow);
	CGFloat unitWidth = imageWidth;
	
	CGFloat imageHeight = desireImagedSize.height / desireImagedSize.width * imageWidth;
	CGFloat unitHeight = imageHeight + attributeHeight;
	
	// compute the number of images per column per page.
	CGFloat pageHeight = compositeSize.height;
	NSInteger unitsPerColumn = floorf((pageHeight + unitMargin.height) / (unitHeight + unitMargin.height));
	//NSLog(@"percol:%i", imagesPerColumn);
	
	//NSInteger numberOfPages = ceilf((CGFloat)nbUnits / ((CGFloat)(unitsPerRow * unitsPerColumn)));
	
	// determine the row and column coordinates of the image on its page. (index starts at 0)
	NSInteger indexOfUnitOnPage = fmodf(unitIndex, unitsPerRow * unitsPerColumn);
	NSInteger unitColumn = fmodf(indexOfUnitOnPage, unitsPerRow);
	NSInteger unitRow = (indexOfUnitOnPage - unitColumn) / unitsPerRow;
	//NSLog(@"row %i, col %i", unitRow, unitColumn);
	
	// determine the page the image in question is on.
	NSInteger pageNumber = (unitIndex - indexOfUnitOnPage) / (unitsPerRow * unitsPerColumn);
	
	// the x-coordinate equals:
	CGFloat unitX = unitColumn * (unitWidth + unitMargin.width);
	
	// y-coordinate : be sure to include the height of the following pages (we position the first page at the bottom and go up). 
	CGFloat unitY = unitRow * unitHeight + (unitRow - 1) * unitMargin.height;
	
	
	//NSLog(@"col %i row %i    ---   %@", imageColumn, imageRow,NSStringFromRect(NSMakeRect(imageX, imageY, imageWidth, imageHeight)));
	
	NSNumber *pageValue = [NSNumber numberWithInt:pageNumber];
	NSValue *unitFrame = [NSValue valueWithRect:NSMakeRect(unitX, unitY, unitWidth, unitHeight)];
	
	return [NSDictionary dictionaryWithObjectsAndKeys:pageValue, @"pageNumber", unitFrame, @"unitFrame", nil];
}


+ (NSDictionary *)adjustNbRowsForUnitToFit:(int)unitIndex withParameters:(NSDictionary *)parameters
{
	// parameters contains the composite size, unit size, image margin.
	NSSize unitMargin = [[parameters valueForKey:@"unitMargin"] sizeValue];
	NSInteger nbUnits = [[parameters valueForKey:@"nbUnits"] intValue];
	
	CGFloat attributeHeight = [[parameters valueForKey:@"attributeHeight"] floatValue];
	NSSize compositeSize = [[parameters valueForKey:@"compositeSize"] sizeValue];
	NSSize desireImagedSize = [[parameters valueForKey:@"desiredImagedSize"] sizeValue];			// the desired size of the images (the ideal width/height ratio).  Based on one of the images.
	
	
	CGFloat hpage = compositeSize.height;
	CGFloat wpage = compositeSize.width;
	
	CGFloat hdes = desireImagedSize.height;
	CGFloat wdes = desireImagedSize.width;
	
	CGFloat hmarg = unitMargin.height;
	CGFloat wmarg = unitMargin.width + 10;
	
	CGFloat c = (hpage + hmarg) * wdes / (hdes * (CGFloat)nbUnits);
	CGFloat d = - (hmarg + attributeHeight) * wdes / hdes - wmarg;
	CGFloat e = - (wpage - wmarg);
	
	// newer (still not correct)
	//CGFloat d = - (hmarg + attributeHeight) * wdes / hdes - wmarg;
	//CGFloat e = - (wpage - wmarg);
	
	// old (not correct)
	//CGFloat d = - (hmarg + attributeHeight) * wdes / hdes;
	//CGFloat e = - (wpage + wmarg);
	
	
	// solve this
	float signd = 1;
	if ( d < 0 ) signd = -1;
	float t;
	
	// make sure we have a valid discriminator
	if (d*d - 4*c*e > 0)
		t = - (d + signd * sqrt (d*d - 4*c*e))/2;
	else
		t = - d/2;
	
	// solution
	
	float r1 = t/c;
	float r2 = e/t;

	//if (unitIndex == 0)
		//NSLog(@"index %i of %i -- attrheight %f --- r1 %f, r2 %f", unitIndex,nbUnits, attributeHeight, r1, r2);
	
	NSInteger nrow = 0;
	
	if (r1 > 0) nrow = ceilf(r1);
	if (r2 > 0) nrow = ceilf(r2);
	
	NSMutableDictionary *paramCopy = [[parameters mutableCopy] autorelease];
	[paramCopy setValue:[NSNumber numberWithInt:nrow] forKey:@"unitsPerRow"];
	
	return [[self class] pageAndFrameForUnit:unitIndex withParameters:paramCopy];
}


@end
