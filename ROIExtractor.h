//
//  ROIExtractor.h
//  BlobExtraction
//
//  Created by Dennis on 10/07/07.
//  Copyright 2007 Orbicule. All rights reserved.
//

#import <Cocoa/Cocoa.h>


static NSString *ROIExtractionOperationDidEndNotification = @"ROIExtractionOperationDidEndNotification";


@interface ROIExtractor : NSObject {

	// private
	
	NSOperationQueue *queue;				// the queue that all operations will be placed on.
	
	// public
	
	NSBitmapImageRep *image;				// the image to be processed. Not optional.
	
	BOOL searchesInnerROIs;					// determines whether the found ROIs will be searched for additional ROIs. Default is YES.
	BOOL allowsImageEdgeROIs;				// determines whether or not blobs that reach the edge of the image are allowed. Default is YES.
	NSInteger minimumROIArea;
	
	CGFloat maxInterpolationError;			// the resolution used when interpolating blobs.

}

@property (copy, readwrite) NSBitmapImageRep *image;
@property (readwrite) NSInteger minimumROIArea;
@property (readwrite) BOOL allowsImageEdgeROIs, searchesInnerROIs;
@property (readwrite) CGFloat maxInterpolationError;

- (id)initWithImage:(NSBitmapImageRep *)theImage;
- (void)extract;


@end
