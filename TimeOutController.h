//
//  TimeOutController.h
//  Filament
//
//  Created by Peter Schols on 31/03/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface TimeOutController : NSWindowController {

	IBOutlet id progressBar;
	IBOutlet NSTextField *daysLeftField;
	
}


- (IBAction)buyOnline:(id)sender;
- (IBAction)enterLicenseKey:(id)sender;
- (NSInteger)daysLeft;


@end
